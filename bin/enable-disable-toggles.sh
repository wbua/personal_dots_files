#!/bin/bash

# Program command: enable-disable-toggles.sh
# Description: Enable/disable variables in settings config.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 04-03-2025
# Program_license: GPL 3.0
# Dependencies: yad, curl

# Exit on non zero status
set -e

#-----------------------------------------------------#
# Source files
#-----------------------------------------------------#

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#-----------------------------------------------------#
# User preferences
#-----------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Screencast sound
MY_SRN_CAST_SND="$SCREENCAST_SOUND"
export MY_SRN_CAST_SND

# Color auto
MY_COLOR_AUTO="$COLOR_AUTO"
export MY_COLOR_AUTO

# Rename screenshot
MY_SCREENSHOT_FILENAME="$SCREENSHOT_FILENAME"
export MY_SCREENSHOT_FILENAME

# Random wallpaper preview
MY_RANDOM_PREVIEW="$RANDOM_PREVIEW"
export MY_RANDOM_PREVIEW

# Use screen or wallpaper for lock screen
MY_SWAY_LOCKING_BACKGROUND="$SWAY_LOCKING_BACKGROUND"
export MY_SWAY_LOCKING_BACKGROUND

# System sounds
MY_SYSTEM_SOUNDS="$SYSTEM_SOUNDS"
export MY_SYSTEM_SOUNDS

# Color icon
MY_COLOR_ICON="$COLOR_ICON"
export MY_COLOR_ICON

# BBC weather
WEATHER_API_KEY="$MAIN_WEATHER_API_KEY"
export WEATHER_API_KEY

# Status text
STAT_INFO="<span color='$MY_COLOR_ICON' font_family='Inter' font_weight='heavy' font='14' rise='-1pt'>"
export STAT_INFO

END="</span>"
export END

# Development
MY_MAIN_DEV="$MAIN_DEV"
export MY_MAIN_DEV

#-----------------------------------------------------#
# Enable/disable screencast sound
#-----------------------------------------------------#

screen_recording_sound() {

killall yad

   if [[ "$(grep SCREENCAST_SOUND= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "on" ]]; then
     sed -i "s|.*\(SCREENCAST_SOUND=\).*|SCREENCAST_SOUND=\"$(printf '%s' "off")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep SCREENCAST_SOUND= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "off" ]]; then
     sed -i "s|.*\(SCREENCAST_SOUND=\).*|SCREENCAST_SOUND=\"$(printf '%s' "on")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

    # Setting config
    source ~/bin/sway_user_preferences.sh

    # Update the MY_SRN_CAST_SND variable
    export MY_SRN_CAST_SND="$SCREENCAST_SOUND"

    # Notify the updated status
    #notify-send "Screencast sound" "Status: $MY_SRN_CAST_SND"
	
} 
export -f screen_recording_sound

#-----------------------------------------------------#
# Enable/disable color auto
#-----------------------------------------------------#

change_color_auto() {

killall yad

   if [[ "$(grep COLOR_AUTO= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(COLOR_AUTO=\).*|COLOR_AUTO=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep COLOR_AUTO= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(COLOR_AUTO=\).*|COLOR_AUTO=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

    source ~/bin/sway_user_preferences.sh

    export MY_COLOR_AUTO="$COLOR_AUTO"

    #notify-send "Color auto" "Status: $MY_COLOR_AUTO"
	
}
export -f change_color_auto

#-----------------------------------------------------#
# Rename screenshot
#-----------------------------------------------------#

rename_screenshot() {

killall yad

   if [[ "$(grep SCREENSHOT_FILENAME= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(SCREENSHOT_FILENAME=\).*|SCREENSHOT_FILENAME=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep SCREENSHOT_FILENAME= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(SCREENSHOT_FILENAME=\).*|SCREENSHOT_FILENAME=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

    source ~/bin/sway_user_preferences.sh

    export MY_SCREENSHOT_FILENAME="$SCREENSHOT_FILENAME"

    #notify-send "Rename screenshot" "Status: $SCREENSHOT_FILENAME"

}
export -f rename_screenshot

#-----------------------------------------------------#
# Random wallpaper preview
#-----------------------------------------------------#

random_wallpapers() {

killall yad

   if [[ "$(grep RANDOM_PREVIEW= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(RANDOM_PREVIEW=\).*|RANDOM_PREVIEW=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep RANDOM_PREVIEW= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(RANDOM_PREVIEW=\).*|RANDOM_PREVIEW=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

    source ~/bin/sway_user_preferences.sh

    export MY_RANDOM_PREVIEW="$RANDOM_PREVIEW"

    #notify-send "Random wallpaper preview" "Status: $MY_RANDOM_PREVIEW"
	
}
export -f random_wallpapers

#-----------------------------------------------------#
# Screen/wallpaper for lockscreen
#-----------------------------------------------------#

srn_wall_lock() {

killall yad

   if [[ "$(grep SWAY_LOCKING_BACKGROUND= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(SWAY_LOCKING_BACKGROUND=\).*|SWAY_LOCKING_BACKGROUND=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep SWAY_LOCKING_BACKGROUND= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(SWAY_LOCKING_BACKGROUND=\).*|SWAY_LOCKING_BACKGROUND=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

    source ~/bin/sway_user_preferences.sh

    export MY_SWAY_LOCKING_BACKGROUND="$SWAY_LOCKING_BACKGROUND"

    #notify-send "Screen/wallpaper lockscreen" "Status: $MY_SWAY_LOCKING_BACKGROUND"
	
}
export -f srn_wall_lock

#-----------------------------------------------------#
# System sound
#-----------------------------------------------------#

system_sounds() {

killall yad

   if [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(SYSTEM_SOUNDS=\).*|SYSTEM_SOUNDS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(SYSTEM_SOUNDS=\).*|SYSTEM_SOUNDS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

    source ~/bin/sway_user_preferences.sh

    export MY_SYSTEM_SOUNDS="$SYSTEM_SOUNDS"

    #notify-send "System sounds" "Status: $MY_SYSTEM_SOUNDS"
	
}
export -f system_sounds

#-----------------------------------------------------#
# Change screenshot delay time
#-----------------------------------------------------#

screenshot_delay_time() {

killall yad

delay_choice=$(GTK_THEME="$MY_GTK_THEME" | yad --form \
 --title="Change screenshot delay time" \
 --text-align="center" \
 --text="\nEnter delay time...\n" \
 --borders=10 \
 --width=300 \
 --height=200 \
 --separator= \
 --field="Delay  ":CE \
 --button="Ok":0 \
 --button="Exit":1)
[ -z "$delay_choice" ] && exit 0
  
sed -i "s|.*\(SCREENSHOT_DELAY=\).*|SCREENSHOT_DELAY=\"$(printf '%s' "$delay_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

source ~/bin/sway_user_preferences.sh

export screenshot_delay_time

#notify-send "Screenshot delay" "Status: $SCREENSHOT_DELAY"
	
}
export -f screenshot_delay_time

#-----------------------------------------------------#
# Screenshot notification open/go to file
#-----------------------------------------------------#

screenshot_notification_actions() {

killall yad

   if [[ "$(grep SCREENSHOT_NOTIFICATION= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "open" ]]; then
     sed -i "s|.*\(SCREENSHOT_NOTIFICATION=\).*|SCREENSHOT_NOTIFICATION=\"$(printf '%s' "file")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep SCREENSHOT_NOTIFICATION= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "file" ]]; then
     sed -i "s|.*\(SCREENSHOT_NOTIFICATION=\).*|SCREENSHOT_NOTIFICATION=\"$(printf '%s' "open")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

    source ~/bin/sway_user_preferences.sh

    export screenshot_notification_actions

    #notify-send "Screenshot notification" "Status: $SCREENSHOT_NOTIFICATION"
	
}
export -f screenshot_notification_actions

#-----------------------------------------------------#
# Switch to browser when opening something in browser
#-----------------------------------------------------#

opening_switch_to_browser() {

killall yad

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(SWITCH_TO_BROWSER=\).*|SWITCH_TO_BROWSER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(SWITCH_TO_BROWSER=\).*|SWITCH_TO_BROWSER=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

    source ~/bin/sway_user_preferences.sh

    export opening_switch_to_browser

    #notify-send "Switch to browser" "Status: $SWITCH_TO_BROWSER"
	
}
export -f opening_switch_to_browser

#-----------------------------------------------------#
# Switch to browser when opening something in browser
#-----------------------------------------------------#

opening_switch_to_filemanager() {

killall yad

   if [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(SWITCH_TO_FILE_MANAGER=\).*|SWITCH_TO_FILE_MANAGER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(SWITCH_TO_FILE_MANAGER=\).*|SWITCH_TO_FILE_MANAGER=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

    source ~/bin/sway_user_preferences.sh

    export opening_switch_to_filemanager

    #notify-send "Switch to file manager" "Status: $SWITCH_TO_FILE_MANAGER"
	
}
export -f opening_switch_to_filemanager

#-----------------------------------------------------#
# Change wttr weather location
#-----------------------------------------------------#

wttr_weather_location() {

killall yad

location_choice=$(GTK_THEME="$MY_GTK_THEME" | yad --form \
 --title="Change wttr weather location" \
 --text-align="center" \
 --text="\nFormat: new-york or london (no spaces)\nEnter weather location...\n" \
 --borders=10 \
 --width=300 \
 --height=200 \
 --separator= \
 --field="Location  ":CE \
 --button="Ok":0 \
 --button="Exit":1)
[ -z "$location_choice" ] && exit 0

sed -i "s|.*\(MAIN_WTTR_API_KEY=\).*|MAIN_WTTR_API_KEY=\"$(printf '%s' "$location_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

curl wttr.in/"$WTTR_API_KEY"?format="%C%t\n" | tee ~/Downloads/.wttr_temp.txt
curl wttr.in/"$WTTR_API_KEY" | tee ~/Documents/.weather_file.rtf

source ~/bin/sway_user_preferences.sh

export wttr_weather_location

#notify-send "Weather location" "Status: $MAIN_WTTR_API_KEY"
	
}
export -f wttr_weather_location

#-----------------------------------------------------#
# Change BBC weather location
#-----------------------------------------------------#

bbc_weather_location() {

killall yad

location_choice=$(GTK_THEME="$MY_GTK_THEME" | yad --form \
 --title="Change BBC weather location" \
 --text-align="center" \
 --text="
BBC weather api key (website: https://www.bbc.co.uk/weather)
Enter your city, for example london.
You will get this result: https://www.bbc.co.uk/weather/2643743
The api key is 7 digits.

Enter 7 digit key..." \
 --borders=10 \
 --width=800 \
 --height=200 \
 --separator= \
 --field="Location  ":CE \
 --button="Ok":0 \
 --button="Exit":1)
[ -z "$location_choice" ] && exit 0

sed -i "s|.*\(MAIN_WEATHER_API_KEY=\).*|MAIN_WEATHER_API_KEY=\"$(printf '%s' "$location_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

rm -rf "$HOME"/Downloads/.temp_weather/* || exit 1
wget https://weather-broker-cdn.api.bbci.co.uk/en/forecast/rss/3day/"$WEATHER_API_KEY" -O "$HOME"/Downloads/.temp_weather/downloadedfile > /dev/null

source ~/bin/sway_user_preferences.sh

export bbc_weather_location

#notify-send "Weather location" "Status: $WEATHER_API_KEY"
	
}
export -f bbc_weather_location

#-----------------------------------------------------#
# Change keyboard layout
#-----------------------------------------------------#

change_keyboard_layout() {

killall yad

key_layout_choice=$(GTK_THEME="$MY_GTK_THEME" | yad --form \
 --title="Change keyboard layout" \
 --text-align="center" \
 --text="\nExamples: gb, us, de\nEnter keyboard layout...\n" \
 --borders=10 \
 --width=300 \
 --height=200 \
 --separator= \
 --field="Layout  ":CE \
 --button="Ok":0 \
 --button="Exit":1)
[ -z "$key_layout_choice" ] && exit 0
  
sed -i "s|.*\(SWAY_KEYBOARD_LAYOUT=\).*|SWAY_KEYBOARD_LAYOUT=\"$(printf '%s' "$key_layout_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

setsid -f bash ~/bin/sway_load_settings.sh >/dev/null 2>&1 & disown

source ~/bin/sway_user_preferences.sh

export change_keyboard_layout

#notify-send "Keyboard layout" "Status: $SWAY_KEYBOARD_LAYOUT"
	
}
export -f change_keyboard_layout

#-----------------------------------------------------#
# Specific app workspaces
#-----------------------------------------------------#

specific_workspaces() {

killall yad

   if [[ "$(grep SPECIFIC_APP_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(SPECIFIC_APP_WORKSPACES=\).*|SPECIFIC_APP_WORKSPACES=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep SPECIFIC_APP_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(SPECIFIC_APP_WORKSPACES=\).*|SPECIFIC_APP_WORKSPACES=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

    source ~/bin/sway_user_preferences.sh

    export specific_workspaces

    #notify-send "Specific workspaces" "Status: $SPECIFIC_APP_WORKSPACES"
	
}
export -f specific_workspaces

#-----------------------------------------------------#
# Change date and time for date/weather module
#-----------------------------------------------------#

date_time_dual_module() {

killall yad

date_choice=$(GTK_THEME="$MY_GTK_THEME" | yad --form \
 --title="Change date and time" \
 --text-align="center" \
 --text="\n
 Dates: %d-%m-%Y = 10-10-2024 or %Y-%m-%d = 2024-10-10
 Times: %I:%M %P = 10:17 am - 12 hour or %H:%M = 10:18 - 24 hour

 Enter date and time...\n" \
 --borders=10 \
 --width=700 \
 --height=300 \
 --separator= \
 --field="Format  ":CE \
 --button="Ok":0 \
 --button="Exit":1)
[ -z "$date_choice" ] && exit 0
  
sed -i "s|.*\(I3BLOCKS_DATE_FORMAT=\).*|I3BLOCKS_DATE_FORMAT=\"$(printf '%s' "$date_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

setsid -f bash ~/bin/sway_load_settings.sh >/dev/null 2>&1 & disown

source ~/bin/sway_user_preferences.sh

export date_time_dual_module

#notify-send "Date/time format" "Status: $I3BLOCKS_DATE_FORMAT"
	
}
export -f date_time_dual_module

#-----------------------------------------------------#
# Change date and time for date/time only module
#-----------------------------------------------------#

data_time_only_module() {

killall yad

time_only_choice=$(GTK_THEME="$MY_GTK_THEME" | yad --form \
 --title="Change date and time" \
 --text-align="center" \
 --text="\n
 Dates: %d-%m-%Y = 10-10-2024 or %Y-%m-%d = 2024-10-10
 Times: %I:%M %P = 10:17 am - 12 hour or %H:%M = 10:18 - 24 hour

 Enter date and time...\n" \
 --borders=10 \
 --width=700 \
 --height=300 \
 --separator= \
 --field="Format  ":CE \
 --button="Ok":0 \
 --button="Exit":1)
[ -z "$time_only_choice" ] && exit 0
  
sed -i "s|.*\(I3BLOCKS_TIME_FORMAT=\).*|I3BLOCKS_TIME_FORMAT=\"$(printf '%s' "$time_only_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

setsid -f bash ~/bin/sway_load_settings.sh >/dev/null 2>&1 & disown

source ~/bin/sway_user_preferences.sh

export data_time_only_module

#notify-send "Date/time format" "Status: $I3BLOCKS_TIME_FORMAT"
	
}
export -f data_time_only_module

#-----------------------------------------------------#
# Kill confirm app
#-----------------------------------------------------#

kill_confirm() {

killall yad

   if [[ "$(grep CONFIRM_KILL_APP= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(CONFIRM_KILL_APP=\).*|CONFIRM_KILL_APP=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep CONFIRM_KILL_APP= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(CONFIRM_KILL_APP=\).*|CONFIRM_KILL_APP=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

    source ~/bin/sway_user_preferences.sh

    export kill_confirm

    #notify-send "Kill confirm" "Status: $CONFIRM_KILL_APP"
	
}
export -f kill_confirm

#-----------------------------------------------------#
# Yad form dialog
#-----------------------------------------------------#

open_config() {

killall yad
check_texteditor=$(xdg-mime query default text/plain | sed 's|.desktop||')

"$check_texteditor" ~/bin/sway_user_preferences.sh	

}
export -f open_config

#-----------------------------------------------------#
# Themes config
#-----------------------------------------------------#

open_theme_config() {

killall yad
check_texteditor=$(xdg-mime query default text/plain | sed 's|.desktop||')

"$check_texteditor" ~/bin/i3blocks_accent_colors
	
}
export -f open_theme_config

#-----------------------------------------------------#
# Pango rise config
#-----------------------------------------------------#

open_other_config() {

killall yad
check_texteditor=$(xdg-mime query default text/plain | sed 's|.desktop||')

"$check_texteditor" ~/bin/i3blocks_pango_rise.sh
	
}
export -f open_other_config

#-----------------------------------------------------#
# Random lockscreen wallpaper
#-----------------------------------------------------#

random_wallpaper_lockscreen() {

killall yad

   if [[ "$(grep SWAY_RANDOM_LOCKING_BG= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(SWAY_RANDOM_LOCKING_BG=\).*|SWAY_RANDOM_LOCKING_BG=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep SWAY_RANDOM_LOCKING_BG= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(SWAY_RANDOM_LOCKING_BG=\).*|SWAY_RANDOM_LOCKING_BG=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

    source ~/bin/sway_user_preferences.sh

    export random_wallpaper_lockscreen

    #notify-send "Random lockscreen wallpaper" "Status: $SWAY_RANDOM_LOCKING_BG"
	
}
export -f random_wallpaper_lockscreen

#-----------------------------------------------------#
# Active app shortcut mode
#-----------------------------------------------------#

active_app_shortcut() {

killall yad

   if [[ "$(grep ACTIVE_SHORTCUT_STATUS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(ACTIVE_SHORTCUT_STATUS=\).*|ACTIVE_SHORTCUT_STATUS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep ACTIVE_SHORTCUT_STATUS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(ACTIVE_SHORTCUT_STATUS=\).*|ACTIVE_SHORTCUT_STATUS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

    source ~/bin/sway_user_preferences.sh

    export active_app_shortcut

    #notify-send "Active app mode" "Status: $ACTIVE_SHORTCUT_STATUS"
	
}
export -f active_app_shortcut

#-----------------------------------------------------#
# Nightlight
#-----------------------------------------------------#

nightlight_value() {

killall yad

nightlight_choice=$(GTK_THEME="$MY_GTK_THEME" | yad --form \
 --title="Change nightlight value" \
 --text-align="center" \
 --text="Enter nightlight value...\n" \
 --borders=10 \
 --width=300 \
 --height=200 \
 --separator= \
 --field="Value  ":CE \
 --button="Ok":0 \
 --button="Exit":1)
[ -z "$nightlight_choice" ] && exit 0
  
setsid sed -i "s|.*\(COLOR_TEMPERATURE_VALUE=\).*|COLOR_TEMPERATURE_VALUE=\"$(printf '%s' "$nightlight_choice")\"|" ~/bin/sway_user_preferences.sh >/dev/null 2>&1 & disown || exit 1

source ~/bin/sway_user_preferences.sh

export nightlight_value

#notify-send "Nightlight value" "Status: $COLOR_TEMPERATURE_VALUE"
	
}
export -f nightlight_value

#-----------------------------------------------------#
# Window title length
#-----------------------------------------------------#

title_length() {

killall yad

title_choice=$(GTK_THEME="$MY_GTK_THEME" | yad --form \
 --title="Change title length" \
 --text-align="center" \
 --text="Enter title length...\n" \
 --borders=10 \
 --width=300 \
 --height=200 \
 --separator= \
 --field="Length  ":CE \
 --button="Ok":0 \
 --button="Exit":1)
[ -z "$title_choice" ] && exit 0
  
setsid sed -i "s|.*\(MODULE_TITLE_LENGTH=\).*|MODULE_TITLE_LENGTH=\"$(printf '%s' "$title_choice")\"|" ~/bin/sway_user_preferences.sh >/dev/null 2>&1 & disown || exit 1

source ~/bin/sway_user_preferences.sh

export title_length

#notify-send "Title length" "Status: $MODULE_TITLE_LENGTH"
	
}
export -f title_length

#-----------------------------------------------------#
# Yad form dialog
#-----------------------------------------------------#

GTK_THEME="$MY_GTK_THEME" yad --form \
 --title="Change settings" \
 --scroll \
 --borders=20 \
 --text-align="Center" \
 --text="\nChange settings\n" \
 --columns=1 \
 --width=650 \
 --height=500 \
 --field="<b>System sounds: ${STAT_INFO}$MY_SYSTEM_SOUNDS${END}</b>":fbtn '/bin/bash -c "system_sounds"' \
 --field="<b>Random wallpaper preview: ${STAT_INFO}$MY_RANDOM_PREVIEW${END}</b>":fbtn '/bin/bash -c "random_wallpapers"' \
 --field="<b>Screencast sound: ${STAT_INFO}$MY_SRN_CAST_SND${END}</b>":fbtn '/bin/bash -c "screen_recording_sound"' \
 --field="<b>Color auto (like pywal): ${STAT_INFO}$MY_COLOR_AUTO${END}</b>":fbtn '/bin/bash -c "change_color_auto"' \
 --field="<b>Rename screenshot: ${STAT_INFO}$MY_SCREENSHOT_FILENAME${END}</b>":fbtn '/bin/bash -c "rename_screenshot"' \
 --field="<b>Screen/wallpaper for lockscreen: ${STAT_INFO}$MY_SWAY_LOCKING_BACKGROUND${END}</b>":fbtn '/bin/bash -c "srn_wall_lock"' \
 --field="<b>Random locking wallpaper: ${STAT_INFO}$SWAY_RANDOM_LOCKING_BG${END}</b>":fbtn '/bin/bash -c "random_wallpaper_lockscreen"' \
 --field="<b>Screenshot delay: ${STAT_INFO}$SCREENSHOT_DELAY${END}</b>":fbtn '/bin/bash -c "screenshot_delay_time"' \
 --field="<b>Screenshot notification open/file: ${STAT_INFO}$SCREENSHOT_NOTIFICATION${END}</b>":fbtn '/bin/bash -c "screenshot_notification_actions"' \
 --field="<b>Switch to browser: ${STAT_INFO}$SWITCH_TO_BROWSER${END}</b>":fbtn '/bin/bash -c "opening_switch_to_browser"' \
 --field="<b>Switch to file manager: ${STAT_INFO}$SWITCH_TO_FILE_MANAGER${END}</b>":fbtn '/bin/bash -c "opening_switch_to_filemanager"' \
 --field="<b>Keyboard layout: ${STAT_INFO}$SWAY_KEYBOARD_LAYOUT${END}</b>":fbtn '/bin/bash -c "change_keyboard_layout"' \
 --field="<b>Specific workspaces: ${STAT_INFO}$SPECIFIC_APP_WORKSPACES${END}</b>":fbtn '/bin/bash -c "specific_workspaces"' \
 --field="<b>Date/time format dual: ${STAT_INFO}$I3BLOCKS_DATE_FORMAT${END}</b>":fbtn '/bin/bash -c "date_time_dual_module"' \
 --field="<b>Date/time format: ${STAT_INFO}$I3BLOCKS_TIME_FORMAT${END}</b>":fbtn '/bin/bash -c "data_time_only_module"' \
 --field="<b>Kill confirm: ${STAT_INFO}$CONFIRM_KILL_APP${END}</b>":fbtn '/bin/bash -c "kill_confirm"' \
 --field="<b>Active app active/shortcut: ${STAT_INFO}$ACTIVE_SHORTCUT_STATUS${END}</b>":fbtn '/bin/bash -c "active_app_shortcut"' \
 --field="<b>Window title length: ${STAT_INFO}$MODULE_TITLE_LENGTH${END}</b>":fbtn '/bin/bash -c "title_length"' \
 --field="<b>Weather location (wttr): ${STAT_INFO}$MAIN_WTTR_API_KEY${END}</b>":fbtn '/bin/bash -c "wttr_weather_location"' \
 --field="<b>Weather location (BBC): ${STAT_INFO}$MAIN_WEATHER_API_KEY${END}</b>":fbtn '/bin/bash -c "bbc_weather_location"' \
 --field="<b>Nightlight value: ${STAT_INFO}$COLOR_TEMPERATURE_VALUE${END}</b>":fbtn '/bin/bash -c "nightlight_value"' \
 --field="":LBL "" \
 --field="":LBL "" \
 --button='Settings config:/bin/bash -c "open_config"' \
 --button='Theme config:/bin/bash -c "open_theme_config"' \
 --button='Other config:/bin/bash -c "open_other_config"' \
 --button="Exit":1

#-----------------------------------------------------#
# Unset variables and functions
#-----------------------------------------------------#

unset MY_GTK_THEME
unset screen_recording_sound
unset MY_SRN_CAST_SND
unset change_color_auto
unset MY_COLOR_AUTO
unset MY_SCREENSHOT_FILENAME
unset rename_screenshot
unset MY_RANDOM_PREVIEW
unset random_wallpapers
unset MY_SWAY_LOCKING_BACKGROUND
unset srn_wall_lock
unset MY_SYSTEM_SOUNDS
unset system_sounds
unset MY_COLOR_ICON
unset END
unset STAT_INFO
unset screenshot_delay_time
unset screenshot_notification_actions
unset opening_switch_to_browser
unset opening_switch_to_filemanager
unset wttr_weather_location
unset WEATHER_API_KEY
unset bbc_weather_location
unset change_keyboard_layout
unset specific_workspaces
unset date_time_dual_module
unset data_time_only_module
unset kill_confirm
unset open_config
unset MY_MAIN_DEV
unset open_theme_config
unset open_other_config
unset random_wallpaper_lockscreen
unset active_app_shortcut
unset nightlight_value
unset title_length
