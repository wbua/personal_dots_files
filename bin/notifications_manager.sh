#!/bin/bash

set -e

# Define the file to save notification details
notif_file="/tmp/dunst_notifications.txt"

# Retrieve notifications from dunst history
history=$(dunstctl history | jq -c '.data[0][]')

# Clear the file before adding new notifications
truncate -s 0 "$notif_file" || exit 1

# Loop through each notification and write the details to the file
echo "$history" | while read -r notif; do
    icon_path=$(echo "$notif" | jq -r '.icon_path.data')
    appname=$(echo "$notif" | jq -r '.appname.data')
    summary=$(echo "$notif" | jq -r '.summary.data')
    body=$(echo "$notif" | jq -r '.body.data')
        
    # Write the notification details to the file
    printf "\"%s\" \"%s\" \"%s\" \"%s\"\n" "$icon_path" "$appname" "$summary" "$body" >> "$notif_file"
done

# Read the notification details into an array in the correct format
declare -a yad_data="($(while IFS= read -r line; do echo "$line"; done < "$notif_file"))"

# Display the notifications using yad --list
printf '%s\n' "${yad_data[@]}" | yad --list \
    --width=1200 \
    --height=400 \
    --title="Dunst Notifications" \
    --column="Icon Path"\
    --column="App Name" \
    --column="Summary" \
    --column="Body"
    
# Clean up temporary file
rm "$notif_file" || exit 1
