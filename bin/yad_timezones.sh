#!/bin/bash

# Program command: yad_timezones.sh
# Description: Shows timezones. Used with polybar.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 02-02-2025
# Program_license: GPL 3.0
# Dependencies: yad, wl-clipboard

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#--------------------------------------------------------------------#
# User preferences
#--------------------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#--------------------------------------------------------------------#
# Yad settings
#--------------------------------------------------------------------#

_yad() {
	
GTK_THEME="$MY_GTK_THEME" yad --list \
--title="" \
--text="<span color='#FFFFFF' font_family='Cascadia Mono' font='18' weight='bold' rise='0pt'>\n$(date)\n</span>" \
--text-align="center" \
--width=700 \
--height=210 \
--separator= \
--column="Double click on item to launch it" \
--buttons-layout="center" \
--borders=10 \
--no-headers \
--center \
--button="exit":1
}
export -f _yad

#------------------------------------------------------#
# Timezones
#------------------------------------------------------#

# Timezone commands

zoneone=$(TZ=America/Los_Angeles date)
export zoneone 
zonetwo=$(TZ=America/New_York date)
export zonetwo
zonethree=$(TZ=Australia/Sydney date)
export zonethree
zonefour=$(TZ=Asia/Tokyo date)
export zonefour
zonefive=$(TZ=Europe/London date)
export zonefive
zonesix=$(TZ=Asia/Jerusalem date)
export zonesix

# Only text output

textone="America/Los Angeles"
export textone
texttwo="America/New York"
export texttwo
textthree="Australia/Sydney"
export textthree
textfour="Japan/Tokyo"
export textfour
textfive="Europe/London"
export textfive
textsix="Asia/Jerusalem"
export textsix

# Menu

com01=$(echo -e "${zoneone}${space} ${textone}")
export com01
com02=$(echo -e "${zonetwo}${space} ${texttwo}")
export com02
com03=$(echo -e "${zonethree}${space} ${textthree}")
export com03
com04=$(echo -e "${zonefour}${space} ${textfour}")
export com04
com05=$(echo -e "${zonefive}${space} ${textfive}")
export com05
com06=$(echo -e "${zonesix}${space} ${textsix}")
export com06

#------------------------------------------------------#
# Main
#------------------------------------------------------#

choice=$(echo -e "$com01\n$com02\n$com03\n$com04\n$com05\n$com06\n" | _yad)
export choice
[ -z "$choice" ] && exit 0
echo "$choice" | wl-copy -n

#------------------------------------------------------#
# Unset variables and functions
#------------------------------------------------------#

# variables
unset zoneone
unset zonetwo
unset zonethree
unset zonefour
unset zonefive
unset zonesix

unset textone
unset texttwo
unset textthree
unset textfour
unset textfive
unset textsix

unset com01
unset com02
unset com03
unset com04
unset com05
unset com06

unset box_x_position
unset box_y_position

unset MY_GTK_THEME

# functions
unset _yad


