#!/bin/bash
# shellcheck disable=SC2090
# shellcheck disable=SC2089
# shellcheck disable=SC1078
# shellcheck disable=SC2027
# shellcheck disable=SC2034
# shellcheck disable=SC1079

#------------------------------------------------------------------#
# Check for installed dependencies
#------------------------------------------------------------------#
 
   # Check for mpv-mpris
   if [ -f "$HOME"/.config/mpv/scripts/mpris.so ]; then
     :
   else
     notify-send "Dependency mpv-mpris missing"
     killall yad
     sleep 5
     killall radioicons.sh
   fi

#--------------------------------------------------------------------#
# Script information
#--------------------------------------------------------------------#

program_command="radioicons.sh"
program_version="1.0.0"
program_updated="29-01-2024"
program_license="GPL 3.0"
contributors="John Mcgrath"
program_contact="none"

#--------------------------------------------------------------------#
# Help (type radioicons.sh -h in prompt)
#--------------------------------------------------------------------#

_help(){
cat << EOF
...

## Tracking

    * Command:      $program_command
    * Version:      $program_version
    * Updated:      $program_updated
    * License:      $program_license
    * Contact:      $program_contact
    * Contributors: $contributors

## Description

    * Plays radio streams with mpv.
    * Add radio station.
    * Delete last added file.
    * Shows currently playing.
    * Show radio stream.
    * Stop radio stream.
    * Refresh yad.
    * Track title to clipboard.
    * Mute and unmute.
    * Record radio stream.
    * Play recording.

## Dependencies

    * Xclip
    * Mpv
    * Yad
    * ffmpeg
    * Playerctl
    * Mpv-mpris (see in other information)
    * Material Icons (see in other information)

## Installation

   When I use double quotes, it is just for emphasis.

   Do not type the double quotes, in less told to do so.
   
   Do not type the "$" character, in less told to do so.

   ---

   Put script into the directory below.

   Example:

   "/home/user/.local/bin/"

   ---
   
   To make the script executable follow the example below.

   Example:

   $ chmod u+x radioicons.sh
   
   ---
   
   Now in the terminal, run this command "radioicons.sh -h".
   
   It will list out a help file. You will need to install the dependencies.
   
   ---

   ### Always update your system first.

   Example:

   $ sudo apt update

   ---

   ### You can now install the dependencies.

   Example:

   $ sudo apt install xclip mpv playerctl yad ffmpeg

   ---

   Now run the script in the terminal once, and then kill the script.
   
   Just press "control+c" or "$ killall radioicons.sh" to kill script.

   ---
   
   Download the image radio.png (see in other information)
   
   Put radio.png into the directory "/home/user/Documents/radio_dotdesk/"

   ---
   
   Download the gtk.css file (see in other information)
   
   Put the gtk.css file into /home/user/.local/share/themes/alt-dialog9/gtk-3.0/
   
   If the directories do not exist, them create them.
   
## Other information

   * Download mpv-mpris: https://github.com/hoyon/mpv-mpris/releases
   * Download material icons: https://github.com/google/material-design-icons/tree/master/font
     
EOF
}

while getopts ":h" option; do
  case $option in
    h) # display help
    _help
    exit;;
   \?) # incorrect option
    echo "Error: invalid option"
    exit;;
  esac
done

#--------------------------------------------------------------------#
# Colors for help page (pango markup)
#--------------------------------------------------------------------#

# Selected
color1="<span color='#25FF00' font_family='Monospace' font='14' weight='bold'>"
export color1
# Heading
color2="<span color='#EBFF00' font_family='Monospace' font='16' weight='bold'>"
export color2
# Normal text
color3="<span color='#FFFFFF' font_family='Monospace' font='14' weight='bold'>"
export color3
# Selected 2
color4="<span color='#F500FF' font_family='Monospace' font='14' weight='bold'>"
export color4
# Color end
end="</span>"
export end

#--------------------------------------------------------------------#
# Help page
#--------------------------------------------------------------------#

freaskque="${color2}Help Information${end}

${color3}This program plays radio stations.${end}

${color2}How to use${end}

${color3}Click on radio station icon to play stream.${end}
${color3}You must be playing a radio stream, before you press the record button.${end}

${color2}Buttons${end}

${color1}Add${end}${color3} Add name and url of radio station.${end}
${color1}Delete${end}${color3} Delete the last added radio station only.${end}
${color1}Open${end}${color3} Open the directory containing all the files.${end}
${color1}Mute${end}${color3} Mute and unmute mpv volume only.${end}
${color1}Title${end}${color3} Send track title to clipboard.${end}
${color1}Refresh${end}${color3} Restarts yad.${end}
${color1}Play${end}${color3} Plays the last recorded stream.${end}
${color1}Stop${end}${color3} Stop mpv and ffplay.${end}
${color1}Record${end}${color3} Record the currently playing stream.${end}
${color1}Exit${end}${color3} Kills yad.${end}

${color2}Button colors${end}

${color4}Record button:${end}
${color1}White${end}${color3} It is not recording.${end}
${color1}Red${end}${color3} It is recording a stream.${end}
${color1}Yellow${end}${color3} There is no file in the temp recording directory.${end}
${color4}Mute button:${end}
${color1}White${end}${color3} It is not muted${end}
${color1}Red${end}${color3} It is muted.${end}

${color2}About${end}

${color3}Program created by${end} ${color1}John Mcgrath${end}

"

export freaskque

#--------------------------------------------------------------------#
# Create files and directories
#--------------------------------------------------------------------#

   # Create documents directory
   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   #Create music directory
   if [ -d "$HOME"/Music ]; then
     :
   else
     mkdir "$HOME"/Music
   fi

   # This is where the dot desktop files are stored
   if [ -d "$HOME"/Documents/radio_dotdesk ]; then
     :
   else
     mkdir "$HOME"/Documents/radio_dotdesk
   fi

   # This is where the recorded files are stored
   if [ -d "$HOME"/Music/radio_recordings ]; then
     :
   else
     mkdir "$HOME"/Music/radio_recordings
   fi

   # This is where added streams are stored temporary
   if [ -d "$HOME"/Music/.temprslink ]; then
     :
   else
     mkdir "$HOME"/Music/.temprslink
   fi

   # This is where recorded streams are stored temporary
   if [ -d "$HOME"/Music/.temprecfile ]; then
     :
   else
     mkdir "$HOME"/Music/.temprecfile
   fi

   # When temp recording title is stored
   if [ -f "$HOME"/Documents/.record_title.txt ]; then
     :
   else
     touch "$HOME"/Documents/.record_title.txt
   fi

   # Create video recording status
   if [ -f "$HOME"/Documents/.video_recording.txt ]; then
     :
   else
     touch "$HOME"/Documents/.video_recording.txt
     echo "off" > "$HOME"/Documents/.video_recording.txt
   fi

   # Check mute status
   if [ -f "$HOME"/Documents/.radio_mute.txt ]; then
     :
   else
     touch "$HOME"/Documents/.radio_mute.txt
     echo "playerctl --player mpv volume 1%" > "$HOME"/Documents/.radio_mute.txt
   fi

#--------------------------------------------------------------------#
# Dot desktop text file
#--------------------------------------------------------------------#

entrytext="[Desktop Entry]
Version=1.0
Type=Application
Name=Station
Comment=Listen to music
Icon="$HOME"/Documents/radio_dotdesk/radio.png
Exec=/bin/bash -c 'killall mpv ; killall ffplay ; mpv stream url/station'
StartupNotify=false
Terminal=false"

export entrytext

#--------------------------------------------------------------------#
# Video recording file status
#--------------------------------------------------------------------#

     recfile=$(cat "$HOME"/Documents/.video_recording.txt)
	 export recfile
   if [[ -z "$recfile" ]]; then
     echo "off" > "$HOME"/Documents/.video_recording.txt
     killall yad
   fi

#--------------------------------------------------------------------#
# Titles for radio track and url, recording, and not playing
#--------------------------------------------------------------------#

_titleorrecord () {

   if [[ "$(pidof mpv)" ]]; then
     echo "<span color='#1DFF00' font_family='Ubuntu Mono' font='14' weight='bold'>$(playerctl --player mpv metadata -f "{{title}}" | \
     cut -c 1-70 | tr "&" "+")</span>"
     echo "<span color='#FFFFFF' font_family='Ubuntu Mono' font='14' weight='bold'>$(playerctl --player mpv metadata -f "{{xesam:url}}" | \
     cut -c 1-70 | tr "&" "+")</span>"
   elif [[ "$(pidof ffplay)" ]]; then
     file_title=$(cat "$HOME"/Documents/.recorded_title.txt)
     export file_title
     echo "<span color='#FF0011' font_family='Ubuntu Mono' font='14' weight='bold'>$(echo "Playing recording: $file_title" | \
     cut -c 1-70 | tr "&" "+")</span>"
   else
     echo "<span color='#FFFFFF' font_family='Ubuntu Mono' font='14' weight='bold'>Not playing</span>"
   fi

}
export -f _titleorrecord

#--------------------------------------------------------------------#
# Recording radio streams with mpv
#--------------------------------------------------------------------#

_recordradiompv () {

   vidfile=$(cat "$HOME"/Documents/.video_recording.txt)
   export vidfile
   if [[ -z "$(ls -A "$HOME"/Music/.temprecfile)" ]]; then
     killall yad
     echo "on" > "$HOME"/Documents/.video_recording.txt
     rm -f "$HOME"/Music/.temprecfile/*.*
     radiotrack=$(playerctl --player mpv metadata -f "{{xesam:url}}")
     export radiotrack
     killall mpv
     killall ffplay
     playerctl --player mpv metadata -f "{{title}}" | tee "$HOME"/Documents/.recorded_title.txt
     mpv --stream-record="$HOME/Music/.temprecfile/$(date +'%d-%m-%Y-%H%M%S').mp4" "$radiotrack"
     cp "$HOME"/Music/.temprecfile/*.* "$HOME"/Music/radio_recordings/
   elif [[ "$vidfile" == 'off' && "$(pidof mpv)" ]]; then
     killall yad
     echo "on" > "$HOME"/Documents/.video_recording.txt
     rm -f "$HOME"/Music/.temprecfile/*.*
     radiotrack=$(playerctl --player mpv metadata -f "{{xesam:url}}")
     export radiotrack
     killall mpv
     killall ffplay
     playerctl --player mpv metadata -f "{{title}}" | tee "$HOME"/Documents/.recorded_title.txt
     mpv --stream-record="$HOME/Music/.temprecfile/$(date +'%d-%m-%Y-%H%M%S').mp4" "$radiotrack"
     cp "$HOME"/Music/.temprecfile/*.* "$HOME"/Music/radio_recordings/
   elif [[ "$vidfile" == 'on' ]]; then
     killall yad
     echo "off" > "$HOME"/Documents/.video_recording.txt
     killall mpv
     killall ffplay
   else
     notify-send "Not playing radio"
   fi

}
export -f _recordradiompv

#--------------------------------------------------------------------#
# Check state of recording
#--------------------------------------------------------------------#

_check_recording(){

   if [[ -z "$(ls -A "$HOME"/Music/.temprecfile)" ]]; then
     echo "<span color='#FAFF00' font_family='Material Icons' font='20'></span>"
   elif [[ "$recfile" == 'off' ]]; then
	 echo "<span color='#FFFFFF' font_family='Material Icons' font='20'></span>"
   elif [[ "$recfile" == 'on' ]]; then
	 echo "<span color='#FF0011' font_family='Material Icons' font='20'></span>"
   else
    echo "<span color='#FF0011' font_family='Material Icons' font='20'></span>"
   fi

}
export -f _check_recording

#--------------------------------------------------------------------#
# Open container for dot desktop files
#--------------------------------------------------------------------#

_containopenradio () {
GTK_THEME="Yaru"
killall yad
checkveros=$(cat /etc/os-release)
export checkveros

if [[ "$checkveros" == *"MANJARO"* ]]; then
   exo-open "$HOME"/Documents/radio_dotdesk/
else
   xdg-open "$HOME"/Documents/radio_dotdesk/
fi

}
export -f _containopenradio

#--------------------------------------------------------------------#
# Add radio station
#--------------------------------------------------------------------#

_radio_main() {

killall yad

add_radio=$(GTK_THEME="Adwaita-dark" yad --form --width=600 --height=200 --title="Music app" \
--borders="20" \
--separator="#" \
--field="Station ":CE "" \
--field="URL":CE "" \
--button="Add":0 \
--button="Exit":1
)
export add_radio

radio_name=$(echo "$add_radio" | awk -F '#' '{print $1}')
export radio_name
radio_station=$(echo "$add_radio" | awk -F '#' '{print $2}')
export radio_station

killall yad
radio_launch="${radio_name}${radio_station}"
export radio_launch

   if [[ -z "$radio_launch" ]]; then
     :
   else
     echo "$radio_name" | tee "$HOME"/Documents/.music_app/.name.txt
     echo "$radio_station" | tee "$HOME"/Documents/.music_app/.station.txt
     _addradiostation
   fi

}
export -f _radio_main

#--------------------------------------------------------------------#
# Creates dot desktop file
#--------------------------------------------------------------------#

_addradiostation() {

killall yad
rm -f "$HOME"/Music/.temprslink/*.*
echo "$entrytext" >> "$HOME/Music/.temprslink/$(date '+%d-%m-%Y-%H-%M-%S')".desktop
find "$HOME"/Music/.temprslink/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | \
xargs sed -i "4s/^.*$/Name=$(cat "$HOME"/Documents/.music_app/.name.txt)/"
find "$HOME"/Music/.temprslink/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | \
xargs sed -i "7s|^.*$|Exec=/bin/bash -c 'killall mpv ; killall ffplay ; mpv $(cat "$HOME"/Documents/.music_app/.station.txt)'|"
cp ~/Music/.temprslink/*.* ~/Documents/radio_dotdesk/
radioicons.sh

}
export -f _addradiostation

#--------------------------------------------------------------------#
# Delete last added radio stream
#--------------------------------------------------------------------#

_dellastaddlink () {

   if [[ -z "$(ls -A "$HOME"/Music/.temprslink)" ]]; then
     notify-send "No file"
   else
     killall yad
     myfilename=$(find "$HOME"/Music/.temprslink/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | cut -d '/' -f6-)
     export myfilename
     rm -f "$HOME"/Documents/radio_dotdesk/"$myfilename"
     rm -f ~/Music/.temprslink/*.*
     radioicons.sh
   fi
   
}
export -f _dellastaddlink

#--------------------------------------------------------------------#
# Play recordings
#--------------------------------------------------------------------#

_playrecfg() {

   if [[ -z "$(ls -A "$HOME"/Music/.temprecfile)" ]]; then
     notify-send "No recording"
   else
     playectemp=$(find "$HOME"/Music/.temprecfile/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     export playectemp
     killall yad
     killall mpv
     killall ffplay
     echo "off" > "$HOME"/Documents/.video_recording.txt
     ffplay "$playectemp" -autoexit -nodisp
   fi
   
}
export -f _playrecfg

#--------------------------------------------------------------------#
# Mute sound
#--------------------------------------------------------------------#

_mute() {

     vfile=$(cat "$HOME"/Documents/.radio_mute.txt)
	 export vfile
   if [[ "$vfile" == 'playerctl --player mpv volume 1%' && "$(pidof mpv)" ]]; then
     killall yad
	 echo "playerctl --player mpv volume 0%" | tee "$HOME"/Documents/.radio_mute.txt
	 playerctl --player mpv volume 0%
   elif [[ "$vfile" == 'playerctl --player mpv volume 0%' && "$(pidof mpv)" ]]; then
	 killall yad
	 echo "playerctl --player mpv volume 1%" | tee "$HOME"/Documents/.radio_mute.txt
	 playerctl --player mpv volume 1%
   else
     notify-send "Not playing radio"
   fi

}
export -f _mute

#--------------------------------------------------------------------#
# Check status of muted sound
#--------------------------------------------------------------------#

_check_mute() {

	 vlfile=$(cat "$HOME"/Documents/.radio_mute.txt)
     export vlfile
   if [[ -z "$vlfile" ]]; then
	 echo "playerctl --player mpv volume 1%" | tee "$HOME"/Documents/.radio_mute.txt
   elif [[ "$vlfile" == 'playerctl --player mpv volume 1%' ]]; then
	 echo "<span color='#FFFFFF' font_family='Material Icons' font='20' weight='bold'></span>"
   elif [[ "$vlfile" == 'playerctl --player mpv volume 0%' ]]; then
	 echo "<span color='#FF002E' font_family='Material Icons' font='20' weight='bold'></span>"
   else
	 :
   fi

}
export -f _check_mute

#--------------------------------------------------------------------#
# Send song title to clipboard
#--------------------------------------------------------------------#

_songtitclip() {

playerctl --player mpv metadata -f "{{title}}" | xclip -selection clipboard

}
export -f _songtitclip

#--------------------------------------------------------------------#
# Stop mpv and ffplay
#--------------------------------------------------------------------#

_stopmpvffplay() {

   if [[ "$(pidof mpv)" ]]; then
     killall yad
     killall mpv
     killall ffplay
     echo "off" > "$HOME"/Documents/.video_recording.txt
   elif [[ "$(pidof ffplay)" ]]; then
     killall yad
     killall mpv
     killall ffplay
     echo "off" > "$HOME"/Documents/.video_recording.txt
   else
     killall yad
     killall mpv
     killall ffplay
     echo "off" > "$HOME"/Documents/.video_recording.txt
   fi

}
export -f _stopmpvffplay

#--------------------------------------------------------------------#
# Yad main
#--------------------------------------------------------------------#
_main() {

GTK_THEME="Yaru-dark" yad --icons --item-width=75 --borders="27" --buttons-layout="spread" \
--center --width=1200 --height=650 \
--image-on-top --dialog-sep --text-align="center" \
--read-dir="$HOME"/Documents/radio_dotdesk/ \
--single-click \
--text="<span color='#ffffff' font_family='DejaVu Sans Mono' font='20' weight='bold'>$(_titleorrecord)</span>" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='20'></span>:/bin/bash -c '_radio_main'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='20'></span>:/bin/bash -c '_dellastaddlink'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='20'></span>:/bin/bash -c '_containopenradio'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='20'>$(_check_mute)</span>:/bin/bash -c '_mute'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='20'></span>:/bin/bash -c '_songtitclip'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='20'></span>:/bin/bash -c 'killall yad ; radioicons.sh'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='23'></span>:/bin/bash -c '_playrecfg'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='23'></span>:/bin/bash -c '_stopmpvffplay'" \
--button="$(_check_recording):/bin/bash -c '_recordradiompv'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='20'></span>":1

}
export -f _main
_main

#--------------------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------------------#

# functions
unset _recordradiompv
unset _titleorrecord
unset _containopenradio
unset _addradiostation
unset _dellastaddlink
unset _playrecfg
unset _stopmpvffplay
unset _songtitclip
unset _check_recording
unset _help
unset _radio_main
unset _check_mute
unset _check_mute
unset _main

# variables
unset radiotrack
unset entrytext
unset freaskque
unset myfilename
unset playectemp
unset record_file
unset file_title
unset recfile
unset vidfile
unset color1
unset color2
unset color3
unset color3
unset end
unset add_radio
unset radio_station
unset radio_name
unset radio_launch
unset vlfile
unset vfile
