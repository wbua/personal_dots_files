#!/bin/bash

# Program command: yad_keyswords_launcher.sh
# Description: Runs your programs and uses keywords.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 22-08-2024
# Program_license: GPL 3.0
# Dependencies: yad, yt-dlp, amixer, zenity, dictd, dict-gcide, aspell, wl-clipboard, qrencode,
# festival, libreoffice, amixer, grim, slurp, ffmpeg, swaylock, wf-recorder, imagemagick, swaymsg, 
# jq, zbar-tools, wtype, playerctl, mpv-mpris, gnome-terminal, thunar, dunst, pactl, pacmd, arecord,
# audacious, webp, qalc

# This script is made to work in wayland only not xorg.
# Some of these keywords will only work in sway.

# Before you use the terminal here function execute this command below first.
# $ xfconf-query -c thunar -p /misc-full-path-in-window-title -t bool -s true --create

#------------------------------------------------------#
# Sourcing files
#------------------------------------------------------#

# Functions
# shellcheck source=/dev/null
source "$HOME"/bin/yad_keywords_functions.sh

# Config
# shellcheck source=/dev/null
source "$HOME"/bin/yad_keywords_config.sh

#------------------------------------------------------#
# Error checking
#------------------------------------------------------#

set -e

#------------------------------------------------------#
# Checks if you are running a wayland session
#------------------------------------------------------#

   if [[ "$WAYLAND_DISPLAY" == "" ]]; then
     yad --error \
     --width 500 \
     --title "Wayland session not found!" \
     --text "Cannot run color picker, wayland only tool!"
     exit 1
   else
     :
   fi

#------------------------------------------------------#
# Create files and directories
#------------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/.temqrcodes ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes
   fi

   if [ "$QR_CODE_DIR" == "disable" ]; then
     :
   elif [ -d "$QR_CODE_DIR" ]; then
     :
   else
     notify-send "QR Code directory does not exist!" && exit 1
   fi

   if [ -d "$HOME"/Documents/.pdftemp ]; then
     :
   else
     mkdir "$HOME"/Documents/.pdftemp
   fi

   if [ "$PDF_DIR" == "disable" ]; then
     :
   elif [ -d "$PDF_DIR" ]; then
     :
   else
     notify-send "PDF directory does not exist!" && exit 1
   fi

   if [ -d "$HOME"/Videos/.yadvidrec ]; then
     :
   else
     mkdir "$HOME"/Videos/.yadvidrec
   fi

   if [ -d "$SCREENCAST_DIR" ]; then
     :
   else
     notify-send "Recordings directory does not exist!" && exit 1
   fi

   if [ -d "$HOME"/Pictures/.tempfullscrns ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempfullscrns
   fi

   if [ -d "$SCREENSHOTS_DIR" ]; then
     :
   else
     notify-send "Screenshot directory does not exist!" && exit 1
   fi

   if [ -d "$HOME"/Pictures/.ocr ]; then
     :
   else
     mkdir "$HOME"/Pictures/.ocr
   fi

   if [ -d "$HOME"/Documents/.zips ]; then
     :
   else
     mkdir "$HOME"/Documents/.zips
   fi

   if [ "$ZIPS_DIR" == "disable" ]; then
     :
   elif [ -d "$ZIPS_DIR" ]; then
     :
   else
     notify-send "Zip files directory does not exist!" && exit 1
   fi

   if [ -d "$HOME"/Music/.yadaudiorec ]; then
     :
   else
     mkdir "$HOME"/Music/.yadaudiorec
   fi

   if [ -d "$AUDIO_DIR" ]; then
     :
   else
     notify-send "Audio directory does not exist!" && exit 1
   fi

   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ -d "$HOME"/Pictures/.image_notifications ]; then
     :
   else
     mkdir "$HOME"/Pictures/.image_notifications
   fi

   if [ -d "$HOME"/Videos/.temp_record_video ]; then
     :
   else
     mkdir "$HOME"/Videos/.temp_record_video
   fi

   if [ -f "$EMOJI_TEXT" ]; then
     :
   else
     notify-send "Emoji text file does not exist!" && exit 1
   fi

   if [ -f "$HOME"/Documents/.bc_results.txt ]; then
     :
   else
     touch "$HOME"/Documents/.bc_results.txt
   fi

   if [ "$QNOTES_FILE" == "disable" ]; then
     :
   elif [ -f "$QNOTES_FILE" ]; then
     :
   else
     notify-send "Quick notes file does not exist!" && exit 1
   fi

   if [ -f "$HOME"/Documents/.zipfile.txt ]; then
     :
   else
     touch "$HOME"/Documents/.zipfile.txt
   fi

   if [ -f "$HOME"/Documents/last_keyword.txt ]; then
     :
   else
     touch "$HOME"/Documents/last_keyword.txt
   fi

   if [ "$RADIO_LINKS" == "disable" ]; then
     :
   elif [ -f "$RADIO_LINKS" ]; then
     :
   else
     notify-send "Radio links file does not exist!" && exit 1
   fi
   
#------------------------------------------------------#
# Colors
#------------------------------------------------------#

# Keyword description
COLOR1=$(printf "<span color='#FFFFFF' font_family='Cascadia Mono' font='15' rise='0pt'>")
export COLOR1
# Keywords
COLOR2=$(printf "<span color='#4AFF00' font_family='Cascadia Mono' font='15' rise='0pt'>")
export COLOR2
# Extra info
COLOR3=$(printf "<span color='#FEFF00' font_family='Cascadia Mono' font='15' rise='0pt'>")
export COLOR3
# Color end
END=$(printf "</span>")
export END

#------------------------------------------------------#
# Text file containing your programs
#------------------------------------------------------#

MY_APPS_FILE="$HOME/Documents/last_keyword.txt"
export MY_APPS_FILE

#------------------------------------------------------#
# Main
#------------------------------------------------------#
 
# Gui program 
selected=$(_yad) 
export selected

# Keyword
BANG=$(echo "$selected" | awk '{print $1}')
export BANG

# Content
LAUNCHER_CONTENT=$(echo "$selected" | cut -d' ' -f2-)
export LAUNCHER_CONTENT
 
case "$BANG" in

   # Youtube search
    "$KEYWORD01")
     youtube_search
     ;;

   # Google translate  
    "$KEYWORD02")
     google_trans_fr
     ;;

   # Find local files
    "$KEYWORD03")
     find_files
     ;;

   # Find local files (history)
    "$KEYWORD04")
     find_files_history
     ;;

   # QR Codes
    "$KEYWORD05")
     qr_codes
     ;;

   # Calculator
    "$KEYWORD06")
     my_calculator
     ;;

   # Calculator history
    "$KEYWORD07")
     calculator_history
     ;;

   # Dictionary
    "$KEYWORD08")
     my_dictionary
     ;;

   # Shutdown
    "$KEYWORD09")
     yadshutdown
     ;;

   # Restart
    "$KEYWORD10")
     yadreboot
     ;;

   # Mute volume toggle
    "$KEYWORD11")
     mute_volume
     ;;

   # Set the volume
    "$KEYWORD12")
     set_volume
     ;;

   # Text to speech (speak)
    "$KEYWORD13")
     tts_speak
     ;;

   # Text to speech (file)
    "$KEYWORD14")
     tts_file
     ;;

   # Text to speech (kill)
    "$KEYWORD15")
     tts_kill
     ;;

   # Kill processes with killall
    "$KEYWORD16")
     kill_app
     ;;

   # Kill processes with pkill
    "$KEYWORD17")
     pkill_app
     ;;

   # Spell checker
    "$KEYWORD18")
     spell_checker
     ;;

   # Password generator
    "$KEYWORD19")
     pass_gen
     ;;

   # QR Code image notification
    "$KEYWORD20")
     qr_code_notification
     ;;
     
   # Google search
    "$KEYWORD21")
     google_search
     ;;

   # Create PDF
    "$KEYWORD22")
     create_pdf
     ;;

   # PDF open file
    "$KEYWORD23")
     pdf_open
     ;;

   # PDF print
    "$KEYWORD24")
     pdf_print
     ;;

   # Record screen (screencast)
    "$KEYWORD25")
     record_screen
     ;;

   # Stop screen recording (screencast)
    "$KEYWORD26")
     stop_rec
     ;;

   # Play screen recording (screencast)
    "$KEYWORD27")
     play_rec
     ;;

   # Take fullscreen screenshot
    "$KEYWORD28")
     full_shot
     ;;

   # Select screenshot
    "$KEYWORD29")
     select_shot
     ;;

   # Delay screenshot
    "$KEYWORD30")
     delay_shot
     ;;

   # Open last screenshot
    "$KEYWORD31")
     open_shot
     ;;

   # Lock machine
    "$KEYWORD32")
     yadlock
     ;;

   # Exec terminal commands
    "$KEYWORD33")
     cli_commands
     ;;

   # Resize image
    "$KEYWORD34")
     resize_image
     ;;

   # Emoji
    "$KEYWORD35")
     emoji_list
     ;;

   # One line note
    "$KEYWORD36")
     one_line_note
     ;;

   # One line note history
    "$KEYWORD37")
     one_line_note_history
     ;;

   # Decode qr code 
    "$KEYWORD38")
     decode_qr_code
     ;;

   # Keywords help page
    "$KEYWORD39")
     /bin/bash -c show_keywords
     ;;

   # Color Picker
    "$KEYWORD40")
     color_picker
     ;;

   # RGB to HEX
    "$KEYWORD41")
     call_rgb_to_hex
     ;;

   # HEX to RGB
    "$KEYWORD42")
     hex_to_rgb
     ;;

   # Brave command palette
    "$KEYWORD43")
     check_brave
     ;;

   # Control music players with playerctl
    "$KEYWORD44")
     music_player
     ;;

   # Volume status
    "$KEYWORD45")
     volume_status
     ;;

   # Open terminal here in thunar
    "$KEYWORD46")
     open_ter_here
     ;;

   # Change uppercase text to lowercase
    "$KEYWORD47")
     upper_to_lower
     ;;

   # Timezone los angeles
    "$KEYWORD48")
     los_angeles
     ;;

   # Timezone new york
    "$KEYWORD49")
     new_york
     ;;

   # Timezone in Sydney
    "$KEYWORD50")
     sydney
     ;;

   # All timezones
    "$KEYWORD51")
     main_timezones
     ;;

   # Notifications
    "$KEYWORD52")
     control_notifications
     ;;

   # Microphone
    "$KEYWORD53")
     my_microphone
     ;;

   # Zip files
    "$KEYWORD54")
     zip_files
     ;;

   # Plays videos in mpv via http link
    "$KEYWORD55")
     mpv_play
     ;;

   # Last modified files
    "$KEYWORD56")
     last_modified_file
     ;;

   # Record from microphone
    "$KEYWORD57")
     record_mic_audio
     ;;

   # Radio
    "$KEYWORD58")
     my_radio
     ;;

   # Duckduckgo web search
    "$KEYWORD59")
     duckduckgo
     ;;

   # Reddit web search
    "$KEYWORD60")
     reddit
     ;;

   # Download video
    "$KEYWORD61")
     download_video
     ;;

   # Wikipedia web search
    "$KEYWORD62")
     wikipedia
     ;;

   # Empty trash
    "$KEYWORD63")
     trash_clear
     ;;

   # Open keywords config
    "$KEYWORD64")
     open_config
     ;;

   # Audacious player
    "$KEYWORD65")
     audacious_player
     ;;

   # Audacious player playlist
    "$KEYWORD66")
     audacious_player_playlist
     ;;  

   # Emoji search
    "$KEYWORD67")
     emoji_search
     ;;
         
   *)
    # Move to already open app
    if [[ "$(pidof "$BANG")" || "$(pgrep -f "$BANG" > /dev/null)" ]]; then
        app_running
    # Open app
    else
        new_instance
    fi 
     ;;
     
esac

#------------------------------------------------------#
# Unset variables and functions
#------------------------------------------------------#

unset _yad
unset BANG
unset LAUNCHER_CONTENT
unset selected
unset google_search
unset download_video
unset youtube_search
unset find_files
unset file_results
unset open_file
unset yadreboot
unset yadshutdown
unset mute_volume
unset open_file_manager
unset FILE_MANAGER
unset MYBROWSER
unset my_dictionary
unset spell_checker
unset spellcheck_results
unset qr_codes
unset qr_code_notification
unset my_calculator
unset my_calculator_copy
unset calculator_clipboard
unset calculator_history
unset tts_speak
unset tts_file
unset tts_kill
unset kill_app
unset pkill_app
unset set_volume
unset create_pdf
unset pdf_open
unset pdf_print
unset check_sound
unset full_shot
unset open_shot
unset yadlock
unset select_shot
unset delay_shot
unset DELAY_TIME
unset record_screen
unset stop_rec
unset play_rec
unset cli_commands
unset MYTERMINAL
unset pass_gen
unset resize_image
unset resize_image
unset keyword_help
unset COLOR1
unset COLOR2
unset COLOR3
unset END
unset show_keywords
unset app_running
unset new_instance
unset emoji_list
unset one_line_note
unset one_line_note_copy
unset one_line_note_select
unset one_line_note_history
unset decode_qr_code
unset decode_qr_code_select
unset decode_qr_code_copy
unset open_apps_text_file
unset color_picker
unset rgb_to_hex
unset call_rgb_to_hex
unset hex_to_rgb
unset brave_palette
unset music_player
unset volume_status
unset open_ter_here
unset upper_to_lower
unset los_angeles
unset new_york
unset sydney
unset check_brave
unset main_timezones
unset yad_timezone
unset control_notifications
unset my_microphone
unset zip_files
unset mpv_play
unset last_modified_file
unset mod_files_select
unset mod_files_open
unset temp_mod_file
unset open_mod_file_manager
unset record_mic_audio
unset lasted_played
unset my_radio
unset play_mpv
unset RADIO_LINKS
unset EMOJI_TEXT
unset duckduckgo
unset wikipedia
unset reddit
unset MY_PRINTER
unset SCREENSHOTS_DIR
unset PDF_DIR
unset SCREENCAST_DIR
unset QR_CODE_DIR
unset OTHER_TERMINAL
unset ZIPS_DIR
unset AUDIO_DIR
unset GTK_THEMES
unset PDF_VIEWER
unset IMAGE_VIEWER
unset VIDEO_PLAYER
unset TEXT_EDITOR
unset AUDIO_PLAYER
unset KEYWORD01
unset KEYWORD02
unset KEYWORD03
unset KEYWORD04
unset KEYWORD05
unset KEYWORD06
unset KEYWORD07
unset KEYWORD08
unset KEYWORD09
unset KEYWORD10
unset KEYWORD11
unset KEYWORD12
unset KEYWORD13
unset KEYWORD14
unset KEYWORD15
unset KEYWORD16
unset KEYWORD17
unset KEYWORD18
unset KEYWORD19
unset KEYWORD20
unset KEYWORD21
unset KEYWORD22
unset KEYWORD23
unset KEYWORD24
unset KEYWORD25
unset KEYWORD26
unset KEYWORD28
unset KEYWORD29
unset KEYWORD30
unset KEYWORD31
unset KEYWORD32
unset KEYWORD33
unset KEYWORD34
unset KEYWORD35
unset KEYWORD36
unset KEYWORD37
unset KEYWORD38
unset KEYWORD39
unset KEYWORD40
unset KEYWORD41
unset KEYWORD42
unset KEYWORD43
unset KEYWORD44
unset KEYWORD45
unset KEYWORD46
unset KEYWORD47
unset KEYWORD48
unset KEYWORD49
unset KEYWORD50
unset KEYWORD51
unset KEYWORD52
unset KEYWORD53
unset KEYWORD54
unset KEYWORD55
unset KEYWORD56
unset KEYWORD57
unset KEYWORD58
unset KEYWORD59
unset KEYWORD60
unset KEYWORD61
unset KEYWORD62
unset KEYWORD63
unset KEYWORD64
unset KEYWORD65
unset KEYWORD66
unset KEYWORD67
unset trash_clear
unset open_config
unset audacious_player
unset audacious_player_playlist
unset find_image_notification
unset stop_players
unset dir_destination
unset copy_dir_main
unset copy_selected_file
unset MY_APPS_FILE
unset dir_destination_move
unset move_dir_main
unset move_selected_file
unset emoji_search
unset preview_audio
unset PREVIEW_AUDIO_TIME
unset preview_video
unset PREVIEW_VIDEO_TIME
unset file_metadata
unset mod_image_gimp
unset IMAGE_EDITOR
unset QNOTES_FILE
unset qr_code_artist
unset MY_COLOR_ALERT
unset MY_COLOR_ICON
unset MY_COLOR_BG
