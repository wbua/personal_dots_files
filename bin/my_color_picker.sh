#!/bin/bash

# Program command: my_color_picker.sh
# Description: Color picker for use in wayland.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 20-04-2024
# Program_license: GPL 3.0
# Dependencies: grim, slurp, imagemagick, yad, wl-clipboard, JetBrains Mono Nerd Font, material icons

#----------------------------------------------------#
# Error checking
#----------------------------------------------------#

set -e

#----------------------------------------------------#
# User preferences
#----------------------------------------------------#

# Image size
image_size='200x200'

#----------------------------------------------------#
# Check if you are running wayland
#----------------------------------------------------#

   if [[ "$(echo $WAYLAND_DISPLAY)" == "" ]]; then
     yad --error \
     --width 500 \
     --title "Wayland session not found!" \
     --text "Cannot run color picker, wayland only tool!"
     exit 1
   else
     :
   fi

#----------------------------------------------------#
# Create files and directories
#----------------------------------------------------#

# Create pictures directory
   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

# Creates rofi colors directory
   if [ -d "$HOME"/Pictures/.rofi_colors ]; then
     :
   else
     mkdir "$HOME"/Pictures/.rofi_colors
   fi

#----------------------------------------------------#
# Colors (pango markup)
#----------------------------------------------------#

# Close button
color01="<span color='#FF0040' font_family='Material Icons' font='20' weight='bold' rise='0pt'>"
# Copy hex code to clipboard
color02="<span color='#E3E3E3' font_family='JetBrains Mono Nerd Font' font='15' weight='bold' rise='0pt'>"
# Icons
color03="<span color='#E3E3E3' font_family='Material Icons' font='20' weight='bold' rise='-3pt'>"
# Color end
end="</span>"

#----------------------------------------------------#   
# Remove file in temp directory
#----------------------------------------------------#

rm -f ~/Pictures/.rofi_colors/*

#----------------------------------------------------#   
# Takes a 1 pixel select screenshot
#----------------------------------------------------#

grim -g "$(slurp -p)" -t ppm - | convert - -format '%[pixel:p{0,0}]' txt:- | \
tail -n 1 | awk '{print $3}' | tee ~/Pictures/.temp_hex_code.txt

#----------------------------------------------------#
# Hex code text file
#----------------------------------------------------#

code_text_file=$(cat ~/Pictures/.temp_hex_code.txt)

#----------------------------------------------------#
# Copy hex code to clipboard
#----------------------------------------------------#

clip_hex() {

cat ~/Pictures/.temp_hex_code.txt | wl-copy -n
	
}
export -f clip_hex

#----------------------------------------------------#
# Copy rgb code to clipboard
#----------------------------------------------------#

clip_rgb() {

image=$(cat ~/Pictures/.temp_hex_code.txt | sed 's|#||')
[ -z "$image" ] && exit 0
hex="$image"
printf "%d %d %d\n" 0x"${hex:0:2}" 0x"${hex:2:2}" 0x"${hex:4:2}" | wl-copy -n
	
}
export -f clip_rgb

#----------------------------------------------------#
# Yad settings
#----------------------------------------------------#

_yad() {
 
GTK_THEME="alt-dialog9" yad --form \
--width=500 \
--height=370 \
--title="Color picker" \
--text-align="center" \
--borders=20 \
--image-on-top \
--image="$(find ~/Pictures/.rofi_colors/ -type f -print0 | xargs -0 ls -tr | tail -n 1)" \
--text="$(cat ~/Pictures/.temp_hex_code.txt)" \
--button="${color02}HEX${end}${color03} ${end}:/bin/bash -c 'clip_hex'" \
--button="${color02}RGB${end}${color03} ${end}:/bin/bash -c 'clip_rgb'" \
--button="${color01}${end}":1 

}
export -f _yad

#----------------------------------------------------#
# Creates image using hex code
#----------------------------------------------------#

   if [[ "$(cat ~/Pictures/.temp_hex_code.txt)" == *"#"* ]]; then
     convert -size "$image_size" xc:"$code_text_file" "$HOME/Pictures/.rofi_colors/$code_text_file".png
     _yad
   elif [[ ! "$(cat ~/Pictures/.temp_hex_code.txt)" == *"#"* ]]; then
     sed -i -e 's/^/#/' ~/Pictures/.temp_hex_code.txt
     convert -size "$image_size" xc:"$code_text_file" "$HOME/Pictures/.rofi_colors/$code_text_file".png
     _yad
   else
     notify-send "Error occurred!" && exit 1 
   fi	

#----------------------------------------------------#
# Unset variables and functions
#----------------------------------------------------#

# Variables
unset code_text_file

# Functions
unset clip_hex
unset _yad
unset clip_rgb
