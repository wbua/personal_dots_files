#!/bin/bash

set -e

# Master config file
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# File manager
MY_FILE_MANAGER="$MAIN_FILE_MANAGER"
export MY_FILE_MANAGER

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Screenshot directory
directory="$HOME/Pictures/screenshots"
# Thumbnail directory
thumbnail_dir="$HOME/.cache/thumbnails/large"
# Output text file
output_file="$HOME/Documents/scrsht_thumbs.txt"
# Temporary file
temp_file=$(mktemp)

# Checks if thumbnail directory exists
if [ ! -d "$thumbnail_dir" ]; then
  mkdir -p "$thumbnail_dir"
else
  echo "Thumbnail directory already exists: $thumbnail_dir"
fi

# Clear the output file if it exists 
truncate -s 0 "$temp_file" || exit 1

# Uses existing or creates thumbnails.
for file in "$directory"/*; do
  if [ -f "$file" ]; then
    if [ ! -r "$file" ] || [ ! -s "$file" ]; then
      echo "Skipping unreadable or empty file: $file"
      continue
    fi

    echo "Processing file: $file"
    base_name=$(basename "$file")
    echo "Base name: $base_name"
    thumbnail_path=$(gio info -a 'thumbnail::path' "$file" 2>/dev/null | \
    grep 'thumbnail::path' | awk '{print $2}')
    
    if [ -z "$thumbnail_path" ]; then
      thumbnail_path="$thumbnail_dir/${base_name%.*}.png"
      echo "Creating thumbnail for $file at $thumbnail_path"
      ffmpegthumbnailer -i "$file" -o "$thumbnail_path" -s 256
    else
      echo "Thumbnail already exists for $file: $thumbnail_path"
    fi
    
    echo "\"$thumbnail_path\" \"$file\"" >> "$temp_file"
  else
    echo "Skipping non-file entry: $file"
  fi
done

# Remove duplicates and save to the output file
sort -u "$temp_file" > "$output_file" || exit 1

# Create array
declare -a screen_list="($(< "$output_file"))"

# Copy png file to clipboard
copy_png() {

killall yad

copy_choice=$(cat ~/Documents/.srnshotfiles_temp.txt)

wl-copy --type image/png < "$(find ~/Documents/.srnshotfiles_temp.txt | xargs cat)"

}
export -f copy_png

# Go to file
go_to_file() {

killall yad

file_choice=$(cat ~/Documents/.srnshotfiles_temp.txt)

"$MY_FILE_MANAGER" "$file_choice" 
	
}
export -f go_to_file

# Open screenshot
open_image() {

killall yad

open_choice=$(cat ~/Documents/.srnshotfiles_temp.txt)

xdg-open "$open_choice"
	
}
export -f open_image

# Yad dialog
printf '%s\n' "${screen_list[@]}" | GTK_THEME="$MY_GTK_THEME" yad --list \
 --title="Screenshot manager" \
 --width=950 \
 --height=600 \
 --borders=10 \
 --regex-search \
 --search-column=2 \
 --column="Image:IMG" \
 --column="File" \
 --dclick-action='/bin/bash -c "echo "$@" > "$HOME"/Documents/.srnshotfiles_temp.txt"' \
 --button="_Open:/bin/bash -c 'open_image'" \
 --button="_Go to file:/bin/bash -c 'go_to_file'" \
 --button="_Copy:/bin/bash -c 'copy_png'" \
 --button="_Exit":1

rm "$temp_file" || exit 1

# Unset variables and functions
unset open_image
unset MY_FILE_MANAGER
unset go_to_file
unset copy_png
unset MY_GTK_THEME
