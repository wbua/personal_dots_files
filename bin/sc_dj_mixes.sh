#!/bin/bash

# Program command: sc_dj_mixes.sh
# Description: Streams Soundcloud url's in mpv.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 21-05-2024
# Program_license: GPL 3.0
# Dependencies: yad, mpv, playerctl, mpv-mpris

#----------------------------------------------------------------#
# Error checking
#----------------------------------------------------------------#

set -e

#----------------------------------------------------------------#
# User preferences
#----------------------------------------------------------------#

# Gtk theme
MY_GTK_THEMES="alt-dialog22"
export MY_GTK_THEMES

# Browser
MY_BROWSER="brave"
export MY_BROWSER

# Yad dialog width
YAD_WIDTH="600"
export YAD_WIDTH

# Yad dialog height
YAD_HEIGHT="600"
export YAD_HEIGHT

#----------------------------------------------------------------#
# Create file and directories
#----------------------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -f "$HOME"/Documents/sc_dj_mixes.txt ]; then
     :
   else
     touch "$HOME"/Documents/sc_dj_mixes.txt
   fi
    
#----------------------------------------------------------------#
# Colors
#----------------------------------------------------------------#

# Greeting
COLOR1=$(printf "<span color='#FFFFFF' font_family='Cascadia Mono' font='18' rise='0pt'>")
export COLOR1
# User
COLOR2=$(printf "<span color='#41FF00' font_family='Cascadia Mono' font='18' rise='0pt'>")
export COLOR2
# Misc
COLOR3=$(printf "<span color='#FEFF00' font_family='Cascadia Mono' font='18' rise='0pt'>")
export COLOR3
# Icons
COLOR4=$(printf "<span color='#FFFFFF' font_family='Material Icons' font='28' rise='0pt'>")
export COLOR4
# Color end
END=$(printf "</span>")
export END

#----------------------------------------------------------------#
# Remove whitespace
#----------------------------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/sc_dj_mixes.txt

#----------------------------------------------------------------#
# Remove duplicate lines
#----------------------------------------------------------------#

gawk -i inplace '!seen[$0]++' "$HOME"/Documents/sc_dj_mixes.txt

#----------------------------------------------------------------#
# Greeting
#----------------------------------------------------------------#

_greeting() {

hour=$(date +%H)
export hour

   if [ "$hour" -ge 12 ] && [ "$hour" -lt 19 ]; then
     echo "${COLOR1}Good afternoon${END}" "${COLOR2}$USER${END}"
   fi

   if [ "$hour" -ge 00 ] && [ "$hour" -lt 12 ]; then
     echo "${COLOR1}Good morning${END}" "${COLOR2}$USER${END}"
   fi

   if [ "$hour" -ge 19 ] && [ "$hour" -lt 24 ]; then
     echo "${COLOR1}Good evening${END}" "${COLOR2}$USER${END}"
   fi	
	
}
export -f _greeting

#----------------------------------------------------------------#
# Suffle playlist
#----------------------------------------------------------------#

shuffle_playlist() {

shuffle_choice=$(cat "$HOME"/Documents/sc_dj_mixes.txt | shuf -n 1)

echo "$shuffle_choice" | sed 's|^|https://soundcloud.com/|' | xargs mpv --no-video

}
export -f shuffle_playlist

#----------------------------------------------------------------#
# Stops mpv
#----------------------------------------------------------------#

stop_mpv() {

killall mpv
	
}
export -f stop_mpv

#----------------------------------------------------------------#
# play-pause mpv
#----------------------------------------------------------------#

play_toggle() {

   if [[ "$(pidof mpv)" ]]; then
     playerctl --player mpv play-pause
   else
     notify-send "Not playing"
   fi

}
export -f play_toggle

#----------------------------------------------------------------#
# Open url in browser
#----------------------------------------------------------------#

open_url() {

killall yad
selected_url=$(playerctl --player mpv metadata -f "{{xesam:url}}")

   if [[ "$(pidof mpv)" ]]; then
     "$MY_BROWSER" "$selected_url"
   else
     notify-send "Not Playing"
   fi
	
}
export -f open_url

#----------------------------------------------------------------#
# Play music mix with mpv
#----------------------------------------------------------------#

play_url() {

selected_choice=$(head ~/Documents/scdjm_selected.txt)
echo "https://soundcloud.com/$selected_choice" > ~/Documents/scdjm_full_path.txt
main_choice=$(head ~/Documents/scdjm_full_path.txt)

   if [[ "$(pidof mpv)" ]]; then
     killall mpv
     mpv --no-video "$main_choice"
   else
     mpv --no-video "$main_choice"
   fi

}
export -f play_url

#----------------------------------------------------------------#
# Select url for actions
#----------------------------------------------------------------#

select_mix() {

echo "$1" > ~/Documents/scdjm_selected.txt
	
}
export -f select_mix

#----------------------------------------------------------------#
# Currently playing
#----------------------------------------------------------------#

current_playing() {
	
notify-send "$(playerctl --player mpv metadata -f "{{xesam:url}}")"

}
export -f current_playing

#----------------------------------------------------------------#
# Open sound cloud text file
#----------------------------------------------------------------#

open_file() {

killall yad
xdg-open ~/Documents/sc_dj_mixes.txt
	
}
export -f open_file

#----------------------------------------------------------------#
# Mpv status
#----------------------------------------------------------------#

mpv_status() {

   if [[ "$(pidof mpv)" ]]; then
     notify-send "$(playerctl --player mpv status)"
   else
     notify-send "Not playing"
   fi

}
export -f mpv_status

#----------------------------------------------------------------#
# Add url
#----------------------------------------------------------------#

add_url() {

add_choice=$(GTK_THEME="$MY_GTK_THEMES" yad --entry \
--text="\nEnter Sound Cloud url...\n" \
--text-align="center" \
--width=900 \
--height=100 \
--button="_Exit":1)
[[ -z "$add_choice" ]] && exit 0

echo "$add_choice" | sed 's|https://soundcloud.com/||' | tee -a ~/Documents/sc_dj_mixes.txt 

	
}
export -f add_url

#----------------------------------------------------------------#
# Yad Dialog
#----------------------------------------------------------------#

choice_list="$HOME/Documents/sc_dj_mixes.txt" 
export choice_list

tail -f "$choice_list" | GTK_THEME="$MY_GTK_THEMES" yad --list \
--title="Sound Cloud Player" \
--column="music" \
--listen \
--search-column=1 \
--regex-search \
--separator= \
--no-headers \
--text="\n$(_greeting)\n" \
--text-align="center" \
--width="$YAD_WIDTH" \
--height="$YAD_HEIGHT" \
--borders=10 \
--dclick-action='/bin/bash -c "select_mix %s"' \
--buttons-layout="center" \
--button="${COLOR4}${END}:/bin/bash -c 'shuffle_playlist'" \
--button="${COLOR4}${END}:/bin/bash -c 'open_url'" \
--button="${COLOR4}${END}:/bin/bash -c 'mpv_status'" \
--button="${COLOR4}${END}:/bin/bash -c 'play_url'" \
--button="${COLOR4}${END}:/bin/bash -c 'open_file'" \
--button="HTTP:/bin/bash -c 'current_playing'" \
--button="${COLOR4}${END}:/bin/bash -c 'add_url'" \
--button="${COLOR4}󰐎${END}:/bin/bash -c 'play_toggle'" \
--button="${COLOR4}${END}:/bin/bash -c 'stop_mpv'" \
--button="${COLOR4}${END}":1 >> "$choice_list" --tail > /dev/null

#----------------------------------------------------------------#
# Unsets variables and functions
#----------------------------------------------------------------#

unset select_mix
unset stop_mpv
unset choice_list
unset launcher
unset MY_GTK_THEMES
unset COLOR1
unset COLOR2
unset COLOR3
unset COLOR4
unset END
unset play_toggle
unset add_url
unset current_playing
unset open_file
unset mpv_status
unset open_url
unset MY_BROWSER
unset play_url
unset _greeting
unset YAD_WIDTH
unset YAD_HEIGHT
unset shuffle_playlist
