#!/bin/bash

window_id=$(xdotool getactivewindow)
class_name=$(xprop -id "$window_id" | grep WM_CLASS | cut -d '"' -f2)
notify-send "$class_name" 
