#!/bin/bash

browser="brave"
book_file=$(cat "$HOME"/Documents/mainprefix/bookmarks.txt)

echo "$book_file" | sort | fzf | awk '{print $NF}' | xargs -r "$browser"
