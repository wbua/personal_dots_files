#!/bin/bash

program_command="qrcodegtk.sh"
program_version="1.0.0"
program_updated="21-02-2023"
program_license="GPL 3.0"
contributors="John Mcgrath"
program_website="none"
program_contact="none"

#--------------------------------------------------------------------#
# Help
#--------------------------------------------------------------------#

help(){
cat << EOF
...

## Tracking

    * Command:      $program_command
    * Version:      $program_version
    * Updated:      $program_updated
    * License:      $program_license
    * Website:      $program_website
    * Contact:      $program_contact
    * Contributors: $contributors

## Description

    * Create qr code from clipboard contents
    * Decode qr code in yad
    * Open qr code in image viewer
    * Open qr codes file location in file manager
    * Sends qr code filename to clipboard
    * Drag and drop text into box to create qr code
    * Copy qr code into clipboard
    * Enter text into box to create qr code
    * Drag and drop QR Code image to convert to text file

## Dependencies

    * Yad
    * Qrencode
    * Zbar-tools
    * Xclip
    * Material-design-icons (see in other information)

## Installation

   Before you install anything, first update your system

   Example:

   $ sudo apt update

   ---

   To install the dependencies for qr code app

   Example:

   $ sudo apt install yad qrencode zbar-tools xclip 

   ---

   To make the script executable follow the example below

   Example:

   $ chmod u+x qrcodegtk.sh

   Install script into /home/user/.local/bin/

   ---

   To install material-design-icons font, install the program below

   Example:

   $ sudo apt install font-manager

## Other information

   To get material-design-icons visit: https://github.com/google/material-design-icons/blob/master/font/MaterialIcons-Regular.ttf

EOF
}

while getopts ":h" option; do
  case $option in
    h) # display help
    help
    exit;;
   \?) # incorrect option
    echo "Error: invalid option"
    exit;;
  esac
done

#--------------------------------------------------------------------#
# Drag and drop QR Code image to convert to text file
#--------------------------------------------------------------------#

covertqrcima () {

rm -f "$HOME"/Documents/.qrcode_text/*.txt 
killall yad ; yad --width"200" --height="500" --center --text-align="center" --dnd --text="Drag and drop your qr code image here" | \
sed 's/^file:\/\///' | xargs zbarimg > "$HOME/Documents/.qrcode_text/$(date '+%d-%m-%Y-%H-%M-%S').txt"
sleep 0.4
find "$HOME"/Documents/.qrcode_text/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs xdg-open
}
export -f covertqrcima

#--------------------------------------------------------------------#
# Checks if directory exists, if not creates it. This is the qr code text directory
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Documents/.qrcode_text ]; then
     :
   else
     mkdir "$HOME"/Documents/.qrcode_text
   fi

#--------------------------------------------------------------------#
# Checks if directory exists, if not creates it. This is the directory where help text for yad is kept.
#--------------------------------------------------------------------#

   if [ -d "$HOME"/.inrsht ]; then
     :
   else
     mkdir "$HOME"/.inrsht
   fi

#--------------------------------------------------------------------#
# Help tab text. This text is put into the file scrsinfo.txt.
#--------------------------------------------------------------------#

helpqrcinfo="## Tracking

program command: qrcodegtk.sh
program version: 1.0.0
program updated: 15-01-2023
program license: GPL 3.0
contributors: John Mcgrath
program website: none
program contact: none

## Description

    * Create: Create qr code from clipboard contents
    * Decode: Decode the last qr code created
    * Viewer: Open qr code in image viewer
    * Open: Open qr codes file location in file manager
    * Name: Sends qr code filename to clipboard
    * Copy: Copy qr code into clipboard
    * Convert: Drag and drop QR Code image to convert to text file
    * Drag and drop text into box to create qr code
    * Enter text into box to create qr code
    
## Dependencies

    * Yad
    * Qrencode
    * Zbar-tools
    * Xclip
    * Material-design-icons (see in other information)

## Installation

   Before you install anything, first update your system

   Example:

   $ sudo apt update

   ---

   To install the dependencies for qr code app

   Example:

   $ sudo apt install yad qrencode zbar-tools xclip 

   ---

   To make the script executable follow the example below

   Example:

   $ chmod u+x qrcodegtk.sh

   Install script into /home/user/bin/

   ---

   To install material-design-icons font, install the program below

   Example:

   $ sudo apt install font-manager

## Other information

   To get material-design-icons visit: https://github.com/google/material-design-icons/blob/master/font/MaterialIcons-Regular.ttf"

export helpqrcinfo

#--------------------------------------------------------------------#
# Checks if the scrsinfo.txt exists, if not creates it. scrsinfo.txt is displayed in yad help tab.
#--------------------------------------------------------------------#

   if [ -f "$HOME"/.inrsht/qrcinfo.txt ]; then
     :
   else
     printf "%s" "$helpqrcinfo" >> "$HOME"/.inrsht/qrcinfo.txt
   fi

#--------------------------------------------------------------------#
# Checks if directory exists, if not creates it
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Pictures/qr_codes ]; then
     :
   else
     mkdir "$HOME"/Pictures/qr_codes
   fi

#--------------------------------------------------------------------#
# Temp qr code directory
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Pictures/.temqrdir ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrdir
   fi

#--------------------------------------------------------------------#
# Create qr code
#--------------------------------------------------------------------#

creqrcode() {

killall yad
rm -f "$HOME"/Pictures/.temqrdir/*.png
sleep 1
xclip -out -selection clipboard | \
qrencode -o "$HOME"/Pictures/.temqrdir/"$(date '+%d-%m-%Y-%H-%M-%S')".png
cp "$HOME"/Pictures/.temqrdir/*.png "$HOME"/Pictures/qr_codes/
qrcodegtk.sh

}
export -f creqrcode 

#--------------------------------------------------------------------#
# Decode qr code
#--------------------------------------------------------------------#

decoqrcode() {

killall yad
find "$HOME"/Pictures/.temqrdir/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg | \
yad --text-info --center --wrap --height=700 --width=670 --back=#1a1818 --fore=#ffffff

}
export -f decoqrcode 


#--------------------------------------------------------------------#
# Copy qr code to clipboard
#--------------------------------------------------------------------#

copytclipqrcode() {

killall yad
xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.temqrdir/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"

}
export -f copytclipqrcode

#--------------------------------------------------------------------#
# Open qr code in image viewer
#--------------------------------------------------------------------#

imavieqrcode() {

killall yad
find "$HOME"/Pictures/.temqrdir/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs xdg-open

}
export -f imavieqrcode 

#--------------------------------------------------------------------#
# Open qr code main directory
#--------------------------------------------------------------------#

openfilmaqrcode() {

GTK_THEME="Yaru-dark"
killall yad
xdg-open "$HOME"/Pictures/qr_codes/

}
export -f openfilmaqrcode 

#--------------------------------------------------------------------#
# Send qr code filename to clipboard
#--------------------------------------------------------------------#

filenatoclipqrode() {

killall yad
find "$HOME"/Pictures/.temqrdir/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xclip -selection clipboard

}
export -f filenatoclipqrode 

#--------------------------------------------------------------------#
# Generate random integer
#--------------------------------------------------------------------#

KEY=$RANDOM

#--------------------------------------------------------------------#
# QR Code menu tab
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog" yad --plug=$KEY --tabnum=1 --form --borders=100 --width=400 --height=400 --columns=4 --title="Yad screenshot tool" \
--image="$(find "$HOME"/Pictures/.temqrdir/ -type f -print0 | xargs -0 ls -tr | tail -n 1)" &

#--------------------------------------------------------------------#
# Help tab
#--------------------------------------------------------------------#

#GTK_THEME="alt-dialog" yad --plug=$KEY --tabnum=4 --wrap --text-info --fontname="Monospace 12" < "$HOME/.inrsht/qrcinfo.txt" &

#--------------------------------------------------------------------#
# Drag and drop tab
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog" yad --plug=$KEY --tabnum=2 --dnd --text="Drag text here to create qr code\nThen press the refresh button" --text-align="center" \
--width"400" --height="400" | \
qrencode -o "$HOME/Pictures/.temqrdir/$(date '+%Y-%m-%d-%H-%M-%S').png" &

#--------------------------------------------------------------------#
# Enter text tab
#--------------------------------------------------------------------#

#GTK_THEME="alt-dialog" yad --plug=$KEY --tabnum=3 --width"400" --height="400" --text-align="center" --title "Select Host" --entry --text="Enter your text\nThen press ok button" | \
#qrencode -o "$HOME/Pictures/.temqrdir/$(date '+%Y-%m-%d-%H-%M-%S').png" &

#--------------------------------------------------------------------#
# Main dialog
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog" yad --notebook --key=$KEY --center --borders=10 --width=400 --height=400 \
--text-align="center" \
--text="<span color='#ffffff' font_family='DejaVu Sans Mono' font='12' weight='bold'>QR Codes App</span>" \
--tab="<span color='#ffffff' font_family='DejaVu Sans Mono' font='12' weight='bold'>QR Code</span>" \
--tab="<span color='#ffffff' font_family='DejaVu Sans Mono' font='12' weight='bold'>Drag and Drop</span>" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Create</span>:/bin/bash -c 'creqrcode'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Decode</span>:/bin/bash -c 'decoqrcode'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Copy</span>:/bin/bash -c 'copytclipqrcode'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Convert</span>:/bin/bash -c 'covertqrcima'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Viewer</span>:/bin/bash -c 'imavieqrcode'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Open</span>:/bin/bash -c 'openfilmaqrcode'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Name</span>:/bin/bash -c 'filenatoclipqrode'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Refresh</span>:/bin/bash -c 'killall yad ; qrcodegtk.sh'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Ok</span>":0 \
--button="<span color='#ffffff' font_family='Inter' font='11'>Exit</span>":1 &

#--tab="<span color='#ffffff' font_family='DejaVu Sans Mono' font='12' weight='bold'>Text</span>" \
#--tab="<span color='#ffffff' font_family='DejaVu Sans Mono' font='12' weight='bold'>Help</span>" \


#--------------------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------------------#

unset helpqrcinfo
unset covertqrcima
unset creqrcode
unset decoqrcode
unset copytclipqrcode
unset imavieqrcode
unset openfilmaqrcode
unset filenatoclipqrode
