#!/bin/bash

# Sway user preferences 
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

ATM_MOD="$APP_ARTIST_TITLE_MOD"

   if [ -f "$HOME"/Documents/.i3blocks_output ]; then
     :
   else
     touch "$HOME"/Documents/.i3blocks_output
   fi

echo "$ATM_MOD" > "$HOME"/Documents/.i3blocks_output   
