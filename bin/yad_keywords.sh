#!/bin/bash

# Program command: yad_keyswords.sh
# Description: Runs your programs and uses keywords.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 04-05-2024
# Program_license: GPL 3.0
# Dependencies: yad, yt-dlp, amixer, zenity, dictd, dict-gcide, aspell, wl-clipboard, qrencode,
# festival, libreoffice, amixer, grim, slurp, ffmpeg, swaylock, wf-recorder, imagemagick, swaymsg, 
# jq, zbar-tools, wtype, playerctl, mpv-mpris, gnome-terminal, thunar, dunst, pactl, pacmd, arecord

# This script has been checked with shellcheck.

# This script is made to work in wayland only not xorg.
# Some of these keywords will only work in sway.

# Before you use the terminal here function execute this command below first.
# $ xfconf-query -c thunar -n -p /misc-full-path-in-title -t bool -s true

#------------------------------------------------------#
# Error checking
#------------------------------------------------------#

set -e

#------------------------------------------------------#
# Checks if you are running a wayland session
#------------------------------------------------------#

   if [[ "$WAYLAND_DISPLAY" == "" ]]; then
     yad --error \
     --width 500 \
     --title "Wayland session not found!" \
     --text "Cannot run color picker, wayland only tool!"
     exit 1
   else
     :
   fi

#------------------------------------------------------#
# User preferences
#------------------------------------------------------#

# Yad GTK theme
gtk_theme="BottleGlass-Dark-2.1"
export gtk_theme

# Terminal
myterminal="foot"
export myterminal

# Other terminal
other_terminal="gnome-terminal"
export other_terminal

# Browser
browser="brave"
export browser

# File manager
file_manager="thunar"
export file_manager

# Delay screenshot time
delay_time="5"
export delay_time

# Path to radio stations
radio_links="$HOME/Documents/mainprefix/radiolinks.txt"
export radio_links

# Emoji text file
emoji_text="$HOME/Documents/emoji-data.txt"
export emoji_text

# My printer
my_printer="Brother_DCP_J1050DW"
export my_printer

# Screenshots directory
screenshots_dir="$HOME/Pictures/screenshots/"
export screenshots_dir

# PDF directory
pdf_dir="$HOME/Documents/pdf/"
export pdf_dir

# Screencast directory
screencast_dir="$HOME/Videos/recordings/" 
export screencast_dir

# Audio recordings directory
audio_dir="$HOME/Music/recordings/" 
export audio_dir

# QR Code directory 
qr_code_dir="$HOME/Pictures/qr_codes/"
export qr_code_dir

# Zips directory
zips_dir="$HOME/Documents/zips/"
export zips_dir

#------------------------------------------------------#
# Create files and directories
#------------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/.temqrcodes ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes
   fi

   if [ -d "$HOME"/Pictures/qr_codes ]; then
     :
   else
     mkdir "$HOME"/Pictures/qr_codes
   fi

   if [ -d "$HOME"/Documents/.pdftemp ]; then
     :
   else
     mkdir "$HOME"/Documents/.pdftemp
   fi

   if [ -d "$HOME"/Documents/pdf ]; then
     :
   else
     mkdir "$HOME"/Documents/pdf
   fi

   if [ -d "$HOME"/Videos/.yadvidrec ]; then
     :
   else
     mkdir "$HOME"/Videos/.yadvidrec
   fi

   if [ -d "$HOME"/Videos/recordings ]; then
     :
   else
     mkdir "$HOME"/Videos/recordings
   fi

   if [ -d "$HOME"/Pictures/.tempfullscrns ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempfullscrns
   fi

   if [ -d "$HOME"/Pictures/screenshots ]; then
     :
   else
     mkdir "$HOME"/Pictures/screenshots
   fi

   if [ -d "$HOME"/Pictures/.ocr ]; then
     :
   else
     mkdir "$HOME"/Pictures/.ocr
   fi

   if [ -d "$HOME"/Documents/.zips ]; then
     :
   else
     mkdir "$HOME"/Documents/.zips
   fi

   if [ -d "$HOME"/Documents/zips ]; then
     :
   else
     mkdir "$HOME"/Documents/zips
   fi

   if [ -d "$HOME"/Music/.yadaudiorec ]; then
     :
   else
     mkdir "$HOME"/Music/.yadaudiorec
   fi

   if [ -d "$HOME"/Music/recordings ]; then
     :
   else
     mkdir "$HOME"/Music/recordings
   fi

   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ -f "$HOME"/Documents/mainprefix/radiolinks.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/radiolinks.txt
   fi

   if [ -f "$HOME"/Documents/.bc_results.txt ]; then
     :
   else
     touch "$HOME"/Documents/.bc_results.txt
   fi

   if [ -f "$HOME"/Documents/quicknotes.txt ]; then
     :
   else
     touch "$HOME"/Documents/quicknotes.txt
   fi

   if [ -f "$HOME"/Documents/.zipfile.txt ]; then
     :
   else
     touch "$HOME"/Documents/.zipfile.txt
   fi

#------------------------------------------------------#
# Colors
#------------------------------------------------------#

# Keyword description
color1=$(printf "<span color='#FFFFFF' font_family='Cascadia Mono' font='16' rise='0pt'>")
export color1
# Keywords
color2=$(printf "<span color='#4AFF00' font_family='Cascadia Mono' font='17' rise='0pt'>")
export color2
# Extra info
color3=$(printf "<span color='#FEFF00' font_family='Cascadia Mono' font='16' rise='0pt'>")
export color3
# Color end
end=$(printf "</span>")
export end

#------------------------------------------------------#
# Keyword help page
#------------------------------------------------------#

keyword_help() {

echo "Keywords are in green, followed by input."
echo "Always add one space after keyword then input."
echo ""
echo "${color1}Find local files:${end} ${color2} f${end} ${color1} text${end}"
echo "${color1}Find local files:${end} ${color2} fh${end} ${color3} last search history${end}"
echo "${color1}Google search:${end} ${color2} g${end} ${color1} text${end}"
echo "${color1}Google translate:${end} ${color2} t${end} ${color1} text${end}"
echo "${color1}Youtube search:${end} ${color2} yt${end} ${color1} text${end}"
echo "${color1}Duckduckgo search:${end} ${color2} ddg${end} ${color1} text${end}"
echo "${color1}Wikipedia search:${end} ${color2} wik${end} ${color1} text${end}"
echo "${color1}Reddit search:${end} ${color2} rd${end} ${color1} text${end}"
echo "${color1}Download video:${end} ${color2} do${end} ${color1} link${end}"
echo "${color1}Shutdown:${end} ${color2} shutdown${end}"
echo "${color1}Reboot:${end} ${color2} restart${end}"
echo "${color1}Lock:${end} ${color2} lock${end}"
echo "${color1}Volume:${end} ${color2} status${end}"
echo "${color1}Volume:${end} ${color2} v${end} ${color1} number${end} ${color3} 0 to 100${end}"
echo "${color1}Mute volume:${end} ${color2} mute${end}"
echo "${color1}Dictionary:${end} ${color2} di${end} ${color1} text${end}"
echo "${color1}Spell checker:${end} ${color2} sp${end} ${color1} text${end}"
echo "${color1}QR codes:${end} ${color2} qr${end} ${color1} text${end}"
echo "${color1}QR codes:${end} ${color2} qrd${end} ${color3} select qr code on screen to decode${end}" 
echo "${color1}Calculator:${end} ${color2} =${end} ${color1} sum${end} ${color3} example: = 12+12${end}"
echo "${color1}Calculator:${end} ${color2} =h${end} ${color3} history${end}"
echo "${color1}Text to speech:${end} ${color2} spk${end} ${color1} text${end} ${color3} speaks text${end}"
echo "${color1}Text to speech:${end} ${color2} text${end} ${color1} path/to/file${end} ${color3} speaks text file contents${end}"
echo "${color1}Text to speech:${end} ${color2} ttsk${end} ${color3} kills text to speech${end}"
echo "${color1}Kill process:${end} ${color2} kill${end} ${color1} program${end} ${color3} killall command${end}"
echo "${color1}Kill process:${end} ${color2} ki${end} ${color1} program${end} ${color3} pkill command${end}"
echo "${color1}PDF:${end} ${color2} pdf${end} ${color1} path/to/file${end} ${color3} creates pdf${end}"
echo "${color1}PDF:${end} ${color2} pdfo${end} ${color3} open last pdf created${end}"
echo "${color1}PDF:${end} ${color2} pdfp${end} ${color3} print pdf${end}"
echo "${color1}Screenshots:${end} ${color2} full${end} ${color3} fullscreen screenshot${end}"
echo "${color1}Screenshots:${end} ${color2} select${end} ${color3} select screenshot${end}"
echo "${color1}Screenshots:${end} ${color2} delay${end} ${color3} delay screenshot${end}"
echo "${color1}Screenshots:${end} ${color2} open${end} ${color3} open last created screenshot${end}"
echo "${color1}Screencast:${end} ${color2} rec${end} ${color3} starts screen recording${end}"
echo "${color1}Screencast:${end} ${color2} stop${end} ${color3} stops screen recording${end}"
echo "${color1}Screencast:${end} ${color2} play${end} ${color3} plays last screen recording${end}"
echo "${color1}Terminal commands:${end} ${color2} #${end} ${color1} command${end}"
echo "${color1}Terminal:${end} ${color2} here${end} ${color3} open terminal from thunar directory${end}"
echo "${color1}Password generator:${end} ${color2} pass${end} ${color1} number${end} ${color3} copies to clipboard, example: pass 10${end}"
echo "${color1}Resize image:${end} ${color2} res${end} ${color1} 400x${end} ${color3} before keyword, first copy image to clipboard, saves to picture directory${end}"
echo "${color1}Emoji:${end} ${color2} em${end}"
echo "${color1}Note:${end} ${color2} n${end} ${color1} text${end}"
echo "${color1}Note:${end} ${color2} nh${end} ${color3} note history${end}"
echo "${color1}Color picker:${end} ${color2} pick${end}"
echo "${color1}Color:${end} ${color2} hex${end} ${color1} code${end} ${color3} changes hex to rgb, copies to clipboard, example: hex #10FF00${end}"
echo "${color1}Color:${end} ${color2} rgb${end} ${color1} code${end} ${color3} changes rgb to hex, copies to clipboard, example: rgb 16 255 0${end}"
echo "${color1}Keywords:${end} ${color2} keys${end}"
echo "${color1}Player:${end} ${color2} p${end}  ${color1} command${end} ${color3} command: play, stop, next, prev, stat, art, qr, last ${end}"
echo "${color1}Palette:${end} ${color2} br${end} ${color1} command${end} ${color3} brave palette: new, close, save, 1-6, history, download, reopen, manager${end}"
echo "${color1}Text:${end} ${color2} lo${end} ${color1} text${end} ${color3} uppercase to lowercase${end}"
echo "${color1}Timezone:${end} ${color2} la${end} ${color1} 15:00 Fri${end} ${color3} Los Angeles${end}"
echo "${color1}Timezone:${end} ${color2} ny${end} ${color1} 15:00 Fri${end} ${color3} New York${end}"
echo "${color1}Timezone:${end} ${color2} sy${end} ${color1} 15:00 Fri${end} ${color3} Sydney${end}"
echo "${color1}Timezone:${end} ${color2} zones${end}"
echo "${color1}Notifications:${end} ${color2} not${end} ${color1} history${end}"
echo "${color1}Notifications:${end} ${color2} not${end} ${color1} toggle${end}"
echo "${color1}Microphone:${end} ${color2} mic${end} ${color1} level${end} ${color3} sends notification${end}"
echo "${color1}Microphone:${end} ${color2} mic${end} ${color1} on${end} ${color3} microphone on${end}"
echo "${color1}Microphone:${end} ${color2} mic${end} ${color1} off${end} ${color3} microphone off${end}"
echo "${color1}Microphone:${end} ${color2} mic${end} ${color1} number${end} ${color3} example: mic 50${end}"
echo "${color1}Zip files:${end} ${color2} com${end}"
echo "${color1}Video:${end} ${color2} pv${end} ${color1} link${end} ${color3} plays video with mpv, example: pv http://videos.mp4${end}"
echo "${color1}Video:${end} ${color2} pv${end} ${color1} stop${end} ${color3} stops mpv video${end}"
echo "${color1}Modified:${end} ${color2} mod${end} ${color3} last modified file${end}"
echo "${color1}Audio:${end} ${color2} rmic${end} ${color1} stop${end} ${color3} stops recording${end}"
echo "${color1}Audio:${end} ${color2} rmic${end} ${color1} open${end} ${color3} opens recording${end}"
echo "${color1}Audio:${end} ${color2} rmic${end} ${color3} records audio from microphone${end}"
echo "${color1}Radio:${end} ${color2} radio${end} ${color3} plays radio url in mpv from text file${end}"

}

export -f keyword_help

#------------------------------------------------------#
# Show keyword help page in yad
#------------------------------------------------------#

show_keywords() {

killall yad
keyword_help | GTK_THEME="$gtk_theme" yad --list \
--column="help" \
--search-column=1 \
--regex-search \
--borders=10 \
--text="\nKeywords help page\nType control+f to search list\n" \
--text-align="center" \
--width=1200 \
--height=450 \
--no-headers \
--button="Close":1
	
}
export -f show_keywords

#------------------------------------------------------#
# Text file containing your programs
#------------------------------------------------------#

my_apps_file="$HOME/Documents/apps.txt"
export my_apps_file

#------------------------------------------------------#
# Google web search
#------------------------------------------------------#

google_search() {

web_search="https://www.google.co.uk/search?q={}"
echo "$launcher_content" | xargs -I{} "$browser" "$web_search"
	
}
export -f google_search

#------------------------------------------------------#
# Youtube web search
#------------------------------------------------------#

youtube_search() {

youtube_search="https://www.youtube.com/results?search_query={}"
echo "$launcher_content" | xargs -I{} "$browser" "$youtube_search"

}
export -f youtube_search

#------------------------------------------------------#
# Duckduckgo web search
#------------------------------------------------------#

duckduckgo() {

duckduckgo_search="https://duckduckgo.com/?q={}"
echo "$launcher_content" | xargs -I{} "$browser" "$duckduckgo_search"

}
export -f duckduckgo

#------------------------------------------------------#
# Wikipedia web search
#------------------------------------------------------#

wikipedia() {

wikipedia_search="https://en.wikipedia.org/wiki/{}"
echo "$launcher_content" | xargs -I{} "$browser" "$wikipedia_search"
	
}
export -f wikipedia

#------------------------------------------------------#
# Reddit web search
#------------------------------------------------------#

reddit() {

reddit_search="https://www.reddit.com/search/?q={}"
echo "$launcher_content" | xargs -I{} "$browser" "$reddit_search"
	
}
export -f reddit

#------------------------------------------------------#
# Google translate (french)
#------------------------------------------------------#

google_trans_fr() {

search_translate="https://translate.google.com/?sl=auto&tl=fr&text={}"
echo "$launcher_content" | xargs -I{} "$browser" "$search_translate"
   	
}
export -f google_trans_fr

#------------------------------------------------------#
# Download video
#------------------------------------------------------#

download_video() {

yt-dlp "$launcher_content" -o "$HOME/Downloads/%(title)s.%(ext)s" && notify-send "Download completed"
	
}
export -f download_video

#------------------------------------------------------#
# Open selected file from (find local files)
#------------------------------------------------------#

open_file() {

temp_search_file=$(head "$HOME"/Documents/.yad_search_results.txt)
killall yad
xdg-open "$temp_search_file"
	
}
export -f open_file

#------------------------------------------------------#
# Select file from list (find local files)
#------------------------------------------------------#

file_results() {

echo "$1" > "$HOME"/Documents/.yad_search_results.txt
	
}
export -f file_results

#------------------------------------------------------#
# Select file and open in file manager  (find local files)
#------------------------------------------------------#

open_file_manager() {

killall yad
chosen_filemanager=$(head "$HOME"/Documents/.yad_search_results.txt)
[ -z "$chosen_filemanager" ] && exit 0
"$file_manager" "$chosen_filemanager" 

}
export -f open_file_manager

#------------------------------------------------------#
# Find local files
#------------------------------------------------------#

find_files() {

# Directories searched
dirs=( ~/Documents/ ~/Downloads/ ~/Videos/ ~/Pictures/ ~/Music/ )

# Yad search settings
echo "$launcher_content" > "$HOME"/Documents/.find_file_results.txt
chosen_search_file=$(head "$HOME"/Documents/.find_file_results.txt)
find "${dirs[@]}" -maxdepth 8 -type f -iname "*$chosen_search_file*" | \
GTK_THEME="$gtk_theme" yad --list \
--column="find" \
--text="Double click file and press button" \
--text-align="center" \
--search-column=1 \
--width=1200 \
--height=500 \
--separator= \
--no-headers \
--no-markup \
--borders=20 \
--dclick-action='/bin/bash -c "file_results %s"' \
--button="Open:/bin/bash -c 'open_file'" \
--button="File manager:/bin/bash -c 'open_file_manager'" \
--button="Close":1

}
export -f find_files

#------------------------------------------------------#
# Find local files (history)
#------------------------------------------------------#

find_files_history() {

# Directories searched
dirs2=( ~/Documents/ ~/Downloads/ ~/Videos/ ~/Pictures/ ~/Music/ )

# Yad search settings
chosen_search_file=$(head "$HOME"/Documents/.find_file_results.txt)
find "${dirs2[@]}" -maxdepth 8 -type f -iname "*$chosen_search_file*" | \
GTK_THEME="$gtk_theme" yad --list \
--column="find" \
--text="Double click file and press button" \
--text-align="center" \
--search-column=1 \
--width=1200 \
--height=500 \
--separator= \
--no-headers \
--no-markup \
--borders=20 \
--dclick-action='/bin/bash -c "file_results %s"' \
--button="Open:/bin/bash -c 'open_file'" \
--button="File manager:/bin/bash -c 'open_file_manager'" \
--button="Close":1
	
}
export -f find_files_history

#------------------------------------------------------#
# Power reboot
#------------------------------------------------------#

yadreboot() {

   if zenity --question --text "Reboot, are you sure"
   then
     poweroff --reboot
   else
     exit 1
   fi

}
export -f yadreboot

#------------------------------------------------------#
# Power shutdown
#------------------------------------------------------#

yadshutdown() {

   if zenity --question --text "Shutdown, are you sure"
   then
     poweroff --poweroff
   else
     exit 1
   fi

}
export -f yadshutdown

#------------------------------------------------------#
# Power lock
#------------------------------------------------------#

yadlock() {

   if zenity --question --text "Lock, are you sure"
   then
     swaylock -c '#000000'
   else
     exit 1
   fi

}
export -f yadlock

#------------------------------------------------------#
# Mute volume
#------------------------------------------------------#

mute_volume() {

amixer -D pulse set Master 1+ toggle && pkill -RTMIN+10 i3blocks

}
export -f mute_volume

#------------------------------------------------------#
# Dictionary
#------------------------------------------------------#

my_dictionary() {

echo "$launcher_content" | xargs dict | GTK_THEME="$gtk_theme" yad --text-info \
--wrap \
--width=900 \
--height=600 \
--borders=20 \
--button="Cancel:1"

}
export -f my_dictionary

#------------------------------------------------------#
# Copy word to clipboard (Spell checker)
#------------------------------------------------------#

copy_spell_word() {

killall yad
head "$HOME"/Documents/.yad_spellcheck_results.txt | \
awk -F ':' '{print $NF}' | sed 's| ||' | wl-copy -n
	
}
export -f copy_spell_word


#------------------------------------------------------#
# Selected word (Spell checker)
#------------------------------------------------------#

spellcheck_results() {

echo "$1" > "$HOME"/Documents/.yad_spellcheck_results.txt
	
}
export -f spellcheck_results

#------------------------------------------------------#
# Spell checker
#------------------------------------------------------#

spell_checker() {

aspell -a list <<< "$launcher_content" | \
awk 'BEGIN{FS=","; OFS="\n"} {$1=$1} 1' | GTK_THEME="$gtk_theme" yad --list \
--text="Double click on word and press button" \
--text-align="center" \
--width=600 \
--no-markup \
--height=600 \
--borders=20 \
--column="spell" \
--no-headers \
--dclick-action='/bin/bash -c "spellcheck_results %s"' \
--button="Copy to clipboard:/bin/bash -c 'copy_spell_word'" \
--button="Cancel:1"

}
export -f spell_checker

#------------------------------------------------------#
# QR codes (send image notification)
#------------------------------------------------------#

qr_code_notification() {

image_notification=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
notify-send -i "$image_notification" "Qrcode"
	
}
export -f qr_code_notification

#------------------------------------------------------#
# QR codes
#------------------------------------------------------#

qr_codes() {

rm -f "$HOME"/Pictures/.temqrcodes/*.png
echo "$launcher_content" | tr -d '\n' | qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
cp "$HOME"/Pictures/.temqrcodes/*.png "$qr_code_dir"
qr_code_notify=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
notify-send -i "$qr_code_notify" "Created QR Code"

}
export -f qr_codes

#------------------------------------------------------#
# Calculator (copy to clipboard)
#------------------------------------------------------#

calculator_clipboard() {

killall yad
head "$HOME"/Documents/.calculator_copy.txt | wl-copy -n
	
}
export -f calculator_clipboard

#------------------------------------------------------#
# Calculator (send sum to text file)
#------------------------------------------------------#

my_calculator_copy() {

echo "$1" > "$HOME"/Documents/.calculator_copy.txt
	
}
export -f my_calculator_copy

#------------------------------------------------------#
# Calculator
#------------------------------------------------------#

my_calculator() {

sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.bc_results.txt
echo "$launcher_content" | bc | tee -a "$HOME"/Documents/.bc_results.txt
tac ~/Documents/.bc_results.txt | GTK_THEME="$gtk_theme" yad --list \
--title="calculator" \
--text-align="center" \
--text="Double click sum and then press button" \
--width=500 \
--height=500 \
--borders=20 \
--no-markup \
--column="calculator" \
--dclick-action='/bin/bash -c "my_calculator_copy %s"' \
--no-headers \
--button="Copy to clipboard:/bin/bash -c 'calculator_clipboard'" \
--button="Cancel:1"
	
}
export -f my_calculator

#------------------------------------------------------#
# Calculator history
#------------------------------------------------------#

calculator_history() {

tac ~/Documents/.bc_results.txt | GTK_THEME="$gtk_theme" yad --list \
--title="calculator" \
--text-align="center" \
--text="Double click sum and then press button" \
--width=500 \
--height=500 \
--borders=20 \
--no-markup \
--column="calculator" \
--dclick-action='/bin/bash -c "my_calculator_copy %s"' \
--no-headers \
--button="Copy to clipboard:/bin/bash -c 'calculator_clipboard'" \
--button="Cancel:1"
	
}
export -f calculator_history

#------------------------------------------------------#
# Text to speech (speak)
#------------------------------------------------------#

tts_speak() {

echo "$launcher_content" | festival --tts
	
}
export -f tts_speak

#------------------------------------------------------#
# Text to speech (file)
#------------------------------------------------------#

tts_file() {

festival --tts "$launcher_content"
	
}
export -f tts_file

#------------------------------------------------------#
# Text to speech (kill)
#------------------------------------------------------#

tts_kill() {

pkill -f audsp

}
export -f tts_kill

#------------------------------------------------------#
# Kill processes with killall command
#------------------------------------------------------#

kill_app() {

killall "$launcher_content"
	
}
export -f kill_app

#------------------------------------------------------#
# Kill processes with pkill command
#------------------------------------------------------#

pkill_app() {

pkill -f "$launcher_content"
	
}
export -f pkill_app

#------------------------------------------------------#
# Volume
#------------------------------------------------------#

set_volume() {

echo "$launcher_content" > "$HOME"/Documents/.set_volume.txt
set_volume_choice=$(< "$HOME"/Documents/.set_volume.txt)
amixer -D pulse sset Master "$set_volume_choice%" && pkill -RTMIN+10 i3blocks	
notify-send "Volume" "$set_volume_choice%"
	
}
export -f set_volume

#------------------------------------------------------#
# Create PDF
#------------------------------------------------------#

create_pdf() {

rm -f "$HOME"/Documents/.pdftemp/*.*
libreoffice --headless --convert-to pdf "$launcher_content" --outdir "$HOME"/Documents/.pdftemp/ && notify-send "PDF created"
cp "$HOME"/Documents/.pdftemp/*.* "$pdf_dir"
	
}
export -f create_pdf

#------------------------------------------------------#
# PDF (open file)
#------------------------------------------------------#

pdf_open() {

   if [[ -z "$(ls -A "$HOME"/Documents/.pdftemp)" ]]; then	
     notify-send "Pdf directory is empty"
   else
     find "$HOME"/Documents/.pdftemp/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 -d '\n' xdg-open
   fi
	
}
export -f pdf_open

#------------------------------------------------------#
# PDF print
#------------------------------------------------------#

pdf_print() {

   if [[ -z "$(ls -A "$HOME"/Documents/.pdftemp)" ]]; then
     notify-send "Pdf directory is empty"
   else
    lp -d "$my_printer" ~/Documents/.pdftemp/*.pdf	
   fi
	
}
export -f pdf_print

#------------------------------------------------------#
# Select screenshot
#------------------------------------------------------#

select_shot() {

rm -f "$HOME"/Pictures/.tempfullscrns/*.png
sleep 1
grimshot save area  ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png && check_sound
cp "$HOME"/Pictures/.tempfullscrns/*.png "$screenshots_dir"
	
}
export -f select_shot

#------------------------------------------------------#
# Full screenshot
#------------------------------------------------------#

full_shot() {

rm -f "$HOME"/Pictures/.tempfullscrns/*.png
sleep 1
grimshot save screen ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png && check_sound 
cp "$HOME"/Pictures/.tempfullscrns/*.png "$screenshots_dir"
	
}
export -f full_shot

#------------------------------------------------------#
# Delay screenshot
#------------------------------------------------------#

delay_shot() {

rm -f "$HOME"/Pictures/.tempfullscrns/*.png
sleep "$delay_time" && grimshot save screen ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png && check_sound
cp "$HOME"/Pictures/.tempfullscrns/*.png "$screenshots_dir"

	
}
export -f delay_shot

#------------------------------------------------------#
# Full screenshot (open screenshot)
#------------------------------------------------------#

open_shot() {

   if [[ -z "$(ls -A "$HOME"/Pictures/.tempfullscrns)" ]]; then
     notify-send "Directory is empty"
   else
     temp_shot=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)	
     xdg-open "$temp_shot"
   fi

}
export -f open_shot

#------------------------------------------------------#
# Check if camera click sound exists
#------------------------------------------------------#

check_sound() {

   if [ -f "$HOME"/Music/sounds/cameraclick.wav ]; then
     ffplay -autoexit -nodisp ~/Music/sounds/cameraclick.wav    
   else
     notify-send "Screenshot taken"     
   fi
	
}
export -f check_sound

#------------------------------------------------------#
# Record screen (screencast)
#------------------------------------------------------#

record_screen() {

rm -f ~/Videos/.yadvidrec/*
notify-send "Recording started!" && wf-recorder -f "$HOME"/Videos/.yadvidrec/"$(date +'%d-%m-%Y-%H%M%S')".mkv
	
}
export -f record_screen

#------------------------------------------------------#
# Stop screen recording (screencast)
#------------------------------------------------------#

stop_rec() {

killall -s SIGINT wf-recorder
cp "$HOME"/Videos/.yadvidrec/*.mkv "$screencast_dir"
notify-send "Recording stopped!"
	
}
export -f stop_rec

#------------------------------------------------------#
# Play screen recording (screencast)
#------------------------------------------------------#

play_rec() {

   if [[ -z "$(ls -A "$HOME"/Videos/.yadvidrec)" ]]; then	
     notify-send "Pdf directory is empty"
   else
     find "$HOME"/Videos/.yadvidrec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 -d '\n' xdg-open
   fi

}
export -f play_rec

#------------------------------------------------------#
# Exec terminal commands
#------------------------------------------------------#

cli_commands() {

"$myterminal" -- /bin/bash -c "$launcher_content; $SHELL"

}
export -f cli_commands

#------------------------------------------------------#
# Password generator
#------------------------------------------------------#

pass_gen() {

tr -dc A-Za-z0-9 </dev/urandom | head -c "$launcher_content" | wl-copy -n

}
export -f pass_gen

#------------------------------------------------------#
# Resize image
#------------------------------------------------------#

resize_image() {

wl-paste > "$HOME"/Pictures/.temp_resize_image.txt
sed -i 's|file:\//||' "$HOME"/Pictures/.temp_resize_image.txt
head "$HOME"/Pictures/.temp_resize_image.txt | xargs | tr -d '\r' | \
sed 's|%20| |g' | tee "$HOME"/Pictures/.temp_resize_image.txt
convert_file=$(head "$HOME"/Pictures/.temp_resize_image.txt)
notify_filename=$(head "$HOME"/Pictures/.temp_resize_image.txt | xargs -0 basename)

convert "$convert_file" -resize "$launcher_content" "$HOME/Pictures/resize_$(date +'%d-%m-%Y-%H%M%S')".png
notify-send "converted image $notify_filename"
	
}
export -f resize_image

#------------------------------------------------------#
# Switch to already open apps
#------------------------------------------------------#
# This works only in sway

app_running() {

   if [[ "$DESKTOP_SESSION" == 'sway' ]]; then
     swaymsg -t get_tree \
       | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class):\(.name)"' \
       | grep -i "$bang" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' focus
   else
      /bin/bash -c "$bang" || exit 1
   fi 
	
}
export -f app_running

#------------------------------------------------------#
# Open new instances of apps
#------------------------------------------------------#

new_instance() {

   if [[ "$bang" == 'foot' ]]; then
     /bin/bash -c foot || exit 1
   else
     /bin/bash -c "$bang" || exit 1
   fi
	
}
export -f new_instance

#------------------------------------------------------#
# Emoji
#------------------------------------------------------#

emoji_list() {

emoji_cat_text=$(cat "$emoji_text")
emoji_choice=$(echo "$emoji_cat_text" | GTK_THEME="$gtk_theme" yad --list --search-column=1 \
--column="emoji" \
--regex-search \
--title="Emoji's" \
--text="Double click to copy to clipboard\nType control+f to search\n" \
--text-align="center" \
--borders=20 \
--no-headers \
--separator= \
--width=500 \
--height=500 \
--no-markup \
--button="Close":1)
[[ -z "$emoji_choice" ]] && exit

echo "$emoji_choice" | awk '{print $1}' | wl-copy -n

}
export -f emoji_list

#------------------------------------------------------#
# One line note (copy note to clipboard)
#------------------------------------------------------#

open_note_text_file() {

killall yad
xdg-open "$HOME"/Documents/quicknotes.txt
	
}
export -f open_note_text_file

#------------------------------------------------------#
# One line note (copy note to clipboard)
#------------------------------------------------------#

one_line_note_copy() {

killall yad
head "$HOME"/Documents/.one_line_temp_note.txt | wl-copy -n
	
}
export -f one_line_note_copy

#------------------------------------------------------#
# One line note (send selection to file)
#------------------------------------------------------#

one_line_note_select() {

echo "$1" > "$HOME"/Documents/.one_line_temp_note.txt
	
}
export -f one_line_note_select

#------------------------------------------------------#
# One line note (create note and view note)
#------------------------------------------------------#

one_line_note() {

printf '%s\n' "$launcher_content" >> "$HOME"/Documents/quicknotes.txt
note_file=$(tac "$HOME"/Documents/quicknotes.txt)
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/quicknotes.txt

echo "$note_file" | GTK_THEME="$gtk_theme" yad --list \
--search-column=1 \
--column="notes" \
--regex-search \
--title="One line note" \
--text="Double click note and press button\nType control+f to search\n" \
--text-align="center" \
--borders=20 \
--no-headers \
--separator= \
--width=800 \
--height=500 \
--no-markup \
--dclick-action='/bin/bash -c "one_line_note_select %s"' \
--button="Copy to clipboard:/bin/bash -c 'one_line_note_copy'" \
--button="Open text file:/bin/bash -c 'open_note_text_file'" \
--button="Close":1
	
}
export -f one_line_note

#------------------------------------------------------#
# One line note (history)
#------------------------------------------------------#

one_line_note_history() {

note_file_history=$(tac "$HOME"/Documents/quicknotes.txt)

echo "$note_file_history" | GTK_THEME="$gtk_theme" yad --list \
--search-column=1 \
--column="notes" \
--regex-search \
--title="One line note" \
--text="Double click note and press button\nType control+f to search\n" \
--text-align="center" \
--borders=20 \
--no-headers \
--separator= \
--width=800 \
--height=500 \
--no-markup \
--dclick-action='/bin/bash -c "one_line_note_select %s"' \
--button="Copy to clipboard:/bin/bash -c 'one_line_note_copy'" \
--button="Open text file:/bin/bash -c 'open_note_text_file'" \
--button="Close":1
	
}
export -f one_line_note_history

#------------------------------------------------------#
# Decode QR Code (copy qr code to clipboard)
#------------------------------------------------------#

decode_qr_code_copy() {

killall yad
head "$HOME"/Documents/.qr_code_decoded_text.txt | wl-copy -n
	
}
export -f decode_qr_code_copy

#------------------------------------------------------#
# Decode QR Code (select decoded qr code text)
#------------------------------------------------------#

decode_qr_code_select() {

echo "$1" > "$HOME"/Documents/.qr_code_decoded_text.txt
	
}
export -f decode_qr_code_select

#------------------------------------------------------#
# Decode QR Code
#------------------------------------------------------#

decode_qr_code() {

rm -f "$HOME"/Pictures/.ocr/*.png
grimshot save area ~/Pictures/.ocr/ocr.png
find "$HOME"/Pictures/.ocr/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw | \
GTK_THEME="$gtk_theme" yad --list \
--search-column=1 \
--column="qr codes" \
--regex-search \
--title="One line note" \
--text="Double click text and press button\nType control+f to search\n" \
--text-align="center" \
--borders=20 \
--no-headers \
--separator= \
--width=800 \
--height=500 \
--no-markup \
--dclick-action='/bin/bash -c "decode_qr_code_select %s"' \
--button="Copy to clipboard:/bin/bash -c 'decode_qr_code_copy'" \
--button="Close":1

}
export -f decode_qr_code

#------------------------------------------------------#
# HEX to RGB
#------------------------------------------------------#

hex_to_rgb() {

my_hex_choice=$(printf '%s' "$launcher_content" | sed 's|#||')
hex="$my_hex_choice"
printf "%d %d %d\n" 0x"${hex:0:2}" 0x"${hex:2:2}" 0x"${hex:4:2}" | wl-copy -n

}
export -f hex_to_rgb

#------------------------------------------------------#
# RGB to HEX
#------------------------------------------------------#

rgb_to_hex() {

printf "%02X%02X%02X" ${*//','/' '}
[ $(basename -- $0) == "hex" ] && [ $# -gt 0 ] && echo $(hex "${*}")

}
export -f rgb_to_hex

#------------------------------------------------------#
# Calls RGB to HEX
#------------------------------------------------------#

call_rgb_to_hex() {

rgb_to_hex "$launcher_content" | sed 's|^|#|' | xargs wl-copy -n

}
export -f call_rgb_to_hex

#------------------------------------------------------#
# Color picker
#------------------------------------------------------#

color_picker() {

grim -g "$(slurp -p)" -t ppm - | convert - -format '%[pixel:p{0,0}]' txt:- | tail -n 1 | \
awk '{print $3}' | xargs wl-copy -n

}
export -f color_picker

#------------------------------------------------------#
# Command palette for brave browser
#------------------------------------------------------#
# This works only in sway.

brave_palette() {

brave_pchoice=$(swaymsg -t get_tree | jq -r '.. | select(.type?) | select(.focused==true).name' | tr "&" "+" | \
tr -d '<>' | tr -d '#' | tr -d '""' | tr '[:upper:]' '[:lower:]' | tr -dC '[:print:]\t\n' | awk '{print $NF}')

   # New tab
   if [[ "$brave_pchoice" == "brave" && "$launcher_content" == 'new' ]]; then
     wtype -M ctrl -k t
   # Close tab
   elif [[ "$brave_pchoice" == "brave" && "$launcher_content" == 'close' ]]; then
     wtype -M ctrl -k w
   # Save bookmark
   elif [[ "$brave_pchoice" == "brave" && "$launcher_content" == 'save' ]]; then
     wtype -M ctrl -k d
   # Switch tab 1
   elif [[ "$brave_pchoice" == "brave" && "$launcher_content" == '1' ]]; then
     wtype -M ctrl -k 1
   # Switch tab 2
   elif [[ "$brave_pchoice" == "brave" && "$launcher_content" == '2' ]]; then
     wtype -M ctrl -k 2
   # Switch tab 3
   elif [[ "$brave_pchoice" == "brave" && "$launcher_content" == '3' ]]; then
     wtype -M ctrl -k 3
   # Switch tab 4
   elif [[ "$brave_pchoice" == "brave" && "$launcher_content" == '4' ]]; then
     wtype -M ctrl -k 4
   # Switch tab 5
   elif [[ "$brave_pchoice" == "brave" && "$launcher_content" == '5' ]]; then
     wtype -M ctrl -k 5
   # Switch tab 6
   elif [[ "$brave_pchoice" == "brave" && "$launcher_content" == '6' ]]; then
     wtype -M ctrl -k 6
   # History
   elif [[ "$brave_pchoice" == "brave" && "$launcher_content" == 'history' ]]; then
     wtype -M ctrl -k h
   # Download
   elif [[ "$brave_pchoice" == "brave" && "$launcher_content" == 'download' ]]; then
     wtype -M ctrl -k j
   # Reopen tab
   elif [[ "$brave_pchoice" == "brave" && "$launcher_content" == 'reopen' ]]; then
     wtype -M ctrl -M shift -k t
   # Bookmark manager
   elif [[ "$brave_pchoice" == "brave" && "$launcher_content" == 'manager' ]]; then
     wtype -M ctrl -M shift -k o
   else
     notify-send "Something went wrong!"
   fi

}
export -f brave_palette

#------------------------------------------------------#
# Check if your running wayland (Brave command palette)
#------------------------------------------------------#

check_brave() {

   if [[ "$DESKTOP_SESSION" == 'sway' ]]; then
     brave_palette
   else
     notify-send "Wayland not detected!"
   fi
	
}
export -f check_brave

#------------------------------------------------------#
# Radio (plays radio)
#------------------------------------------------------#

play_mpv() {

killall mpv
echo "$1" | tee "$HOME"/Documents/.temp_radio_stream.txt
temp_cat_file=$(cat "$HOME"/Documents/.temp_radio_stream.txt)
temp_file=$(echo "$temp_cat_file" | awk '{print $NF}')
echo "$temp_file" > "$HOME"/Documents/.lasted_played.txt

notify-send "$temp_file" && mpv --no-video "$temp_file" 
	
}
export -f play_mpv

#------------------------------------------------------#
# Radio
#------------------------------------------------------#

my_radio() {

radio_cat_links=$(cat "$radio_links")
echo "$radio_cat_links" | sort | GTK_THEME="$gtk_theme" yad --list --column="radio" \
--title="Online radio" \
--borders=20 \
--no-markup \
--search-column=1 \
--text="\nDouble click to play radio station\nUse keyword: p to control player\nPress control+f to search\n" \
--text-align="center" \
--height=550 \
--width=900 \
--no-headers \
--buttons-layout="center" \
--separator= \
--dclick-action='/bin/bash -c "play_mpv %s"' \
--button="Close":1 

}
export -f my_radio

#------------------------------------------------------#
# Control music players with playerctl (play last played track)
#------------------------------------------------------#
# What ever program your playing music with add this below into playing function.
#check_last_played=$(playerctl metadata -f '{{xesam:url}}')
#echo "$check_last_played" > "$HOME"/Documents/.lasted_played.txt

lasted_played() {

sed -i 's|file://||' "$HOME"/Documents/.lasted_played.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.lasted_played.txt
sed -i 's|%20| |g' "$HOME"/Documents/.lasted_played.txt
last_played_file=$(cat "$HOME"/Documents/.lasted_played.txt)

   if [[ "$last_played_file" == *"http"* ]]; then
     mpv "$last_played_file"
   else
     notify-send "Something went wrong with lasted_played function!"
   fi
   	
}
export -f lasted_played

#------------------------------------------------------#
# Control music players with playerctl
#------------------------------------------------------#

music_player() {

   # Play
   if [[ "$launcher_content" == "play" ]]; then
     playerctl play-pause
   # Stop
   elif [[ "$launcher_content" == "stop" ]]; then
     playerctl stop
   # Next
   elif [[ "$launcher_content" == "next" ]]; then
     playerctl next
   # Prev
   elif [[ "$launcher_content" == "prev" ]]; then
     playerctl previous
   # Status
   elif [[ "$launcher_content" == "stat" ]]; then
     notify-send "$(playerctl status)"
   # Artist and title
   elif [[ "$launcher_content" == "art" ]]; then
     notify-send "$(playerctl metadata --format '{{xesam:artist}} - {{xesam:title}}')"
   # Create qr code from artist and title
   elif [[ "$launcher_content" == "qr" ]]; then
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     playerctl metadata --format '{{xesam:artist}} - {{xesam:title}}' | \
     tr -d '\n' | qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$qr_code_dir"
     find "$HOME"/Pictures/.temqrcodes/*.png -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw | \
     tee ~/Documents/.keys_player_title.txt
     notification_player=$(head ~/Documents/.keys_player_title.txt)
     qr_code_mnotify=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     notify-send -i "$qr_code_mnotify" "$notification_player"
   elif [[ "$launcher_content" == "last" ]]; then
     lasted_played
   else
     notify-send "Something went wrong!"
   fi
	
}
export -f music_player

#------------------------------------------------------#
# Volume status
#------------------------------------------------------#

volume_status() {

vol_status01=$(amixer -D pulse get Master | tail -1 | awk '{print $5}')
vol_status02=$(amixer -D pulse get Master | tail -1 | awk '{print $6}')

notify-send "Volume: $vol_status01 $vol_status02"
	
}
export -f volume_status

#------------------------------------------------------#
# Open terminal here in thunar directory
#------------------------------------------------------#
# This works only in sway.
# Run this command before you run this function.
# $ xfconf-query -c thunar -n -p /misc-full-path-in-title -t bool -s true

open_ter_here() {

   if [[ "$DESKTOP_SESSION" == 'sway' ]]; then
     choice_ter=$(swaymsg -t get_tree | \
     jq -r '.. | select(.type?) | select(.focused==true).name' | \
     cut -d'/' -f4- | sed 's/^/"/;s/$/"/')
     "$other_terminal" -- /bin/bash -c "cd ~/$choice_ter; $SHELL"
   else
     notify-send "Sway not detected!"
   fi

# Another method
#choice_ter=$(swaymsg -t get_tree | \
#jq -r '.. | select(.type?) | select(.focused==true).name' | \
#cut -d'/' -f4-)
#gnome-terminal --working-directory="$choice_ter"
	
}
export -f open_ter_here

#------------------------------------------------------#
# Change text from uppercase to lowercase
#------------------------------------------------------#

upper_to_lower() {

echo "$launcher_content" | tr '[:upper:]' '[:lower:]' | wl-copy -n

}
export -f upper_to_lower

#------------------------------------------------------#
# Timezone Los Angeles
#------------------------------------------------------#

los_angeles() {

notify-send "$(date --date='TZ="'America/Los_Angeles'"'" $launcher_content")"

}
export -f los_angeles

#------------------------------------------------------#
# Timezone New York
#------------------------------------------------------#

new_york() {

notify-send "$(date --date='TZ="'America/New_York'"'" $launcher_content")"

}
export -f new_york

#------------------------------------------------------#
# Timezone Sydney
#------------------------------------------------------#

sydney() {

notify-send "$(date --date='TZ="'Australia/Sydney'"'" $launcher_content")"

}
export -f sydney

#------------------------------------------------------#
# Yad timezones
#------------------------------------------------------#

yad_timezone() {
	
GTK_THEME="$gtk_theme" yad --list \
--title="" \
--text="<span color='#FFFFFF' font_family='Cascadia Mono' font='18' weight='bold' rise='0pt'>\n$(date)\n</span>" \
--text-align="center" \
--width=700 \
--height=210 \
--separator= \
--column="Double click on item to launch it" \
--buttons-layout="center" \
--borders=10 \
--no-headers \
--center \
--button="exit":1
}
export -f yad_timezone

#------------------------------------------------------#
# Timezones
#------------------------------------------------------#

main_timezones() {

zoneone=$(TZ=America/Los_Angeles date) 
zonetwo=$(TZ=America/New_York date)
zonethree=$(TZ=Australia/Sydney date)
zonefour=$(TZ=Asia/Tokyo date)
zonefive=$(TZ=Europe/London date)
zonesix=$(TZ=Asia/Jerusalem date)

textone="America/Los Angeles"
texttwo="America/New York"
textthree="Australia/Sydney"
textfour="Japan/Tokyo"
textfive="Europe/London"
textsix="Asia/Jerusalem"

com01=$(echo -e "${zoneone} ${textone}")
com02=$(echo -e "${zonetwo} ${texttwo}")
com03=$(echo -e "${zonethree} ${textthree}")
com04=$(echo -e "${zonefour} ${textfour}")
com05=$(echo -e "${zonefive} ${textfive}")
com06=$(echo -e "${zonesix} ${textsix}")

tz_choice=$(echo -e "$com01\n$com02\n$com03\n$com04\n$com05\n$com06\n" | yad_timezone)
[ -z "$tz_choice" ] && exit 0
echo "$tz_choice" | wl-copy -n

}
export -f main_timezones

#------------------------------------------------------#
# Notifications
#------------------------------------------------------#

control_notifications() {

   if [[ "$launcher_content" == 'history' ]]; then
     dunstctl history-pop
   elif [[ "$launcher_content" == 'toggle' ]]; then 
     dunstctl set-paused toggle
   else
     :
   fi

}
export -f control_notifications

#------------------------------------------------------#
# Microphone
#------------------------------------------------------#

my_microphone() {

   if [[ "$launcher_content" == 'on' ]]; then
     pactl set-source-mute @DEFAULT_SOURCE@ 0
   elif [[ "$launcher_content" == 'off' ]]; then
     pactl set-source-mute @DEFAULT_SOURCE@ 1
   elif [[ "$launcher_content" == 'level' ]]; then
     notify-send "$(pacmd list-sources | grep -e 'volume: front-left:' | awk 'NR==2 {print $5}')"
   else
     pactl set-source-volume "$(pactl get-default-source)" "$launcher_content%"
   fi

}
export -f my_microphone

#------------------------------------------------------#
# Zipping files
#------------------------------------------------------#

zip_files() {

rm -f "$HOME"/Documents/.zips/*.*
wl-paste > "$HOME"/Documents/.zipfile.txt
sed -i 's|file://||' "$HOME"/Documents/.zipfile.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.zipfile.txt
zip_text=$(cat "$HOME"/Documents/.zipfile.txt)
echo "$zip_text" | tr -d '\r' | tee "$HOME"/Documents/.zipfile.txt
sed -i 's|%20| |g' "$HOME"/Documents/.zipfile.txt
xargs -L 1 -a "$HOME"/Documents/.zipfile.txt -d '\n' zip -j "$HOME"/Documents/.zips/"$(date +'%d-%m-%Y-%H%M%S')".zip	
cp "$HOME"/Documents/.zips/*.* "$zips_dir"
	
}
export -f zip_files

#------------------------------------------------------#
# Plays videos in mpv via http link
#------------------------------------------------------#

mpv_play() {

   if [[ "$launcher_content" == 'stop' ]]; then
     killall mpv
   else
     mpv "$launcher_content"
   fi
	
}
export -f mpv_play

#------------------------------------------------------#
# Open selected file from (Last modified files)
#------------------------------------------------------#

mod_files_open() {

temp_mod_file=$(head "$HOME"/Documents/.temp_mod_selected_file.txt)
export temp_mod_file
killall yad
xdg-open "$temp_mod_file"
	
}
export -f mod_files_open


#------------------------------------------------------#
# Select file and open in file manager (Last modified files)
#------------------------------------------------------#

open_mod_file_manager() {

killall yad
mod_filemanager=$(head "$HOME"/Documents/.temp_mod_selected_file.txt)
[ -z "$mod_filemanager" ] && exit 0
"$file_manager" "$mod_filemanager" 

}
export -f open_mod_file_manager

#------------------------------------------------------#
# Select file from list (Last modified files)
#------------------------------------------------------#

mod_files_select() {

echo "$1" > "$HOME"/Documents/.temp_mod_selected_file.txt
	
}
export -f mod_files_select

#------------------------------------------------------#
# Last modified files
#------------------------------------------------------#

last_modified_file() {

# Selected directories
dirs3=( ~/Documents/ ~/Downloads/ ~/Videos/ ~/Pictures/ ~/Music/ )

# Documents
mod_choice01=$(find "${dirs3[@]}" -type f \( -iname '*.doc' -o -iname '*.docx' -o -iname '*.pdf' -o -iname '*.ods' -o -iname '*.xlsx' -o -iname '*.odt' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
# Downloads
mod_choice02=$(find ~/Downloads/ -not -path '*/.*' -type f -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
# Images
mod_choice03=$(find "${dirs3[@]}" -type f \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
# Videos
mod_choice04=$(find "${dirs3[@]}" -type f \( -iname '*.mp4' -o -iname '*.mkv' -o -iname '*.webm' -o -iname '*.avi' -o -iname '*.wmv' -o -iname '*.mov' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
# Sounds
mod_choice05=$(find "${dirs3[@]}" -type f \( -iname '*.mp3' -o -iname '*.wav' -o -iname '*.aiff' -o -iname '*.acc' -o -iname '*.flac' -o -iname '*.wma' -o -iname '*.m4a' -o -iname '*.ogg' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")

# Yad settings
echo -e "$mod_choice01\n$mod_choice02\n$mod_choice03\n$mod_choice04\n$mod_choice05" | GTK_THEME="$gtk_theme" yad --list \
--column="mod" \
--title="Last Modified file" \
--text="\nLast Modified files\nDouble click file and press button\n" \
--text-align="center" \
--width=950 \
--height=500 \
--separator= \
--buttons-layout="center" \
--borders=10 \
--no-headers \
--center \
--dclick-action='/bin/bash -c "mod_files_select %s"' \
--button="Open:/bin/bash -c 'mod_files_open'" \
--button="File manager:/bin/bash -c 'open_mod_file_manager'" \
--button="exit":1

}
export -f last_modified_file

#------------------------------------------------------#
# Record audio from microphone
#------------------------------------------------------#

record_mic_audio() {

   if [[ "$launcher_content" == 'stop' ]]; then
     killall arecord
     cp "$HOME"/Music/.yadaudiorec/*.* "$audio_dir"
     notify-send "Audio recording stopped"
   elif [[ "$launcher_content" == 'open' ]]; then
     find "$HOME"/Music/.yadaudiorec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs xdg-open
   else 
     rm -f "$HOME"/Music/.yadaudiorec/*.*
     notify-send "Recording audio only" ; arecord -f S16_LE -t wav "$HOME/Music/.yadaudiorec/audio_$(date '+%d%m%Y-%H%M-%S').wav"
   fi

}
export -f record_mic_audio

#------------------------------------------------------#
# Open apps text file
#------------------------------------------------------#

open_apps_text_file() {

killall yad
xdg-open "$HOME"/Documents/apps.txt
	
}
export -f open_apps_text_file

#------------------------------------------------------#
# Yad settings
#------------------------------------------------------#

_yad() {

GTK_THEME="$gtk_theme" yad --entry \
--title="Run launcher" \
--width=600 \
--text-align="left" \
--image-on-top \
--height=100 \
--text="Enter command to execute.." \
--center \
--editable \
--borders=20 \
--rest="$my_apps_file" \
--buttons-layout="spread" \
--button="gtk-execute:0" \
--button="Keywords:/bin/bash -c 'show_keywords'" \
--button="gtk-file:/bin/bash -c 'open_apps_text_file'" \
--button="gtk-cancel:1"

}
export -f _yad

#------------------------------------------------------#
# Main
#------------------------------------------------------#

main() {
 
# Gui program 
selected=$(_yad) 

# Keyword
bang=$(echo "$selected" | awk '{print $1}')

# Content
launcher_content=$(echo "$selected" | cut -d' ' -f2-)
 
case "$bang" in

   # Youtube search
    'yt')
     youtube_search
     ;;

   # Google translate  
    't')
     google_trans_fr
     ;;

   # Find local files
    'f')
     find_files
     ;;

   # Find local files (history)
    'fh')
     find_files_history
     ;;

   # QR Codes
    'qr')
     qr_codes
     ;;

   # Calculator
    '=')
     my_calculator
     ;;

   # Calculator history
    '=h')
     calculator_history
     ;;

   # Dictionary
    'di')
     my_dictionary
     ;;

   # Shutdown
    'shutdown')
     yadshutdown
     ;;

   # Restart
    'restart')
     yadreboot
     ;;

   # Mute volume toggle
    'mute')
     mute_volume
     ;;

   # Set the volume
    'v')
     set_volume
     ;;

   # Text to speech (speak)
    'spk')
     tts_speak
     ;;

   # Text to speech (file)
    'text')
     tts_file
     ;;

   # Text to speech (kill)
    'ttsk')
     tts_kill
     ;;

   # Kill processes with killall
    'kill')
     kill_app
     ;;

   # Kill processes with pkill
    'ki')
     pkill_app
     ;;

   # Spell checker
    'sp')
     spell_checker
     ;;

   # Password generator
    'pass')
     pass_gen
     ;;

   # QR Code image notification
    'view')
     qr_code_notification
     ;;
     
   # Google search
    'g')
     google_search
     ;;

   # Create PDF
    'pdf')
     create_pdf
     ;;

   # PDF open file
    'pdfo')
     pdf_open
     ;;

   # PDF print
    'pdfp')
     pdf_print
     ;;

   # Record screen (screencast)
    'rec')
     record_screen
     ;;

   # Stop screen recording (screencast)
    'stop')
     stop_rec
     ;;

   # Play screen recording (screencast)
    'play')
     play_rec
     ;;

   # Take fullscreen screenshot
    'full')
     full_shot
     ;;

   # Select screenshot
    'select')
     select_shot
     ;;

   # Open fullscreen screenshot
    'open')
     open_shot
     ;;

   # Delay screenshot
    'delay')
     delay_shot
     ;;

   # Lock machine
    'lock')
     yadlock
     ;;

   # Exec terminal commands
    '#')
     cli_commands
     ;;

   # Resize image
    'res')
     resize_image
     ;;

   # Emoji
    'em')
     emoji_list
     ;;

   # One line note
    'n')
     one_line_note
     ;;

   # One line note history
    'nh')
     one_line_note_history
     ;;

   # Decode qr code 
    'qrd')
     decode_qr_code
     ;;

   # Keywords
    'keys')
     /bin/bash -c show_keywords
     ;;

   # Color Picker
    'pick')
     color_picker
     ;;

   # RGB to HEX
    'rgb')
     call_rgb_to_hex
     ;;

   # HEX to RGB
    'hex')
     hex_to_rgb
     ;;

   # Brave command palette
    'br')
     check_brave
     ;;

   # Control music players with playerctl
    'p')
     music_player
     ;;

   # Volume status
    'status')
     volume_status
     ;;

   # Open terminal here
    'here')
     open_ter_here
     ;;

   # Change uppercase text to lowercase
    'lo')
     upper_to_lower
     ;;

   # Timezone los angeles
    'la')
     los_angeles
     ;;

   # Timezone new york
    'ny')
     new_york
     ;;

   # Timezone in Sydney
    'sy')
     sydney
     ;;

   # All timezones
    'zones')
     main_timezones
     ;;

   # Notifications
    'not')
     control_notifications
     ;;

   # Microphone
    'mic')
     my_microphone
     ;;

   # Zip files
    'com')
     zip_files
     ;;

   # Plays videos in mpv via http link
    'pv')
     mpv_play
     ;;

   # Last modified files
    'mod')
     last_modified_file
     ;;

   # Record from microphone
    'rmic')
     record_mic_audio
     ;;

   # Radio
    'radio')
     my_radio
     ;;

   # Duckduckgo web search
    'ddg')
     duckduckgo
     ;;

   # Reddit web search
    'rd')
     reddit
     ;;

   # Download video
    'do')
     download_video
     ;;

   # Wikipedia web search
    'wik')
     wikipedia
     ;;

         
   *)
    # Move to already open app
    if [[ "$(pidof "$bang")" || "$(pgrep -f "$bang" > /dev/null)" ]]; then
        app_running
    # Open app
    else
        new_instance
    fi 
     ;;
     
esac

}
export -f main
main

#------------------------------------------------------#
# Unset variables and functions
#------------------------------------------------------#

unset _yad
unset main
unset google_search
unset download_video
unset youtube_search
unset find_files
unset file_results
unset open_file
unset yadreboot
unset yadshutdown
unset mute_volume
unset open_file_manager
unset file_manager
unset browser
unset my_dictionary
unset spell_checker
unset spellcheck_results
unset qr_codes
unset qr_code_notification
unset my_calculator
unset my_calculator_copy
unset calculator_clipboard
unset calculator_history
unset tts_speak
unset tts_file
unset tts_kill
unset kill_app
unset pkill_app
unset set_volume
unset create_pdf
unset pdf_open
unset pdf_print
unset check_sound
unset full_shot
unset open_shot
unset yadlock
unset select_shot
unset delay_shot
unset delay_time
unset record_screen
unset stop_rec
unset play_rec
unset cli_commands
unset myterminal
unset pass_gen
unset resize_image
unset resize_image
unset keyword_help
unset color1
unset color2
unset color3
unset end
unset show_keywords
unset app_running
unset new_instance
unset emoji_list
unset one_line_note
unset one_line_note_copy
unset one_line_note_select
unset one_line_note_history
unset decode_qr_code
unset decode_qr_code_select
unset decode_qr_code_copy
unset open_apps_text_file
unset color_picker
unset rgb_to_hex
unset call_rgb_to_hex
unset hex_to_rgb
unset brave_palette
unset music_player
unset volume_status
unset open_ter_here
unset upper_to_lower
unset los_angeles
unset new_york
unset sydney
unset check_brave
unset main_timezones
unset yad_timezone
unset control_notifications
unset my_microphone
unset zip_files
unset mpv_play
unset last_modified_file
unset mod_files_select
unset mod_files_open
unset temp_mod_file
unset open_mod_file_manager
unset record_mic_audio
unset lasted_played
unset my_radio
unset play_mpv
unset radio_links
unset emoji_text
unset duckduckgo
unset wikipedia
unset reddit
unset my_printer
unset screenshots_dir
unset pdf_dir
unset screencast_dir
unset qr_code_dir
unset other_terminal
unset zips_dir
unset audio_dir
unset gtk_theme
