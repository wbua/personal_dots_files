#!/bin/bash

# Keybindings
color1=$(tput setaf 191)
export color1
#
color2=$(tput setaf 45)
#
color3=$(tput setaf 196)
# Color end
end=$(tput sgr0)
export end


my_text=$(echo "${color3}Software${end}

${color1}Check for updates first:${end} sudo apt update
${color1}Update system:${end} sudo apt full-upgrade
${color1}Install software:${end} sudo apt install package_name
${color1}Install software:${end} sudo snap install package_name
${color1}Install deb file:${end} sudo dpkg -i package_name
${color1}Remove software:${end} sudo apt remove package_name
${color1}Remove software:${end} sudo snap remove package_name
${color1}Remove deb file:${end} sudo dpkg -r package_name
${color1}Search for software:${end} apt search package_name
${color1}Search for software:${end} snap find package_name

${color3}Basic commands${end}

${color2}Terminal is case sensitive.${end}

${color1}list directory contents:${end} ls
${color1}list directory contents, hidden files:${end} ls -a
${color1}Clear screen:${end} clear
${color1}Change directory:${end} cd Documents
${color1}Go back to previous directory:${end} cd ..
${color1}Go back to home directory:${end} cd
${color1}Working directory:${end} pwd
${color1}System info:${end} uname -a
${color1}Create directory without spaces:${end} mkdir name
${color1}Create directory with spaces:${end} \"my name\"
${color1}User ID:${end} whoami
${color1}System monitor:${end} htop
${color1}Who is logged on:${end} w
${color1}Kill all processes:${end} killall program_name
${color1}Text editor:${end} nano
${color1}Remove file:${end} rm file
${color1}Remove directory:${end} rm -r dir
${color1}View contents of text file:${end} less text
${color1}Create text file:${end} touch text1.txt
${color1}Copy file:${end} cp file1 file2
${color1}Move file:${end} mv file1 ~/Download/
${color1}Rename file:${end} mv file1 file2
${color1}Find files:${end} find ~ -type f -iname \"*talia*\"
${color1}Find directories:${end} find ~ -type d -iname \"*videos*\"
${color1}Copy file:${end} ctrl+shift+c
${color1}Paste file:${end} ctrl+shift+v
${color1}Freeze cursor:${end} ctrl+s
${color1}Unfreeze cursor:${end} ctrl+q
${color1}Kill runing command:${end} ctrl+c
${color1}Suspend command:${end} ctrl+z
${color1}View suspended commands:${end} jobs
${color1}Bring suspended command to the foreground:${end} fg
${color1}It your terminal becomes buggy:${end} reset
${color1}Open file:${end} xdg-open file
${color1}Open program from terminal:${end} setsid program_name
${color1}Run program with administrator privileges:${end} sudo program_name
${color1}Close the terminal:${end} exit
${color1}Search manuals for criteria:${end} apropos permission
${color1}list block devices:${end} lsblk
${color1}Find out extra information about commands:${end} man ls
${color1}Find out extra information about commands:${end} ls --help
${color1}Shred file:${end} shred -zvun 5 file
${color1}Network:${end} ip addr
${color1}Date and time:${end} date
${color1}Test if you have an internet connection:${end} nm-online

${color2}scroll up with mouse wheel to see all information.${end}
")

gnome-terminal -- /bin/bash -c "echo \"$my_text\"; $SHELL"

