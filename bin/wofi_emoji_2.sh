#!/bin/bash
choice="$(wofi --dmenu -p "Emoji:" < "$HOME/Documents/mainprefix/emoji-data.txt" | cut -d' ' -f1 | tr -d '\n')"
[[ -n "$choice" ]] && wtype "$choice"
