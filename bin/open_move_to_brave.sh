#!/bin/bash

     active_app=$(swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class):\(.name)"' \
     | grep -i "brave" | cut -d: -f2)

   if [[ "$active_app" == *"Brave"* || "$active_app" == *"brave"* ]]; then
     swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class):\(.name)"' \
     | grep -i "brave" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' focus
   else
    /bin/bash -c "brave"
   fi 
	
