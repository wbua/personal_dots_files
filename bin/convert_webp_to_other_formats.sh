#!/bin/bash

# Program command: convert_webp_to_other_formats.sh
# Description: Convert image to png.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 09-12-2024
# Program_license: GPL 3.0
# Dependencies: yad, imagemagick, webp

# User settings
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#--------------------------------------------------------#
# Error checking
#--------------------------------------------------------#

set -e

#--------------------------------------------------------#
# User preferences
#--------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Image preview size
IMAGE_PREVIEW_SIZE="512x512"
export IMAGE_PREVIEW_SIZE

#--------------------------------------------------------#
# Create directories
#--------------------------------------------------------#

   if [ -d "$HOME"/Downloads ]; then
     :
   else
     mkdir "$HOME"/Downloads
   fi

   if [ -d "$HOME"/Downloads/.cwtof_temp ]; then
     :
   else
     mkdir "$HOME"/Downloads/.cwtof_temp
   fi

#--------------------------------------------------------#
# Finds the most recent image in download directory
#--------------------------------------------------------#

# Full path to file
choice01=$(find ~/Downloads/ -not -path '*/.*' -type f \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -printf '%T@ %p\n' | \
sort -n | tail -1 | cut -f2- -d" ")   
export choice01

# Filename without path
choice02=$(find ~/Downloads/ -not -path '*/.*' -type f \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -printf '%T@ %p\n' | \
sort -n | tail -1 | cut -f2- -d" " | xargs -0 basename)   
export choice02

#--------------------------------------------------------#
# Converts image to smaller size for image preview
#--------------------------------------------------------#

convert "$choice01" -resize "$IMAGE_PREVIEW_SIZE" "$HOME"/Downloads/.cwtof_temp/recent_image.jpg

#--------------------------------------------------------#
# Yad list dialog
#--------------------------------------------------------#

# Yad dialog
main_choice=$(GTK_THEME="$MY_GTK_THEME" yad --form \
--title="Convert downloaded image" \
--text-align="center" \
--image-on-top \
--image="$(find ~/Downloads/.cwtof_temp/ -type f -print0 | xargs -0 ls -tr | tail -n 1)" \
--borders=10 \
--width=400 \
--height=400 \
--separator="#" \
--field="File: $choice02":LBL "" \
--field="Enter file extension: jpg or png\nNote: webp images can only use the png extension":LBL "" \
--field="File extension: ":CE \
--button="Ok":0 \
--button="Exit":1)
[ -z "$main_choice" ] && exit 0

#--------------------------------------------------------#
# Converts images based upon file extension entered
#--------------------------------------------------------#

EXT01=$(echo "$main_choice" | awk -F '#' '{print $3}')
export EXT01

   if [[ "$(file --mime-type "$choice01")" == *"webp"* ]]; then
     dwebp "$choice01" -o ~/Downloads/"${choice02%.*}"."$EXT01" || notify-send "Something went wrong!" && exit 1    
   else
     convert "$choice01" ~/Downloads/"${choice02%.*}"."$EXT01" || notify-send "Something went wrong!" && exit 1
   fi

#--------------------------------------------------------#
# Unset variables
#--------------------------------------------------------#

unset EXT01
unset main_choice
unset choice01
unset choice02
unset MY_GTK_THEME
unset IMAGE_PREVIEW_SIZE
