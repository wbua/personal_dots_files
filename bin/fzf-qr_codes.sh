#!/bin/bash

# Program command: fzf-qr_codes.sh
# Description: Decodes all qr codes in directory and displays it in fzf.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 04-01-2024
# Program_license: GPL 3.0
# Dependencies: yad, xclip, zbar-tools, qrencode

#-------------------------------------------------------#
# Software preferences
#-------------------------------------------------------#

# Browser
browser="brave"
export browser

# File manager
file_manager="thunar"
export file_manager

# Image editor
image_viewer="gthumb"
export image_viewer

#-------------------------------------------------------#
# Creates files and directories
#-------------------------------------------------------#

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/.temqrcodes ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes
   fi

   if [ -d "$HOME"/Pictures/qr_codes ]; then
     :
   else
     mkdir "$HOME"/Pictures/qr_codes
   fi

   if [ -d "$HOME"/Pictures/.ocr/ ]; then
    :
   else
     mkdir "$HOME"/Pictures/.ocr/
   fi

   if [ -f "$HOME"/Pictures/.qrcode_dir.txt ]; then
     :
   else
     touch "$HOME"/Pictures/.qrcode_dir.txt
   fi

#-------------------------------------------------------#
# Colors
#-------------------------------------------------------#

# Keybindings
color1=$(tput setaf 10)
export color1
# Color end
end=$(tput sgr0)
export end

#-------------------------------------------------------#
# Create qr codes list
#-------------------------------------------------------#
     
# Text file where all qr codes text is sent
file_dir="$HOME/Pictures/.qrcode_dir.txt"

# Decodes all qr code png's in directory to text file
zbarimg -q --raw "$HOME"/Pictures/qr_codes/*.png > "$file_dir"

# Removes whitespace from text file
sed -i '/^[[:space:]]*$/d' "$file_dir"

# Send all filenames in qr codes directory to text file
ls "$HOME"/Pictures/qr_codes/ > "$HOME"/Pictures/.qrfilenames.txt

# Merge text files
paste "$HOME"/Pictures/.qrfilenames.txt "$HOME"/Pictures/.qrcode_dir.txt > "$HOME"/Pictures/.qrmixed.txt

#-------------------------------------------------------#
# Create qr codes list when creating new qr code
#-------------------------------------------------------#

qr_codes_list() {

# Text file where all qr codes text is sent
file_dir="$HOME/Pictures/.qrcode_dir.txt"

# Decodes all qr code png's in directory to text file
zbarimg -q --raw "$HOME"/Pictures/qr_codes/*.png > "$file_dir"

# Removes whitespace from text file
sed -i '/^[[:space:]]*$/d' "$file_dir"

# Send all filenames in qr codes directory to text file
ls "$HOME"/Pictures/qr_codes/ > "$HOME"/Pictures/.qrfilenames.txt

# Merge text files
paste "$HOME"/Pictures/.qrfilenames.txt "$HOME"/Pictures/.qrcode_dir.txt > "$HOME"/Pictures/.qrmixed.txt

pkill -f ~/bin/fzf-qr_codes.sh

}
export -f qr_codes_list

#-------------------------------------------------------#
# Create Qr Code from clipboard
#-------------------------------------------------------#

qrcode_from_clip() {

rm -f "$HOME"/Pictures/.temqrcodes/*.png
wl-paste | tr -d '\n' | qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
qr_codes_list
pkill -f ~/bin/fzf-qr_codes.sh

}
export -f qrcode_from_clip

#-------------------------------------------------------#
# Type text to create qr code
#-------------------------------------------------------#

create_qr_code() {
	
choice=$(echo "" | fzf --print-query \
--padding=0% \
--border="none" \
--header="
Press Esc to exit fzf

" \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'create qr code by typing> ' \
--cycle \
--info=inline \
--reverse)
[[ -z "$choice" ]] && exit

rm -f "$HOME"/Pictures/.temqrcodes/*.png
echo "$choice" | tr -d '\n' | qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
qr_codes_list
pkill -f ~/bin/fzf-qr_codes.sh
     
}
export -f create_qr_code

#-------------------------------------------------------#
# Web link
#-------------------------------------------------------#

web_search() {

qrcode_file=$(cat "$HOME"/Documents/.qr_code_mixed_temp.txt | awk '{print $NF}')
[[ -z "$qrcode_file" ]] && exit
"$browser" "$qrcode_file"
pkill -f ~/bin/fzf-qr_codes.sh

}
export -f web_search

#-------------------------------------------------------#
# Open in file manager
#-------------------------------------------------------#

open_in_file_manager() {

qrcode_file=$(cat "$HOME"/Documents/.qr_code_mixed_temp.txt | awk '{print $1}')
[[ -z "$qrcode_file" ]] && exit
setsid "$file_manager" "$HOME"/Pictures/qr_codes/"$qrcode_file" >/dev/null 2>&1 & disown
pkill -f ~/bin/fzf-qr_codes.sh

}
export -f open_in_file_manager

#-------------------------------------------------------#
# Google search
#-------------------------------------------------------#

google_search() {

qrcode_file=$(cat "$HOME"/Documents/.qr_code_mixed_temp.txt | \
sed 's|&||' | awk '{$1="";print $0}' | xargs | sed 's/^/"/;s/$/"/')
[[ -z "$qrcode_file" ]] && exit
search="https://www.google.co.uk/search?q={}"
echo "$qrcode_file" | xargs -I{} "$browser" "$search"
pkill -f ~/bin/fzf-qr_codes.sh

}
export -f google_search

#-------------------------------------------------------#
# Open file with path
#-------------------------------------------------------#

open_file() {

cat "$HOME"/Documents/.qr_code_mixed_temp.txt | \
awk '{$1="";print $0}' | sed 's| ||' | tee "$HOME"/Documents/.qr_code_mixed_temp.txt
choice=$(cat "$HOME"/Documents/.qr_code_mixed_temp.txt | xargs -0 -d '\n')
[[ -z "$choice" ]] && exit 0 
setsid xdg-open "$choice" >/dev/null 2>&1 & disown 
sleep 0.3
pkill -f ~/bin/fzf-qr_codes.sh

}
export -f open_file

#-------------------------------------------------------#
# Open qr code png file
#-------------------------------------------------------#

open_png_file() {

qrcode_file=$(cat "$HOME"/Documents/.qr_code_mixed_temp.txt | awk '{print $1}')
[[ -z "$qrcode_file" ]] && exit
setsid "$image_viewer" "$HOME"/Pictures/qr_codes/"$qrcode_file" >/dev/null 2>&1 & disown 
pkill -f ~/bin/fzf-qr_codes.sh

}
export -f open_png_file

#-------------------------------------------------------#
# Kill script
#-------------------------------------------------------#

kill_script() {

pkill -f ~/bin/fzf-qr_codes.sh
	
}
export -f kill_script

#-------------------------------------------------------#
# Main
#-------------------------------------------------------#

main() {

mixed_file=$(cat "$HOME"/Pictures/.qrmixed.txt)
qrcode_list=$(echo "$mixed_file" | sort | fzf --reverse \
--ansi \
--cycle \
--info=inline \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:#660020,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--header="
Open png ${color1}enter${end} | Type to create qr code ${color1}F1${end} | Open file ${color1}F5${end} 
Google search ${color1}F4${end} | Open in file manager ${color1}F3${end} Web link ${color1}F7${end}
Kill script ${color1}F9${end} | qr code from clipboard ${color1}F2${end}

" \
--prompt 'select qr code> ' \
--bind='F9:execute(kill_script {})' \
--bind='F7:execute-silent(echo -n {} | tee "$HOME"/Documents/.qr_code_mixed_temp.txt)+execute(web_search {})' \
--bind='F2:execute-silent(echo -n {} | tee "$HOME"/Documents/.qr_code_mixed_temp.txt)+execute(qrcode_from_clip {})' \
--bind='F3:execute-silent(echo -n {} | tee "$HOME"/Documents/.qr_code_mixed_temp.txt)+execute(open_in_file_manager {})' \
--bind='F4:execute-silent(echo -n {} | tee "$HOME"/Documents/.qr_code_mixed_temp.txt)+execute(google_search {})' \
--bind='F5:execute-silent(echo -n {} | tee "$HOME"/Documents/.qr_code_mixed_temp.txt)+execute(open_file {})' \
--bind='F1:execute(create_qr_code {})' \
--bind='enter:execute-silent(echo -n {} | tee "$HOME"/Documents/.qr_code_mixed_temp.txt)+execute(open_png_file {})')
[ -z "$qrcode_list" ] && exit 0

echo "$qrcode_list" | tee "$HOME"/Documents/.qr_code_mixed_temp.txt
qrcode_file_main=$(cat "$HOME"/Documents/.qr_code_mixed_temp.txt | awk '{print $1}')
[ -z "$qrcode_file_main" ] && exit 0

echo "$qrcode_list" > /dev/null
pkill -f ~/bin/fzf-qr_codes.sh

}
export -f main
main

#-------------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------------#

unset main
unset image_viewer
unset browser
unset file_manager
unset color1
unset end
unset choice
unset create_qr_code
unset qr_codes_list
unset web_search
unset copy_to_clip
unset open_in_file_manager
unset google_search
unset open_file
unset copy_png_to_clip
unset open_png_file
unset kill_script
unset qrcode_from_clip
