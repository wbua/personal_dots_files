#!/bin/bash


     active_app=$(swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class):\(.name)"' \
     | grep -i "firefox" | cut -d: -f2)

   if [[ "$active_app" == *"firefox"* ]]; then
     swaymsg -t get_tree \
       | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class):\(.name)"' \
       | grep -i "firefox" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' focus
   else
    /bin/bash -c "firefox"
   fi
