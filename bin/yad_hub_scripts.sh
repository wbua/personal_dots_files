#!/bin/bash

# Program command: yad_hub-scripts.sh
# Description: This shows all my yad scripts.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 08-06-2024
# Program_license: GPL 3.0
# Dependencies: yad

#--------------------------------------------------#
# Error checking
#--------------------------------------------------#

set -e

#--------------------------------------------------#
# Colors
#--------------------------------------------------#

# Title
COLOR1=$(printf '%s' "<span color='#FFFFFF' font_family='JetBrainsMono Nerd Font' font='16' rise='0pt'>")
export COLOR1
# Keys
COLOR2=$(printf '%s' "<span color='#16FF00' font_family='JetBrainsMono Nerd Font' font='16' rise='0pt'>")
export COLOR2
# Color end
END=$(printf '%s' "</span>")
export END

#--------------------------------------------------#
# PDF creator
#--------------------------------------------------#

pdf_app() {

killall yad
setsid bash ~/bin/pdf_creator.sh >/dev/null 2>&1 & disown
	
}
export -f pdf_app

#--------------------------------------------------#
# Run launcher
#--------------------------------------------------#

run_launcher() {

killall yad
setsid bash ~/bin/yad_keywords_launcher.sh >/dev/null 2>&1 & disown
	
}
export -f run_launcher

#--------------------------------------------------#
# Convert images
#--------------------------------------------------#

convert_app() {

killall yad
setsid bash ~/bin/yad_convert.sh >/dev/null 2>&1 & disown
	
}
export -f convert_app

#--------------------------------------------------#
# Color picker
#--------------------------------------------------#

color_picker() {

killall yad
setsid bash ~/bin/yad_hex_color_codes.sh >/dev/null 2>&1 & disown

}
export -f color_picker

#--------------------------------------------------#
# Volume
#--------------------------------------------------#

volume_app() {

killall yad
setsid bash ~/bin/yad-volume.sh >/dev/null 2>&1 & disown
	
}
export -f volume_app

#--------------------------------------------------#
# Notes
#--------------------------------------------------#

notes_app() {

killall yad
setsid bash ~/bin/yad-oneline_notes_auto.sh >/dev/null 2>&1 & disown

}
export -f notes_app

#--------------------------------------------------#
# QR Codes
#--------------------------------------------------#

qr_codes() {

killall yad
setsid bash ~/bin/qr_code_list.sh >/dev/null 2>&1 & disown

}
export -f qr_codes

#--------------------------------------------------#
# Video cutter
#--------------------------------------------------#

video_cutter() {

killall yad
setsid bash ~/bin/video_cutter.sh >/dev/null 2>&1 & disown

}
export -f video_cutter

#--------------------------------------------------#
# News
#--------------------------------------------------#

news_app() {

killall yad
setsid bash ~/bin/yad_bbc_news.sh >/dev/null 2>&1 & disown
	
}
export -f news_app

#--------------------------------------------------#
# Screenshots tool
#--------------------------------------------------#

screenshots_app() {

killall yad
setsid bash ~/bin/yad_screenshots.sh >/dev/null 2>&1 & disown

}
export -f screenshots_app

#--------------------------------------------------#
# Video downloader
#--------------------------------------------------#

video_downloader() {

killall yad
setsid bash ~/bin/videodownloadergtk.sh >/dev/null 2>&1 & disown

}
export -f video_downloader

#--------------------------------------------------#
# File search
#--------------------------------------------------#

file_search() {

killall yad
setsid bash ~/bin/yad_search.sh >/dev/null 2>&1 & disown

}
export -f file_search

#--------------------------------------------------#
# Radio
#--------------------------------------------------#

radio_app() {

killall yad
setsid bash ~/bin/yad-radio-wayland.sh >/dev/null 2>&1 & disown

}
export -f radio_app

#--------------------------------------------------#
# Screen recorder
#--------------------------------------------------#

record_screen() {

killall yad
setsid bash ~/bin/yad_record.sh >/dev/null 2>&1 & disown

}
export -f record_screen

#--------------------------------------------------#
# Power menu
#--------------------------------------------------#

power_menu() {

killall yad
setsid bash ~/.config/i3blocks/scripts/power >/dev/null 2>&1 & disown

}
export -f power_menu

#--------------------------------------------------#
# Emoji
#--------------------------------------------------#

emoji_app() {

killall yad
setsid bash ~/bin/yad_emoji.sh >/dev/null 2>&1 & disown

}
export -f emoji_app

#--------------------------------------------------#
# Sound Cloud player
#--------------------------------------------------#

sound_cloud() {

killall yad
setsid bash ~/bin/sc_dj_mixes.sh >/dev/null 2>&1 & disown
	
}
export -f sound_cloud

#--------------------------------------------------#
# Sound effects
#--------------------------------------------------#

sound_effects() {

killall yad
setsid bash ~/bin/yad_ambient_sounds.sh >/dev/null 2>&1 & disown

}
export -f sound_effects

#--------------------------------------------------#
# Yad settings
#--------------------------------------------------#

GTK_THEME="alt-dialog22" yad --form \
--columns=2 \
--title="Yad gui apps" \
--width=800 \
--height=600 \
--borders=20 \
--field="<b>PDF Creator</b>":fbtn '/bin/bash -c "pdf_app"' \
--field="<b>Run launcher ${COLOR2}win period${END}</b>":fbtn '/bin/bash -c "run_launcher"' \
--field="<b>Covert images ${COLOR2}win i${END}</b>":fbtn '/bin/bash -c "convert_app"' \
--field="<b>Volume</b>":fbtn '/bin/bash -c "volume_app"' \
--field="<b>Notes</b>":fbtn '/bin/bash -c "notes_app"' \
--field="<b>Video cutter</b>":fbtn '/bin/bash -c "video_cutter"' \
--field="<b>News ${COLOR2}altgr n${END}</b>":fbtn '/bin/bash -c "news_app"' \
--field="<b>Screenshots tool</b>":fbtn '/bin/bash -c "screenshots_app"' \
--field="<b>Video downloader</b>":fbtn '/bin/bash -c "video_downloader"' \
--field="<b>File search</b>":fbtn '/bin/bash -c "file_search"' \
--field="<b>Radio</b>":fbtn '/bin/bash -c "radio_app"' \
--field="<b>Screen recorder</b>":fbtn '/bin/bash -c "record_screen"' \
--field="<b>Power Menu</b>":fbtn '/bin/bash -c "power_menu"' \
--field="<b>QR Codes ${COLOR2}win q${END}</b>":fbtn '/bin/bash -c "qr_codes"' \
--field="<b>Color picker ${COLOR2}altgr i${END}</b>":fbtn '/bin/bash -c "color_picker"' \
--field="<b>Emoji's</b>":fbtn '/bin/bash -c "emoji_app"' \
--field="<b>SoundCloud player</b>":fbtn '/bin/bash -c "sound_cloud"' \
--field="<b>Sound effects ${COLOR2}win a${END}</b>":fbtn '/bin/bash -c "sound_effects"' \
--button="Exit":1

#--------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------#

unset pdf_app
unset run_launcher
unset convert_app
unset color_picker
unset volume_app
unset notes_app
unset qr_codes
unset video_cutter
unset news_app
unset screenshots_app
unset video_downloader
unset file_search
unset radio_app
unset record_screen
unset power_menu
unset emoji_app
unset sound_cloud
unset COLOR1
unset COLOR2
unset END
unset sound_effects
