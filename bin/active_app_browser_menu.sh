#!/bin/bash

# Program command: active_app_browser_menu.sh
# Description: Menu options for i3blocks browser_app module.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 13-10-2024
# Program_license: GPL 3.0
# Dependencies: yad

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#---------------------------------------------------------#
# Error checking
#---------------------------------------------------------#

set -e

#---------------------------------------------------------#
# User preferences
#---------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME     

# Browser
MY_BROWSER="$MAIN_BROWSER"
export MY_BROWSER

# Search engine
SEARCH_ENGINE="$ACTIVE_APP_SEARCH_ENGINE"
export SEARCH_ENGINE

# Web search
MY_WEB_SEARCH="$ACTIVE_APP_WEB_SEARCH"
export MY_WEB_SEARCH

#---------------------------------------------------------#
# Colors
#---------------------------------------------------------#

# Highlight
COLOR01="<span color='#45FF00' font_family='Monospace' font='14'>"
export COLOR01
# Color end
END="</span>"
export END

#---------------------------------------------------------#
# Bring window to current workspace
#---------------------------------------------------------#

bring_window() {

killall yad
bash ~/bin/bring_browser_to_current.sh
	
}
export -f bring_window

#---------------------------------------------------------#
# Google search
#---------------------------------------------------------#

google_search() {

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     web_search="https://www.google.co.uk/search?q={}"
     echo "$my_choice" | xargs -I{} "$MY_BROWSER" "$web_search"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     web_search="https://www.google.co.uk/search?q={}"
     echo "$my_choice" | xargs -I{} "$MY_BROWSER" "$web_search"
   fi

}
export -f google_search

#---------------------------------------------------------#
# Duckduckgo search
#---------------------------------------------------------#

duckduckgo_search() {

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     duckduckgo_search="https://duckduckgo.com/?q={}"
     echo "$my_choice" | xargs -I{} "$MY_BROWSER" "$duckduckgo_search"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     duckduckgo_search="https://duckduckgo.com/?q={}"
     echo "$my_choice" | xargs -I{} "$MY_BROWSER" "$duckduckgo_search"
   fi

}
export -f duckduckgo_search

#---------------------------------------------------------#
# Bing search
#---------------------------------------------------------#

bing_search() {

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     bing_search="https://www.bing.com/search?q={}"
     echo "$my_choice" | xargs -I{} "$MY_BROWSER" "$bing_search"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     bing_search="https://www.bing.com/search?q={}"
     echo "$my_choice" | xargs -I{} "$MY_BROWSER" "$bing_search"
   fi

}
export -f bing_search

#---------------------------------------------------------#
# Close window
#---------------------------------------------------------#

close_window() {

   if [[ "$MY_BROWSER" == "google-chrome" ]]; then
     killall yad
     pkill chrome
   else
     killall yad
     killall "$MY_BROWSER" || pkill "$MY_BROWSER"
   fi 
	
}
export -f close_window

#---------------------------------------------------------#
# New tab search
#---------------------------------------------------------#

new_browser_tab() {

   if [[ "$(grep MAIN_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "brave" ]]; then
     killall yad
     brave --app-url "$SEARCH_ENGINE"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep MAIN_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "firefox" ]]; then
     killall yad
     firefox --new-tab "$SEARCH_ENGINE"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep MAIN_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "google-chrome" || "$(grep MAIN_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == *"${MY_BROWSER^}"* ]]; then
     killall yad
     google-chrome --app-url "$SEARCH_ENGINE"   
   else
     notify-send "Browser not supported!"
   fi
     
}
export -f new_browser_tab

#---------------------------------------------------------#
# Change new tab search
#---------------------------------------------------------#

change_new_tab() {

killall yad
field_choice=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--width=500  \
--height=130 \
--separator= \
--text-align="center" \
--text="\nCurrent new tab: ${COLOR01}${SEARCH_ENGINE}${END}\nExamples: duckduckgo.com, google.com, bing.com\nEnter new tab choice...\n" \
--button="_OK":0 \
--button="_Exit":1)
[ -z "$field_choice" ] && exit 0

sed -i "s|.*\(ACTIVE_APP_SEARCH_ENGINE=\).*|ACTIVE_APP_SEARCH_ENGINE=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
	
}
export -f change_new_tab

#---------------------------------------------------------#
# Change browser in sway_user_preferences.sh file
#---------------------------------------------------------#

change_browser() {

killall yad
field_choice=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--width=500  \
--height=130 \
--separator= \
--text-align="center" \
--text="\nCurrent browser: ${COLOR01}${MY_BROWSER}${END}\nEnter your browser choice...\n" \
--button="_OK":0 \
--button="_Exit":1)
[ -z "$field_choice" ] && exit 0

sed -i "s|.*\(MAIN_BROWSER=\).*|MAIN_BROWSER=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
	
}
export -f change_browser

#---------------------------------------------------------#
# Browser web search
#---------------------------------------------------------#

change_web_search() {

killall yad
field_choice=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--width=500  \
--height=130 \
--separator= \
--text-align="center" \
--text="\nCurrent web search: ${COLOR01}${MY_WEB_SEARCH}${END}\nExamples: google, duckduckgo, bing\nEnter web search choice...\n" \
--button="_OK":0 \
--button="_Exit":1)
[ -z "$field_choice" ] && exit 0

sed -i "s|.*\(ACTIVE_APP_WEB_SEARCH=\).*|ACTIVE_APP_WEB_SEARCH=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
	
}
export -f change_web_search

#---------------------------------------------------------#
# Browser status
#---------------------------------------------------------#

browser_status() {

   if [[ "$(pidof "$MY_BROWSER")" || "$(pgrep -f "$MY_BROWSER")" ]]; then
     echo "${COLOR01}on${END}"
   else
     echo "${COLOR01}off${END}"
   fi  
	
}
export -f browser_status

#---------------------------------------------------------#
# Yad dialog
#---------------------------------------------------------#

my_choice=$(GTK_THEME="$MY_GTK_THEME" yad --form \
--column=1 \
--width=500 \
--height=550 \
--borders=50 \
--text-align="center" \
--text="Browser options\nCurrent browser: ${COLOR01}${MY_BROWSER}${END}\nBrowser is $(browser_status)\nNew tab: ${COLOR01}${SEARCH_ENGINE}${END}\n" \
--align="center" \
--field="<b>Bring window</b>":fbtn '/bin/bash -c "bring_window"' \
--field="<b>Change browser</b>":fbtn '/bin/bash -c "change_browser"' \
--field="<b>Change web search</b>":fbtn '/bin/bash -c "change_web_search"' \
--field="<b>Close window</b>":fbtn '/bin/bash -c "close_window"' \
--field="<b>Change new tab</b>":fbtn '/bin/bash -c "change_new_tab"' \
--field="<b>New tab search</b>":fbtn '/bin/bash -c "new_browser_tab"' \
--field=" ":LBL " " \
--field="${MY_WEB_SEARCH} search":LBL " " \
--field="":CE \
--separator= \
--button="Exit":1)
[ -z "$my_choice" ] && exit 0
export my_choice

#---------------------------------------------------------#
# Check web search engine
#---------------------------------------------------------#

web_search_engine() {

   if [[ "$(grep ACTIVE_APP_WEB_SEARCH= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "google" ]]; then
     google_search
   elif [[ "$(grep ACTIVE_APP_WEB_SEARCH= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "duckduckgo" ]]; then
     duckduckgo_search
   elif [[ "$(grep ACTIVE_APP_WEB_SEARCH= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "bing" ]]; then
     bing_search
   else
     :
   fi
	
}
export -f web_search_engine

#---------------------------------------------------------#
# Main
#---------------------------------------------------------#

   if [[ -z "$my_choice" ]]; then
     :
   else
     web_search_engine
   fi          

#---------------------------------------------------------#
# Unset variables and functions
#---------------------------------------------------------#

unset MY_GTK_THEME
unset my_choice
unset MY_BROWSER
unset selected_choice
unset web_search
unset bring_window
unset google_search
unset close_window
unset new_browser_tab
unset change_browser
unset browser_status
unset COLOR01
unset END
unset duckduckgo_search
unset web_search_engine
unset MY_WEB_SEARCH
unset bing_search
unset change_web_search
unset change_new_tab
