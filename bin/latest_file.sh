#!/bin/bash

dirs=( ~/Documents/ ~/Downloads/ ~/Music/ ~/Pictures/ ~/Videos/ ~/bin/ )

choice=$(find "${dirs[@]}" -type f -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" " | rofi -dmenu -i -p '')

#echo "$choice" | xargs -0 -d '\n' xdg-open

thunar "$choice"
