#!/bin/bash


#-------------------------------------------------------#
# Source file for config
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/prefixrofi_config.sh

#-------------------------------------------------------#
# File actions
#-------------------------------------------------------#

# Show last file selected by finder
action_last_file() {

my_actions_file=$(cat "$HOME"/Documents/.temp_actions.txt)
echo ""
echo "${my_actions_file##*/}"
    	
}

# Open with selected programs
selected_menu() {
	
echo "$image_editor"
echo "$image_viewer"
echo "$audio_player"
echo "$sound_editor"
echo "$video_player"
echo "$other_video_player"
echo "$file_manager"
echo "$texteditor"
echo "$office_suite"
echo "$other_office_suite"
	
}

# Menu for actions
menu3() {

echo "open ${color6}opens the file${end}"
echo "open-with ${color6}select program to open file${end}"
echo "file ${color6}opens file in file manager${end}"
echo "copy ${color6}copies the file${end}"
echo "move ${color6}moves the file${end}"	
echo "clip ${color6}copies file path to clipboard${end}"
echo "previous ${color6}previous file searches${end}"
echo "none ${color6}perform no action${end}"
echo "quicklink ${color6}use quicklinks to open file quickly${end}"	
		
}

# Perform actions on files
rofi_actions() {
	
choice=$(menu3 | _rofi14 | awk '{print $1}')
[[ -z "$choice" ]] && exit
	
case "$choice" in

 'copy')
   actions_file=$(cat "$HOME"/Documents/.temp_actions.txt)
   dir_choice=$(find "${dirs[@]}" -maxdepth 9 -type d | _rofi14)
   echo "$dir_choice" | tee "$HOME"/Documents/.dir_actions.txt
   cp "$actions_file" "$dir_choice"
   ;;
 'open')
   cat "$HOME"/Documents/.temp_actions.txt | xargs -0 -d '\n' xdg-open
   ;;
  
 'move')
   actions_file=$(cat "$HOME"/Documents/.temp_actions.txt)
   dir_choice=$(find "${dirs[@]}" -maxdepth 9 -type d | _rofi14)
   echo "$dir_choice" | tee "$HOME"/Documents/.dir_actions.txt
   mv "$actions_file" "$dir_choice"
   ;;
   
 'file')
   cat "$HOME"/Documents/.temp_actions.txt | xargs -0 -d '\n' "$file_manager"
   ;;
 
 'clip')
   cat "$HOME"/Documents/.temp_actions.txt | xclip -selection clipboard
   ;;
   
 'open-with')
   choice_file=$(cat "$HOME"/Documents/.temp_actions.txt)
   choice=$(selected_menu | _rofi14)
   [[ -z "$choice" ]] && exit
   if [[ "$choice" == "$image_editor" ]]; then
     "$image_editor" "$choice_file"
   elif [[ "$choice" == "$image_viewer" ]]; then
     "$image_viewer" "$choice_file"
   elif [[ "$choice" == "$audio_player" ]]; then
     "$audio_player" "$choice_file"
   elif [[ "$choice" == "$sound_editor" ]]; then
     "$sound_editor" "$choice_file"
   elif [[ "$choice" == "$video_player" ]]; then
     "$video_player" "$choice_file"
   elif [[ "$choice" == "$other_video_player" ]]; then
     "$other_video_player" "$choice_file"
   elif [[ "$choice" == "$file_manager" ]]; then
     "$file_manager" "$choice_file"
   elif [[ "$choice" == "$texteditor" ]]; then
     "$texteditor" "$choice_file"
   elif [[ "$choice" == "$office_suite" ]]; then
     "$office_suite" "$choice_file"
   elif [[ "$choice" == "$other_office_suite" ]]; then
   "$other_office_suite" "$choice_file"
   else
    :
   fi
   ;;
   
 'previous')
  cat "$HOME"/Documents/.temp_actions.txt  | tee -a "$HOME"/Documents/.all_actions.txt
  gawk -i inplace '!a[$0]++' "$HOME"/Documents/.all_actions.txt
  sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.all_actions.txt
  choice=$(tac "$HOME"/Documents/.all_actions.txt  | _rofi2)
  [[ -z "$choice" ]] && exit
  echo "$choice" | tee "$HOME"/Documents/.temp_actions.txt
  rofi_actions
  ;;
   
 'quicklink')
   sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/quick_links.txt
   cat "$HOME"/Documents/.temp_actions.txt | sed 's|^|loc:|' | tee -a "$HOME"/Documents/mainprefix/quick_links.txt
   ;;
  
 'none')
   :
   ;;
  
    
  *)
  echo "Something went wrong!" | rofi2 || exit 1
  ;;

esac	
	
}

#-------------------------------------------------------#
# Run launchers
#-------------------------------------------------------#

_rofi14() {
	
rofi -dmenu -i -p ''
	
}

_rofi() {
	
rofi -dmenu -i -p '' -kb-custom-2 "alt+c"
	
}

#-------------------------------------------------------#
# Main
#-------------------------------------------------------#

main() {

choice=$(find "$HOME"/Documents/ -type f -iname '*' | _rofi)
exit_status=$?

   if [[ $exit_status == "11" ]]; then
   echo "$choice" | tee "$HOME"/Documents/.temp_actions.txt
   rofi_actions
   elif [[ "$choice" == '#' ]]; then
   rofi_actions
   else
   echo "$choice" | xargs -0 -d '\n' xdg-open
   fi

}
main
