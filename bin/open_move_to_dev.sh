#!/bin/bash

# Program command: open_move_to_dev.sh
# Description: Opens program or move to already open instance of program.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 16-09-2024
# Program_license: GPL 3.0
# Dependencies: swaymsg, jq

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-----------------------------------------------------------#
# Error checking
#-----------------------------------------------------------#

set -e

#-----------------------------------------------------------#
# User preferences
#-----------------------------------------------------------#

MY_DEV="$MAIN_DEV"

#-----------------------------------------------------------#
# Create files and directories
#-----------------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -f "$HOME"/Documents/.set_workspace_dev.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_dev.txt
     echo "1" > "$HOME"/Documents/.set_workspace_dev.txt
   fi

#-----------------------------------------------------------#
# Check text file is empty
#-----------------------------------------------------------#

check_text_file=$(cat "$HOME"/Documents/.set_workspace_dev.txt)

   if [[ -z "$check_text_file" ]]; then
     echo "1" > "$HOME"/Documents/.set_workspace_dev.txt
   else
     :
   fi

#-----------------------------------------------------------#
# Checks if text file contains workspace number
#-----------------------------------------------------------#

empty_number() {

set_choice=$(cat "$HOME"/Documents/.set_workspace_dev.txt)

   if [[ -z "$set_choice" ]]; then
     /bin/bash -c "$MY_DEV"
   else
     swaymsg workspace "$set_choice"
     setsid /bin/bash -c "$MY_DEV" >/dev/null 2>&1 & disown && exit
   fi
	
}

#-----------------------------------------------------------#
# Check if set workspaces variable is enabled
#-----------------------------------------------------------#

dev_workspace() {

   if [[ "$(grep SPECIFIC_APP_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     empty_number 
   elif [[ "$(grep SPECIFIC_APP_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     /bin/bash -c "$MY_DEV"
   else
     :
   fi	
}

#-----------------------------------------------------------#
# Open program or move to already open instance
#-----------------------------------------------------------#
   
     active_app=$(swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"' \
     | grep -i "$MAIN_DEV" | cut -d: -f2)
 
   if [[ "$active_app" == *"$MAIN_DEV"* || "$active_app" == *"${MAIN_DEV^}"* ]]; then
     swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"' \
     | grep -i "$MAIN_DEV" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' focus
   else
     dev_workspace
   fi 
   
