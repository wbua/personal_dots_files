#!/bin/bash

#!/bin/bash

# Program command: powermenu.sh
# Description: Power menu to shutdown, reboot, lock and logout.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 19-09-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad, google material icons font, slock, xdotool, gtk.css

#-------------------------------------------------------#
# Greeting
#-------------------------------------------------------#

greet=$(printf '%s\n' "$USER")
export greet

#-------------------------------------------------------#
# Confirm prompt
#-------------------------------------------------------#

# Reboot
_reboot() {

   killall yad
   if yad --question --width=400 --height=100 --borders=10 --text "Reboot, are you sure" 
   then
     poweroff --reboot
   else
     exit 1
   fi
   
}
export -f _reboot

# Shutdown 
_shutdown() {
 
   killall yad
   if yad --question --width=400 --height=100 --borders=10 --text "Shutdown, are you sure" 
   then
     poweroff --poweroff
   else
     exit 1
   fi
   
}
export -f _shutdown

# Lock 
_lock() {
 
   killall yad
   if yad --question --width=400 --height=100 --borders=10 --text "Lock, are you sure" 
   then
     swaylock -c '#000000'
   else
     exit 1
   fi
   
}
export -f _lock

# Logout
_logout() {

   killall yad
   if yad --question --width=400 --height=100 --borders=10 --text "Logout, are you sure" 
   then
     wtype -M win -M shift -k e
   else
     exit 1
   fi
   
}
export -f _logout

#-------------------------------------------------------#
# Yad settings
#-------------------------------------------------------#

_yad() {

GTK_THEME="Adwaita" yad --form --width=700 --height=400 --title="Power Menu" \
--text="<span color='#FFFFFF' font_family='Monospace' font='18'>Goodbye</span>\n<span color='red' font_family='Monospace' font='25'>${greet}</span>\n" \
--text-align="center" \
--center \
--timeout=10 \
--timeout-indicator=top \
--borders=35 \
--columns=8 \
--buttons-layout="center" \
--field="":LBL "" \
--field="":LBL "" \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='80'></span></b>":fbtn "/bin/bash -c '_shutdown'" \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='80'></span></b>":fbtn "/bin/bash -c '_reboot'" \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='80'></span></b>":fbtn "/bin/bash -c '_lock'" \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='80'></span></b>":fbtn "/bin/bash -c '_logout'" \
--field="":LBL "" \
--field="":LBL "" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='25'></span>":1 


}
export -f _yad
_yad

#-------------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------------#

# Functions
unset _yad
unset _main
unset _menu
unset _shutdown
unset _reboot
unset _lock
unset _logout

# Variables
unset greet
unset logout
unset lock
unset reboot
unset shutdown
