#!/bin/bash

# Program command: yad_keybindings.sh
# Description: Shows sway config keyboard shortcuts.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 23-08-2024
# Program_license: GPL 3.0
# Dependencies: yad

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#---------------------------------------------#
# User preferences
#---------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Browser
MY_BROWSER="$MAIN_BROWSER"
export MY_BROWSER

# Bookmarks text file
BOOKMARKS_FILE="$MAIN_BOOKMARKS_FILE"
export BOOKMARKS_FILE

# Text editor
TEXT_EDITOR="$MAIN_TEXT_EDITOR"
export TEXT_EDITOR

#---------------------------------------------#
# Create files and directories
#---------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ "$BOOKMARKS_FILE" == "disable" ]; then
     :
   elif [ -f "$BOOKMARKS_FILE" ]; then
     :
   else
     notify-send "Bookmarks text file does not exist!" && exit 1
   fi

#---------------------------------------------#
# Create my array
#---------------------------------------------#

mapfile -t my_bookmarks < "$BOOKMARKS_FILE"

#---------------------------------------------#
# Remove whitespace
#---------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$BOOKMARKS_FILE"

#---------------------------------------------#
# Open bookmarks text file in text editor
#---------------------------------------------#

open_in_texteditor() {

killall yad

   if [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     setsid "$TEXT_EDITOR" "$BOOKMARKS_FILE" && bash ~/bin/switch_to_texteditor.sh >/dev/null 2>&1 & disown && exit      
   elif [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     setsid "$TEXT_EDITOR" "$BOOKMARKS_FILE" >/dev/null 2>&1 & disown && exit
   else
     :
   fi
	
}
export -f open_in_texteditor

#---------------------------------------------#
# Add bookmark
#---------------------------------------------#

add_bookmark() {

   if [[ "$BOOKMARKS_FILE" == "disable" ]]; then
     notify-send "Bookmarks has been disable" && exit 1
   else
     killall yad
     choice=$(yad --form \
     --text-align="center" \
     --text="\nAdd new bookmark\n" \
     --border=30 \
     --width=600 \
     --height=300 \
     --separator="#" \
     --field="Name: ":CE \
     --field="Link: ":CE \
     --button="_Exit":1)
     [[ -z "$choice" ]] && exit

     field1=$(echo "$choice" | awk -F '#' '{print $1}')
     field2=$(echo "$choice" | awk -F '#' '{print $2}')

     echo "${field1} - ${field2}" | tee -a "$BOOKMARKS_FILE"
   fi
	
}
export -f add_bookmark

#---------------------------------------------#
# Yad dialog
#---------------------------------------------#

_yad() {

   if [[ "$BOOKMARKS_FILE" == "disable" ]]; then
     notify-send "Bookmarks has been disable" && exit 1
   else
     GTK_THEME="$MY_GTK_THEME" yad --list \
     --column="bookmarks" \
     --search-column=1 \
     --regex-search \
     --separator= \
     --title="Bookmarks" \
     --text="\nBookmarks\nJust start typing to search\nWhile in search mode use up and down arrows for next and prev search\n" \
     --text-align="center" \
     --borders=20 \
     --no-markup \
     --no-headers \
     --width=900 \
     --height=600 \
     --buttons-layout="center" \
     --button="_Add bookmark:/bin/bash -c 'add_bookmark'" \
     --button="_File:/bin/bash -c 'open_in_texteditor'" \
     --button="_Exit":1
   fi
	
}
export -f _yad

#---------------------------------------------#
# Open and move to browser
#---------------------------------------------#

open_move_to_browser() {

   if [[ "$BOOKMARKS_FILE" == "disable" ]]; then
     notify-send "Bookmarks has been disable" && exit 1
   else
     echo "$choice" | wl-copy -n
     "$MY_BROWSER" "$choice"
     bash ~/bin/switch_to_browser.sh
   fi   
	
}
export -f open_move_to_browser

#---------------------------------------------#
# Open in browser
#---------------------------------------------#

open_in_browser() {

   if [[ "$BOOKMARKS_FILE" == "disable" ]]; then
     notify-send "Bookmarks has been disable" && exit 1
   else
     echo "$choice" | wl-copy -n
     "$MY_BROWSER" "$choice"
   fi
	
}
export -f open_in_browser

#---------------------------------------------#
# Main
#---------------------------------------------#

text_choice=$(printf "%s\n" "${my_bookmarks[@]}" | _yad)
[ -z "$text_choice" ] && exit 0
export text_choice

choice=$(echo "$text_choice" | awk '{print $NF}')
export choice
[ -z "$choice" ] && exit 0

killall yad

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     open_move_to_browser
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     open_in_browser
   fi

#---------------------------------------------#
# Unset variables and functions
#---------------------------------------------#

unset _yad
unset text_choice
unset choice
unset MY_BROWSER
unset MY_GTK_THEME
unset COLOR2
unset END
unset add_bookmark
unset BOOKMARKS_FILE
unset open_move_to_browser
unset open_in_browser
unset TEXT_EDITOR
unset open_in_texteditor
