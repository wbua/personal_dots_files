#!/bin/bash

# Program command: fzf-pdf-wayland.sh
# Description: Open and creates pdf files
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 27-12-2023
# Program_license: GPL 3.0
# Dependencies: fzf, libreoffice

#------------------------------------------#
# Software preferences
#------------------------------------------#

# Printer
my_printer="Brother_DCP_J1050DW"
export my_printer

# File manager
file_manager="thunar"
export file_manager

# Pdf viewer
pdf_viewer="evince"
export pdf_viewer

#------------------------------------------#
# Create files and directories
#------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Documents/pdf ]; then
     :
   else
     mkdir "$HOME"/Documents/pdf
   fi
   
   if [ -d "$HOME"/Documents/.pdftemp ]; then
     :
   else
     mkdir "$HOME"/Documents/.pdftemp
   fi

#------------------------------------------#
# Colors
#------------------------------------------#

# Keybindings
color1=$(tput setaf 15)
export color1
# Color end
end=$(tput sgr0)
export end

#------------------------------------------#
# File search locations
#------------------------------------------#

dirs=( ~/Documents/ ~/Pictures/ ~/Videos/ ~/Downloads/ ~/Music/ )
export dirs

#------------------------------------------#
# Open last created pdf in file manager
#------------------------------------------#

pdf_dir() {

chosen5=$(find "$HOME"/Documents/.pdftemp/ -type f -iname '*.*' | sort | cut -d '/' -f6-)
export chosen5
[ -z "$chosen5" ] && exit 0
setsid "$file_manager" ~/Documents/pdf/"$chosen5" >/dev/null 2>&1 & disown	
sleep 0.3
pkill -f ~/bin/fzf-pdf-wayland.sh
	
}
export -f pdf_dir

#------------------------------------------#
# Open last created pdf
#------------------------------------------#

pdf_open() {

   if [[ -z "$(ls -A "$HOME"/Documents/.pdftemp)" ]]; then	
     notify-send "Pdf directory is empty"
     pkill -f ~/bin/fzf-pdf-wayland.sh
   else
     choice=$(find "$HOME"/Documents/.pdftemp/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     export -f choice
     setsid "$pdf_viewer" "$choice" >/dev/null 2>&1 & disown
     sleep 0.3
     pkill -f ~/bin/fzf-pdf-wayland.sh
   fi
	
}
export -f pdf_open

#------------------------------------------#
# Print last pdf created
#------------------------------------------#

pdf_print() {

   if [[ -z "$(ls -A "$HOME"/Documents/.pdftemp)" ]]; then
     notify-send "Pdf directory is empty"
     pkill -f ~/bin/fzf-pdf-wayland.sh
   else
     setsid lp -d "$my_printer" "$HOME"/Documents/.pdftemp/*.pdf >/dev/null 2>&1 & disown
     sleep 0.3
     pkill -f ~/bin/fzf-pdf-wayland.sh
   fi	
	
}
export -f pdf_print

#------------------------------------------#
# Type path to file to create pdf
#------------------------------------------#

pdf_out() {
	
choice=$(echo "" | fzf --reverse \
--ansi \
--print-query \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--info=inline \
--pointer=">" \
--prompt 'create pdf> ' \
--header='')
export choice
[ -z "$choice" ] && exit 0

rm -f "$HOME"/Documents/.pdftemp/*.*
libreoffice --headless --convert-to pdf "$choice" --outdir "$HOME"/Documents/.pdftemp/ && notify-send "PDF created"
setsid cp "$HOME"/Documents/.pdftemp/* "$HOME"/Documents/pdf/ >/dev/null 2>&1 & disown
pkill -f ~/bin/fzf-pdf-wayland.sh
	
}
export -f pdf_out

#------------------------------------------#
# Kill script
#------------------------------------------#

kill_script() {

pkill -f ~/bin/fzf-pdf-wayland.sh

}
export -f kill_script

#------------------------------------------#
# Menu
#------------------------------------------#

menu() {

echo "dir ${color1} open last created pdf in file manager${end}"
echo "open ${color1} open last created pdf${end}"
echo "create ${color1} paste path to file to create pdf${end}"
echo "print ${color1} print the last pdf created${end}"
	
}
export -f menu

#------------------------------------------#
# Main
#------------------------------------------#

main() {

choice=$(menu | fzf --reverse \
--ansi \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--info=inline \
--pointer=">" \
--prompt 'pdf> ' \
--header="
To paste press control+shift+v
${color1}F9${end} kill script

" \
--bind='F9:execute(kill_script {})' | awk '{print $1}')
export choice
[ -z "$choice" ] && exit 0

case "$choice" in

 'dir')
   pdf_dir   
 ;;

 'open')
   pdf_open
 ;;

 'create')
   pdf_out
 ;;

 'print')
   pdf_print
 ;;

 *)
   echo "Something went wrong!" || exit 1
 ;;

esac
	
}
export -f main
main

#------------------------------------------#
# Unset variables and functions
#------------------------------------------#

unset main
unset choice
unset menu
unset pdf_out
unset pdf_print
unset pdf_open
unset dirs
unset pdf_viewer
unset file_manager
unset my_printer
unset chosen5
unset kill_script
unset color1
unset end
