#!/bin/bash

set -e

#---------------------------------------------------#
# Sourcing files
#---------------------------------------------------#

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Color config
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#---------------------------------------------------#
# User preferences
#---------------------------------------------------#

# Background color
MY_COLOR_BG="$COLOR_BG"

# Icon color
MY_COLOR_ICON="$COLOR_ICON"

# Geany directory
GEANY_DIR="$HOME/.config/geany"

# Geany file
GEANY_FILE="geany.css"

#---------------------------------------------------#
# Create files and directories
#---------------------------------------------------#

   if [ -d "$GEANY_DIR" ]; then
     :
   else
     mkdir "$GEANY_DIR"
   fi
   
   if [ -f "$GEANY_DIR"/"$GEANY_FILE" ]; then
     :
   else
     touch "$GEANY_DIR"/"$GEANY_FILE"
   fi

#---------------------------------------------------#
# Create the geany css file
#---------------------------------------------------#
   
cat <<EOF > "$GEANY_DIR"/"$GEANY_FILE"

/* Active tab styles */
tab:checked,
tab:checked .label {
    background-color: $MY_COLOR_ICON;
    color: black;
}

/* Menubar background and text color */
menubar {
    background-color: $MY_COLOR_BG;
    color: white;
}

/* Button bar background and text color */
toolbar {
    background-color: $MY_COLOR_BG;
    color: white;
}

/* Menubar dropdowns background and text color */
menubar menu, menubar menuitem {
    background-color: $MY_COLOR_BG;
    color: white;
}

notebook {
    background-color: $MY_COLOR_BG;
    border: $MY_COLOR_BG;
    
}

/* Highlight color for menu items when hovered */
menubar menuitem:hover {
    background-color: $MY_COLOR_ICON;
    color: black;
}

/* Inactive tab styles */
tab,
tab .label {
    color: white;
}

EOF
