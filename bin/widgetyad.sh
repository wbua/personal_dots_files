#!/bin/bash
# shellcheck disable=SC2090
# shellcheck disable=SC2089

# Program command: widgetyad.sh
# Description: Shows many widgets. Used with polybar.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 14-07-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad, pactl, audacious, playerctl, mpv, mpv-mpris, maim, xclip, material icons, custom gtk.css,
# imagemagick, amixer , zenity, slock, xdotool

#-------------------------------------------------------#
# Colors for menu items (pango markup)
#-------------------------------------------------------#

# Song artist for audacious
color1="<span color='#FFFFFF' font_family='Inter' font='11' weight='bold' rise='0pt'>"
export color1
# Song title for audacious
color2="<span color='#FFFFFF' font_family='Inter' font='10' weight='bold' rise='0pt'>"
export color1
# On
color3="<span color='#20FF00' font_family='Material Icons' font='28' weight='bold' rise='0pt'>"
export color3
# Off
color4="<span color='#FF002D' font_family='Material Icons' font='28' weight='bold' rise='0pt'>"
export color4
end="</span>"
export end

#--------------------------------------------------------------------#
# Creates picture directory
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

#--------------------------------------------------------------------#
# This is where the full size screenshots are stored
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Pictures/yad_screenshots2 ]; then
     :
   else
     mkdir "$HOME"/Pictures/yad_screenshots2
   fi

#--------------------------------------------------------------------#
# This is where the temp thumbnails for previews are stored
#--------------------------------------------------------------------#

   if [ -d "$HOME"/.cvssgtk2 ]; then
     :
   else
     mkdir "$HOME"/.cvssgtk2
   fi

#--------------------------------------------------------------------#
# This is where full size screenshots are stored temporary
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Pictures/.tempfullscrns2 ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempfullscrns2
   fi

#--------------------------------------------------------------------#
# Change playlists for audacious
#--------------------------------------------------------------------#

audacheckpl () {

audapcone=$(audtool --current-playlist)
export audapcone 
 
   if [[ "$audapcone" == *"0"* ]]; then
     audtool --set-current-playlist 1
   elif [[ "$audapcone" == *"1"* ]]; then
     audtool --set-current-playlist 2
   elif [[ "$audapcone" == *"2"* ]]; then
     audtool --set-current-playlist 3
   elif [[ "$audapcone" == *"3"* ]]; then
     audtool --set-current-playlist 1
   else
     :
   fi
	
}
export -f audacheckpl 

#--------------------------------------------------------------------#
# Currently playing
#--------------------------------------------------------------------#

currently_playing() {
   
   if pgrep -x "audacious" > /dev/null
   then  
     echo "${color1}$(playerctl --player audacious metadata -f "{{artist}}" | cut -c 1-30 | tr "&" "+")${end}"
     echo "${color2}$(playerctl --player audacious metadata -f "{{title}}" | cut -c 1-30 | tr "&" "+")${end}"   
   else
     :
   fi
     	
}
export -f currently_playing

#--------------------------------------------------------------------#
# Check microphone is on or off
#--------------------------------------------------------------------#

check_mic() {
	
   if [[ "$(pactl get-source-mute @DEFAULT_SOURCE@ | awk -F ':' '{print $2}' | sed 's/ //')" == 'no' ]]; then
     echo "${color3}${end}"
   else
     echo "${color4}${end}"
   fi
	
}
export -f check_mic

#--------------------------------------------------------------------#
# Change microphone to on or off
#--------------------------------------------------------------------#

change_mic() {

   if [[ "$(pactl get-source-mute @DEFAULT_SOURCE@ | awk -F ':' '{print $2}' | sed 's/ //')" == 'no' ]]; then
     killall yad
     pactl set-source-mute @DEFAULT_SOURCE@ 1
     widgetyad.sh
   else
     killall yad
     pactl set-source-mute @DEFAULT_SOURCE@ 0
     widgetyad.sh
   fi
     
}
export -f change_mic

#--------------------------------------------------------------------#
# Lofi currently playing
#--------------------------------------------------------------------#

lofi_current() {

  playerctl --player mpv metadata -f "{{title}}" | cut -c 1-24 | tr "&" "+"
	
}
export -f lofi_current

#--------------------------------------------------------------------#
# open calendar
#--------------------------------------------------------------------#

yadhubcalen () {

killall yad
gnome-calendar
	
}
export -f yadhubcalen 

#--------------------------------------------------------------------#
# Takes a fullscreen screenshot
#--------------------------------------------------------------------#

fullscreenshots () {
	
    killall yad
    rm -f "$HOME"/Pictures/.tempfullscrns2/*.png
    rm -f "$HOME"/.cvssgtk2/*.png
    sleep 1
    maim --hidecursor | tee "$HOME/Pictures/.tempfullscrns2/scn_$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
    convert "$HOME"/Pictures/.tempfullscrns2/*.png -resize 320x320 "$HOME"/.cvssgtk2/current_screenshot.png
    cp "$HOME"/Pictures/.tempfullscrns2/*.png "$HOME"/Pictures/yad_screenshots2/
    widgetyad.sh
}
export -f fullscreenshots

#--------------------------------------------------------------------#
# Takes a fullscreen screenshot delayed 5 seconds
#--------------------------------------------------------------------#

fullscreenshotsdelay () {
	
    killall yad
    rm -f "$HOME"/Pictures/.tempfullscrns2/*.png
    rm -f "$HOME"/.cvssgtk2/*.png
    sleep 1
    maim --hidecursor --delay=5 | tee "$HOME/Pictures/.tempfullscrns2/scn_$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
    convert "$HOME"/Pictures/.tempfullscrns2/*.png -resize 320x320 "$HOME"/.cvssgtk2/current_screenshot.png
    cp "$HOME"/Pictures/.tempfullscrns2/*.png "$HOME"/Pictures/yad_screenshots2/
    widgetyad.sh
}
export -f fullscreenshotsdelay


#--------------------------------------------------------------------#
# Open last taken screenshot 
#--------------------------------------------------------------------#

opensrnyadhub () {

killall yad ; find "$HOME"/Pictures/.tempfullscrns2/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open

}
export -f opensrnyadhub 

#--------------------------------------------------------------------#
# Volume down 
#--------------------------------------------------------------------#

voldownyad () {

amixer set Master 5%- unmute
	
}
export -f voldownyad

#--------------------------------------------------------------------#
# Volume up 
#--------------------------------------------------------------------#

volupyad () {

amixer set Master 5%+ unmute
	
}
export -f volupyad 

#--------------------------------------------------------------------#
# Volume up 
#--------------------------------------------------------------------#

yadnotitrack () {

playerctl --player mpv metadata -f "{{title}}" | xclip -selection clipboard

}
export -f yadnotitrack 

#--------------------------------------------------------------------#
# Check volume
#--------------------------------------------------------------------#

check_volume() {
	
   if [[ "$(amixer -D pulse | grep "Front Left: Playback" | awk '{print $NF}')" == '[on]' ]]; then
     echo "${color3}${end}"
   else 
     echo "${color4}${end}"
   fi  
 
}
export -f check_volume

#--------------------------------------------------------------------#
# Power lock 
#--------------------------------------------------------------------#

yadlock () {
if zenity --question --text "Are you sure" 
then
     slock
else
     exit 1
 fi
}

export -f yadlock

#--------------------------------------------------------------------#
# Power logout 
#--------------------------------------------------------------------#

yadlogout () {
if zenity --question --text "Are you sure" 
then
     xdotool key super+shift+q
else
     exit 1
 fi
}

export -f yadlogout

#--------------------------------------------------------------------#
# Power reboot 
#--------------------------------------------------------------------#

yadreboot () {
if zenity --question --text "Are you sure" 
then
     poweroff --reboot
else
     exit 1
 fi
}

export -f yadreboot

#--------------------------------------------------------------------#
# Power shutdown 
#--------------------------------------------------------------------#

yadshutdown () {
if zenity --question --text "Are you sure" 
then
     poweroff --poweroff
else
     exit 1
 fi
}

export -f yadshutdown

#--------------------------------------------------------------------#
# Clock box
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog2" yad --form --no-buttons --align="center" --text-align="center" --title="Clock" \
--field="<span color='#FF5801' font_family='Inter' font='15' weight='bold'>Welcome</span>":LBL "" \
--text="\n<span color='#ffffff' font_family='Inter' font='18'>$(date | awk '{print $4}' | xargs)</span>
<span color='#ffffff' font_family='Inter' font='12'>$(date | awk '{print $1} {print $2} {print $3}' | xargs)</span>" --posx=715 \
--posy=465 --width=269 --height=200 \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='25' weight='bold'></span></b>":fbtn '/bin/bash -c "yadhubcalen"' &

#--------------------------------------------------------------------#
# Audacious player box
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog2" yad --form --text-info --width=250 --height=253 --image-on-top --borders=0 --buttons-layout="center" --columns=3 \
--posx=715 --posy=200 \
--title="Audacious player" \
--text-align="center" \
--align="center" \
--text="$(currently_playing)" \
--field="<span color='#ffffff' font_family='Material Icons' font='25'></span>":BTN "/bin/bash -c 'killall yad ; audacious --rew ; widgetyad.sh'" \
--field="25%":BTN "bash -c 'audtool --playback-seek 25'" \
--field="<span color='#ffffff' font_family='Material Icons' font='40'></span>":BTN "/bin/bash -c 'killall yad ; audacious -H --play-pause ; widgetyad.sh'" \
--field="50%":BTN "bash -c 'audtool --playback-seek 50'" \
--field="<span color='#ffffff' font_family='Material Icons' font='25'></span>":BTN "/bin/bash -c 'killall yad ; audacious --fwd ; widgetyad.sh'" \
--field="75%":BTN "bash -c 'audtool --playback-seek 75'" \
--button="<span color='#ffffff' font_family='Material Icons' font='25'></span>:/bin/bash -c 'audacheckpl'" \
--button="<span color='#ffffff' font_family='Material Icons' font='25'></span>:/bin/bash -c 'killall yad ; sleep 1 ; widgetyad.sh'" \
--button="<span color='#ffffff' font_family='Material Icons' font='25'></span>:/bin/bash -c 'killall yad ; audacious --stop ; widgetyad.sh'" &

#--------------------------------------------------------------------#
# Screenshot tool box
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog2" yad --form --width=335 --height=253 --posx=998 --posy=200 --no-buttons --columns=3 --buttons-layout="center" --image-on-top \
--text-align="center" --title="Screenshot tool" \
--image="$(find "$HOME"/.cvssgtk2/ -type f -print0 | xargs -0 ls -tr | tail -n 1)" \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='25' weight='bold'></span></b>":fbtn '/bin/bash -c "fullscreenshots"' \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='29' weight='bold'></span></b>":fbtn '/bin/bash -c "fullscreenshotsdelay"' \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='25' weight='bold'></span></b>":fbtn '/bin/bash -c "opensrnyadhub"' &

#--------------------------------------------------------------------#
# Uptime box
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog2" yad --form --no-buttons --align="center" --text-align="center" --title="Uptime" \
--text="\n<span color='#FF22F4' font_family='Inter' font='15' weight='bold'>uptime: </span><span color='#FFFFFF' font_family='Inter' font='12' weight='bold'>$(uptime | awk '{print $3}' | tr "," " ")</span>" \
--posx=715 --posy=681 --width=270 --height=70 &

#--------------------------------------------------------------------#
# Power off box
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog2" yad --form --columns=4 --no-buttons --title="Power menu" \
--text-align="center" --text="\n<span color='#11E7EE' font_family='Inter' font='14' weight='bold'>Power</span>\n\n" \
--posx=998 --posy=465 --width=333 --height=200 \
--field="<span color='#ffffff' font_family='Material Icons' font='30'></span>":BTN "/bin/bash -c 'killall yad ; yadlock'" \
--field="<span color='#ffffff' font_family='Material Icons' font='30'></span>":BTN "/bin/bash -c 'killall yad ; yadlogout'" \
--field="<span color='#ffffff' font_family='Material Icons' font='30'></span>":BTN "/bin/bash -c 'killall yad ; yadreboot'" \
--field="<span color='#ffffff' font_family='Material Icons' font='30'></span>":BTN "/bin/bash -c 'killall yad ; yadshutdown'" &

#--------------------------------------------------------------------#
# Volume box
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog2" yad --form --align="center" --columns=3 --no-buttons --buttons-layout="center" --text-align="center" \
--posx=1000 --posy=680 --width=331 --height=71 \
--field="<span color='#ffffff' font_family='Material Icons' font='30'> </span>":BTN "/bin/bash -c 'voldownyad'" \
--field="<span color='#ffffff' font_family='Material Icons' font='30'>$(check_volume)</span>":BTN "/bin/bash -c 'killall yad ; amixer -D pulse set Master 1+ toggle ; widgetyad.sh'" \
--field="<span color='#ffffff' font_family='Material Icons' font='30'></span>":BTN "/bin/bash -c 'volupyad'" &

#--------------------------------------------------------------------#
# Microphone box
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog2" yad --form --text-info --width=270 --no-buttons --height=100 --image-on-top --borders=10 --buttons-layout="end" --columns=3 \
--posx=430 --posy=466 \
--buttons-layout="center" \
--title="Microphone" \
--text="<span color='#EAFF00' font_family='Inter' font='15' weight='bold'>Microphone</span>" \
--text-align="center" \
--align="center" \
--field="":LBL "" \
--field="<b><span color='#ffffff' font_family='Inter' font='14'>$(check_mic)</span></b>":fbtn '/bin/bash -c "change_mic"' \
--field="":LBL "" \
--button="<span color='#ffffff' font_family='Material Icons' font='14'></span>":1 & 

#--------------------------------------------------------------------#
# Lofi radio box
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog2" yad --form --no-buttons --align="center" --text-align="center" --columns=3 --title="Radio" \
--text="\n<span color='#FF00AB' font_family='Inter' font='16' weight='bold'>Lofi music</span>\n\n$(lofi_current)\n\n" \
--posy=200 --posx=430 --width=271 --height=253 \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='25' weight='bold'></span></b>":fbtn "/bin/bash -c 'killall yad ; notify-send playing lofi ; mpv https://play.streamafrica.net/lofiradio'" \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='25' weight='bold'></span></b>":fbtn '/bin/bash -c "yadnotitrack"' \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='25' weight='bold'></span></b>":fbtn "/bin/bash -c 'killall yad ; killall mpv ; sleep 1 ; widgetyad.sh'" &

#--------------------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------------------#

unset fullscreenshots
unset opensrnyadhub
unset yadhubcalen
unset audacheckpl
unset voldownyad
unset volupyad
unset yadnotitrack
unset yadlogout
unset yadlock
unset yadreboot
unset yadshutdown
unset fullscreenshotsdelay
unset yadterminal
unset yadfileman
unset yadfirefox
unset yadgeany
unset yadbrave
unset yadoffice
unset yadgimp
unset yadaudacity
unset yadsmplayer
unset currently_playing
unset color1
unset color2
unset color3
unset color4
unset end
unset lofi_current
unset check_volume
unset check_mic
unset change_mic
