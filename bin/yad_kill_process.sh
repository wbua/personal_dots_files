#!/bin/bash

# Program command: yad_kill_process.sh
# Description: Kills the focused application.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 06-11-2024
# Program_license: GPL 3.0
# Dependencies: yad, swaymsg, jq

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#---------------------------------------------------------#
# User preferences
#---------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#---------------------------------------------------------#
# Get class name for currently focused application
#---------------------------------------------------------#

swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | \
tr -d '"' | tee ~/Documents/.kill_focused_name.txt

#---------------------------------------------------------#
# Colors
#---------------------------------------------------------#

# Title
color01="<span color='#FFFFFF' font_family='Monospace' font='16' rise='0pt'>"
export color01
# Color end
end="</span>"
export end

#---------------------------------------------------------#
# Kill process (safely)
#---------------------------------------------------------#

yes_choice() {

killall yad
sleep 0.5
choice=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | \
tr -d '"' | tr '[:upper:]' '[:lower:]')
kill $(pidof "$choice") || kill $(pgrep -f "$choice")

}
export -f yes_choice

#---------------------------------------------------------#
# Kill process (force)
#---------------------------------------------------------#

force_kill_process() {

killall yad
sleep 0.8
choice=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | \
tr -d '"' | tr '[:upper:]' '[:lower:]')
pkill -f "$choice" ; pkill -f "${choice^}"
	
}
export -f force_kill_process

#---------------------------------------------------------#
# Cancel
#---------------------------------------------------------#

kill_yad() {

killall yad
	
}
export -f kill_yad

#---------------------------------------------------------#
# Yad dialog
#---------------------------------------------------------#

process_name_text=$(cat ~/Documents/.kill_focused_name.txt)
export process_name_text

   if [[ "$(grep CONFIRM_KILL_APP= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     GTK_THEME="$MY_GTK_THEME" yad --form \
     --height=150 \
     --width=450 \
     --columns=3 \
     --borders=10 \
     --no-buttons \
     --text-align="center" \
     --text="\nKill focused application\n\n${color01}$(echo "$process_name_text")\n${end}" \
     --field=" ":LBL "" \
     --field=" ":LBL "" \
     --field=" ":LBL "" \
     --field="<b>Yes (safely)</b>":fbtn '/bin/bash -c "yes_choice"' \
     --field="<b>Yes (force)</b>":fbtn '/bin/bash -c "force_kill_process"' \
     --field="<b>No</b>":fbtn '/bin/bash -c "kill_yad"' \
     --field=" ":LBL "" \
     --field=" ":LBL "" \
     --field=" ":LBL ""
   elif [[ "$(grep CONFIRM_KILL_APP= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     killall yad
     sleep 0.5
     choice=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | \
     tr -d '"' | tr '[:upper:]' '[:lower:]')
     kill $(pidof "$choice") || kill $(pgrep -f "$choice")
   else
     :
   fi

#---------------------------------------------------------#
# Unset variables and functions
#---------------------------------------------------------#

unset yes_choice
unset MY_GTK_THEME
unset kill_yad
unset process_name_text
unset color01
unset end
unset force_kill_process
