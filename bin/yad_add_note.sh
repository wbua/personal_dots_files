#!/bin/bash

# Program command: yad_add_note.sh
# Description: Add text to note. Used with polybar.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 15-07-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad

#--------------------------------------------------------------------#
# Position of yad on screeen
#--------------------------------------------------------------------#

# Yad box y position
box_y_position="60"
export box_y_position
# Yad box x position
box_x_position="1300"
export box_x_position

#--------------------------------------------------------------------#
# Remove whitespace from text file
#--------------------------------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/yad_note.txt

#--------------------------------------------------------------------#
# Creates files and directories
#--------------------------------------------------------------------#

   # Create Documents directory
   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   # Creates note
   if [ -f "$HOME"/Documents/yad_note.txt ]; then
     :
   else
     touch "$HOME"/Documents/yad_note.txt
   fi
   
#--------------------------------------------------------------------#
# Colors for menu items
#--------------------------------------------------------------------#

color1="<span color='#FE2FFF' font_family='Inter' font='13' weight='bold' rise='0pt'>"
export color1
end="</span>"
export end

#--------------------------------------------------------------------#
# Main
#--------------------------------------------------------------------#

text_file="$HOME/Documents/yad_note.txt"
export text_file 
choice=$(GTK_THEME="alt-dialog" yad --entry --width=500 --height=80 --no-buttons --text-align="center" \
--posx="$box_x_position" --posy="$box_y_position" \
--title="Notes" \
--text="${color1}Add text to note${end}")
export choice
[ -z "$choice" ] && exit 0 || echo "$choice" | tee -a "$text_file" 

#--------------------------------------------------------------------#
# Unsetting variables and functions
#--------------------------------------------------------------------#

unset choice
unset text_file
unset color1
unset end
unset box_y_position
unset box_x_position
