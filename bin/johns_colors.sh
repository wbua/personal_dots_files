#!/bin/bash
# shellcheck disable=SC2034

# Program command: johns_colors.sh
# Description: Colors for johns scripts.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 09-04-2024
# Program_license: GPL 3.0

# Colors not in order.
# Current on color32

#-------------------------------------------------------#
# Quick links
#-------------------------------------------------------#

# Keyword
color1="<span color='#04A5E5' font_family='Cascadia Mono' font='16' weight='bold' rise='0pt'>"
# Link
color2="<span color='#CAD3F5' font_family='Cascadia Mono' font='14' weight='bold' rise='0pt'>"
# Description
color3="<span color='#8087A2' font_family='Cascadia Mono' font='14' weight='bold' rise='0pt'>"
# Quick links
color19="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Keywords
#-------------------------------------------------------#

# Descriptions
color4="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Menu
#-------------------------------------------------------#

# Descriptions
color5="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Actions palette
#-------------------------------------------------------#

# Descriptions
color6="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Radio
#-------------------------------------------------------#

# Currently playing track
color7="<span color='#02FF00' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"
# Descriptions
color17="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Sticky note
#-------------------------------------------------------#

# Characters
color8="<span color='#FFFFFF' font_family='Cascadia Mono' font='11'>"
# Words
color9="<span color='#FFFFFF' font_family='Cascadia Mono' font='11'>"

#-------------------------------------------------------#
# Convert
#-------------------------------------------------------#

# Descriptions
color10="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Apps
#-------------------------------------------------------#

# Descriptions
color11="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Weather
#-------------------------------------------------------#

# Title
color12="<span color='#FFC600' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Find files
#-------------------------------------------------------#

# Message
color13="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Message text for all extensions
#-------------------------------------------------------#

# Message
color14="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"
# Message icons
color29="<span color='#B5B5B5' font_family='Material Icons Outlined' font='22' rise='-8pt'>"
# Action
color30="<span color='#FFFFFF' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Actions text
#-------------------------------------------------------#

# Descriptions
color15="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Actions bookmarks
#-------------------------------------------------------#

# Descriptions
color16="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Actions directories
#-------------------------------------------------------#

# Descriptions
color18="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Last keyword used
#-------------------------------------------------------#

# last keyword used
color20="<span color='#FF0068' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"
# Descriptions
color21="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Control brave with playerctl
#-------------------------------------------------------#

# Currently playing track
color22="<span color='#19FF00' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"
# Descriptions
color23="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"
# Media status
color24="<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Active content
#-------------------------------------------------------#

color25="<span color='#FF004B' font_family='Cascadia Mono' weight='medium' font='15' rise='0pt'>"

#-------------------------------------------------------#
# Root search icons
#-------------------------------------------------------#

color26="<span color='#646B86' font_family='Material Icons Outlined' font='22' rise='-8pt'>"

#-------------------------------------------------------#
# Video cutter
#-------------------------------------------------------#

# Text
color27="<span color='#C4C4C4' font_family='Cascadia Mono' weight='medium' font='13' rise='0pt'>"
# Input labels
color28="<span color='#C4C4C4' font_family='Cascadia Mono' weight='medium' font='13' rise='0pt'>"

#-------------------------------------------------------#
# Snippets
#-------------------------------------------------------#

# Text information
color31="<span color='#C4C4C4' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"
# Keyword information
color32="<span color='#2FFF00' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"

#-------------------------------------------------------#
# Misc
#-------------------------------------------------------#

end="</span>"
