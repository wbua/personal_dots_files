#!/bin/bash

find ~/bin/ -type f -iname '*' -printf '%T@ %p\n' | sort -n | \
cut -f2- -d" " | wofi --dmenu -i | xargs bash -c
