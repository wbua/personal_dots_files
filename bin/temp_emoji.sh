#!/bin/bash

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

set -e

echo "$TEMPORARY_EMOJI" | wl-copy -n

wtype -M ctrl -k v
