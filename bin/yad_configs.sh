#!/bin/bash

# Program command: yad_configs.sh
# Description: Shows all my configs for swaywm.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 23-01-2025
# Program_license: GPL 3.0
# Dependencies: yad

# Format for the configs.txt
# Bashrc=/.bashrc
# Sway keybindings=/.config/sway/keybindings

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-----------------------------------------------------#
# Error checking
#-----------------------------------------------------#

set -e

#-----------------------------------------------------#
# User preferences
#-----------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Config files
CONFIGS_FILE="$MAIN_CONFIGS_FILE"
export CONFIGS_FILE

# Text editor
MY_DEV="$MAIN_DEV"
export MY_DEV

#-----------------------------------------------------#
# Create files and directories
#-----------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -f "$CONFIGS_FILE" ]; then
     :
   else
     notify-send "Config text file does not exist!" && exit 1
   fi

#-----------------------------------------------------#
# Remove whitespace
#-----------------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$CONFIGS_FILE"

#-----------------------------------------------------#
# Array containing list of config files
#-----------------------------------------------------#

fileName="$CONFIGS_FILE"
export fileName

declare -A ary

readarray -t lines < "$fileName"

while IFS= read -r line; do
    ary["${line%%=*}"]="${line#*=}"
done < "$fileName"

#-----------------------------------------------------#
# Add new config
#-----------------------------------------------------#

add_file() {

killall yad

choice=$(yad --form \
--text-align="center" \
--text="\nAdd new config file\nFormat: Sway keybindings=/.config/sway/keybindings\n" \
--border=30 \
--width=600 \
--height=300 \
--separator="#" \
--field="Name: ":CE \
--field="Path: ":CE \
--button="_Exit":1)
[[ -z "$choice" ]] && exit

field1=$(echo "$choice" | awk -F '#' '{print $1}')
field2=$(echo "$choice" | awk -F '#' '{print $2}')

echo "${field1}=${field2}" | tee -a "$CONFIGS_FILE"
	
}
export -f add_file

#-----------------------------------------------------#
# Open configs text file
#-----------------------------------------------------#

configs_text() {

killall yad

   if [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     "$MY_DEV" "$CONFIGS_FILE"
     bash ~/bin/switch_to_development.sh
   elif [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     "$MY_DEV" "$CONFIGS_FILE"
   else
     :
   fi
	
}
export -f configs_text

#-----------------------------------------------------#
# Yad dialog
#-----------------------------------------------------#

_yad() {

GTK_THEME="$MY_GTK_THEME" yad --list \
--column="configs" \
--search-column=1 \
--regex-search \
--separator= \
--title="Configs" \
--text="\nList total: $(cat "$CONFIGS_FILE" | wc -l)\n" \
--text-align="center" \
--borders=20 \
--no-headers \
--no-markup \
--width=800 \
--height=600 \
--button="_Configsrc:/bin/bash -c 'configs_text'" \
--button="_Add file:/bin/bash -c 'add_file'" \
--button="_Exit":1
	
}
export -f _yad

#-----------------------------------------------------#
# Main
#-----------------------------------------------------#

title=$(printf '%s\n' "${!ary[@]}" | sort | _yad)
export title

   if [ "$title" ]; then
     url="${ary["${title}"]}"
     export url  
     "$MY_DEV" "${HOME}""${url}"
   else
     echo "Program terminated." && exit 0
   fi

#-----------------------------------------------------#
# Unset variables and functions
#-----------------------------------------------------#

unset _yad
unset MY_GTK_THEME
unset MY_EDITOR
unset title
unset url
unset fileName
unset add_file
unset configs_text
unset CONFIGS_FILE
unset MY_DEV
