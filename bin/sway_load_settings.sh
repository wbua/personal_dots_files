#!/bin/bash

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

# Icon color
MY_COLOR_ICON="$COLOR_ICON"

# Sway border size
sed -i "s|.*\(default_border pixel\).*|default_border pixel $(printf '%s' "$SWAY_BORDER_SIZE")|" ~/.config/sway/border_size || exit 1

# Sway gaps size
sed -i "s|.*\(gaps inner\).*|gaps inner $(printf '%s' "$SWAY_GAPS_SIZE")|" ~/.config/sway/gap_size || exit 1

# Sway font
sed -i "s|.*\(font pango\).*|font pango\:$(printf '%s' "$SWAY_FONT")|" ~/.config/sway/sway_font || exit 1
   
# Sway focus follows mouse
sed -i "s|.*\(focus_follows_mouse\).*|focus_follows_mouse $(printf '%s' "$SWAY_FOCUS_FOLLOWS_MOUSE")|" ~/.config/sway/focus_follows_mouse || exit 1

# Sway scaling mode
sed -i "s|.*\(output\).*|output * bg \"\$(bash ~/bin/current_wallpaper.sh)\" $(printf '%s' "$WALLPAPER_FILL_MODE")|" ~/.config/sway/sway_wallpapers || exit 1

# Inverted background color
sed -i "s|.*\(INVERTED_BGD=\).*|INVERTED_BGD=\"$(printf '%s' "$MY_COLOR_ICON")\"|" "$I3BLOCKS_THEMES_CONFIG" || exit 1

# Remove double file extensions for wallpaper
bash ~/bin/remove_wallpaper_extensions.sh

# Swaybar background color
bash ~/bin/create_sway_gnome_transparent_theme.sh    

# Reload sway
bash ~/bin/reload_sway.sh

# Sway urgent workspaces
swaymsg bar bar-0 colors urgent_workspace "$(bash ~/bin/swaybar_urgent_workspace.sh)"

# Sway keyboard layout
swaymsg input type:keyboard xkb_layout "$(bash ~/bin/sway_keyboard_layout.sh)"
