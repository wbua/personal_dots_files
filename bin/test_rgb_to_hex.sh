#!/bin/bash

#-----------------------------------------
#function hex() {
 #   printf "%02X%02X%02X" ${*//','/' '}
#}
#[ $(basename -- $0) == "hex" ] && [ $# -gt 0 ] && echo $(hex "${*}")

#hex "232 0 255" | sed 's|^|#|' | xargs wl-copy -n
#-----------------------------------------

#hex() { bash -c """printf "%02X%02X%02X" $(echo "$@" | tr , \ )"""; }

#-----------------------------------------


read -r a b c;

if [ "$a" -gt  255 ]; then
	echo value 1 greater than 255
        exit 0
elif [ "$b" -gt  255 ]; then
	echo value 2 greater than 255
        exit 0 
elif [ "$c" -gt  255 ]; then
	echo value 3 greater than 255
        exit 0
fi

c1=$(printf '%02x\n' "$a");
c2=$(printf '%02x\n' "$b");
c3=$(printf '%02x\n' "$c");
hexStr='#'$c1$c2$c3;
echo "$hexStr" | tr '[:lower:]' '[:upper:]';
