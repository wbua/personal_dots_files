#!/bin/bash

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
source "$I3BLOCKS_THEMES_CONFIG"


MY_TEMP="$COLOR_TEMPERATURE_VALUE"

   if [[ "$(pidof gammastep)" ]]; then
     pkill gammastep
   else
     setsid gammastep -O "$MY_TEMP" && pkill -RTMIN+10 i3blocks >/dev/null 2>&1 & disown
   fi
