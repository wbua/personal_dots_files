#!/bin/bash

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

MY_TIME="$SUSPEND_TIME_AMOUNT"

while true
do
    sleep "$(echo $MY_TIME)"
    notify-send -u critical "Monitor will turn off!" "Press win+shift+m to turn back on!"
    sleep 10
    swaymsg "output * dpms off"
done
