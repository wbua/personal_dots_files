#!/bin/bash

# Program command: startup_mime_list.sh
# Description: Sets the default file associations.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 22-10-2024
# Program_license: GPL 3.0
# Dependencies: mime

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Find out file type
# file --mime-type file
# xdg-mime query filetype file
# xdg-mime query default image/jpeg

#--------------------------------------------------------------#
# Error checking
#--------------------------------------------------------------#

set -e

#--------------------------------------------------------------#
# User preferences
#--------------------------------------------------------------#

# Image viewer
MY_IMAGE_VIEWER="$MAIN_IMAGE_VIEWER"

# Text editor
MY_TEXT_EDITOR="$MAIN_TEXT_EDITOR"

# Video player
MY_VIDEO_PLAYER="$MAIN_VIDEO_PLAYER"

# Browser
MY_BROWSER="$MAIN_BROWSER"

# Pdf viewer
MY_PDF_VIEWER="$MAIN_PDF_VIEWER"

# Audio player
MY_AUDIO_PLAYER="$MAIN_AUDIO_PLAYER"

# Image editor
MY_IMAGE_EDITOR="$MAIN_IMAGE_EDITOR"

# Libreoffice
MY_OFFICE="$MAIN_OFFICE"

#--------------------------------------------------------------#
# Checks if variable contains gthumb
#--------------------------------------------------------------#

check_image_viewer() {

   if [[ "$MY_IMAGE_VIEWER" == "gthumb" ]]; then
     xdg-mime default org.gnome.gThumb.desktop image/png
     xdg-mime default org.gnome.gThumb.desktop image/webp
     xdg-mime default org.gnome.gThumb.desktop image/jpeg
   else
     xdg-mime default "$MY_IMAGE_VIEWER".desktop image/jpeg
     xdg-mime default "$MY_IMAGE_VIEWER".desktop image/png
   fi
	
}

#--------------------------------------------------------------#
# Checks if variable contains evince
#--------------------------------------------------------------#

check_pdf_viewer() {

   if [[ "$MY_PDF_VIEWER" == "evince" ]]; then
     xdg-mime default org.gnome.Evince.desktop application/pdf    
   else
     xdg-mime default "$MY_PDF_VIEWER".desktop application/pdf
     xdg-mime default "$MY_PDF_VIEWER".desktop application/pdf
   fi
	
}

#--------------------------------------------------------------#
# Checks if variable contains libreoffice
#--------------------------------------------------------------#

check_libreoffice_program() {

   if [[ "$MY_OFFICE" == "libreoffice" ]]; then
     xdg-mime default libreoffice_writer.desktop application/vnd.oasis.opendocument.text
     xdg-mime default libreoffice_calc.desktop application/vnd.oasis.opendocument.text
     xdg-mime default libreoffice_draw.desktop application/vnd.oasis.opendocument.text
     xdg-mime default libreoffice_impress.desktop application/vnd.oasis.opendocument.text    
   else
     xdg-mime default "$MY_OFFICE".desktop application/vnd.openxmlformats-officedocument.wordprocessingml.document
     xdg-mime default "$MY_OFFICE".desktop application/vnd.oasis.opendocument.text
   fi
	
}

#--------------------------------------------------------------#
# Main
#--------------------------------------------------------------#

   if [[ "$(grep SWAY_FILE_ASSOCIATIONS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then

     # Image viewer
     check_image_viewer
     # Text editor
     xdg-mime default "$MY_TEXT_EDITOR".desktop text/plain
     # Video player
     xdg-mime default "$MY_VIDEO_PLAYER".desktop video/webm
     xdg-mime default "$MY_VIDEO_PLAYER".desktop video/x-matroska
     xdg-mime default "$MY_VIDEO_PLAYER".desktop video/mp4
     # Browser
     xdg-mime default "$MY_BROWSER".desktop text/html
     # Audio player
     xdg-mime default "$MY_AUDIO_PLAYER".desktop audio/mpeg
     xdg-mime default "$MY_AUDIO_PLAYER".desktop audio/m4a
     xdg-mime default "$MY_AUDIO_PLAYER".desktop audio/x-ms-wma
     xdg-mime default "$MY_AUDIO_PLAYER".desktop audio/wav
     # Office
     check_libreoffice_program
     # Image editor
     xdg-mime default "$MY_IMAGE_EDITOR".desktop image/x-xcf
     # Pdf viewer
     check_pdf_viewer
          
   elif [[ "$(grep SWAY_FILE_ASSOCIATIONS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     :
   else
     :
   fi
