#!/bin/bash



# Location of brave bookmarks
brave_file="/home/john/snap/brave/current/.config/BraveSoftware/Brave-Browser/Default/Bookmarks"

# Greps the titles
awk -F '"name":' '{print $2}' "$brave_file" | sed '/^[[:space:]]*$/d' | \
tr -d '"' | sed 's| ||' | tee ~/Documents/.brave_bookmarks_title.txt
sed -i -e 's|Bookmarks bar,||g' -e '/^$/d' ~/Documents/.brave_bookmarks_title.txt

# Greps the url's
awk -F '"url":' '{print $2}' "$brave_file" | sed '/^[[:space:]]*$/d' | \
tr -d '"' | sed 's| ||' | tee ~/Documents/.brave_bookmarks_url.txt

# Combines both titles and url's
paste ~/Documents/.brave_bookmarks_title.txt ~/Documents/.brave_bookmarks_url.txt | \
tee "$HOME"/Documents/.bookmarks_brave_mixed.txt

file_combined=$(cat "$HOME"/Documents/.bookmarks_brave_mixed.txt)
choice=$(echo "$file_combined" | rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'window {width: 60%;}')

echo "$choice" | awk '{print $NF}' | xargs -d '\n' "$browser"
