#!/bin/bash

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

pactl set-sink-volume @DEFAULT_SINK@ +5% && pkill -RTMIN+10 i3blocks

if [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
paplay ~/Music/sounds/volume.mp3
elif [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
:
fi
