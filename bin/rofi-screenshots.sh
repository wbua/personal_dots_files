#!/bin/bash

# Program command: rofi-screenshots.sh
# Description: Screenshot tool.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 09-10-2023
# Program_license: GPL 3.0
# Dependencies: rofi, maim, xclip

#-------------------------------------------------------#
# Error checking
#-------------------------------------------------------#

set -euo pipefail

#-------------------------------------------------------#
# Source files
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/prefixrofi_functions.sh

# shellcheck disable=SC1091
source "$HOME"/bin/prefixrofi_files_directories.sh

# shellcheck disable=SC1091
source "$HOME"/bin/prefixrofi_config.sh

#-------------------------------------------------------#
# Create directories and files
#-------------------------------------------------------#

create_file_and_directories

#-------------------------------------------------------#
# Menu
#-------------------------------------------------------#

menu() {
	
echo "fullscreen"
echo "select"
echo "delay"
echo "open"
echo "directory"	
	
}

#-------------------------------------------------------#
# Main
#-------------------------------------------------------#

choice=$(menu | sort | _rofi)
case "$choice" in

 'fullscreen')
  take_screenshot
  ;;
 
 'select')
  ss_select
  ;;
  
 'delay')
  ss_delay
  ;;
  
 'open')
  ss_open
  ;;
  
 'directory')
  view_screenshots
  ;;
  
  *)
  echo "Something when wrong" | _rofi2
  ;;


esac
