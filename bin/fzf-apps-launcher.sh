#!/bin/bash

# Program command: fzf-apps-launcher.sh
# Description: This launches all of your apps.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 05-01-2024
# Program_license: GPL 3.0
# Dependencies: fzf

#-------------------------------------------------#
# Software preferences
#-------------------------------------------------#

# Text editor
text_editor="geany"
export text_editor

#-------------------------------------------------#
# Create file and directories
#-------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ -f "$HOME"/Documents/mainprefix/rofi_apps.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/rofi_apps.txt
   fi

#-------------------------------------------------#
# Colors
#-------------------------------------------------#

# Keybindings
color1=$(tput setaf 15)
export color1
# Color end
end=$(tput sgr0)
export end

#-------------------------------------------------#
# Open apps text file
#-------------------------------------------------#

text_file() {

setsid "$text_editor" ~/Documents/mainprefix/rofi_apps.txt >/dev/null 2>&1 & disown
pkill -f ~/bin/fzf-apps-launcher.sh
	
}
export -f text_file

#-------------------------------------------------#
# Kill script
#-------------------------------------------------#

kill_script() {

pkill -f ~/bin/fzf-apps-launcher.sh
	
}
export -f kill_script

#-------------------------------------------------#
# fzf settings
#-------------------------------------------------#

_fzf() {

fzf --reverse \
--padding 5% \
--ansi \
--cycle \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:#660020,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--info=inline \
--header="
launch app: ${color1}enter${end} | open apps text file: ${color1}F1${end} | kill script: ${color1}F9${end}

" \
--prompt 'Search apps...> ' \
--bind='F1:execute(text_file {})' \
--bind='F9:execute(kill_script {})'
	
}
export -f _fzf

#-------------------------------------------------#
# Main
#-------------------------------------------------#

main() {

apps_file=$(cat ~/Documents/mainprefix/rofi_apps.txt | _fzf)
[[ -z "$apps_file" ]] && exit 0
setsid /bin/bash -c "$apps_file" >/dev/null 2>&1 & disown
pkill -f ~/bin/fzf-apps-launcher.sh

}
export -f main
main

#-------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------#

unset main
unset _fzf
unset kill_script
unset text_file
unset color1
unset end
unset text_editor
