import os
import tkinter as tk
import subprocess
from tkinter import messagebox
from tkinter import PhotoImage

# Specify the directory containing images
IMAGE_DIRECTORY = "/home/john/Pictures/test_wall/"  # Change this to your desired directory

# Function to generate a thumbnail using ImageMagick's convert command
def generate_thumbnail(image_path, size=(250, 141)):
    try:
        # Create a temporary file for the thumbnail
        thumbnail_path = f"/tmp/{os.path.basename(image_path)}_thumb.png"
        # subprocess.run(['convert', image_path, '-resize', f'{size[0]}x{size[1]}', thumbnail_path], check=True)

        # Read the generated thumbnail and convert to Tkinter format
        with open(thumbnail_path, "rb") as f:
            image_data = f.read()
        return PhotoImage(data=image_data)
    except Exception as e:
        print(f"Error generating thumbnail for {image_path}: {e}")
        return None

# Function to load images from the fixed directory
def load_images_from_directory():
    if not os.path.exists(IMAGE_DIRECTORY):
        messagebox.showerror("Error", f"The directory {IMAGE_DIRECTORY} does not exist.")
        return
    
    images = []
    
    for filename in os.listdir(IMAGE_DIRECTORY):
        file_path = os.path.join(IMAGE_DIRECTORY, filename)
        if os.path.isfile(file_path) and file_path.lower().endswith(('.png', '.jpg', '.jpeg', '.gif', '.bmp')):
            thumb = generate_thumbnail(file_path)
            if thumb:
                images.append((file_path, thumb))
    
    display_images(images)

# Function to display images on the Tkinter window
def display_images(images):
    for widget in frame.winfo_children():
        widget.destroy()

    for idx, (file_path, thumb) in enumerate(images):
        label = tk.Label(frame, image=thumb)
        label.grid(row=idx // 8, column=idx % 8, padx=5, pady=5)
        label.image = thumb  # Keep a reference to avoid garbage collection
        label.bind("<Button-1>", lambda event, path=file_path: show_image_path(path))

# Function to show the full path of the image when clicked
def show_image_path(image_path):
    # Print the image path to stdout (this will be captured by Bash)
    print(image_path)

# Function to exit the Tkinter application when Escape is pressed
def exit_application(event=None):
    root.quit()  # Exits the Tkinter event loop

# Set up the main Tkinter window
root = tk.Tk()
root.title("Image Thumbnails Viewer py")
root.geometry("1600x800")  # width=1600px, height=800px

# Frame for displaying images (inside a canvas for scrollbars)
canvas = tk.Canvas(root)
canvas.pack(side="left", fill="both", expand=True)

# Scrollbars
vsb = tk.Scrollbar(root, orient="vertical", command=canvas.yview)
vsb.pack(side="right", fill="y")
hsb = tk.Scrollbar(root, orient="horizontal", command=canvas.xview)
hsb.pack(side="bottom", fill="x")

canvas.configure(yscrollcommand=vsb.set, xscrollcommand=hsb.set)

# Frame inside the canvas to hold the image thumbnails
frame = tk.Frame(canvas)
canvas.create_window((0, 0), window=frame, anchor="nw")

# Update scroll region after images are displayed
def update_scroll_region(event=None):
    canvas.config(scrollregion=canvas.bbox("all"))

# Label for showing the selected image path
#path_label = tk.Label(root, text="Full Path: None", font=("Helvetica", 10))
#path_label.pack(pady=10)

# Button to load images from the specified directory (no longer needed)
# button = tk.Button(root, text="Load Images", command=load_images_from_directory)
# button.pack(pady=20)

# Bind the Escape key to exit the application
root.bind('<Escape>', exit_application)

# Bind the frame update after images are displayed to resize canvas area
root.bind('<Configure>', update_scroll_region)

# Arrow key bindings to scroll the canvas
def on_arrow_key(event):
    # Check which key was pressed and scroll accordingly
    if event.keysym == "Up":
        canvas.yview_scroll(-1, "units")  # Scroll up
    elif event.keysym == "Down":
        canvas.yview_scroll(1, "units")  # Scroll down
    elif event.keysym == "Left":
        canvas.xview_scroll(-1, "units")  # Scroll left
    elif event.keysym == "Right":
        canvas.xview_scroll(1, "units")  # Scroll right

# Bind arrow keys to the canvas
root.bind("<Up>", on_arrow_key)
root.bind("<Down>", on_arrow_key)
root.bind("<Left>", on_arrow_key)
root.bind("<Right>", on_arrow_key)

# Automatically load images when the program starts
load_images_from_directory()

# Start the Tkinter event loop
root.mainloop()
