#!/bin/bash
# shellcheck disable=SC2317

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#--------------------------------------------------------------------#
# User preferences
#--------------------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#--------------------------------------------------------------------#
# Help (type: videodownloadergtk.sh -h)
#--------------------------------------------------------------------#

program_command="videodownloadergtk.sh"
program_version="1.0.0"
program_updated="04-07-2024"
program_license="GPL 3.0"
contributors="John Mcgrath"
program_website="none"
program_contact="none"


help(){
cat << EOF
...

## Tracking

    * Command:      $program_command
    * Version:      $program_version
    * Updated:      $program_updated
    * License:      $program_license
    * Website:      $program_website
    * Contact:      $program_contact
    * Contributors: $contributors

## Description

    * Download videos from youtube (not shorts) and other sites    
    * Paste audio link: Paste audio link from clipboard (download only audio from video)
    * Paste video link: Paste video link from clipboard (download video and audio)
    * Paste url: Paste url into input box (download video and audio)
    * Play: Play last downloaded video
    * Delete: Delete last downloaded video
    * Video: Open downloaded video directory
    * Sound: Open downloaded audio directory
 
## Dependencies

   * Yad
   * Zenity
   * Wl-clipboard
   * Yt-dlp
   * Ffmpeg
   * Imagemagick
   * Inter font (see in other information)

## Installation

   Before you install anything, first update your system

   Example:

   $ sudo apt update

   ---

   To install the dependencies for video downloading app

   Example:

   $ sudo apt install yad zenity yt-dlp imagemagick wl-clipboard

   ---

   To make the script executable follow the example below

   Example:

   $ chmod u+x videodownloadergtk.sh

   Install script into /home/user/.local/bin/ or /home/user/bin/

   ---

   To install inter font, install the program below

   Example:

   $ sudo apt install font-manager

## Other information

   To download font visit: https://fonts.google.com/specimen/Inter

EOF
}

while getopts ":h" option; do
  case $option in
    h) # display help
    help
    exit;;
   \?) # incorrect option
    echo "Error: invalid option"
    exit;;
  esac
done

#--------------------------------------------------------------------#
# Checks if your running wayland
#--------------------------------------------------------------------#

   if [[ "$(echo $WAYLAND_DISPLAY)" == "" ]]; then
     yad --error \
     --width 500 \
     --title "Wayland session not found!" \
     --text "Video downloader error, wayland only tool!"
     exit 1
   else
     :
   fi

#--------------------------------------------------------------------#
# Create files and directories
#--------------------------------------------------------------------#

# Create video directory
   if [ -d "$HOME"/Videos ]; then
     :
   else
     mkdir "$HOME"/Videos
   fi

# Create music directory
   if [ -d "$HOME"/Music ]; then
     :
   else
     mkdir "$HOME"/Music
   fi

# Create pictures directory
   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi
   
# Create video directory
   if [ -d "$HOME"/Videos ]; then
     :
   else
     mkdir "$HOME"/Videos
   fi

# Where youtube videos are stored
   if [ -d "$HOME"/Videos/youtube ]; then
     :
   else
     mkdir "$HOME"/Videos/youtube
   fi

# Where youtube audio files are stored
   if [ -d "$HOME"/Music/youtube ]; then
     :
   else
     mkdir "$HOME"/Music/youtube
   fi

# Temp thumbnail directory
   if [ -d "$HOME"/.viddwlgtk ]; then
     :
   else
     mkdir "$HOME"/.viddwlgtk
   fi

# Temp video and audio directory
   if [ -d "$HOME"/.tempytdl ]; then
     :
   else
     mkdir "$HOME"/.tempytdl
   fi

# Mp3 png file 
   if [ -d "$HOME"/Pictures/.tempdlpics ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempdlpics
   fi


# Empty png file
   if [ -d "$HOME"/Pictures/.tempdelemp ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempdelemp
   fi

#--------------------------------------------------------------------#
# Paste url
#--------------------------------------------------------------------# 

downloadvids () {

# Download video from youtube, and puts it in .tempytdl 
killall yad
rm -f ~/.tempytdl/*.*
rm -f "$HOME"/.viddwlgtk/*.*
inputstr=$(zenity --entry --text="Enter url" --width=500 --height=100)
[ -z "$inputstr" ] && exit 0
videostr=$(yt-dlp "$inputstr" -o "$HOME/.tempytdl/%(title)s.%(ext)s" --write-thumbnail | \
zenity --progress --width=500 --height=100 \
--title="Enter youtube link" --text="Downloading..." --auto-close \
--auto-kill --pulsate && zenity --info --width=500 --height=100 \
--title="Youtube-dl" --text="Download completed") 
export videostr
export inputstr

echo "$videostr" 

if [ "$?" = -1 ] ; then
        zenity --error --width=500 --height=100 \
        --text="Canceled."
fi

# Takes downloaded video and captures thumbnail from it. Then sends thumbnail to .viddwlgtk
cp "$HOME"/.tempytdl/*.webp "$HOME"/.viddwlgtk/
rm -f "$HOME"/.tempytdl/*.webp
convert "$HOME"/.viddwlgtk/*.webp -resize 384x384 "$HOME/.viddwlgtk/$(date +'%d-%m-%Y-%H%M%S')".png
rm -f "$HOME"/.viddwlgtk/*.webp
cp ~/.tempytdl/*.* ~/Videos/youtube/
videodownloadergtk.sh
}
export -f downloadvids 

#--------------------------------------------------------------------#
# Paste Video link
#--------------------------------------------------------------------# 

downloadvidstwo () {

# Download video from youtube, and puts it in .tempytdl
killall yad
rm -f ~/.tempytdl/*.*
rm -f "$HOME"/.viddwlgtk/*.*
inputstr02=$(wl-paste)
[ -z "$inputstr02" ] && exit 0
videostr02=$(yt-dlp "$inputstr02" -o "$HOME/.tempytdl/%(title)s.%(ext)s" --write-thumbnail | \
zenity --progress --width=500 --height=100 \
--title="Enter youtube link" --text="Downloading..." --auto-close \
--auto-kill --pulsate && zenity --info --width=500 --height=100 \
--title="Youtube-dl" --text="Download completed") 
export videostr02
export inputstr02

echo "$videostr02" 

if [ "$?" = -1 ] ; then
        zenity --error --width=500 --height=100 \
        --text="Canceled."
fi

# Takes downloaded video and captures thumbnail from it. Then sends thumbnail to .viddwlgtk
cp "$HOME"/.tempytdl/*.webp "$HOME"/.viddwlgtk/
rm -f "$HOME"/.tempytdl/*.webp
convert "$HOME"/.viddwlgtk/*.webp -resize 384x384 "$HOME/.viddwlgtk/$(date +'%d-%m-%Y-%H%M%S')".png
rm -f "$HOME"/.viddwlgtk/*.webp
cp ~/.tempytdl/*.* ~/Videos/youtube/
videodownloadergtk.sh	
}
export -f downloadvidstwo

#--------------------------------------------------------------------#
# Paste audio link
#--------------------------------------------------------------------# 

downloadaudio () {

# Download video audio from youtube, and puts it in .tempytdl
killall yad
rm -f ~/.tempytdl/*.*
rm -f "$HOME"/.viddwlgtk/*.*
inputstr03=$(wl-paste)
[ -z "$inputstr03" ] && exit 0
videostr03=$(yt-dlp -x --audio-format mp3 "$inputstr03" -o "$HOME/.tempytdl/%(title)s.%(ext)s" | \
zenity --progress --width=500 --height=100 \
--title="Enter youtube link" --text="Downloading..." --auto-close \
--auto-kill --pulsate && zenity --info --width=500 --height=100 \
--title="Youtube-dl" --text="Download completed") 
export videostr03
export inputstr03

echo "$videostr03" 

if [ "$?" = -1 ] ; then
        zenity --error --width=500 --height=100 \
        --text="Canceled."
fi

cp ~/Pictures/.tempdlpics/*.* ~/.viddwlgtk/
cp ~/.tempytdl/*.* ~/Music/youtube/
videodownloadergtk.sh	
}
export -f downloadaudio 

#--------------------------------------------------------------------#
# Play last downloaded video
#--------------------------------------------------------------------#

lastconvideo () {

killall yad
openlast=$(find "$HOME"/.tempytdl/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open)
echo "$openlast"


}
export -f lastconvideo

#--------------------------------------------------------------------#
# Delete last downloaded video in main directory 
#--------------------------------------------------------------------#

dellastdovod () {

killall yad
myfilename=$(find "$HOME"/.tempytdl/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | cut -d '/' -f5-)
export myfilename
rm -f "$HOME"/Videos/youtube/"$myfilename"
rm -f ~/.viddwlgtk/*.*
cp ~/Pictures/.tempdelemp/*.* ~/.viddwlgtk/
rm -f ~/.tempytdl/*.*
videodownloadergtk.sh
	
}
export -f dellastdovod 

#--------------------------------------------------------------------#
# Open container for youtube videos
#--------------------------------------------------------------------#

containopenvid () {

killall yad
checkveros=$(uname -a | xargs)
export checkveros

if [[ "$checkveros" == *"MANJARO"* ]]; then
   exo-open "$HOME"/Videos/youtube/
else
   xdg-open "$HOME"/Videos/youtube/
fi

}
export -f containopenvid

#--------------------------------------------------------------------#
# Open container for youtube mp3
#--------------------------------------------------------------------#

containopensnd () {

killall yad
checkveros=$(uname -a | xargs)
export checkveros

if [[ "$checkveros" == *"MANJARO"* ]]; then
   exo-open "$HOME"/Music/youtube/
else
   xdg-open "$HOME"/Music/youtube/
fi

}
export -f containopensnd

#--------------------------------------------------------------------#
# Yad settings
#--------------------------------------------------------------------#

GTK_THEME="$MY_GTK_THEME" yad --form --width=850 --height=500 \
--image-on-top --borders=20 --columns=1 --title="Video downloader" \
--text="<span color='#ffffff' font_family='Inter' font='14'>$(find "$HOME"/.tempytdl/ -type f -print0 | \
xargs -0 ls -tr | tail -n 1 | \
tr "&" "+" | \
tr -d '<>' | tr -d '#' | tr -d '""' | \
tr -dC '[:print:]\t\n' | \
cut -d '/' -f5- | cut -f 1 -d '.')</span>" \
--image="$(find "$HOME"/.viddwlgtk/ -type f -print0 | xargs -0 ls -tr | tail -n 1)" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Paste audio link</span>:/bin/bash -c 'downloadaudio'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Paste Video link</span>:/bin/bash -c 'downloadvidstwo'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Paste url</span>:/bin/bash -c 'downloadvids'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Play</span>:/bin/bash -c 'lastconvideo'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Delete</span>:/bin/bash -c 'dellastdovod'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Video</span>:/bin/bash -c 'containopenvid'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Sound</span>:/bin/bash -c 'containopensnd'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Exit</span>":1

#--------------------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------------------#

unset downloadvids
unset xcinpstr
unset xcinpstr02
unset videostr
unset videostr02
unset videostr03
unset inputstr
unset inputstr02
unset inputstr03
unset lastconvideo
unset containopenvid
unset downloadvidstwo
unset downloadaudio
unset containopensnd
unset dellastdovod
unset myfilename
