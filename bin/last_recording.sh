#!/bin/bash

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

choice=$(find ~/Videos/recordings/ -type f \( -iname '*.mp4' -o -iname '*.mkv' -o -iname '*.webm' -o -iname '*.avi' -o -iname '*.wmv' -o -iname '*.mov' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
"$MAIN_VIDEO_PLAYER" "$choice"
