#!/bin/bash

# Program command: yad_workspace_changer.sh
# Description: View and change programs workspace numbers.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-01-2025
# Program_license: GPL 3.0
# Dependencies: yad

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#----------------------------------------------------------#
# Error checking
#----------------------------------------------------------#

set -e

#----------------------------------------------------------#
# User preferences
#----------------------------------------------------------#

# Browser
MY_BROWSER="$MAIN_BROWSER"
export MY_BROWSER

# Terminal
MY_TERMINAL="$MAIN_TERMINAL"
export MY_TERMINAL

# File manager
MY_FILEMANAGER="$MAIN_FILE_MANAGER"
export MY_FILEMANAGER

# Development
MY_DEV="$MAIN_DEV"
export MY_DEV

# Social
MY_SOCIAL="$MAIN_SOCIAL"
export MY_SOCIAL

# Office
MY_OFFICE="$MAIN_OTHER_OFFICE"
export MY_OFFICE

# Video editor
MY_VIDEO_EDITOR="$MAIN_VIDEO_EDITOR"
export MY_VIDEO_EDITOR

# Sound editor
MY_SOUND_EDITOR="$MAIN_SOUND_EDITOR"
export MY_SOUND_EDITOR

# Image editor
MY_IMAGEEDITOR="$MAIN_IMAGE_EDITOR"
export MY_IMAGEEDITOR

# Text editor
MY_TEXTEDITOR="$MAIN_TEXT_EDITOR"
export MY_TEXTEDITOR

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#----------------------------------------------------------#
# Colors
#----------------------------------------------------------#

# Numbers
color001="<span color='$COLOR_ICON' font_family='JetBrainsMono Nerd Font' weight='bold' font='16' rise='0pt'>"
export color001
# Color end
end="</span>"
export end

#----------------------------------------------------------#
# Check if variable is enabled or disabled
#----------------------------------------------------------#

   if [[ "$(grep SPECIFIC_APP_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     :
   elif [[ "$(grep SPECIFIC_APP_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     notify-send "Specific app workpaces" "Has been disabled" && exit 1
   else
     :
   fi

#----------------------------------------------------------#
# Create files and directories
#----------------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   # Browser
   if [ -f "$HOME"/Documents/.set_workspace_browser.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_browser.txt
   fi

   # File manager
   if [ -f "$HOME"/Documents/.set_workspace_filemanager.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_filemanager.txt
   fi

   # File terminal
   if [ -f "$HOME"/Documents/.set_workspace_terminal.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_terminal.txt
   fi

   # Development
   if [ -f "$HOME"/Documents/.set_workspace_dev.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_dev.txt
   fi

   # Social
   if [ -f "$HOME"/Documents/.set_workspace_social.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_social.txt
   fi

   # Office
   if [ -f "$HOME"/Documents/.set_workspace_office.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_office.txt
   fi

   # Video editor
   if [ -f "$HOME"/Documents/.set_workspace_videoeditor.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_videoeditor.txt
   fi

   # Audio editor
   if [ -f "$HOME"/Documents/.set_workspace_audioeditor.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_audioeditor.txt
   fi

   # Image editor
   if [ -f "$HOME"/Documents/.set_workspace_imageeditor.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_imageeditor.txt
   fi

   # Text editor
   if [ -f "$HOME"/Documents/.set_workspace_texteditor.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_texteditor.txt
   fi

#----------------------------------------------------------#
# Text files containing workspace numbers
#----------------------------------------------------------#

# Browser
select_browser=$(cat "$HOME"/Documents/.set_workspace_browser.txt)
export select_browser

# Terminal
select_terminal=$(cat "$HOME"/Documents/.set_workspace_terminal.txt)
export select_terminal

# File manager
select_filemanager=$(cat "$HOME"/Documents/.set_workspace_filemanager.txt)
export select_filemanager

# Development
select_dev=$(cat "$HOME"/Documents/.set_workspace_dev.txt)
export select_dev

# Social
select_social=$(cat "$HOME"/Documents/.set_workspace_social.txt)
export select_social

# Office
select_office=$(cat "$HOME"/Documents/.set_workspace_office.txt)
export select_office

# Video editor
select_videoeditor=$(cat "$HOME"/Documents/.set_workspace_videoeditor.txt)
export select_videoeditor

# Audio editor
select_audioeditor=$(cat "$HOME"/Documents/.set_workspace_audioeditor.txt)
export select_audioeditor

# Image editor
select_imageeditor=$(cat "$HOME"/Documents/.set_workspace_imageeditor.txt)
export select_imageeditor

# Text editor
select_texteditor=$(cat "$HOME"/Documents/.set_workspace_texteditor.txt)
export select_texteditor

#----------------------------------------------------------#
# Enter number for browser
#----------------------------------------------------------#

browser_choice() {

killall yad
browser_number=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--text-align="center" --text="\nEnter number...\n" \
--width=200 --height=150 --borders=10 --button="Exit":1)	
[ -z "$browser_number" ] && exit 0
echo "$browser_number" > "$HOME"/Documents/.set_workspace_browser.txt

}
export -f browser_choice

#----------------------------------------------------------#
# Enter number for terminal
#----------------------------------------------------------#

terminal_choice() {

killall yad
terminal_number=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--text-align="center" --text="\nEnter number...\n" \
--width=200 --height=150 --borders=10 --button="Exit":1)	
[ -z "$terminal_number" ] && exit 0
echo "$terminal_number" > "$HOME"/Documents/.set_workspace_terminal.txt
	
}
export -f terminal_choice

#----------------------------------------------------------#
# Enter number for file manager
#----------------------------------------------------------#

filemanager_choice() {

killall yad
filemanager_number=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--text-align="center" --text="\nEnter number...\n" \
--width=200 --height=150 --borders=10 --button="Exit":1)	
[ -z "$filemanager_number" ] && exit 0
echo "$filemanager_number" > "$HOME"/Documents/.set_workspace_filemanager.txt
	
}
export -f filemanager_choice 

#----------------------------------------------------------#
# Enter number for development
#----------------------------------------------------------#

development_choice() {

killall yad
dev_number=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--text-align="center" --text="\nEnter number...\n" \
--width=200 --height=150 --borders=10 --button="Exit":1)	
[ -z "$dev_number" ] && exit 0
echo "$dev_number" > "$HOME"/Documents/.set_workspace_dev.txt

}
export -f development_choice

#----------------------------------------------------------#
# Enter number for social
#----------------------------------------------------------#

social_choice() {

killall yad
social_number=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--text-align="center" --text="\nEnter number...\n" \
--width=200 --height=150 --borders=10 --button="Exit":1)	
[ -z "$social_number" ] && exit 0
echo "$social_number" > "$HOME"/Documents/.set_workspace_social.txt

}
export -f social_choice

#----------------------------------------------------------#
# Enter number for office
#----------------------------------------------------------#

office_choice() {

killall yad
office_number=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--text-align="center" --text="\nEnter number...\n" \
--width=200 --height=150 --borders=10 --button="Exit":1)	
[ -z "$office_number" ] && exit 0
echo "$office_number" > "$HOME"/Documents/.set_workspace_office.txt
	
}
export -f office_choice

#----------------------------------------------------------#
# Enter number for video
#----------------------------------------------------------#

video_choice() {

killall yad
video_number=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--text-align="center" --text="\nEnter number...\n" \
--width=200 --height=150 --borders=10 --button="Exit":1)	
[ -z "$video_number" ] && exit 0
echo "$video_number" > "$HOME"/Documents/.set_workspace_videoeditor.txt
	
}
export -f video_choice

#----------------------------------------------------------#
# Enter number for audio editor
#----------------------------------------------------------#

audio_choice() {

killall yad
audio_number=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--text-align="center" --text="\nEnter number...\n" \
--width=200 --height=150 --borders=10 --button="Exit":1)	
[ -z "$audio_number" ] && exit 0
echo "$audio_number" > "$HOME"/Documents/.set_workspace_audioeditor.txt

}
export -f audio_choice

#----------------------------------------------------------#
# Enter number for image editor
#----------------------------------------------------------#

image_choice() {

killall yad
image_number=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--text-align="center" --text="\nEnter number...\n" \
--width=200 --height=150 --borders=10 --button="Exit":1)	
[ -z "$image_number" ] && exit 0
echo "$image_number" > "$HOME"/Documents/.set_workspace_imageeditor.txt

}
export -f image_choice

#----------------------------------------------------------#
# Enter number for text editor
#----------------------------------------------------------#

text_choice() {

killall yad
image_number=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--text-align="center" --text="\nEnter number...\n" \
--width=200 --height=150 --borders=10 --button="Exit":1)	
[ -z "$image_number" ] && exit 0
echo "$image_number" > "$HOME"/Documents/.set_workspace_texteditor.txt

}
export -f text_choice

#----------------------------------------------------------#
# Entry boxes for all main programs
#----------------------------------------------------------#

all_entry_boxes() {

killall yad
entry_choice=$(GTK_THEME="$MY_GTK_THEME" yad --form \
--title="Workspace changer" \
--columns=2 \
--width=800 \
--height=400 \
--borders=20 \
--separator="|" \
--align="right" \
--field="Browser: ${color001}${select_browser}${end}":LBL "" \
--field="Number ":CE \
--field="Terminal: ${color001}${select_terminal}${end}":LBL "" \
--field="Number ":CE \
--field="File manager: ${color001}${select_filemanager}${end}":LBL "" \
--field="Number ":CE \
--field="Development: ${color001}${select_dev}${end}":LBL "" \
--field="Number ":CE \
--field="Social: ${color001}${select_social}${end}":LBL "" \
--field="Number ":CE \
--field="Office: ${color001}${select_office}${end}":LBL "" \
--field="Number ":CE \
--field="Video editor: ${color001}${select_videoeditor}${end}":LBL "" \
--field="Number ":CE \
--field="Audio editor: ${color001}${select_audioeditor}${end}":LBL "" \
--field="Number ":CE \
--field="Image editor: ${color001}${select_imageeditor}${end}":LBL "" \
--field="Number ":CE \
--field="Text editor: ${color001}${select_texteditor}${end}":LBL "" \
--field="Number ":CE \
--button="Ok":0 \
--button="Exit":1)
[ -z "$entry_choice" ] && exit 0

# Browser
field1=$(echo "$entry_choice" | awk -F '|' '{print $2}')
# Terminal
field2=$(echo "$entry_choice" | awk -F '|' '{print $4}')
# File manager
field3=$(echo "$entry_choice" | awk -F '|' '{print $6}')
# Development
field4=$(echo "$entry_choice" | awk -F '|' '{print $8}')
# Social
field5=$(echo "$entry_choice" | awk -F '|' '{print $10}')
# Office
field6=$(echo "$entry_choice" | awk -F '|' '{print $12}')
# Video editor
field7=$(echo "$entry_choice" | awk -F '|' '{print $14}')
# Audio editor
field8=$(echo "$entry_choice" | awk -F '|' '{print $16}')
# Image editor
field9=$(echo "$entry_choice" | awk -F '|' '{print $18}')
# Text editor
field10=$(echo "$entry_choice" | awk -F '|' '{print $20}')

# Browser   
[ -z "$field1" ] && echo "empty" || echo "$entry_choice" | \
awk -F '|' '{print $2}' > "$HOME"/Documents/.set_workspace_browser.txt
# Terminal 
[ -z "$field2" ] && echo "empty" || echo "$entry_choice" | \
awk -F '|' '{print $4}' > "$HOME"/Documents/.set_workspace_terminal.txt
# File manager
[ -z "$field3" ] && echo "empty" || echo "$entry_choice" | \
awk -F '|' '{print $6}' > "$HOME"/Documents/.set_workspace_filemanager.txt
# Development
[ -z "$field4" ] && echo "empty" || echo "$entry_choice" | \
awk -F '|' '{print $8}' > "$HOME"/Documents/.set_workspace_dev.txt
# Social
[ -z "$field5" ] && echo "empty" || echo "$entry_choice" | \
awk -F '|' '{print $10}' > "$HOME"/Documents/.set_workspace_social.txt
# Office
[ -z "$field6" ] && echo "empty" || echo "$entry_choice" | \
awk -F '|' '{print $12}' > "$HOME"/Documents/.set_workspace_office.txt
# Video editor
[ -z "$field7" ] && echo "empty" || echo "$entry_choice" | \
awk -F '|' '{print $14}' > "$HOME"/Documents/.set_workspace_videoeditor.txt
# Audio editor
[ -z "$field8" ] && echo "empty" || echo "$entry_choice" | \
awk -F '|' '{print $16}' > "$HOME"/Documents/.set_workspace_audioeditor.txt
# Image editor
[ -z "$field9" ] && echo "empty" || echo "$entry_choice" | \
awk -F '|' '{print $18}' > "$HOME"/Documents/.set_workspace_imageeditor.txt
# Text editor
[ -z "$field10" ] && echo "empty" || echo "$entry_choice" | \
awk -F '|' '{print $20}' > "$HOME"/Documents/.set_workspace_texteditor.txt
   
}
export -f all_entry_boxes

#----------------------------------------------------------#
# Yad dialog
#----------------------------------------------------------#

GTK_THEME="$MY_GTK_THEME" yad --form \
--title="Workspace changer" \
--columns=2 \
--width=800 \
--height=400 \
--borders=20 \
--align="center" \
--field="Browser: ${color001}${select_browser}${end}":LBL "" \
--field="<b>Browser number</b>":fbtn '/bin/bash -c "browser_choice"' \
--field="Terminal: ${color001}${select_terminal}${end}":LBL "" \
--field="<b>Terminal number</b>":fbtn '/bin/bash -c "terminal_choice"' \
--field="File manager: ${color001}${select_filemanager}${end}":LBL "" \
--field="<b>File manager number</b>":fbtn '/bin/bash -c "filemanager_choice"' \
--field="Development: ${color001}${select_dev}${end}":LBL "" \
--field="<b>Development number</b>":fbtn '/bin/bash -c "development_choice"' \
--field="Social: ${color001}${select_social}${end}":LBL "" \
--field="<b>Social number</b>":fbtn '/bin/bash -c "social_choice"' \
--field="Office: ${color001}${select_office}${end}":LBL "" \
--field="<b>Office number</b>":fbtn '/bin/bash -c "office_choice"' \
--field="Video editor: ${color001}${select_videoeditor}${end}":LBL "" \
--field="<b>Video editor number</b>":fbtn '/bin/bash -c "video_choice"' \
--field="Audio editor: ${color001}${select_audioeditor}${end}":LBL "" \
--field="<b>Audio editor number</b>":fbtn '/bin/bash -c "audio_choice"' \
--field="Image editor: ${color001}${select_imageeditor}${end}":LBL "" \
--field="<b>Image editor number</b>":fbtn '/bin/bash -c "image_choice"' \
--field="Text editor: ${color001}${select_texteditor}${end}":LBL "" \
--field="<b>Text editor number</b>":fbtn '/bin/bash -c "text_choice"' \
--button="_Enter all:/bin/bash -c 'all_entry_boxes'" \
--button="Exit":1

#----------------------------------------------------------#
# Unset variables and functions
#----------------------------------------------------------#

unset select_audioeditor
unset select_videoeditor
unset select_office
unset select_social
unset select_dev
unset select_filemanager
unset select_terminal
unset select_browser
unset MY_BROWSER
unset MY_TERMINAL
unset MY_FILEMANAGER
unset MY_DEV
unset MY_SOCIAL
unset MY_OFFICE
unset MY_SOUND_EDITOR
unset MY_VIDEO_EDITOR
unset MY_GTK_THEME
unset color001
unset end
unset browser_choice
unset terminal_choice
unset filemanager_choice
unset development_choice
unset social_choice
unset office_choice
unset video_choice
unset audio_choice
unset all_entry_boxes
unset MY_IMAGEEDITOR
unset select_imageeditor
unset image_choice
unset MY_TEXTEDITOR
unset select_texteditor
unset text_choice
