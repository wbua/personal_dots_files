#!/bin/bash

# Program command: sway_random_wallpaper.sh
# Description: Sway random wallpaper changer.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-01-2025
# Program_license: GPL 3.0
# Website: https://gitlab.com/wbua/
# Dependencies: swaymsg

# Master config file
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#---------------------------------------------------#
# Error checking
#---------------------------------------------------#

set -e

#---------------------------------------------------#
# User preferences
#---------------------------------------------------#

# Wallpaper directory
WALLPAPER_DIR="$MAIN_WALLPAPER_DIR"

# Sway currently set wallpaper
TEMP_WALLPAPER_RANDOM="$MAIN_SWAY_WALLPAPER"

# Description color
MY_COLOR_DES="$COLOR_DES"

#---------------------------------------------------#
# Check description color
#---------------------------------------------------#

   if [[ "$MY_COLOR_DES" == "#000000" ]]; then
     notify-send "Invert colors" "win shift x" && exit 1
   fi

#---------------------------------------------------#
# Create files and directories
#---------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -f "$HOME"/Documents/.random_sort_alpha.txt ]; then
     :
   else
     touch "$HOME"/Documents/.random_sort_alpha.txt
   fi

   if [ "$WALLPAPER_DIR" == "disable" ]; then
     notify-send "Wallpapers has been disabled" && exit 1
   elif [ -d "$WALLPAPER_DIR" ]; then
     :
   else
     notify-send "Wallpaper directory does not exist!" && exit 1
   fi

#---------------------------------------------------#
# Send wallpapers to text file
#---------------------------------------------------#

   if [ -z "$HOME"/Documents/.random_sort_alpha.txt ]; then
     find "$WALLPAPER_DIR" -type f -iname '*.*' | sort -R | tee ~/Documents/.random_sort_alpha.txt
   else
     :
   fi

#---------------------------------------------------#
# Genarate new random list file
#---------------------------------------------------#

   if [[ "$(grep RANDOM_GENERATE_LIST= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     find "$WALLPAPER_DIR" -type f -iname '*.*' | sort -R | tee ~/Documents/.random_sort_alpha.txt
   elif [[ "$(grep RANDOM_GENERATE_LIST= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     :
   else
     :
   fi

#---------------------------------------------------#
# Check update colors script variable
#---------------------------------------------------#

   if [[ "$(grep UPDATE_COLORS_SCRIPT= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     notify-send "UPDATE_COLORS_SCRIPT variable is enable" && exit 1
   elif [[ "$(grep UPDATE_COLORS_SCRIPT= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     :
   else
     :
   fi
 
#---------------------------------------------------#
# Random wallpaper with color auto (disable)
#---------------------------------------------------#

random_wall_disable() {

wall_file=$(ls "$WALLPAPER_DIR" | shuf -n1)
echo "$WALLPAPER_DIR/$wall_file" | tee ~/Documents/.temp_random_store.txt
temp_wall=$(cat ~/Documents/.temp_random_store.txt | tr -d '\n' | xargs -0 basename)

sed -i "s|.*\(MAIN_SWAY_WALLPAPER=\).*|MAIN_SWAY_WALLPAPER=\"$(printf '%s' "$WALLPAPER_DIR/$temp_wall")\"|" ~/bin/sway_user_preferences.sh || exit 1
setsid bash ~/bin/sway_load_settings.sh >/dev/null 2>&1 & disown
	
}

#---------------------------------------------------#
# Random wallpaper with color auto (enable)
#---------------------------------------------------#

random_wall_shuffle() {

wall_file=$(ls "$WALLPAPER_DIR" | shuf -n1)
echo "$WALLPAPER_DIR/$wall_file" | tee ~/Documents/.temp_random_store.txt
temp_wall=$(cat ~/Documents/.temp_random_store.txt | tr -d '\n' | xargs -0 basename)

sed -i "s|.*\(MAIN_SWAY_WALLPAPER=\).*|MAIN_SWAY_WALLPAPER=\"$(printf '%s' "$WALLPAPER_DIR/$temp_wall")\"|" ~/bin/sway_user_preferences.sh || exit 1
echo "$temp_wall" > "$HOME"/Documents/.walls_selected.txt    
bash ~/bin/wallpaper_auto_colors.sh
setsid bash ~/bin/sway_load_settings.sh >/dev/null 2>&1 & disown
	
}

#---------------------------------------------------#
# Random text notification
#---------------------------------------------------#

random_list_text() {

ACTION=$(dunstify --action="default,Reply" --action="forwardAction,Forward" "Random wallpaper does not exist in file" "Middle click notification!")

case "$ACTION" in
"default")
    cat ~/Documents/random_wallist_error.txt | wl-copy -n
    xdg-open ~/Documents/.random_sort_alpha.txt && exit 1
    ;;
"forwardAction")
    exit 1
    ;;
"2")
    exit 1
    ;;
esac

}

#---------------------------------------------------#
# Sort random list from start to finish
#---------------------------------------------------#

random_sort_list() {

end_of_list=$(tail -n 1 ~/Documents/.random_sort_alpha.txt | tr -d '\n' | xargs -0 basename)
start_of_list=$(head -n 1 ~/Documents/.random_sort_alpha.txt)
wall_choice=$(echo "$TEMP_WALLPAPER_RANDOM" | xargs -0 basename)
temp_edit_random=$(echo "$TEMP_WALLPAPER_RANDOM" | xargs -0 basename)
mychoice=$(awk "/$temp_edit_random/{getline; print}" ~/Documents/.random_sort_alpha.txt)
echo "$mychoice" | xargs -0 basename | tee "$HOME"/Documents/.walls_selected.txt
random_wall_tempfile=$(cat "$HOME"/Documents/.walls_selected.txt)

   if [ -f "$WALLPAPER_DIR"/"$random_wall_tempfile" ]; then
     :
   else
     echo "$WALLPAPER_DIR"/"$random_wall_tempfile" | tee ~/Documents/random_wallist_error.txt
     random_list_text      
   fi

   if [[ "$wall_choice" == "$end_of_list" ]]; then
     sed -i "s|.*\(MAIN_SWAY_WALLPAPER=\).*|MAIN_SWAY_WALLPAPER=\"$(printf '%s' "$start_of_list")\"|" ~/bin/sway_user_preferences.sh || exit 1
     bash ~/bin/wallpaper_auto_colors.sh
     setsid bash ~/bin/sway_load_settings.sh >/dev/null 2>&1 & disown
   else
     sed -i "s|.*\(MAIN_SWAY_WALLPAPER=\).*|MAIN_SWAY_WALLPAPER=\"$(printf '%s' "$mychoice")\"|" ~/bin/sway_user_preferences.sh || exit 1
     bash ~/bin/wallpaper_auto_colors.sh
     setsid bash ~/bin/sway_load_settings.sh >/dev/null 2>&1 & disown
   fi
	
}

#---------------------------------------------------#
# Random wallpaper with color auto (enable)
#---------------------------------------------------#

random_wall_enable() {

   if [[ "$(grep RANDOM_WALLPAPER_SHUFFLE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     random_sort_list
   elif [[ "$(grep RANDOM_WALLPAPER_SHUFFLE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then   
     random_wall_shuffle
   else
     :
   fi
	
}

#---------------------------------------------------#
# Use wallpaper colors for theming
#---------------------------------------------------#

auto_color_wall() {

   if [[ "$(grep COLOR_AUTO= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     random_wall_enable  
   elif [[ "$(grep COLOR_AUTO= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     random_wall_disable     
   fi

}

#---------------------------------------------------#
# Checks if random preview is enable or disable
#---------------------------------------------------#

wallpaper_modes() {

   if [[ "$(grep RANDOM_PREVIEW= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     echo "$TEMP_WALLPAPER_RANDOM" > ~/Documents/.temp_random_store.txt || exit 1
     sed -i "s|.*\(RANDOM_TEMPORARY_WALLPAPER=\).*|RANDOM_TEMPORARY_WALLPAPER=\"$(printf '%s' "$MAIN_SWAY_WALLPAPER")\"|" ~/bin/sway_user_preferences.sh || exit 1
     bash ~/bin/yad_random_preview.sh
   elif [[ "$(grep RANDOM_PREVIEW= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     wall_file=$(ls "$WALLPAPER_DIR" | shuf -n1)
     echo "$WALLPAPER_DIR/$wall_file" | tee ~/Documents/.temp_random_store.txt
     auto_color_wall
   fi

}

#---------------------------------------------------#
# Checks the wallpaper and wallpaper directory
#---------------------------------------------------#

check_wall_dir=$(echo "$MAIN_SWAY_WALLPAPER" | xargs -0 dirname)

   if [[ ! "$check_wall_dir" == "$WALLPAPER_DIR" ]]; then
     notify-send "Sway wallpaper path does not contain wallpaper directory path" "in sway_user_preferences.sh" && exit 1
   else
     wallpaper_modes
   fi
	
