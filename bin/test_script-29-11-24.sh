#!/bin/bash

current_workspace=$(swaymsg -t get_outputs | jq -r '.[] | select(.focused)' | jq -r '.current_workspace')
all_workspaces=$(swaymsg -t get_workspaces | jq '.[] | select(.focused == false) | .name' | tr -d '"' | xargs)

#
#
