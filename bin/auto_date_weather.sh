#!/bin/bash

# Sway user preferences 
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

ADW_MOD="$DATE_WEATHER_MOD"

   if [ -f "$HOME"/Documents/.date_weather_toggle.txt ]; then
     :
   else
     touch "$HOME"/Documents/.date_weather_toggle.txt
   fi

echo "$ADW_MOD" > "$HOME"/Documents/.date_weather_toggle.txt
