#!/bin/bash

killall yad
rm -f "$HOME"/Pictures/.tempfullscrns/*.png
rm -f "$HOME"/.cvssgtk/*.png
sleep 1
maim --hidecursor | tee "$HOME/Pictures/.tempfullscrns/scn_$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
convert "$HOME"/Pictures/.tempfullscrns/*.png -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/screenshots/
yad_screenshots.sh
