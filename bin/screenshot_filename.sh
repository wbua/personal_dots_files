#!/bin/bash

# Program command: screenshot_filename.sh
# Description: Rename screenshot file.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 12-02-2025
# Program_license: GPL 3.0
# Dependencies: yad

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#--------------------------------------------------------#
# Error checking
#--------------------------------------------------------#

set -e 

#--------------------------------------------------------#
# Preferences
#--------------------------------------------------------#

# Wallpaper directory
SCREENSHOT_DIR="$MAIN_SCREENSHOT_DIR"
export SCREENSHOT_DIR

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#--------------------------------------------------------#
# Copy screenshot with date and time
#--------------------------------------------------------#

copy_screenshot() {

killall yad
cp ~/Pictures/.tempfullscrns/*.* "$SCREENSHOT_DIR"
	
}
export -f copy_screenshot

#--------------------------------------------------------#
# Give screenshot a specific filename
#--------------------------------------------------------#

set_filename() {

killall yad

# Yad dialog
FILE_RENAME=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--width=500 \
--height=110 \
--borders=20 \
--title="Rename screenshot" \
--text-align="center" \
--text="\nType filename for screenshot...\nUse same extension as below\n$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | \
xargs -0 ls -tr | tail -n 1 | xargs -0 basename)\n" \
--separator= \
--button="Save":0 \
--button="Exit":1)
export FILE_RENAME
[[ -z "$FILE_RENAME" ]] && exit

# Location of temp screenshot
choice=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
export choice

# Rename screenshot
mv "$choice" ~/Pictures/.tempfullscrns/"$FILE_RENAME"

# Copy screenshot from temp directory to main directory
cp ~/Pictures/.tempfullscrns/*.* "$SCREENSHOT_DIR"

# Copy screenshot to clipboard
wl-copy --type image/png < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)" || wl-copy --type image/jpeg < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"

}
export -f set_filename

#--------------------------------------------------------#
# Converts screenshot to a thumbnail
#--------------------------------------------------------#

convert "$HOME"/Pictures/.tempfullscrns/*.png -resize 512x512 "$HOME"/Pictures/.temp_srnshots_thumbs/current_screenshot.png || convert "$HOME"/Pictures/.tempfullscrns/*.jpg -resize 512x512 "$HOME"/Pictures/.temp_srnshots_thumbs/current_screenshot.jpg

#--------------------------------------------------------#
# Yad dialog filename
#--------------------------------------------------------#

only_extension=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 basename | \
tr -d '\n' | awk -F '.' '{print $NF}')

GTK_THEME="$MY_GTK_THEME" yad --form \
--width=400 \
--height=80 \
--borders=20 \
--image="$HOME/Pictures/.temp_srnshots_thumbs/current_screenshot.${only_extension}" \
--title="Rename screenshot" \
--buttons-layout="center" \
--separator= \
--button="_Filename:/bin/bash -c 'set_filename'" \
--button="_Date:/bin/bash -c 'copy_screenshot'" \
--button="Exit":1

#--------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------#

unset choice
unset FILE_RENAME
unset SCREENSHOT_DIR
unset MY_GTK_THEME
unset switch_to_current
unset copy_screenshot
unset set_filename
