#!/bin/bash

# Program command: create_dunst_config.sh
# Description: This script creates the dunst color config file.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-01-2025
# Program_license: GPL 3.0
# Dependencies: dunst

# Exit with non zero status
set -e

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Color config
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#--------------------------------------------------------#
# User preferences
#--------------------------------------------------------#

# Dunst backgound color
MY_COLOR_BG="$COLOR_BG"

# Dunst text color
MY_COLOR_ICON="$COLOR_ICON"

# Dunst flame color
MY_COLOR_ALERT="$COLOR_ALERT"

#--------------------------------------------------------#
# Create the dunst color config file
#--------------------------------------------------------#

   if [ ! -d "$HOME"/.config/dunst/dunstrc.d ]; then
     mkdir -p "$HOME"/.config/dunst/dunstrc.d
   fi

#--------------------------------------------------------#
# Create the dunst color config file
#--------------------------------------------------------#

# Create the file 'example.txt' and write the content
cat <<EOF > ~/.config/dunst/dunstrc.d/colors.conf
[urgency_low]
background = "$MY_COLOR_BG"
foreground = "$MY_COLOR_ICON"
frame_color = "$MY_COLOR_ALERT"

[urgency_normal]
background = "$MY_COLOR_BG"
foreground = "$MY_COLOR_ICON"
frame_color = "$MY_COLOR_ALERT"

[urgency_critical]
background = "$MY_COLOR_BG"
foreground = "#ff0031"
frame_color = "#ff0031"

EOF

# Kill dunst process
killall dunst
