#!/bin/bash
# shellcheck disable=SC2089
# shellcheck disable=SC2090

# Program command: yad_lofi.sh
# Description: Plays lofi radio stations.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 07-12-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad, mpv, mpv-mpris, playerctl, xclip, material icons, custom gtk.css

################################################
################################################
# Readme start #################################
################################################
################################################

# Follow the instructions in the readme only.
# These instructions are for people using Ubuntu.
# If you are using a different system, you are on your own sorry!

################################################
# Installation:
################################################

# When I use double quotes, it is just for emphasis in readme.
# Do not type the double quotes, in less told to do so. 
# Do not type the "$" character, in less told to do so.

#-----------------------------------------------

# Update your system first.
# Open terminal and type:

# $ "sudo apt update"

#-----------------------------------------------

# Now install the dependencies.
# Open terminal and type:

# $ "sudo apt install yad mpv playerctl xclip"

#-----------------------------------------------

# You will need to install "mpv-mpris" (see in Download links)
# You will need to install "material icons" (see in Download links)
# You will need to install "gtk.css" (see in Download links)

#-----------------------------------------------

# To install the fonts I would use font-manager.
# Open the terminal and type:

# Step 1: $ "sudo apt update"
# Step 2: $ "sudo apt install font-manager"

#-----------------------------------------------

# Put the script "yad_lofi.sh" into "/home/user/.local/bin/"

#-----------------------------------------------

# You will need to make the script executable.
# Open the terminal and type:

# $ "cd" and then type "cd .local/bin/"

# Then type $ "chmod u+x yad_lofi.sh"

# Keep terminal in ".local/bin/"

#-----------------------------------------------

# Run the script in the terminal.

# $ "yad_lofi.sh" press enter to run.

# In the terminal type control+c to kill the script.
# If control+c does not kill script, then open a new terminal.
# In that new terminal type $ "killall yad_lofi.sh"

################################################
# Download links:
################################################

# mpv-mpris: https://github.com/hoyon/mpv-mpris/releases/
# material icons: https://github.com/google/material-design-icons/blob/master/font/MaterialIcons-Regular.ttf
# material icons: https://github.com/google/material-design-icons/blob/master/font/MaterialIconsOutlined-Regular.otf
# gtk.css: https://sourceforge.net/projects/jm-dots/files/lofi/

################################################
# Other information:
################################################

# Stores the url's in a text file: "/home/user/Documents/mainprefix/lofi_stations.txt"
# The link format should just be the url itself: "http://stream.radio.example"

# Randomly shuffles backgrounds via button, put your wallpapers in: "/home/user/Pictures/lofi_images/"
# The wallpapers must be 1920x1080.

# Put some quotes into ""$HOME"/Documents/.lofi_quotes.txt"

################################################
################################################
# Readme end ###################################
################################################
################################################

#------------------------------------------------------------#
# Kill existing yad instance
#------------------------------------------------------------#

killall yad

#------------------------------------------------------------#
# General variables
#------------------------------------------------------------#

# Yad box width
width="1400"
export res_width

# Yad box height
height="800"
export res_height

#------------------------------------------------------------#
# Create files and directories
#------------------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi
   
   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi
   
   if [ -d "$HOME"/Pictures/lofi_images ]; then
     :
   else
     mkdir "$HOME"/Pictures/lofi_images
   fi
   
   if [ -d "$HOME"/Pictures/.temp_lofi ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temp_lofi
   fi
   
   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ -f "$HOME"/Documents/mainprefix/lofi_stations.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/lofi_stations.txt
   fi
   
   if [ -f "$HOME"/Documents/.temp_radio_shuf.txt ]; then
     :
   else
     touch "$HOME"/Documents/.temp_radio_shuf.txt
   fi
   
   if [ -f "$HOME"/Documents/.lofi_track.txt ]; then
     :
   else
     touch "$HOME"/Documents/.lofi_track.txt
   fi
   
   if [ -f "$HOME"/Documents/track_log.txt ]; then
     :
   else
     touch "$HOME"/Documents/track_log.txt
   fi
   
   if [ -f "$HOME"/Documents/.lofi_quotes.txt ]; then
     :
   else
     touch "$HOME"/Documents/.lofi_quotes.txt
   fi
  
   if [ -f "$HOME"/Documents/.lofi_volume.txt ]; then
     :
   else
     touch "$HOME"/Documents/.lofi_volume.txt
     echo "playerctl --player mpv volume 1%" > "$HOME"/Documents/.lofi_volume.txt
   fi
 
#------------------------------------------------------------#
# Check if files and directories are empty
#------------------------------------------------------------#
 
check_links=$(cat "$HOME"/Documents/mainprefix/lofi_stations.txt)
export check_links
 
   # Checks if radio links exist 
   if [[ -z "$check_links" ]]; then
     notify-send "No radio links in text file!"
     killall yad_lofi.sh
   else
     :
   fi  
  
   # Checks if wallpapers exist
   if [[ -z "$(ls -A "$HOME"/Pictures/lofi_images)" ]]; then
     notify-send "Directory does not contain wallpapers!"
     killall yad_lofi.sh
   else
     :
   fi  
  
#------------------------------------------------------------#
# Colors
#------------------------------------------------------------#

color1="<span color='#FFFFFF' font_family='Monospace' weight='bold' font='25'>"
export color1
color2="<span color='#FFFFFF' font_family='Monospace' weight='bold' font='25'>"
export color2
color3="<span color='#FFFFFF' font_family='Monospace' weight='bold' font='40'>"
export color3
color4="<span color='#FFFFFF' font_family='Monospace' weight='bold' font='18'>"
export color4
color5="<span color='#FB00FF' font_family='Monospace' font='15'>"
export color5
color6="<span color='#FFFFFF' font_family='Monospace' weight='bold' font='15'>"
export color6
end="</span>"
export end

#------------------------------------------------------------#
# Greeting
#------------------------------------------------------------#

_greeting() {

hour=$(date +%H)
export hour

   if [ "$hour" -ge 12 ] && [ "$hour" -lt 19 ]; then
     echo "${color1}Good afternoon${end}" "${color2} $USER${end}"
   fi

   if [ "$hour" -ge 00 ] && [ "$hour" -lt 12 ]; then
     echo "${color1}Good morning${end}" "${color2} $USER${end}"
   fi

   if [ "$hour" -ge 19 ] && [ "$hour" -lt 24 ]; then
     echo "${color1}Good evening${end}" "${color2} $USER${end}"
   fi	
	
}
export -f _greeting

#------------------------------------------------------------#
# Change background
#------------------------------------------------------------#

_change_background() {

killall yad	
find_images=$(find ~/Pictures/lofi_images/ -type f -iname '*.*' | shuf -n1)
export find_images
rm -f "$HOME"/Pictures/.temp_lofi/*.*
cp "$find_images" "$HOME"/Pictures/.temp_lofi/
mv "$HOME"/Pictures/.temp_lofi/*.* "$HOME"/Pictures/.temp_lofi/lofi.jpg 
sleep 1
yad_lofi.sh
		
}
export -f _change_background

#------------------------------------------------------------#
# Refresh yad
#------------------------------------------------------------#

_radio_info() {


killall yad ; yad_lofi.sh
	
}
export -f _radio_info

#------------------------------------------------------------#
# Shuffle radio stations
#------------------------------------------------------------#

_shuffle_radio() {

text_file=$(cat "$HOME"/Documents/mainprefix/lofi_stations.txt)
export text_file
killall mpv
echo "$text_file" | shuf -n1 | tee "$HOME"/Documents/.temp_radio_shuf.txt
cat "$HOME"/Documents/.temp_radio_shuf.txt | xargs mpv

}
export -f _shuffle_radio

#------------------------------------------------------------#
# Plays radio
#------------------------------------------------------------#

_play_radio() {

     shuf_file=$(cat "$HOME"/Documents/.temp_radio_shuf.txt)
	 export shuf_file 
   if [[ -z "$shuf_file" ]]; then
     text_file=$(cat "$HOME"/Documents/mainprefix/lofi_stations.txt)
     export text_file
     killall mpv
     echo "$text_file" | shuf -n1 | tee "$HOME"/Documents/.temp_radio_shuf.txt
     cat "$HOME"/Documents/.temp_radio_shuf.txt | xargs mpv
   elif [[ "$(playerctl status -p mpv)" == 'Paused' ]]; then
   playerctl --player mpv play
   else
     killall mpv	
     cat "$HOME"/Documents/.temp_radio_shuf.txt | xargs mpv
   fi
	
}
export -f _play_radio

#------------------------------------------------------------#
# Stop mpv
#------------------------------------------------------------#

_stop_mpv() {

killall mpv	
	
}
export -f _stop_mpv

#------------------------------------------------------------#
# Pause mpv
#------------------------------------------------------------#

_pause_mpv() {

   if [[ "$(pidof mpv)" ]]; then	
     playerctl --player mpv pause	
   else
     :
   fi  
     
}
export -f _pause_mpv

#------------------------------------------------------------#
# Open text file containing radio stations
#------------------------------------------------------------#

_radio_textfile() {

GTK_THEME="yaru"
killall yad	
xdg-open "$HOME"/Documents/mainprefix/lofi_stations.txt
	
}
export -f _radio_textfile

#------------------------------------------------------------#
# Open text file containing track titles
#------------------------------------------------------------#

_title_log() {
	
GTK_THEME="yaru"
killall yad	
xdg-open "$HOME"/Documents/track_log.txt
	
}
export -f _title_log

#------------------------------------------------------------#
# Current track
#------------------------------------------------------------#

_current_track() {

   if [[ "$(pidof mpv)" ]]; then	
      notify-send "$(playerctl --player mpv metadata -f "{{title}}" | cut -c 1-90 | tr "&" "+")"
      notify-send "$(playerctl --player mpv metadata -f "{{xesam:url}}" | cut -c 1-90 | tr "&" "+")"
   else
     notify-send "Not playing"
   fi
	
}
export -f _current_track

#------------------------------------------------------------#
# Send track title to text file
#------------------------------------------------------------#

_clipboard() {

   if [[ "$(pidof mpv)" ]]; then
     sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/track_log.txt
     playerctl --player mpv metadata -f "{{title}}" | tee -a "$HOME"/Documents/track_log.txt /dev/null
     notify-send "Track copied!"
   else
     :
   fi
}
export -f _clipboard

#------------------------------------------------------------#
# Change volume
#------------------------------------------------------------#

_change_volume() {
	
volume_file=$(cat "$HOME"/Documents/.lofi_volume.txt)
export volume_file	
   if [[ "$volume_file" == 'playerctl --player mpv volume 1%' && "$(pidof mpv)" ]]; then
     echo "playerctl --player mpv volume 0.7%" | tee "$HOME"/Documents/.lofi_volume.txt
	 playerctl --player mpv volume 0.7%
	 notify-send "Volume 70%"
   elif [[ "$volume_file" == 'playerctl --player mpv volume 0.7%' && "$(pidof mpv)" ]]; then
     echo "playerctl --player mpv volume 0.5%" | tee "$HOME"/Documents/.lofi_volume.txt
	 playerctl --player mpv volume 0.5%
	 notify-send "Volume 50%"
   elif [[ "$volume_file" == 'playerctl --player mpv volume 0.5%' && "$(pidof mpv)" ]]; then
     echo "playerctl --player mpv volume 0.3%" | tee "$HOME"/Documents/.lofi_volume.txt
	 playerctl --player mpv volume 0.3%
	 notify-send "Volume 30%"
   elif [[ "$volume_file" == 'playerctl --player mpv volume 0.3%' && "$(pidof mpv)" ]]; then
     echo "playerctl --player mpv volume 1%" | tee "$HOME"/Documents/.lofi_volume.txt
	 playerctl --player mpv volume 1%
	 notify-send "Volume 100%"
   else
     :
   fi
   
}
export -f _change_volume

#------------------------------------------------------------#
# Quotes
#------------------------------------------------------------#

_pro_info() {

quote_file=$(cat "$HOME"/Documents/.lofi_quotes.txt)
export quote_file

   if [[ -z "$quote_file" ]]; then
     echo "No quotes in file"
   else
     echo "Quote:" "$(cat "$HOME"/Documents/.lofi_quotes.txt | shuf -n1)"	
   fi

}
export _pro_info

#------------------------------------------------------------#
# Check status of mpv
#------------------------------------------------------------#

_mpv_check() {
	
   if [[ "$(playerctl status -p mpv)" == 'Playing' ]]; then
     notify-send "Playing"
   elif	[[ "$(playerctl status -p mpv)" == 'Paused' ]]; then
	 notify-send "Paused"
   else
     notify-send "Not Playing"
   fi  
     
}
export -f _mpv_check

#------------------------------------------------------------#
# Check current volume level
#------------------------------------------------------------#

_check_volume() {

   if [[ "$(cat "$HOME"/Documents/.lofi_volume.txt)" == 'playerctl --player mpv volume 1%' ]]; then
     notify-send "Volume 100%"
   elif [[ "$(cat "$HOME"/Documents/.lofi_volume.txt)" == 'playerctl --player mpv volume 0.7%' ]]; then
     notify-send "Volume 70%"
   elif [[ "$(cat "$HOME"/Documents/.lofi_volume.txt)" == 'playerctl --player mpv volume 0.5%' ]]; then
     notify-send "Volume 50%"
   elif [[ "$(cat "$HOME"/Documents/.lofi_volume.txt)" == 'playerctl --player mpv volume 0.3%' ]]; then  
     notify-send "Volume 30%"
   else
     :
   fi  
          
}
export -f _check_volume

#------------------------------------------------------------#
# Main
#------------------------------------------------------------#

main() {

GTK_THEME="alt-dialog8" yad --form --separator= --title="Lofi radio player" \
--width="$width" --height="$height" \
--buttons-layout="spread" \
--center \
--columns=1 \
--borders="0" \
--align="center" \
--text="$(_pro_info)\n\n${color3}$(date '+%H:%M')${end}\n${color4}$(date '+%d-%m-%Y')${end}\n\n$(_greeting)\n\n" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='90'></span>:/bin/bash -c '_play_radio'" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='30'></span>:/bin/bash -c '_pause_mpv'" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='30'></span>:/bin/bash -c '_stop_mpv'" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='30'></span>:/bin/bash -c '_shuffle_radio'" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='30'></span>:/bin/bash -c '_change_volume'" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='30'></span>:/bin/bash -c '_check_volume'" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='30'></span>:/bin/bash -c '_mpv_check'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='30'></span>:/bin/bash -c '_current_track'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='30'></span>:/bin/bash -c '_change_background'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='30'></span>:/bin/bash -c '_clipboard'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='30'></span>:/bin/bash -c '_title_log'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='30'></span>:/bin/bash -c '_radio_textfile'" \
--button="<span color='#ffffff' font_family='Material Icons' font='30'></span>":1

}
export -f main
main

#------------------------------------------------------------#
# Unset variables and functions
#------------------------------------------------------------#

# Functions
unset _greeting
unset _radio_info
unset _play_radio
unset _stop_mpv
unset _shuffle_radio
unset _pause_mpv
unset _radio_textfile
unset _change_background
unset _clipboard
unset _current_track
unset _title_log
unset _change_volume
unset _pro_info
unset _mpv_check
unset _check_volume

# Variables
unset key
unset height
unset width
unset main
unset hour
unset color1
unset color2
unset color3
unset color4
unset color5
unset color6
unset end
unset text_file
unset shuf_file
unset res_height
unset res_width
unset find_images
unset volume_file
unset quote_file
unset check_links
