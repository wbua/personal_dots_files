#!/bin/bash

#--------------------------------------------------------------------#
# This is where full size screenshots are stored temporary
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Pictures/.tempfullscrns ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempfullscrns
   fi

#--------------------------------------------------------------------#
# Main
#--------------------------------------------------------------------#

rm -f "$HOME"/Pictures/.tempfullscrns/*.png
sleep 1 ; maim --hidecursor | tee "$HOME"/Pictures/.tempfullscrns/$(date +'%d-%m-%Y-%H%M%S').png | xclip -selection clipboard -t image/png
cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/screenshots/
one=$(find "$HOME"/Pictures/.tempfullscrns/ -type f | xargs ls -tr | tail -n 1)
notify-send --icon="$one" "Screenshot" "taken"    


