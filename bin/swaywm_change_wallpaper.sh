#!/bin/bash

# Program command: swaywm_change_wallpaper.sh
# Description: This changes the swaywm desktop wallpaper.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 20-12-2023
# Program_license: GPL 3.0
# Dependencies: rofi

killall rofi
pkill -f rofi

# Wallpapers location
screendir="$HOME/Pictures/sway_wallpapers/"

# Wallpapers location
# Do not add ~, $HOME or /home/user/.
# Do not add / at the end
locationwall="Pictures/sway_wallpapers"

# My swaywm config
my_swaywm_config="$HOME/.config/sway/config"

# Line thats sets the wallpaper
lineset="29s"

# Shows wallpaper in rofi
choice=$(find "$screendir" -maxdepth 1 -iname '*.*' -printf '%T@ %p\n' | sort -rn | cut -f2- -d" " | \
awk -F/ '{print $NF}' | while read -r A ; do  echo -en "$A\x00icon\x1f~/$locationwall/$A\n" ; done | \
rofi -dmenu -i -p ''  -theme-str 'entry {placeholder : "Search container";}' \
-show-icons -theme-str 'element-icon { size: 16em;}' \
-theme-str 'window {fullscreen: true; location: center; padding: 0px 0px 0px 0px; border: 0px 0px 0px 0px;}' \
-theme-str '* {background: rgba ( 0, 0, 0, 90 % );}' \
-theme-str 'entry {vertical-align: 0.9; placeholder: "Search keybindings..."; placeholder-color: #FF004D; background-color: #E6000000;}' \
-theme-str 'element {orientation: vertical; }' \
-theme-str 'element.selected.normal {background-color: transparent; border-radius: 10; border-color: #FF004D; padding: 0px 0px 0px 0px; border: 0 0 0 0; text-color: #FF004D;}' \
-theme-str 'inputbar {children: [ prompt,textbox-prompt-colon,entry,case-indicator ]; background-color: #E6000000; border: 0 0 0 0;}' \
-theme-str 'element-text {horizontal-align: 0.5; }' \
-theme-str 'listview {dynamic: false; scrollbar: true; columns: 2; lines: 2; }')
[ -z "$choice" ] && exit 0 || killall rofi

   # Changes your current wallpaper and your swaywm config.
   if [ "$choice" ]; then
     wfile="$HOME/Pictures/.swaywm_wallpaper.txt"
     image=$(printf '%s\n' "${screendir}${choice}")
     echo "$image" > "$wfile"
     sed -i "$lineset|^.*$|output * bg \"$(head "$wfile")\" fill|" "$my_swaywm_config"
     swaymsg reload
   else
     echo "program terminated" || exit 1
   fi

