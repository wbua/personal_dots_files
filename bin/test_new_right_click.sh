#!/bin/bash

# Function01
my_function01() {

notify-send "function01"
	
}
export -f my_function01

# Second function
my_second_function() {

notify-send "second function"
	
}
export -f my_second_function

# Yad form dialog
selected_choice=$(GTK_THEME="Yaru-dark" yad --form \
 --width=300 \
 --height=300 \
 --title="Settings box" \
 --text-align="center" \
 --text="\nSettings dialog\n" \
 --borders=10 \
 --separator="|" \
 --field="Convert:CB" 'None!Resize!Pdf' \
 --field="<b>Numbers</b>":fbtn '/bin/bash -c "my_function01"' \
 --field="<b>Letters</b>":fbtn '/bin/bash -c "my_second_function"' \
 --field="Resize ":CE \
 --field="File ":FL \
 --field="File ":DIR \
 --button="Ok":0 \
 --button="Exit":1)
 [[ -z "$selected_choice" ]] && exit

# Drop down list
field1=$(echo "$selected_choice" | awk -F '|' '{print $1}')
# Resize
field2=$(echo "$selected_choice" | awk -F '|' '{print $4}')
# File selection
field3=$(echo "$selected_choice" | awk -F '|' '{print $5}')
# Directory selections
field4=$(echo "$selected_choice" | awk -F '|' '{print $6}')

echo "$field1"
echo "$field2"
echo "$field3"
echo "$field4"

# Unset functions
unset my_function01
unset my_second_function

# Main
if [[ -n "$field1" ]]; then
    echo "var1 is not empty: $var1"
fi

if [[ -n "$field2" ]]; then
    echo "var2 is not empty: $var2"
fi

if [[ -n "$field3" ]]; then
    echo "var3 is not empty: $var3"
fi

if [[ -n "$field4" ]]; then
    echo "var3 is not empty: $var3"
fi
