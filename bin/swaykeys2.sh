#!/bin/bash

# Program command: swaykeys2.sh 
# Description: Show keybinds in fzf for swaywm and execute them.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 20-04-2024
# Program_license: GPL 3.0
# Dependencies: fzf, wtype

##################################################
# Section 1:
# You will also have to add the extra condition in the if statement:
# If statement example:

#------------------------------------------------#

# elif [[ "$choice" == 'win e' ]]; then
# killall kitty
# setsid ~/bin/swayrun_layout_toggle.sh >/dev/null 2>&1 & disown
# pkill -f ~/bin/swaykeys2.sh

#------------------------------------------------#

##################################################
# Section 2:

# You may have to create scripts like this below to run specific keybinds. 
# Script example: 

#------------------------------------------------#

#!/bin/bash

# sleep 0.3
# wtype -M win -k e

#------------------------------------------------#

##################################################

#-------------------------------------------------------#
# Colors
#-------------------------------------------------------#

# Keybindings
color1=$(tput setaf 76)
export color1
#
color2=$(tput setaf 9)
export color2
# Color end
end=$(tput sgr0)
export end

#-------------------------------------------------------#
# Shows menu items in fzf
#-------------------------------------------------------#

launch() {

echo -e "Start a terminal: ${color1}win return${end}"
echo -e "Kill focused window (close): ${color1}win shift q${end}${end}"
#echo -e "Install software: ${color1}altgr u${end}${end}"
#echo -e "Yad launcher: ${color1}win period${end}${end}"
#echo -e "Custom launcher: ${color1}win Space${end}${end}"
#echo -e "Color picker: ${color1}altgr i${end}${end}"
#echo -e "App launcher: ${color1}win d${end}${end}"
#echo -e "Gtk theme changer: ${color1}win t${end}${end}"
#echo -e "i3blocks program title: ${color1}win g${end}${end}"
#echo -e "i3blocks track title: ${color1}win shift g${end}${end}"
#echo -e "Turn monitor off: ${color1}win m${end}${end}"
#echo -e "Turn monitor on: ${color1}win shift m${end}${end}"
echo -e "FZF launcher: ${color1}win slash${end}${end}"
#echo -e "Connect to wifi: ${color1}win c${end}${end}"
#echo -e "Change swaybar theme: ${color1}altgr t${end}${end}"
#echo -e "Dictionary: ${color1}win shift d${end}${end}"
#echo -e "Shuffle toggle audacious: ${color1}altgr alt s${end}${end}"
#echo -e "Spellcheck: ${color1}win alt s${end}${end}"
#echo -e "Create pdf file: ${color1}altgr alt p${end}"
#echo -e "Toggle audacious playlists: ${color1}altgr alt l${end}"
#echo -e "Play audacious player: ${color1}win shift p${end}"
#echo -e "Stop audacious player: ${color1}win shift s${end}"
#echo -e "Create and open bash scripts: ${color1}altgr s${end}"
#echo -e "Bookmarks: ${color1}altgr b${end}"
#echo -e "Emoji's (copies to clipboard): ${color1}altgr e${end}"
#echo -e "Autostart programs: ${color1}altgr a${end}"
#echo -e "Configuration files: ${color1}altgr c${end}"
#echo -e "Find files (search): ${color1}altgr f${end}"
#echo -e "Quicklinks: ${color1}altgr q${end}"
#echo -e "Change wallpaper, press enter to set: ${color1}altgr w${end}"
#echo -e "Restart gpaste daemon (clipboard): ${color1}altgr x${end}"
echo -e "Terminal scratchpad: ${color2}altgr shift p${end}"
#echo -e "Create QR Codes: ${color1}win q${end}"
#echo -e "Control brave with playerctl: ${color1}altgr y${end}"
#echo -e "Screencast (record screen): ${color1}win shift r${end}"
#echo -e "Internet radio: ${color1}altgr r${end}"
#echo -e "Refresh weather data: ${color1}altgr d${end}"
#echo -e "Gmrun: ${color1}win F2${end}"
#echo -e "Lock Screen: ${color1}win alt l${end}"
#echo -e "Window switcher: ${color1}alt tab${end}"
#echo -e "Bring window to current workspace: ${color1}alt escape${end}"
#echo -e "Fullscreen screenshot: ${color1}print${end}"
#echo -e "Fullscreen delay 5 second screenshot: ${color1}win shift print${end}"
#echo -e "Select screenshot: ${color1}win print${end}"
#echo -e "Open last screenshot taken: ${color1}win o${end}"
echo -e "Move to workspace on the left: ${color1}win ctrl left${end}"
echo -e "Move to workspace on the right: ${color1}win ctrl right${end}"
#echo -e "Gpaste clipboard: ${color1}win shift v${end}"
echo -e "Move back and forth on workspace: ${color1}win tab${end}"
echo -e "Reload the configuration file: ${color1}win shift c${end}"
echo -e "Move window to next workspace and follow: ${color1}win alt left${end}"
echo -e "Exit sway: ${color1}win shift e${end}"
echo -e "Layout tabbed: ${color1}win w${end}"
echo -e "Layout toggle split: ${color1}win e${end}"
echo -e "Fullscreen: ${color1}win f${end}"
echo -e "Floating toggle: ${color1}win shift space${end}"
echo -e "Move scratchpad: ${color1}win shift minus${end}"
echo -e "Scratchpad show: ${color1}win minus${end}"
echo -e "Resize windows mode: ${color1}win r${end}"
echo -e "Move focus left: ${color1}win left${end}"
echo -e "Move focus down: ${color1}win down${end}"
echo -e "Move focus up: ${color1}win up${end}"
echo -e "Move focus right: ${color1}win right${end}"
echo -e "Swap window left: ${color1}win shift left${end}"
echo -e "Swap window down: ${color1}win shift down${end}"
echo -e "Swap window up: ${color1}win shift up${end}"
echo -e "Swap window right: ${color1}win shift right${end}"
echo -e "Swap focus between tiling and floating area: ${color1}alt space${end}"

}
export -f launch

#-------------------------------------------------------#
# Kill script
#-------------------------------------------------------#

kill_script() {

pkill -f ~/bin/swaykeys2.sh
	
}
export -f kill_script

#-------------------------------------------------------#
# fzf settings
#-------------------------------------------------------#

_fzf() {

fzf --print-query \
--cycle \
--padding 5% \
--reverse \
--ansi \
--info=inline \
--bind='F9:execute(kill_script {})' \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:#660020,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'FZF Launcher...> ' \
--header="
Run keybind: ${color1}enter${end} | kill script: ${color1}F9${end}
If keybind is in red then press keybind directory.

"
	
}
export -f _fzf

#-------------------------------------------------------#
# Main
#-------------------------------------------------------#

main() {

choice=$(launch | sort | _fzf | tr -d '\n' | awk -F ':' '{print $2}' | sed 's|${end}||' | sed 's| ||')
[[ -z "$choice" ]] && exit 0

   # Print
   if [[ "$choice" == 'print' ]]; then
     setsid wtype -k "$choice" >/dev/null 2>&1 & disown
     pkill -f ~/bin/swaykeys2.sh
   # Ignore key
   elif [[ "$choice" == 'altgr p' ]]; then
     :
   # Ignore key
   elif [[ "$choice" == 'win space' ]]; then
     :
   # Change layout
   elif [[ "$choice" == 'win e' ]]; then
     killall xfce4-terminal
     setsid ~/bin/swayrun_layout_toggle.sh >/dev/null 2>&1 & disown
     pkill -f ~/bin/swaykeys2.sh
   # Layout tabbed
   elif [[ "$choice" == 'win w' ]]; then
     killall xfce4-terminal
     setsid ~/bin/swayrun_layout_toggle_tab.sh >/dev/null 2>&1 & disown
     pkill -f ~/bin/swaykeys2.sh
   # Float window
   elif [[ "$choice" == 'win shift space' ]]; then
     killall xfce4-terminal
     setsid ~/bin/swayrun_float.sh >/dev/null 2>&1 & disown
     pkill -f ~/bin/swaykeys2.sh
   # Pdf
   elif [[ "$choice" == 'altgr alt p' ]]; then
     setsid wtype -M altgr -M alt -k p >/dev/null 2>&1 & disown
     pkill -f ~/bin/swaykeys2.sh
   # Shuffle playlists audacious
   elif [[ "$choice" == 'altgr alt l' ]]; then
     setsid wtype -M altgr -M alt -k l >/dev/null 2>&1 & disown
     pkill -f ~/bin/swaykeys2.sh
   # Fullscreen
   elif [[ "$choice" == 'win f' ]]; then
     killall xfce4-terminal
     setsid ~/bin/swayrun_fullscreen.sh >/dev/null 2>&1 & disown
     pkill -f ~/bin/swaykeys2.sh
   elif [[ "$(echo \"$choice)\" | awk '{print $2}')" == *"shift"* ]]; then
     my_choice01=$(echo "$choice" | awk '{print $1}')
     my_choice02=$(echo "$choice" | awk '{print $2}')
     my_choice03=$(echo "$choice" | awk '{print $3}')
     setsid wtype -M $my_choice01 -M $my_choice02 -k $my_choice03 >/dev/null 2>&1 & disown
     pkill -f ~/bin/swaykeys2.sh
   elif [[ "$(echo \"$choice)\" | awk '{print $2}')" == *"ctrl"* ]]; then
     my_choice01=$(echo "$choice" | awk '{print $1}')
     my_choice02=$(echo "$choice" | awk '{print $2}')
     my_choice03=$(echo "$choice" | awk '{print $3}')
     setsid wtype -M $my_choice01 -M $my_choice02 -k $my_choice03 >/dev/null 2>&1 & disown
     pkill -f ~/bin/swaykeys2.sh
   elif [[ "$(echo \"$choice\" | awk '{print $2}')" == *"alt"* && "$choice" != *"altgr"* ]]; then
     my_choice01=$(echo "$choice" | awk '{print $1}')
     my_choice02=$(echo "$choice" | awk '{print $2}')
     my_choice03=$(echo "$choice" | awk '{print $3}')
     setsid wtype -M $my_choice01 -M $my_choice02 -k $my_choice03 >/dev/null 2>&1 & disown
     pkill -f ~/bin/swaykeys2.sh
   else
     my_choice01=$(echo "$choice" | awk '{print $1}')
     my_choice02=$(echo "$choice" | awk '{print $2}')
     setsid wtype -M $my_choice01 -k $my_choice02 >/dev/null 2>&1 & disown
     pkill -f ~/bin/swaykeys2.sh
   fi

}
export -f main
main

#-------------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------------#

unset main
unset _fzf
unset kill_script
unset launch
unset color1
unset color2
unset end
