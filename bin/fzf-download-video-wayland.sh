#!/bin/bash

# Program command: fzf-download-video-wayland.sh
# Description: Download videos from youtube.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 04-12-2023
# Program_license: GPL 3.0
# Dependencies: fzf, yt-dlp

#----------------------------------------#
# Software preferences
#----------------------------------------#

video_player="smplayer"

#----------------------------------------#
# Create files and directories
#----------------------------------------#

   if [ -d "$HOME"/Videos ]; then
     :
   else
     mkdir "$HOME"/Videos
   fi

   if [ -d "$HOME"/Videos/.ytdownvids ]; then
     :
   else
     mkdir "$HOME"/Videos/.ytdownvids
   fi

   if [ -d "$HOME"/Videos/youtube ]; then
     :
   else
     mkdir "$HOME"/Videos/youtube
   fi

#----------------------------------------#
# Colors
#----------------------------------------#

# Descriptions
color1=$(tput setaf 244)
# Color end
end=$(tput sgr0)

#----------------------------------------#
# Open downloaded video
#----------------------------------------#

do_open() {

nohup "$video_player" "$HOME"/Videos/.ytdownvids/*.webm	&
fzf-launcher-wayland.sh
	
}

#----------------------------------------#
# Paste in link to download
#----------------------------------------#

download_yt_video() {

choice=$(echo "" | fzf --print-query \
--cycle \
--border="none" \
--header="
Press F1 for main menu

" \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'download video> ' \
--bind='F1:execute(fzf-launcher-wayland.sh {})' \
--info=inline \
--reverse)
rm -f "$HOME"/Videos/.ytdownvids/*.webm
nohup yt-dlp "$choice" -o "$HOME/Videos/.ytdownvids/%(title)s.%(ext)s" && notify-send "Download completed" &
cp "$HOME"/Videos/.ytdownvids/*.webm "$HOME"/Videos/youtube/	
fzf-launcher-wayland.sh
	
}

#----------------------------------------#
# Menu
#----------------------------------------#

menu() {

echo "download ${color1} download youtube video${end}"
echo "open ${color1} open youtube video${end}"
	
}

#----------------------------------------#
# Main
#----------------------------------------#

main() {

choice=$(menu | fzf --ansi \
--cycle \
--padding=0% \
--border="none" \
--header="
Press F1 for main menu

" \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'downloads> ' \
--bind='F1:execute(fzf-launcher-wayland.sh {})' \
--info=inline \
--reverse | awk '{print $1}') 

case "$choice" in

 'download')
   download_yt_video
  ;;

 'open')
   do_open
  ;;

 *)
   echo "Something went wrong!" || exit 1
  ;;

esac
	
}
main
