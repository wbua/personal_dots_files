#!/bin/bash

# Program command: sway-bring-window.sh
# Description: Bring window to current workspace.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-01-2025
# Program_license: GPL 3.0
# Dependencies: yad, rofi, wofi, dmenu, swaymsg, jq

# Sway user preferences 
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#-----------------------------------------------------------#
# User preferences
#-----------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Background color
MY_COLOR_BG="$COLOR_BG"
export MY_COLOR_BG

# Icon color
MY_COLOR_ICON="$COLOR_ICON"
export MY_COLOR_BG

#-----------------------------------------------------------#
# Run launchers
#-----------------------------------------------------------#

choice_launcher() {

   if [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "rofi" ]]; then
     rofi -dmenu -i -config "$ROFI_THEME_CONFIG"
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "wofi" ]]; then
     wofi --dmenu -i
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "bemenu" ]]; then
     bemenu -i -l 8 -H 40 --fn "Monospace 18" --fb "$MY_COLOR_BG" --ff "#cdd6f4" --nb "$MY_COLOR_BG" --nf "#cdd6f4" --tb "$MY_COLOR_BG" \
     --hb "$MY_COLOR_BG" --tf "$MY_COLOR_ICON" --hf "$MY_COLOR_ICON" --af "#FFFFFF" --ab "$MY_COLOR_BG"
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "yad" ]]; then
     GTK_THEME="$MY_GTK_THEME" yad --list --column="list" --search-column=1 --regex-search --no-headers \
     --borders=10 --width=800 --height=600 --button="Exit":1 
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "fuzzel" ]]; then
     fuzzel -d
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "tofi" ]]; then
     tofi
   else
     :
   fi
	
}
export -f choice_launcher

#-----------------------------------------------------------#
# Main
#-----------------------------------------------------------#

# Get windows
all_windows=$(swaymsg -t get_tree | \
jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id): \(.app_id // .window_properties.class)"')
export all_windows

# Select window with rofi
selected=$(echo "$all_windows" | choice_launcher | awk -F ':' '{print $1}')
[[ -z "$selected" ]] && exit 0
export selected

# Tell sway to focus said window
swaymsg [con_id="$selected"] move workspace current

#-----------------------------------------------------------#
# Unset variables and functions
#-----------------------------------------------------------#

unset MY_GTK_THEME
unset choice_launcher
unset all_windows
unset selected
unset MY_COLOR_BG
unset MY_COLOR_ICON
