#!/bin/bash

# Program command: sway_delay_screenshot.sh
# Description: Takes a fullscreen delayed screenshot.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-01-2025
# Program_license: GPL 3.0
# Dependencies: grimshot, wl-clipboard, dunst

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#--------------------------------------------------------#
# Error checking
#--------------------------------------------------------#

set -e 

#--------------------------------------------------------#
# Preferences
#--------------------------------------------------------#

# Wallpaper directory
SCREENSHOT_DIR="$MAIN_SCREENSHOT_DIR"

# Screenshot selay
MY_DELAY="$SCREENSHOT_DELAY"

# Dunst backgound color
MY_COLOR_BG="$COLOR_BG"

# Dunst text color
MY_COLOR_ICON="$COLOR_ICON"

# Dunst flame color
MY_COLOR_ALERT="$COLOR_ALERT"

#--------------------------------------------------------#
# Create files and directories
#--------------------------------------------------------#

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ ! -d "$SCREENSHOT_DIR" ]; then
     mkdir -p "$SCREENSHOT_DIR"
   fi
   
   if [ -d "$HOME"/Pictures/.tempfullscrns ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempfullscrns
   fi

   if [ -d "$HOME"/Pictures/.temp_srnshots_thumbs ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temp_srnshots_thumbs
   fi

#--------------------------------------------------------#
# Screenshot file type (jpg.png)
#--------------------------------------------------------#

screenshot_filetype() {

file_locate=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 basename | tr -d '\n')
dir_locate=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 dirname)
strip_ext=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | \
xargs -0 basename | sed 's|.png||g' | sed 's|.jpg||g' | sed 's|.jpeg||g' | tr -d '\n')

   if [[ "$(grep SCREENSHOT_FILE_TYPE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "png" ]]; then
     convert "$dir_locate"/"$file_locate" "$dir_locate"/"$strip_ext".png
     rm -f ~/Pictures/.tempfullscrns/*.jpg
     rm -f ~/Pictures/.temp_srnshots_thumbs/current_screenshot.jpg
   elif [[ "$(grep SCREENSHOT_FILE_TYPE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "jpg" ]]; then
	 convert "$dir_locate"/"$file_locate" "$dir_locate"/"$strip_ext".jpg
	 rm -f ~/Pictures/.tempfullscrns/*.png
	 rm -f ~/Pictures/.temp_srnshots_thumbs/current_screenshot.png
   else
    :
   fi
   
}

#--------------------------------------------------------#
# Main
#--------------------------------------------------------#

   if [[ "$(grep SCREENSHOT_FILENAME= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     rm -f ~/Pictures/.tempfullscrns/*.*
     sleep "$MY_DELAY" && grimshot save screen ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png
     screenshot_filetype
     wl-copy --type image/png < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)" || wl-copy --type image/jpeg < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"
     setsid bash ~/bin/screenshot_filename.sh >/dev/null 2>&1 & disown && exit 0
   elif [[ "$(grep SCREENSHOT_FILENAME= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     rm -f ~/Pictures/.tempfullscrns/*.*
     sleep "$MY_DELAY" && grimshot save screen ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png
     screenshot_filetype
     wl-copy --type image/png < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)" || wl-copy --type image/jpeg < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"
     cp ~/Pictures/.tempfullscrns/*.* "$SCREENSHOT_DIR"     
   fi

#--------------------------------------------------------#
# Notifications options
#--------------------------------------------------------#

reply_action () {

open_file_path=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 basename)
xdg-open "$SCREENSHOT_DIR"/"$open_file_path"
	
}

forward_action () {

:
	
}

handle_dismiss () {

:
	
}

#--------------------------------------------------------#
# Notification alert and clickable action
#--------------------------------------------------------#

notify_shot() {

file_path=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
ACTION=$(dunstify -i "$file_path" -h string:bgcolor:$MY_COLOR_BG -h string:fgcolor:$MY_COLOR_ICON -h string:frcolor:$MY_COLOR_ALERT --action="default,Reply" --action="forwardAction,Forward" "Delay" "Screenshot")

case "$ACTION" in
"default")
    reply_action
    ;;
"forwardAction")
    forward_action
    ;;
"2")
    handle_dismiss
    ;;
esac

}

#--------------------------------------------------------#
# Enable or disable screenshot notification alert
#--------------------------------------------------------#

   if [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     setsid paplay ~/Music/sounds/notification.mp3 >/dev/null 2>&1 & disown
     notify_shot
   elif [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     notify_shot
   else
     :
   fi
