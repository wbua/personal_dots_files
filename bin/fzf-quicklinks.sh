#!/bin/bash

# Program command: fzf-quicklinks.sh
# Description: Quick links to files, paths, web sites, google searches and web query searches.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 10-12-2023
# Program_license: GPL 3.0
# Dependencies: fzf

#--------------------------------------------------#
# Software preferences
#--------------------------------------------------#

# Terminal
my_terminal="gnome-terminal"
export my_terminal

# Browser
browser="brave"
export browser

# Text editor
text_editor="geany"
export text_editor

# File manager
file_manager="thunar"
export file_manager

#--------------------------------------------------#
# Create files and directories
#--------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ -f "$HOME"/Documents/.temp_quicklinks_file.txt ]; then
     :
   else
     touch "$HOME"/Documents/.temp_quicklinks_file.txt
   fi

   if [ -f "$HOME"/Documents/mainprefix/quick_links.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/quick_links.txt
   fi
   
#--------------------------------------------------#
# Colors
#--------------------------------------------------#

# Highlight keybinding
color1=$(tput setaf 15)
export color1
# Color end
end=$(tput sgr0)
export end

#--------------------------------------------------#
# Open path in terminal
#--------------------------------------------------#

open_terminal() {

links_text=$(cat "$HOME"/Documents/.temp_quicklinks_file.txt) 
[ -z "$links_text" ] && exit 0
echo "$links_text" | tee "$HOME"/Documents/.cd_path.txt
path_file=$(cat "$HOME"/Documents/.cd_path.txt)
setsid "$my_terminal" -- /bin/bash -c "cd \"$path_file\"; $SHELL" >/dev/null 2>&1 & disown
sleep 0.4
pkill -f ~/bin/fzf-quicklinks.sh

}
export -f open_terminal

#--------------------------------------------------#
# Copy link to clipboard
#--------------------------------------------------#

copy_to_clip() {

links_text=$(cat "$HOME"/Documents/.temp_quicklinks_file.txt) 
[ -z "$links_text" ] && exit 0
setsid echo "$links_text" | xclip -selection clipboard >/dev/null 2>&1 & disown
sleep 0.4
	
}
export -f copy_to_clip

#--------------------------------------------------#
# Open text file
#--------------------------------------------------#

open_text_file() {

links_text=$(cat "$HOME"/Documents/.temp_quicklinks_file.txt) 
[ -z "$links_text" ] && exit 0
echo "$links_text" > /dev/null
setsid "$text_editor" "$HOME"/Documents/mainprefix/quick_links.txt >/dev/null 2>&1 & disown
sleep 0.4
pkill -f ~/bin/fzf-quicklinks.sh

}
export -f open_text_file

#--------------------------------------------------#
# Web site query search
#--------------------------------------------------#

query_search() {

links_text=$(cat "$HOME"/Documents/.temp_quicklinks_file.txt) 
[ -z "$links_text" ] && exit 0
echo "$links_text" | tee "$HOME"/Documents/.temp_query.txt
sed -i "s|$|\"$(echo "" | fzf --print-query)\"|" "$HOME"/Documents/.temp_query.txt
cat "$HOME"/Documents/.temp_query.txt | tr -d '"' | tee "$HOME"/Documents/.temp_query.txt
sed -i 's| |+|g' "$HOME"/Documents/.temp_query.txt
choice=$(cat "$HOME"/Documents/.temp_query.txt)
[ -z "$choice" ] && exit 0
setsid "$browser" "$choice" >/dev/null 2>&1 & disown 
sleep 0.4
pkill -f ~/bin/fzf-quicklinks.sh

}
export -f query_search

#--------------------------------------------------#
# Open file or directory path
#--------------------------------------------------#

open_file_dir() {

links_text=$(cat "$HOME"/Documents/.temp_quicklinks_file.txt) 
[ -z "$links_text" ] && exit 0

   if [[ -f "$links_text" ]]; then
     setsid xdg-open "$links_text" >/dev/null 2>&1 & disown
     sleep 0.4
     pkill -f ~/bin/fzf-quicklinks.sh
   elif [[ -d "$links_text" ]]; then
     setsid "$file_manager" "$links_text" >/dev/null 2>&1 & disown
     sleep 0.4
     pkill -f ~/bin/fzf-quicklinks.sh
   else
     :
   fi

}
export -f open_file_dir

#--------------------------------------------------#
# Open web link in browser
#--------------------------------------------------#

open_link_browser() {

links_text=$(cat "$HOME"/Documents/.temp_quicklinks_file.txt) 
[ -z "$links_text" ] && exit 0
setsid echo "$links_text" | awk '{print $NF}' | xargs -r "$browser" >/dev/null 2>&1 & disown
sleep 0.4
pkill -f ~/bin/fzf-quicklinks.sh

}
export -f open_link_browser

#--------------------------------------------------#
# Kill script
#--------------------------------------------------#

kill_script() {

pkill -f ~/bin/fzf-quicklinks.sh
	
}
export -f kill_script

#--------------------------------------------------#
# Main
#--------------------------------------------------#

main() {

links_text=$(cat "$HOME"/Documents/mainprefix/quick_links.txt) 
choice=$(echo "$links_text" | fzf --print-query \
--ansi \
--border="none" \
--header="
Press Esc to exit fzf

Press ${color1}F1${end} terminal | Press ${color1}F2${end} clip | Press ${color1}F3${end} file | Press ${color1}F4${end} query
Press ${color1}F5${end} file or path | Press ${color1}F6${end} browser | Press ${color1}F9${end} Kill script

" \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:#660020,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'quicklinks> ' \
--cycle \
--bind='F9:execute(kill_script {})' \
--bind='F1:execute-silent(echo -n {} | tee "$HOME"/Documents/.temp_quicklinks_file.txt)+execute(open_terminal {})' \
--bind='F2:execute-silent(echo -n {} | tee "$HOME"/Documents/.temp_quicklinks_file.txt)+execute(copy_to_clip {})' \
--bind='F3:execute-silent(echo -n {} | tee "$HOME"/Documents/.temp_quicklinks_file.txt)+execute(open_text_file {})' \
--bind='F4:execute-silent(echo -n {} | tee "$HOME"/Documents/.temp_quicklinks_file.txt)+execute(query_search {})' \
--bind='F5:execute-silent(echo -n {} | tee "$HOME"/Documents/.temp_quicklinks_file.txt)+execute(open_file_dir {})' \
--bind='F6:execute-silent(echo -n {} | tee "$HOME"/Documents/.temp_quicklinks_file.txt)+execute(open_link_browser {})' \
--info=inline \
--reverse)
[ -z "$choice" ] && exit 0

setsid echo "$choice" >/dev/null 2>&1 & disown
sleep 0.2
	
}
export -f main
main

#--------------------------------------------------#
# Unset variables and function
#--------------------------------------------------#

# Function
unset main
unset open_link_browser
unset open_file_dir
unset query_search
unset open_text_file
unset copy_to_clip
unset open_terminal
unset kill_script

# Variables
unset file_manager
unset my_terminal
unset text_editor
unset browser
unset color1
unset end
