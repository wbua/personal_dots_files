#!/bin/bash

# Program command: fzf-bash-wayland.sh
# Description: Open and create bash scripts.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-12-2023
# Program_license: GPL 3.0
# Dependencies: fzf

#------------------------------------------#
# Software preferences
#------------------------------------------#

# Text editor
text_editor="geany"
export text_editor

#------------------------------------------#
# User paths
#------------------------------------------#

# Bash scripts executable path location
bash_dir="$HOME/bin/"
export bash_dir

#------------------------------------------#
# Create bash script
#------------------------------------------#

new_script() {
 
choice=$(echo "" | fzf --print-query \
--cycle \
--info=inline \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--reverse \
--header='
Press F1 for main menu

' \
--prompt 'create script> ' \
--bind='F1:execute(fzf-launcher-wayland.sh {})')	
bashfile="$bash_dir/$choice".sh
echo "#!/bin/bash" >> "$bashfile"
chmod u+x "$bashfile"
setsid "$text_editor" "$bashfile" >/dev/null 2>&1 & disown
pkill -f ~/bin/fzf-bash-wayland.sh
     
}
export -f new_script

#------------------------------------------#
# fzf main
#------------------------------------------#

main_fzf() {

fzf --reverse \
--cycle \
--info=inline \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:#660020,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--header='
Open script: enter | New script: F1 | kill script: F9
Preview page up: shift+up | Preview page down: shift+down

' \
--prompt 'open script> ' \
--preview-window=top,75% \
--preview 'batcat --color=always {}' \
--bind shift-up:preview-page-up,shift-down:preview-page-down \
--bind='F9:execute(kill_script {})' \
--bind='F1:execute(new_script {})'
	
}
export -f main_fzf

#------------------------------------------#
# Kill script
#------------------------------------------#

kill_script() {

pkill -f ~/bin/fzf-bash-wayland.sh
	
}
export -f kill_script

#------------------------------------------#
# Main
#------------------------------------------#

main() {

choice=$(find "$bash_dir" -type f -iname '*')
launcher=$(echo "$choice" | main_fzf)
[ -z "$launcher" ] && exit 0

setsid "$text_editor" "$launcher" >/dev/null 2>&1 & disown
pkill -f ~/bin/fzf-bash-wayland.sh

}
export -f main
main

#------------------------------------------#
# Unset variables and functions
#------------------------------------------#

unset main
unset new_script
unset bash_dir
unset text_editor
unset main_fzf
unset kill_script

