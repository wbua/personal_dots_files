#!/bin/bash

# Program command: fzf-color_picker-wayland.sh
# Description: Color picker for fzf.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 09-05-2024
# Program_license: GPL 3.0
# Dependencies: fzf, catimg, imagemagick, wl-clipboard

#----------------------------------------------#
# Create files and directories
#----------------------------------------------#

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/fzf_colors ]; then
     :
   else
     mkdir "$HOME"/Pictures/fzf_colors
   fi

#----------------------------------------------#
# View hex colors and copy to clipboard
#----------------------------------------------#

kill_script() {

pkill -f ~/bin/fzf-color_picker-wayland.sh
	
}
export -f kill_script 

#----------------------------------------------#
# View hex colors and copy to clipboard
#----------------------------------------------#

hex_colors() {

choice03=$(find ~/Pictures/fzf_colors/ -maxdepth 5 -name "*.png" -printf '%T@ %p\n' | \
sort -rn | cut -f2- -d" " | fzf --preview="catimg {}" \
--info=inline \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--margin="2" \
--reverse \
--header='
Press F1 for main menu

Menu: F8 | Kill script: F9

' \
--bind='F8:execute(main {})' \
--bind='F9:execute(kill_script {})' \
--prompt 'select hex color> ')
[ -z "$choice03" ] && exit

echo "$choice03" | cut -d '/' -f6- | sed 's|.png||' | wl-copy -n
	
}
export -f hex_colors

#----------------------------------------------#
# Adding hex color
#----------------------------------------------#

add_color() {

choice01=$(echo "" | fzf --print-query \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--margin="2" \
--info=inline \
--reverse \
--header='
Press F1 for main menu

Menu: F8 | Kill script: F9
' \
--bind='F8:execute(main {})' \
--bind='F9:execute(kill_script {})' \
--prompt 'add hex color> ')
[[ -z "$choice01" ]] && exit
convert -size 200x200 xc:"$choice01" "$HOME/Pictures/fzf_colors/$choice01".png && notify-send "Hex color added!"
	
}
export -f add_color

#----------------------------------------------#
# Menu
#----------------------------------------------#

menu() {

echo "hex-codes"
echo "add-code"
	
}
export -f menu

#----------------------------------------------#
# Main
#----------------------------------------------#

main() {

choice=$(menu | fzf --reverse \
--info=inline \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--margin="2" \
--bind='F9:execute(kill_script {})' \
--header='
Press F1 for main menu

Kill script: F9

' \
--prompt 'hex colors> ')	

case "$choice" in

 'hex-codes')
   hex_colors
   ;;

 'add-code')
   add_color
   ;;

 *)
   echo "Something went wrong!" || exit 1
 ;;

esac
	
}
export -f main
main


unset main
unset menu
unset kill_script
unset add_color
unset hex_colors
