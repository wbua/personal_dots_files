#!/bin/bash

# Program command: yad_convert.sh
# Description: Converts images using imagemagick in gui.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 07-03-2025
# Program_license: GPL 3.0
# Dependencies: yad, imagemagick, webp

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#---------------------------------------------------#
# Error checking
#---------------------------------------------------#

set -e

#---------------------------------------------------#
# User preferences
#---------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#---------------------------------------------------#
# Checks if you are running a wayland session
#---------------------------------------------------#

   if [[ "$WAYLAND_DISPLAY" == "" ]]; then
     yad --error \
     --width 500 \
     --title "Wayland session not found!" \
     --text "Run launcher error, wayland only tool!"
     exit 1
   else
     :
   fi

#---------------------------------------------------#
# Colors
#---------------------------------------------------#

# Title
COLOR1=$(printf "<span color='#FFFFFF' font_family='Inter' font='18' rise='0pt'>")
export COLOR1
# Labels
COLOR2=$(printf "<span color='#FFFFFF' font_family='Inter' font='12' rise='0pt'>")
export COLOR2
# Number title
COLOR3=$(printf "<span color='#EEFF00' font_family='Inter' font='12' rise='0pt'>")
export COLOR3
# Separators
COLOR4=$(printf "<span color='#838181' font_family='Inter' font='15' rise='0pt'>")
export COLOR4
# Color end
END=$(printf "</span>")
export END

#---------------------------------------------------#
# Resizes image
#---------------------------------------------------#

resize_image() {

convert "$field3" -resize "$field2" "$field5"/"$(date +'%d-%m-%Y-%H%M%S')".png
	
}
export -f resize_image

#---------------------------------------------------#
# Greyscale image
#---------------------------------------------------#

greyscale() {

convert "$field3" -set colorspace Gray -separate -average "$field5"/"$(date +'%d-%m-%Y-%H%M%S')".png
	
}
export -f greyscale

#---------------------------------------------------#
# Border image
#---------------------------------------------------#

border_image() {

convert "$field3" -bordercolor black -border "$field4" "$field5"/"$(date +'%d-%m-%Y-%H%M%S')".png
	
}
export -f border_image

#---------------------------------------------------#
# Negate image
#---------------------------------------------------#

negate_image() {

convert "$field3" -channel RGB -negate "$field5"/"$(date +'%d-%m-%Y-%H%M%S')".png
	
}
export -f negate_image

#---------------------------------------------------#
# Blur image
#---------------------------------------------------#

blur_image() {

convert "$field3" -blur "$field4" "$field5"/"$(date +'%d-%m-%Y-%H%M%S')".png
	
}
export -f blur_image

#---------------------------------------------------#
# Rotate image plus
#---------------------------------------------------#

rotate_image_plus() {

convert "$field3" -rotate "$field4" "$field5"/"$(date +'%d-%m-%Y-%H%M%S')".png

}
export -f rotate_image_plus

#---------------------------------------------------#
# Rotate image minus
#---------------------------------------------------#

rotate_image_minus() {

convert "$field3" -rotate "$field4" "$field5"/"$(date +'%d-%m-%Y-%H%M%S')".png

}
export -f rotate_image_minus

#---------------------------------------------------#
# Sepia image
#---------------------------------------------------#

sepia_image() {

convert "$field3" -sepia-tone "$field4" "$field5"/"$(date +'%d-%m-%Y-%H%M%S')".png
	
}
export -f sepia_image

#---------------------------------------------------#
# Emboss image
#---------------------------------------------------#

emboss_image() {

convert "$field3" -emboss "$field4" "$field5"/"$(date +'%d-%m-%Y-%H%M%S')".png
	
}
export -f emboss_image

#---------------------------------------------------#
# Pdf
#---------------------------------------------------#

pdf_image() {

libreoffice --headless --convert-to pdf "$field3" --outdir "$field5"

}
export -f pdf_image

#---------------------------------------------------#
# Jpg to png
#---------------------------------------------------#

jpg_to_png() {

   if [[ "$(file --mime-type "$field3")" == *"jpeg"* ]]; then
     remove_ext=$(echo "$field3" | sed 's|.jpg||' | xargs -0 basename)
     convert "$field3" "$field5"/"$remove_ext".png
   else
     notify-send "No jpg image detected!" && exit 1
   fi
     
}
export -f jpg_to_png

#---------------------------------------------------#
# Webp to png
#---------------------------------------------------#

webp_to_png() {

   if [[ "$(file --mime-type "$field3")" == *"webp"* ]]; then
     remove_ext=$(echo "$field3" | sed 's|.webp||' | xargs -0 basename)
     dwebp "$field3" -o "$field5"/"$remove_ext".png
   else
     notify-send "No webp image detected!" && exit 1
   fi

}
export -f webp_to_png

#---------------------------------------------------#
# Png to jpg
#---------------------------------------------------#

png_to_jpg() {

   if [[ "$(file --mime-type "$field3")" == *"png"* ]]; then
     remove_ext=$(echo "$field3" | sed 's|.png||' | xargs -0 basename)
     convert "$field3" "$field5"/"$remove_ext".jpg
   else
     notify-send "No png image detected!" && exit 1
   fi

}
export -f png_to_jpg

#---------------------------------------------------#
# Main
#---------------------------------------------------#

main() {

# Yad gui
choice=$(GTK_THEME="$MY_GTK_THEME" yad --form --title="Yad convert tool" \
--text="${COLOR1}Converts images${END}\n" \
--text-align="center" \
--center \
--align="left" \
--width=600 \
--height=500 \
--borders=20 \
--separator="#" \
--field="${COLOR2}Convert ${END}:CB" 'None!Resize!Pdf!Jpgtopng!Webtopng!Pngtojpg!Border!Sepia!Greyscale!Emboss!Negate!Blur!Rotateplus!Rotateminus' \
--field="${COLOR2}Resize only ${END}":CE \
--field="${COLOR2}File ${END}":FL \
--field="${COLOR2}Number ${END}":CE \
--field="${COLOR2}Directory ${END}":DIR \
--field="\n${COLOR3}Numbers${END}
Border: ${COLOR3}20${END} ${COLOR4}|${END} Blur: ${COLOR3}0x5${END} ${COLOR4}|${END} Rotate plus: ${COLOR3}90${END} 
Rotate minus: ${COLOR3}-90${END} ${COLOR4}|${END} Sepia: ${COLOR3}80%${END} ${COLOR4}|${END} Emboss: ${COLOR3}10${END}

Can take a few seconds to convert":LBL "" \
--button="Convert":0 \
--button="Exit":1)
[[ -z "$choice" ]] && exit

# Convert option
field1=$(echo "$choice" | awk -F '#' '{print $1}')
# Resize dimensions
field2=$(echo "$choice" | awk -F '#' '{print $2}')
# Selected file
field3=$(echo "$choice" | awk -F '#' '{print $3}')
# Number
field4=$(echo "$choice" | awk -F '#' '{print $4}')
# Output directory
field5=$(echo "$choice" | awk -F '#' '{print $5}')

   # None
   if [[ "$field1" == *"None"* ]]; then
     :
   # Resize image
   elif [[ "$field1" == *"Resize"* ]]; then
     resize_image
   # Greyscale
   elif [[ "$field1" == *"Greyscale"* ]]; then
     greyscale
   # Border
   elif [[ "$field1" == *"Border"* ]]; then
     border_image
   # Negate
   elif [[ "$field1" == *"Negate"* ]]; then
     negate_image
   # Blur
   elif [[ "$field1" == *"Blur"* ]]; then
     blur_image
   # Rotate +90
   elif [[ "$field1" == *"Rotateplus"* ]]; then
     rotate_image_plus
   # Rotate -90
   elif [[ "$field1" == *"Rotateminus"* ]]; then
     rotate_image_minus
   # Sepia
   elif [[ "$field1" == *"Sepia"* ]]; then
     sepia_image
   # Emboss
   elif [[ "$field1" == *"Emboss"* ]]; then
     emboss_image
   # Pdf
   elif [[ "$field1" == *"Pdf"* ]]; then
     pdf_image
   # Jpg to png
   elif [[ "$field1" == *"Jpgtopng"* ]]; then
     jpg_to_png
   # Webp to png
   elif [[ "$field1" == *"Webtopng"* ]]; then
     webp_to_png
   elif [[ "$field1" == *"Pngtojpg"* ]]; then
     png_to_jpg
   else
     notify-send "Something went wrong!" || exit 1
   fi

}
export -f main
main

#---------------------------------------------------#
# Unsets variables and functions
#---------------------------------------------------#

unset _yad
unset choice
unset field1
unset field2
unset main
unset resize_image
unset greyscale
unset border_image
unset negate_image
unset blur_image
unset rotate_image_plus
unset rotate_image_minus
unset sepia_image
unset emboss_image
unset COLOR1
unset COLOR2
unset COLOR3
unset COLOR4
unset END
unset pdf_image
unset jpg_to_png
unset webp_to_png
unset png_to_jpg
