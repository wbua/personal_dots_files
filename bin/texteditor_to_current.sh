#!/bin/bash

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh
   
     swaymsg -t get_tree | \
     jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"'| \
     grep -i "$MAIN_TEXT_EDITOR" | \
     cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' move workspace current


