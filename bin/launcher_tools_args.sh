#!/bin/bash

# Sway user preferences 
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

   if [[ "$SELECT_RUN_LAUNCHER" == 'rofi' ]]; then
     echo "-config $ROFI_THEME_CONFIG"
   elif [[ "$SELECT_RUN_LAUNCHER" == "fuzzel" ]]; then
     echo "-d"
   elif [[ "$SELECT_RUN_LAUNCHER" == 'wofi' ]]; then
     echo "--dmenu"
   elif [[ "$SELECT_RUN_LAUNCHER" == "bemenu" ]]; then
      echo "-l 8"
   elif [[ "$SELECT_RUN_LAUNCHER" == 'tofi' ]]; then
     echo "--fuzzy-match=true"
   else
     :
   fi

unset BEMENU_OPTS
