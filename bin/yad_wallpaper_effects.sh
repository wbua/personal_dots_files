#!/bin/bash

# Program command: yad_wallpaper_effects.sh
# Description: Convert wallpaper using various imagemagick effects.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 27-08-2024
# Program_license: GPL 3.0
# Dependencies: yad, imagemagick

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#---------------------------------------------------#
# Error checking
#---------------------------------------------------#

set -e

#---------------------------------------------------#
# Creates files and directories 
#---------------------------------------------------#

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/.temp_wallpaper_effects ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temp_wallpaper_effects
   fi

   if [ -d "$HOME"/Pictures/.temp_wallpaper_effects2 ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temp_wallpaper_effects2
   fi

   if [ -d "$HOME"/Pictures/.temp_wallpaper_effects3 ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temp_wallpaper_effects3
   fi

#---------------------------------------------------#
# User preferences
#---------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME
   
#---------------------------------------------------#
# Misc
#---------------------------------------------------#

rm -f "$HOME"/Pictures/.temp_wallpaper_effects/*.*
rm -f "$HOME"/Pictures/.temp_wallpaper_effects2/*.*
rm -f "$HOME"/Pictures/.temp_wallpaper_effects3/*.*
selected_choice=$(cat "$HOME"/Documents/.walls_selected.txt)
export selected_choice
ima_dir="$HOME/Pictures/.temp_wallpaper_effects"
export ima_dir
cp ~/Pictures/sway_wallpapers/"$selected_choice" "$HOME"/Pictures/.temp_wallpaper_effects3/
convert ~/Pictures/.temp_wallpaper_effects3/*.* -resize 600x "$ima_dir"/"${selected_choice}"

#---------------------------------------------------#
# Creates dot desktop file
#---------------------------------------------------#

main_choice() {

# Name	
choice01=$(find ~/Pictures/.temp_wallpaper_effects/ -type f -iname '*.png' -print0 | sed 's|.png||g' | xargs -0 basename)

# Icon
choice02=$(find ~/Pictures/.temp_wallpaper_effects/ -type f -iname '*.png')

# Desktop file
choice_main=$(cat << EOF 
[Desktop Entry]
Version=1.0
Type=Application
Name=$choice01
Comment=
Icon=$choice02
Exec=
StartupNotify=false
Terminal=false

EOF
)

# Creates dot desktop file
echo "$choice_main" | tee "$HOME"/Pictures/.temp_wallpaper_effects/"$selected_choice".desktop
file_rename=$(find ~/Pictures/.temp_wallpaper_effects/ -type f -iname '*.desktop' -print0 | sed 's|.png||g' | xargs -0 basename)
mv ~/Pictures/.temp_wallpaper_effects/*.desktop ~/Pictures/.temp_wallpaper_effects/"$file_rename"

}
export -f main_choice
main_choice

#---------------------------------------------------#
# Reload image
#---------------------------------------------------#

reload_image() {

rm -f "$HOME"/Pictures/.temp_wallpaper_effects/*.*
rm -f "$HOME"/Pictures/.temp_wallpaper_effects2/*.*
rm -f "$HOME"/Pictures/.temp_wallpaper_effects3/*.*
selected_choice=$(cat "$HOME"/Documents/.walls_selected.txt)
export selected_choice
ima_dir="$HOME/Pictures/.temp_wallpaper_effects"
export ima_dir
cp ~/Pictures/sway_wallpapers/"$selected_choice" "$HOME"/Pictures/.temp_wallpaper_effects3/
convert ~/Pictures/.temp_wallpaper_effects3/*.* -resize 600x "$ima_dir"/"${selected_choice}"
main_choice

}
export -f reload_image

#---------------------------------------------------#
# Convert wallpaper to greyscale
#---------------------------------------------------#

greyscale_image() {

rm -f "$HOME"/Pictures/.temp_wallpaper_effects2/*.*
image_name=$(find ~/Pictures/.temp_wallpaper_effects/ -type f -iname '*.png' -print0 | sed 's|.png||' | xargs -0 basename)
image_dir="$HOME/Pictures/.temp_wallpaper_effects2"
image_dir2="$HOME/Pictures/.temp_wallpaper_effects3"

convert ~/Pictures/.temp_wallpaper_effects3/*.png -set colorspace Gray -separate -average "$image_dir2"/"${image_name}".png
convert ~/Pictures/.temp_wallpaper_effects/*.png -set colorspace Gray -separate -average "$image_dir"/"${image_name}".png
cp "$HOME"/Pictures/.temp_wallpaper_effects/*.desktop "$HOME"/Pictures/.temp_wallpaper_effects2/
rm -f "$HOME"/Pictures/.temp_wallpaper_effects/*.*
cp "$HOME"/Pictures/.temp_wallpaper_effects2/*.* "$HOME"/Pictures/.temp_wallpaper_effects/
	
}
export -f greyscale_image

#---------------------------------------------------#
# Blur wallpaper
#---------------------------------------------------#

blur_image() {

rm -f "$HOME"/Pictures/.temp_wallpaper_effects2/*.*
image_name=$(find ~/Pictures/.temp_wallpaper_effects/ -type f -iname '*.png' -print0 | sed 's|.png||' | xargs -0 basename)
image_dir="$HOME/Pictures/.temp_wallpaper_effects2"
image_dir2="$HOME/Pictures/.temp_wallpaper_effects3"

convert ~/Pictures/.temp_wallpaper_effects3/*.png -blur 0x5 "$image_dir2"/"${image_name}".png
convert ~/Pictures/.temp_wallpaper_effects/*.png -blur 0x5 "$image_dir"/"${image_name}".png
cp "$HOME"/Pictures/.temp_wallpaper_effects/*.desktop "$HOME"/Pictures/.temp_wallpaper_effects2/
rm -f "$HOME"/Pictures/.temp_wallpaper_effects/*.*
cp "$HOME"/Pictures/.temp_wallpaper_effects2/*.* "$HOME"/Pictures/.temp_wallpaper_effects/

}
export -f blur_image

#---------------------------------------------------#
# Negate wallpaper
#---------------------------------------------------#

negate_image() {

rm -f "$HOME"/Pictures/.temp_wallpaper_effects2/*.*
image_name=$(find ~/Pictures/.temp_wallpaper_effects/ -type f -iname '*.png' -print0 | sed 's|.png||' | xargs -0 basename)
image_dir="$HOME/Pictures/.temp_wallpaper_effects2"
image_dir2="$HOME/Pictures/.temp_wallpaper_effects3"

convert ~/Pictures/.temp_wallpaper_effects3/*.png -channel RGB -negate "$image_dir2"/"${image_name}".png
convert ~/Pictures/.temp_wallpaper_effects/*.png -channel RGB -negate "$image_dir"/"${image_name}".png
cp "$HOME"/Pictures/.temp_wallpaper_effects/*.desktop "$HOME"/Pictures/.temp_wallpaper_effects2/
rm -f "$HOME"/Pictures/.temp_wallpaper_effects/*.*
cp "$HOME"/Pictures/.temp_wallpaper_effects2/*.* "$HOME"/Pictures/.temp_wallpaper_effects/
	
}
export -f negate_image

#---------------------------------------------------#
# Sepia wallpaper
#---------------------------------------------------#

sepia_image() {

rm -f "$HOME"/Pictures/.temp_wallpaper_effects2/*.*
image_name=$(find ~/Pictures/.temp_wallpaper_effects/ -type f -iname '*.png' -print0 | sed 's|.png||' | xargs -0 basename)
image_dir="$HOME/Pictures/.temp_wallpaper_effects2"
image_dir2="$HOME/Pictures/.temp_wallpaper_effects3"

convert ~/Pictures/.temp_wallpaper_effects3/*.png -sepia-tone 80% "$image_dir2"/"${image_name}".png
convert ~/Pictures/.temp_wallpaper_effects/*.png -sepia-tone 80% "$image_dir"/"${image_name}".png
cp "$HOME"/Pictures/.temp_wallpaper_effects/*.desktop "$HOME"/Pictures/.temp_wallpaper_effects2/
rm -f "$HOME"/Pictures/.temp_wallpaper_effects/*.*
cp "$HOME"/Pictures/.temp_wallpaper_effects2/*.* "$HOME"/Pictures/.temp_wallpaper_effects/
	
}
export -f sepia_image

#---------------------------------------------------#
# Increase brightness for wallpaper
#---------------------------------------------------#

increase_brightness() {

rm -f "$HOME"/Pictures/.temp_wallpaper_effects2/*.*
image_name=$(find ~/Pictures/.temp_wallpaper_effects/ -type f -iname '*.png' -print0 | sed 's|.png||' | xargs -0 basename)
image_dir="$HOME/Pictures/.temp_wallpaper_effects2"
image_dir2="$HOME/Pictures/.temp_wallpaper_effects3"

convert ~/Pictures/.temp_wallpaper_effects3/*.png -brightness-contrast 10X10 "$image_dir2"/"${image_name}".png
convert ~/Pictures/.temp_wallpaper_effects/*.png -brightness-contrast 10X10 "$image_dir"/"${image_name}".png
cp "$HOME"/Pictures/.temp_wallpaper_effects/*.desktop "$HOME"/Pictures/.temp_wallpaper_effects2/
rm -f "$HOME"/Pictures/.temp_wallpaper_effects/*.*
cp "$HOME"/Pictures/.temp_wallpaper_effects2/*.* "$HOME"/Pictures/.temp_wallpaper_effects/
	
}
export -f increase_brightness

#---------------------------------------------------#
# Decrease brightness for wallpaper
#---------------------------------------------------#

decrease_brightness() {

rm -f "$HOME"/Pictures/.temp_wallpaper_effects2/*.*
image_name=$(find ~/Pictures/.temp_wallpaper_effects/ -type f -iname '*.png' -print0 | sed 's|.png||' | xargs -0 basename)
image_dir="$HOME/Pictures/.temp_wallpaper_effects2"
image_dir2="$HOME/Pictures/.temp_wallpaper_effects3"

convert ~/Pictures/.temp_wallpaper_effects3/*.png -brightness-contrast -10X-10 "$image_dir2"/"${image_name}".png
convert ~/Pictures/.temp_wallpaper_effects/*.png -brightness-contrast -10X-10 "$image_dir"/"${image_name}".png
cp "$HOME"/Pictures/.temp_wallpaper_effects/*.desktop "$HOME"/Pictures/.temp_wallpaper_effects2/
rm -f "$HOME"/Pictures/.temp_wallpaper_effects/*.*
cp "$HOME"/Pictures/.temp_wallpaper_effects2/*.* "$HOME"/Pictures/.temp_wallpaper_effects/
	
}
export -f decrease_brightness

#---------------------------------------------------#
# Charcoal wallpaper
#---------------------------------------------------#

charcoal_image() {

rm -f "$HOME"/Pictures/.temp_wallpaper_effects2/*.*
image_name=$(find ~/Pictures/.temp_wallpaper_effects/ -type f -iname '*.png' -print0 | sed 's|.png||' | xargs -0 basename)
image_dir="$HOME/Pictures/.temp_wallpaper_effects2"
image_dir2="$HOME/Pictures/.temp_wallpaper_effects3"

convert ~/Pictures/.temp_wallpaper_effects3/*.png -charcoal 0x5 "$image_dir2"/"${image_name}".png
convert ~/Pictures/.temp_wallpaper_effects/*.png -charcoal 0x5 "$image_dir"/"${image_name}".png
cp "$HOME"/Pictures/.temp_wallpaper_effects/*.desktop "$HOME"/Pictures/.temp_wallpaper_effects2/
rm -f "$HOME"/Pictures/.temp_wallpaper_effects/*.*
cp "$HOME"/Pictures/.temp_wallpaper_effects2/*.* "$HOME"/Pictures/.temp_wallpaper_effects/
	
}
export -f charcoal_image

#---------------------------------------------------#
# Blue colorize wallpaper
#---------------------------------------------------#

blue_colorize() {

rm -f "$HOME"/Pictures/.temp_wallpaper_effects2/*.*
image_name=$(find ~/Pictures/.temp_wallpaper_effects/ -type f -iname '*.png' -print0 | sed 's|.png||' | xargs -0 basename)
image_dir="$HOME/Pictures/.temp_wallpaper_effects2"
image_dir2="$HOME/Pictures/.temp_wallpaper_effects3"

convert ~/Pictures/.temp_wallpaper_effects3/*.png -fill blue -tint 70 "$image_dir2"/"${image_name}".png
convert ~/Pictures/.temp_wallpaper_effects/*.png -fill blue -tint 70 "$image_dir"/"${image_name}".png

cp "$HOME"/Pictures/.temp_wallpaper_effects/*.desktop "$HOME"/Pictures/.temp_wallpaper_effects2/
rm -f "$HOME"/Pictures/.temp_wallpaper_effects/*.*
cp "$HOME"/Pictures/.temp_wallpaper_effects2/*.* "$HOME"/Pictures/.temp_wallpaper_effects/

}
export -f blue_colorize

#---------------------------------------------------#
# Green colorize wallpaper
#---------------------------------------------------#

green_colorize() {

rm -f "$HOME"/Pictures/.temp_wallpaper_effects2/*.*
image_name=$(find ~/Pictures/.temp_wallpaper_effects/ -type f -iname '*.png' -print0 | sed 's|.png||' | xargs -0 basename)
image_dir="$HOME/Pictures/.temp_wallpaper_effects2"
image_dir2="$HOME/Pictures/.temp_wallpaper_effects3"

convert ~/Pictures/.temp_wallpaper_effects3/*.png -fill green -tint 70 "$image_dir2"/"${image_name}".png
convert ~/Pictures/.temp_wallpaper_effects/*.png -fill green -tint 70 "$image_dir"/"${image_name}".png

cp "$HOME"/Pictures/.temp_wallpaper_effects/*.desktop "$HOME"/Pictures/.temp_wallpaper_effects2/
rm -f "$HOME"/Pictures/.temp_wallpaper_effects/*.*
cp "$HOME"/Pictures/.temp_wallpaper_effects2/*.* "$HOME"/Pictures/.temp_wallpaper_effects/

}
export -f green_colorize

#---------------------------------------------------#
# Red colorize wallpaper
#---------------------------------------------------#

red_colorize() {

rm -f "$HOME"/Pictures/.temp_wallpaper_effects2/*.*
image_name=$(find ~/Pictures/.temp_wallpaper_effects/ -type f -iname '*.png' -print0 | sed 's|.png||' | xargs -0 basename)
image_dir="$HOME/Pictures/.temp_wallpaper_effects2"
image_dir2="$HOME/Pictures/.temp_wallpaper_effects3"

convert ~/Pictures/.temp_wallpaper_effects3/*.png -fill red -tint 70 "$image_dir2"/"${image_name}".png
convert ~/Pictures/.temp_wallpaper_effects/*.png -fill red -tint 70 "$image_dir"/"${image_name}".png

cp "$HOME"/Pictures/.temp_wallpaper_effects/*.desktop "$HOME"/Pictures/.temp_wallpaper_effects2/
rm -f "$HOME"/Pictures/.temp_wallpaper_effects/*.*
cp "$HOME"/Pictures/.temp_wallpaper_effects2/*.* "$HOME"/Pictures/.temp_wallpaper_effects/

}
export -f red_colorize

#---------------------------------------------------#
# Export wallpaper
#---------------------------------------------------#

export_wallpaper() {

cp "$HOME"/Pictures/.temp_wallpaper_effects3/*.* "$HOME"/Pictures/
notify-send "Image exported"

}
export -f export_wallpaper

#---------------------------------------------------#
# Set wallpaper
#---------------------------------------------------#

set_wallpaper() {

temp_effect_wall="$HOME/Pictures/.temp_wallpaper_effects3"

killall yad
wall_file=$(find ~/Pictures/.temp_wallpaper_effects3/ -type f -iname '*.png' -print0 | xargs -0 basename)
sed -i "s|.*\(MAIN_SWAY_WALLPAPER=\).*|MAIN_SWAY_WALLPAPER=\"$(printf '%s' "$temp_effect_wall/$wall_file")\"|" ~/bin/sway_user_preferences.sh || exit 1
bash ~/bin/sway_load_settings.sh
   
}
export -f set_wallpaper

#---------------------------------------------------#
# Kill yad
#---------------------------------------------------#

kill_yad() {

killall yad
	
}
export -f kill_yad

#---------------------------------------------------#
# Yad settings
#---------------------------------------------------#

GTK_THEME="$MY_GTK_THEME" yad --icons \
--read-dir="$HOME/Pictures/.temp_wallpaper_effects/" \
--monitor \
--descend \
--item-width=650 \
--image-on-top \
--text="\nWallpaper effects\n" \
--text-align="center" \
--single-click \
--buttons-layout="center" \
--borders=20 \
--width="800" \
--height="600" \
--title="Wallpaper effects" \
--center \
--button='Coal:/bin/bash -c "sleep 0.5 && charcoal_image"' \
--button='Bri+:/bin/bash -c "sleep 0.5 && increase_brightness"' \
--button='Bri-:/bin/bash -c "sleep 0.5 && decrease_brightness"' \
--button='Blue:/bin/bash -c "sleep 0.5 && blue_colorize"' \
--button='Green:/bin/bash -c "sleep 0.5 && green_colorize"' \
--button='Red:/bin/bash -c "sleep 0.5 && red_colorize"' \
--button='Grey:/bin/bash -c "sleep 0.5 && greyscale_image"' \
--button='Sepia:/bin/bash -c "sleep 0.5 && sepia_image"' \
--button='Negate:/bin/bash -c "sleep 0.5 && negate_image"' \
--button='Blur:/bin/bash -c "sleep 0.5 && blur_image"' \
--button='Reload:/bin/bash -c "sleep 0.5 && reload_image"' \
--button='Set:/bin/bash -c "sleep 0.5 && set_wallpaper"' \
--button='Export:/bin/bash -c "sleep 0.5 && export_wallpaper"' \
--button='Exit:/bin/bash -c "sleep 0.5 && kill_yad"'

#---------------------------------------------------#
# Unset variables and functions
#---------------------------------------------------#

unset main_choice
unset greyscale_image
unset selected_choice
unset ima_dir
unset set_wallpaper
unset blur_image
unset negate_image
unset sepia_image
unset increase_brightness
unset decrease_brightness
unset blue_colorize
unset green_colorize
unset red_colorize
unset charcoal_image
unset kill_yad
unset MY_GTK_THEME
unset export_wallpaper
