#!/bin/bash

set -e

#
_rofi12() {
	
rofi -dmenu -i -p ''	
	
}

# Office suite
launch_only-office() {
	
launcher="xdotool key"
fileone="$(cat /home/john/Documents/palettes/onlyoffice.txt)"
choice=$(echo -e "$fileone" | sort | rofi -dmenu -i -p '')
[ -z "$choice" ] && exit 0
echo -e "$choice" | awk -F ':' '{print $NF}' | sed 's/ //' | xargs $launcher	
	
}

# Image editor
launch_gimp() {

launcher="xdotool key"
fileone="$(cat /home/john/Documents/palettes/gimp.txt)"
choice=$(echo -e "$fileone" | sort | rofi -dmenu -i -p '')
[ -z "$choice" ] && exit 0
echo -e "$choice" | awk -F ':' '{print $NF}' | sed 's/ //' | xargs $launcher	
	
}

# Browser
launch_browser() {

launcher="xdotool key"
fileone="$(cat /home/john/Documents/palettes/brave.txt)"
choice=$(echo -e "$fileone" | sort | rofi -dmenu -i -p '')
[ -z "$choice" ] && exit 0
echo -e "$choice" | awk -F ':' '{print $NF}' | sed 's/ //' | xargs $launcher
	
}

window_id=$(xdotool getactivewindow)
class_name=$(xprop -id "$window_id" | grep WM_CLASS | cut -d '"' -f2)
  
   if [[ "$(echo "$class_name")" = 'brave-browser' ]]; then
   launch_browser
   fi
   
   if [[ "$(echo "$class_name")" = 'DesktopEditors' ]]; then
   launch_only-office
   fi
   
   if [[ "$(echo "$class_name")" = 'gimp' ]]; then
   launch_gimp
   fi
      
