#!/bin/bash

# Program command: yad_settings_app.sh
# Description: Settings app for sway.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 18-08-2024
# Program_license: GPL 3.0
# Website: https://gitlab.com/wbua
# Dependencies: yad, slurp, grim, imagemagick, neofetch, gnome-terminal

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-------------------------------------------------------#
# Error checking
#-------------------------------------------------------#

set -e

#-------------------------------------------------------#
# User preferences
#-------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Wallpaper directory
WALLPAPER_DIR="$MAIN_WALLPAPER_DIR"
export WALLPAPER_DIR

#-------------------------------------------------------#
# Colors
#-------------------------------------------------------#

# Title
COLOR1=$(printf "<span color='#FFFFFF' font_family='Cascadia Mono' font='18' rise='0pt'>")
export COLOR1

# Info mode
COLOR2=$(printf "<span color='#F6FF00' font_family='Cascadia Mono' font='17' rise='0pt'>")
export COLOR2

# Info format
COLOR3=$(printf "<span color='#13FF00' font_family='Cascadia Mono' font='17' rise='0pt'>")
export COLOR3

# Color end
END=$(printf "</span>")
export END

#-------------------------------------------------------#
# Help page
#-------------------------------------------------------#

text_info() {

killall yad
info_text_file="${COLOR1}Settings app help page${END}

Setting: ${COLOR2}Sway${END}
Sway: ${COLOR2}keyboard layout${END}
Enter choice: ${COLOR3}us${END}

Setting: ${COLOR2}Sway${END}
Sway: ${COLOR2}Gap size${END}
Enter choice: ${COLOR3}10${END}

Setting: ${COLOR2}Sway${END}
Sway: ${COLOR2}Border size${END}
Enter choice: ${COLOR3}6${END}

Setting: ${COLOR2}Sway${END}
Sway: ${COLOR2}Opacity${END}
Enter choice: ${COLOR3}0.50${END}
1 is off

Setting: ${COLOR2}Sway${END}
Sway: ${COLOR2}Border color${END}
Enter choice: ${COLOR3}#ED00FF #ED00FF #000000${END}

Setting: ${COLOR2}Sway${END}
Sway: ${COLOR2}Gtk theme${END}
Enter choice: ${COLOR3}Yaru-dark${END}

Setting: ${COLOR2}Sway${END}
Sway: ${COLOR2}Icon theme${END}
Enter choice: ${COLOR3}Adwaita${END}

Setting: ${COLOR2}Sway${END}
Sway: ${COLOR2}Font${END}
Enter choice: ${COLOR3}Monospace 15${END}

"
echo "$info_text_file" | GTK_THEME="$MY_GTK_THEME" yad --list \
--column="info" \
--search-column=1 \
--regex-search \
--separator= \
--borders=20 \
--no-headers \
--width=700 \
--height=600 \
--button="Exit":1
	
}
export -f text_info

#-------------------------------------------------------#
# Change gtk theme (yad)
#-------------------------------------------------------#

change_gtk() {

sed -i "s|.*\(ALL_YAD_GTK=\).*|ALL_YAD_GTK=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
	
}
export -f change_gtk

#-------------------------------------------------------#
# Keyboard layout (sway)
#-------------------------------------------------------#

keyboard_layout() {

sed -i "s|.*\(SWAY_KEYBOARD_LAYOUT=\).*|SWAY_KEYBOARD_LAYOUT=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
bash ~/bin/sway_load_settings.sh
	
}
export -f keyboard_layout

#-------------------------------------------------------#
# Gap size (sway)
#-------------------------------------------------------#

gap_size() {

sed -i "s|.*\(gaps inner\).*|gaps inner $(printf '%s' "$field_choice")|" ~/.config/sway/gap_size || exit 1
bash ~/bin/sway_load_settings.sh

}
export -f gap_size

#-------------------------------------------------------#
# Border size (sway)
#-------------------------------------------------------#

border_size() {

sed -i "s|.*\(default_border pixel\).*|default_border pixel $(printf '%s' "$field_choice")|" ~/.config/sway/border_size || exit 1
bash ~/bin/sway_load_settings.sh
	
}
export -f border_size

#-------------------------------------------------------#
# Opacity (sway)
#-------------------------------------------------------#

sway_opacity() {

sed -i "s|.*\(set \$opacity\).*|set \$opacity $(printf '%s' "$field_choice")|" ~/.config/sway/set_opacity || exit 1
bash ~/bin/sway_load_settings.sh
	
}
export -f sway_opacity

#-------------------------------------------------------#
# Border color (sway)
#-------------------------------------------------------#

border_color() {

sed -i "s|.*\(client.focused\).*|client.focused $(printf '%s' "$field_choice")|" ~/.config/sway/border_color || exit 1
bash ~/bin/sway_load_settings.sh
	
}
export -f border_color

#-------------------------------------------------------#
# Update system (commands)
#-------------------------------------------------------#

update_system() {

gnome-terminal -- /bin/bash -c "sudo apt update && sudo apt full-upgrade; $SHELL" 

}
export -f update_system

#-------------------------------------------------------#
# System information (commands)
#-------------------------------------------------------#

system_info() {

gnome-terminal -- /bin/bash -c "neofetch; $SHELL"

}
export -f system_info

#-------------------------------------------------------#
# Random wallpaper (commands)
#-------------------------------------------------------#

random_wallpaper() {

ran_wall_file=$(ls "$WALLPAPER_DIR" | shuf -n1)
echo "$ran_wall_file" | tee "$HOME"/Documents/.walls_selected.txt
wall_file=$(cat "$HOME"/Documents/.walls_selected.txt)
sed -i 's|.png||g' ~/Documents/.walls_selected.txt
sed -i 's|$|.png|' ~/Documents/.walls_selected.txt
sed -i "s|.*\(MAIN_SWAY_WALLPAPER=\).*|MAIN_SWAY_WALLPAPER=\"$(printf '%s' "$WALLPAPER_DIR/$wall_file")\"|" ~/bin/sway_user_preferences.sh || exit 1

   if [[ "$(grep COLOR_AUTO= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     bash ~/bin/wallpaper_auto_colors.sh
   elif [[ "$(grep COLOR_AUTO= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     bash ~/bin/sway_load_settings.sh
   fi

}
export -f random_wallpaper

#-------------------------------------------------------#
# Enable/disable auto-colors (commands) (like pywal)
#-------------------------------------------------------#

change_auto_colors() {

sed -i "s|.*\(COLOR_AUTO=\).*|COLOR_AUTO=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
	
}
export -f change_auto_colors

#-------------------------------------------------------#
# Screenshot delay (commands)
#-------------------------------------------------------#

change_screenshot_delay() {

sed -i "s|.*\(SCREENSHOT_DELAY=\).*|SCREENSHOT_DELAY=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
	
}
export -f change_screenshot_delay


#-------------------------------------------------------#
# Screencast sound (commands)
#-------------------------------------------------------#

screencast_sound() {

sed -i "s|.*\(SCREENCAST_SOUND=\).*|SCREENCAST_SOUND=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
	
}
export -f screencast_sound

#-------------------------------------------------------#
# Image viewer (preferences)
#-------------------------------------------------------#

pre_image_viewer() {

sed -i "s|.*\(MAIN_IMAGE_VIEWER=\).*|MAIN_IMAGE_VIEWER=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_image_viewer

#-------------------------------------------------------#
# Image editor (preferences)
#-------------------------------------------------------#

pre_image_editor() {

sed -i "s|.*\(MAIN_IMAGE_EDITOR=\).*|MAIN_IMAGE_EDITOR=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_image_editor

#-------------------------------------------------------#
# text editor (preferences)
#-------------------------------------------------------#

pre_text_editor() {

sed -i "s|.*\(MAIN_TEXT_EDITOR=\).*|MAIN_TEXT_EDITOR=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_text_editor

#-------------------------------------------------------#
# Video player (preferences)
#-------------------------------------------------------#

pre_video_player() {

sed -i "s|.*\(MAIN_VIDEO_PLAYER=\).*|MAIN_VIDEO_PLAYER=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_video_player

#-------------------------------------------------------#
# Social media (preferences)
#-------------------------------------------------------#

social_media() {

sed -i "s|.*\(MAIN_SOCIAL=\).*|MAIN_SOCIAL=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f social_media

#-------------------------------------------------------#
# Other video player (preferences)
#-------------------------------------------------------#

pre_other_video_player() {

sed -i "s|.*\(MAIN_OTHER_VIDEO_PLAYER=\).*|MAIN_OTHER_VIDEO_PLAYER=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_other_video_player

#-------------------------------------------------------#
# Other terminal (preferences)
#-------------------------------------------------------#

pre_other_terminal() {

sed -i "s|.*\(MAIN_OTHER_TERMINAL=\).*|MAIN_OTHER_TERMINAL=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_other_terminal

#-------------------------------------------------------#
# Browser (preferences)
#-------------------------------------------------------#

pre_browser() {

sed -i "s|.*\(MAIN_BROWSER=\).*|MAIN_BROWSER=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_browser

#-------------------------------------------------------#
# Other browser (preferences)
#-------------------------------------------------------#

pre_other_browser() {

sed -i "s|.*\(MAIN_OTHER_BROWSER=\).*|MAIN_OTHER_BROWSER=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_other_browser

#-------------------------------------------------------#
# File manager (preferences)
#-------------------------------------------------------#

pre_file_manager() {

sed -i "s|.*\(MAIN_FILE_MANAGER=\).*|MAIN_FILE_MANAGER=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_file_manager

#-------------------------------------------------------#
# Other file manager (preferences)
#-------------------------------------------------------#

pre_other_file_manager() {

sed -i "s|.*\(MAIN_OTHER_FILE_MANAGER=\).*|MAIN_OTHER_FILE_MANAGER=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_other_file_manager

#-------------------------------------------------------#
# Pdf viewer (preferences)
#-------------------------------------------------------#

pre_pdf_viewer() {

sed -i "s|.*\(MAIN_PDF_VIEWER=\).*|MAIN_PDF_VIEWER=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_pdf_viewer

#-------------------------------------------------------#
# Audio player (preferences)
#-------------------------------------------------------#

pre_audio_player() {

sed -i "s|.*\(MAIN_AUDIO_PLAYER=\).*|MAIN_AUDIO_PLAYER=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_audio_player

#-------------------------------------------------------#
# Other audio player (preferences)
#-------------------------------------------------------#

pre_other_audio_player() {

sed -i "s|.*\(MAIN_OTHER_AUDIO_PLAYER=\).*|MAIN_OTHER_AUDIO_PLAYER=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_other_audio_player

#-------------------------------------------------------#
# Sound editor (preferences)
#-------------------------------------------------------#

pre_sound_editor() {

sed -i "s|.*\(MAIN_SOUND_EDITOR=\).*|MAIN_SOUND_EDITOR=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_sound_editor

#-------------------------------------------------------#
# Office (preferences)
#-------------------------------------------------------#

pre_office() {

sed -i "s|.*\(MAIN_OFFICE=\).*|MAIN_OFFICE=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_office

#-------------------------------------------------------#
# Other office (preferences)
#-------------------------------------------------------#

pre_other_office() {

sed -i "s|.*\(MAIN_OTHER_OFFICE=\).*|MAIN_OTHER_OFFICE=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f pre_other_office

#-------------------------------------------------------#
# Change gtk theme (Sway)
#-------------------------------------------------------#

change_gtk_theme() {

sed -i "s|.*\(SWAY_GTK_THEME=\).*|SWAY_GTK_THEME=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
setsid bash ~/bin/sway_load_settings.sh >/dev/null 2>&1 & disown 
swaymsg reload
sleep 5 && dunstctl close

}
export -f change_gtk_theme

#-------------------------------------------------------#
# Change icon theme (Sway)
#-------------------------------------------------------#

change_icon_theme() {

sed -i "s|.*\(SWAY_ICON_THEME=\).*|SWAY_ICON_THEME=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
setsid bash ~/bin/sway_load_settings.sh >/dev/null 2>&1 & disown 
swaymsg reload
sleep 5 && dunstctl close

}
export -f change_icon_theme

#-------------------------------------------------------#
# Change font (Sway)
#-------------------------------------------------------#

change_font() {

sed -i "s|.*\(font pango\:\).*|font pango\:$(printf '%s' "$field_choice")|" ~/.config/sway/sway_font || exit 1
bash ~/bin/sway_load_settings.sh
	
}
export -f change_font

#-------------------------------------------------------#
# Button color (yad)
#-------------------------------------------------------#

yad_button_color() {

sed -i "s|.*\(\@define-color yad-button-color\).*|@define-color yad-button-color $(printf '%s' "${field_choice};")|" ~/.local/share/themes/alt-dialog22/gtk-3.0/colors.css 

}
export -f yad_button_color

#-------------------------------------------------------#
# Selection color (yad)
#-------------------------------------------------------#

yad_select_color() {

sed -i "s|.*\(\@define-color yad-selection-color\).*|@define-color yad-selection-color $(printf '%s' "${field_choice};")|" ~/.local/share/themes/alt-dialog22/gtk-3.0/colors.css
	
}
export -f yad_select_color

#-------------------------------------------------------#
# Background color (yad)
#-------------------------------------------------------#

yad_bg_color() {

sed -i "s|.*\(\@define-color yad-background-color\).*|@define-color yad-background-color $(printf '%s' "${field_choice};")|" ~/.local/share/themes/alt-dialog22/gtk-3.0/colors.css
	
}
export -f yad_bg_color

#-------------------------------------------------------#
# Background color 2 (yad)
#-------------------------------------------------------#

yad_bg2_color() {

sed -i "s|.*\(\@define-color yad-auxiliary-color\).*|@define-color yad-auxiliary-color $(printf '%s' "${field_choice};")|" ~/.local/share/themes/alt-dialog22/gtk-3.0/colors.css
	
}
export -f yad_bg2_color

#-------------------------------------------------------#
# Entry color (wofi)
#-------------------------------------------------------#

wofi_entry() {

sed -i "s|.*\(\@define-color wofi-upsilon\).*|@define-color wofi-upsilon $(printf '%s' "${field_choice};")|" ~/.config/wofi/colors.css
	
}
export -f wofi_entry

#-------------------------------------------------------#
# Selection color (wofi)
#-------------------------------------------------------#

wofi_selection() {

sed -i "s|.*\(\@define-color wofi-omicron\).*|@define-color wofi-omicron $(printf '%s' "${field_choice};")|" ~/.config/wofi/colors.css
	
}
export -f wofi_selection

#-------------------------------------------------------#
# Border color (wofi)
#-------------------------------------------------------#

wofi_border() {

sed -i "s|.*\(\@define-color wofi-epsilon\).*|@define-color wofi-epsilon $(printf '%s' "${field_choice};")|" ~/.config/wofi/colors.css	

}
export -f wofi_border

#-------------------------------------------------------#
# Background color (wofi)
#-------------------------------------------------------#

wofi_bg() {

sed -i "s|.*\(\@define-color wofi-kappa\).*|@define-color wofi-kappa $(printf '%s' "${field_choice};")|" ~/.config/wofi/colors.css
	
}
export -f wofi_bg

#-------------------------------------------------------#
# Background color (dunst)
#-------------------------------------------------------#

dunst_background() {

sed -i "304s|^.*$|background = \"$(printf '%s' "${field_choice}")\"|" ~/.config/dunst/dunstrc
killall dunst
	
}
export -f dunst_background

#-------------------------------------------------------#
# Foreground color (dunst)
#-------------------------------------------------------#

dunst_foreground() {

sed -i "305s|^.*$|foreground = \"$(printf '%s' "$field_choice")\"|" ~/.config/dunst/dunstrc
killall dunst
	
}
export -f dunst_foreground

#-------------------------------------------------------#
# Frame color (dunst)
#-------------------------------------------------------#

dunst_frame() {

sed -i "94s|^.*$|frame_color = \"$(printf '%s' "$field_choice")\"|" ~/.config/dunst/dunstrc
killall dunst
	
}
export -f dunst_frame

#-------------------------------------------------------#
# Selection color (thunar)
#-------------------------------------------------------#

thunar_selection() {

sed -i "s|.*\(\@define-color thunar-alternative-color\).*|@define-color thunar-alternative-color $(printf '%s' "$field_choice");|" ~/.config/gtk-3.0/mytheme.css || exit 1

}
export -f thunar_selection

#-------------------------------------------------------#
# Background color (thunar)
#-------------------------------------------------------#

thunar_background() {

sed -i "s|.*\(\@define-color thunar-background-color\).*|@define-color thunar-background-color $(printf '%s' "$field_choice");|" ~/.config/gtk-3.0/mytheme.css || exit 1

}
export -f thunar_background

#-------------------------------------------------------#
# Position (waybar)
#-------------------------------------------------------#

waybar_position() {

sed -i "3s|^.*$| \"position\"\: \"$(printf '%s' "${field_choice}")\",|" ~/.config/waybar/config
killall waybar
sleep 0.5
setsid waybar >/dev/null 2>&1 & disown

}
export -f waybar_position

#-------------------------------------------------------#
# Color picker
#-------------------------------------------------------#

color_picker() {

grim -g "$(slurp -p)" -t ppm - | convert - -format '%[pixel:p{0,0}]' txt:- | \
tr -d '\n' | awk '{print $7}' | wl-copy -n

}
export -f color_picker

#-------------------------------------------------------#
# Main
#-------------------------------------------------------#

main() {

# Yad dialog
choice=$(GTK_THEME="$MY_GTK_THEME" yad --form --title="Settings app" \
--text="\n${COLOR1}Settings app${END}\nTo change setting press update\n" \
--text-align="center" \
--columns=1 \
--center \
--align="left" \
--width=600 \
--height=700 \
--borders=20 \
--scroll \
--separator="|" \
--field="Setting :CB" 'None!Sway!Yad!Preferences!Commands!Wofi!Dunst!Thunar!Waybar' \
--field="Sway :CB" 'None!Keyboard layout!Gap size!Border size!Opacity!Border color!Gtk theme!Icon theme!Font' \
--field="Yad :CB" 'None!Gtk theme!Button!Selection!Background!Bg2' \
--field="Commands :CB" 'None!Update system!System information!Random wallpaper!Auto-colors!Screenshot delay!Screencast sound' \
--field="Preferences :CB" 'None!Image viewer!Image editor!Text editor!Video player!Other video player!Terminal!Other terminal!Browser!Other browser!File manager!Other file manager!Pdf viewer!Audio player!Other audio player!Sound editor!Office!Other office!Social media' \
--field="Wofi :CB" 'None!Entry!Selection!Border!Background' \
--field="Dunst :CB" 'None!Background!Foreground!Frame' \
--field="Thunar :CB" 'None!Selection!Background' \
--field="Waybar :CB" 'None!Position' \
--field="Enter choice ":CE \
--button="_Picker:/bin/bash -c 'color_picker'" \
--button="_Help:/bin/bash -c 'text_info'" \
--button="_Update":0 \
--button="_Exit":1)
[[ -z "$choice" ]] && exit

# Setting
field1=$(echo "$choice" | awk -F '|' '{print $1}')
# Sway
field2=$(echo "$choice" | awk -F '|' '{print $2}')
# Yad
field3=$(echo "$choice" | awk -F '|' '{print $3}')
# Commands
field4=$(echo "$choice" | awk -F '|' '{print $4}')
# Preferences
field5=$(echo "$choice" | awk -F '|' '{print $5}')
# Wofi
field6=$(echo "$choice" | awk -F '|' '{print $6}')
# Dunst
field7=$(echo "$choice" | awk -F '|' '{print $7}')
# Thunar
field8=$(echo "$choice" | awk -F '|' '{print $8}')
# Waybar
field9=$(echo "$choice" | awk -F '|' '{print $9}')
# Enter choice
field_choice=$(echo "$choice" | awk -F '|' '{print $10}')

   # No setting
   if [[ "$field1" == *"None"* ]]; then
     :

   # Sway --------------------------------------------------------#
   
   # Sway keyboard layout
   elif [[ "$field1" == *"Sway"* && "$field2" == *"Keyboard layout"* ]]; then
     keyboard_layout
   # Sway border size
   elif [[ "$field1" == *"Sway"* && "$field2" == *"Border size"* ]]; then
     border_size
   # Sway Border color
   elif [[ "$field1" == *"Sway"* && "$field2" == *"Border color"* ]]; then
     border_color
   # Sway window opacity
   elif [[ "$field1" == *"Sway"* && "$field2" == *"Opacity"* ]]; then
     sway_opacity
   # Sway Gap size
   elif [[ "$field1" == *"Sway"* && "$field2" == *"Gap size"* ]]; then
     gap_size
   # Change gtk theme
   elif [[ "$field1" == *"Sway"* && "$field2" == *"Gtk theme"* ]]; then
     change_gtk_theme
   # Change icon theme
   elif [[ "$field1" == *"Sway"* && "$field2" == *"Icon theme"* ]]; then
     change_icon_theme
   # Change font
   elif [[ "$field1" == *"Sway"* && "$field2" == *"Font"* ]]; then
     change_font
     
   # Yad --------------------------------------------------------#
   
   # Yad gtk theme
   elif [[ "$field1" == *"Yad"* && "$field3" == *"Gtk theme"* ]]; then
     change_gtk
   # Yad button color
   elif [[ "$field1" == *"Yad"* && "$field3" == *"Button"* ]]; then
     yad_button_color
   # Yad Selection color
   elif [[ "$field1" == *"Yad"* && "$field3" == *"Selection"* ]]; then
     yad_select_color
   elif [[ "$field1" == *"Yad"* && "$field3" == *"Background"* ]]; then
     yad_bg_color
   # Background
   elif [[ "$field1" == *"Yad"* && "$field3" == *"Bg2"* ]]; then
     yad_bg2_color
   

   # Commands --------------------------------------------------------#
    
   # Commands update system
   elif [[ "$field1" == *"Commands"* && "$field4" == *"Update system"* ]]; then
     update_system
   # Commands system information
   elif [[ "$field1" == *"Commands"* && "$field4" == *"System information"* ]]; then
     system_info
   # Commands random wallpaper
   elif [[ "$field1" == *"Commands"* && "$field4" == *"Random wallpaper"* ]]; then
     random_wallpaper
   # Enable/disable auto colors (like pywal)
   elif [[ "$field1" == *"Commands"* && "$field4" == *"Auto-colors"* ]]; then
     change_auto_colors
   # Screenshot delay
   elif [[ "$field1" == *"Commands"* && "$field4" == *"Screenshot delay"* ]]; then
     change_screenshot_delay
   # Screencast sound
   elif [[ "$field1" == *"Commands"* && "$field4" == *"Screencast sound"* ]]; then
     screencast_sound

   # Preferences --------------------------------------------------------#

   # Preferences image viewer
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Image viewer"* ]]; then
     pre_image_viewer
   # Preferences image editor
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Image editor"* ]]; then
     pre_image_editor
   # Preferences text editor
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Text editor"* ]]; then
     pre_text_editor
   # Preferences Video player
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Video player"* ]]; then
     pre_video_player
   # Preferences Other video player
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Other video player"* ]]; then
     pre_other_video_player
   # Preferences terminal
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Terminal"* ]]; then
     pre_terminal
   # Preferences other terminal
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Other terminal"* ]]; then
     pre_other_terminal
   # Preferences browser
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Browser"* ]]; then
     pre_browser
   # Preferences other browser
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Other browser"* ]]; then
     pre_other_browser
   # Preferences file manager
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"File manager"* ]]; then
     pre_file_manager
   # Preferences other file manager
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Other file manager"* ]]; then
     pre_other_file_manager
   # Preferences pdf viewer
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Pdf viewer"* ]]; then
     pre_pdf_viewer
   # Preferences audio player
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Audio player"* ]]; then
     pre_audio_player
   # Preferences other audio player
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Other audio player"* ]]; then
     pre_other_audio_player
   # Preferences sound editor
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Sound editor"* ]]; then
     pre_sound_editor
   # Preferences Office
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Office"* ]]; then
     pre_office
   # Preferences other office
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Other office"* ]]; then
     pre_other_office
   elif [[ "$field1" == *"Preferences"* && "$field5" == *"Social media"* ]]; then
     social_media


   # Wofi --------------------------------------------------------#

   # Wofi entry
   elif [[ "$field1" == *"Wofi"* && "$field6" == *"Entry"* ]]; then
     wofi_entry
   # Wofi selection
   elif [[ "$field1" == *"Wofi"* && "$field6" == *"Selection"* ]]; then
     wofi_selection
   # Wofi border
   elif [[ "$field1" == *"Wofi"* && "$field6" == *"Border"* ]]; then
     wofi_border
   elif [[ "$field1" == *"Wofi"* && "$field6" == *"Background"* ]]; then
     wofi_bg

   # Dunst --------------------------------------------------------#

   # Dunst background
   elif [[ "$field1" == *"Dunst"* && "$field7" == *"Background"* ]]; then
     dunst_background
   # Dunst foreground
   elif [[ "$field1" == *"Dunst"* && "$field7" == *"Foreground"* ]]; then
     dunst_foreground
   # Dunst frame
   elif [[ "$field1" == *"Dunst"* && "$field7" == *"Frame"* ]]; then
     dunst_frame

   # Thunar --------------------------------------------------------#

   # Selection
   elif [[ "$field1" == *"Thunar"* && "$field8" == *"Selection"* ]]; then
     thunar_selection
   # Background
   elif [[ "$field1" == *"Thunar"* && "$field8" == *"Background"* ]]; then
     thunar_background

   # Waybar --------------------------------------------------------#

   # Position
   elif [[ "$field1" == *"Waybar"* && "$field9" == *"Position"* ]]; then
     waybar_position
     
   else
     :
   fi

}
export -f main
main

#-------------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------------#

unset main
unset change_gtk
unset field1
unset keyboard_layout
unset gap_size
unset COLOR1
unset COLOR2
unset COLOR3
unset END
unset MY_GTK_THEME
unset border_size
unset sway_opacity
unset border_color
unset update_system
unset system_info
unset random_wallpaper
unset pre_image_viewer
unset pre_image_editor
unset pre_text_editor
unset pre_video_player
unset pre_other_video_player
unset pre_terminal
unset pre_other_terminal
unset pre_browser
unset pre_other_browser
unset pre_file_manager
unset pre_other_file_manager
unset pre_pdf_viewer
unset pre_audio_player
unset pre_other_audio_player
unset pre_sound_editor
unset pre_office
unset pre_other_office
unset change_gtk_theme
unset change_icon_theme
unset change_font
unset text_info
unset yad_button_color
unset yad_select_color
unset wofi_entry
unset wofi_selection
unset wofi_border
unset dunst_background
unset dunst_foreground
unset dunst_frame
unset thunar_selection
unset color_picker
unset yad_bg_color
unset wofi_bg
unset waybar_position
unset change_auto_colors
unset social_media
unset thunar_background
unset change_screenshot_delay
unset screencast_sound
unset WALLPAPER_DIR
