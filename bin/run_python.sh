#!/bin/bash

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Run the Python script and capture the output into a variable
output1=$(python3 ~/bin/gui_entry_to_stdout.py)
[[ -z "$output1" ]] && exit

# Change run launcher
sed -i "s|.*\(SELECT_RUN_LAUNCHER=\).*|SELECT_RUN_LAUNCHER=\"$(printf '%s' "$output1")\"|" ~/bin/sway_user_preferences.sh || exit 1
