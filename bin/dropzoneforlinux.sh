#!/bin/bash

# Program command: dropzoneforlinux
# Description: Drag files to perform various actions.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 06-06-2024
# Program_license: GPL 3.0
# Dependencies: yad, gtk.css, wl-clipboard, qrencode

#---------------------------------------------------------------#
# Error checking
#---------------------------------------------------------------#

set -e

#---------------------------------------------------------------#
# User preferences
#---------------------------------------------------------------#

# Set yad gtk theme
gtk_theme="alt-dialog22"
export gtk_theme

#---------------------------------------------------------------#
# Create files and directories
#---------------------------------------------------------------#

# Create documents directory
   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

# Create dropzone directory
   if [ -d "$HOME"/Documents/dropzone ]; then
     :
   else
     mkdir "$HOME"/Documents/dropzone
   fi

#---------------------------------------------------------------#
# Colors
#---------------------------------------------------------------#

# Titles
color1=$(printf "<span color='#FFFFFF' font_family='Cascadia Mono' font='18' rise='0pt'>")
export color1
# 
color2=$(printf "<span color='#4AFF00' font_family='Cascadia Mono' font='17' rise='0pt'>")
export color2
# Color end
end=$(printf "</span>")
export end

#---------------------------------------------------------------#
# Clipboard actions (copies path)
#---------------------------------------------------------------#

clipboard_action() {

# Kills yad
killall yad

# Yad dnd settings
GTK_THEME="$gtk_theme" yad --dnd \
--width=500 \
--text="\nCopies file path to clipboard\n" \
--text-align="center" \
--height=500 \
--button="Ok":0 \
--button="Close":1 | tee ~/Documents/.dnd_dropzone_clipboard.txt

# Removes things from text file
sed -i 's|file://||' "$HOME"/Documents/.dnd_dropzone_clipboard.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.dnd_dropzone_clipboard.txt
sed -i 's|%20| |g' "$HOME"/Documents/.dnd_dropzone_clipboard.txt

# Copies text file contents to clipboard
choice05=$(cat ~/Documents/.dnd_dropzone_clipboard.txt)
echo "$choice05" | wl-copy -n
	
}
export -f clipboard_action

#---------------------------------------------------------------#
# QR Code actions
#---------------------------------------------------------------#

qr_code_action() {

# Kills yad
killall yad

# Yad dnd settings
GTK_THEME="$gtk_theme" yad --dnd \
--text="\nCreates QR Code\n" \
--text-align="center" \
--width=500 \
--height=500 \
--button="Ok":0 \
--button="Close":1 | tee ~/Documents/.dnd_dropzone_qrcode.txt

# Removes things from text file
sed -i 's|file://||' "$HOME"/Documents/.dnd_dropzone_qrcode.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.dnd_dropzone_qrcode.txt
sed -i 's|%20| |g' "$HOME"/Documents/.dnd_dropzone_qrcode.txt

# Create qr code from text file
choice04=$(cat ~/Documents/.dnd_dropzone_qrcode.txt)
echo "$choice04" | tr -d '\n' | qrencode -m 10 -l L -o "$HOME/Pictures/$(date '+%d-%m-%Y-%H-%M-%S')".png
	
}
export -f qr_code_action

#---------------------------------------------------------------#
# Videos directory actions
#---------------------------------------------------------------#

videos_action() {

# Kills yad
killall yad

# Yad dnd settings
GTK_THEME="$gtk_theme" yad --dnd \
--text="\nCopies to Videos directory\n" \
--text-align="center" \
--width=500 \
--height=500 \
--button="Ok":0 \
--button="Close":1 | tee ~/Documents/.dnd_dropzone_videos.txt

# Removes things from text file
sed -i 's|file://||' "$HOME"/Documents/.dnd_dropzone_videos.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.dnd_dropzone_videos.txt
sed -i 's|%20| |g' "$HOME"/Documents/.dnd_dropzone_videos.txt

# Copy whats in text file to videos directory
choice03=$(cat ~/Documents/.dnd_dropzone_videos.txt)
cp "$choice03" ~/Videos/
	
}
export -f videos_action

#---------------------------------------------------------------#
# Documents directory actions
#---------------------------------------------------------------#

documents_action() {

# Kills yad
killall yad

# Yad dnd settings
GTK_THEME="$gtk_theme" yad --dnd \
--text="\nCopies to Documents directory\n" \
--text-align="center" \
--width=500 \
--height=500 \
--button="Ok":0 \
--button="Close":1 | tee ~/Documents/.dnd_dropzone_documents.txt

# Removes things from text file
sed -i 's|file://||' "$HOME"/Documents/.dnd_dropzone_documents.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.dnd_dropzone_documents.txt
sed -i 's|%20| |g' "$HOME"/Documents/.dnd_dropzone_documents.txt

# Copy whats in text file to documents directory
choice02=$(cat ~/Documents/.dnd_dropzone_documents.txt)
cp "$choice02" ~/Documents/
	
}
export -f documents_action

#---------------------------------------------------------------#
# Download directory actions
#---------------------------------------------------------------#

downloads_action() {

# Kills yad
killall yad

# Yad dnd settings
GTK_THEME="$gtk_theme" yad --dnd \
--text="\nCopies to Downloads directory\n" \
--text-align="center" \
--width=500 \
--height=500 \
--button="Ok":0 \
--button="Close":1 | tee ~/Documents/.dnd_dropzone_dowloads.txt

# Removes things from text file
sed -i 's|file://||' "$HOME"/Documents/.dnd_dropzone_dowloads.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.dnd_dropzone_dowloads.txt
sed -i 's|%20| |g' "$HOME"/Documents/.dnd_dropzone_dowloads.txt

# Copy whats in text file to downloads directory
choice01=$(cat ~/Documents/.dnd_dropzone_dowloads.txt)
cp "$choice01" ~/Downloads/
	
}
export -f downloads_action
 
#---------------------------------------------------------------#
# Main
#---------------------------------------------------------------#

GTK_THEME="$gtk_theme" yad --form \
--text="\n${color1}Dropzone for linux${end}\nDrag and drop file or text\n" \
--text-align="center" \
--borders=20 \
--columns=5 \
--width=600 \
--height=600 \
--field="!$HOME/Documents/dropzone/videos_folder.png!Videos":fbtn '/bin/bash -c "videos_action"' \
--field="!$HOME/Documents/dropzone/documents_folder.png!Documents":fbtn '/bin/bash -c "documents_action"' \
--field="!$HOME/Documents/dropzone/download_folder.png!Downloads":fbtn '/bin/bash -c "downloads_action"' \
--field="!$HOME/Documents/dropzone/qr_code.png!QR Code":fbtn '/bin/bash -c "qr_code_action"' \
--field="!$HOME/Documents/dropzone/clipboard.png!Clipboard":fbtn '/bin/bash -c "clipboard_action"' \
--button="Close":1

#---------------------------------------------------------------#
# Unset variables and functions
#---------------------------------------------------------------#

unset downloads_action
unset documents_action
unset videos_action
unset qr_code_action
unset clipboard_action
unset gtk_theme
unset color1
unset color2
unset end
