#!/bin/bash

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

GTK_THEME="$MY_GTK_THEME" yad --image "gtk-close" \
--title "Alert" \
--button=gtk-yes:0 \
--button=gtk-no:1 \
--text "Close focused window?" && swaymsg kill
