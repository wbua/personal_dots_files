#!/bin/bash

# Program command: hub_central.sh
# Description: This is a hub for my websites and scripts.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 20-08-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad

killall yad

#------------------------------------------------------------#
# General variables
#------------------------------------------------------------#


#------------------------------------------------------------#
# Yad size dimensions
#------------------------------------------------------------#

width="1400"
export res_width

height="800"
export res_height

#------------------------------------------------------------#
# Colors
#------------------------------------------------------------#

color1="<span color='#FFFFFF' font_family='Monospace' font='25'>"
export color1
color2="<span color='#FB00FF' font_family='Monospace' font='25'>"
export color2
color3="<span color='#FB00FF' font_family='Monospace' font='40'>"
export color3
color4="<span color='#FFFFFF' font_family='Monospace' font='18'>"
export color4
color5="<span color='#FB00FF' font_family='Monospace' font='15'>"
export color5
end="</span>"
export end


#------------------------------------------------------------#
# Greeting
#------------------------------------------------------------#

greeting() {

hour=$(date +%H)
export hour

   if [ "$hour" -ge 12 ] && [ "$hour" -lt 19 ]; then
     echo "${color1}Good afternoon${end}" "${color2}$USER${end}"
   fi

   if [ "$hour" -ge 1 ] && [ "$hour" -lt 12 ]; then
     echo "${color1}Good morning${end}" "${color2}$USER${end}"
   fi

   if [ "$hour" -ge 19 ] && [ "$hour" -lt 24 ]; then
     echo "${color1}Good evening${end}" "${color2}$USER${end}"
   fi	
	
}
export -f greeting

#------------------------------------------------------------#
# 
#------------------------------------------------------------#

radio_info() {

#echo "title"	
playerctl --player mpv metadata -f "{{title}}" | cut -c 1-70 | tr "&" "+"
	
}
export -f radio_info

#------------------------------------------------------------#
# 
#------------------------------------------------------------#

shuffle_radio() {

text_file=$(cat "$HOME"/Documents/mainprefix/radiolinks.txt)
export text_file
killall yad
killall mpv
echo "$text_file" | shuf -n1 | tee "$HOME"/Documents/temp_radio_shuf.txt
cat "$HOME"/Documents/temp_radio_shuf.txt | xargs mpv
sleep 1
hub_central.sh

}
export -f shuffle_radio

#------------------------------------------------------------#
# 
#------------------------------------------------------------#

play_radio() {

      shuf_file=$(cat "$HOME"/Documents/temp_radio_shuf.txt)
	  export shuf_file
   if [[ -z "$shuf_file" ]]; then
     text_file=$(cat "$HOME"/Documents/mainprefix/radiolinks.txt)
     export text_file
     killall yad
     killall mpv
     echo "$text_file" | shuf -n1 | tee "$HOME"/Documents/temp_radio_shuf.txt
     cat "$HOME"/Documents/temp_radio_shuf.txt | xargs mpv
     sleep 1
     hub_central.sh
   else
     killall yad
     killall mpv	
     cat "$HOME"/Documents/temp_radio_shuf.txt | xargs mpv
     sleep 1
     hub_central.sh
   fi
	
}
export -f play_radio

#------------------------------------------------------------#
# 
#------------------------------------------------------------#

stop_mpv() {
	
killall mpv	
	
}
export -f stop_mpv

#------------------------------------------------------------#
# 
#------------------------------------------------------------#

pause_mpv() {
	
playerctl --player mpv pause	
	
}
export -f pause_mpv

#------------------------------------------------------------#
# Main
#------------------------------------------------------------#

main() {

# Websites page
GTK_THEME="alt-dialog8" yad --form --separator= --no-buttons \
--width="$width" --height="$height" \
--columns=3 \
--borders="0" \
--align="center" \
--field="":LBL "" \
--field="":LBL "" \
--text="${color3}$(date '+%H:%M')${end}\n${color4}$(date '+%d-%m-%Y')${end}\n\n$(greeting)\n\n\n" \
--field="":LBL "" \
--field="":LBL "" \
--field="":LBL "" \
--field="$(radio_info)":LBL "" \
--field="<span color='#ffffff' font_family='Material Icons' font='25'></span>":BTN "/bin/bash -c 'pause_mpv'" \
--field="<span color='#ffffff' font_family='Material Icons' font='40'></span>":BTN "/bin/bash -c 'play_radio'" \
--field="<span color='#ffffff' font_family='Material Icons' font='25'></span>":BTN "/bin/bash -c 'stop_mpv'" \
--field="<span color='#ffffff' font_family='Material Icons' font='25'></span>":BTN "/bin/bash -c 'shuffle_radio'" \
--field="":LBL "" \
--field="":LBL "" \
--field="":LBL "" \
--field="":LBL "" \
--field="":LBL "" \
--button="Exit":1

}
export -f main
main

#------------------------------------------------------------#
# Unset variables and functions
#------------------------------------------------------------#

unset key
unset height
unset width
unset main
unset greeting
unset hour
unset color1
unset color2
unset color3
unset color4
unset color5
unset end
unset radio_info
unset play_radio
unset text_file
unset shuffle_radio
unset stop_mpv
unset shuf_file
unset pause_mpv
