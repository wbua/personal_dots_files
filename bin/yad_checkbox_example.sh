#!/bin/bash

yad --list \
--center \
--height="400" \
--checklist \
--column="Select" \
--column="Device to mount:" \
--print-column="2" FALSE selection1 FALSE selection2 FALSE selection3 \
--multiple \
--separator=" "
