#!/bin/bash

choice=$(find ~/Downloads/ -type f -iname "*" -print0 | xargs -0 ls -t | cut -d '/' -f5-  | rofi -dmenu -i -p '')
[ -z "$choice" ] && exit 0
echo "$HOME"/Downloads/"$choice" | xargs -0 -d '\n' xdg-open
