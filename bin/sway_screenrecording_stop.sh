#!/bin/bash

# Program command: sway_screenrecording_stop
# Description: Stops screen recording.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 29-07-2024
# Program_license: GPL 3.0
# Dependencies: yad, wf-recorder, imagemagick

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#------------------------------------------------------#
# Error checking
#------------------------------------------------------#

set -e

#------------------------------------------------------#
# User preferences
#------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Video player
VIDEO_PLAYER="$MAIN_VIDEO_PLAYER"
export VIDEO_PLAYER

# File manager
FILE_MANAGER="$MAIN_FILE_MANAGER"
export FILE_MANAGER

# Screencast recording directory
SCREENCAST_DIR="$MAIN_SCREENCAST_DIR"
export SCREENCAST_DIR

#------------------------------------------------------#
# Create files and directories
#------------------------------------------------------#

   if [ -d "$SCREENCAST_DIR" ]; then
     :
   else
     notify-send "Screencast directory does not exist!" && exit 1
   fi

#------------------------------------------------------#
# Play screen recording (screencast)
#------------------------------------------------------#

play_rec() {

   killall yad
   if [[ -z "$(ls -A "$HOME"/Videos/.yadvidrec)" ]]; then	
     notify-send "Pdf directory is empty"
   else
     find "$HOME"/Videos/.yadvidrec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 -d '\n' "$VIDEO_PLAYER"
   fi

}
export -f play_rec

#------------------------------------------------------#
# Delete recording
#------------------------------------------------------#

del_rec() {

killall yad
select_file=$(find "$HOME"/Videos/.yadvidrec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -n 1 basename)
rm -f "$SCREENCAST_DIR"/"$select_file" || exit 1
rm -f "$HOME"/Videos/.yadvidrec/* || exit 1
rm -f "$HOME"/Videos/.temp_record_image/* || exit 1
	
}
export -f del_rec

#------------------------------------------------------#
# Open screen recording directory
#------------------------------------------------------#

open_dir() {

"$FILE_MANAGER" "$SCREENCAST_DIR"
	
}
export -f open_dir

#------------------------------------------------------#
# Yad dialog
#------------------------------------------------------#

_yad() {

GTK_THEME="$MY_GTK_THEME" yad --form --width=450 --height=400 \
--title="Screen Recorder" \
--image-on-top \
--columns=3 \
--borders=20 \
--image-on-top \
--image="$HOME/Videos/.temp_record_image/thumbnail01.png" \
--text-align="center" \
--buttons-layout="center" \
--field=" ":LBL "" \
--field=" ":LBL "" \
--field=" ":LBL "" \
--field="<b>Open directory</b>":fbtn '/bin/bash -c "open_dir"' \
--field="<b>Play recording</b>":fbtn '/bin/bash -c "play_rec"' \
--field="<b>Delete recording</b>":fbtn '/bin/bash -c "del_rec"' \
--field=" ":LBL "" \
--field=" ":LBL "" \
--field=" ":LBL "" \
--button="<span color='#ffffff' font_family='Material Icons' font='25'></span>":1

}

#------------------------------------------------------#
# Main
#------------------------------------------------------#

   if [[ "$(pidof wf-recorder)" ]]; then
     killall -s SIGINT wf-recorder
     cp "$HOME"/Videos/.yadvidrec/*.* "$SCREENCAST_DIR"
     rm -f "$HOME"/Videos/.temp_record_image/*
     convert "$HOME"/Videos/.yadvidrec/*.mkv[10] "$HOME"/Videos/.temp_record_image/thumbnail.png
     convert "$HOME"/Videos/.temp_record_image/thumbnail.png -resize 512x288 "$HOME"/Videos/.temp_record_image/thumbnail01.png   
     _yad
   else
     :
   fi

#------------------------------------------------------#
# Unset variables and functions
#------------------------------------------------------#

unset _yad
unset play_rec
unset VIDEO_PLAYER
unset del_rec
unset open_dir
unset FILE_MANAGER
unset MY_GTK_THEME
unset SCREENCAST_DIR
