#!/bin/bash
# shellcheck disable=SC2034
# shellcheck disable=SC2154

# Program command: johns_launcher.sh
# Description: This is johns launcher.
# Contributors: John Mcgrath
# Program_version: 2.0.0
# Program updated: 16-04-2024
# Program_license: GPL 3.0
# Dependencies:
#rofi, zenity, slock, xdotool, playerctl, yt-dlp, mpv, mpv-mpris, ffmpeg, yt-dlp, xclip, xsel, gpick
#maim, amixer, wmctrl, yad, feh, imagemagick, qrencode, zbar-tools ,libreoffice, festival, arecord, wl-clipboard
#pulseaudio, amixer, audacious, dictd, dict-gcide, gawk, material icons outlined, Noto Color Emoji.

#-------------------------------------------------------#
# Error checking
#-------------------------------------------------------#

#set -e

#-------------------------------------------------------#
# Source file for colors
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_colors.sh

#-------------------------------------------------------#
# Source file for functions
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_functions.sh

#-------------------------------------------------------#
# Source file for config
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_config.sh

#-------------------------------------------------------#
# Source file for creating files and directories
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_files_directories.sh

#-------------------------------------------------------#
# Source file for misc
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_misc.sh

#-------------------------------------------------------#
# Create directories and files
#-------------------------------------------------------#

create_file_and_directories
                
#-------------------------------------------------------#
# Misc
#-------------------------------------------------------#

misc_functions

#-------------------------------------------------------#
# Main
#-------------------------------------------------------#

selected=$(menu | _rofi | awk -F "${color26}" '{print $2,$3,$4,$5,$6,$7,$8,$9,$10}' | sed 's|</span>||')
bang=$(echo "${selected[@]}" | awk '{print $2}')
extra_content=$(echo "${selected[@]}" | awk '{print $3,$4,$5,$6,$7,$8,$9,$10}')
launchercontent=$(echo "$selected" | awk '{print $3}')
[ -z "$selected" ] && exit 0
[ -z "$launchercontent" ] && exit 0

case "$bang" in

  # Image editor
 "$image_editor")
  gimp_class_name
  ;;
  # Last sub menu used
 'sub')
  last_sub_menu
  ;;
  # Exit rofi
 'exit-rofi')
  killall rofi
  ;;
  # Reminders
 'reminder')
  show_reminders
  ;;
  # Oneline notes
 'oneline-notes')
  oneline_notes
  ;;
  # Color picker
 'pick')
  root_pick
  ;;
  # Brave bookmarks
 'brave-bookmarks')
  brave_bookmarks
  ;;
  # Quicklinks help page for root search
 'quicklinks-help')
  ql_info_root
  ;;
  # Last modified sound file
 'mod-sound')
  last_mod_sound
  ;;
  # Last modified video
 'mod-video')
  last_mod_video
  ;;
  # Video cutter
 'video-cutter')
  video_cutter
  ;;
  # Google recent
 'go-search')
  google_recent
  ;;
  # Google images recent search
 'gi-search')
  google_images_recent
  ;;
  # Duckduckgo recent
 'ddg-search')
  duckduckgo_recent
  ;;
  # Youtube recent
 'yt-search')
  youtube_recent
  ;;
  # Reddit recent
 'rd-search')
  reddit_recent
  ;;
  # QR Code gallery
 'qr-gallery')
  qr_code_gallery
  ;;
  # Color codes
 'hex-codes')
  hex_codes
  ;;
  # Download videos
 'do')
   if [[ "$launchercontent" == 'open' ]]; then
     do_open
   else
     do_out
   fi
  ;;
  # Download file
 'wg')
   if [[ "$launchercontent" == 'open' ]]; then
     open_download
   elif [[ "$launchercontent" == 'wallpaper' ]]; then
     down_set_wall
   else
     download_file
   fi
  ;; 
  # Control media players with playerctl
 'brave-playerctl')
  media_brave
  ;;
  # Add quicklink for root search
 'add-quicklink')
  add_quicklink
  ;;
  # Your IP address
 'ip-address')
  your_ip_address
  ;;
  # Google search from text
 'clip-search')
  clip_search
  ;;
  # Find file from text
 'clip-find')
  clip_find
  ;;
  # Dictionary search from text
 'clip-dictionary')
  clip_dictionary
  ;;
  # Microphone toggle
 'microphone-toggle')
  microphone_toggle
  ;;
  # Microphone check
 'check-microphone')
  check_microphone
  ;;
  # Command palettes
 'keys')
  main_command
  ;;
  # Weather
 'weather')
  main_weather
  ;;
  # Actions
 'actions')
  rofi_actions
  ;;
  # Actions downloads
 'actions-downloads')
  rofi_actions_download
  ;;
  # Radio actions
 'actions-radio')
  rofi_actions_radio
  ;;
  # Actions text
 'actions-block')
  rofi_actions_block
  ;;
  # Actions bookmarks
 'actions-bookmarks')
  rofi_actions_bookmarks
  ;;
  # Search for directories
 'directories')
  find_directories
  ;;
  # Actions for directories
 'actions-directories') 
  rofi_actions_directories
  ;;
  # Open last modified documents
 'mod-documents')
  last_mod_docs
  ;;
  # Open last download image
 'mod-gimp')
  last_mod_downloaded
  ;;
  # Last downloaded file
 'mod-downloaded')
  last_mod_downloaded_file
  ;;
  # Open last modified image
 'mod-image')
  last_mod_image
  ;;
  # Set wallpaper and change autostart script
 'wall')
  walloptions
  ;;
  # View screenshots in rofi
 'shots')
  view_screenshots
  ;;
  # Select screenshot
 'select')
  take_select_screenshot
  ;;
  # Delay
 'delay')
  take_delayed_screenshot
  ;;
  # Rofi theme changer
 'theme')
  rofi_theme_changer
  ;;
  # Resize image
 'resize')
  resize_root
  ;;
  # Take fullscreen screenshot
 'full')
  take_screenshot
  ;;
  # Last created temporary files
 'last')
  open_last
  ;;
  # Recent screenshots
 'recent')
  recent_screenshots
  ;;
  # Video recording
 'record')
  video_record  
  ;; 
  # Record status
 'status')
  record_status
  ;;
  # Audio recording
 'audio')
  audio_record
  ;;
  # Stop video and audio recording
 'stop')
  stop_recording
  ;;
  # Opens last video recording
 'vplay')
  video_play
  ;;
  # Open last audio recording
 'aplay')
  audio_play
  ;;
  # Select screenshot
 'select')
  ss_select
  ;;
  # Delay 5 seconds screenshot
 'delay')
  ss_delay
  ;;
  # Mute (toggle)
 'mute')
  vol_mute
  ;;
  # Set volume to 100%
 '100')
  vol_100
  ;;
  # Set volume to 50%
 '50')
  vol_50
  ;;
  # Set volume to 70%
 '70')
  vol_70
  ;;
  # Plays radio stations
 'radio')
  online_radio
  ;;
  # Extra radio station prefixes
 'ra')
   if [[ "$launchercontent" == 'txt' ]]; then
     ra_txt
   elif [[ "$launchercontent" == 'space' ]]; then
     ra_space
   elif [[ "$launchercontent" == 'clip' ]]; then
     ra_clip
   elif [[ "$launchercontent" == 'notify' ]]; then
     ra_notify
   elif [[ "$launchercontent" == 'record' ]]; then
     recordradiompv
   elif [[ "$launchercontent" == 'play' ]]; then
     playrecfg
   elif [[ "$launchercontent" == 'stop' ]]; then
     stopmpv
   elif [[ "$launchercontent" == 'qrcode' ]]; then
     ra_qrcode
   elif [[ "$launchercontent" == '50' ]]; then
     ra_vol50
   elif [[ "$launchercontent" == '70' ]]; then
     ra_vol70
   elif [[ "$launchercontent" == '100' ]]; then
     ra_vol100
   else
     ra_out
   fi
  ;;
 # Open bookmarks
 'bookmarks')
  open_bookmarks
  ;;
 # Open bookmarks with firefox
 'F')
  open_firefox
  ;;
  # Downloads folder
 'downloads')
  download_folder
  ;;
  # Add bookmark
 'bo')
   if [[ "$launchercontent" == 'space' ]]; then
     bo_space
   elif [[ "$launchercontent" == 'txt' ]]; then
     bo_txt
   elif [[ "$launchercontent" == 'clip' ]]; then
     bo_clip
   else
     bo_out
   fi
  ;;
  # Configs
 'configs')
  system_configs
  ;;
  # Google search
 'go')
   web_google
  ;;
  # Google images
 'gi')
   if [[ "$launchercontent" == '#1' ]]; then
     last_google_search_images
   else
    google_images
   fi
  ;;
  # Collins dictionary
 'di')
  web_collins
 ;;
  # DuckDuckGo
 'ddg')
  duck_duck_go
  ;;
  # Youtube search
 'yt')
  web_youtube 
  ;;
  # Wiki search
 'wik')
  web_wiki
  ;;
  # Arch wiki
 'aw')
  arch_wiki
  ;;
  # Google maps
 'gm')
  google_maps
  ;;
  # Reddit
 'rd')
  reddit
  ;;
  # Google news
 'gn')
  google_news
  ;;
  # Amazon search
 'am')
  amazon
  ;;
  # Stackoverflow
 'so')
  stackoverflow
  ;;
  # Imdb
 'im')
  imdb
  ;;
  # Translate english to french
 'gtf')
  google_trans_fr
  ;;
  # Translate english to spanish
 'gts')
  google_trans_es
  ;;
  # Poweroff
 'shutdown')
  yadshutdown
 ;;
  # Restart
 'restart')
  yadreboot
  ;;
  # Lock (slock)
 'lock')
  yadlock
  ;;
  # Find local files
 'find')
  find_files
  ;;
  # Find all file
 'all')
  find_all_files
  ;;
  # Find recent files
 'fi')
   if [[ "$launchercontent" == 'recent' ]]; then
     find_recent
   elif [[ "$launchercontent" == 'gimp' ]]; then
     images_recent
   elif [[ "$launchercontent" == 'dir' ]]; then
     find_directories
   elif [[ "$launchercontent" == 'ter' ]]; then
     find_terminal_directories
   elif [[ "$launchercontent" == 'clip' ]]; then
     copy_file_path
   elif [[ "$launchercontent" == 'clip-dir' ]]; then
     clip_directories
   elif [[ "$launchercontent" == 'ext' ]]; then
     find_without_extension
   elif [[ "$launchercontent" == 'man' ]]; then
     find_file_manager
   else
     :
   fi
  ;;
  # File manager
 'file-manager')
  rofifilemanager
  ;;
  # Calculator
 'cal')
  my_calculator
  ;;
 'W')
  window_switcher
 ;;
  # Kill app
 'kill')
  kill_process
  ;;
  # Open bash scripts gui
 'bin')
  bash_scripts_gui
  ;;
  # Open bash scripts cli
 'X')
  bash_scripts_cli
  ;;
  # Exec bash scripts
 'E')
  exec_scripts
  ;;
  # Bash scripts
 'te')
   if [[ "$launchercontent" == 'e' ]]; then
     te_exec
   else
     te_out
   fi
  ;;
  # QR Codes
 'qr')
   if [[ "$launchercontent" == 'decode' ]]; then
     qr_decode
   elif [[ "$launchercontent" == 'clip' ]]; then
     qr_clip
   elif [[ "$launchercontent" == 'view' ]]; then
     qr_view
   elif [[ "$launchercontent" == 'open' ]]; then
     qr_open
   elif [[ "$launchercontent" == 'dir' ]]; then
     qr_dir
   elif [[ "$launchercontent" == 'speak' ]]; then
     qr_speak
   elif [[ "$launchercontent" == 'ocr' ]]; then
     qr_ocr
   else
     qr_out
   fi
  ;;
  # Run terminal commands
 'ter')
  run_term
  ;;
  # PDF
 'pdf')
   if [[ "$launchercontent" == 'dir' ]]; then
     pdf_dir
   elif [[ "$launchercontent" == 'open' ]]; then
     pdf_open
   elif [[ "$launchercontent" == 'print' ]]; then
     pdf_print
   elif [[ "$launchercontent" == 'clip' ]]; then
     pdf_clip
   else
     pdf_out
   fi
  ;;
  # Text to speech
 'text')
   if [[ "$launchercontent" == 'clip' ]]; then
     text_clip
   elif [[ "$launchercontent" == 'stop' ]]; then
     text_stop
   else
     text_out
   fi
  ;;
  # Text to speech
 'speak')
  text_speak
  ;;
  # Sticky note
 'Q')
  yad --notification --image="text-editor" --command "/bin/bash -c show_note"
  ;;
  # Emoji
 'emoji')
  emojilist "$@"
  ;;
  # Zipping files
 'zi')    
   if [[ "$launchercontent" == 'clip' ]]; then
     zi_clip
   elif [[ "$launchercontent" == 'dir' ]]; then
     zi_dir
   elif [[ "$launchercontent" == 'open' ]]; then
     zi_open
   else
     zi_out
   fi
  ;;
  # Date
 'date')
  date_out
  ;;
  # Uptime
 'uptime')
  uptime_out
  ;;
  # Spell checker
 'spell')
  spell_checker
  ;;
  # Check volume
 'vol')
  sendnotifyvol "$@"
  ;;
  # Audacious player
 'au')
   if [[ "$launchercontent" == 'stop' ]]; then
     au_stop
   elif [[ "$launchercontent" == 'next' ]]; then
     au_next
   elif [[ "$launchercontent" == 'prev' ]]; then
     au_prev
   elif [[ "$launchercontent" == 'play-pause' ]]; then
     au_play
   elif [[ "$launchercontent" == 'p1' ]]; then
     au_p1
   elif [[ "$launchercontent" == 'p2' ]]; then
     au_p2
   elif [[ "$launchercontent" == 'p3' ]]; then
     au_p3
   elif [[ "$launchercontent" == 'p4' ]]; then
     au_p4
   elif [[ "$launchercontent" == 'p5' ]]; then
     au_p5
   elif [[ "$launchercontent" == 's0' ]]; then
     au_s0
   elif [[ "$launchercontent" == 's25' ]]; then
     au_s25
   elif [[ "$launchercontent" == 's50' ]]; then
     au_s50
   elif [[ "$launchercontent" == 's75' ]]; then
     au_s75
   elif [[ "$launchercontent" == 'v100' ]]; then
     au_vol_100
   elif [[ "$launchercontent" == 'v50' ]]; then
     au_vol_50
   elif [[ "$launchercontent" == 'kill' ]]; then
     au_kill
   elif [[ "$launchercontent" == 'playlist' ]]; then
     au_playlist
   elif [[ "$launchercontent" == 'status' ]]; then
     au_status
   elif [[ "$launchercontent" == 'repeat' ]]; then
     au_repeat
   elif [[ "$launchercontent" == 'shuffle' ]]; then
     au_shuffle
   elif [[ "$launchercontent" == 'current' ]]; then
     au_current
   elif [[ "$launchercontent" == 'now' ]]; then
     nowplaylist
   else
     :
   fi
  ;;
  # Separate notes
 'no')
   if [[ "$launchercontent" == 'open' ]]; then
     no_open
   elif [[ "$launchercontent" == 'dir' ]]; then
     no_dir
   elif [[ "$launchercontent" == 'copy' ]]; then
     no_copy
   else
     no_out
   fi
  ;;
  # Screenshots
 'ss')
   if [[ "$launchercontent" == 'dir' ]]; then
     ss_dir  
   elif [[ "$launchercontent" == 'view' ]]; then
     ss_view
   elif [[ "$launchercontent" == 'copy' ]]; then
     ss_copy
   elif [[ "$launchercontent" == 'gimp' ]]; then
     ss_gimp
   elif [[ "$launchercontent" == 'open' ]]; then
     ss_open
   else
     :
   fi
  ;;
  # Open last screenshot
 'open')
  ss_open
  ;;
  # Time zones
 'zones')
  time_zones
  ;;
  # Convert files
 'convert')
  convert_files
  ;;
  # Open prefixrofi config
 'config')
  open_config
 ;;
  # Xkill
 'xkill')
  xkill_process
  ;;
  # Password generator
 'pass')
  pass_gen
  ;;
  # Local dictionary
 'dict')
  dict_local
  ;;
  # Empty trash
 'empty')
  trash_clear
  ;;
  # Quick links
 'ql')
  if [[ "$launchercontent" == 'links' ]]; then
    quick_links
  elif [[ "$launchercontent" == 'info' ]]; then
    ql_info
  else
    ql_out
  fi
  ;;
  # Snippets
 'sni')
  if [[ "$launchercontent" == 'view' ]]; then
    view_snips
  elif [[ "$launchercontent" == 'txt' ]]; then
    sni_txt
  else
    create_snip
  fi
  ;;
  # Paste snippet to clipboard
 'sn')
  open_snip
  ;;
  # Misc
 'misc')
  notify-send "No app set in johns config"
  ;;
  # Close app
 'close')
  close_app
  ;;
  
  *)
  # Opens web link in browser for quicklinks root search
  if [[ "$bang" == 'link' ]]; then
    root_link
  # Open local file for quicklinks root search
  elif [[ "$bang" == 'file' ]]; then
    root_file
  # Query search within web page for quicklinks root search
  elif [[ "$bang" == 'query' ]]; then
    root_query
  # Open paths to directories for quicklinks root search
  elif [[ "$bang" == 'path' ]]; then
    root_path
  # Window switcher
  elif [[ "$(pidof "$bang")" || "$(pgrep -f "$bang" > /dev/null)" ]]; then
    app_running
  else
  # Open app
    new_instance
  fi
  ;;

esac

#-------------------------------------------------------#
# Unset
#-------------------------------------------------------#

unset show_note
unset note_count
unset clear_note
unset start
unset end
unset file
unset choice
unset title
unset notes
unset dates
unset priority
unset text_file
unset color_field
unset color_field2
unset color_field3
unset color_field4
unset color_field5
