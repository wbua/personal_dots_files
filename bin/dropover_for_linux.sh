#!/bin/bash

# Program command: dropover_for_linux.sh
# Description: Drag and drop files into program and perform various actions.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 25-12-2024
# Program_license: GPL 3.0
# Dependencies: yad, swaymsg, jq, thunar

# Master config file
source "$HOME"/bin/sway_user_preferences.sh

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Ensure the Documents directory exists
if [ ! -d "$HOME"/Documents ]; then
  mkdir "$HOME"/Documents
fi

# Ensure the dropover_list.txt file exists
if [ ! -f "$HOME"/Documents/dropover_list.txt ]; then
  touch "$HOME"/Documents/dropover_list.txt
fi

# Colors

# Button icons
color01="<span color='#FFFFFF' font_family='JetBrainsMono Nerd Font' font='24' rise='0pt'>"
export color01 
# Color end
color_end="</span>"
export color_end

# Remove any empty lines from the dropover_list.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/dropover_list.txt

# Copy files into chosen directory
copy_files_from_list() {
  # Get the destination directory from Thunar (user-selected directory)
  choice_home=$(swaymsg -t get_tree | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.name)"' | grep -i "thunar" | sed 's|- Thunar||' | sed 's/[[:space:]]*$//')

  # Ensure the destination directory exists
  if [ ! -d "$choice_home" ]; then
    echo "Error: Destination directory '$choice_home' does not exist."
    exit 1
  fi

  # Debug: Show the destination directory
  echo "Destination directory: $choice_home"

  # Copy files from the dropover_list.txt
  while IFS= read -r file || [ -n "$file" ]; do
    # Ensure both source file and destination are quoted correctly
    if [[ -f "$file" ]]; then
      # Ensure the file isn't already in the destination directory
      destination_file="$choice_home/$(basename "$file")"
      if [[ "$file" != "$destination_file" ]]; then
        # Debug: Show the file being copied
        echo "Copying: $file"
        cp -v "$file" "$choice_home"  # Use -v for verbose output
        if [ $? -ne 0 ]; then
          echo "Failed to copy: $file"
        fi
      else
        echo "Skipping copy of '$file', already in the destination."
      fi
    else
      echo "Warning: '$file' is not a valid file."
    fi
  done < "$HOME"/Documents/dropover_list.txt  # Read from dropover_list.txt
}

# Move files into chosen directory
move_files_from_list() {
  # Get the destination directory from Thunar (user-selected directory)
  choice_home=$(swaymsg -t get_tree | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.name)"' | grep -i "thunar" | sed 's|- Thunar||' | sed 's/[[:space:]]*$//')

  # Ensure the destination directory exists
  if [ ! -d "$choice_home" ]; then
    echo "Error: Destination directory '$choice_home' does not exist."
    exit 1
  fi

  # Debug: Show the destination directory
  echo "Destination directory: $choice_home"

  # Move files from the dropover_list.txt
  while IFS= read -r file || [ -n "$file" ]; do
    # Ensure both source file and destination are quoted correctly
    if [[ -f "$file" ]]; then
      # Debug: Show the file being moved
      echo "Moving: $file"
      mv "$file" "$choice_home"  # Use mv to move files
      if [ $? -ne 0 ]; then
        echo "Failed to move: $file"
      fi
    else
      echo "Warning: '$file' is not a valid file."
    fi
  done < "$HOME"/Documents/dropover_list.txt  # Read from dropover_list.txt
}

# Copy the files again (new independent function)
copy_again() {
  # Get the destination directory from Thunar (user-selected directory)
  choice_home=$(swaymsg -t get_tree | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.name)"' | grep -i "thunar" | sed 's|- Thunar||' | sed 's/[[:space:]]*$//')

  # Ensure the destination directory exists
  if [ ! -d "$choice_home" ]; then
    echo "Error: Destination directory '$choice_home' does not exist."
    exit 1
  fi

  # Copy files again from dropover_list.txt
  while IFS= read -r file || [ -n "$file" ]; do
    # Ensure both source file and destination are quoted correctly
    if [[ -f "$file" ]]; then
      cp "$file" "$choice_home"
      if [ $? -ne 0 ]; then
        echo "Failed to copy: $file"
      fi
    else
      echo "Warning: '$file' is not a valid file."
    fi
  done < "$HOME"/Documents/dropover_list.txt  # Re-read from dropover_list.txt
}

save_to_list() {
  local file_path=$1

  # Debug: Show the file path being passed to the function
  #echo "save_to_list called with file path: '$file_path'"

  # Remove the "file://" prefix if it exists
  if [[ "$file_path" == file://* ]]; then
    file_path="${file_path#file://}"
    echo "Cleaned file path (after removing 'file://'): $file_path"
  fi

  # Check if the file path is empty
  if [ -z "$file_path" ]; then
    echo "Error: File path is empty. Exiting save_to_list."
    return 1
  fi

  # Check if file_path already exists in dropover_list.txt
  if ! grep -qF "$file_path" "$HOME"/Documents/dropover_list.txt; then
    # Append the new file path to dropover_list.txt
    echo "$file_path" >> "$HOME"/Documents/dropover_list.txt
    #echo "File path '$file_path' added to dropover_list.txt"
    # Debug: Show the list before sorting
    #echo "Before sorting: "
    #cat "$HOME"/Documents/dropover_list.txt

    # Sort dropover_list.txt to maintain file order and remove duplicates
    sort -u "$HOME"/Documents/dropover_list.txt -o "$HOME"/Documents/dropover_list.txt
    #echo "dropover_list.txt sorted and duplicates removed."

    # Debug: Show the list after sorting
    #echo "After sorting: "
    #cat "$HOME"/Documents/dropover_list.txt

    # Send notification
    notify-send "File saved to list: $file_path"
  else
    echo "File path '$file_path' already exists in dropover_list.txt"
  fi
}

export -f save_to_list

export -f copy_files_from_list
export -f move_files_from_list
export -f copy_again

# Open images files
open_image_types() {

killall yad

# Initialize an empty variable to store image file paths
image_files=""

# Loop through each file in my_list.txt
while IFS= read -r filepath; do
  # Skip empty lines in the file
  if [[ -z "$filepath" ]]; then
    continue
  fi

  # Check if the file exists and is a regular file (not a directory)
  if [[ -f "$filepath" ]]; then
    # Get the file type using the file command and shorten the output
    filetype=$(file -b "$filepath" | awk -F', ' '{print $1}')
    
    # Check if the file is an image (make sure it's not a directory or non-image file)
    if [[ "$filetype" == *image* || "$filetype" == *bitmap* || "$filetype" == *raster* || "$filetype" == *little-endian* ]]; then
      # Append the file path to the image_files variable
      image_files+="$filepath"$'\n'
    else
      echo "$filepath is not an image, skipping..."
    fi
  else
    echo "$filepath does not exist or is not a regular file."
  fi
done < ~/Documents/dropover_list.txt

# Output the list of image files and open them in GIMP
if [[ -n "$image_files" ]]; then
  #echo "Opening the following image files in GIMP:"
  echo "$image_files"

  # Open each image file in GIMP, properly handling spaces in the file paths
  while IFS= read -r image; do
    # Skip empty lines (just in case)
    if [[ -z "$image" ]]; then
      continue
    fi

    # Debugging: Print file type for each image before opening
    filetype=$(file -b "$image")
    #echo "File type for $image: $filetype"

    # Ensure the file path is quoted when passed to the gimp command
    # Improved check for valid image files
    if [[ "$filetype" == *image* || "$filetype" == *bitmap* || "$filetype" == *raster* || "$filetype" == *little-endian* ]]; then
      # Open the image in GIMP
      win_class=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | \
      tr -d '"' | tr -d '"' | tr '[:upper:]' '[:lower:]')
      "$win_class" "$image" >/dev/null 2>&1 &
    else
      echo "$image is not a valid image file, skipping."
    fi
  done <<< "$image_files"
else
  echo "No valid image files found."
fi
	
}
export -f open_image_types

# Open video files
open_video_file() {

killall yad

# Initialize an empty variable to store video file paths
video_files=""

# Loop through each file in my_list.txt
while IFS= read -r filepath; do
  # Skip empty lines in the file
  if [[ -z "$filepath" ]]; then
    continue
  fi

  # Check if the file exists and is a regular file (not a directory)
  if [[ -f "$filepath" ]]; then
    # Get the file type using the file command and shorten the output
    filetype=$(file -b "$filepath" | awk -F', ' '{print $1}')
    
    # Check if the file is a video (make sure it's not a directory or non-video file)
    if [[ "$filetype" == *video* || "$filetype" == *MPEG* || "$filetype" == *AVI* || "$filetype" == *MP4* || "$filetype" == *ISO* || "$filetype" == *WebM* || "$filetype" == *quicktime* || "$filetype" == *Matroska* ]]; then
      # Append the file path to the video_files variable
      video_files+="$filepath"$'\n'
    else
      echo "$filepath is not a video, skipping..."
    fi
  else
    echo "$filepath does not exist or is not a regular file."
  fi
done < ~/Documents/dropover_list.txt

# Output the list of video files and open them
if [[ -n "$video_files" ]]; then
  #echo "Opening the following video files:"
  #echo "$video_files"

  # Open each video file
  while IFS= read -r video; do
    # Skip empty lines (just in case)
    if [[ -z "$video" ]]; then
      continue
    fi

    # Debugging: Print file type for each video before opening
    filetype=$(file -b "$video")
    #echo "File type for $video: $filetype"

    # Ensure the file path is quoted when passed to the video player
    # Improved check for valid video files
    if [[ "$filetype" == *video* || "$filetype" == *MPEG* || "$filetype" == *AVI* || "$filetype" == *MP4* || "$filetype" == *WebM* || "$filetype" == *ISO* || "$filetype" == *quicktime* || "$filetype" == *Matroska* ]]; then
      # Open the video file with your preferred video player (e.g., VLC, mpv)
      vid_class=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | \
       tr -d '"' | tr -d '"' | tr '[:upper:]' '[:lower:]')
       "$vid_class" "$video" >/dev/null 2>&1 &  # Replace mpv with your video player
    else
      echo "$video is not a valid video file, skipping."
    fi
  done <<< "$video_files"
else
  echo "No valid video files found."
fi
	
}
export -f open_video_file

# Open sound files
open_sound_files() {

killall yad

# Initialize an empty variable to store sound file paths
sound_files=""

# Loop through each file in my_list.txt
while IFS= read -r filepath; do
  # Skip empty lines in the file
  if [[ -z "$filepath" ]]; then
    continue
  fi

  # Check if the file exists and is a regular file (not a directory)
  if [[ -f "$filepath" ]]; then
    # Get the file type using the file command and shorten the output
    filetype=$(file -b "$filepath" | awk -F', ' '{print $1}')
    
    # Check if the file is a sound file (make sure it's not a directory or non-sound file)
    if [[ "$filetype" == *audio* || "$filetype" == *RIFF* || "$filetype" == *MP3* || "$filetype" == *WAV* || "$filetype" == *FLAC* || "$filetype" == *AAC* || "$filetype" == *MIDI* ]]; then
      # Append the file path to the sound_files variable
      sound_files+="$filepath"$'\n'
    else
      echo "$filepath is not a sound file, skipping..."
    fi
  else
    echo "$filepath does not exist or is not a regular file."
  fi
done < ~/Documents/dropover_list.txt

# Output the list of sound files and play them
if [[ -n "$sound_files" ]]; then
  #echo "Playing the following sound files:"
  echo "$sound_files"

  # Play each sound file
  while IFS= read -r sound; do
    # Skip empty lines (just in case)
    if [[ -z "$sound" ]]; then
      continue
    fi

    # Debugging: Print file type for each sound before playing
    filetype=$(file -b "$sound")
    #echo "File type for $sound: $filetype"

    # Ensure the file path is quoted when passed to the audio player
    # Improved check for valid sound files
    if [[ "$filetype" == *audio* || "$filetype" == *MP3* || "$filetype" == *RIFF* || "$filetype" == *WAV* || "$filetype" == *FLAC* || "$filetype" == *AAC* || "$filetype" == *MIDI* ]]; then
      # Play the sound file with your preferred audio player
       snd_class=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | \
       tr -d '"' | tr -d '"' | tr '[:upper:]' '[:lower:]')
       "$snd_class" "$sound" >/dev/null 2>&1 &
    else
      echo "$sound is not a valid sound file, skipping."
    fi
  done <<< "$sound_files"
else
  echo "No valid sound files found."
fi
	
}
export -f open_sound_files

# Open document files
open_document_types() {

killall yad

# Initialize an empty variable to store document file paths
document_files=""

# Loop through each file in dropover_list.txt
while IFS= read -r filepath; do
  # Skip empty lines in the file
  if [[ -z "$filepath" ]]; then
    continue
  fi

  # Check if the file exists and is a regular file (not a directory)
  if [[ -f "$filepath" ]]; then
    # Get the file extension to determine the document type
    file_extension="${filepath##*.}"
    
    # Check if the file is one of the supported document types
    case "$file_extension" in
      odt)
        document_files+="$filepath"$'\n'
        ;;
      ods)
        document_files+="$filepath"$'\n'
        ;;
      odp)
        document_files+="$filepath"$'\n'
        ;;
      txt)
        document_files+="$filepath"$'\n'
        ;;
      pdf)
        document_files+="$filepath"$'\n'
        ;;
      docx)
        document_files+="$filepath"$'\n'
        ;;
      doc)
        document_files+="$filepath"$'\n'
        ;;
      html)
        document_files+="$filepath"$'\n'
        ;;
      csv)
        document_files+="$filepath"$'\n'
        ;;
      *)
        echo "$filepath is not a supported document type, skipping..."
        ;;
    esac
  else
    echo "$filepath does not exist or is not a regular file."
  fi
done < ~/Documents/dropover_list.txt

# Output the list of document files and open them in LibreOffice
if [[ -n "$document_files" ]]; then
  echo "Opening the following document files:"

  # Open each document file in the appropriate LibreOffice application
  while IFS= read -r document; do
    # Skip empty lines (just in case)
    if [[ -z "$document" ]]; then
      continue
    fi

    # Get the file extension to determine the LibreOffice application to use
    file_extension="${document##*.}"

    # Determine the LibreOffice application based on file extension
    case "$file_extension" in
      odt)
        libreoffice_cmd="libreoffice.writer"
        ;;
      ods)
        libreoffice_cmd="libreoffice.calc"
        ;;
      odp)
        libreoffice_cmd="libreoffice.impress"
        ;;
      txt | pdf | docx | doc | html | csv)
        app_class=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | \
        tr -d '"' | tr -d '"' | tr '[:upper:]' '[:lower:]')
        libreoffice_cmd="libreoffice.writer"
        ;; # Default to writer for text-based formats
      *)
        echo "$document is not a recognized document type, skipping."
        continue
        ;;
    esac

    # Ensure the LibreOffice command uses the correct app name
    if [[ "$libreoffice_cmd" == "libreoffice.writer" ]]; then
      # Open document with LibreOffice Writer
      "$libreoffice_cmd" "$document" >/dev/null 2>&1 &
    elif [[ "$libreoffice_cmd" == "libreoffice.calc" ]]; then
      # Open document with LibreOffice Calc
      "$libreoffice_cmd" "$document" >/dev/null 2>&1 &
    elif [[ "$libreoffice_cmd" == "libreoffice.impress" ]]; then
      # Open document with LibreOffice Impress
      "$libreoffice_cmd" "$document" >/dev/null 2>&1 &
    fi
  done <<< "$document_files"
else
  echo "No valid document files found."
fi
	
}
export -f open_document_types

# Delete contains of text file
delete_contains() {

truncate -s 0 ~/Documents/dropover_list.txt || exit 1

}
export -f delete_contains

# Zip all files in list
Zip_all_files() {

# Name of the text file that contains the list of files to zip
filelist="$HOME/Documents/dropover_list.txt"

# Specify the directory where you want to save the zip file
output_dir="$HOME/Documents"

# Generate output zip filename with current date and time, and save it to the specific directory
output_zip="$output_dir/backup-$(date '+%Y-%m-%d-%H-%M-%S').zip"

# Initialize an empty string for the files to include in the zip
file_list=""

# Read the file paths from the text file and accumulate them
while IFS= read -r line; do
    # Check if the file exists before adding it to the list
    if [[ -e "$line" ]]; then
        file_list+="$line "
    else
        echo "Warning: File not found - $line"
    fi
done < "$filelist"

# If no files were added, exit with a message
if [[ -z "$file_list" ]]; then
    echo "No valid files to zip. Exiting."
    exit 1
fi

# Create the zip file with the valid files
zip -j "$output_zip" $file_list > /dev/null

notify-send "Files have been zipped"
	
}
export -f Zip_all_files

# Drag files out of yad into program.
drag_files_out() {

killall yad

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Documents/.dofl_temp_dir ]; then
     :
   else
     mkdir "$HOME"/Documents/.dofl_temp_dir
   fi

# Create a temporary directory
temp_dir="$HOME/Documents/.dofl_temp_dir/"

# Copy files listed in dropover_list.txt to the temporary directory
while IFS= read -r file; do
    cp "$file" "$temp_dir"
done < ~/Documents/dropover_list.txt

# Open the yad file chooser dialog pointing to the temporary directory
GTK_THEME="$MY_GTK_THEME" yad --file --multiple --title="Select Files" \
 --width=700 --height=600 \
 --no-escape --add-preview \
 --text="Choose files" --filename="$temp_dir" --button="Exit":1 > /dev/null

# Delete contents of directory
rm -f ~/Documents/.dofl_temp_dir/*.* || exit 1
	
}
export -f drag_files_out

# More yad options
yad_functions() {

killall yad
GTK_THEME="$MY_GTK_THEME" yad --form --width=300 --height=300 \
    --title="DropOver for linux" \
    --columns=4 \
    --text-align="center" \
    --text="\nOptions\n" \
    --field="<b>${color01}${color_end}</b>":fbtn '/bin/bash -c "open_image_types"' \
    --field="<b>${color01}${color_end}</b>":fbtn '/bin/bash -c "open_video_file"' \
    --field="<b>${color01}${color_end}</b>":fbtn '/bin/bash -c "open_sound_files"' \
    --field="<b>${color01}󰈙${color_end}</b>":fbtn '/bin/bash -c "open_document_types"' \
    --field="<b>${color01}󰇾${color_end}</b>":fbtn '/bin/bash -c "delete_contains"' \
    --field="<b>${color01}${color_end}</b>":fbtn '/bin/bash -c "copy_again"' \
    --field="<b>${color01}${color_end}</b>":fbtn '/bin/bash -c "Zip_all_files"' \
    --button="${color01}${color_end}":1

}
export -f yad_functions

# Run yad and capture the output
yad_drop=$(GTK_THEME="$MY_GTK_THEME" yad --dnd --width=200 --height=300 \
    --title="DropOver for linux" \
    --text-align="center" \
    --text="\nDrag files here\nFiles: $(wc -l < ~/Documents/dropover_list.txt | awk '{print $1}')\n" \
    --button="${color01}${color_end}:/bin/bash -c 'yad_functions'" \
    --button="${color01}${color_end}:/bin/bash -c 'drag_files_out'" \
    --button="${color01}${color_end}:1" \
    --button="${color01}󰆏${color_end}:0" \
    --button="${color01}󰪹${color_end}:2" \
    --button="${color01}${color_end}:3")

# Capture the exit code from yad
exit_code=$?

# Print the exit code
echo "$exit_code"

# Exit if no files were dropped
if [[ -z "$yad_drop" ]]; then
  exit 0
fi

# Append the dropped files to dropover_list.txt
echo "$yad_drop" | sed 's|file://||' | sort -u >> "$HOME"/Documents/dropover_list.txt

# Ensure no duplicates are present in the list (optional)
sort -u -o "$HOME"/Documents/dropover_list.txt "$HOME"/Documents/dropover_list.txt

# Determine which button was pressed and take action
case "$exit_code" in
  0)
    # User pressed Copy
    copy_files_from_list "$HOME"/Documents/dropover_list.txt
    ;;
  1)
    # User pressed Save (Just add to list, no copying)
    save_to_list "$yad_drop"
    ;;
  2)
    # User pressed Move
    move_files_from_list "$HOME"/Documents/dropover_list.txt
    ;;
  3)
    # User pressed Exit
    exit 0
    ;;
  *)
    echo "Unknown action triggered."
    ;;
esac

# Unset variables and functions
unset yad_drop
unset copy_files_from_list
unset move_files_from_list
unset save_to_list
unset copy_again
unset MY_GTK_THEME
unset delete_contains
unset color01
unset color_end
unset open_image_types
unset open_document_types
unset yad_functions
unset open_video_file
unset open_sound_files
unset drag_files_out
unset Zip_all_files
