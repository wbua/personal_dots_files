#!/bin/bash

#-----------------------------------------------------------------#
# Error checking
#-----------------------------------------------------------------#

set -e

#-----------------------------------------------------------------#
# Software preferences
#-----------------------------------------------------------------#

# Browser
MYBROWSER="brave"

#-----------------------------------------------------------------#
# Apps text file
#-----------------------------------------------------------------#

app_text=$(cat /home/john/Documents/apps.txt)

#-----------------------------------------------------------------#
# Prefixes text file
#-----------------------------------------------------------------#

prefixes_text=$(cat /home/john/Documents/prefixes.txt )

#-----------------------------------------------------------------#
# Main
#-----------------------------------------------------------------#
 
# Run launcher 
selected=$(echo "$app_text" | sort | rofi -dmenu -p '' -matching prefix)
 
# Keyword
bang=$(echo "$selected" | awk '{print $1}')

# Content
launcherContent=$(echo "$selected" | cut -d' ' -f2-)
 
case "$bang" in

   yt)
     yt-dlp $launcherContent
     ;;
   t)
     /usr/bin/some-translate-program $launcherContent
     ;;

   g)
   web_search="https://www.google.co.uk/search?q={}"
   echo "$launcherContent" | xargs -I{} "$MYBROWSER" "$web_search"
   ;;
   w)
     /home/user/.bin/search-wikipedia.sh $launcherContent
     ;;
   n) 
    notify-send "$launcherContent"
    ;;
    
   *)
    if [[ "$bang" == "g " ]]; then
      exit 1
    else
      /bin/bash -c "$bang" || exit 1
    fi 
     #echo "This is not a recognized service" | rofi -dmenu 
     ;;
     
esac
