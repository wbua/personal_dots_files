#!/bin/bash

# Program command: video_cutter.sh
# Description: Cuts section of video using ffmpeg in gui.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 04-07-2024
# Program_license: GPL 3.0
# Dependencies: yad, ffmpeg, Material icons, Cascadia Mono

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#--------------------------------------------------#
# Error checking
#--------------------------------------------------#

set -e

#--------------------------------------------------#
# User preferences
#--------------------------------------------------#

# Video player
video_player="$MAIN_VIDEO_PLAYER"
export video_player

# Video format
video_format="mp4"
export video_format

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#--------------------------------------------------#
# Create files and directories
#--------------------------------------------------#

# Create video directory
   if [ -d "$HOME"/Videos ]; then
     :
   else
     mkdir "$HOME"/Videos
   fi
   
# Temp video directory
   if [ -d "$HOME"/Videos/.temp_cut_video ]; then
     :
   else
     mkdir "$HOME"/Videos/.temp_cut_video
   fi
   
#--------------------------------------------------#
# Checks if wayland is present
#--------------------------------------------------#

   if [[ "$(echo "$WAYLAND_DISPLAY")" == "" ]]; then
     yad --error \
     --width 500 \
     --title "Wayland session not found!" \
     --text "Video cutter error, wayland only tool!"
     exit 1
   else
     :
   fi

#--------------------------------------------------#
# Colors
#--------------------------------------------------#

# Descriptions
color1="<span color='#D1D1D1' font_family='Cascadia Mono' font='16' rise='0pt'>"
export color1
# Time
color2="<span color='#27FF00' font_family='Cascadia Mono' font='16' rise='0pt'>"
export color2 
# Buttons
color3="<span color='#FFFFFF' font_family='Material icons' font='20' rise='0pt'>"
export color3
# Close button
color4="<span color='#FF0022' font_family='Material icons' font='20' rise='0pt'>"
export color4
# Changing preferences
color5="<span color='#ECFF00' font_family='Cascadia Mono' font='16' rise='0pt'>"
export color5
# Color end
end="</span>"
export end

#--------------------------------------------------#
# Change preferences
#--------------------------------------------------#

# Video format
change_video_format() {

format_example=$(echo "${color1}Video format example:${end} ${color2}webm${end} 
${color1}no dot before type${end}")
killall yad
sed -i "30s|^.*$|video_format=\"$(GTK_THEME="$MY_GTK_THEME" yad --entry --center \
--width=500 --height=200 \
--text="$format_example")\"|" ~/bin/video_cutter.sh
	
}
export -f change_video_format

#--------------------------------------------------#
# Yad settings
#--------------------------------------------------#

choice=$(GTK_THEME="$MY_GTK_THEME" yad --form --width=700 --height=400 --title="Video cutter" \
--borders=20 \
--text-align="center" \
--buttons-layout="center" \
--align="center" \
--text="${color1}Start:${end} ${color2}00:00${end} | ${color1}End:${end} ${color2}00:00${end}
${color1}Input:${end} Select file | ${color1}Output: Select path${end}\n" \
--separator="#" \
--field="Start ":CE \
--field="End ":CE \
--field="Input ":FL \
--field="Output ":DIR \
--button="${color1}File type${end}:/bin/bash -c 'change_video_format'" \
--button="${color3}${end}":0 \
--button="${color4}${end}":1)
export choice
[[ -z "$choice" ]] && exit

#--------------------------------------------------#
# Input fields
#--------------------------------------------------#

start=$(echo "$choice" | awk -F '#' '{print $1}')
export start
end=$(echo "$choice" | awk -F '#' '{print $2}')
export end
file=$(echo "$choice" | awk -F '#' '{print $3}')
export file
vid_dir=$(echo "$choice" | awk -F '#' '{print $4}')
export vid_dir

#--------------------------------------------------#
# Creates video
#--------------------------------------------------#

rm -f ~/Videos/.temp_cut_video/* || exit 1
ffmpeg -ss 00:"$start" -to 00:"$end" -i "$file" -c copy "$vid_dir"/"$(date '+%d-%m-%Y-%H-%M-%S')"."$video_format" && notify-send "Video had been cut"
cp ~/Videos/.temp_cut_video/* ~/Videos/

#--------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------#

unset start
unset end
unset file
unset video_player
unset choice
unset color1
unset color2
unset color3
unset color4
unset color4
unset end
unset video_format
unset change_video_format
unset vid_dir
