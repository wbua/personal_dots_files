#!/bin/bash

# Title text
TITLE_PANGO_TEXT_RISE="3pt"

# Chatbot
CHATBOT_PANGO_ICON_RISE="-5pt"

# Color picker
COLOR_PICKER_PANGO_ICON_RISE="-5pt"

# Apps
APPS_PANGO_ICON_RISE="-5pt"

# Search
SEARCH_PANGO_ICON_RISE="-5pt"

# Current workspace
CURRENT_WORKSPACE_PANGO_TEXT_RISE="1pt"

# Brightness icon
BRIGHT_PANGO_ICON_RISE="-4pt"

# Brightness text
BRIGHT_PANGO_TEXT_RISE="1pt"

# Volume icon
VOLUME_PANGO_ICON_RISE="-5pt"
# Volume text
VOLUME_PANGO_TEXT_RISE="1pt"

# Microphone icon
MICROPHONE_PANGO_ICON_RISE="-4pt"
# Microphone text
MICROPHONE_PANGO_TEXT_RISE="1pt"

# Battery icon
BATTERY_PANGO_ICON_RISE="-4pt"
# Battery text
BATTERY_PANGO_TEXT_RISE="1pt"

# Date icon
DATE_PANGO_ICON_RISE="-4pt"
# Date text
DATE_PANGO_TEXT_RISE="1pt"

# Weather text
WEATHER_PANGO_TEXT_RISE="1pt"

# Night light
NIGHT_LIGHT_PANGO_ICON_RISE="-3pt"

# Quick player text
QUICK_PLAYER_PANGO_TEXT_RISE="-4pt"

# Notification text
NOTIFICATIONS_PANGO_TEXT_RISE="2pt"
# Notification icon
NOTIFICATIONS_PANGO_ICON_RISE="-3pt"

# Keys icon
KEYS_PANGO_ICON_RISE="-4pt"

# Power menu icon
POWER_MENU_PANGO_ICON_RISE="-4pt"

# Layout icon
LAYOUT_PANGO_ICON_RISE="-5pt"

# Keyboard layout
KEYBOARD_LAYOUT_PANGO_TEXT_RISE="1pt"

# Normal half circle
HALF_CHARACTER_NORMAL="0pt"

# Invert text color
INVERT_TEXT_COLOR_RISE="3pt"

# Inverted no background text
INVERTED_NO_TEXT_RISE="2pt"

# Inverted font size
INVERT_FONT_SIZE="14.5"

# Half character inverted background
HALF_INVERTED_CHARACTER_SIZE="22"

# inverted rise half character
HALF_RISE_INVERTED="0pt"

# Inerted white text size
INVERTED_WHITE_TEXT_SIZE="14.5"

# Inverted white rise (title)
WHITE_RISE_TEXT="2pt"

# Settings no background rise
SETTINGS_NO_BACKGROUND="-4pt"
