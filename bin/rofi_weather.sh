#!/bin/bash

# Program command: rofi_weather.sh
# Description: Show weather information.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 06-10-2023
# Program_license: GPL 3.0
# Dependencies: rofi, wget

#----------------------------------------------------------------#
# Error checking
#----------------------------------------------------------------#

set -euo pipefail

#----------------------------------------------------------------#
# Create file and directories
#----------------------------------------------------------------#
 
   if [ -d "$HOME"/Downloads ]; then
     :
   else
     mkdir "$HOME"/Downloads
   fi
 
   if [ -d "$HOME"/Downloads/.temp_weather ]; then
     :
   else
     mkdir "$HOME"/Downloads/.temp_weather
   fi  
   
   if [ -f "$HOME"/Downloads/.weather_api_key.txt ]; then
     :
   else
     touch "$HOME"/Downloads/.weather_api_key.txt
     echo "2643743" | tee "$HOME"/Downloads/.weather_api_key.txt
   fi 

#----------------------------------------------------------------#
# Colors
#----------------------------------------------------------------#

color10="<span color='#0EFF00' font_family='Inter' font='18' rise='0pt'>"
end="</span>"

#----------------------------------------------------------------#
# Weather api key
#----------------------------------------------------------------#

# To get api key visit https://www.bbc.co.uk/weather
# Example api key 2643743
weather_api_key=$(cat "$HOME"/Downloads/.weather_api_key.txt)

#----------------------------------------------------------------#
# Check if weather directory is empty
#----------------------------------------------------------------#	
	
   if [[ -z "$(ls -A "$HOME"/Downloads/.temp_weather)" ]]; then
     rm -rf "$HOME"/Downloads/.temp_weather/*
     wget https://weather-broker-cdn.api.bbci.co.uk/en/forecast/rss/3day/"$weather_api_key" -O "$HOME"/Downloads/.temp_weather/downloadedfile
     notify-send "Weather updated"
   else
     :
   fi
     
#----------------------------------------------------------------#
# 3-day forecast for city
#----------------------------------------------------------------#

forecast_area=$(grep "3-day forecast for" "$HOME"/Downloads/.temp_weather/downloadedfile | \
sed -e 's|<description>||' -e 's|</description>||' | \
sed 's|from BBC Weather, including weather, temperature and wind information||')

#----------------------------------------------------------------#
# Run launcher
#----------------------------------------------------------------#

_rofi() {
	
rofi -dmenu -i -p '' -markup-rows -mesg "${color10}${forecast_area}${end}$(weather_info)" \
-theme-str 'window {width: 70%;}' \
-theme-str 'entry {placeholder : "Enter 7 digit number...";}' \
-theme-str 'textbox-custom {str: "after entering 7 digit number press update weather";}' \
-theme-str 'inputbar {children:   [ prompt,textbox-prompt-colon,entry,textbox-custom ];}'
	
}
	
#----------------------------------------------------------------#
# Update and download weather data
#----------------------------------------------------------------#

weather_download() {

rm -rf "$HOME"/Downloads/.temp_weather/*
wget https://weather-broker-cdn.api.bbci.co.uk/en/forecast/rss/3day/"$weather_api_key" -O "$HOME"/Downloads/.temp_weather/downloadedfile
notify-send	"Weather updated"	
rofi_weather.sh	
	
}

#----------------------------------------------------------------#
# Menu
#----------------------------------------------------------------#

menu() {
	
echo "update weather"	
		
}

#----------------------------------------------------------------#
# Parse weather data
#----------------------------------------------------------------#

weather_info() {
	
grep Today: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'		
grep Monday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'
grep Tuesday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'
grep Wednesday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'
grep Thursday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'
grep Friday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'
grep Saturday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'
grep Sunday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'

}	

#----------------------------------------------------------------#
# Main
#----------------------------------------------------------------#

main() {
	
choice=$(menu | _rofi)

case "$choice" in

 'update weather')
   weather_download
  ;;
  
  *)
  echo "$choice" | tee "$HOME"/Downloads/.weather_api_key.txt || exit
  rofi_weather.sh
  ;;
  
esac

}
main

