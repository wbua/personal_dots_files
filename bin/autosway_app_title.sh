#!/bin/bash

# Sway user preferences 
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

ATM_MOD="$APP_ARTIST_TITLE_MOD"

   if [ -f "$HOME"/Documents/.switch_mode_i3blocks.txt ]; then
     :
   else
     touch "$HOME"/Documents/.switch_mode_i3blocks.txt
   fi

echo "$ATM_MOD" > "$HOME"/Documents/.switch_mode_i3blocks.txt   
