#!/bin/bash

# Program command: fzf-notes-wayland.sh
# Description: Open and create oneline notes.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 03-12-2023
# Program_license: GPL 3.0
# Dependencies: fzf

#---------------------------------------------#
# Software preferences
#---------------------------------------------#

text_editor="geany"

#---------------------------------------------#
# Colors
#---------------------------------------------#

# Descriptions
color1=$(tput setaf 244)
# Color end
end=$(tput sgr0)

#---------------------------------------------#
# Create files and directories
#---------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ -f "$HOME"/Documents/mainprefix/quicknotes.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/quicknotes.txt
   fi

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/.temqrcodes ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes
   fi

   if [ -d "$HOME"/Pictures/qr_codes ]; then
     :
   else
     mkdir "$HOME"/Pictures/qr_codes
   fi   

#---------------------------------------------#
# Open online notes text file
#---------------------------------------------#

open_note_file() {

"$text_editor" "$HOME"/Documents/mainprefix/quicknotes.txt
fzf-launcher-wayland.sh
	
}

#---------------------------------------------#
# Add note
#---------------------------------------------#

add_note() {

sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/quicknotes.txt
choice=$(echo "" | fzf --print-query \
--info=inline \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--reverse \
--header='
Press F1 for main menu

' \
--prompt 'add note> ' \
--bind='F1:execute(fzf-launcher-wayland.sh {})')	
[ -z "$choice" ] && exit 0
echo "$choice" >> "$HOME"/Documents/mainprefix/quicknotes.txt
fzf-launcher-wayland.sh
	
}

#---------------------------------------------#
# Create qr code form note
#---------------------------------------------#     

create_qr_code() {

launcher=$(tac ~/Documents/mainprefix/quicknotes.txt | fzf --reverse \
--cycle \
--info=inline \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--reverse \
--header='
Press F1 for main menu

' \
--prompt 'create qr code> ' \
--bind='F1:execute(fzf-launcher-wayland.sh {})')
[ -z "$launcher" ] && exit 0
rm -f "$HOME"/Pictures/.temqrcodes/*.png
echo "$launcher" | tr -d '\n' | qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
fzf-launcher-wayland.sh
	
}

#---------------------------------------------#
# Speak note
#---------------------------------------------#      

speak_note() {

launcher=$(tac ~/Documents/mainprefix/quicknotes.txt | fzf --reverse \
--cycle \
--info=inline \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--reverse \
--header='
Press F1 for main menu

' \
--prompt 'speak note> ' \
--bind='F1:execute(fzf-launcher-wayland.sh {})')
[ -z "$launcher" ] && exit 0
setsid echo -n "$launcher" | xargs -d '\n' | festival --tts >/dev/null 2>&1 & disown
fzf-launcher-wayland.sh
	
}     

#---------------------------------------------#
# Send note to clipboard
#---------------------------------------------#

clip_note() {

launcher=$(tac ~/Documents/mainprefix/quicknotes.txt | fzf --reverse \
--cycle \
--info=inline \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--reverse \
--header='
Press F1 for main menu

' \
--prompt 'clipboard> ' \
--bind='F1:execute(fzf-launcher-wayland.sh {})')
[ -z "$launcher" ] && exit 0
printf '%s' "$launcher" | xclip -selection clipboard
fzf-launcher-wayland.sh
	
}  
     
#---------------------------------------------#
# Menu
#---------------------------------------------#

menu() {

echo "add ${color1} add note to text file${end}"
echo "create ${color1} create qr code from note${end}"
echo "clip ${color1} send note to clipboard${end}"
echo "speak ${color1} speak note with text to speech${end}"
echo "text ${color1} open notes text file${end}"
	
}

#---------------------------------------------#
# Main
#---------------------------------------------#

main() {

choice=$(menu | fzf --reverse \
--ansi \
--cycle \
--info=inline \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--header='
Press F1 for main menu

' \
--prompt 'oneline notes> ' \
--bind='F1:execute(fzf-launcher-wayland.sh {})' | awk '{print $1}')

case "$choice" in

 'add')
   add_note
 ;;

 'create')
   create_qr_code
 ;;

 'clip')
   clip_note
 ;;

 'speak')
   speak_note
 ;;

 'text')
   open_note_file
 ;;

 *)
   echo "Something went wrong!" || exit 1
 ;;

esac
	
}
main
