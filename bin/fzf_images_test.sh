#!/bin/bash


find ~/Pictures/sway_wallpapers/ -type f -iname '*' | fzf --no-margin --no-padding \
--no-border --preview-window border-none --preview 'imgcat {}'
