#!/bin/bash

# Program command: fzf-radio-wayland.sh
# Description: This plays online radio streams.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 09-05-2024
# Program_license: GPL 3.0
# Dependencies: fzf, mpv, playerctl, mpv-mpris, qrencode, wl-clipboard

#---------------------------------------#
# Software preferences
#---------------------------------------#

# Text editor
text_editor="geany"
export text_editor

#---------------------------------------#
# Create files and directories
#---------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/.temqrcodes ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes
   fi

   if [ -d "$HOME"/Pictures/qr_codes/ ]; then
     :
   else
     mkdir "$HOME"/Pictures/qr_codes/
   fi

   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ -f "$HOME"/Documents/mainprefix/radiolinks.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/radiolinks.txt
   fi

   if [ -f "$HOME"/Documents/.fzf_volume_level.txt ]; then
     :
   else
     touch "$HOME"/Documents/.fzf_volume_level.txt
     echo "playerctl --player mpv volume 1%" > "$HOME"/Documents/.fzf_volume_level.txt
   fi

#---------------------------------------#
# Add 100% volume to empty file
#---------------------------------------#

volume_file=$(cat "$HOME"/Documents/.fzf_volume_level.txt)
export volume_file
   
   if [[ -z "$volume_file" ]]; then
     echo "playerctl --player mpv volume 1%" | tee "$HOME"/Documents/.fzf_volume_level.txt
     killall yad
   else
     :
   fi
   
#---------------------------------------#
# Colors
#---------------------------------------#

# Keybindings
color1=$(tput setaf 15)
export color1
# Color end
end=$(tput sgr0)
export end

#---------------------------------------#
# Currently playing url
#---------------------------------------#

radio_url() {

   if [[ "$(pidof mpv)" ]]; then
     notify-send "$(playerctl --player mpv metadata -f "{{xesam:url}}")"
   else
     notify-send "Not playing!"
   fi
	
}
export -f radio_url

#---------------------------------------#
# Check mpv status
#---------------------------------------#

mpv_check() {
	   
   if [[ "$(playerctl status -p mpv)" == 'Playing' ]]; then
     notify-send "Playing"
   elif	[[ "$(playerctl status -p mpv)" == 'Paused' ]]; then
	 notify-send "Paused"
   else
     notify-send "Not Playing"
   fi  
     
}
export -f mpv_check

#---------------------------------------#
# Play radio stream
#---------------------------------------#

plays_radio() {

killall mpv
check_last_played=$(playerctl metadata -f '{{xesam:url}}')
echo "$check_last_played" > "$HOME"/Documents/.lasted_played.txt
choice=$(cat "$HOME"/Documents/.current_url.txt | awk '{print $NF}')
setsid mpv --no-video "$choice" >/dev/null 2>&1 & disown

}
export -f plays_radio

#---------------------------------------#
# Play and pause toggle
#---------------------------------------#

play_toggle() {

   if [[ "$(pidof mpv)" ]]; then
      setsid playerctl --player mpv play-pause >/dev/null 2>&1 & disown
   else
     notify-send "Not playing!"
   fi
	
}
export -f play_toggle

#---------------------------------------#
# Send track title to clipboard
#---------------------------------------#

clip_title() {

setsid playerctl --player mpv metadata -f "{{title}}" | wl-copy -n >/dev/null 2>&1 & disown

}
export -f clip_title

#---------------------------------------#
# Open radio links text file
#---------------------------------------#

open_text() {
	
setsid "$text_editor" "$HOME"/Documents/mainprefix/radiolinks.txt >/dev/null 2>&1 & disown
sleep 0.2

}
export -f open_text

#---------------------------------------#
# Volume toggle
#---------------------------------------#

volume_toggle() {

   if [[ "$(cat "$HOME"/Documents/.fzf_volume_level.txt)" == 'playerctl --player mpv volume 1%' && "$(pidof mpv)" ]]; then
     setsid echo "playerctl --player mpv volume 0%" | tee "$HOME"/Documents/.fzf_volume_level.txt >/dev/null 2>&1 & disown
	 setsid playerctl --player mpv volume 0% >/dev/null 2>&1 & disown
	 notify-send "Muted!"
   elif [[ "$(cat "$HOME"/Documents/.fzf_volume_level.txt)" == 'playerctl --player mpv volume 0%' && "$(pidof mpv)" ]]; then
     setsid echo "playerctl --player mpv volume 1%" | tee "$HOME"/Documents/.fzf_volume_level.txt >/dev/null 2>&1 & disown
	 setsid playerctl --player mpv volume 1% >/dev/null 2>&1 & disown
	 notify-send "Volume 100%"
   else
     notify-send "Not playing!"
   fi

}
export -f volume_toggle

#---------------------------------------#
# QR Code
#---------------------------------------#

create_qr_code() {

rm -f "$HOME"/Pictures/.temqrcodes/*.png
playerctl --player mpv metadata -f "{{title}}" | tr -d '\n' | \
qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
one3=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
notify-send -i "$one3" "Created" "Qr Code"

}
export -f create_qr_code

#---------------------------------------#
# Kill mpv
#---------------------------------------#

kill_mpv() {

killall mpv
	
}
export -f kill_mpv

#---------------------------------------#
# Kill mpv
#---------------------------------------#

kill_terminal() {

pkill -f ~/bin/fzf-radio-wayland.sh
	
}
export -f kill_terminal

#---------------------------------------#
# Main
#---------------------------------------#

main(){

textfile=$(cat "$HOME"/Documents/mainprefix/radiolinks.txt | sort)
launcher=$(echo "$textfile" | fzf --reverse \
--ansi \
--cycle \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:#660020,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--margin="5" \
--info=inline \
--header="
${color1}enter${end} play | ${color1}F1${end} create qr code | ${color1}F2${end} play-pause | ${color1}F3${end} stop | ${color1}F4${end} text file
${color1}F5${end} status | ${color1}F6${end} URL | ${color1}F7${end} toggle volume | ${color1}F8${end} clip title | ${color1}F9${end} kill script

" \
--bind='F3:execute(kill_mpv {})' \
--bind='F1:execute(create_qr_code {})' \
--bind='F9:execute(kill_terminal {})' \
--bind='F2:execute(play_toggle {})' \
--bind='F6:execute(radio_url {})' \
--bind='F7:execute(volume_toggle {})' \
--bind='F5:execute(mpv_check {})' \
--bind='F4:execute(open_text {})' \
--bind='F8:execute(clip_title {})' \
--bind='enter:execute-silent(echo -n {} | tee "$HOME"/Documents/.current_url.txt)+execute(plays_radio {})' \
--prompt 'radio> ')
[[ -z "$launcher" ]] && exit 0
setsid echo "$launcher" >/dev/null 2>&1 & disown
 
}
export -f main
main

#---------------------------------------#
# Unset variables and functions
#---------------------------------------#

unset color1
unset end
unset text_editor
unset radio_url
unset mpv_check
unset main
unset plays_radio
unset play_toggle
unset clip_title
unset open_text
unset volume_file
unset volume_toggle
unset kill_mpv
unset kill_terminal
unset create_qr_code
