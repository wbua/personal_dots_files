#!/bin/bash

# Program command: set_gtk_theme.sh
# Description: Changes gtk themes for sway.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 05-03-2024
# Program_license: GPL 3.0
# Dependencies: yad, Material Icons

#---------------------------------------------#
# Yaru light theme
#---------------------------------------------#

yaru_light() {
:
#echo "'Yaru'" > ~/Documents/.sway_gtk_theme.txt
#sed -i "7s|^.*$|gsettings set \$gnome-schema gtk-theme $(cat "$HOME"/Documents/.sway_gtk_theme.txt)|" /home/john/.config/sway/gtkthemes
#swaymsg reload
	
}
export -f yaru_light

#---------------------------------------------#
# Yaru dark theme
#---------------------------------------------#

yaru_dark() {
:
#echo "'Yaru-dark'" > ~/Documents/.sway_gtk_theme.txt
#sed -i "7s|^.*$|gsettings set \$gnome-schema gtk-theme $(cat "$HOME"/Documents/.sway_gtk_theme.txt)|" /home/john/.config/sway/gtkthemes
#swaymsg reload
	
}
export -f yaru_dark

#---------------------------------------------#
# Colors
#---------------------------------------------#

color01="<span color='#FFFFFF' font_family='JetBrainsMono Nerd Font' font='20' rise='0pt'>"
export color01
end="</span>"
export end

#---------------------------------------------#
# Main
#---------------------------------------------#

_yad() {

GTK_THEME="Mc-OS-Transparent-1.3" yad --form --column="search" --width=500 --height=500 --columns=1 \
--borders=60 \
--text="${color01}Gtk themes${end}

Reload your program to see theme
" \
--text-align="center" \
--field="<b>Yaru Light</b>":fbtn '/bin/bash -c "yaru_light"' \
--field="<b>Yaru Dark</b>":fbtn '/bin/bash -c "yaru_dark"' \
--button="<span color='#FF0031' font_family='Material Icons' font='20'></span>":1
	
}
_yad
export -f _yad

#---------------------------------------------#
# Unset variables and functions
#---------------------------------------------#

unset _yad
unset yaru_dark
unset yaru_light
unset color01
unset end
