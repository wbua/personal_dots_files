#!/bin/bash

set -e

#-----------------------------------------------------#
#
#-----------------------------------------------------#

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Color config
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#-----------------------------------------------------#
#
#-----------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

MY_COLOR_ICON="$COLOR_ICON"
export MY_COLOR_ICON

#-----------------------------------------------------#
#
#-----------------------------------------------------#

# Current program
color_app="<span color='$MY_COLOR_ICON' font_family='Inter' weight='bold' font='14' rise='0pt'>"
export color_app
# Color end
color_end="</span>"
export color_end

#-----------------------------------------------------#
#
#-----------------------------------------------------#

update_browser() {

sed -i "s|.*\(MAIN_BROWSER=\).*|MAIN_BROWSER=\"$(printf '%s' "$field1")\"|" ~/bin/sway_user_preferences.sh || exit 1

	
}
export -f update_browser

#-----------------------------------------------------#
#
#-----------------------------------------------------#

update_terminal() {

sed -i "s|.*\(MAIN_TERMINAL=\).*|MAIN_TERMINAL=\"$(printf '%s' "$field2")\"|" ~/bin/sway_user_preferences.sh || exit 1

	
}
export -f update_terminal

#-----------------------------------------------------#
#
#-----------------------------------------------------#

update_filemanager() {

sed -i "s|.*\(MAIN_FILE_MANAGER=\).*|MAIN_FILE_MANAGER=\"$(printf '%s' "$field3")\"|" ~/bin/sway_user_preferences.sh || exit 1

	
}
export -f update_filemanager

#-----------------------------------------------------#
#
#-----------------------------------------------------#

update_social() {

sed -i "s|.*\(MAIN_SOCIAL=\).*|MAIN_SOCIAL=\"$(printf '%s' "$field4")\"|" ~/bin/sway_user_preferences.sh || exit 1

	
}
export -f update_social

#-----------------------------------------------------#
#
#-----------------------------------------------------#

update_texteditor() {

sed -i "s|.*\(MAIN_TEXT_EDITOR=\).*|MAIN_TEXT_EDITOR=\"$(printf '%s' "$field5")\"|" ~/bin/sway_user_preferences.sh || exit 1

	
}
export -f update_texteditor

#-----------------------------------------------------#
#
#-----------------------------------------------------#

update_development() {

sed -i "s|.*\(MAIN_DEV=\).*|MAIN_DEV=\"$(printf '%s' "$field6")\"|" ~/bin/sway_user_preferences.sh || exit 1

	
}
export -f update_development

#-----------------------------------------------------#
#
#-----------------------------------------------------#

update_videoeditor() {

sed -i "s|.*\(MAIN_VIDEO_EDITOR=\).*|MAIN_VIDEO_EDITOR=\"$(printf '%s' "$field7")\"|" ~/bin/sway_user_preferences.sh || exit 1

	
}
export -f update_videoeditor

#-----------------------------------------------------#
#
#-----------------------------------------------------#

update_audioeditor() {

sed -i "s|.*\(MAIN_SOUND_EDITOR=\).*|MAIN_SOUND_EDITOR=\"$(printf '%s' "$field8")\"|" ~/bin/sway_user_preferences.sh || exit 1

	
}
export -f update_audioeditor

#-----------------------------------------------------#
#
#-----------------------------------------------------#

update_imageeditor() {

sed -i "s|.*\(MAIN_IMAGE_EDITOR=\).*|MAIN_IMAGE_EDITOR=\"$(printf '%s' "$field9")\"|" ~/bin/sway_user_preferences.sh || exit 1

	
}
export -f update_imageeditor

#-----------------------------------------------------#
#
#-----------------------------------------------------#

update_imageviewer() {

sed -i "s|.*\(MAIN_IMAGE_VIEWER=\).*|MAIN_IMAGE_VIEWER=\"$(printf '%s' "$field10")\"|" ~/bin/sway_user_preferences.sh || exit 1

	
}
export -f update_imageviewer

#-----------------------------------------------------#
#
#-----------------------------------------------------#

update_videoplayer() {

sed -i "s|.*\(MAIN_VIDEO_PLAYER=\).*|MAIN_VIDEO_PLAYER=\"$(printf '%s' "$field11")\"|" ~/bin/sway_user_preferences.sh || exit 1

	
}
export -f update_videoplayer

#-----------------------------------------------------#
#
#-----------------------------------------------------#

update_pdfviewer() {

sed -i "s|.*\(MAIN_PDF_VIEWER=\).*|MAIN_PDF_VIEWER=\"$(printf '%s' "$field12")\"|" ~/bin/sway_user_preferences.sh || exit 1

	
}
export -f update_pdfviewer

#-----------------------------------------------------#
#
#-----------------------------------------------------#

entry_choice=$(GTK_THEME="$MY_GTK_THEME" yad --form \
 --title="Change main programs" \
 --text-align="center" \
 --text="\nEnter program choice...\n" \
 --columns=1 \
 --scroll \
 --width=500 \
 --height=510 \
 --borders=20 \
 --separator="|" \
 --align="right" \
 --field="Browser: ${color_app}$MAIN_BROWSER${color_end}":LBL "" \
 --field="Browser ":CE \
 --field="Terminal: ${color_app}$MAIN_TERMINAL${color_end}":LBL "" \
 --field="Terminal ":CE \
 --field="File manager: ${color_app}$MAIN_FILE_MANAGER${color_end}":LBL "" \
 --field="File manager ":CE \
 --field="Social: ${color_app}$MAIN_SOCIAL${color_end}":LBL "" \
 --field="Social ":CE \
 --field="Text editor: ${color_app}$MAIN_TEXT_EDITOR${color_end}":LBL "" \
 --field="Text editor ":CE \
 --field="Development: ${color_app}$MAIN_DEV${color_end}":LBL "" \
 --field="Development ":CE \
 --field="Video editor: ${color_app}$MAIN_VIDEO_EDITOR${color_end}":LBL "" \
 --field="Video editor ":CE \
 --field="Audio editor: ${color_app}$MAIN_SOUND_EDITOR${color_end}":LBL "" \
 --field="Audio editor ":CE \
 --field="Image editor: ${color_app}$MAIN_IMAGE_EDITOR${color_end}":LBL "" \
 --field="Image editor ":CE \
 --field="Image viewer: ${color_app}$MAIN_IMAGE_VIEWER${color_end}":LBL "" \
 --field="Image viewer ":CE \
 --field="Video player: ${color_app}$MAIN_VIDEO_PLAYER${color_end}":LBL "" \
 --field="Video player ":CE \
 --field="PDF viewer: ${color_app}$MAIN_PDF_VIEWER${color_end}":LBL "" \
 --field="PDF viewer ":CE \
 --button="Ok":0 \
 --button="Exit":1)

field1=$(echo "$entry_choice" | awk -F '|' '{print $2}')
export field1 
field2=$(echo "$entry_choice" | awk -F '|' '{print $4}')
export field2
field3=$(echo "$entry_choice" | awk -F '|' '{print $6}')
export field3
field4=$(echo "$entry_choice" | awk -F '|' '{print $8}')
export field4
field5=$(echo "$entry_choice" | awk -F '|' '{print $10}')
export field5
field6=$(echo "$entry_choice" | awk -F '|' '{print $12}')
export field6
field7=$(echo "$entry_choice" | awk -F '|' '{print $14}')
export field7
field8=$(echo "$entry_choice" | awk -F '|' '{print $16}')
export field8
field9=$(echo "$entry_choice" | awk -F '|' '{print $18}')
export field9
field10=$(echo "$entry_choice" | awk -F '|' '{print $20}')
export field10
field11=$(echo "$entry_choice" | awk -F '|' '{print $22}')
export field11
field12=$(echo "$entry_choice" | awk -F '|' '{print $24}')
export field12

# Browser   
[ -z "$field1" ] && : || update_browser 
# Terminal 
[ -z "$field2" ] && : || update_terminal
# File manager
[ -z "$field3" ] && : || update_filemanager
# File manager
[ -z "$field4" ] && : || update_social
# Text editor
[ -z "$field5" ] && : || update_texteditor
# Development
[ -z "$field6" ] && : || update_development
# Video editor
[ -z "$field7" ] && : || update_videoeditor
# Audio editor
[ -z "$field8" ] && : || update_audioeditor
# Image editor
[ -z "$field9" ] && : || update_imageeditor
# Image viewer
[ -z "$field10" ] && : || update_imageviewer
# Video player
[ -z "$field11" ] && : || update_videoplayer
# PDF viewer
[ -z "$field12" ] && : || update_pdfviewer

#-----------------------------------------------------#
#
#-----------------------------------------------------#

unset update_terminal
unset field1
unset field2
unset update_browser
unset MY_GTK_THEME
unset color_end
unset color_end
unset MY_COLOR_ICON
unset update_filemanager
unset field3
unset update_social
unset field4
unset update_texteditor
unset field5
unset field6
unset field7
unset field8
unset field9
unset field10
unset field11
unset field12
