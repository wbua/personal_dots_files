#!/bin/bash
# shellcheck disable=SC2090
# shellcheck disable=SC2089

# Program command: yad_show_notes.sh
# Description: Shows notes. Used with polybar.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 16-07-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad, xclip, festival, qrencode, material icons, custom gtk.css

######################################
# Features
######################################

# FILE - opens text file containing your notes.
# QR Code - creates qr code from your notes
# SPEAK - speaks your notes with text to speech program festival.
# CLIP - sends notes to clipboard.
# X - closes the program.

######################################
# How to use this script
######################################

# This script shows whats in the text file: "$HOME"/Documents/yad_note.txt
# It shows you oneline notes.
# Just click on an item and press the corresponding button.
# Example 1: Say there is a oneline note called "I like listening to chillout music".
# Just click on it and press the button "clip". It will then add it to the clipboard.
# Example 2: Again there is a oneline note called "I like listening to chillout music".
# Just click on it and press the button "speak". It will now speak the text using festival.

#--------------------------------------------------------------------#
# Position of dialog on screen
#--------------------------------------------------------------------#

# Yad box y position
box_y_position="60"
export box_y_position
# Yad box x position
box_x_position="1300"
export box_x_position

#--------------------------------------------------------------------#
# Creates files and directories
#--------------------------------------------------------------------#

   # Create Documents directory
   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   # Create Pictures directory
   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   # Create temp qr code directory
   if [ -d "$HOME"/Pictures/.temqrcodes ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes
   fi

   # Create main qr code directory
   if [ -d "$HOME"/Pictures/qr_codes ]; then
     :
   else
     mkdir "$HOME"/Pictures/qr_codes
   fi

   # Creates note
   if [ -f "$HOME"/Documents/yad_note.txt ]; then
     :
   else
     touch "$HOME"/Documents/yad_note.txt
   fi

#--------------------------------------------------------------------#
# Pango markup
#--------------------------------------------------------------------#

color1="<span color='#FF00FE' font_family='Inter' font='11' weight='bold'>"
export color1
color2="<span color='#FF00FE' font_family='Material Icons' font='18' weight='bold'>"
export color2
end="</span>"
export end

#--------------------------------------------------------------------#
# Text file location
#--------------------------------------------------------------------#

text_notes=$(tac "$HOME"/Documents/yad_note.txt)
export text_notes

#--------------------------------------------------------------------#
# Open text file
#--------------------------------------------------------------------#

open_text() {
  GTK_THEME="Yaru"
  xdg-open "$HOME"/Documents/yad_note.txt

}
export -f open_text

#--------------------------------------------------------------------#
# Yad settings
#--------------------------------------------------------------------#

_yad() {

GTK_THEME="alt-dialog" yad --list --column="notes":TEXT --no-headers --separator= \
--width=500 --height=250 --borders=10 \
--posx="$box_x_position" --posy="$box_y_position" \
--button="${color1}FILE${end}:/bin/bash -c 'open_text'" \
--button="${color1}QR Code${end}":30 \
--button="${color1}SPEAK${end}":20 \
--button="${color1}CLIP${end}":10 \
--button="${color2}${end}":01
echo $?

}
export -f _yad

#--------------------------------------------------------------------#
# Main
#--------------------------------------------------------------------#

main() {

choice=$(echo "$text_notes" | _yad)
export choice

   if [[ "${choice:0-2}" == *"10"* ]]; then
     echo -n "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | xclip -selection clipboard
   elif [[ "${choice:0-2}" == *"20"* ]]; then
     echo -n "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | festival --tts
   elif [[ "${choice:0-2}" == *"30"* ]]; then
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     echo -n "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | qrencode -m 36 -l H -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
     one2=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     export one2
     notify-send -i "$one2" "Created" "Qr Code"
   else
     :
   fi

}
export -f main
main

#--------------------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------------------#

unset choice
unset text_notes
unset _yad
unset box_y_position
unset box_x_position
unset color1
unset color2
unset end
unset open_text
unset main
unset one2
