#!/bin/bash

# Program command: fzf-configs-wayland.sh
# Description: View, preview and open configuration file.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 27-12-2023
# Program_license: GPL 3.0
# Dependencies: fzf

#----------------------------------------------#
# Software preferences
#----------------------------------------------#

# Text editor
text_editor="geany"
export text_editor

#----------------------------------------------#
# Create files and directories
#----------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi
   
   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ -f "$HOME"/Documents/mainprefix/configs.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/configs.txt
   fi

#----------------------------------------------#
# Colors
#----------------------------------------------#

# Keybindings
color1=$(tput setaf 15)
export color1
# Color end
end=$(tput sgr0)
export end

#----------------------------------------------#
# Kill script
#----------------------------------------------#

kill_script() {

pkill -f ~/bin/fzf-configs-wayland.sh
	
}
export -f kill_script

#----------------------------------------------#
# Open configs text file
#----------------------------------------------#

open_file() {

setsid "$text_editor" ~/Documents/mainprefix/configs.txt >/dev/null 2>&1 & disown
pkill -f ~/bin/fzf-configs-wayland.sh
	
}
export -f open_file

#----------------------------------------------#
# fzf settings
#----------------------------------------------#

choice=$(cat "$HOME"/Documents/mainprefix/configs.txt | \
fzf  --ansi \
--cycle \
--reverse \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:#660020,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--info=inline \
--header="

Preview page up: ${color1}shift+up${end} | Preview page down: ${color1}shift+down${end}
${color1}enter${end} open file | ${color1}F9${end} kill script | ${color1}F1${end} text file

" \
--prompt 'configs> ' \
--bind='F1:execute(open_file {})' \
--bind='F9:execute(kill_script {})' \
--preview-window=top,70% \
--preview 'batcat --color=always {}' \
--bind shift-up:preview-page-up,shift-down:preview-page-down)
export choice
[ -z "$choice" ] && exit 0

#----------------------------------------------#
# Main
#----------------------------------------------#

main() {
	
setsid "$text_editor" "$choice" >/dev/null 2>&1 & disown
pkill -f ~/bin/fzf-configs-wayland.sh

}
export -f main
main

#----------------------------------------------#
# Unset variables and functions
#----------------------------------------------#

unset main
unset kill_script
unset choice
unset open_file
unset color1
unset end
