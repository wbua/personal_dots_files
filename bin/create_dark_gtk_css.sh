#!/bin/bash

set -e

#---------------------------------------------------#
# Sourcing files
#---------------------------------------------------#

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Color config
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#---------------------------------------------------#
# User preferences
#---------------------------------------------------#

# Geany themes directory
GTK_DIR="$HOME/.config/gtk-3.0"

# Geany theme file
GTK_FILE="gtk.css"

#---------------------------------------------------#
# Create files and directories
#---------------------------------------------------#

   if [ -d "$GTK_DIR" ]; then
     :
   else
     mkdir "$GTK_DIR"
   fi
   
   if [ -f "$GTK_DIR"/"$GTK_FILE" ]; then
     :
   else
     touch "$GTK_DIR"/"$GTK_FILE"
   fi

#---------------------------------------------------#
# Create the geany css file
#---------------------------------------------------#
   
cat <<EOF > "$GTK_DIR"/"$GTK_FILE"

@import url("mytheme.css");

.thunar .standard-view .view:selected { 
background-color: @thunar-alternative-color;
color: black; 
}

.thunar .sidebar .view { 
background-color: @thunar-background-color; 
color: white;

}

.thunar .standard-view { 
background-color: @thunar-background-color; 
color: white;

}

.thunar toolbar, .thunar toolbar entry  { 
background-color: @thunar-background-color; 
color: white; 
}

.thunar grid paned grid  { 
background-color: @thunar-background-color;
color: white; 
}

.thunar menubar { 
background-color: @thunar-background-color;
color: white; 
}

.thunar .shortcuts-pane .view:selected {
background-color: @thunar-alternative-color;
color: black;
}

/* Menubar dropdowns background and text color */
.thunar menubar menu, menubar menuitem {
    background-color: @thunar-background-color;
    color: white;
}


/* Highlight color for menu items when hovered */
.thunar menubar menuitem:hover {
    background-color: @thunar-alternative-color;
    color: black;
}

/* Active tab styles */
.thunar tab:checked,
tab:checked .label {
    background-color: @thunar-alternative-color;
    color: black;
}

/* Inactive tab styles */
.thunar tab,
tab .label {
    color: white;
}

/* Highlight color for menu items when hovered */
menubar menuitem:hover {
    background-color: @thunar-alternative-color;
    color: black;
}

menubar {
    background-color: @thunar-background-color;
    color: white;
}

.thunar .view {
   background-color: @thunar-background-color;
   color: white;
}

EOF
