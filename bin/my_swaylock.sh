#!/bin/bash

# Program command: my_swaylock.sh
# Description: Sway screen locking.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-08-2024
# Program_license: GPL 3.0
# Dependencies: ffmpeg, imagemagick, grim

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#---------------------------------------------------------#
# User Preferences
#---------------------------------------------------------#

# Wallpaper directory
WALLPAPER_DIR="$MAIN_WALLPAPER_DIR"

# Wallpaper file
TEMP_WALLPAPER_RANDOM="$MAIN_SWAY_WALLPAPER"

#---------------------------------------------------------#
# Create files and directories
#---------------------------------------------------------#

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/sway_lock ]; then
     :
   else
     mkdir "$HOME"/Pictures/sway_lock
   fi

#---------------------------------------------------------#
# Sound effects
#---------------------------------------------------------#

snd_effects() {

   if [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     setsid paplay ~/Music/sounds/locking.mp3 >/dev/null 2>&1 & disown
   elif [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     :
   else
     :
   fi
	
}

#---------------------------------------------------------#
# Use currently set wallpaper or random wallpaper background
#---------------------------------------------------------#

random_locking_bg() {

   if [[ "$(grep SWAY_RANDOM_LOCKING_BG= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     #notify-send "Locking"
     rm -f ~/Pictures/sway_lock/*.*
     snd_effects
     ran_wall_file=$(ls "$WALLPAPER_DIR" | shuf -n1)
     cp "$WALLPAPER_DIR/$ran_wall_file" ~/Pictures/sway_lock/lockscreen.jpg
     convert ~/Pictures/sway_lock/lockscreen.jpg -fill black -colorize 80% ~/Pictures/sway_lock/lockscreen.jpg
   elif [[ "$(grep SWAY_RANDOM_LOCKING_BG= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     rm -f ~/Pictures/sway_lock/*.*
     snd_effects
     cp "$MAIN_SWAY_WALLPAPER" ~/Pictures/sway_lock/lockscreen.jpg
     convert ~/Pictures/sway_lock/lockscreen.jpg -fill black -colorize 80% ~/Pictures/sway_lock/lockscreen.jpg
   else
     :
   fi
	
}

#---------------------------------------------------------#
# Use screenshot or wallpaper as background
#---------------------------------------------------------#

   if [[ "$(grep SWAY_LOCKING_BACKGROUND= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     random_locking_bg  
   elif [[ "$(grep SWAY_LOCKING_BACKGROUND= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     rm -f ~/Pictures/sway_lock/*.*
     snd_effects
     grim ~/Pictures/sway_lock/lockscreen.jpg && convert -filter Gaussian -blur 0x1 ~/Pictures/sway_lock/lockscreen.jpg ~/Pictures/sway_lock/lockscreen.jpg
     convert ~/Pictures/sway_lock/lockscreen.jpg -fill black -colorize 80% ~/Pictures/sway_lock/lockscreen.jpg
   else
    :
   fi
