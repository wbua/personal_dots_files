#!/bin/bash

# Program command: yad_local_songs.sh
# Description: Plays local songs.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 22-08-2024
# Program_license: GPL 3.0
# Dependencies: yad, mpv, playerctl, mpv-mpris

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-------------------------------------------------#
# Error checking
#-------------------------------------------------#

set -e

#-------------------------------------------------#
# User preferences
#-------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Music directory
MUSIC_DIR="$MAIN_SONGS_DIR"
export MUSIC_DIR

#-------------------------------------------------#
# Create files and directories
#-------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Music ]; then
     :
   else
     mkdir "$HOME"/Music
   fi

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/.cover_art ]; then
     :
   else
     mkdir "$HOME"/Pictures/.cover_art
   fi

   if [ "$MUSIC_DIR" == "disable" ]; then
     :
   elif [ -d "$MUSIC_DIR" ]; then
     :
   else
     notify-send "Directory does not exist!" && exit 1
   fi

#-------------------------------------------------#
# Colors
#-------------------------------------------------#

COLOR1="<span color='#FFFFFF' font_family='JetBrainsMono Nerd Font' font='21' rise='0pt'>"
export COLOR1
END="</span>"
export END

#-------------------------------------------------#
# Plays shuffle tracks
#-------------------------------------------------#

play_track() {

killall mpv
killall ffplay
killall rhythmbox
mpv --shuffle --loop-playlist --vid=no "$MUSIC_DIR"/*.*

}
export -f play_track

#-------------------------------------------------#
# Prev track
#-------------------------------------------------#

mpv_prev() {

playerctl previous
	
}
export -f mpv_prev

#-------------------------------------------------#
# Play-pause toggle
#-------------------------------------------------#

play_toggle() {

playerctl play-pause
	
}
export -f play_toggle

#-------------------------------------------------#
# Selected item from dclick
#-------------------------------------------------#

select_track() {

echo "$1" > "$HOME"/Documents/.your_music_file.txt
	
}
export -f select_track

#-------------------------------------------------#
# Seek forward in track
#-------------------------------------------------#

track_seek() {

playerctl --player=mpv position 30+

}
export -f track_seek

#-------------------------------------------------#
# Currently playing track
#-------------------------------------------------#

current_track() {

rm -f "$HOME"/Pictures/.cover_art/*.*
art_file="$HOME/Documents/.cover_art_temp.txt"
playerctl metadata --format '{{xesam:url}}' | sed 's|file://||' | sed 's|%20| |g'  > "$art_file"
ffmpeg -i "$(cat "$art_file")" -an -vcodec copy "$HOME"/Pictures/.cover_art/cover.png
convert "$HOME"/Pictures/.cover_art/cover.png -resize 150x150 "$HOME"/Pictures/.cover_art/cover.png

cover_dir=$(find "$HOME"/Pictures/.cover_art/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
notify-send -i "$cover_dir" "$(playerctl metadata --format '{{xesam:artist}} - {{xesam:title}}')"

}
export -f current_track

#-------------------------------------------------#
# Mpv status
#-------------------------------------------------#

mpv_status() {

   if [[ "$(pidof mpv)"  ]]; then
     notify-send "$(playerctl --player mpv status)"
   else
     notify-send "Not playing!"
   fi

}
export -f mpv_status

#-------------------------------------------------#
# Mute
#-------------------------------------------------#

mpv_mute() {

playerctl --player mpv volume 0%

}
export -f mpv_mute

#-------------------------------------------------#
# Unmute
#-------------------------------------------------#

mpv_unmute() {

playerctl --player mpv volume 1%
	
}
export -f mpv_unmute

#-------------------------------------------------#
# Plays selected track only
#-------------------------------------------------#

chosen_track() {

killall mpv
killall ffplay
killall rhythmbox
chosen_file=$(cat "$HOME"/Documents/.your_music_file.txt)
mpv --vid=no "$MUSIC_DIR"/"$chosen_file"
	
}
export -f chosen_track

#-------------------------------------------------#
# Kills mpv
#-------------------------------------------------#

kill_yad() {

killall mpv
	
}
export -f kill_yad

#-------------------------------------------------#
# Next track
#-------------------------------------------------#

mpv_next() {

playerctl next

}
export -f mpv_next

#-------------------------------------------------#
# Change music directory
#-------------------------------------------------#

change_music_dir() {

killall yad
md_choice=$(yad --form --width=600 --height=150 --separator= --field="Directory ":DIR --button="OK:0" --button="Exit:1" )
[[ -z "$md_choice" ]] && exit
sed -i "s|.*\(MAIN_SONGS_DIR=\).*|MAIN_SONGS_DIR=\"$(printf '%s' "$md_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
	
}
export -f change_music_dir

#-------------------------------------------------#
# Settings
#-------------------------------------------------#

mpv_settings() {

killall yad
yad --form \
--borders=80 \
--width=600 \
--height=500 \
--field="<b>Music directory</b>":fbtn '/bin/bash -c "change_music_dir"' \
--button="${COLOR1}󰅖${END}:0"
	
}
export -f mpv_settings

#-------------------------------------------------#
# Yad dialog
#-------------------------------------------------#

   if [[ "$MUSIC_DIR" == "disable" ]]; then
     notify-send "Music player has been disabled" && exit 1
   else
     find "$MUSIC_DIR" -type f -iname '*.*' | sed "s/\&/\&amp;/g" | sort | \
     cut -d'/' -f6- | GTK_THEME="$MY_GTK_THEME" yad --list \
     --column="music" \
     --search-column=1 \
     --regex-search \
     --text="\nLocal music playlist\n" \
     --text-align="center" \
     --width=900 \
     --height=600 \
     --no-headers \
     --borders=10 \
     --dclick-action='/bin/bash -c "select_track %s"' \
     --button="${COLOR1}󰐎${END}":"bash -c play_toggle" \
     --button="${COLOR1}󰒅${END}":"bash -c chosen_track" \
     --button="${COLOR1}󰒝${END}":"bash -c play_track" \
     --button="${COLOR1}󰒮${END}":"bash -c mpv_prev" \
     --button="${COLOR1}󰒭${END}":"bash -c mpv_next" \
     --button="${COLOR1}󰠃${END}":"bash -c current_track" \
     --button="${COLOR1}󰒬${END}":"bash -c track_seek" \
     --button="${COLOR1}󱖫${END}":"bash -c mpv_status" \
     --button="${COLOR1}${END}":"bash -c mpv_mute" \
     --button="${COLOR1}${END}":"bash -c mpv_unmute" \
     --button="${COLOR1}${END}":"bash -c kill_yad" \
     --button="${COLOR1}${END}":"bash -c mpv_settings" \
     --button="${COLOR1}󰅖${END}:0"
   fi

#-------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------#

unset play_track
unset kill_yad
unset mpv_next
unset MUSIC_DIR
unset chosen_track
unset mpv_prev
unset play_toggle
unset MY_GTK_THEME
unset current_track
unset track_seek
unset mpv_status
unset COLOR1
unset END
unset mpv_mute
unset mpv_unmute
unset mpv_unmute
unset mpv_settings
unset change_music_dir
