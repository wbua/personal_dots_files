#!/bin/bash

# Program command: yad-volume.sh
# Description: Shows and changes system volume.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 26-04-2024
# Program_license: GPL 3.0
# Dependencies: yad, amixer, material icons

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#--------------------------------------------------#
# Error checking
#--------------------------------------------------#

set -e

#--------------------------------------------------#
# User preferences
#--------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#--------------------------------------------------#
# Colors
#--------------------------------------------------#

# Volume up
color01="<span color='#FFFFFF' font_family='Material Icons' font='16' rise='0pt'>"
export color01
# Volume down
color02="<span color='#FFFFFF' font_family='Material Icons' font='16' rise='0pt'>"
export color02
# Close
color03="<span color='#FF0036' font_family='Material Icons' font='16' rise='0pt'>"
export color03
# Color end
end="</span>"
export end

#--------------------------------------------------#
# Toggle mute
#--------------------------------------------------#

toggle_mute() {

amixer -q -D pulse sset Master toggle && pkill -RTMIN+10 i3blocks
	
}
export -f toggle_mute

#--------------------------------------------------#
# Volume up
#--------------------------------------------------#

volume_up() {

amixer -q -D pulse sset Master 5%+ && pkill -RTMIN+10 i3blocks
	
}
export -f volume_up

#--------------------------------------------------#
# Volume down
#--------------------------------------------------#

volume_down() {

amixer -q -D pulse sset Master 5%- && pkill -RTMIN+10 i3blocks
	
}
export -f volume_down

#--------------------------------------------------#
# Volume status
#--------------------------------------------------#

volume_status() {

while true
do
sleep 0.1 && amixer -D pulse get Master | tail -1 | awk '{print $5}' | tr -d "[]"
done

}
export -f volume_status

#--------------------------------------------------#
# Kill script
#--------------------------------------------------#

kill_script() {

killall yad
pkill -f ~/bin//yad-volume.sh
pkill -f ~/bin/yad-volume.sh

}
export -f kill_script

#--------------------------------------------------#
# Yad settings
#--------------------------------------------------#

volume_status | GTK_THEME="$MY_GTK_THEME" yad --progress \
--title="Volume tool" \
--width=300 \
--height=100 \
--text-align="center" \
--text="Volume\n" \
--on-top \
--no-escape \
--borders=30 \
--buttons-layout="center" \
--button="${color02}${end}:/bin/bash -c 'volume_down'" \
--button="${color01}${end}:/bin/bash -c 'volume_up'" \
--button="${color01}${end}:/bin/bash -c 'toggle_mute'" \
--button="${color03}${end}:/bin/bash -c 'kill_script'" 

#--------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------#

unset volume_status 
unset volume_up
unset volume_down
unset kill_script
unset color01
unset color02
unset color03
unset end
unset toggle_mute
unset MY_GTK_THEME
