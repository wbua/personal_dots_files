#!/bin/bash

set -e

# Function to search for files and display them in YAD list dialog
search_files() {
    local search_term="$1"
    local search_dirs=("$HOME/Documents" "$HOME/Pictures" "$HOME/Music" "$HOME/Videos" "$HOME/Downloads")
    local file_list=()

    for dir in "${search_dirs[@]}"; do
        while IFS= read -r -d '' file; do
            file_list+=("$(basename "$file")" "$file")
        done < <(find "$dir" -type f -iname "*$search_term*" -print0)
    done

    yad --list --title="Search Results" --column="File Name" --column="Full Path" "${file_list[@]}" --hide-column=2 --width=600 --height=400
}

# Function to open the selected file
open_file() {
    local file_path="$1"
    setsid -f xdg-open "$file_path" >/dev/null 2>&1 & disown
}

# Main script
search_term=$(yad --entry --title="File Search" --text="Enter search term:")

if [ -n "$search_term" ]; then
    selected_file=$(search_files "$search_term" | awk -F'|' '{print $2}')
    if [ -n "$selected_file" ]; then
        open_file "$selected_file"
    fi
fi
