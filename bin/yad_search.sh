#!/bin/bash
# shellcheck disable=SC2090
# shellcheck disable=SC2089

# Program command: yad_search.sh
# Description: Search for files on local system.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 09-09-2023
# Program_license: GPL 3.0
# Dependencies: yad, xclip, Material Icons

#--------------------------------------------------------------#
# General variables
#--------------------------------------------------------------#

# File manager
file_manager="thunar"
export file_manager

#--------------------------------------------------------------#
# Kill existing yad instance
#--------------------------------------------------------------#

killall yad

#--------------------------------------------------------------#
# Colors for help page
#--------------------------------------------------------------#

# Selected
color1="<span color='#25FF00' font_family='Monospace' font='12' weight='bold'>"
export color1
# Headline
color2="<span color='#EBFF00' font_family='Monospace' font='12' weight='bold'>"
export color2
# Normal text
color3="<span color='#FFFFFF' font_family='Monospace' font='12' weight='bold'>"
export color3
# Color end
end="</span>"
export end

#--------------------------------------------------------------#
# Creates files and directories
#--------------------------------------------------------------#

   if [ -f "$HOME"/Documents/.multiple_temp.txt  ]; then
     :
   else
     touch "$HOME"/Documents/.multiple_temp.txt 
   fi

#--------------------------------------------------------------#
# Help page
#--------------------------------------------------------------#

_help_info() {

info_text="${color2}Help information${end}

${color3}This program searches for files.${end}

${color2}How to use${end}

${color3}When viewing search results, type ${color1}control+f${end} to search.${end}

${color2}Results Buttons${end}

${color1}Multiple button:${end} ${color3}Select multiple items and press button. Will save items to text file.${end}
${color3}Hold down control button and left click items${end}
${color1}Directory button:${end} ${color3}Select item and press button, to open file in file manager.${end}
${color1}Clipboard button:${end} ${color3}Select item and press button, copies item to clipboard.${end}
${color1}Default button:${end} ${color3}Select item and press button, open file in default program.${end}
${color1}Close button:${end} ${color3}Close yad.${end}

${color2}Search Buttons${end}

${color1}Saved button:${end} ${color3}Show the items you have saved.${end}
${color1}Settings button:${end} ${color3}Change your current file manager.${end}
${color1}Help button:${end} ${color3}Shows help page about this program.${end}
${color1}Close button:${end} ${color3}Close yad.${end}

${color2}About${end}

${color3}Program created by${end} ${color1}John Mcgrath${end}
"
export info_text
killall yad
echo "$info_text" | GTK_THEME="Adwaita-dark" yad --list --column="help" --height=500 --width=1200 \
--center --borders=20 \
--no-headers --separator= \
--button="<span color='#FF002A' font_family='Material Icons' font='18'></span>":1

}
export -f _help_info

#--------------------------------------------------------------#
# Saved text list box (saved text)
#--------------------------------------------------------------#

_multiple_text() {

killall yad
saved_text_file=$(cat "$HOME"/Documents/.multiple_temp.txt)
export saved_text_file
choice_text=$(echo "$saved_text_file" | sed "s/\&/\&amp;/g" | GTK_THEME="Adwaita-dark" yad --list --column="help" \
--search-column=1 --regex-search \
--height=500 --width=1200 \
--text="<span color='#ffffff' font_family='Monospace' font='14'>Saved items\n</span>" \
--text-align="center" \
--center --borders=20 \
--no-headers --separator= \
--button="<span color='#ffffff' font_family='Material Icons' font='20'></span>":40 \
--button="<span color='#ffffff' font_family='Material Icons' font='20'></span>":20 \
--button="<span color='#FFFFFF' font_family='Material Icons' font='23'></span>:/bin/bash -c '_open_saved_text'" \
--button="<span color='#ffffff' font_family='Material Icons' font='20'></span>":30 \
--button="<span color='#FF002A' font_family='Material Icons' font='18'></span>":1	
echo $?	
)	
export choice_text

   if [[ "${choice_text:0-2}" == *"30"* ]]; then
     _open_saved_default
   elif [[ "${choice_text:0-2}" == *"20"* ]]; then
     _clip_board_saved
   elif [[ "${choice_text:0-2}" == *"40"* ]]; then
     _select_saved_file
   else
     :
   fi
	
}
export -f _multiple_text

#--------------------------------------------------------------#
# Open item with default program (saved text)
#--------------------------------------------------------------#

_open_saved_default() {

   if [[ "$choice_text" == '30' ]]; then
     :
   else
     echo "${choice_text::-2}" | sed "s/\&amp;/\&/" | tr '\n' '\0' | xargs -d '\n' xdg-open
   fi

}
export -f _open_saved_default

#--------------------------------------------------------------#
# Open text file (saved text)
#--------------------------------------------------------------#

_open_saved_text() {

killall yad
xdg-open "$HOME"/Documents/.multiple_temp.txt	
	
}
export -f _open_saved_text

#--------------------------------------------------------------#
# Send item to clipboard (saved text)
#--------------------------------------------------------------#

_clip_board_saved() {

   if [[ "$choice_text" == '20' ]]; then
     :
   else
     echo "${choice_text::-2}"| sed "s/\&amp;/\&/" | tr '\n' '\0' | xclip -selection clipboard
   fi

}
export -f _clip_board_saved

#--------------------------------------------------------------#
# Select item to open in file manager (saved text)
#--------------------------------------------------------------#

_select_saved_file() {

   if [[ "$choice_text" == '40' ]]; then
     :
   else
     killall yad
     chosen=$(echo "${choice_text::-2}" | sed "s/\&amp;/\&/" | tr '\n' '\0' | xargs -d '\n')
     export chosen
     [ -z "$chosen" ] && exit 0
     "$file_manager" "$chosen"
   fi

}
export -f _select_saved_file

#--------------------------------------------------------------#
# Settings menu
#--------------------------------------------------------------#

_settings_menu() {

killall yad
GTK_THEME="Adwaita-dark" yad --form --column="search" --width=500 --height=500 --columns=2 \
--borders=20 \
--field="<b><span color='#5EFF00' font_family='Monospace' font='14'>Change File manager</span></b>":fbtn '/bin/bash -c "_change_file_manager"' \
--button="<span color='#FF0031' font_family='Material Icons' font='20'></span>":1

}
export -f _settings_menu

#--------------------------------------------------------------#
# Change file manager
#--------------------------------------------------------------#

_change_file_manager() {

killall yad
sed -i "19s|^.*$|file_manager=\"$(GTK_THEME="Adwaita-dark" yad --entry --center --width=400 --text="Change file manager")\"|" "$HOME"/bin/yad_search.sh

}
export -f _change_file_manager

#--------------------------------------------------------------#
# Directories to search
#--------------------------------------------------------------#

dirs=( ~/Documents/ ~/Pictures/ ~/Videos/ ~/Music/ ~/Downloads/)
export dirs

#--------------------------------------------------------------#
# Send item to clipboard
#--------------------------------------------------------------#

_clip_board() {

   if [[ "$launch" == '20' ]]; then
     :
   else
     echo "${launch::-2}"| sed "s/\&amp;/\&/" | tr '\n' '\0' | xclip -selection clipboard
   fi

}
export -f _clip_board

#--------------------------------------------------------------#
# Open item with default program
#--------------------------------------------------------------#

_open_default() {

   if [[ "$launch" == '30' ]]; then
     :
   else
     echo "${launch::-2}" | sed "s/\&amp;/\&/" | tr '\n' '\0' | xargs -d '\n' xdg-open
   fi

}
export -f _open_default

#--------------------------------------------------------------#
# Select item to open in file manager
#--------------------------------------------------------------#

_select_file() {

   if [[ "$choice" == '40' ]]; then
     :
   else
     killall yad
     chosen=$(echo "${launch::-2}" | sed "s/\&amp;/\&/" | tr '\n' '\0' | xargs -d '\n')
     export chosen
     [ -z "$chosen" ] && exit 0
     "$file_manager" "$chosen"
   fi

}
export -f _select_file

#--------------------------------------------------------------#
# Selects multiple items and copies them to saved text file (saved text)
#--------------------------------------------------------------#

_open_multple() {

sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.multiple_temp.txt
echo "$launch" | sed "s/\&amp;/\&/g" | tee -a "$HOME"/Documents/.multiple_temp.txt 	
sed -i '$ d' "$HOME"/Documents/.multiple_temp.txt 
	
}
export -f _open_multple

#--------------------------------------------------------------#
# Main
#--------------------------------------------------------------#

_main() {
 
# Search entry box
choice=$(GTK_THEME="Adwaita-dark" yad --entry --width=600 --height=100 \
--title="Yad search tool" \
--text="<span color='#ffffff' font_family='Monospace' font='14'>Type text to search</span>" \
--text-align="center" \
--borders=20 \
--separator= \
--button="<span color='#ffffff' font_family='Material Icons' font='23'></span>:/bin/bash -c '_multiple_text'" \
--button="<span color='#ffffff' font_family='Material Icons' font='20'></span>:/bin/bash -c '_settings_menu'" \
--button="<span color='#ffffff' font_family='Material Icons' font='20'></span>:/bin/bash -c '_help_info'" \
--buttons-layout="center" \
--button="<span color='#FF0031' font_family='Material Icons' font='20'></span>":1) || exit 0
export choice
 
# Results list box
launch=$(find "${dirs[@]}" -type f -iname "*$choice*" | sed "s/\&/\&amp;/g" | \
GTK_THEME="Adwaita-dark" yad --list --column="search" --search-column=1 --regex-search --width=1100 --height=500 \
--title="Yad search tool" \
--text="<span color='#ffffff' font_family='Monospace' font='14'>Search results\n</span>" \
--text-align="center" \
--no-headers \
--buttons-layout="center" \
--borders=20 \
--separator= \
--multiple \
--button="<span color='#ffffff' font_family='Material Icons' font='20'></span>":50 \
--button="<span color='#ffffff' font_family='Material Icons' font='20'></span>":40 \
--button="<span color='#ffffff' font_family='Material Icons' font='20'></span>":20 \
--button="<span color='#ffffff' font_family='Material Icons' font='20'></span>":30 \
--button="<span color='#FF0031' font_family='Material Icons' font='20'></span>":1
echo $?
)
export launch 
 
   # Select item and copy to clipboard
   if [[ "${launch:0-2}" == *"20"* ]]; then
     _clip_board
   # Select item and open it in default program
   elif [[ "${launch:0-2}" == *"30"* ]]; then
     _open_default
   # Select item and open it in file manager
   elif [[ "${launch:0-2}" == *"40"* ]]; then
     _select_file
   elif [[ "${launch:0-2}" == *"50"* ]]; then
     _open_multple
   else
     :
   fi

}
export -f _main
_main

#--------------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------------#

# Functions
unset _main
unset _clip_board
unset _open_default
unset _select_file
unset _change_file_manager
unset _help_info
unset _settings_menu
unset _open_multple
unset _multiple_text
unset _open_saved_text
unset _open_saved_default
unset _clip_board_saved
unset _select_saved_file

# Variables
unset choice
unset dirs
unset launch
unset file_manager
unset color1
unset color2
unset color3
unset end
unset info_text
unset chosen
unset choice_text
unset saved_text_file
