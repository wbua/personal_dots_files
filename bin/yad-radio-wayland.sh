#!/bin/bash

# Program command: yad-radio-wayland.sh
# Description: Plays radio online streams.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 23-08-2024
# Program_license: GPL 3.0
# Dependencies: yad, mpv, playerctl, mpv-mpris, Jetbrains mono nerd font, material icons

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#----------------------------------------#
# Preferences
#----------------------------------------#

# Radio streams file
RADIO_FILE="$MAIN_RADIO_FILE"
export RADIO_FILE

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Text editor
TEXT_EDITOR="$MAIN_TEXT_EDITOR"
export TEXT_EDITOR

#----------------------------------------#
# Create files and directories
#----------------------------------------#

   # Documents directory
   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi
   # Directory where text file url's is stored
   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi
  
   # Send title data from playerctl to text file
   if [ -f "$HOME"/Documents/track_log.txt ]; then
     :
   else
     touch "$HOME"/Documents/track_log.txt
   fi

   # Check radio streams file exists
   if [ "$RADIO_FILE" == "disable" ]; then
     :
   elif [ -f "$RADIO_FILE" ]; then
     :
   else
     notify-send "Radio streams file does not exist!" && exit 1
   fi

   # Volume level
   if [ -f "$HOME"/Documents/.volume_level.txt ]; then
     :
   else
     touch "$HOME"/Documents/.volume_level.txt
     echo "playerctl --player mpv volume 1%" > "$HOME"/Documents/.volume_level.txt
   fi

#----------------------------------------#
# Add volume 100% to .volume_level.txt
#----------------------------------------#

volume_file=$(cat "$HOME"/Documents/.volume_level.txt)
export volume_file
   
   if [[ -z "$volume_file" ]]; then
     echo "playerctl --player mpv volume 1%" | tee "$HOME"/Documents/.volume_level.txt
     killall yad
   else
     :
   fi

#----------------------------------------#
# Play radio
#----------------------------------------#

play_mpv() {

killall mpv
choice=$(echo "$1" | tee "$HOME"/Documents/.temp_radio_stream.txt)
export choice
temp_file=$(cat "$HOME"/Documents/.temp_radio_stream.txt | awk '{print $NF}')
export temp_file

mpv --no-video "$temp_file" 
	
}
export -f play_mpv

#----------------------------------------#
# Pause radio
#----------------------------------------#

pause_mpv() {

   if [[ "$(pidof mpv)" ]]; then	
     playerctl --player mpv play-pause	
   else
     :
   fi  
     
}
export -f pause_mpv

#----------------------------------------#
# Stop radio
#----------------------------------------#

stop_mpv() {

killall mpv	
	
}
export -f stop_mpv

#----------------------------------------#
# Currently playing track
#----------------------------------------#

current_track() {

   if [[ "$(pidof mpv)" ]]; then	
      notify-send "$(playerctl --player mpv metadata -f "{{title}}" | tr "&" "+")"
   else
     notify-send "Not playing"
   fi
	
}
export -f current_track

#----------------------------------------#
# Currently playing url
#----------------------------------------#

current_url() {
	
notify-send "$(playerctl --player mpv metadata -f "{{xesam:url}}" | tr "&" "+")"

}
export -f current_url

#----------------------------------------#
# Radio links text file
#----------------------------------------#

radio_textfile() {

killall yad	

   if [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     "$TEXT_EDITOR" "$RADIO_FILE"
     bash ~/bin/switch_to_texteditor.sh
   elif [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disabel" ]]; then
     "$TEXT_EDITOR" "$RADIO_FILE"
   else
     : 
   fi
	
}
export -f radio_textfile

#----------------------------------------#
# Greeting
#----------------------------------------#

greeting() {

hour=$(date +%H)
export hour
    
   # Afternoon
   if [ "$hour" -ge 12 ] && [ "$hour" -lt 19 ]; then
     echo "Good afternoon <span color='#25FF00' font_family='Monospace' font='15' weight='bold'>$USER</span>"
   fi
   # Morning
   if [ "$hour" -ge 00 ] && [ "$hour" -lt 12 ]; then
     echo "Good morning <span color='#25FF00' font_family='Monospace' font='15' weight='bold'>$USER</span>"
   fi
   # Evening
   if [ "$hour" -ge 19 ] && [ "$hour" -lt 24 ]; then
     echo "Good evening <span color='#25FF00' font_family='Monospace' font='15' weight='bold'>$USER</span>"
   fi	
	
}
export -f greeting

#----------------------------------------#
# Send track title to clipboard
#----------------------------------------#

clipboard() {

   if [[ "$(pidof mpv)" ]]; then
     sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/track_log.txt
     playerctl --player mpv metadata -f "{{title}}" | tee ~/Documents/.temp_yad_radio_title.txt
     temp_radio_track=$(cat ~/Documents/.temp_yad_radio_title.txt)
     echo "$temp_radio_track" | tee -a "$HOME"/Documents/track_log.txt /dev/null
     notify-send "Track copied!" "$temp_radio_track"
   else
     :
   fi
}
export -f clipboard

#----------------------------------------#
# Open text file containing track titles
#----------------------------------------#

title_log() {
	
killall yad	
xdg-open "$HOME"/Documents/track_log.txt
	
}
export -f title_log

#----------------------------------------#
# Check status of mpv
#----------------------------------------#

mpv_check() {
	
   if [[ "$(playerctl status -p mpv)" == 'Playing' ]]; then
     notify-send "Playing"
   elif	[[ "$(playerctl status -p mpv)" == 'Paused' ]]; then
	 notify-send "Paused"
   else
     notify-send "Not Playing"
   fi  
     
}
export -f mpv_check

#----------------------------------------#
# Change volume
#----------------------------------------#

change_volume() {
	
   volume_file=$(cat "$HOME"/Documents/.volume_level.txt)
   export volume_file
   # Volume 70%
   if [[ "$volume_file" == 'playerctl --player mpv volume 1%' && "$(pidof mpv)" ]]; then
     echo "playerctl --player mpv volume 0.7%" | tee "$HOME"/Documents/.volume_level.txt
	 playerctl --player mpv volume 0.7%
	 notify-send "Volume 70%"
   # Volume 50%
   elif [[ "$volume_file" == 'playerctl --player mpv volume 0.7%' && "$(pidof mpv)" ]]; then
     echo "playerctl --player mpv volume 0.5%" | tee "$HOME"/Documents/.volume_level.txt
	 playerctl --player mpv volume 0.5%
	 notify-send "Volume 50%"
   # Volume 30%
   elif [[ "$volume_file" == 'playerctl --player mpv volume 0.5%' && "$(pidof mpv)" ]]; then
     echo "playerctl --player mpv volume 0.3%" | tee "$HOME"/Documents/.volume_level.txt
	 playerctl --player mpv volume 0.3%
	 notify-send "Volume 30%"
   # Volume muted
   elif [[ "$volume_file" == 'playerctl --player mpv volume 0.3%' && "$(pidof mpv)" ]]; then
     echo "playerctl --player mpv volume 0%" | tee "$HOME"/Documents/.volume_level.txt
	 playerctl --player mpv volume 0%
	 notify-send "Volume muted!"
   # Volume 100% 
   elif [[ "$volume_file" == 'playerctl --player mpv volume 0%' && "$(pidof mpv)" ]]; then
     echo "playerctl --player mpv volume 1%" | tee "$HOME"/Documents/.volume_level.txt
     playerctl --player mpv volume 1%
     notify-send "Volume 100%"
   else
     notify-send "Not playing!"
   fi
   
}
export -f change_volume

#----------------------------------------#
# Yad settings
#----------------------------------------#

   if [[ "$RADIO_FILE" == "disable" ]]; then
     notify-send "Radio has been disabled"
   else
     cat "$RADIO_FILE" | sort | \
     sed "s/\&/\&amp;/g" | GTK_THEME="$ALL_YAD_GTK" yad --list --column="radio" \
     --title="Online radio" \
     --borders=20 \
     --search-column=1 \
     --text="\nDouble click on radio link to play!\n$(greeting)\n" \
     --text-align="center" \
     --height=550 --width=900 \
     --no-headers \
     --buttons-layout="center" \
     --separator= \
     --dclick-action='/bin/bash -c "play_mpv %s"' \
     --button="<span color='#25FF00' font_family='JetBrains Mono Nerd Font' font='20' weight='bold'>󰐎</span>:/bin/bash -c 'pause_mpv'" \
     --button="<span color='#25FF00' font_family='Material Icons' font='20' weight='bold'></span>:/bin/bash -c 'stop_mpv'" \
     --button="<span color='#E3E3E3' font_family='JetBrains Mono Nerd Font' font='12' weight='bold'>Volume</span>:/bin/bash -c 'change_volume'" \
     --button="<span color='#E3E3E3' font_family='JetBrains Mono Nerd Font' font='12' weight='bold'>Status</span>:/bin/bash -c 'mpv_check'" \
     --button="<span color='#E3E3E3' font_family='JetBrains Mono Nerd Font' font='12' weight='bold'>Track</span>:/bin/bash -c 'current_track'" \
     --button="<span color='#E3E3E3' font_family='JetBrains Mono Nerd Font' font='12' weight='bold'>URL</span>:/bin/bash -c 'current_url'" \
     --button="<span color='#E3E3E3' font_family='JetBrains Mono Nerd Font' font='12' weight='bold'>File</span>:/bin/bash -c 'radio_textfile'" \
     --button="<span color='#E3E3E3' font_family='JetBrains Mono Nerd Font' font='12' weight='bold'>Clip</span>:/bin/bash -c 'clipboard'" \
     --button="<span color='#E3E3E3' font_family='JetBrains Mono Nerd Font' font='12' weight='bold'>Log</span>:/bin/bash -c 'title_log'" \
     --button="<span color='#FF004B' font_family='Material Icons Outlined' font='20' weight='bold'></span>":1 
   fi

#----------------------------------------#
# Unset variables and functions
#----------------------------------------#

unset play_mpv
unset stop_mpv
unset choice
unset pause_mpv
unset current_track
unset current_url
unset radio_textfile
unset greeting
unset temp_file
unset clipboard
unset title_log
unset mpv_check
unset volume_file
unset change_volume
unset RADIO_FILE
unset MY_GTK_THEME
unset TEXT_EDITOR
