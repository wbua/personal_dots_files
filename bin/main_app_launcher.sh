#!/bin/bash

# Sway user preferences 
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

# Background color
MY_COLOR_BG="$COLOR_BG"

# Color icons
MY_COLOR_ICON="$COLOR_ICON"

# Bemenu background
MY_BEMENU_BG="$BEMENU_BACKGROUND"

   if [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == 'rofi' ]]; then
     rofi -show drun -config "$ROFI_THEME_CONFIG"
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == 'wofi' ]]; then
     wofi --show=drun -p ""
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "bemenu" ]]; then
     bemenu-run -l 8 -H 40 --fn "Monospace 18" --fb "$MY_BEMENU_BG" --ff "#cdd6f4" --nb "$MY_BEMENU_BG" --nf "#cdd6f4" --tb "$MY_BEMENU_BG" --hb "$MY_BEMENU_BG" \
     --tf "$MY_COLOR_ICON" --hf "$MY_COLOR_ICON" --af "#FFFFFF" --ab "$MY_BEMENU_BG"
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "fuzzel" ]]; then
     fuzzel
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == 'tofi' ]]; then
     tofi-drun | xargs -r swaymsg exec -- > /dev/null
   else
     :
   fi
	
