#!/bin/bash

dirs=( ~/Pictures/ )

_rofi() {

rofi -dmenu -i -p '' -kb-custom-2 "alt+c"
	
}
exit_status=$?

   if [[ $exit_status = "11" ]]; then
     recent_file=$(find "${dirs[@]}" \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -ctime 0 | sort | cut -d '/' -f4- | _rofi)	
     [ -z "$recent_file" ] && exit 0
     echo "$HOME"/"$recent_file" | xargs -0 -d '\n' display
   else
     recent_file=$(find "${dirs[@]}" \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -ctime 0 | sort | cut -d '/' -f4- | _rofi)	
     [ -z "$recent_file" ] && exit 0
     echo "$HOME"/"$recent_file" | xargs -0 -d '\n' xdg-open
   fi
