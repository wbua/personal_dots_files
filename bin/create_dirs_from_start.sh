#!/bin/bash

set -e

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

DOCS_DIR="$HOME/Documents"
PICS_DIR="$HOME/Pictures"
VIDS_DIR="$HOME/Videos"
MUSIC_DIR="$HOME/Music"
DOWNLOADS_DIR="$HOME/Downloads"

# Documents
if [ ! -d "$DOCS_DIR" ]; then
   mkdir -p "$DOCS_DIR"
fi

# Pictures
if [ ! -d "$PICS_DIR" ]; then
   mkdir -p "$PICS_DIR"
fi

# Videos
if [ ! -d "$VIDS_DIR" ]; then
   mkdir -p "$VIDS_DIR"
fi

# Music
if [ ! -d "$MUSIC_DIR" ]; then
   mkdir -p "$MUSIC_DIR"
fi

# Downloads
if [ ! -d "$DOWNLOADS_DIR" ]; then
   mkdir -p "$DOWNLOADS_DIR"
fi

# Text file store
if [ ! -d "$ALL_TEXT_STORE" ]; then
   mkdir -p "$ALL_TEXT_STORE"
fi

# Songs directory
if [ ! -d "$MAIN_SONGS_DIR" ]; then
   mkdir -p "$MAIN_SONGS_DIR"
fi

# Screencast directory
if [ ! -d "$MAIN_SCREENCAST_DIR" ]; then
   mkdir -p "$MAIN_SCREENCAST_DIR"
fi

# Screenshot directory
if [ ! -d "$MAIN_SCREENSHOT_DIR" ]; then
   mkdir -p "$MAIN_SCREENSHOT_DIR"
fi

# Audio recordings
if [ ! -d "$MAIN_AUDIO_DIR" ]; then
   mkdir -p "$MAIN_AUDIO_DIR"
fi

# PDF directory
if [ ! -d "$MAIN_PDF_DIR" ]; then
   mkdir -p "$MAIN_PDF_DIR"
fi

# QR Codes directory
if [ ! -d "$MAIN_QR_CODES_DIR" ]; then
   mkdir -p "$MAIN_QR_CODES_DIR"
fi

# Zips directory
if [ ! -d "$MAIN_ZIPS_DIR" ]; then
   mkdir -p "$MAIN_ZIPS_DIR"
fi

# Walllpapers directory
if [ ! -d "$MAIN_WALLPAPER_DIR" ]; then
   mkdir -p "$MAIN_WALLPAPER_DIR"
fi
