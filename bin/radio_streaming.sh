#!/usr/bin/env bash

# Program command: radio_streaming.sh
# Description: Plays online radio streams.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 28-12-2024
# Program_license: GPL 3.0
# Dependencies: yad, playerctl, mpv

# Master config file
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#---------------------------------------------------#
# Error checking
#---------------------------------------------------#

set -e

#---------------------------------------------------#
# User preferences
#---------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Qr Code directory
MY_QR_CODE_DIR="$MAIN_QR_CODES_DIR"
export MY_QR_CODE_DIR

#---------------------------------------------------#
# Create directories and functions
#---------------------------------------------------#

   if [ -d "$HOME"/Music ]; then
     :
   else
     mkdir "$HOME"/Music
   fi

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/.temqrcodes ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes
   fi

   if [ -f "$HOME"/Music/radio_links.txt ]; then
     :
   else
     touch "$HOME"/Music/radio_links.txt
   fi

   if [ -f "$HOME"/Music/.radio_rec_filename.txt ]; then
     :
   else
     touch "$HOME"/Music/.radio_rec_filename.txt
   fi

#---------------------------------------------------#
# Declare array
#---------------------------------------------------#

declare -a TABLE="($(sort < ~/Music/radio_links.txt))"

#---------------------------------------------------#
# Remove spaces
#---------------------------------------------------#

sed -i '/^[[:space:]]*$/d' ~/Music/radio_links.txt

#---------------------------------------------------#
# Add new radio station
#---------------------------------------------------#

add_station() {

killall yad
choice=$(GTK_THEME="$MY_GTK_THEME" yad --form \
--text-align="center" \
--text="\nAdd new radio station...\n" \
--borders=30 \
--width=600 \
--height=300 \
--separator="#" \
--field="Name: ":CE \
--field="Path: ":CE \
--button="Ok":0 \
--button="_Exit":1)
[[ -z "$choice" ]] && exit

field1=$(echo "$choice" | awk -F '#' '{print $1}')
field2=$(echo "$choice" | awk -F '#' '{print $2}')

echo "\"$field1\"" "\"$field2\"" | tee -a ~/Music/radio_links.txt

}
export -f add_station

#---------------------------------------------------#
# Record radio stream
#---------------------------------------------------#

record_radio() {

# Extract the current URL from playerctl and store it in a file
playerctl --player mpv metadata -f "{{xesam:url}}" | tee ~/Documents/.i3blocks_qp_rc_url.txt > /dev/null

# Kill the current mpv process
killall mpv

# Read the stored URL from the file
radiotrack=$(cat ~/Documents/.i3blocks_qp_rc_url.txt)

# Generate the filename for the recording
recording_filename="$HOME/Music/$(date +'%d-%m-%Y-%H%M%S').mp3"

# Record the stream using mpv
mpv --no-video --stream-record="$recording_filename" "$radiotrack"

# Store the path of the recorded file in a separate log file
echo "$recording_filename" > ~/Documents/.i3blocks_qp_rc_recording_path.txt

}
export -f record_radio

#---------------------------------------------------#
# Create Qr Code from track title
#---------------------------------------------------#

play_recording() {

killall mpv
rec_file_path=$(cat ~/Documents/.i3blocks_qp_rc_recording_path.txt)

   if [[ -f "$rec_file_path" ]]; then
     play_track=$(cat ~/Documents/.i3blocks_qp_rc_recording_path.txt)
     mpv "$play_track" --no-video
   else
     notify-send "File does not exist" "$rec_file_path"
   fi

}
export -f play_recording

#---------------------------------------------------#
# Current url
#---------------------------------------------------#

current_url() {

notify-send "$(playerctl --player mpv metadata -f "{{xesam:url}}")"

}
export -f current_url


#---------------------------------------------------#
# Current track title
#---------------------------------------------------#

current_track() {

notify-send "$(playerctl --player mpv metadata -f "{{xesam:title}}")"

}
export -f current_track

#---------------------------------------------------#
# Create Qr Code from track title
#---------------------------------------------------#

qr_code_from_track() {

   if [[ "$QR_CODE_DIR" == "disable" ]]; then
     notify-send "QR Code script has been disabled" && exit 1
   else
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     playerctl metadata --format '{{xesam:artist}}{{xesam:title}}' | \
     tr -d '\n' | \
     sed 's/"//g' | qrencode -m 5 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png     
     cp "$HOME"/Pictures/.temqrcodes/*.png "$MY_QR_CODE_DIR"
     find "$HOME"/Pictures/.temqrcodes/*.png -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw | \
     tee ~/Documents/.keys_player_title.txt
     notification_player=$(head ~/Documents/.keys_player_title.txt)
     qr_code_mnotify=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     notify-send -i "$qr_code_mnotify" "$notification_player"
   fi

}
export -f qr_code_from_track

#---------------------------------------------------#
# Play and pause toggle
#---------------------------------------------------#

play_and_pause() {

playerctl --player mpv play-pause

}
export -f play_and_pause

#---------------------------------------------------#
# Stop player
#---------------------------------------------------#

stop_player() {

killall yad
sleep 0.3
killall mpv

}
export -f stop_player

#---------------------------------------------------#
# Play radio station
#---------------------------------------------------#

play_station() {

radio_choice=$(echo "$@" | awk '{print $NF}')

   if [[ "$(pidof mpv)" || "$(pgrep -f mpv)" ]]; then
     killall mpv
     mpv "$radio_choice" --no-video
   else
     mpv "$radio_choice" --no-video
   fi

}
export -f play_station

#---------------------------------------------------#
# Radiorc
#---------------------------------------------------#

radiorc_info() {

killall yad
sleep 0.2
xdg-open ~/Music/radio_links.txt

}
export -f radiorc_info

#---------------------------------------------------#
# Yad dialog
#---------------------------------------------------#

printf '%s\n' "${TABLE[@]}" | GTK_THEME="$MY_GTK_THEME" yad --list \
 --title="Online Radio Player" \
 --text-align="center" \
 --width=950 \
 --height=500 \
 --column="Name" \
 --column="Url" \
 --borders=10 \
 --search-column=1 \
 --no-markup \
 --dclick-action='/bin/bash -c "play_station %s"' \
 --separator="|" \
 --button="Play:/bin/bash -c 'play_recording'" \
 --button="Record:/bin/bash -c 'record_radio'" \
 --button="QR Code:/bin/bash -c 'qr_code_from_track'" \
 --button="Radiorc:/bin/bash -c 'radiorc_info'" \
 --button="Url:/bin/bash -c 'current_url'" \
 --button="Song:/bin/bash -c 'current_track'" \
 --button="Add:/bin/bash -c 'add_station'" \
 --button="Play-pause:/bin/bash -c 'play_and_pause'" \
 --button="Stop:/bin/bash -c 'stop_player'" \
 --button="Exit":1

[[ -z "$selected_station" ]] && exit

#---------------------------------------------------#
# Unset variables and functions
#---------------------------------------------------#

unset play_radio
unset add_station
unset stop_player
unset play_and_pause
unset play_station
unset MY_GTK_THEME
unset radiorc_info
unset qr_code_from_track
unset MY_QR_CODE_DIR
unset record_radio
unset play_recording
unset current_url
unset current_track
