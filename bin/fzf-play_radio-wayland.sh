#!/bin/bash

killall mpv
killall ffplay
choice=$(cat "$HOME"/Documents/.current_url.txt | awk '{print $NF}')
mpv "$choice"
