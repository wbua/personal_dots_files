#!/bin/bash

# Program command: wel2.sh
# Description: Welcome app.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 17-07-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad

# Example welcome app
# You first must create a autostart file.

#-------------------------------------------------------#
# Create directories and files
#-------------------------------------------------------#

# Create xmonad directory
   if [ -d "$HOME"/.xmonad ]; then
     :
   else
     mkdir "$HOME"/.xmonad
   fi
   
# Create scripts directory
   if [ -d "$HOME"/.xmonad/scripts ]; then
     :
   else
     mkdir "$HOME"/.xmonad/scripts
   fi

# Create xmonad autostart file
   if [ -f "$HOME"/.xmonad/scripts/autostart.sh ]; then
     :
   else
        touch "$HOME"/.xmonad/scripts/autostart.sh
   fi

#-------------------------------------------------------#
# Location of autostart file
#-------------------------------------------------------#

my_autostart_file="$HOME/.xmonad/scripts/autostart.sh"
export my_autostart_file

#-------------------------------------------------------#
# Checks files
#-------------------------------------------------------#

check_file() {

    if [[ "$(grep -o "^[#]wel2.*$" "$my_autostart_file")" == *"#"* ]]; then
    echo "<span color='#FF0021' font_family='Inter' weight='bold' font='12'>off</span>"
    else
    echo "<span color='#30FF00' font_family='Inter' weight='bold' font='12'>on</span>"
    fi
	
}
export -f check_file

#-------------------------------------------------------#
# Creates files
#-------------------------------------------------------#

create_file() {
	
	killall yad
	if [[ "$(grep -o "^[#]wel2.*$" "$my_autostart_file")" == *"#"* ]]; then
	sed -i '16s|^.*$|wel2.sh \&|' "$my_autostart_file"
	else
	sed -i '16s|^.*$|#wel2.sh \&|' "$my_autostart_file"
	fi
	wel2.sh
	
}
export -f create_file

#--------------------------------------------------------------------#
# Example function
#--------------------------------------------------------------------#

example_function() {
	
notify-send "item 9"
	
}
export -f example_function

#--------------------------------------------------------------------#
# Random numbers
#--------------------------------------------------------------------#

KEY=$RANDOM

#--------------------------------------------------------------------#
# Yad settings
#--------------------------------------------------------------------#

# Page 1
GTK_THEME="alt-dialog" yad --plug=$KEY --tabnum=1 --form --width=900 --height=200 --image-on-top --center --columns=3 \
--text-align="center" \
--align="center" \
--field="":LBL "" \
--field="<b>item1</b>":fbtn "bash -c ''" \
--field="<b>item 2</b>":fbtn "bash -c ''" \
--field="":LBL "" \
--field="<b><span color='#ffffff' font_family='Ubuntu Mono' weight='medium' font='15'>item 9</span></b>":fbtn '/bin/bash -c "example_function"' \
--field="<span color='#FD4C5E' font_family='Inter' font='16' weight='bold'>Welcome to Xmonad</span>\n:LBL" '' \
--field="<b>item 8</b>":fbtn "bash -c ''" \
--field="<b>item 5</b>":fbtn "bash -c ''" \
--field="<b>item 6</b>":fbtn "bash -c ''" \
--field="":LBL "" \
--field="":LBL "" \
--field="<b>item 4</b>":fbtn "bash -c ''" \
--field="<b>item 7</b>":fbtn "bash -c ''" \
--field="":LBL "" \
--field="<b>item 10</b>":fbtn "bash -c ''" &

# Page 2
GTK_THEME="alt-dialog" yad --plug=$KEY --tabnum=2 --form --width=900 --height=200  --columns=3 --text="" \
--field="<b>item 8</b>":fbtn "bash -c ''" \
--field="<b>item 5</b>":fbtn "bash -c ''" \
--field="<b>item 6</b>":fbtn "bash -c ''" &

# Notebook 
GTK_THEME="alt-dialog" yad --notebook --key=$KEY --buttons-layout="center" --center --borders=10 --width=900 --height=450 --text="" \
--tab="<span color='#ffffff' font_family='Inter' font='12' weight='bold'>Main</span>" \
--tab="<span color='#ffffff' font_family='Inter' font='12' weight='bold'>Links</span>" \
--button="<span color='#ffffff' font_family='Inter' font='11'>$(check_file)</span>:/bin/bash -c 'create_file'" \
--button="Exit":1 &

#--------------------------------------------------------------------#
# Unset variables and function
#--------------------------------------------------------------------#

unset check_file
unset my_autostart_file
unset create_file
unset example_function
