#!/bin/bash

choice=$(swaymsg -t get_workspaces | jq '.[] | .num' | xargs)

   if [[ ! "$choice" == *"1"* ]]; then
     swaymsg workspace 1
   elif [[ ! "$choice" == *"2"* ]]; then
     swaymsg workspace 2
   elif [[ ! "$choice" == *"3"* ]]; then
     swaymsg workspace 3
   elif [[ ! "$choice" == *"4"* ]]; then
     swaymsg workspace 4
   elif [[ ! "$choice" == *"5"* ]]; then
     swaymsg workspace 5
   elif [[ ! "$choice" == *"6"* ]]; then
     swaymsg workspace 6
   elif [[ ! "$choice" == *"7"* ]]; then
     swaymsg workspace 7
   elif [[ ! "$choice" == *"8"* ]]; then
     swaymsg workspace 8
   elif [[ ! "$choice" == *"9"* ]]; then
     swaymsg workspace 9
   elif [[ ! "$choice" == *"0"* ]]; then
     swaymsg workspace 0
   elif [[ ! "$choice" == *"11"* ]]; then
     swaymsg workspace 11
   elif [[ ! "$choice" == *"12"* ]]; then
     swaymsg workspace 12
   else
     :
   fi
