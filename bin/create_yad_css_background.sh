#!/bin/bash

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Color config
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

set -e

MY_WALLPAPER="$MAIN_SWAY_WALLPAPER"

   if [ -d "$HOME"/Pictures/.yad_css_background ]; then
     :
   else
     mkdir "$HOME"/Pictures/.yad_css_background
   fi

YAD_DIR="$HOME/Pictures/.yad_css_background"   


rm -f "$YAD_DIR"/*.* || exit 1

setsid convert "$MY_WALLPAPER" -quality 50 -fill black -colorize 80% "$YAD_DIR"/yad_background.jpg >/dev/null 2>&1 & disown

