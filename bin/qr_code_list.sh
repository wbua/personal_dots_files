#!/bin/bash

# Program command: qr_code_list.sh
# Description: Decodes all qr codes in directory and displays it in yad.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 15-10-2024
# Program_license: GPL 3.0
# Dependencies: yad, wl-clipboard, zbar-tools, qrencode, grim

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-------------------------------------------------------#
# General variables different on each system
#-------------------------------------------------------#

# Browser
MY_BROWSER="$MAIN_BROWSER"
export MY_BROWSER

# File manager
FILE_MANAGER="$MAIN_FILE_MANAGER"
export FILE_MANAGER

# QR Codes directory
QR_CODES_DIR="$MAIN_QR_CODES_DIR"
export QR_CODES_DIR

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#-------------------------------------------------------#
# Checks if you are running a wayland session
#-------------------------------------------------------#

  if [[ "$(echo $WAYLAND_DISPLAY)" == "" ]]; then
     yad --error \
     --width 500 \
     --title "Wayland session not found!" \
     --text "Cannot run qr code app, wayland only tool!"
     exit 1
   else
     :
   fi

#-------------------------------------------------------#
# Colors for text and buttons (pango markup)
#-------------------------------------------------------#

# Icons for main buttons
color1="<span color='#25FF00' font_family='Material Icons' font='25'>"
export color1
# Help page title
color2="<span color='#FFA503' font_family='Monospace' font='14' weight='bold'>"
export color2
# Help page and text info highlighted text
color3="<span color='#FF00FE' font_family='Monospace' font='14' weight='bold'>"
export color3
# Help page icons
color5="<span color='#25FF00' font_family='Material Icons' font='25'>"
export color5
# Help page button definition text
color6="<span color='#FFFFFF' font_family='Monospace' font='14' rise='8pt'>"
export color6
# Help page general text
color7="<span color='#FFFFFF' font_family='Monospace' font='14' rise='0pt'>"
export color7
# Close button icon
color8="<span color='#FF0020' font_family='Material Icons' font='25' rise='0pt'>"
export color8
# Option buttons
color9="<span color='#FFFFFF' font_family='Material Icons' font='25'>"
export color9
# Action buttons
color10="<span color='#F89E3F' font_family='Material Icons' font='25'>"
export color10
# Ends color
end="</span>"
export end

#-------------------------------------------------------#
# Creates files and directories
#-------------------------------------------------------#

   # Create bin directory
   if [ -d "$HOME"/bin ]; then
     :
   else
     mkdir "$HOME"/bin
   fi

   # Create qr codes directory
   if [ "$QR_CODES_DIR" == "disable" ]; then
     :
   elif [ -d "$QR_CODES_DIR" ]; then
     :
   else
     notify-send "Qr Codes directory does not exist!" && exit 1
   fi

   # Create pictures directory
   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi
   
   # Create documents directory
   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   # Temporary qr code directory
   if [ -d "$HOME"/Pictures/.temqrcodes/ ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes/
   fi

   # No qr codes in main directory png file
   if [ -d "$HOME"/Pictures/.qr_codes_empty ]; then
     :
   else
     mkdir "$HOME"/Pictures/.qr_codes_empty
   fi

   # Qr code filesize is too big
   if [ -d "$HOME"/Pictures/.qr_codes_badsize ]; then
     :
   else
     mkdir "$HOME"/Pictures/.qr_codes_badsize
   fi

   # Creates qr code text file
   if [ -f "$HOME"/Pictures/.qrcode_dir.txt ]; then
     :
   else
     touch "$HOME"/Pictures/.qrcode_dir.txt
   fi

   # Creates qr code hex color
   if [ -f "$HOME"/Documents/.hex_color.txt ]; then
     :
   else
     touch "$HOME"/Documents/.hex_color.txt
   fi
  
   # Decode qr code OCR directory 
   if [ -d "$HOME"/Pictures/.ocr/ ]; then
     :
   else
     mkdir "$HOME"/Pictures/.ocr/
   fi

#-------------------------------------------------------#
# Changing qr code color text file
#-------------------------------------------------------#

   if [[ -z "$(cat "$HOME"/Documents/.hex_color.txt)" ]]; then
     echo "000000" > "$HOME"/Documents/.hex_color.txt
   else
     :
   fi   

#-------------------------------------------------------#
# Qr code color text file
#-------------------------------------------------------#

hex_color=$(cat "$HOME"/Documents/.hex_color.txt)
export hex_color

#-------------------------------------------------------#
# Yad settings menu
#-------------------------------------------------------#

_settings_menu() {

GTK_THEME="$MY_GTK_THEME" yad --form --column="search" --width=500 --height=500 --columns=2 --borders=20 \
--field="<b><span color='#5EFF00' font_family='Monospace' font='14'>QR Code color</span></b>":fbtn '/bin/bash -c "_change_hex_color"' \
--button="${color8}${end}":1

}
export -f _settings_menu

#-------------------------------------------------------#
# Yad search settings
#-------------------------------------------------------#

_yad2() {

GTK_THEME="$MY_GTK_THEME" yad --list --column="search" \
--width=1200 --height=500 \
--image="$(_current_image)" \
--center --no-headers --separator= \
--button="${color1}${end}":90 \
--button="${color1}${end}":30 \
--button="${color1}${end}":44 \
--button="${color1}${end}":60 \
--button="${color1}${end}":20 \
--button="${color1}${end}":68 \
--button="${color1}${end}":74 \
--button="${color8}${end}":1

ret=$?
echo $ret
	
}
export -f _yad2

#-------------------------------------------------------#
# Yad main settings
#-------------------------------------------------------#

_yad() {

   if [[ "$QR_CODES_DIR" == "disable" ]]; then
     notify-send "QR Codes script has been disabled" && exit 1
   else  
     GTK_THEME="$MY_GTK_THEME" yad --list --search-column=1 --column="list" --regex-search \
     --width=1300 --height=500 \
     --no-headers --separator= --buttons-layout="center" \
     --borders=10 --title="QR Code Creator" \
     --image="$(_current_image)" \
     --text="$(_current_text)" \
     --button="${color1}${end}":15 \
     --button="${color1}${end}":55 \
     --button="${color1}${end}":95 \
     --button="${color9}${end}":35 \
     --button="${color9}${end}":70 \
     --button="${color9}${end}":10 \
     --button="${color9}${end}":74 \
     --button="${color9}${end}":84 \
     --button="${color10}${end}":90 \
     --button="${color10}${end}":60 \
     --button="${color10}${end}":44 \
     --button="${color10}${end}":20 \
     --button="${color10}${end}":66 \
     --button="${color10}${end}":30 \
     --button="${color8}${end}":1
   fi

ret2=$?
echo $ret2

}
export -f _yad

#-------------------------------------------------------#
# Help page
#-------------------------------------------------------#

_help_info() {

info_text="${color2}Help information${end}

${color7}This program decodes all QR Codes in the QR Codes directory.${end}
${color7}It then shows them in this program, using a text file.${end}
${color7}QR Codes can only be on one line.${end}

${color2}How to use${end}

${color7}If searching google and opening url do not work, then change browser.${end}
${color7}If qr code does not open in file manager, then change file manager.${end}

${color2}Buttons${end}

${color5}${end} ${color6}Create qr code from clipboard contents.${end}
${color5}${end} ${color6}Create qr code from typed text in entry box.${end}
${color5}${end} ${color6}Selects qr code on screen, and creates qr code (OCR)${end}
${color5}${end} ${color6}Change your file manager. Change your browser. Change Qr Code color.${end}
${color5}${end} ${color6}Show help page.${end}
${color5}${end} ${color6}Click on text once, and then press the file button. Show selected qr code in file manager.${end}
${color5}${end} ${color6}Click on text once, and then press the google button. This will do a google search.${end}
${color5}${end} ${color6}This will do a text search on file.${end}
${color5}${end} ${color6}Click on text once, and then press the copy button. Will copy qr code png file to clipboard.${end}
${color5}${end} ${color6}Click on text once, and then press the url button. Opens url in browser.${end}
${color5}${end} ${color6}Click on text once, and then press the select button. Shows qr code image in main display.${end}
${color5}${end} ${color6}Click on text once, and then press the clip button. Sends text to clipboard.${end}
${color5}${end} ${color6}Kills yad.${end}

${color2}About${end}

${color7}Program created by ${color3}John Mcgrath${end}${end}
"
export info_text
killall yad
echo "$info_text" | GTK_THEME="$MY_GTK_THEME" yad --list --column="help" --height=500 --width=1200 --center \
--no-headers --separator= \
--button="${color8}${end}":1

}
export -f _help_info

#-------------------------------------------------------#
# Change qr code color (hex code)
#-------------------------------------------------------#

_change_hex_color() {

killall yad
GTK_THEME="$MY_GTK_THEME" yad --entry --center --width=400 --height=300 \
--text="Change QR Code hex color\n example: 00FF4C\nDo not put the # before code" | tee "$HOME"/Documents/.hex_color.txt

}
export -f _change_hex_color

#-------------------------------------------------------#
# Enter text to create Qr Code
#-------------------------------------------------------#

_text_qrcode() {

     killall yad
     entry_box=$(GTK_THEME="$MY_GTK_THEME" yad --entry --width=500 --height=200 \
     --text="${color3}Enter text to create QR Code${end}" \
     --button="${color8}${end}":1)
     export entry_box
   if [[ -z "$entry_box" ]]; then
     exit 0
   else
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     echo "$entry_box" | tr -d '\n' | \
     qrencode -m 5 -l L --foreground="$hex_color" -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$QR_CODES_DIR"
     sleep 0.2
   fi

}
export -f _text_qrcode

#-------------------------------------------------------#
# Create qr code from clipboard
#-------------------------------------------------------#

_create_qrcode() {

   if [[ -z "$(wl-paste)" ]]; then
     :
   else
   killall yad
   rm -f "$HOME"/Pictures/.temqrcodes/*.png
   wl-paste -n | sed 's|file://||' | tr -d '\n' | tr -d '\r' | \
   qrencode -m 5 -l L --foreground="$hex_color" -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
   cp "$HOME"/Pictures/.temqrcodes/*.png "$QR_CODES_DIR"
   sleep 0.2
   fi

}
export -f _create_qrcode

#-------------------------------------------------------#
# Select qr code png filename to display image in main yad window
#-------------------------------------------------------#

_select_image() {

   if [[ "$choice" == '74' ]]; then
     :
   else
     image_display=$(echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | awk '{print $1}')
     export image_display
     killall yad
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     cp "$QR_CODES_DIR"/"$image_display" "$HOME"/Pictures/.temqrcodes/
     sleep 0.2
   fi

}
export -f _select_image

#-------------------------------------------------------#
# Select qr code png filename to display image in search window
#-------------------------------------------------------#

_select_image_search() {

   if [[ "$choice" == '74' ]]; then
     :
   else
     image_display=$(echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | awk '{print $1}')
     export image_display
     killall yad
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     cp "$QR_CODES_DIR"/"$image_display" "$HOME"/Pictures/.temqrcodes/
     sleep 0.2
     _main2
   fi

}
export -f _select_image_search

#-------------------------------------------------------#
# Last qr code png created (displays in main yad window)
#-------------------------------------------------------#

_current_image() {

   if [[ -z "$(ls -A "$QR_CODES_DIR")" ]]; then
     find "$HOME"/Pictures/.qr_codes_empty/ -type f -print0 | xargs -0 ls -tr | tail -n 1
   elif [[ -z "$(ls -A "$HOME"/Pictures/.temqrcodes)" ]]; then
     find "$HOME"/Pictures/.qr_codes_badsize/ -type f -print0 | xargs -0 ls -tr | tail -n 1
   else
     find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1
   fi

}
export -f _current_image

#-------------------------------------------------------#
# Show the current qr code filename in main window
#-------------------------------------------------------#

_current_text() {

   if [[ -z "$(ls -A "$QR_CODES_DIR")" ]]; then
     find "$HOME"/Pictures/.qr_codes_empty/ -type f -print0 | xargs -0 ls -tr | tail -n 1
   elif [[ -z "$(ls -A "$HOME"/Pictures/.temqrcodes)" ]]; then
     echo "${color3}Current QR Code: $(find "$HOME"/Pictures/.qr_codes_badsize/ -type f -print0 | \
     xargs -0 ls -tr | tail -n 1 | cut -d '/' -f6)${end}" | sed "s/\&/\&amp;/g" 
   else
     echo "${color3}Current QR Code: $(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | \
     xargs -0 ls -tr | tail -n 1 | cut -d '/' -f6)${end}"
     echo "${color2}$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | \
     xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw)${end}" | sed "s/\&/\&amp;/g" 
   fi

}
export -f _current_text

#-------------------------------------------------------#
# Go to browser after google search 
#-------------------------------------------------------#

goto_browser() {
     
   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     search="https://www.google.co.uk/search?q={}"
     export search
     google_search_choice=$(echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | \
     awk '{$1="";print $0}' | sed "s|&amp;||")
     echo "$google_search_choice" | xargs -d '\n' -I{} "$MY_BROWSER" "$search"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     search="https://www.google.co.uk/search?q={}"
     export search
     echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | \
     awk '{$1="";print $0}' | sed "s|&amp;||" | xargs -d '\n' -I{} "$MY_BROWSER" "$search"
   fi
	
}
export -f goto_browser

#-------------------------------------------------------#
# Google search
#-------------------------------------------------------#

_google_search() {

   if [[ "$choice" == '60' ]]; then
     :
   else
     goto_browser
   fi

}
export -f _google_search

#-------------------------------------------------------#
# Go to browser after opening link
#-------------------------------------------------------#

goto_link() {

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | \
     awk '{$1="";print $0}' | sed "s/\&amp;/\&/" | awk '{print $NF}' | xargs -d '\n' "$MY_BROWSER"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | \
     awk '{$1="";print $0}' | sed "s/\&amp;/\&/" | awk '{print $NF}' | xargs -d '\n' "$MY_BROWSER"
   fi
	
}
export -f goto_link

#-------------------------------------------------------#
# Query link search
#-------------------------------------------------------#

query_link_search() {

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     killall yad
     query_path_search=$(cat ~/Documents/.qr_code_list_query | tr -d '\n')
     yad_query=$(GTK_THEME="$MY_GTK_THEME" yad --entry --width=800 --height=200 --borders=10 --buttons="Ok":0 --buttons="Exit":1)
     [ -z "$yad_query" ] && exit 0
     query_combine_choice=$(echo "${query_path_search}""${yad_query}")
     "$MY_BROWSER" "$query_combine_choice"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     killall yad
     query_path_search=$(cat ~/Documents/.qr_code_list_query | tr -d '\n')
     yad_query=$(GTK_THEME="$MY_GTK_THEME" yad --entry --width=800 --height=200 --borders=10 --buttons="Ok":0 --buttons="Exit":1)
     [ -z "$yad_query" ] && exit 0
     query_combine_choice=$(echo "${query_path_search}""${yad_query}")
     "$MY_BROWSER" "$query_combine_choice"
   else
     :
   fi
   
}
export -f query_link_search

#-------------------------------------------------------#
# Check link type
#-------------------------------------------------------#

check_link_type() {

echo "$choice" | sed 's/[0-9]\+$//' | \
awk '{$1="";print $0}' | tee ~/Documents/.qr_code_list_query
link_choice=$(echo "$choice" | sed 's/[0-9]\+$//' | \
awk '{$1="";print $0}')

   if [[ "$link_choice" = *"http"* && "$link_choice" =~ "="$ ]]; then
    query_link_search
   else
    goto_link
   fi
   	
}
export -f check_link_type

#-------------------------------------------------------#
# Open url in browser
#-------------------------------------------------------#

_open_url() {

   if [[ "$choice" == '20' ]]; then
     :
   else
     check_link_type
   fi

}
export -f _open_url

#-------------------------------------------------------#
# Send text to clipboard
#-------------------------------------------------------#

_text_clip() {

   if [[ "$choice" == '30' ]]; then
     :
   else
     echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | \
     awk '{$1="";print $0}' | sed 's| ||' | wl-copy -n
   fi

}
export -f _text_clip

#-------------------------------------------------------#
# Select qr code filename to open in file manager
#-------------------------------------------------------#

_select_file() {

   if [[ "$choice" == '90' ]]; then
     :
   else
     GTK_THEME="Yaru"
     killall yad
     chosen=$(echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | awk '{print $1}')
     export chosen
     [ -z "$chosen" ] && exit 0
     "$FILE_MANAGER" "$QR_CODES_DIR"/"$chosen"
   fi

}
export -f _select_file

#-------------------------------------------------------#
# Select qr code and copy png to clipboard
#-------------------------------------------------------#

_image_to_clip() {

killall yad

  if [[ "$choice" == '44' ]]; then
    :
  else
    echo "$QR_CODES_DIR"/"$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | \
    awk '{print $1}' | xargs -d '\n' | tee ~/Documents/.qrcode_list_copy_png.txt
    copy_png=$(cat ~/Documents/.qrcode_list_copy_png.txt | xargs -0 basename)
    wl-copy --type image/png < "$QR_CODES_DIR"/"$copy_png"    
  fi

}
export -f _image_to_clip

#-------------------------------------------------------#
# This will do a text search on file
#-------------------------------------------------------#

_file_search() {

killall yad
choice=$(GTK_THEME="$MY_GTK_THEME" yad --entry --width=800 --height=200 \
--no-headers --separator= \
--text="${color3}Type text to search${end}" \
--button="${color8}${end}":1)
launch=$(grep -i "$choice" "$HOME"/Pictures/.qrmixed.txt | tee "$HOME"/Documents/.qrcode_grep.txt /dev/null)
export launch
[ -z "$choice" ] && exit 0 || echo "$launch" ; _main2
  
}
export -f _file_search

#-------------------------------------------------------#
# Select qr code on screen to create using OCR
#-------------------------------------------------------#

_ocr_select() {

killall yad
 
   if [[ -z "$(ls -A "$HOME"/Pictures/.ocr/)" ]]; then
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     rm -f "$HOME"/Pictures/.ocr/*.png
     grimshot save area ~/Pictures/.ocr/ocr.png
     find "$HOME"/Pictures/.ocr/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw | \
     tr -d '\n' | qrencode -m 5 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$QR_CODES_DIR"
     qr_code_list.sh
   else
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     rm -f "$HOME"/Pictures/.ocr/*.png
     grimshot save area ~/Pictures/.ocr/ocr.png
     find "$HOME"/Pictures/.ocr/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw | \
     tr -d '\n' | qrencode -m 5 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$QR_CODES_DIR"
     qr_code_list.sh
   fi

}
export -f _ocr_select

#-------------------------------------------------------#
# Open file and directory path
#-------------------------------------------------------#

_open_file_path() {

launch_file_dir=$(echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | \
awk '{$1="";print $0}' | sed "s/\&amp;/\&/" | sed 's| ||')
echo "$launch_file_dir" | tee ~/Documents/.qrcode_list_file_dir.txt
list_file_dir=$(cat ~/Documents/.qrcode_list_file_dir.txt)

   if [[ "$choice" == '66' ]]; then
     :
   elif [[ -f "$list_file_dir" ]]; then
     xdg-open "$list_file_dir"
   elif [[ -d "$list_file_dir" ]]; then
     "$FILE_MANAGER" "$list_file_dir"
   else
     notify-send "Path does not exist!"
   fi

}
export -f _open_file_path

#-------------------------------------------------------#
# Delete qr code
#-------------------------------------------------------#

_delete_qrcode() {

delete_choice=$(echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | awk '{print $1}')
rm "$QR_CODES_DIR"/"$delete_choice" || exit 1
	
}
export -f _delete_qrcode

#-------------------------------------------------------#
# Text file where all qr codes text is sent
#-------------------------------------------------------#

file_dir="$HOME/Pictures/.qrcode_dir.txt"
export file_dir

#-------------------------------------------------------#
# Decodes all qr code png's in directory to text file
#-------------------------------------------------------#

decode_qrcode_dir=$(find "$QR_CODES_DIR" -maxdepth 1 -iname '*.*' -printf '%T@ %p\n' | \
sort -rn | cut -f2- -d" ")
export decode_qrcode_dir
echo "$decode_qrcode_dir" | tee ~/Documents/all_qrcodes_decoded.txt
all_qrcodes_decoded=$(cat ~/Documents/all_qrcodes_decoded.txt)
export all_qrcodes_decoded 
echo "$all_qrcodes_decoded" | xargs zbarimg -q --raw > "$file_dir"

#-------------------------------------------------------#
# Removes whitespace from text file
#-------------------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$file_dir"

#-------------------------------------------------------#
# Send all filenames in qr codes directory to text file
#-------------------------------------------------------#

decode_filenames_dir=$(find "$QR_CODES_DIR" -maxdepth 1 -iname '*.*' -printf '%T@ %p\n' | \
sort -rn | cut -f2- -d" " | xargs -n 1 basename)
export decode_filenames_dir
echo "$decode_filenames_dir" > "$HOME"/Pictures/.qrfilenames.txt

#-------------------------------------------------------#
# Merge text files
#-------------------------------------------------------#

paste "$HOME"/Pictures/.qrfilenames.txt "$HOME"/Pictures/.qrcode_dir.txt > "$HOME"/Pictures/.qrmixed.txt

#-------------------------------------------------------#
# Removes special characters
#-------------------------------------------------------#

#sed -i 's|<||g' "$HOME"/Pictures/.qrmixed.txt
#sed -i 's|&|+|g' "$HOME"/Pictures/.qrmixed.txt

#-------------------------------------------------------#
# Search main
#-------------------------------------------------------#

_main2() {

launch=$(cat "$HOME"/Documents/.qrcode_grep.txt)
export launch
choice=$(echo "$launch" | _yad2)
export choice

   # Open selected text in browser
   if [[ "${choice:0-2}" == *"60"* ]]; then
     _google_search
   # Open selected text in browser
   elif [[ "${choice:0-2}" == *"20"* ]]; then
     _open_url
   # Select qr code png filename and copy to clipboard
   elif [[ "${choice:0-2}" == *"44"* ]]; then
     _image_to_clip
   # Send selected text to clipboard
   elif [[ "${choice:0-2}" == *"30"* ]]; then
     _text_clip
   # Select qr code filename to open in file manager
   elif [[ "${choice:0-2}" == *"90"* ]]; then
     _select_file
   elif [[ "${choice:0-2}" == *"74"* ]]; then
     _select_image_search
   elif [[ "${choice:0-2}" == *"68"* ]]; then
     _open_file_path
   else
    :
   fi

}
export -f _main2

#-------------------------------------------------------#
# Main
#-------------------------------------------------------#

_main() {

launch=$(cat "$HOME"/Pictures/.qrmixed.txt)
export launch
choice=$(echo "$launch" | sed "s/\&/\&amp;/g" | _yad)
export choice

   # Open selected text in browser
   if [[ "${choice:0-2}" == *"20"* ]]; then
     _open_url
   # Send selected text to clipboard
   elif [[ "${choice:0-2}" == *"30"* ]]; then
     _text_clip
   # Select text for a google search
   elif [[ "${choice:0-2}" == *"60"* ]]; then
     _google_search
   # Help page
   elif [[ "${choice:0-2}" == *"70"* ]]; then
     _help_info
   # Select qr code filename to open in file manager
   elif [[ "${choice:0-2}" == *"90"* ]]; then
     _select_file
   # Settings menu
   elif [[ "${choice:0-2}" == *"35"* ]]; then
     _settings_menu
   # File search contents of text file
   elif [[ "${choice:0-2}" == *"10"* ]]; then
     _file_search
   # Select qr code png filename and copy to clipboard
   elif [[ "${choice:0-2}" == *"44"* ]]; then
     _image_to_clip
   # Select qr code png filename to display image in main yad window
   elif [[ "${choice:0-2}" == *"74"* ]]; then
     _select_image
   # Delete qr code
   elif [[ "${choice:0-2}" == *"84"* ]]; then
     _delete_qrcode
   # Creates qr code from clipboard contents
   elif [[ "${choice:0-2}" == *"15"* ]]; then
     _create_qrcode
   # Create qr code from text typed in entry box
   elif [[ "${choice:0-2}" == *"55"* ]]; then
     _text_qrcode
   # Select screenshot QCR
   elif [[ "${choice:0-2}" == *"95"* ]]; then
     _ocr_select
   # Open file path
   elif [[ "${choice:0-2}" == *"66"* ]]; then
     _open_file_path
   else
     :
   fi

}
export -f _main
_main

#-------------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------------#

# Functions
unset _yad
unset _yad2
unset _open_url
unset _text_clip
unset _google_search
unset _help_info
unset _select_file
unset _main
unset _main2
unset _image_to_clip
unset _current_image
unset _select_image
unset _create_qrcode
unset _text_qrcode
unset _file_search
unset _maim_select
unset _select_image_search
unset _settings_menu
unset _change_hex_color
unset _current_text
unset _open_file_path
unset _delete_qrcode

# Variables
unset file_dir
unset choice
unset MY_BROWSER
unset color1
unset color2
unset color3
unset color5
unset color6
unset color7
unset color8
unset color9
unset color10
unset end
unset open_smplayer
unset launch
unset info_text
unset FILE_MANAGER
unset chosen
unset vplayer
unset search
unset search2
unset send_notify
unset youtube_search
unset qr_notify
unset image_display
unset search_box
unset yad_choice
unset hex_color
unset decode_qrcode_dir
unset all_qrcodes_decoded
unset decode_filenames_dir
unset QR_CODES_DIR
unset MY_GTK_THEME
unset goto_browser
unset goto_link
unset check_link_type
unset query_link_search
