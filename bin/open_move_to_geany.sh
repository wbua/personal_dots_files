#!/bin/bash

     active_app=$(swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class):\(.name)"' \
     | grep -i "geany" | cut -d: -f2)
 
   if [[ "$active_app" == *"Geany"* || "$active_app" == *"geany"* ]]; then
     swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class):\(.name)"' \
     | grep -i "geany" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' focus
   else
     /bin/bash -c "geany"
   fi 
