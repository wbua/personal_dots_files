#!/bin/bash

# Sway user preferences 
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Create directories

   if [ -d "$HOME"/Downloads ]; then
     :
   else
     mkdir "$HOME"/Downloads
   fi

   if [ -d "$HOME"/Downloads/.temp_weather ]; then
     :
   else
     mkdir "$HOME"/Downloads/.temp_weather
   fi   

   if [ -f "$HOME"/Downloads/.temp_weather/downloadedfile ]; then
     :
   else
     touch "$HOME"/Downloads/.temp_weather/downloadedfile
   fi

#---------------------------------------------------------#
# BBC weather
#---------------------------------------------------------#

# Weather api key
WEATHER_API_KEY="$MAIN_WEATHER_API_KEY"

# Download weather information at startup
rm -rf "$HOME"/Downloads/.temp_weather/* || exit 1
wget https://weather-broker-cdn.api.bbci.co.uk/en/forecast/rss/3day/"$WEATHER_API_KEY" -O "$HOME"/Downloads/.temp_weather/downloadedfile

#---------------------------------------------------------#
# Wttr weather
#---------------------------------------------------------#

# Weather api key
WTTR_API_KEY="$MAIN_WTTR_API_KEY"

# Download weather information at startup
curl wttr.in/"$WTTR_API_KEY"?format="%C%t\n" | tee ~/Downloads/.wttr_temp.txt

# Download forecast for many days for i3blocks weather dialog
curl wttr.in/"$WTTR_API_KEY" | tee ~/Documents/.weather_file.rtf
