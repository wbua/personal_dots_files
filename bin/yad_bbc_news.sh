#!/bin/bash

# Program command: yad_bbc_news.sh
# Description: BBC world news rss feed.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 08-08-2024
# Program_license: GPL 3.0
# Dependencies: yad, libxml2-utils, curl
# Website: https://gitlab.com/52521/mydots

# Yad settings app
# shellcheck source=/dev/null
source ~/bin/sway_user_preferences.sh

#-----------------------------------------------------#
# User preferences
#-----------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Browser
BROWSER="$MAIN_BROWSER"
export BROWSER

#-----------------------------------------------------#
# Create files and directories
#-----------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -f "$HOME"/Documents/.news.html ]; then
     :
   else
     touch "$HOME"/Documents/.news.html
   fi

#-----------------------------------------------------#
# BBC rss feed text file
#-----------------------------------------------------#

bbc_rss=$(cat "$HOME"/Documents/.news.html)
export bbc_rss

#-----------------------------------------------------#
# Download new copy of BBC rss news feed XML
#-----------------------------------------------------#

update_feed() {

killall yad
curl --silent https://feeds.bbci.co.uk/news/rss.xml | tee "$HOME"/Documents/.news.html

}
export -f update_feed

#-----------------------------------------------------#
# Processes the XML file into readable format
#-----------------------------------------------------#

# Rss titles
rss_title=$(echo "$bbc_rss" | \
xmllint --xpath '/rss/channel/item[*]/title/text()' - | \
sed -e 's/\!\[CDATA\[//' -e 's/\]\]//' | tr -d '<>,')
export rss_title

# Rss link
rss_links=$(echo "$bbc_rss" | \
xmllint --xpath '/rss/channel/item[*]/link/text()' - | \
sed -e 's/\!\[CDATA\[//' -e 's/\]\]//' | tr -d '<>,')
export rss_links

#-----------------------------------------------------#
# Creates text file with titles and links and merges them
#-----------------------------------------------------#

# Creates text file containing title headlines
echo "$rss_title" | tee ~/Documents/.news_titles.txt

# Creates text file containing links
echo "$rss_links" | tee ~/Documents/.news_links.txt

# Combines titles and links into one text file
paste ~/Documents/.news_titles.txt ~/Documents/.news_links.txt > ~/Documents/.mixed_news.txt

# Adds the = symbol so that the rss feed text file can be used as an array
sed -i 's|http|= http|g' ~/Documents/.mixed_news.txt

#-----------------------------------------------------#
# Create array from rss feed text file
#-----------------------------------------------------#

fileName="$HOME/Documents/.mixed_news.txt"
export fileName

declare -A ary

readarray -t LINES < "$fileName"

while IFS= read -r line; do
    ary["${line%%=*}"]="${line#*=}"
done < "$fileName"

#-----------------------------------------------------#
# Yad dialog
#-----------------------------------------------------#

_yad() {

GTK_THEME="$MY_GTK_THEME" yad --list \
--column="Description" \
--search-column=1 \
--regex-search \
--no-headers \
--no-markup \
--buttons-layout="center" \
--title="BBC News feed" \
--text-align="center" \
--text="\nBBC News feed\n" \
--borders=20 \
--separator= \
--width=1200 \
--height=650 \
--button="_Update:/bin/bash -c 'update_feed'" \
--button="_Exit":1

}
export -f _yad

#-----------------------------------------------------#
# Open news link in browser
#-----------------------------------------------------#

open_link() {

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     "$BROWSER" "$url"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     "$BROWSER" "$url"
   fi
	
}
export -f open_link

#-----------------------------------------------------#
# Main
#-----------------------------------------------------#

title=$(printf '%s\n' "${!ary[@]}" | sort | _yad)
[[ -z "$title" ]] && exit
export title

   if [ "$title" ]; then
     url="${ary["${title}"]}"
     [[ -z "$url" ]] && exit
     export url
     open_link
   else
     echo "Program terminated." && exit 0
   fi

#-----------------------------------------------------#
# Unset variables and functions
#-----------------------------------------------------#

unset update_feed
unset rss_title
unset rss_links
unset launch
unset MY_GTK_THEME
unset fileName
unset _yad
unset title
unset url
unset BROWSER
unset bbc_rss
unset open_link
