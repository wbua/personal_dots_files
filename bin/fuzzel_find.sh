#!/bin/bash

dirs_paths=( ~/Documents/ ~/Downloads/ ~/Videos/ ~/Pictures/ ~/Music/ )

file_choice=$(find "${dirs_paths[@]}" -type f -iname '*.*' | \
fuzzel --dmenu -w 70 -P 10 -p "Find local files... ")
[ -z "$file_choice" ] && exit 0

xdg-open "$file_choice"
