#!/bin/bash

set -e

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -f "$HOME"/Documents/.chatbot_selected_choice.txt ]; then
     :
   else
     touch "$HOME"/Documents/.chatbot_selected_choice.txt
   fi

my_file=$(cat ~/Documents/.chatbot_selected_choice.txt)

if [[ -z "$my_file" ]]; then
echo "copilot" | tee ~/Documents/.chatbot_selected_choice.txt || exit
elif [[ "$my_file" == "copilot" ]]; then
:
elif [[ "$my_file" == "chatgpt" ]]; then
:
else
:
fi
