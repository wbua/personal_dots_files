#!/bin/bash

# Program command: yad_quick_emoji.sh
# Description: Paste favorite emojis to clipboard.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 28-08-2024
# Program_license: GPL 3.0
# Dependencies: yad, wl-clipboard, wtype

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-----------------------------------------------------------#
# User Preferences
#-----------------------------------------------------------#

MY_EMOJI="$TEMPORARY_EMOJI"
export MY_EMOJI

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#-----------------------------------------------------------#
# Main
#-----------------------------------------------------------#

   if [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "rofi" ]]; then
     main_choice=$(echo "$MY_EMOJI" | tr " " "\n" | ${MENU_ROFI} -config "$ROFI_THEME_CONFIG")
     export main_choice
     [ -z "$main_choice" ] && exit 0
     echo "$main_choice" | wl-copy -n
     wtype -M ctrl -k v
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "dmenu" ]]; then
     main_choice=$(echo "$MY_EMOJI" | tr " " "\n" | ${MENU_DMENU_SUCKLESS})
     export main_choice
     [ -z "$main_choice" ] && exit 0
     echo "$main_choice" | wl-copy -n
     wtype -M ctrl -k v
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "yad" ]]; then
     main_choice=$(echo "$MY_EMOJI" | tr " " "\n" | GTK_THEME="$MY_GTK_THEME" ${MENU_YAD_LIST})
     export main_choice
     [ -z "$main_choice" ] && exit 0
     echo "$main_choice" | wl-copy -n
     wtype -M ctrl -k v
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "fzf" ]]; then
     kitty --execute bash -c "~/bin/fzf_emoji_quick.sh"
   else
     :
   fi

#-----------------------------------------------------------#
# Unset variables and functions
#-----------------------------------------------------------#

unset MY_EMOJI
unset main_choice
unset MY_GTK_THEME
