#!/bin/bash

# Program command: fzf-dictionary-wayland.sh
# Description: Local dictionary.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 04-01-2024
# Program_license: GPL 3.0
# Dependencies: fzf, dictd, dict-gcide

#------------------------------------------#
# Kill script
#------------------------------------------#

kill_script() {

pkill -f ~/bin/fzf-dictionary
	
}
export -f kill_script

#------------------------------------------#
# Display dictionary results
#------------------------------------------#

results() {

choice=$(cat "$HOME"/Documents/.dict_look_up.txt | xargs dict)
[ -z "$choice" ] && exit 0

echo "$choice" | fzf --cycle \
--padding=0% \
--border="none" \
--header="
kill script: F9

" \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'dictionary word> ' \
--bind='F9:execute(kill_script {})' \
--info=inline \
--reverse

}
export -f results


#------------------------------------------#
# Add word to look up in dictionary
#------------------------------------------#

word_lookup() {

choice=$(echo "" | fzf --print-query \
--cycle \
--border="none" \
--header="
kill script: F9

" \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'dictionary search> ' \
--bind='F9:execute(kill_script {})' \
--info=inline \
--reverse)
[ -z "$choice" ] && exit 0

echo "$choice" | tee "$HOME"/Documents/.dict_look_up.txt
results

}
word_lookup
export -f word_lookup

#------------------------------------------#
# Unset variables and functions
#------------------------------------------#
	
unset word_lookup
unset results
