#!/bin/bash

browser="brave"
export browser

#----------------------------------------------------#
#
#----------------------------------------------------#

info_text="${color2}Help information${end}

${color7}This program decodes all QR Codes in the QR Codes directory.${end}
${color7}It then shows them in this program, using a text file.${end}

${color2}Buttons${end}

${color5}${end} ${color6}Create qr code from clipboard contents.${end}
${color5}${end} ${color6}Create qr code from typed text in entry box.${end}
${color5}${end} ${color6}Change your internet browser${end}
${color5}${end} ${color6}Change your file manager${end}
${color5}${end} ${color6}Show help page${end}
${color5}${end} ${color6}Click on text once, and then press the file button. Show selected qr code in file manager${end}
${color5}${end} ${color6}Click on text once, and then press the google button. This will do a google search.${end}
${color5}${end} ${color6}This will do a text search on file${end}
${color5}${end} ${color6}Click on text once, and then press the youtube button. This will do a youtube search.${end}
${color5}${end} ${color6}Click on text once, and then press the copy button. Will copy qr code png file to clipboard.${end}
${color5}${end} ${color6}Click on text once, and then press the SMPlayer button. Plays url.${end}
${color5}${end} ${color6}Click on text once, and then press the url button. Opens url in browser.${end}
${color5}${end} ${color6}Click on text once, and then press the select button. Shows qr code image in main display.${end}
${color5}${end} ${color6}Click on text once, and then press the clip button. Sends text to clipboard.${end}
${color5}${end} ${color6}Kills yad${end}

${color2}Keys${end}

${color7}Press ${color4}control+f${end} to file search.${end}

${color2}About${end}

${color7}Program created by ${color3}John Mcgrath${end}${end}
"
export info_text


#----------------------------------------------------#
#
#----------------------------------------------------#

open_url() {

echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | awk '{$1="";print $0}' | awk '{print $NF}' | xargs -d '\n' "$browser"

}
export -f open_url

#----------------------------------------------------#
#
#----------------------------------------------------#

KEY=$RANDOM
export KEY

#----------------------------------------------------#
#
#----------------------------------------------------#


_yad() {
#
GTK_THEME="alt-dialog4" yad --plug=$KEY --tabnum=2 --form --width="1000" --height="500" \
--columns=1 --separator= \
--field="<b><span color='#5EFF00' font_family='Monospace' font='24'>File manager</span></b>":fbtn '/bin/bash -c "text_qrcode"' &

#
echo "$info_text" | GTK_THEME="alt-dialog4" yad --plug=$KEY --tabnum=3 --list --column="help" --width="1000" --height="500" \
--no-headers --separator= &

#
tac "$HOME"/Pictures/.qrmixed.txt | GTK_THEME="alt-dialog4" yad --plug=$KEY --tabnum=1 --list --column="list" \
--width="1000" --height="500" --no-headers --separator= &

# 
GTK_THEME="alt-dialog4" yad --notebook --key=$KEY --width="1000" --height="500" --dialog-sep --separator= \
--tab="<span color='#ffffff' font_family='DejaVu Sans Mono' font='14' weight='bold'>QR Codes</span>" \
--tab="<span color='#ffffff' font_family='DejaVu Sans Mono' font='14' weight='bold'>Settings</span>" \
--tab="<span color='#ffffff' font_family='DejaVu Sans Mono' font='14' weight='bold'>Help</span>" \
--button="${color1}${end}":20 \
--button="<span color='#FFFFFF' font_family='Monospace' font='11'>Exit</span>":1

echo $?

}
export -f _yad


choice=$(_yad)
export choice

main() {

   # 
   if [[ "${choice:0-2}" == *"20"* ]]; then
     open_url
   else
   :
   fi

}
export -f main
main


#----------------------------------------------------#
#
#----------------------------------------------------#

unset KEY
unset info_text
unset open_url
unset browser
unset _yad
unset choice
unset _yad
