#!/bin/bash

# Program command: sway_screenrecording.sh
# Description: Records your screen.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 29-07-2024
# Program_license: GPL 3.0
# Dependencies: wf-recorder, wtype

# User preferences
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#--------------------------------------------------#
# Preferences
#--------------------------------------------------#

# Recording directory
SCREENCAST_DIR="$MAIN_SCREENCAST_DIR"
export SCREENCAST_DIR

#--------------------------------------------------#
# Create files and directories
#--------------------------------------------------#

# Create videos directory

   if [ -d "$HOME"/Videos ]; then
     :
   else
     mkdir "$HOME"/Videos
   fi
   
# Create documents directory

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi
   
   if [ -d "$SCREENCAST_DIR" ]; then
     :
   else
     notify-send "Screencast directory does not exist!" && exit 1
   fi

# This is where the video temporary recordings are stored

   if [ -d "$HOME"/Videos/.yadvidrec ]; then
     :
   else
     mkdir "$HOME"/Videos/.yadvidrec
   fi

# Thumbnail from video

   if [ -d "$HOME"/Videos/.temp_record_image ]; then
     :
   else
     mkdir "$HOME"/Videos/.temp_record_image
   fi

#--------------------------------------------------#
# Main
#--------------------------------------------------#
    
   if [[ "$(grep SCREENCAST_SOUND= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "on" ]]; then
     rm -f ~/Videos/.yadvidrec/*
     wf-recorder -a -f "$HOME"/Videos/.yadvidrec/"$(date +'%d-%m-%Y-%H%M%S')".mkv
   elif [[ "$(grep SCREENCAST_SOUND= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "off" ]]; then
     rm -f ~/Videos/.yadvidrec/*
     wf-recorder -f "$HOME"/Videos/.yadvidrec/"$(date +'%d-%m-%Y-%H%M%S')".mkv
   fi
