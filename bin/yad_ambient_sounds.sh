#!/bin/bash

# Program command: yad_ambient_sounds.sh
# Description: Plays sound effects with mpv.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 29-10-2024
# Program_license: GPL 3.0
# Dependencies: yad, mpv, playerctl, mpris

# Format for the sound-effects.txt
# Rainy day with birds=/home/user/Music/ambient_sounds/rainy-day-with-birds.mp3

# Get your sound effects from https://pixabay.com/sound-effects/

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-----------------------------------------------------#
# User preferences
#-----------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Text files directory
MY_ALL_TEXT_STORE="$ALL_TEXT_STORE"
export MY_ALL_TEXT_STORE

#-----------------------------------------------------#
# Create files and directories
#-----------------------------------------------------#

   if [ -d "$HOME"/Music ]; then
     :
   else
     mkdir "$HOME"/Music
   fi

   if [ -d "$HOME"/Music/ambient_sounds ]; then
     :
   else
     mkdir "$HOME"/Music/ambient_sounds
   fi

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$MY_ALL_TEXT_STORE" ]; then
     :
   else
     mkdir "$MY_ALL_TEXT_STORE"
   fi

   if [ -f "$MY_ALL_TEXT_STORE"/sound-effects.txt ]; then
     :
   else
     touch "$MY_ALL_TEXT_STORE"/sound-effects.txt
   fi

#-----------------------------------------------------#
# Remove whitespace
#-----------------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$MY_ALL_TEXT_STORE"/sound-effects.txt

#-----------------------------------------------------#
# Array containing list of sound files
#-----------------------------------------------------#

fileName="$MY_ALL_TEXT_STORE/sound-effects.txt"
export fileName

declare -A ary

readarray -t LINES < "$fileName"

while IFS= read -r line; do
    ary["${line%%=*}"]="${line#*=}"
done < "$fileName"

#-----------------------------------------------------#
# Add new sound file
#-----------------------------------------------------#

add_file() {

killall yad

choice=$(GTK_THEME="$MY_GTK_THEME" yad --form \
--text-align="center" \
--text="\nAdd new sound effect\n" \
--borders=30 \
--width=600 \
--height=300 \
--separator="#" \
--field="Name: ":CE \
--field="Path: ":CE \
--button="_Exit":1)
[[ -z "$choice" ]] && exit

field1=$(echo "$choice" | awk -F '#' '{print $1}')
field2=$(echo "$choice" | awk -F '#' '{print $2}')

echo "${field1}=${field2}" | tee -a "$MY_ALL_TEXT_STORE"/sound-effects.txt
	
}
export -f add_file

#-----------------------------------------------------#
# Stop mpv
#-----------------------------------------------------#

stop_mpv() {

killall mpv
	
}
export -f stop_mpv

#-----------------------------------------------------#
# Currently playing
#-----------------------------------------------------#

currently_playing() {

   if [[ "$(pidof mpv)" ]]; then	
      notify-send "$(playerctl --player mpv metadata -f "{{title}}" | tr "&" "+")"
   else
     notify-send "Not playing"
   fi
	
}
export -f currently_playing

#-----------------------------------------------------#
# Play-pause toggle
#-----------------------------------------------------#

play_pause() {

   if [[ "$(pidof mpv)" ]]; then	
     playerctl --player mpv play-pause	
   else
     :
   fi  

}
export -f play_pause     

#-----------------------------------------------------#
# Mpv status
#-----------------------------------------------------#

mpv_status() {

   if [[ "$(playerctl status -p mpv)" == 'Playing' ]]; then
     notify-send "Playing"
   elif	[[ "$(playerctl status -p mpv)" == 'Paused' ]]; then
	 notify-send "Paused"
   else
     notify-send "Not Playing"
   fi 

}
export -f mpv_status

#-----------------------------------------------------#
# Add directory contents to file
#-----------------------------------------------------#

add_dir_text() {

killall yad
add_all_to_file=$(find ~/Music/ambient_sounds/ -type f -iname "*.*")
echo "$add_all_to_file" | tee "$MY_ALL_TEXT_STORE"/sound-effects.txt
	
}
export -f add_dir_text

#-----------------------------------------------------#
# Yad dialog
#-----------------------------------------------------#

_yad() {

GTK_THEME="$MY_GTK_THEME" yad --list \
--column="sound-effects" \
--search-column=1 \
--regex-search \
--separator= \
--title="Sound effects" \
--text="\nAmbient sounds\n" \
--text-align="center" \
--borders=20 \
--no-headers \
--no-markup \
--width=1000 \
--height=600 \
--button="Status:/bin/bash -c 'mpv_status'" \
--button="Play-pause:/bin/bash -c 'play_pause'" \
--button="Playing:/bin/bash -c 'currently_playing'" \
--button="Stop:/bin/bash -c 'stop_mpv'" \
--button="Update:/bin/bash -c 'add_dir_text'" \
--button="Add file:/bin/bash -c 'add_file'" \
--button="Exit":1
	
}
export -f _yad

#-----------------------------------------------------#
# Main
#-----------------------------------------------------#

title=$(printf '%s\n' "${!ary[@]}" | sort | _yad)
export title

   if [ "$title" ]; then
     url="${ary["${title}"]}"
     export url
     playerctl --player=mpv stop >/dev/null 2>&1
     setsid mpv --no-video --loop "${url}" >/dev/null 2>&1 & disown
   else
     echo "Program terminated." && exit 0
   fi

#-----------------------------------------------------#
# Unset variables and functions
#-----------------------------------------------------#

unset _yad
unset MY_GTK_THEME
unset MY_EDITOR
unset title
unset url
unset fileName
unset add_file
unset stop_mpv
unset currently_playing
unset play_pause
unset mpv_status
unset add_dir_text
unset MY_ALL_TEXT_STORE
