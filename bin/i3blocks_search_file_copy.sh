#!/bin/bash

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

selected_file_choice=$(cat ~/Documents/.i3blocks_temp_selected_file.txt)
only_filename=$(cat ~/Documents/.i3blocks_temp_selected_file.txt | xargs -0 basename)
copy_choice=$(GTK_THEME="$MY_GTK_THEME" yad --form --width=800 --height=150 \
--separator= \
--field="Directory":DIR \
--button="Ok":0 \
--button="Exit":1)
[ -z "$copy_choice" ] && exit 0

cp "$selected_file_choice" "$copy_choice"/"$only_filename"

unset MY_GTK_THEME
unset selected_file_choice
unset only_filename
unset copy_choice
