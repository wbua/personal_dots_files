#!/bin/sh
# shell script to prepend i3status with more stuff


i3status | \
while :
do
        read line
        echo "<span color='#8abeb7'>KEYS</span> win+/ $line" || exit 1
done


