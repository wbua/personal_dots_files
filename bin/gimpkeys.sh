#!/bin/bash

launcher="xdotool key"

fileone="$(cat /home/john/Documents/palettes/gimp.txt)"

choice=$(echo -e "$fileone" | sort | rofi -dmenu -i -p '')
[ -z "$choice" ] && exit 0
echo -e "$choice" | awk -F ':' '{print $NF}' | sed 's/ //' | xargs $launcher
