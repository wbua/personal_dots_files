#!/bin/bash

dirs=( ~/Documents/ ~/Pictures/ ~/Videos/ ~/Music/ ~/Downloads/)

chosen=$(find "${dirs[@]}" -type f -iname '*.*' | sort | cut -d '/' -f4- | rofi -dmenu -i -p '' -me-select-entry '' -me-accept-entry 'MousePrimary' \
-theme-str 'element.selected.normal {background-color: #000000; padding: 0px 0px 0px 0px; border: 0 0 0 0; text-color: #22FF00;}' \
-theme-str 'configuration {hover-select: true; me-accept-entry: "MousePrimary"; me-select-entry: "";}' \
-theme-str 'entry {vertical-align: 0.5; placeholder: "Search files"; padding: 2px 0px 0px 0px; placeholder-color: grey;}')
[ -z "$chosen" ] && exit 0
sleep 0.1 ; echo "$HOME"/"$chosen" | xargs -0 xdg-open
