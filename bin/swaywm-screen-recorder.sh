#!/bin/bash

# Program command: swaywm-screen-recorder.sh
# Description: This is a screen recorder.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 21-12-2023
# Program_license: GPL 3.0
# Dependencies: rofi, wf-recorder, slurp

#-------------------------------------------------#
# Software preferences
#-------------------------------------------------#

# Video player
video_player="smplayer"

#-------------------------------------------------#
# Creates file and directories
#-------------------------------------------------#

   if [ -d "$HOME"/Videos ]; then
     :
   else
     mkdir "$HOME"/Videos
   fi

   if [ -d "$HOME"/Videos/.temp_swaywm_videos ]; then
     :
   else
     mkdir "$HOME"/Videos/.temp_swaywm_videos
   fi

   if [ -d "$HOME"/Videos/swaywm_videos ]; then
     :
   else
     mkdir "$HOME"/Videos/swaywm_videos
   fi

#-------------------------------------------------#
# Colors
#-------------------------------------------------#

# Description
color1="<span color='#8087A2' font_family='Cascadia Mono' font='14' weight='bold' rise='0pt'>"
# Colors end
end="</span>"

#-------------------------------------------------#
# Rofi settings
#-------------------------------------------------#

_rofi() {

wofi --dmenu -i -p 'Screen recording search...' --allow-markup 
#rofi -dmenu -i -p '' \
#-markup-rows \
#-theme-str 'message { margin: 30px; }' \
#-theme-str 'entry { placeholder: "Screen recording search...";}'
	
}

#-------------------------------------------------#
# Start select screen recording
#-------------------------------------------------#

select_recording() {

rm -f "$HOME"/Videos/.temp_swaywm_videos/*	
notify-send "Recording started!" && wf-recorder -g "$(slurp)" -f ~/Videos/.temp_swaywm_videos/"$(date +'%d-%m-%Y-%H%M%S')".mp4

}

#-------------------------------------------------#
# Start fullcreen screen recording
#-------------------------------------------------#

start_recording() {

rm -f "$HOME"/Videos/.temp_swaywm_videos/*
notify-send "Recording started!" && wf-recorder -f ~/Videos/.temp_swaywm_videos/"$(date +'%d-%m-%Y-%H%M%S')".mp4

}

#-------------------------------------------------#
# Stop screen recording
#-------------------------------------------------#

stop_recording() {

killall -s SIGINT wf-recorder
cp ~/Videos/.temp_swaywm_videos/* ~/Videos/swaywm_videos/
sleep 0.2
notify-send "Recording stopped!"
	
}

#-------------------------------------------------#
# Check status of recording
#-------------------------------------------------#

recording_status() {

   if [[ "$(pidof wf-recorder)" ]]; then
     notify-send "recording"
   else
     notify-send "Not recording"
   fi
	
}

#-------------------------------------------------#
# Open recording
#-------------------------------------------------#

open_recording() {

"$video_player" ~/Videos/.temp_swaywm_videos/*.mp4
	
}

#-------------------------------------------------#
# Menu
#-------------------------------------------------#

menu() {

echo "record ${color1}fullscreen recording${end}"
echo "select-record ${color1}select a region to record${end}"
echo "stop ${color1}recording${end}"
echo "open ${color1}last recorded file${end}"
echo "status ${color1}status of recording${end}"
	
}

#-------------------------------------------------#
# Main
#-------------------------------------------------#

main() {

choice=$(menu | _rofi | awk '{print $1}')
[[ -z "$choice" ]] && exit

case "$choice" in

 'record')
  start_recording
 ;;
 
 'select-record')
  select_recording
 ;;
  
 'stop')
  stop_recording
 ;;

 'status')
  recording_status
 ;;

 'open')
  open_recording
 ;;

 *)
  echo "Something went wrong!" | rofi -dmenu -i -p '' || exit 1
 ;;

esac
	
}
main
