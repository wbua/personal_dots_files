#!/bin/bash

# Program command: run_radio.sh
# Description: Script that runs the radio function with keybinding.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 11-11-2023
# Program_license: GPL 3.0

#-------------------------------------------------------#
# Source file for colors
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_colors.sh

#-------------------------------------------------------#
# Source file for functions
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_functions.sh

#-------------------------------------------------------#
# Source file for config
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_config.sh

#-------------------------------------------------------#
# Source file for creating files and directories
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_files_directories.sh

#-------------------------------------------------------#
# Source file for misc
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_misc.sh

#-------------------------------------------------------#
# Create directories and files
#-------------------------------------------------------#

create_file_and_directories
                
#-------------------------------------------------------#
# Misc
#-------------------------------------------------------#

misc_functions

#-------------------------------------------------------#
# Radio main
#-------------------------------------------------------#

online_radio
