#!/bin/bash
# shellcheck disable=SC2154 

# Program command: run_notes.sh
# Description: Taking oneline notes.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 11-11-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/

#-------------------------------------------------------#
# Source file for colors
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_colors.sh

#-------------------------------------------------------#
# Source file for functions
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_functions.sh

#-------------------------------------------------------#
# Source file for config
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_config.sh

#-------------------------------------------------------#
# Source file for creating files and directories
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_files_directories.sh

#-------------------------------------------------------#
# Source file for misc
#-------------------------------------------------------#

# shellcheck disable=SC1091
source "$HOME"/bin/johns_misc.sh

#-------------------------------------------------------#
# Create directories and files
#-------------------------------------------------------#

create_file_and_directories
                
#-------------------------------------------------------#
# Misc
#-------------------------------------------------------#

misc_functions

#------------------------------------------------------#
# Menu
#------------------------------------------------------#

menu() {

echo -e "${color26}${end}${space}nl ${color4}${space}create oneline note${end}"
echo -e "${color26}${end}${space}nl copy ${color4}${space}oneline note${end}"
echo -e "${color26}${end}${space}nl txt ${color4}${space}oneline note${end}"
echo -e "${color26}${end}${space}nl space ${color4}${space}oneline note${end}"
echo -e "${color26}${end}${space}nl qrcode ${color4}${space}oneline note${end}"
echo -e "${color26}${end}${space}nl speak ${color4}${space}oneline note${end}"
	
}

#------------------------------------------------------#
# Main
#------------------------------------------------------#

main() {

selected=$(menu | _rofi | awk -F "${color26}" '{print $2,$3}' | sed 's|</span>||')
bang=$(echo "${selected[@]}" | awk '{print $2}')
launchercontent=$(echo "$selected" | awk '{print $3}')
[ -z "$launchercontent" ] && exit 0

case "$bang" in

'nl')
   if [[ "$launchercontent" == 'copy' ]]; then
     nl_copy
   elif [[ "$launchercontent" == 'txt' ]]; then
     nl_txt
   elif [[ "$launchercontent" == 'space' ]]; then
     nl_space
   elif [[ "$launchercontent" == 'qrcode' ]]; then
     nl_qrcode
   elif [[ "$launchercontent" == 'speak' ]]; then
     nl_speak
   else
     nl_out
   fi
  ;;
  
  *)
  echo "Something when wrong!" | rofi -dmenu -i -p ''
  ;;

esac  

}
main
