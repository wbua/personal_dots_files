#!/bin/bash

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

TEXT_EDITOR="$MAIN_TEXT_EDITOR"

   if [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then 
     setsid "$TEXT_EDITOR" ~/.config/sway/autostart.sh >/dev/null 2>&1 & disown && exit
     bash ~/bin/switch_to_texteditor.sh
   elif [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     setsid "$TEXT_EDITOR" ~/.config/sway/autostart.sh >/dev/null 2>&1 & disown && exit
   else
     :
   fi
