#!/bin/bash

# Master config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

MY_COLOR_ALERT="#ff0022"
MY_COLOR_ICON="#ff0022"
MY_COLOR_BG="$COLOR_BG"

reply_action () {

pkill -f ~/bin/turning_off_monitor.sh
	
}

forward_action () {

:
	
}

handle_dismiss () {

:
	
}

ACTION=$(dunstify -t 20000 -u critical -h string:bgcolor:$MY_COLOR_BG -h string:fgcolor:$MY_COLOR_ICON -h string:frcolor:$MY_COLOR_ALERT --action="default,Reply" --action="forwardAction,Forward" "Turning off screen!" "Press win shift m to turn on screen\nMiddle click notification to cancel")

case "$ACTION" in
"default")
    reply_action
    ;;
"forwardAction")
    forward_action
    ;;
"2")
    handle_dismiss
    ;;
esac

swaymsg "output * dpms off"
