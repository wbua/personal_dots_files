#!/bin/bash

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

   if [ -d "$HOME"/Music ]; then
     :
   else
     mkdir "$HOME"/Music
   fi

   if [ -d "$HOME"/Music/sounds ]; then
     :
   else
     mkdir "$HOME"/Music/sounds
   fi
   

   if [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     ffplay -autoexit -nodisp ~/Music/sounds/notification.mp3
   elif [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     :
   fi


