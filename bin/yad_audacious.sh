#!/bin/bash
# shellcheck disable=SC2090
# shellcheck disable=SC2089

# Program command: yad_audacious.sh
# Description: Control audacious player. Used with polybar.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 13-07-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad, audacious, playerctl, ffmpeg, imagemagick, material icons

# Google material icons link:
# https://github.com/google/material-design-icons/blob/master/font/MaterialIcons-Regular.ttf

# Download no_album.png from my dots: https://sourceforge.net/projects/jm-dots/files/photos/
# Once you had downloaded it, put the png in: /home/user/Pictures/.audacious_empty/

# Download gtk.css in my dots: https://sourceforge.net/projects/jm-dots/files/yad_scripts/
# Put the gtk.css file in: /home/user/.local/share/themes/alt-dialog/gtk-3.0/
# If this directory, does not exist then create it.

#################################################
# Issues
#################################################

# There can be problems with the audacheckpl function.
# The playlists you add in audacious, should be the same in audacheckpl.
# So if you have 4 playlists in audacious, you should only have 4 playlists in audacheckpl.
# If you don't do this, then the function breaks.  

#--------------------------------------------------------------------#
# Create directories and files
#--------------------------------------------------------------------#

   # Create pictures directory
   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   # Create Documents directory
   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi
 
   # Stores the temp album art 
   if [ -d "$HOME"/Pictures/.audacious_art ]; then
     :
   else
     mkdir "$HOME"/Pictures/.audacious_art
   fi
   
   # Stores the empty album art png
   if [ -d "$HOME"/Pictures/.audacious_empty ]; then
     :
   else
     mkdir "$HOME"/Pictures/.audacious_empty
   fi

   # Skips to positions in the track
   if [ -f "$HOME"/Documents/.skip_position.txt ]; then
     :
   else
        touch "$HOME"/Documents/.skip_position.txt
   fi
   
#--------------------------------------------------------------------#
# Kill new instance
#--------------------------------------------------------------------#

killall yad

#--------------------------------------------------------------------#
# Create album art
#--------------------------------------------------------------------#

create_art() {
	
  rm -f "$HOME"/Pictures/.audacious_art/*.*
  cfile="$HOME/Documents/.audacious_temp.txt"
  export cfile
  audtool --current-song-filename  > "$cfile"
  ffmpeg -i "$(cat "$cfile")" -an -vcodec copy "$HOME"/Pictures/.audacious_art/cover.png
  convert "$HOME"/Pictures/.audacious_art/cover.png -resize 150x150 "$HOME"/Pictures/.audacious_art/cover.png
	
}
export -f create_art 
create_art

#--------------------------------------------------------------------#
# Check album art exists
#--------------------------------------------------------------------#

check_art() {

   if [ -z "$(ls -A "$HOME"/Pictures/.audacious_art/)" ]; then
     find "$HOME"/Pictures/.audacious_empty/ -type f -print0 | xargs -0 ls -tr | tail -n 1
   else
     find "$HOME"/Pictures/.audacious_art/ -type f -print0 | xargs -0 ls -tr | tail -n 1
   fi
   
}
export -f check_art

#--------------------------------------------------------------------#
# Position of yad on the desktop
#--------------------------------------------------------------------#

# yad x position
box_x_position="1200"
export box_x_position
# yad y position
box_y_position="55"
export box_y_position

#-------------------------------------------------------#
# Colors for menu items (pango markup)
#-------------------------------------------------------#

# Artist
color1="<span color='#FFFFFF' font_family='Inter' font='14' weight='bold' rise='0pt'>"
export color1
# Off
color2="<span color='#FF002E' font_family='Material Icons' font='18' weight='bold' rise='0pt'>"
export color2
# On
color3="<span color='#22FF00' font_family='Material Icons' font='18' weight='bold' rise='0pt'>"
export color3
# Title
color4="<span color='#FFFFFF' font_family='Inter' font='12' rise='0pt'>"
export color4
# Audacious off
color5="<span color='#FFFFFF' font_family='Material Icons' font='18' rise='0pt'>"
export color5
end="</span>"
export end

#--------------------------------------------------------------------#
# Displays the current playlist
#--------------------------------------------------------------------#

check_playlist() {
	
   if [[ "$(audtool --current-playlist)" == *"1"* ]]; then
     echo "one"
   elif [[ "$(audtool --current-playlist)" == *"2"* ]]; then
     echo "two"
   elif [[ "$(audtool --current-playlist)" == *"3"* ]]; then
     echo "three"
   elif [[ "$(audtool --current-playlist)" == *"4"* ]]; then
     echo "four"
   else
     echo "off"
   fi
	
}
export -f check_playlist 

#--------------------------------------------------------------------#
# Change playlists for audacious
#--------------------------------------------------------------------#

audacheckpl() {

audapcone=$(audtool --current-playlist)
export audapcone 
 
   if [[ "$audapcone" == *"1"* ]]; then
     killall yad   
     audtool --set-current-playlist 2
     yad_audacious.sh
   elif [[ "$audapcone" == *"2"* ]]; then
     killall yad
     audtool --set-current-playlist 3
     yad_audacious.sh
   elif [[ "$audapcone" == *"3"* ]]; then
     killall yad
     audtool --set-current-playlist 1
     yad_audacious.sh
   else
     :
   fi
	
}
export -f audacheckpl 

#--------------------------------------------------------------------#
# Skip to different positions on the track
#--------------------------------------------------------------------#

skip_position(){
    
     mfile=$(cat "$HOME"/Documents/.skip_position.txt)
     echo "audtool --playback-seek 0" > "$HOME"/Documents/.skip_position.txt
     export mfile 
   if [[ "$mfile" == 'audtool --playback-seek 0' ]]; then
     killall yad
     echo "audtool --playback-seek 25" > "$HOME"/Documents/.skip_position.txt
     audtool --playback-seek 25
     yad_audacious.sh
   elif [[ "$mfile" == 'audtool --playback-seek 25' ]]; then
     killall yad
     echo "audtool --playback-seek 50" > "$HOME"/Documents/.skip_position.txt
     audtool --playback-seek 50
     yad_audacious.sh
   elif [[ "$mfile" == 'audtool --playback-seek 50' ]]; then
     killall yad
     echo "audtool --playback-seek 75" > "$HOME"/Documents/.skip_position.txt
     audtool --playback-seek 75
     yad_audacious.sh
   elif [[ "$mfile" == 'audtool --playback-seek 75' ]]; then
     killall yad
     echo "audtool --playback-seek 0" > "$HOME"/Documents/.skip_position.txt
     audtool --playback-seek 0
     yad_audacious.sh
   else
    :
   fi
	
}
export -f skip_position 

#--------------------------------------------------------------------#
# Checks the volume is on or off
#--------------------------------------------------------------------#

check_mute() {
	
   if [[ -z "$(pidof audacious)" ]]; then
     echo "${color5}${end}"
   elif [[ "$(audtool --get-volume)" == *"100"* ]]; then
     echo "${color3}${end}"
   elif [[ "$(audtool --get-volume)" == *"0"* ]]; then
     echo "${color2}${end}" 
   fi
    	
}
export -f check_mute

#--------------------------------------------------------------------#
# Checks shuffle is on or off
#--------------------------------------------------------------------#

check_shuffle() {

   if [[ -z "$(pidof audacious)" ]]; then
     echo "${color5}${end}"
   elif [[ "$(audtool --playlist-shuffle-status)" == *"on"* ]]; then
     echo "${color3}${end}"
   elif [[ "$(audtool --playlist-shuffle-status)" == *"off"* ]]; then
     echo "${color2}${end}"
   fi
	
}
export -f check_shuffle

#--------------------------------------------------------------------#
# Currently playing
#--------------------------------------------------------------------#

currently_playing() {
   
   if pgrep -x "audacious" > /dev/null
   then     
     echo "${color1}$(playerctl --player audacious metadata -f "{{artist}}" | cut -c 1-30 | tr "&" "+")${end}"
     echo ""
     echo "${color4}$(playerctl --player audacious metadata -f "{{title}}" | cut -c 1-30 | tr "&" "+")${end}"   
   else
     echo "Not playing"
   fi
     	
}
export -f currently_playing

#--------------------------------------------------------------------#
# Check play and pause
#--------------------------------------------------------------------#

check_play() {
	
   if [[ "$(audtool --playback-status)" == 'paused' ]]; then
     echo "${color2}${end}"   
   elif [[ "$(audtool --playback-status)" == 'playing' ]]; then
     echo "${color3}${end}"
   else
     echo "${color5}${end}"
   fi
   
}
export -f check_play

#--------------------------------------------------------------------#
# Play and pause
#--------------------------------------------------------------------#

play_audacious() {
	
   if [[ "$(audtool --playback-status)" == 'paused' ]]; then
     killall yad
     audacious -H --play
     yad_audacious.sh
   elif [[ "$(audtool --playback-status)" == 'playing' ]]; then
     killall yad
     audacious -H --pause
     yad_audacious.sh
   else
     killall yad
     audacious -H --play
   fi
   
}
export -f play_audacious

#--------------------------------------------------------------------#
# Prev
#--------------------------------------------------------------------#

prev_audacious() {
	
  killall yad
  audacious --rew
  yad_audacious.sh	
	
}
export -f prev_audacious

#--------------------------------------------------------------------#
# Next
#--------------------------------------------------------------------#

next_audacious() {
	
  killall yad
  audacious --fwd	
  yad_audacious.sh	
	
}
export -f next_audacious

#--------------------------------------------------------------------#
# Mute and unmute
#--------------------------------------------------------------------#

_mute(){
	
   if [[ "$(audtool --get-volume)" == *"100"* ]]; then
     killall yad
     audtool --set-volume 0%
     yad_audacious.sh
   else
     killall yad
     audtool --set-volume 100%
     yad_audacious.sh
   fi
	
}
export -f _mute

#--------------------------------------------------------------------#
# Toggle shuffle
#--------------------------------------------------------------------#

toggle_shuffle(){
	
   if [[ "$(audtool --playlist-shuffle-status)" == *"on"* ]]; then
     killall yad
     audtool --playlist-shuffle-toggle
     yad_audacious.sh
   else
     killall yad
     audtool --playlist-shuffle-toggle
     yad_audacious.sh
   fi
	
}
export -f toggle_shuffle

#--------------------------------------------------------------------#
# Refresh yad
#--------------------------------------------------------------------#

yad_refresh() {
	  
  killall yad ; sleep 1 ; yad_audacious.sh
    	
}
export -f yad_refresh 

#--------------------------------------------------------------------#
# Kill audacious
#--------------------------------------------------------------------#

kill_audacious() {
	  
  killall audacious ; yad_audacious.sh
  	
}
export -f kill_audacious

#--------------------------------------------------------------------#
# Yad settings
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog" yad --form --text-info --width=600 --height=150 --image-on-top --borders=10 --columns=11 \
--posx="$box_x_position" --posy="$box_y_position" \
--buttons-layout="center" \
--title="Audacious player" \
--text-align="center" \
--align="center" \
--image-on-top \
--image="$(check_art)" \
--text="$(currently_playing)" \
--field="<b><span color='#ffffff' font_family='Material Icons' font='18'>$(check_play)</span></b>":fbtn '/bin/bash -c "play_audacious"' \
--field="<b><span color='#ffffff' font_family='Material Icons' font='18'></span></b>":fbtn '/bin/bash -c "prev_audacious"' \
--field="<b><span color='#ffffff' font_family='Material Icons' font='18'></span></b>":fbtn '/bin/bash -c "next_audacious"' \
--field="<b><span color='#ffffff' font_family='Material Icons' font='18'>$(check_shuffle)</span></b>":fbtn '/bin/bash -c "toggle_shuffle"' \
--field="<b><span color='#ffffff' font_family='Material Icons' font='18'></span></b>":fbtn '/bin/bash -c "kill_audacious"' \
--button="<span color='#ffffff' font_family='Inter' font='11'>skip</span>:/bin/bash -c 'skip_position'" \
--button="<span color='#ffffff' font_family='Material Icons' font='18'>$(check_mute)</span>:/bin/bash -c '_mute'" \
--button="<span color='#ffffff' font_family='Inter' font='11'>$(check_playlist)</span>:/bin/bash -c 'audacheckpl'" \
--button="<span color='#ffffff' font_family='Material Icons' font='18'></span>:/bin/bash -c 'yad_refresh'" \
--button="<span color='#ffffff' font_family='Material Icons' font='18'></span>":1

#--------------------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------------------#

unset audacheckpl
unset currently_playing
unset color1
unset color2
unset color3
unset color4
unset color5
unset end
unset box_x_position
unset box_y_position
unset create_art
unset cfile
unset check_art
unset _mute
unset toggle_shuffle
unset check_mute 
unset check_shuffle
unset skip_position
unset mfile
unset check_playlist
unset yad_refresh
unset kill_audacious
unset play_audacious
unset check_play
unset prev_audacious
unset next_audacious
