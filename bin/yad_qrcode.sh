#!/bin/bash

# Program command: yad_qrcode.sh
# Description: Creates qr codes.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-07-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad, xclip, zbar-tools, qrencode, material icons, custom gtk.css

########################################

# Downloads

# material icons: https://github.com/google/material-design-icons/blob/master/font/MaterialIcons-Regular.ttf
# custom gtk.css: https://sourceforge.net/projects/jm-dots/files/gtk_css/alt-dialog4/

########################################

# Installation

# You will first need to install the dependencies.
# In the terminal type the following below.

# Step 1:
# $ sudo apt update

# Step 2:
# $ sudo apt install yad xclip zbar-tools qrencode

#--------------------------------

# You will need to put yad_qrcode.sh into your executable path.
# This is in /home/user/.local/bin/
# Then type the command below.
# $ chmod u+x yad_qrcode.sh

#--------------------------------

# You will then need to download the files in the Download links section.
# After you have downloaded them do the following.
# If the directories do not exist, then create them.

# Material icons installation: 
# You should install font-manager, to install the font.
# Step 1: $ sudo apt update
# Step 2: $ sudo apt install font-manager
# Open font manager and press the plus button.
# Select the MaterialIcons-Regular.ttf font and press ok.

# Gtk.css installation:
# Copy and paste gtk.css file into /home/user/.local/share/themes/alt-dialog4/

########################################

# How to use

# Text button: Type text to create qr code.
# Clip button: Creates QR Code from clipboard contents. 
# If contents contain url, it will put url in the beginning of the filename.
# If contents contain youtube, it will put youtube in the beginning of the filename.
# Open button: Open last created QR Code in image viewer.
# Speak button: Reads the text using text to speech.
# Copy Button: Copies the QR Code png to the clipboard
# Close button: Kills yad
# Directory button: Select in file manager the last created QR Code.
# Decode button: Decodes the last QR Code in text form.

#--------------------------------------------------------------------#
# General variables
#--------------------------------------------------------------------#

file_manager="nautilus"
export file_manager

#--------------------------------------------------------------------#
# Creates directories and files
#--------------------------------------------------------------------#

# Create Pictures directory
   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   # Create temp qr code directory
   if [ -d "$HOME"/Pictures/.temqrcodes ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes
   fi

   # Create main qr code directory
   if [ -d "$HOME"/Pictures/qr_codes ]; then
     :
   else
     mkdir "$HOME"/Pictures/qr_codes
   fi

#--------------------------------------------------------------------#
# Create qr code from clipboard
#--------------------------------------------------------------------#
  
create_qrcode() {
	 
killall yad
rm -f "$HOME"/Pictures/.temqrcodes/*.png
xclip -o | tr -d '\n' | qrencode -m 36 -l H -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
yad_qrcode.sh
   
}
export -f create_qrcode   
 
#--------------------------------------------------------------------#
# Open qr code
#--------------------------------------------------------------------#

open_qrcode() {
	
GTK_THEME="Yaru"
killall yad 
find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open 
   
}
export -f open_qrcode
  
#--------------------------------------------------------------------#
# Decode qr code
#--------------------------------------------------------------------#

decode_qrcode() {

killall yad	
find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw | \
GTK_THEME="alt-dialog4" yad --text-info --wrap --height=500 --width=800 --center --back="#20252C" --fore="#ffffff" \
--button="<span color='#F056FF' font_family='Material Icons' font='18'></span>":1   
   
}
export -f decode_qrcode
  
#--------------------------------------------------------------------#
# Copy qr code png to clipboard
#--------------------------------------------------------------------#

image_to_clip() {
	
killall yad
xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"  
  
}
export -f image_to_clip
  
#--------------------------------------------------------------------#
# Select qr code in file manager
#--------------------------------------------------------------------#  

dir_qrcode() {

GTK_THEME="Yaru"
killall yad 
chosen=$(find "$HOME"/Pictures/.temqrcodes/ -type f -iname '*.*' | sort | cut -d '/' -f6-)
export chosen
[ -z "$chosen" ] && exit 0
"$file_manager" "$HOME"/Pictures/qr_codes/"$chosen"  

}
export -f dir_qrcode
  
#--------------------------------------------------------------------#
# Enter text to create Qr Code
#--------------------------------------------------------------------#   
  
text_qrcode() {

     killall yad	
     entry_box=$(GTK_THEME="alt-dialog4" yad --entry --width=500 --height=200 \
     --button="<span color='#F056FF' font_family='Material Icons' font='18'></span>":1)
     export entry_box
   if [[ -z "$entry_box" ]]; then
     exit 0
   else
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     echo "$entry_box" | tr -d '\n' | qrencode -m 36 -l H -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
     yad_qrcode.sh
   fi
     		
}
export -f text_qrcode  
  
#--------------------------------------------------------------------#
# Decode Qr Code and speak the text
#--------------------------------------------------------------------#     
  
decode_speak() {

killall yad	
find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw | festival --tts
	
}
export -f decode_speak  
  
#--------------------------------------------------------------------#
# Killall yad
#--------------------------------------------------------------------# 
  
kill_yad() {

killall yad	
	
}
export -f kill_yad
  
#--------------------------------------------------------------------#
# Yad settings
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog4" yad --form --text-info --width=300 --height=200 --borders=20 --buttons-layout="end" \
--title="QR Code: $(find "$HOME"/Pictures/.temqrcodes/ -type f -iname '*.*' | sort | cut -d '/' -f6-)" --columns=10 --center \
--image-on-top --no-buttons --columns=3 \
--image="$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)" \
--field="<b><span color='#5EFF00' font_family='Material Icons' font='24'></span></b>":fbtn '/bin/bash -c "text_qrcode"' \
--field="<b><span color='#FFBF00' font_family='Material Icons' font='24'></span></b>":fbtn '/bin/bash -c "decode_speak"' \
--field="<b><span color='#FFBF00' font_family='Material Icons' font='25'></span></b>":fbtn '/bin/bash -c "dir_qrcode"' \
--field="<b><span color='#5EFF00' font_family='Material Icons' font='24'></span></b>":fbtn '/bin/bash -c "create_qrcode"' \
--field="<b><span color='#FFBF00' font_family='Material Icons' font='24'></span></b>":fbtn '/bin/bash -c "image_to_clip"' \
--field="<b><span color='#FFBF00' font_family='Material Icons' font='24'></span></b>":fbtn '/bin/bash -c "decode_qrcode"' \
--field="<b><span color='#FFBF00' font_family='Material Icons' font='24'></span></b>":fbtn '/bin/bash -c "open_qrcode"' \
--field="<b><span color='#FF002C' font_family='Material Icons' font='24'></span></b>":fbtn '/bin/bash -c "kill_yad"' \
--button="<span color='#FFBF00' font_family='Material Icons' font='18'></span>":1

#--------------------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------------------#

unset create_qrcode
unset open_qrcode
unset decode_qrcode
unset image_to_clip
unset dir_qrcode
unset chosen
unset file_manager
unset text_qrcode
unset entry_box
unset decode_speak
unset kill_yad
