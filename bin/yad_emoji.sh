#!/bin/bash

# Program command: yad_emoji.sh
# Description: Copies emoji to clipboard in gui.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 30-07-2024
# Program_license: GPL 3.0
# Dependencies: yad, wl-clipboard

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-------------------------------------------------#
# Error checking
#-------------------------------------------------#

set -e

#-------------------------------------------------#
# User preferences
#-------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Emoji file
EMOJI_FILE="$MAIN_EMOJI_FILE"
export EMOJI_FILE

#-------------------------------------------------#
# Create files and directories
#-------------------------------------------------#

   if [ -f "$EMOJI_FILE" ]; then
     :
   else
     notify-send "Emoji file does not exist!" && exit 1
   fi

#-------------------------------------------------#
# Colors
#-------------------------------------------------#

COLOR1=$(printf '%s' "<span color='#FFFFFF' font_family='JetBrainsMono Nerd Font' font='14' rise='0pt'>")
export COLOR1

END=$(printf '%s' "</span>")
export END

#-------------------------------------------------#
# Check if your running wayland session
#-------------------------------------------------#

   if [[ "$(echo $WAYLAND_DISPLAY)" == "" ]]; then
     yad --error \
     --width 500 \
     --title "Wayland session not found!" \
     --text "Run launcher error, wayland only tool!"
     exit 1
   else
     :
   fi

#-------------------------------------------------#
# Yad settings
#-------------------------------------------------#

choice=$(cat "$EMOJI_FILE" | GTK_THEME="$MY_GTK_THEME" yad --list --search-column=1 \
--column="emoji" \
--regex-search \
--title="${COLOR1}Emoji's${END}" \
--text="\n${COLOR1}Emoji's${END}\n" \
--text-align="center" \
--borders=20 \
--no-headers \
--separator= \
--width=700 \
--height=600 \
--button="${COLOR1}Close${END}":1)
export choice
[[ -z "$choice" ]] && exit

#-------------------------------------------------#
# Copy emoji to clipboard
#-------------------------------------------------#

echo "$choice" | awk '{print $1}' | wl-copy -n

#-------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------#

unset choice
unset COLOR1
unset END
unset EMOJI_FILE
