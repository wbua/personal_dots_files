#!/bin/bash

#-------------------------------------------------------#
# 
#-------------------------------------------------------#

browser="brave"
export browser

file_manager="thunar"
export file_manager

#-------------------------------------------------------#
# 
#-------------------------------------------------------#

color1="<span color='#25FF00' font_family='Material Icons' font='25'>"
export color1
end="</span>"
export end

#-------------------------------------------------------#
# 
#-------------------------------------------------------#

_yad2() {

GTK_THEME="alt-dialog5" yad --list --column="search" --width=1200 --height=500 \
--center --no-headers --separator= \
--button="${color1}${end}":90 \
--button="${color1}${end}":30 \
--button="${color1}${end}":44 \
--button="${color1}${end}":60 \
--button="${color1}${end}":20 \
--button="<span color='#FF0026' font_family='Material Icons' font='18'></span>":1

echo $?
	
}
export -f _yad2

#-------------------------------------------------------#
# 
#-------------------------------------------------------#

select_file() {

   if [[ "$choice" == '90' ]]; then
     :
   else
     GTK_THEME="Yaru"
     killall yad
     chosen=$(echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | awk '{print $1}')
     export chosen
     [ -z "$chosen" ] && exit 0
     "$file_manager" "$HOME"/Pictures/qr_codes/"$chosen"
   fi

}
export -f select_file

#-------------------------------------------------------#
# 
#-------------------------------------------------------#


text_clip() {

   if [[ "$choice" == '30' ]]; then
     :
   else
     echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | awk '{$1="";print $0}' | xclip -selection clipboard
   fi

}
export -f text_clip

#-------------------------------------------------------#
# 
#-------------------------------------------------------#

image_to_clip() {

killall yad

  if [[ "$choice" == '44' ]]; then
    :
  else
    xclip -selection clipboard -t image/png -i < "$(echo "$HOME"/Pictures/qr_codes/"$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | \
    awk '{print $1}' | xargs -d '\n')"
  fi

}
export -f image_to_clip

#-------------------------------------------------------#
# 
#-------------------------------------------------------#

open_url() {

   if [[ "$choice" == '20' ]]; then
     :
   else
     echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | awk '{$1="";print $0}' | awk '{print $NF}' | xargs -d '\n' "$browser"
   fi

}
export -f open_url

#-------------------------------------------------------#
# 
#-------------------------------------------------------#

google_search() {

   if [[ "$choice" == '60' ]]; then
     :
   else
     search="https://www.google.co.uk/search?q={}"
     export search
     echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | awk '{$1="";print $0}' | xargs -d '\n' -I{} "$browser" "$search"
   fi

}
export -f google_search

#-------------------------------------------------------#
# 
#-------------------------------------------------------#

main() {

launch=$(cat "$HOME"/Documents/.qrcode_grep.txt)
export launch
choice=$(echo "$launch" | _yad2)
export choice

   # Open selected text in browser
   if [[ "${choice:0-2}" == *"60"* ]]; then
     google_search
   elif [[ "${choice:0-2}" == *"20"* ]]; then
     open_url
   elif [[ "${choice:0-2}" == *"44"* ]]; then
     image_to_clip
   elif [[ "${choice:0-2}" == *"30"* ]]; then
     text_clip
   elif [[ "${choice:0-2}" == *"90"* ]]; then
     select_file
   else
   :
   fi

}
export -f main
main

#-------------------------------------------------------#
# 
#-------------------------------------------------------#

unset _yad2
unset color1
unset end
unset main
unset launch
unset choice
unset google_search
unset browser
unset search
unset open_url
unset image_to_clip
unset text_clip
unset select_file
unset file_manager
