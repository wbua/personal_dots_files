#!/bin/bash

# Program command: yad_keyswords_functions.sh
# Description: Functions for yad keywords launcher.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 27-01-2025
# Program_license: GPL 3.0

#------------------------------------------------------#
# Keyword help page
#------------------------------------------------------#

keyword_help() {

echo "${COLOR3}Controls for all yad dialogs below:${END}"
echo "Use up and down arrows to scroll through list."
echo "Press enter or double click to select item."
echo "Hold down alt key and press the first letter of the button, to trigger button."
echo "After you have double clicked, then press button."
echo "Just start typing to search."
echo "Use up and down arrows for next and previous search"
echo "For keyword history, page down with arrow key to see last keyword used in root search."
echo "-------------------------------------------------------------------------------------------------------------------------"
echo "${COLOR3}Keywords:${END}"
echo "Keywords are in green, followed by input."
echo "Always add one space after KEYWORD then input."
echo "${COLOR1}Find local files:${END} ${COLOR2} $KEYWORD03${END} ${COLOR1} text${END}"
echo "${COLOR1}Find local files:${END} ${COLOR2} $KEYWORD04${END} ${COLOR3} last search history${END}"
echo "${COLOR1}Google search:${END} ${COLOR2} $KEYWORD21${END} ${COLOR1} text${END}"
echo "${COLOR1}Google translate:${END} ${COLOR2} $KEYWORD02${END} ${COLOR1} text${END}"
echo "${COLOR1}Youtube search:${END} ${COLOR2} $KEYWORD01${END} ${COLOR1} text${END}"
echo "${COLOR1}Duckduckgo search:${END} ${COLOR2} $KEYWORD59${END} ${COLOR1} text${END}"
echo "${COLOR1}Wikipedia search:${END} ${COLOR2} $KEYWORD62${END} ${COLOR1} text${END}"
echo "${COLOR1}Reddit search:${END} ${COLOR2} $KEYWORD60${END} ${COLOR1} text${END}"
echo "${COLOR1}Download video:${END} ${COLOR2} $KEYWORD61${END} ${COLOR1} link${END}"
echo "${COLOR1}Shutdown:${END} ${COLOR2} $KEYWORD09${END}"
echo "${COLOR1}Reboot:${END} ${COLOR2} $KEYWORD10${END}"
echo "${COLOR1}Lock:${END} ${COLOR2} $KEYWORD32${END}"
echo "${COLOR1}Volume:${END} ${COLOR2} $KEYWORD45${END}"
echo "${COLOR1}Volume:${END} ${COLOR2} $KEYWORD12${END} ${COLOR1} number${END} ${COLOR3} 0 to 100${END}"
echo "${COLOR1}Mute volume:${END} ${COLOR2} $KEYWORD11${END}"
echo "${COLOR1}Dictionary:${END} ${COLOR2} $KEYWORD08${END} ${COLOR1} text${END}"
echo "${COLOR1}Spell checker:${END} ${COLOR2} $KEYWORD18${END} ${COLOR1} text${END}"
echo "${COLOR1}QR codes:${END} ${COLOR2} $KEYWORD05${END} ${COLOR1} text${END}"
echo "${COLOR1}QR codes:${END} ${COLOR2} $KEYWORD38${END} ${COLOR3} select qr code on screen to decode${END}" 
echo "${COLOR1}Calculator:${END} ${COLOR2} $KEYWORD06${END} ${COLOR1} sum${END} ${COLOR3} example: $KEYWORD06 12+12${END}"
echo "${COLOR1}Calculator:${END} ${COLOR2} $KEYWORD07${END} ${COLOR3} history${END}"
echo "${COLOR1}Text to speech:${END} ${COLOR2} $KEYWORD13${END} ${COLOR1} text${END} ${COLOR3} speaks text${END}"
echo "${COLOR1}Text to speech:${END} ${COLOR2} $KEYWORD14${END} ${COLOR1} path/to/file${END} ${COLOR3} speaks text file contents${END}"
echo "${COLOR1}Text to speech:${END} ${COLOR2} $KEYWORD15${END} ${COLOR3} kills text to speech${END}"
echo "${COLOR1}Kill process:${END} ${COLOR2} $KEYWORD16${END} ${COLOR1} program${END} ${COLOR3} killall command${END}"
echo "${COLOR1}Kill process:${END} ${COLOR2} $KEYWORD17${END} ${COLOR1} program${END} ${COLOR3} pkill command${END}"
echo "${COLOR1}PDF:${END} ${COLOR2} $KEYWORD22${END} ${COLOR1} path/to/file${END} ${COLOR3} creates pdf${END}"
echo "${COLOR1}PDF:${END} ${COLOR2} $KEYWORD23${END} ${COLOR3} open last pdf created${END}"
echo "${COLOR1}PDF:${END} ${COLOR2} $KEYWORD24${END} ${COLOR3} print pdf${END}"
echo "${COLOR1}Screenshots:${END} ${COLOR2} $KEYWORD28${END} ${COLOR3} fullscreen screenshot${END}"
echo "${COLOR1}Screenshots:${END} ${COLOR2} $KEYWORD29${END} ${COLOR3} select screenshot${END}"
echo "${COLOR1}Screenshots:${END} ${COLOR2} $KEYWORD30${END} ${COLOR3} delay screenshot${END}"
echo "${COLOR1}Screenshots:${END} ${COLOR2} $KEYWORD31${END} ${COLOR3} open last created screenshot${END}"
echo "${COLOR1}Screencast:${END} ${COLOR2} $KEYWORD25${END} ${COLOR3} starts screen recording${END}"
echo "${COLOR1}Screencast:${END} ${COLOR2} $KEYWORD26${END} ${COLOR3} stops screen recording${END}"
echo "${COLOR1}Screencast:${END} ${COLOR2} $KEYWORD27${END} ${COLOR3} plays last screen recording${END}"
echo "${COLOR1}Terminal commands:${END} ${COLOR2} $KEYWORD33${END} ${COLOR1} command${END}"
echo "${COLOR1}Terminal:${END} ${COLOR2} $KEYWORD46${END} ${COLOR3} open terminal from thunar directory${END}"
echo "${COLOR1}Password generator:${END} ${COLOR2} $KEYWORD19${END} ${COLOR1} number${END} ${COLOR3} copies to clipboard, example: $KEYWORD19 10${END}"
echo "${COLOR1}Resize image:${END} ${COLOR2} $KEYWORD34${END} ${COLOR1} 400x${END} ${COLOR3} before KEYWORD, first copy image to clipboard, saves to picture directory${END}"
echo "${COLOR1}Emoji:${END} ${COLOR2} $KEYWORD35${END} ${COLOR3} all emoji's${END}"
echo "${COLOR1}Emoji:${END} ${COLOR2} $KEYWORD67${END} ${COLOR1} text${END} ${COLOR3} example: em smiling${END}"
echo "${COLOR1}Note:${END} ${COLOR2} $KEYWORD36${END} ${COLOR1} text${END}"
echo "${COLOR1}Note:${END} ${COLOR2} $KEYWORD37${END} ${COLOR3} note history${END}"
echo "${COLOR1}Color picker:${END} ${COLOR2} $KEYWORD40${END}"
echo "${COLOR1}Color:${END} ${COLOR2} $KEYWORD42${END} ${COLOR1} code${END} ${COLOR3} changes hex to rgb, copies to clipboard, example: $KEYWORD42 #10FF00${END}"
echo "${COLOR1}Color:${END} ${COLOR2} $KEYWORD41${END} ${COLOR1} code${END} ${COLOR3} changes rgb to hex, copies to clipboard, example: $KEYWORD41 16 255 0${END}"
echo "${COLOR1}Keywords:${END} ${COLOR2} $KEYWORD39${END}"
echo "${COLOR1}Player:${END} ${COLOR2} $KEYWORD44${END}  ${COLOR1} command${END} ${COLOR3} command: play, stop, next, prev, stat, art, qr, last ${END}"
echo "${COLOR1}Palette:${END} ${COLOR2} $KEYWORD43${END} ${COLOR1} command${END} ${COLOR3} brave palette: new, close, save, 1-6, history, download, reopen, manager${END}"
echo "${COLOR1}Text:${END} ${COLOR2} $KEYWORD47${END} ${COLOR1} text${END} ${COLOR3} uppercase to lowercase${END}"
echo "${COLOR1}Timezone:${END} ${COLOR2} $KEYWORD48${END} ${COLOR1} 15:00 Fri${END} ${COLOR3} Los Angeles${END}"
echo "${COLOR1}Timezone:${END} ${COLOR2} $KEYWORD49${END} ${COLOR1} 15:00 Fri${END} ${COLOR3} New York${END}"
echo "${COLOR1}Timezone:${END} ${COLOR2} $KEYWORD50${END} ${COLOR1} 15:00 Fri${END} ${COLOR3} Sydney${END}"
echo "${COLOR1}Timezone:${END} ${COLOR2} $KEYWORD51${END}"
echo "${COLOR1}Notifications:${END} ${COLOR2} $KEYWORD52${END} ${COLOR1} history${END}"
echo "${COLOR1}Notifications:${END} ${COLOR2} $KEYWORD52${END} ${COLOR1} toggle${END}"
echo "${COLOR1}Microphone:${END} ${COLOR2} $KEYWORD53${END} ${COLOR1} level${END} ${COLOR3} sENDs notification${END}"
echo "${COLOR1}Microphone:${END} ${COLOR2} $KEYWORD53${END} ${COLOR1} on${END} ${COLOR3} microphone on${END}"
echo "${COLOR1}Microphone:${END} ${COLOR2} $KEYWORD53${END} ${COLOR1} off${END} ${COLOR3} microphone off${END}"
echo "${COLOR1}Microphone:${END} ${COLOR2} $KEYWORD53${END} ${COLOR1} number${END} ${COLOR3} example: $KEYWORD53 50${END}"
echo "${COLOR1}Zip files:${END} ${COLOR2} $KEYWORD54${END}"
echo "${COLOR1}Video:${END} ${COLOR2} $KEYWORD55${END} ${COLOR1} link${END} ${COLOR3} plays video with mpv, example: $KEYWORD55 http://videos.mp4${END}"
echo "${COLOR1}Video:${END} ${COLOR2} $KEYWORD55${END} ${COLOR1} stop${END} ${COLOR3} stops mpv video${END}"
echo "${COLOR1}Modified:${END} ${COLOR2} $KEYWORD56${END} ${COLOR3} last modified file${END}"
echo "${COLOR1}Audio:${END} ${COLOR2} $KEYWORD57${END} ${COLOR1} stop${END} ${COLOR3} stops recording${END}"
echo "${COLOR1}Audio:${END} ${COLOR2} $KEYWORD57${END} ${COLOR1} open${END} ${COLOR3} opens recording${END}"
echo "${COLOR1}Audio:${END} ${COLOR2} $KEYWORD57${END} ${COLOR3} records audio from microphone${END}"
echo "${COLOR1}Radio:${END} ${COLOR2} $KEYWORD58${END} ${COLOR3} plays radio url in mpv from text file${END}"
echo "${COLOR1}Empty trash:${END} ${COLOR2} $KEYWORD63${END}"
echo "${COLOR1}Config:${END} ${COLOR2} $KEYWORD64${END} ${COLOR3} yad KEYWORDs config${END}"
echo "${COLOR1}Audacious:${END} ${COLOR2} $KEYWORD65${END} ${COLOR1} play${END}"
echo "${COLOR1}Audacious:${END} ${COLOR2} $KEYWORD65${END} ${COLOR1} current${END} ${COLOR3} current playlist${END}"
echo "${COLOR1}Audacious:${END} ${COLOR2} $KEYWORD65${END} ${COLOR1} toggle${END} ${COLOR3} toggle playlist${END}"
echo "${COLOR1}Audacious:${END} ${COLOR2} $KEYWORD66${END} ${COLOR1} number${END} ${COLOR3} set playlist number, example: $KEYWORD66 1${END}"

}

export -f keyword_help

#------------------------------------------------------#
# Show keyword help page in yad
#------------------------------------------------------#

show_keywords() {

echo "$KEYWORD39" > "$HOME"/Documents/last_keyword.txt
killall yad
keyword_help | GTK_THEME="$GTK_THEMES" yad --list \
--column="help" \
--search-column=1 \
--regex-search \
--borders=10 \
--text="\nHelp page\n" \
--text-align="center" \
--width=1250 \
--height=650 \
--no-headers \
--button="gtk-close":1
	
}
export -f show_keywords

#------------------------------------------------------#
# Google web search
#------------------------------------------------------#

google_search() {

echo "$KEYWORD21" > "$HOME"/Documents/last_keyword.txt

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     web_search="https://www.google.co.uk/search?q={}"
     echo "$LAUNCHER_CONTENT" | xargs -I{} "$MYBROWSER" "$web_search"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     web_search="https://www.google.co.uk/search?q={}"
     echo "$LAUNCHER_CONTENT" | xargs -I{} "$MYBROWSER" "$web_search"
   fi
	
}
export -f google_search

#------------------------------------------------------#
# Youtube web search
#------------------------------------------------------#

youtube_search() {

echo "$KEYWORD01" > "$HOME"/Documents/last_keyword.txt

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     youtube_search="https://www.youtube.com/results?search_query={}"
     echo "$LAUNCHER_CONTENT" | xargs -I{} "$MYBROWSER" "$youtube_search"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     youtube_search="https://www.youtube.com/results?search_query={}"
     echo "$LAUNCHER_CONTENT" | xargs -I{} "$MYBROWSER" "$youtube_search"
   fi

}
export -f youtube_search

#------------------------------------------------------#
# Duckduckgo web search
#------------------------------------------------------#

duckduckgo() {

echo "$KEYWORD59" > "$HOME"/Documents/last_keyword.txt

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     duckduckgo_search="https://duckduckgo.com/?q={}"
     echo "$LAUNCHER_CONTENT" | xargs -I{} "$MYBROWSER" "$duckduckgo_search"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     duckduckgo_search="https://duckduckgo.com/?q={}"
     echo "$LAUNCHER_CONTENT" | xargs -I{} "$MYBROWSER" "$duckduckgo_search"
   fi

}
export -f duckduckgo

#------------------------------------------------------#
# Wikipedia web search
#------------------------------------------------------#

wikipedia() {

echo "$KEYWORD62" > "$HOME"/Documents/last_keyword.txt

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     wikipedia_search="https://en.wikipedia.org/wiki/{}"
     echo "$LAUNCHER_CONTENT" | xargs -I{} "$MYBROWSER" "$wikipedia_search"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     wikipedia_search="https://en.wikipedia.org/wiki/{}"
     echo "$LAUNCHER_CONTENT" | xargs -I{} "$MYBROWSER" "$wikipedia_search"
   fi
   	
}
export -f wikipedia

#------------------------------------------------------#
# Reddit web search
#------------------------------------------------------#

reddit() {

echo "$KEYWORD60" > "$HOME"/Documents/last_keyword.txt

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     reddit_search="https://www.reddit.com/search/?q={}"
     echo "$LAUNCHER_CONTENT" | xargs -I{} "$MYBROWSER" "$reddit_search"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     reddit_search="https://www.reddit.com/search/?q={}"
     echo "$LAUNCHER_CONTENT" | xargs -I{} "$MYBROWSER" "$reddit_search"
   fi
	
}
export -f reddit

#------------------------------------------------------#
# Google translate (french)
#------------------------------------------------------#

google_trans_fr() {

echo "$KEYWORD02" > "$HOME"/Documents/last_keyword.txt

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     search_translate="https://translate.google.com/?sl=auto&tl=fr&text={}"
     echo "$LAUNCHER_CONTENT" | xargs -I{} "$MYBROWSER" "$search_translate"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     search_translate="https://translate.google.com/?sl=auto&tl=fr&text={}"
     echo "$LAUNCHER_CONTENT" | xargs -I{} "$MYBROWSER" "$search_translate"
   fi
   	
}
export -f google_trans_fr

#------------------------------------------------------#
# Download video
#------------------------------------------------------#

download_video() {

echo "$KEYWORD61" > "$HOME"/Documents/last_keyword.txt
yt-dlp "$LAUNCHER_CONTENT" -o "$HOME/Downloads/%(title)s.%(ext)s" && notify-send "Download completed"
	
}
export -f download_video

#------------------------------------------------------#
# Moving file (find local files)
#------------------------------------------------------#

move_selected_file() {

select_move_source=$(head "$HOME"/Documents/.yad_search_results.txt)
select_move_destination=$(head "$HOME"/Documents/.move_file_destination.txt)
file_moved_notify=$(head "$HOME"/Documents/.yad_search_results.txt | xargs -0 basename)

mv "$select_move_source" "$select_move_destination"
notify-send "$file_moved_notify" "File moved"

}
export -f move_selected_file

#------------------------------------------------------#
# Select file for moving (find local files)
#------------------------------------------------------#

move_dir_main() {

echo "$1" > "$HOME"/Documents/.move_file_destination.txt

}
export -f move_dir_main

#------------------------------------------------------#
# Yad move file (find local files)
#------------------------------------------------------#

dir_destination_move() {

killall yad
dirs6=( ~/Documents/ ~/Downloads/ ~/Videos/ ~/Pictures/ ~/Music/ )
export dirs6

find "${dirs6[@]}" -type d -iname '*' | yad --list \
--column="find" \
--search-column=1 \
--text="\nDouble click directory to move file to and press button\nPress control+f the search\nUse up and down arrows for next and previous search\n" \
--text-align="center" \
--regex-search \
--no-headers \
--separator= \
--width=1000 \
--height=500 \
--dclick-action='/bin/bash -c "move_dir_main %s"' \
--button="_Move:/bin/bash -c 'move_selected_file'" \
--button="_Close":1

}
export -f dir_destination_move

#------------------------------------------------------#
# Copying file (find local files)
#------------------------------------------------------#

copy_selected_file() {

select_copy_source=$(head "$HOME"/Documents/.yad_search_results.txt)
select_copy_destination=$(head "$HOME"/Documents/.copy_file_destination.txt)
file_copied_notify=$(head "$HOME"/Documents/.yad_search_results.txt | xargs -0 basename)

cp "$select_copy_source" "$select_copy_destination"
notify-send "$file_copied_notify" "File copied"

}
export -f copy_selected_file

#------------------------------------------------------#
# Select file for Copying (find local files)
#------------------------------------------------------#

copy_dir_main() {

echo "$1" > "$HOME"/Documents/.copy_file_destination.txt

}
export -f copy_dir_main

#------------------------------------------------------#
# Yad copy file (find local files)
#------------------------------------------------------#

dir_destination() {

killall yad
dirs5=( ~/Documents/ ~/Downloads/ ~/Videos/ ~/Pictures/ ~/Music/ )
export dirs

find "${dirs5[@]}" -type d -iname '*' | yad --list \
--column="find" \
--search-column=1 \
--text="\nDouble click directory to copy file to and press button\nPress control+f the search\nUse up and down arrows for next and previous search\n" \
--text-align="center" \
--regex-search \
--no-headers \
--separator= \
--width=1000 \
--height=500 \
--dclick-action='/bin/bash -c "copy_dir_main %s"' \
--button="_Copy:/bin/bash -c 'copy_selected_file'" \
--button="_Exit":1

}
export -f dir_destination

#------------------------------------------------------#
# Send image notification (find local files)
#------------------------------------------------------#

find_image_notification() {

killall dunst
rm -f ~/Pictures/.image_notifications/*
image_notify_choice=$(cat ~/Documents/.yad_search_results.txt)

if [[ "$(file --mime-type "$image_notify_choice")" == *"webp"* ]]; then
dwebp "$image_notify_choice" -o ~/Pictures/.image_notifications/webp_image.png
notify-send -i ~/Pictures/.image_notifications/webp_image.png selected image
else
notify-send -i "$image_notify_choice" selected image
fi

}
export -f find_image_notification

#------------------------------------------------------#
# Open selected file from (find local files)
#------------------------------------------------------#

open_file() {

temp_search_file=$(head "$HOME"/Documents/.yad_search_results.txt)
killall yad
xdg-open "$temp_search_file"
	
}
export -f open_file

#------------------------------------------------------#
# Select file from list (find local files)
#------------------------------------------------------#

file_results() {

echo "$1" > "$HOME"/Documents/.yad_search_results.txt
	
}
export -f file_results

#------------------------------------------------------#
# Select file and open in file manager  (find local files)
#------------------------------------------------------#

open_file_manager() {

killall yad
chosen_filemanager=$(head "$HOME"/Documents/.yad_search_results.txt)
[ -z "$chosen_filemanager" ] && exit 0

   if [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     "$FILE_MANAGER" "$chosen_filemanager"
     bash ~/bin/switch_to_filemanager.sh
   elif [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     "$FILE_MANAGER" "$chosen_filemanager"
   fi

}
export -f open_file_manager

#------------------------------------------------------#
# Find file (preview audio files)
#------------------------------------------------------#

preview_audio() {

preview_choice=$(cat "$HOME"/Documents/.yad_search_results.txt)

ffplay "$preview_choice" -t "$PREVIEW_AUDIO_TIME" -autoexit -nodisp

}
export -f preview_audio

#------------------------------------------------------#
# Find file (preview video)
#------------------------------------------------------#

preview_video() {

killall dunst
rm -f "$HOME"/Videos/.temp_record_video/*
preview_video_choice=$(cat "$HOME"/Documents/.yad_search_results.txt)

   if [[ "$(file --mime-type "$preview_video_choice")" == *"webp"* ]]; then
     ffmpeg -i "$preview_video_choice" -vframes 1 "$HOME"/Videos/.temp_record_video/thumbnail.png
     notify-send -i ~/Videos/.temp_record_video/thumbnail.png selected image
   else
     convert "$preview_video_choice""$PREVIEW_VIDEO_TIME" "$HOME"/Videos/.temp_record_video/thumbnail.png
     notify-send -i ~/Videos/.temp_record_video/thumbnail.png selected image
   fi

}
export -f preview_video

#------------------------------------------------------#
# Find file (file metadata)
#------------------------------------------------------#

file_metadata() {

killall dunst
metadata_choice=$(cat "$HOME"/Documents/.yad_search_results.txt)	
notify-send "$(file "$metadata_choice" | xargs -0 basename)"

}
export -f file_metadata

#------------------------------------------------------#
# Find local files
#------------------------------------------------------#

find_files() {

echo "$KEYWORD03" > "$HOME"/Documents/last_keyword.txt
# Directories searched
dirs=( ~/Documents/ ~/Downloads/ ~/Videos/ ~/Pictures/ ~/Music/ )

# Yad search settings
echo "$LAUNCHER_CONTENT" > "$HOME"/Documents/.find_file_results.txt
chosen_search_file=$(head "$HOME"/Documents/.find_file_results.txt)
find "${dirs[@]}" -maxdepth 8 -type f -iname "*$chosen_search_file*" | \
GTK_THEME="$GTK_THEMES" yad --list \
--search-column=1 \
--regex-search \
--column="find" \
--text="\nEnter or Double click file and press button\nScroll up and down with arrow keys in list\nHold down alt key and press first letter of the button\nTo search just start typing\nUse up and down arrows for next and previous search\n" \
--text-align="center" \
--width=1200 \
--height=600 \
--separator= \
--no-headers \
--no-markup \
--borders=20 \
--dclick-action='/bin/bash -c "file_results %s"' \
--button="_Data:/bin/bash -c 'file_metadata'" \
--button="_Video:/bin/bash -c 'preview_video'" \
--button="_Audio:/bin/bash -c 'preview_audio'" \
--button="_Image:/bin/bash -c 'find_image_notification'" \
--button="_Move:/bin/bash -c 'dir_destination_move'" \
--button="gtk-copy:/bin/bash -c 'dir_destination'" \
--button="gtk-open:/bin/bash -c 'open_file'" \
--button="_file manager:/bin/bash -c 'open_file_manager'" \
--button="_Exit":01

}
export -f find_files

#------------------------------------------------------#
# Find local files (history)
#------------------------------------------------------#

find_files_history() {

echo "$KEYWORD04" > "$HOME"/Documents/last_keyword.txt
# Directories searched
dirs2=( ~/Documents/ ~/Downloads/ ~/Videos/ ~/Pictures/ ~/Music/ )

# Yad search settings
chosen_search_file=$(head "$HOME"/Documents/.find_file_results.txt)
find "${dirs2[@]}" -maxdepth 8 -type f -iname "*$chosen_search_file*" | \
GTK_THEME="$GTK_THEMES" yad --list \
--column="find" \
--text="\nDouble click file and press button\n" \
--text-align="center" \
--search-column=1 \
--width=1200 \
--height=500 \
--separator= \
--no-headers \
--no-markup \
--borders=20 \
--dclick-action='/bin/bash -c "file_results %s"' \
--button="_Data:/bin/bash -c 'file_metadata'" \
--button="_Video:/bin/bash -c 'preview_video'" \
--button="_Audio:/bin/bash -c 'preview_audio'" \
--button="_Image:/bin/bash -c 'find_image_notification'" \
--button="_Move:/bin/bash -c 'dir_destination_move'" \
--button="gtk-copy:/bin/bash -c 'dir_destination'" \
--button="_Open:/bin/bash -c 'open_file'" \
--button="_File manager:/bin/bash -c 'open_file_manager'" \
--button="_Exit":1
	
}
export -f find_files_history

#------------------------------------------------------#
# Power reboot
#------------------------------------------------------#

yadreboot() {

   if zenity --question --text "Reboot, are you sure"
   then
     poweroff --reboot
   else
     exit 1
   fi

}
export -f yadreboot

#------------------------------------------------------#
# Power shutdown
#------------------------------------------------------#

yadshutdown() {

   if zenity --question --text "Shutdown, are you sure"
   then
     poweroff --poweroff
   else
     exit 1
   fi

}
export -f yadshutdown

#------------------------------------------------------#
# Power lock
#------------------------------------------------------#

yadlock() {

   if zenity --question --text "Lock, are you sure"
   then
     swaylock -c '#000000'
   else
     exit 1
   fi

}
export -f yadlock

#------------------------------------------------------#
# Mute volume
#------------------------------------------------------#

mute_volume() {

echo "$KEYWORD11" > "$HOME"/Documents/last_keyword.txt 
amixer -D pulse set Master 1+ toggle && pkill -RTMIN+10 i3blocks

}
export -f mute_volume

#------------------------------------------------------#
# Dictionary
#------------------------------------------------------#

my_dictionary() {

echo "$KEYWORD08" > "$HOME"/Documents/last_keyword.txt
echo "$LAUNCHER_CONTENT" | xargs dict | GTK_THEME="$GTK_THEMES" yad --text-info \
--wrap \
--width=900 \
--height=600 \
--borders=20 \
--button="_Cancel:1"

}
export -f my_dictionary

#------------------------------------------------------#
# Copy word to clipboard (Spell checker)
#------------------------------------------------------#

copy_spell_word() {

killall yad
head "$HOME"/Documents/.yad_spellcheck_results.txt | \
awk -F ':' '{print $NF}' | sed 's| ||' | wl-copy -n
	
}
export -f copy_spell_word

#------------------------------------------------------#
# Selected word (Spell checker)
#------------------------------------------------------#

spellcheck_results() {

echo "$1" > "$HOME"/Documents/.yad_spellcheck_results.txt
	
}
export -f spellcheck_results

#------------------------------------------------------#
# Spell checker
#------------------------------------------------------#

spell_checker() {

echo "$KEYWORD18" > "$HOME"/Documents/last_keyword.txt
aspell -a list <<< "$LAUNCHER_CONTENT" | \
awk 'BEGIN{FS=","; OFS="\n"} {$1=$1} 1' | GTK_THEME="$GTK_THEMES" yad --list \
--text="Double click on word and press button" \
--text-align="center" \
--width=600 \
--no-markup \
--height=600 \
--borders=20 \
--column="spell" \
--no-headers \
--dclick-action='/bin/bash -c "spellcheck_results %s"' \
--button="_Copy to clipboard:/bin/bash -c 'copy_spell_word'" \
--button="_Exit:1"

}
export -f spell_checker

#------------------------------------------------------#
# QR codes (send image notification)
#------------------------------------------------------#

qr_code_notification() {

echo "$KEYWORD20" > "$HOME"/Documents/last_keyword.txt

   if [[ "$QR_CODE_DIR" == "disable" ]]; then
     notify-send "QR Code directory has been disabled" && exit 1
   else
     reply_action () {

     open_file_path=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     xdg-open "$open_file_path"
	
     }

     forward_action () {

     :
	
     }

     handle_dismiss () {

     :
	
     }

     file_path=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     ACTION=$(dunstify -i "$file_path" -h string:bgcolor:$MY_COLOR_BG -h string:fgcolor:$MY_COLOR_ICON -h string:frcolor:$MY_COLOR_ALERT --action="default,Reply" --action="forwardAction,Forward" "QR Code")

     case "$ACTION" in
     "default")
     reply_action
     ;;
     "forwardAction")
     forward_action
     ;;
     "2")
     handle_dismiss
     ;;
     esac
   fi
	
}
export -f qr_code_notification

#------------------------------------------------------#
# QR codes
#------------------------------------------------------#

qr_codes() {

   if [[ "$QR_CODE_DIR" == "disable" ]]; then
     notify-send "QR Code directory has been disabled" && exit 1
   else
     echo "$KEYWORD05" > "$HOME"/Documents/last_keyword.txt
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     echo "$LAUNCHER_CONTENT" | tr -d '\n' | sed 's/"//g' | qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$QR_CODE_DIR"
     qr_code_notify=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     dunstify -i "$qr_code_notify" -h string:bgcolor:$COLOR_BG -h string:fgcolor:$COLOR_ICON -h string:frcolor:$COLOR_ALERT "Created QR Code"
   fi

}
export -f qr_codes

#------------------------------------------------------#
# Calculator (copy to clipboard)
#------------------------------------------------------#

calculator_clipboard() {

killall yad
head "$HOME"/Documents/.calculator_copy.txt | wl-copy -n
	
}
export -f calculator_clipboard

#------------------------------------------------------#
# Calculator (send sum to text file)
#------------------------------------------------------#

my_calculator_copy() {

echo "$1" > "$HOME"/Documents/.calculator_copy.txt
	
}
export -f my_calculator_copy

#------------------------------------------------------#
# Calculator
#------------------------------------------------------#

my_calculator() {

echo "$KEYWORD06" > "$HOME"/Documents/last_keyword.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.bc_results.txt
echo "$LAUNCHER_CONTENT" | qalc | tail -n 3 | sed 's|>||g' | sed -e 's/\x1b\[[0-9;]*m//g' | \
sed 's| ||'| tee -a "$HOME"/Documents/.bc_results.txt
tac ~/Documents/.bc_results.txt | GTK_THEME="$GTK_THEMES" yad --list \
--title="calculator" \
--text-align="center" \
--text="Double click sum and then press button" \
--width=500 \
--height=500 \
--borders=20 \
--no-markup \
--column="calculator" \
--dclick-action='/bin/bash -c "my_calculator_copy %s"' \
--no-headers \
--button="_Copy to clipboard:/bin/bash -c 'calculator_clipboard'" \
--button="_Exit:1"
	
}
export -f my_calculator

#------------------------------------------------------#
# Calculator history
#------------------------------------------------------#

calculator_history() {

echo "$KEYWORD07" > "$HOME"/Documents/last_keyword.txt
tac ~/Documents/.bc_results.txt | GTK_THEME="$GTK_THEMES" yad --list \
--title="calculator" \
--text-align="center" \
--text="Double click sum and then press button" \
--width=500 \
--height=500 \
--borders=20 \
--no-markup \
--column="calculator" \
--dclick-action='/bin/bash -c "my_calculator_copy %s"' \
--no-headers \
--button="_Copy to clipboard:/bin/bash -c 'calculator_clipboard'" \
--button="_Exit:1"
	
}
export -f calculator_history

#------------------------------------------------------#
# Text to speech (speak)
#------------------------------------------------------#

tts_speak() {

echo "$KEYWORD13" > "$HOME"/Documents/last_keyword.txt
echo "$LAUNCHER_CONTENT" | festival --tts
	
}
export -f tts_speak

#------------------------------------------------------#
# Text to speech (file)
#------------------------------------------------------#

tts_file() {

echo "$KEYWORD14" > "$HOME"/Documents/last_keyword.txt
festival --tts "$LAUNCHER_CONTENT"
	
}
export -f tts_file

#------------------------------------------------------#
# Text to speech (kill)
#------------------------------------------------------#

tts_kill() {

echo "$KEYWORD15" > "$HOME"/Documents/last_keyword.txt
pkill -f audsp

}
export -f tts_kill

#------------------------------------------------------#
# Kill processes with killall command
#------------------------------------------------------#

kill_app() {

echo "$KEYWORD16" > "$HOME"/Documents/last_keyword.txt
killall "$LAUNCHER_CONTENT"
	
}
export -f kill_app

#------------------------------------------------------#
# Kill processes with pkill command
#------------------------------------------------------#

pkill_app() {

echo "$KEYWORD17" > "$HOME"/Documents/last_keyword.txt
pkill -f "$LAUNCHER_CONTENT"
	
}
export -f pkill_app

#------------------------------------------------------#
# Volume
#------------------------------------------------------#

set_volume() {

echo "$KEYWORD12" > "$HOME"/Documents/last_keyword.txt
echo "$LAUNCHER_CONTENT" > "$HOME"/Documents/.set_volume.txt
set_volume_choice=$(< "$HOME"/Documents/.set_volume.txt)
amixer -D pulse sset Master "$set_volume_choice%" && pkill -RTMIN+10 i3blocks	
notify-send "Volume" "$set_volume_choice%"
	
}
export -f set_volume

#------------------------------------------------------#
# Create PDF
#------------------------------------------------------#

create_pdf() {

   if [[ "$PDF_DIR" == "disable" ]]; then
     notify-send "PDF keyword has been disabled" && exit 1
   else
     echo "$KEYWORD22" > "$HOME"/Documents/last_keyword.txt
     rm -f "$HOME"/Documents/.pdftemp/*.*
     libreoffice --headless --convert-to pdf "$LAUNCHER_CONTENT" --outdir "$HOME"/Documents/.pdftemp/ && notify-send "PDF created"
     cp "$HOME"/Documents/.pdftemp/*.* "$PDF_DIR"
   fi
	
}
export -f create_pdf

#------------------------------------------------------#
# PDF (open file)
#------------------------------------------------------#

pdf_open() {

echo "$KEYWORD23" > "$HOME"/Documents/last_keyword.txt

   if [[ "$PDF_DIR" == "disable" ]]; then
     notify-send "PDF keyword has been disabled" && exit 1
   elif [[ -z "$(ls -A "$HOME"/Documents/.pdftemp)" ]]; then	
     notify-send "Pdf directory is empty"
   else
     find "$HOME"/Documents/.pdftemp/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 -d '\n' "$PDF_VIEWER"
   fi
	
}
export -f pdf_open

#------------------------------------------------------#
# PDF print
#------------------------------------------------------#

pdf_print() {

echo "$KEYWORD24" > "$HOME"/Documents/last_keyword.txt

   if [[ "$PDF_DIR" == "disable" ]]; then
     notify-send "PDF keyword has been disabled" && exit 1
   elif [[ -z "$(ls -A "$HOME"/Documents/.pdftemp)" ]]; then
     notify-send "Pdf directory is empty"
   else
    lp -d "$MY_PRINTER" ~/Documents/.pdftemp/*.pdf	
   fi
	
}
export -f pdf_print

#------------------------------------------------------#
# Select screenshot
#------------------------------------------------------#

select_shot() {

echo "$KEYWORD29" > "$HOME"/Documents/last_keyword.txt
rm -f "$HOME"/Pictures/.tempfullscrns/*.png
sleep 1
grimshot save area  ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png && check_sound
cp "$HOME"/Pictures/.tempfullscrns/*.png "$SCREENSHOTS_DIR"
	
}
export -f select_shot

#------------------------------------------------------#
# Full screenshot
#------------------------------------------------------#

full_shot() {

echo "$KEYWORD28" > "$HOME"/Documents/last_keyword.txt
rm -f "$HOME"/Pictures/.tempfullscrns/*.png
sleep 1
grimshot save screen ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png && check_sound 
cp "$HOME"/Pictures/.tempfullscrns/*.png "$SCREENSHOTS_DIR"
	
}
export -f full_shot

#------------------------------------------------------#
# Delay screenshot
#------------------------------------------------------#

delay_shot() {

echo "$KEYWORD30" > "$HOME"/Documents/last_keyword.txt
rm -f "$HOME"/Pictures/.tempfullscrns/*.png
sleep "$DELAY_TIME" && grimshot save screen ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png && check_sound
cp "$HOME"/Pictures/.tempfullscrns/*.png "$SCREENSHOTS_DIR"
	
}
export -f delay_shot

#------------------------------------------------------#
# Full screenshot (open screenshot)
#------------------------------------------------------#

open_shot() {

echo "$KEYWORD31" > "$HOME"/Documents/last_keyword.txt

   if [[ -z "$(ls -A "$HOME"/Pictures/.tempfullscrns)" ]]; then
     notify-send "Directory is empty"
   else
     temp_shot=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)	
     "$IMAGE_VIEWER" "$temp_shot"
   fi

}
export -f open_shot

#------------------------------------------------------#
# Check if camera click sound exists
#------------------------------------------------------#

check_sound() {

if [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
ffplay -autoexit -nodisp ~/Music/sounds/cameraclick.wav
elif [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
:
fi   
         	
}
export -f check_sound

#------------------------------------------------------#
# Record screen (screencast)
#------------------------------------------------------#

record_screen() {

echo "$KEYWORD25" > "$HOME"/Documents/last_keyword.txt
rm -f ~/Videos/.yadvidrec/*
notify-send "Recording started!" && wf-recorder -f "$HOME"/Videos/.yadvidrec/"$(date +'%d-%m-%Y-%H%M%S')".mkv
	
}
export -f record_screen

#------------------------------------------------------#
# Stop screen recording (screencast)
#------------------------------------------------------#

stop_rec() {

echo "$KEYWORD26" > "$HOME"/Documents/last_keyword.txt
killall -s SIGINT wf-recorder
cp "$HOME"/Videos/.yadvidrec/*.mkv "$SCREENCAST_DIR"
notify-send "Recording stopped!"
	
}
export -f stop_rec

#------------------------------------------------------#
# Play screen recording (screencast)
#------------------------------------------------------#

play_rec() {

echo "$KEYWORD27" > "$HOME"/Documents/last_keyword.txt

   if [[ -z "$(ls -A "$HOME"/Videos/.yadvidrec)" ]]; then	
     notify-send "Pdf directory is empty"
   else
     find "$HOME"/Videos/.yadvidrec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 -d '\n' "$VIDEO_PLAYER"
   fi

}
export -f play_rec

#------------------------------------------------------#
# Exec terminal commands
#------------------------------------------------------#

cli_commands() {

echo "$KEYWORD33" > "$HOME"/Documents/last_keyword.txt
"$MYTERMINAL" -- /bin/bash -c "$LAUNCHER_CONTENT; $SHELL"

}
export -f cli_commands

#------------------------------------------------------#
# Password generator
#------------------------------------------------------#

pass_gen() {

echo "$KEYWORD19" > "$HOME"/Documents/last_keyword.txt
tr -dc A-Za-z0-9 </dev/urandom | head -c "$LAUNCHER_CONTENT" | wl-copy -n

}
export -f pass_gen

#------------------------------------------------------#
# Resize image
#------------------------------------------------------#

resize_image() {

echo "$KEYWORD34" > "$HOME"/Documents/last_keyword.txt
wl-paste > "$HOME"/Pictures/.temp_resize_image.txt
sed -i 's|file:\//||' "$HOME"/Pictures/.temp_resize_image.txt
head "$HOME"/Pictures/.temp_resize_image.txt | xargs | tr -d '\r' | \
sed 's|%20| |g' | tee "$HOME"/Pictures/.temp_resize_image.txt
convert_file=$(head "$HOME"/Pictures/.temp_resize_image.txt)
notify_filename=$(head "$HOME"/Pictures/.temp_resize_image.txt | xargs -0 basename)

convert "$convert_file" -resize "$LAUNCHER_CONTENT" "$HOME/Pictures/resize_$(date +'%d-%m-%Y-%H%M%S')".png
notify-send "converted image $notify_filename"
	
}
export -f resize_image

#------------------------------------------------------#
# Switch to already open apps
#------------------------------------------------------#
# This works only in sway

app_running() {

   if [[ "$DESKTOP_SESSION" == 'sway' ]]; then
     swaymsg -t get_tree \
       | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"' \
       | grep -i "$BANG" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' focus
   else
      /bin/bash -c "$BANG" || exit 1
   fi 
	
}
export -f app_running

#------------------------------------------------------#
# Open new instances of apps
#------------------------------------------------------#

new_instance() {

   if [[ "$BANG" == 'foot' ]]; then
     /bin/bash -c foot || exit 1
   else
     /bin/bash -c "$BANG" || exit 1
   fi
	
}
export -f new_instance

#------------------------------------------------------#
# Emoji Search
#------------------------------------------------------#

emoji_search() {

echo "$KEYWORD67" > "$HOME"/Documents/last_keyword.txt
emoji_select_choice=$(grep -i "$LAUNCHER_CONTENT" "$EMOJI_TEXT")
emojo_search_choice=$(echo "$emoji_select_choice" | GTK_THEME="$GTK_THEMES" yad --list \
--column="emoji" \
--regex-search \
--title="Emoji's" \
--text="Enter or Double click emoji to copy to clipboard\nTo search just start typing\nUse up and down arrows for next and prev search.\n" \
--text-align="center" \
--borders=20 \
--no-headers \
--separator= \
--width=600 \
--height=500 \
--no-markup \
--button="_Exit":1)
[[ -z "$emojo_search_choice" ]] && exit

echo "$emojo_search_choice" | awk '{print $1}' | wl-copy -n
	
}
export -f emoji_search

#------------------------------------------------------#
# Emoji
#------------------------------------------------------#

emoji_list() {

echo "$KEYWORD35" > "$HOME"/Documents/last_keyword.txt
emoji_cat_text=$(cat "$EMOJI_TEXT")
emoji_choice=$(echo "$emoji_cat_text" | GTK_THEME="$GTK_THEMES" yad --list --search-column=1 \
--column="emoji" \
--regex-search \
--title="Emoji's" \
--text="Double click to copy to clipboard\nType control+f to search\n" \
--text-align="center" \
--borders=20 \
--no-headers \
--separator= \
--width=600 \
--height=500 \
--no-markup \
--button="_Exit":1)
[[ -z "$emoji_choice" ]] && exit

echo "$emoji_choice" | awk '{print $1}' | wl-copy -n

}
export -f emoji_list

#------------------------------------------------------#
# One line note (copy note to clipboard)
#------------------------------------------------------#

open_note_text_file() {

killall yad

   if [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     "$TEXT_EDITOR" "$QNOTES_FILE"
     bash ~/bin/switch_to_texteditor.sh
   elif [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     "$TEXT_EDITOR" "$QNOTES_FILE"
   else
     :
   fi
	
}
export -f open_note_text_file

#------------------------------------------------------#
# One line note (copy note to clipboard)
#------------------------------------------------------#

one_line_note_copy() {

killall yad
head "$HOME"/Documents/.one_line_temp_note.txt | wl-copy -n
	
}
export -f one_line_note_copy

#------------------------------------------------------#
# One line note (send selection to file)
#------------------------------------------------------#

one_line_note_select() {

echo "$1" > "$HOME"/Documents/.one_line_temp_note.txt
	
}
export -f one_line_note_select

#------------------------------------------------------#
# One line note (create note and view note)
#------------------------------------------------------#

one_line_note() {

   if [[ "$QNOTES_FILE" == "disable" ]]; then
     notify-send "Quick notes has been disabled"
   else
     echo "$KEYWORD36" > "$HOME"/Documents/last_keyword.txt
     printf '%s\n' "$LAUNCHER_CONTENT" >> "$QNOTES_FILE"
     note_file=$(tac "$QNOTES_FILE")
     sed -i '/^[[:space:]]*$/d' "$QNOTES_FILE"

     echo "$note_file" | GTK_THEME="$GTK_THEMES" yad --list \
     --search-column=1 \
     --column="notes" \
     --regex-search \
     --title="One line note" \
     --text="Double click note and press button\nType control+f to search\n" \
     --text-align="center" \
     --borders=20 \
     --no-headers \
     --separator= \
     --width=800 \
     --height=500 \
     --no-markup \
     --dclick-action='/bin/bash -c "one_line_note_select %s"' \
     --button="_Copy to clipboard:/bin/bash -c 'one_line_note_copy'" \
     --button="_Open text file:/bin/bash -c 'open_note_text_file'" \
     --button="_Exit":1
   fi
	
}
export -f one_line_note

#------------------------------------------------------#
# One line note (history)
#------------------------------------------------------#

one_line_note_history() {

   if [[ "$QNOTES_FILE" == "disable" ]]; then
        notify-send "Quick notes has been disabled"
   else
     echo "$KEYWORD37" > "$HOME"/Documents/last_keyword.txt
     note_file_history=$(tac "$QNOTES_FILE")
     echo "$note_file_history" | GTK_THEME="$GTK_THEMES" yad --list \
     --search-column=1 \
     --column="notes" \
     --regex-search \
     --title="One line note" \
     --text="Double click note and press button\nType control+f to search\n" \
     --text-align="center" \
     --borders=20 \
     --no-headers \
     --separator= \
     --width=800 \
     --height=500 \
     --no-markup \
     --dclick-action='/bin/bash -c "one_line_note_select %s"' \
     --button="_Copy to clipboard:/bin/bash -c 'one_line_note_copy'" \
     --button="_Open text file:/bin/bash -c 'open_note_text_file'" \
     --button="_Exit":1
   fi
	
}
export -f one_line_note_history

#------------------------------------------------------#
# Decode QR Code (copy qr code to clipboard)
#------------------------------------------------------#

decode_qr_code_copy() {

killall yad
head "$HOME"/Documents/.qr_code_decoded_text.txt | wl-copy -n
	
}
export -f decode_qr_code_copy

#------------------------------------------------------#
# Decode QR Code (select decoded qr code text)
#------------------------------------------------------#

decode_qr_code_select() {

echo "$1" > "$HOME"/Documents/.qr_code_decoded_text.txt
	
}
export -f decode_qr_code_select

#------------------------------------------------------#
# Decode QR Code
#------------------------------------------------------#

decode_qr_code() {

   if [[ "$QR_CODE_DIR" == "disable" ]]; then
     notify-send "QR Code directory has been disabled" && exit 1
   else
     echo "$KEYWORD38" > "$HOME"/Documents/last_keyword.txt
     rm -f "$HOME"/Pictures/.ocr/*.png
     grimshot save area ~/Pictures/.ocr/ocr.png
     find "$HOME"/Pictures/.ocr/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw | \
     GTK_THEME="$GTK_THEMES" yad --list \
     --search-column=1 \
     --column="qr codes" \
     --regex-search \
     --title="One line note" \
     --text="Double click text and press button\nType control+f to search\n" \
     --text-align="center" \
     --borders=20 \
     --no-headers \
     --separator= \
     --width=800 \
     --height=500 \
     --no-markup \
     --dclick-action='/bin/bash -c "decode_qr_code_select %s"' \
     --button="_Copy to clipboard:/bin/bash -c 'decode_qr_code_copy'" \
     --button="_Exit":1
   fi

}
export -f decode_qr_code

#------------------------------------------------------#
# HEX to RGB
#------------------------------------------------------#

hex_to_rgb() {

echo "$KEYWORD42" > "$HOME"/Documents/last_keyword.txt
my_hex_choice=$(printf '%s' "$LAUNCHER_CONTENT" | sed 's|#||')
hex="$my_hex_choice"
printf "%d %d %d\n" 0x"${hex:0:2}" 0x"${hex:2:2}" 0x"${hex:4:2}" | wl-copy -n

}
export -f hex_to_rgb

#------------------------------------------------------#
# RGB to HEX
#------------------------------------------------------#

rgb_to_hex() {

echo "$KEYWORD41" > "$HOME"/Documents/last_keyword.txt
printf "%02X%02X%02X" ${*//','/' '}
[ $(basename -- $0) == "hex" ] && [ $# -gt 0 ] && echo $(hex "${*}")

}
export -f rgb_to_hex

#------------------------------------------------------#
# Calls RGB to HEX
#------------------------------------------------------#

call_rgb_to_hex() {

rgb_to_hex "$LAUNCHER_CONTENT" | sed 's|^|#|' | xargs wl-copy -n

}
export -f call_rgb_to_hex

#------------------------------------------------------#
# Color picker
#------------------------------------------------------#

color_picker() {

echo "$KEYWORD40" > "$HOME"/Documents/last_keyword.txt
grim -g "$(slurp -p)" -t ppm - | convert - -format '%[pixel:p{0,0}]' txt:- | tail -n 1 | \
awk '{print $3}' | xargs wl-copy -n

}
export -f color_picker

#------------------------------------------------------#
# Command palette for brave MYBROWSER
#------------------------------------------------------#
# This works only in sway.

brave_palette() {

echo "$KEYWORD43" > "$HOME"/Documents/last_keyword.txt
brave_pchoice=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | \
tr -d '"' | tr '[:upper:]' '[:lower:]')

   # New tab
   if [[ "$brave_pchoice" == "brave" && "$LAUNCHER_CONTENT" == 'new' ]]; then
     wtype -M ctrl -k t
   # Close tab
   elif [[ "$brave_pchoice" == "brave" && "$LAUNCHER_CONTENT" == 'close' ]]; then
     wtype -M ctrl -k w
   # Save bookmark
   elif [[ "$brave_pchoice" == "brave" && "$LAUNCHER_CONTENT" == 'save' ]]; then
     wtype -M ctrl -k d
   # Switch tab 1
   elif [[ "$brave_pchoice" == "brave" && "$LAUNCHER_CONTENT" == '1' ]]; then
     wtype -M ctrl -k 1
   # Switch tab 2
   elif [[ "$brave_pchoice" == "brave" && "$LAUNCHER_CONTENT" == '2' ]]; then
     wtype -M ctrl -k 2
   # Switch tab 3
   elif [[ "$brave_pchoice" == "brave" && "$LAUNCHER_CONTENT" == '3' ]]; then
     wtype -M ctrl -k 3
   # Switch tab 4
   elif [[ "$brave_pchoice" == "brave" && "$LAUNCHER_CONTENT" == '4' ]]; then
     wtype -M ctrl -k 4
   # Switch tab 5
   elif [[ "$brave_pchoice" == "brave" && "$LAUNCHER_CONTENT" == '5' ]]; then
     wtype -M ctrl -k 5
   # Switch tab 6
   elif [[ "$brave_pchoice" == "brave" && "$LAUNCHER_CONTENT" == '6' ]]; then
     wtype -M ctrl -k 6
   # History
   elif [[ "$brave_pchoice" == "brave" && "$LAUNCHER_CONTENT" == 'history' ]]; then
     wtype -M ctrl -k h
   # Download
   elif [[ "$brave_pchoice" == "brave" && "$LAUNCHER_CONTENT" == 'download' ]]; then
     wtype -M ctrl -k j
   # Reopen tab
   elif [[ "$brave_pchoice" == "brave" && "$LAUNCHER_CONTENT" == 'reopen' ]]; then
     wtype -M ctrl -M shift -k t
   # Bookmark manager
   elif [[ "$brave_pchoice" == "brave" && "$LAUNCHER_CONTENT" == 'manager' ]]; then
     wtype -M ctrl -M shift -k o
   else
     notify-send "Something went wrong!"
   fi

}
export -f brave_palette

#------------------------------------------------------#
# Check if your running wayland (Brave command palette)
#------------------------------------------------------#

check_brave() {

   if [[ "$DESKTOP_SESSION" == 'sway' ]]; then
     brave_palette
   else
     notify-send "Wayland not detected!"
   fi
	
}
export -f check_brave

#------------------------------------------------------#
# Radio (plays radio)
#------------------------------------------------------#

play_mpv() {

killall mpv
echo "$1" | tee "$HOME"/Documents/.temp_radio_stream.txt
temp_cat_file=$(cat "$HOME"/Documents/.temp_radio_stream.txt)
temp_file=$(echo "$temp_cat_file" | awk '{print $NF}')
echo "$temp_file" > "$HOME"/Documents/.lasted_played.txt

mpv --no-video "$temp_file" 
	
}
export -f play_mpv

#------------------------------------------------------#
# Radio
#------------------------------------------------------#

my_radio() {

   if [[ "$RADIO_LINKS" == "disable" ]]; then
     notify-send "Radio has been disable" && exit 1
   else
     echo "$KEYWORD58" > "$HOME"/Documents/last_keyword.txt
     radio_cat_links=$(cat "$RADIO_LINKS")
     echo "$radio_cat_links" | sort | GTK_THEME="$GTK_THEMES" yad --list --column="radio" \
     --title="Online radio" \
     --borders=20 \
     --no-markup \
     --search-column=1 \
     --text="\nDouble click to play radio station\nUse keyword: p to control player\nPress control+f to search\n" \
     --text-align="center" \
     --height=550 \
     --width=900 \
     --no-headers \
     --buttons-layout="center" \
     --separator= \
     --dclick-action='/bin/bash -c "play_mpv %s"' \
     --button="_Exit":1 
   fi

}
export -f my_radio

#------------------------------------------------------#
# Control music players with playerctl (play last played track)
#------------------------------------------------------#
# What ever program your playing music with add this below into playing function.
#check_last_played=$(playerctl metadata -f '{{xesam:url}}')
#echo "$check_last_played" > "$HOME"/Documents/.lasted_played.txt

lasted_played() {

sed -i 's|file://||' "$HOME"/Documents/.lasted_played.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.lasted_played.txt
sed -i 's|%20| |g' "$HOME"/Documents/.lasted_played.txt
last_played_file=$(cat "$HOME"/Documents/.lasted_played.txt)

   if [[ "$last_played_file" == *"http"* ]]; then
     "$VIDEO_PLAYER" "$last_played_file"
   else
     notify-send "Something went wrong with lasted_played function!"
   fi
   	
}
export -f lasted_played

#------------------------------------------------------#
# Control music players with playerctl (stopping players)
#------------------------------------------------------#

stop_players() {

   if [[ "$(pidof mpv)" ]]; then
     killall mpv
   elif [[ "$(pidof audacious)" ]]; then
     killall audacious
   elif [[ "$(pidof ffplay)" ]]; then
     killall ffplay
   elif [[ "$(pidof rhythmbox)" ]]; then
     killall rhythmbox
   else
     playerctl stop
   fi
	
}
export -f stop_players

#------------------------------------------------------#
# Create QR Code from radio artist and title
#------------------------------------------------------#

qr_code_artist() {

   if [[ "$QR_CODE_DIR" == "disable" ]]; then
     notify-send "QR Code script has been disabled" && exit 1
   else 
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     playerctl metadata --format '{{xesam:artist}}{{xesam:title}}' | \
     tr -d '\n' | sed 's/"//g' | qrencode -m 5 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$QR_CODE_DIR"
     find "$HOME"/Pictures/.temqrcodes/*.png -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw | \
     tee ~/Documents/.keys_player_title.txt
     notification_player=$(head ~/Documents/.keys_player_title.txt)
     qr_code_mnotify=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     dunstify -i "$qr_code_mnotify" -h string:bgcolor:$COLOR_BG -h string:fgcolor:$COLOR_ICON -h string:frcolor:$COLOR_ALERT "$notification_player"
   fi
	
}
export -f qr_code_artist

#------------------------------------------------------#
# Control music players with playerctl
#------------------------------------------------------#

music_player() {

echo "$KEYWORD44" > "$HOME"/Documents/last_keyword.txt

   # Play
   if [[ "$LAUNCHER_CONTENT" == "play" ]]; then
     playerctl play-pause
   # Stop
   elif [[ "$LAUNCHER_CONTENT" == "stop" ]]; then
     stop_players
   # Next
   elif [[ "$LAUNCHER_CONTENT" == "next" ]]; then
     playerctl next
   # Prev
   elif [[ "$LAUNCHER_CONTENT" == "prev" ]]; then
     playerctl previous
   # Status
   elif [[ "$LAUNCHER_CONTENT" == "stat" ]]; then
     notify-send "$(playerctl status)"
   # Artist and title
   elif [[ "$LAUNCHER_CONTENT" == "art" ]]; then
     notify-send "$(playerctl metadata --format '{{xesam:artist}} - {{xesam:title}}')"
   # Create qr code from artist and title
   elif [[ "$LAUNCHER_CONTENT" == "qr" ]]; then
     qr_code_artist
   elif [[ "$LAUNCHER_CONTENT" == "last" ]]; then
     lasted_played
   else
     notify-send "Something went wrong!"
   fi
	
}
export -f music_player

#------------------------------------------------------#
# Volume status
#------------------------------------------------------#

volume_status() {

echo "$KEYWORD45" > "$HOME"/Documents/last_keyword.txt
vol_status01=$(amixer -D pulse get Master | tail -1 | awk '{print $5}')
vol_status02=$(amixer -D pulse get Master | tail -1 | awk '{print $6}')

notify-send "Volume: $vol_status01 $vol_status02"
	
}
export -f volume_status

#------------------------------------------------------#
# Open terminal here in thunar directory
#------------------------------------------------------#
# This works only in sway.
# Run this command before you run this function.
# $ xfconf-query -c thunar -p /misc-full-path-in-window-title -t bool -s true --create

open_ter_here() {

echo "$KEYWORD46" > "$HOME"/Documents/last_keyword.txt
choice_home=$(swaymsg -t get_tree | \
jq -r '.. | select(.type?) | select(.focused==true).name' | \
sed 's| - Thunar||' | sed 's/^/"/;s/$/"/')
choice_root=$(swaymsg -t get_tree | \
jq -r '.. | select(.type?) | select(.focused==true).name' | \
sed 's| - Thunar||' | sed 's/^/"/;s/$/"/')

   if [[ "$choice_home" == '/home/' ]]; then
     "$OTHER_TERMINAL" -- /bin/bash -c "cd ~/$choice_home; $SHELL"
     "$OTHER_TERMINAL" -- /bin/bash -c "cd ~/$choice_home; $SHELL"
   elif [[ ! "$choice_root" == '/home/' ]]; then
     "$OTHER_TERMINAL" -- /bin/bash -c "cd $choice_root; $SHELL"      
     :
   else
     notify-send "Sway not detected!"
   fi

# Another method
#choice_ter=$(swaymsg -t get_tree | \
#jq -r '.. | select(.type?) | select(.focused==true).name' | \
#cut -d'/' -f4-)
#gnome-terminal --working-directory="$choice_ter"
	
}
export -f open_ter_here

#------------------------------------------------------#
# Change text from uppercase to lowercase
#------------------------------------------------------#

upper_to_lower() {

echo "$KEYWORD47" > "$HOME"/Documents/last_keyword.txt
echo "$LAUNCHER_CONTENT" | tr '[:upper:]' '[:lower:]' | wl-copy -n

}
export -f upper_to_lower

#------------------------------------------------------#
# Timezone Los Angeles
#------------------------------------------------------#

los_angeles() {

echo "$KEYWORD48" > "$HOME"/Documents/last_keyword.txt
notify-send "$(date --date='TZ="'America/Los_Angeles'"'" $LAUNCHER_CONTENT")"

}
export -f los_angeles

#------------------------------------------------------#
# Timezone New York
#------------------------------------------------------#

new_york() {

echo "$KEYWORD49" > "$HOME"/Documents/last_keyword.txt
notify-send "$(date --date='TZ="'America/New_York'"'" $LAUNCHER_CONTENT")"

}
export -f new_york

#------------------------------------------------------#
# Timezone Sydney
#------------------------------------------------------#

sydney() {

echo "$KEYWORD50" > "$HOME"/Documents/last_keyword.txt
notify-send "$(date --date='TZ="'Australia/Sydney'"'" $LAUNCHER_CONTENT")"

}
export -f sydney

#------------------------------------------------------#
# Yad timezones
#------------------------------------------------------#

yad_timezone() {

echo "$KEYWORD51" > "$HOME"/Documents/last_keyword.txt	
GTK_THEME="$GTK_THEMES" yad --list \
--title="" \
--text="<span color='#FFFFFF' font_family='Cascadia Mono' font='18' weight='bold' rise='0pt'>\n$(date)\n</span>" \
--text-align="center" \
--width=700 \
--height=210 \
--separator= \
--column="Double click on item to launch it" \
--buttons-layout="center" \
--borders=10 \
--no-headers \
--center \
--button="_Exit":1
}
export -f yad_timezone

#------------------------------------------------------#
# Timezones
#------------------------------------------------------#

main_timezones() {

zoneone=$(TZ=America/Los_Angeles date) 
zonetwo=$(TZ=America/New_York date)
zonethree=$(TZ=Australia/Sydney date)
zonefour=$(TZ=Asia/Tokyo date)
zonefive=$(TZ=Europe/London date)
zonesix=$(TZ=Asia/Jerusalem date)

textone="America/Los Angeles"
texttwo="America/New York"
textthree="Australia/Sydney"
textfour="Japan/Tokyo"
textfive="Europe/London"
textsix="Asia/Jerusalem"

com01=$(echo -e "${zoneone} ${textone}")
com02=$(echo -e "${zonetwo} ${texttwo}")
com03=$(echo -e "${zonethree} ${textthree}")
com04=$(echo -e "${zonefour} ${textfour}")
com05=$(echo -e "${zonefive} ${textfive}")
com06=$(echo -e "${zonesix} ${textsix}")

tz_choice=$(echo -e "$com01\n$com02\n$com03\n$com04\n$com05\n$com06\n" | yad_timezone)
[ -z "$tz_choice" ] && exit 0
echo "$tz_choice" | wl-copy -n

}
export -f main_timezones

#------------------------------------------------------#
# Notifications
#------------------------------------------------------#

control_notifications() {

echo "$KEYWORD52" > "$HOME"/Documents/last_keyword.txt

   if [[ "$LAUNCHER_CONTENT" == 'history' ]]; then
     dunstctl history-pop
   elif [[ "$LAUNCHER_CONTENT" == 'toggle' ]]; then 
     dunstctl set-paused toggle
   else
     :
   fi

}
export -f control_notifications

#------------------------------------------------------#
# Microphone
#------------------------------------------------------#

my_microphone() {

echo "$KEYWORD53" > "$HOME"/Documents/last_keyword.txt

   if [[ "$LAUNCHER_CONTENT" == 'on' ]]; then
     pactl set-source-mute @DEFAULT_SOURCE@ 0
   elif [[ "$LAUNCHER_CONTENT" == 'off' ]]; then
     pactl set-source-mute @DEFAULT_SOURCE@ 1
   elif [[ "$LAUNCHER_CONTENT" == 'level' ]]; then
     notify-send "$(pacmd list-sources | grep -e 'volume: front-left:' | awk 'NR==2 {print $5}')"
   else
     pactl set-source-volume "$(pactl get-default-source)" "$LAUNCHER_CONTENT%"
   fi

}
export -f my_microphone

#------------------------------------------------------#
# Zipping files
#------------------------------------------------------#

zip_files() {

   if [[ "$ZIPS_DIR" == "disable" ]]; then
     notify-send "Zip keyword has been disabled" && exit 1
   else
     echo "$KEYWORD54" > "$HOME"/Documents/last_keyword.txt
     rm -f "$HOME"/Documents/.zips/*.*
     wl-paste > "$HOME"/Documents/.zipfile.txt
     sed -i 's|file://||' "$HOME"/Documents/.zipfile.txt
     sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.zipfile.txt
     zip_text=$(cat "$HOME"/Documents/.zipfile.txt)
     echo "$zip_text" | tr -d '\r' | tee "$HOME"/Documents/.zipfile.txt
     sed -i 's|%20| |g' "$HOME"/Documents/.zipfile.txt
     xargs -L 1 -a "$HOME"/Documents/.zipfile.txt -d '\n' zip -j "$HOME"/Documents/.zips/"$(date +'%d-%m-%Y-%H%M%S')".zip	
     cp "$HOME"/Documents/.zips/*.* "$ZIPS_DIR"
   fi
	
}
export -f zip_files

#------------------------------------------------------#
# Plays videos in mpv via http link
#------------------------------------------------------#

mpv_play() {

echo "$KEYWORD55" > "$HOME"/Documents/last_keyword.txt

   if [[ "$LAUNCHER_CONTENT" == 'stop' ]]; then
     killall mpv
   else
     "$VIDEO_PLAYER" "$LAUNCHER_CONTENT"
   fi
	
}
export -f mpv_play

#------------------------------------------------------#
# Open image in gimp (Last modified files)
#------------------------------------------------------#

mod_image_gimp() {

killall yad
temp_mod_gimp=$(head "$HOME"/Documents/.temp_mod_selected_file.txt)
"$IMAGE_EDITOR" "$temp_mod_gimp"

}
export -f mod_image_gimp

#------------------------------------------------------#
# Open selected file from (Last modified files)
#------------------------------------------------------#

mod_files_open() {

temp_mod_file=$(head "$HOME"/Documents/.temp_mod_selected_file.txt)
export temp_mod_file
killall yad
xdg-open "$temp_mod_file"
	
}
export -f mod_files_open

#------------------------------------------------------#
# Select file and open in file manager (Last modified files)
#------------------------------------------------------#

open_mod_file_manager() {

killall yad
mod_filemanager=$(head "$HOME"/Documents/.temp_mod_selected_file.txt)
[ -z "$mod_filemanager" ] && exit 0
 
   if [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     "$FILE_MANAGER" "$mod_filemanager"
     bash ~/bin/switch_to_filemanager.sh
   elif [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     "$FILE_MANAGER" "$mod_filemanager"
   else
     :
   fi

}
export -f open_mod_file_manager

#------------------------------------------------------#
# Select file from list (Last modified files)
#------------------------------------------------------#

mod_files_select() {

echo "$1" > "$HOME"/Documents/.temp_mod_selected_file.txt
echo "$1" > "$HOME"/Documents/.yad_search_results.txt
	
}
export -f mod_files_select

#------------------------------------------------------#
# Last modified files
#------------------------------------------------------#

last_modified_file() {

echo "$KEYWORD56" > "$HOME"/Documents/last_keyword.txt
# Selected directories
dirs3=( ~/Documents/ ~/Downloads/ ~/Videos/ ~/Pictures/ ~/Music/ )

# Documents
mod_choice01=$(find "${dirs3[@]}" -type f \( -iname '*.doc' -o -iname '*.docx' -o -iname '*.pdf' -o -iname '*.ods' -o -iname '*.xlsx' -o -iname '*.odt' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
# Downloads
mod_choice02=$(find ~/Downloads/ -not -path '*/.*' -type f -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
# Images
mod_choice03=$(find "${dirs3[@]}" -type f \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
# Videos
mod_choice04=$(find "${dirs3[@]}" -type f \( -iname '*.mp4' -o -iname '*.mkv' -o -iname '*.webm' -o -iname '*.avi' -o -iname '*.wmv' -o -iname '*.mov' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
# Sounds
mod_choice05=$(find "${dirs3[@]}" -type f \( -iname '*.mp3' -o -iname '*.wav' -o -iname '*.aiff' -o -iname '*.acc' -o -iname '*.flac' -o -iname '*.wma' -o -iname '*.m4a' -o -iname '*.ogg' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")

# Yad settings
echo -e "$mod_choice01\n$mod_choice02\n$mod_choice03\n$mod_choice04\n$mod_choice05" | GTK_THEME="$GTK_THEMES" yad --list \
--column="mod" \
--title="Last Modified file" \
--text="\nLast Modified files\nPress enter or double click file and press button\n" \
--text-align="center" \
--width=1100 \
--height=500 \
--separator= \
--buttons-layout="center" \
--borders=10 \
--no-headers \
--center \
--dclick-action='/bin/bash -c "mod_files_select %s"' \
--button="_Data:/bin/bash -c 'file_metadata'" \
--button="_Gimp:/bin/bash -c 'mod_image_gimp'" \
--button="_Video:/bin/bash -c 'preview_video'" \
--button="_Audio:/bin/bash -c 'preview_audio'" \
--button="_Image:/bin/bash -c 'find_image_notification'" \
--button="_Move:/bin/bash -c 'dir_destination_move'" \
--button="gtk-copy:/bin/bash -c 'dir_destination'" \
--button="_Open:/bin/bash -c 'mod_files_open'" \
--button="_File manager:/bin/bash -c 'open_mod_file_manager'" \
--button="_Exit":1

}
export -f last_modified_file

#------------------------------------------------------#
# Record audio from microphone
#------------------------------------------------------#

record_mic_audio() {

echo "$KEYWORD57" > "$HOME"/Documents/last_keyword.txt

   if [[ "$LAUNCHER_CONTENT" == 'stop' ]]; then
     killall arecord
     cp "$HOME"/Music/.yadaudiorec/*.* "$AUDIO_DIR"
     notify-send "Audio recording stopped"
   elif [[ "$LAUNCHER_CONTENT" == 'open' ]]; then
     find "$HOME"/Music/.yadaudiorec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs "$AUDIO_PLAYER"
   else 
     rm -f "$HOME"/Music/.yadaudiorec/*.*
     notify-send "Recording audio only" ; arecord -f S16_LE -t wav "$HOME/Music/.yadaudiorec/audio_$(date '+%d%m%Y-%H%M-%S').wav"
   fi

}
export -f record_mic_audio

#------------------------------------------------------#
# Empty trash
#------------------------------------------------------#

trash_clear() {

echo "$KEYWORD63" > "$HOME"/Documents/last_keyword.txt

   if [[ -z "$(ls -A "$HOME"/.local/share/Trash)" ]]; then	
     notify-send "trash is empty"
   else
     notify-send "Trash emptied" && rm -rf "$HOME"/.local/share/Trash/*	
   fi
	
}
export -f trash_clear

#------------------------------------------------------#
# Open yad keywords config
#------------------------------------------------------#

open_config() {

echo "$KEYWORD64" > "$HOME"/Documents/last_keyword.txt

   if [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     "$TEXT_EDITOR" ~/bin/yad_keywords_config.sh
     bash ~/bin/switch_to_texteditor.sh
   elif [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     "$TEXT_EDITOR" ~/bin/yad_keywords_config.sh
   else
     :
   fi

}
export -f open_config

#------------------------------------------------------#
# Audacious player (playlist)
#------------------------------------------------------#

audacious_player_playlist() {

echo "$KEYWORD66" > "$HOME"/Documents/last_keyword.txt
audtool --set-current-playlist "$LAUNCHER_CONTENT"
	
}
export -f audacious_player_playlist

#------------------------------------------------------#
# Audacious player
#------------------------------------------------------#

audacious_player() {

echo "$KEYWORD65" > "$HOME"/Documents/last_keyword.txt

   if [[ "$LAUNCHER_CONTENT" == 'play' ]]; then
     audacious -H --play-pause
   elif [[ "$LAUNCHER_CONTENT" == 'current' ]]; then
     notify-send "$(audtool --current-playlist)"
   elif [[ "$LAUNCHER_CONTENT" == 'toggle' ]]; then
     audtool --playlist-shuffle-toggle
   else
     notify-send "Something went wrong with audacious_player"
   fi
	
}
export -f audacious_player

#------------------------------------------------------#
# Open apps text file
#------------------------------------------------------#

open_apps_text_file() {

killall yad
"$TEXT_EDITOR" "$HOME"/Documents/apps.txt
	
}
export -f open_apps_text_file

#------------------------------------------------------#
# Yad settings
#------------------------------------------------------#

_yad() {

GTK_THEME="$GTK_THEMES" yad --entry \
--title="Run launcher" \
--width=600 \
--text-align="left" \
--image-on-top \
--height=100 \
--editable \
--rest="$MY_APPS_FILE" \
--text="Enter program to open or move to or keyword.." \
--center \
--borders=20 \
--buttons-layout="spread" \
--button="gtk-execute:0" \
--button="_Help:/bin/bash -c 'show_keywords'" \
--button="gtk-file:/bin/bash -c 'open_apps_text_file'" \
--button="gtk-cancel:1"

}
export -f _yad

