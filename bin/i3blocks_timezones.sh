#!/bin/bash

# Program command: i3blocks_timezones.sh
# Description: Changes the i3blocks timezone for time module.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 03-10-2024
# Program_license: GPL 3.0
# Dependencies: yad

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-----------------------------------------------------------#
# User preferences
#-----------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Timezone
MY_TIMEZONE="$MODULE_TIMEZONE"
export MY_TIMEZONE

#-----------------------------------------------------------#
# Colors
#-----------------------------------------------------------#

# Currently set timezone
COLOR1="<span color='#40FF00' font_family='Monospace' font='16'>"
export COLOR1
# Color end
END1="</span>"

#-----------------------------------------------------------#
# Yad dialog
#-----------------------------------------------------------#

field_choice=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--height=100 \
--width=600 \
--separator= \
--borders=10 \
--text-align="center" \
--text="\nAvailable timezones: la, gb, ny, au, jp, de \nCurrently set timezone: ${COLOR1}$MY_TIMEZONE${END1}\nEnter your timezone...\n" \
--button="Ok":0 \
--button="Exit":1)
[ -z "$field_choice" ] && exit 0

#-----------------------------------------------------------#
# Main
#-----------------------------------------------------------#

sed -i "s|.*\(MODULE_TIMEZONE=\).*|MODULE_TIMEZONE=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

#-----------------------------------------------------------#
# Unset variables and functions
#-----------------------------------------------------------#

unset field_choice 
unset MY_GTK_THEME
unset MY_TIMEZONE
unset COLOR1
unset END1
