#!/bin/bash

choice=$(swaymsg -t get_workspaces | jq '.[] | .num' | xargs)

   if [[ ! "$choice" == *"1"* ]]; then
     swaymsg move container to workspace 1
   elif [[ ! "$choice" == *"2"* ]]; then
     swaymsg move container to workspace 2
   elif [[ ! "$choice" == *"3"* ]]; then
     swaymsg move container to workspace 3
   elif [[ ! "$choice" == *"4"* ]]; then
     swaymsg move container to workspace 4
   elif [[ ! "$choice" == *"5"* ]]; then
     swaymsg move container to workspace 5
   elif [[ ! "$choice" == *"6"* ]]; then
     swaymsg move container to workspace 6
   elif [[ ! "$choice" == *"7"* ]]; then
     swaymsg move container to workspace 7
   elif [[ ! "$choice" == *"8"* ]]; then
     swaymsg move container to workspace 8
   elif [[ ! "$choice" == *"9"* ]]; then
     swaymsg move container to workspace 9
   elif [[ ! "$choice" == *"0"* ]]; then
     swaymsg move container to workspace 0
   elif [[ ! "$choice" == *"11"* ]]; then
     swaymsg move container to workspace 11
   elif [[ ! "$choice" == *"12"* ]]; then
     swaymsg move container to workspace 12
   else
     :
   fi
