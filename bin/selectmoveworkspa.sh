#!/bin/bash

launcher=$(echo "" | rofi -dmenu -i -p '' \
-theme-str 'listview {lines: 0; columns: 1; padding: 0px 0px 0px 0px;}' \
-theme-str 'window {width: 100%; location: south;}' \
-theme-str 'element.selected.normal {background-color: transparent;}' \
-theme-str 'inputbar {children:   [ prompt,textbox-prompt-colon,entry,case-indicator ];}' \
-theme-str 'entry {vertical-align: 0.5; placeholder: "Type number 1 through 0 to move window to numbered workspace";}')
[ -z "$launcher" ] && exit

if [[ "$launcher" =~ ^[0-9]+$ ]]
then
    xdotool key super+shift+"$launcher"
else
    :
fi


