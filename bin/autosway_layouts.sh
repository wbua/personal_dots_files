#!/bin/bash

# Sway user preferences 
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

TTK_MOD="$TILE_TAB_KEYB_MOD"

   if [ -f "$HOME"/Documents/.layouts_toggle.txt ]; then
     :
   else
     touch "$HOME"/Documents/.layouts_toggle.txt
   fi

echo "$TTK_MOD" > "$HOME"/Documents/.layouts_toggle.txt   
