#!/bin/bash

# Program command: rofi-command_palette.sh
# Description: Keyboard shortcuts for programs.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 12-10-2023
# Program_license: GPL 3.0
# Dependencies: rofi, xdotool

#-------------------------------------------------------------#
# Error checking
#-------------------------------------------------------------#

set -euo

#-------------------------------------------------------------#
# Rofi run launcher
#-------------------------------------------------------------#

_rofi() {
	
rofi -dmenu -i -p ''	
	
}

#-------------------------------------------------------------#
# Office suite
#-------------------------------------------------------------#

launch_only-office() {
	
launcher="xdotool key"
fileone="$(cat /home/john/Documents/palettes/onlyoffice.txt)"
choice=$(echo -e "$fileone" | sort | _rofi)
[ -z "$choice" ] && exit 0
echo -e "$choice" | awk -F ':' '{print $NF}' | sed 's/ //' | xargs $launcher	
	
}

#-------------------------------------------------------------#
# Image editor
#-------------------------------------------------------------#

launch_gimp() {

launcher="xdotool key"
fileone="$(cat /home/john/Documents/palettes/gimp.txt)"
choice=$(echo -e "$fileone" | sort | _rofi)
[ -z "$choice" ] && exit 0
echo -e "$choice" | awk -F ':' '{print $NF}' | sed 's/ //' | xargs $launcher	
	
}

#-------------------------------------------------------------#
# Browser
#-------------------------------------------------------------#

launch_browser() {

launcher="xdotool key"
fileone="$(cat /home/john/Documents/palettes/brave.txt)"
choice=$(echo -e "$fileone" | sort | _rofi)
[ -z "$choice" ] && exit 0
echo -e "$choice" | awk -F ':' '{print $NF}' | sed 's/ //' | xargs $launcher
	
}

#-------------------------------------------------------------#
# Gnome-terminal
#-------------------------------------------------------------#

launch_terminal() {

launcher="xdotool key"
fileone="$(cat /home/john/Documents/palettes/gnome-terminal.txt)"
choice=$(echo -e "$fileone" | sort | _rofi)
[ -z "$choice" ] && exit 0
echo -e "$choice" | awk -F ':' '{print $NF}' | sed 's/ //' | xargs $launcher	
	
}

#-------------------------------------------------------------#
# Main
#-------------------------------------------------------------#

main() {

window_id=$(xdotool getactivewindow)
class_name=$(xprop -id "$window_id" | grep WM_CLASS | cut -d '"' -f2)
  
   if [[ "$(echo "$class_name")" = 'brave-browser' ]]; then
   launch_browser
   fi
   
   if [[ "$(echo "$class_name")" = 'DesktopEditors' ]]; then
   launch_only-office
   fi
   
   if [[ "$(echo "$class_name")" = 'gimp' ]]; then
   launch_gimp
   fi
   
   if [[ "$(echo "$class_name")" = 'gnome-terminal-server' ]]; then
   launch_terminal
   fi
      
}
main
