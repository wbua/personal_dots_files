#!/bin/bash

# Program command: yad_theme_gtk.sh
# Description: Change gtk theme and icon theme.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 12-12-2024
# Program_license: GPL 3.0
# Dependencies: yad 

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#---------------------------------------------#
# Error checking
#---------------------------------------------#

set -e

#---------------------------------------------#
# User preferences
#---------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Gtk theme
MY_THEME="$SWAY_GTK_THEME"
export MY_THEME

# Gtk Icon theme
MY_ICON="$SWAY_ICON_THEME"
export MY_ICON

# Font
MY_FONT="$SWAY_FONT_NAME"
export MY_FONT

# Font size
MY_FONT_SIZE="$SWAY_FONT_SIZE"
export MY_FONT_SIZE

#---------------------------------------------#
# Finds the directories containing the themes
#---------------------------------------------#

find /usr/share/icons/ -mindepth 1 -maxdepth 1 -type d | \
xargs -n 1 basename | sed 's/$/!/' | tee ~/Documents/.gtk_icons_temp.txt > /dev/null

find /usr/share/themes/ -mindepth 1 -maxdepth 1 -type d | \
xargs -n 1 basename | sed 's/$/!/' | tee ~/Documents/.gtk_themes_temp.txt > /dev/null

#---------------------------------------------#
# Yad dialog
#---------------------------------------------#

gtk_choice=$(GTK_THEME="$MY_GTK_THEME" yad --form \
--title="Gtk theme chooser" \
--text-align="center" \
--text="\nGTK Theme Chooser\n\nGtk theme: $MY_THEME\nIcon theme: $MY_ICON\nFont: $MY_FONT $MY_FONT_SIZE\n" \
--field="Theme:CB" "None!$(cat ~/Documents/.gtk_themes_temp.txt)" \
--field="Icon:CB" "None!$(cat ~/Documents/.gtk_icons_temp.txt)" \
--field="Font ":CE \
--field="Font size ":CE \
--field=" ":LBL "" \
--borders=20 \
--separator="#" \
--button="Ok":0 \
--button="Exit":1 | xargs)
export gtk_choice 
[[ -z "$gtk_choice" ]] && exit

#---------------------------------------------#
# Picks up different fields from variable
#---------------------------------------------#

field1=$(echo "$gtk_choice" | sed 's| ||' | awk -F '#' '{print $1}')
export field1
field2=$(echo "$gtk_choice" | sed 's| ||' | awk -F '#' '{print $2}')
export field2
field3=$(echo "$gtk_choice" | sed 's| ||' | awk -F '#' '{print $3}')
export field3
field4=$(echo "$gtk_choice" | sed 's| ||' | awk -F '#' '{print $4}')
export field4

#---------------------------------------------#
# Sets the gtk variables in config
#---------------------------------------------#

   if [[ "$field1" == *"None"* ]]; then
     :
   else
     sed -i "s|.*\(SWAY_GTK_THEME=\).*|SWAY_GTK_THEME=\"$(printf '%s' "$field1")\"|" ~/bin/sway_user_preferences.sh || exit 1
   fi

   if [[ "$field2" == *"None"* ]]; then
     :
   else
     sed -i "s|.*\(SWAY_ICON_THEME=\).*|SWAY_ICON_THEME=\"$(printf '%s' "$field2")\"|" ~/bin/sway_user_preferences.sh || exit 1
   fi

   if [[ -z "$field3" ]]; then
     :
   else
     sed -i "s|.*\(SWAY_FONT_NAME=\).*|SWAY_FONT_NAME=\"$(printf '%s' "$field3")\"|" ~/bin/sway_user_preferences.sh || exit 1
   fi

   if [[ -z "$field4" ]]; then
     :
   else
     sed -i "s|.*\(SWAY_FONT_SIZE=\).*|SWAY_FONT_SIZE=\"$(printf '%s' "$field4")\"|" ~/bin/sway_user_preferences.sh || exit 1
   fi

bash ~/bin/sway_load_settings.sh
sleep 5 && dunstctl close

#---------------------------------------------#
# Unset variables and functions
#---------------------------------------------#

unset MY_GTK_THEME
unset gtk_choice
unset field1
unset field2
unset field3
unset field4
unset MY_THEME
unset MY_ICON
unset MY_FONT
unset MY_FONT_SIZE
