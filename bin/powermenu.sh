#!/bin/bash

# Program command: powermenu.sh
# Description: Power menu to shutdown, reboot, lock and logout.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 23-06-2023
# Program_license: GPL 3.0
# Dependencies: rofi, google material icons font, zenity, slock, xdotool

# This power menu is used with resolution 1920x1080.
# Material icons font - https://github.com/google/material-design-icons/blob/master/font/MaterialIcons-Regular.ttf
# Pango markup - https://docs.gtk.org/Pango/pango_markup.html
#maim ~/Pictures/$(date +%s).png

#-------------------------------------------------------#
# Run launcher main
#-------------------------------------------------------#

_rofi() {

rofi -dmenu -i -p '' -mesg "${color1}$(echo -e "Goodbye $greet\nuptime ")${end} ${color3}$(uptime -p)${end}" -markup-rows -me-select-entry '' -me-accept-entry 'MousePrimary' \
-theme-str 'window {location: center; anchor: center; fullscreen: true; width: 100%; border-radius: 0px; border: 0;}' \
-theme-str 'element-text {background-color: #00000060; padding: 50px; border-radius: 30px; border-color: #04040450; border: 0; }' \
-theme-str 'element {background-color: #F500FF; padding: 100px 30px; border-radius: 0px; }' \
-theme-str 'mainbox {children: [ "message", "listview" ]; padding: 50; border-radius: 0px; }' \
-theme-str 'element.selected.normal {background-color: transparent; padding: 0px 0px 0px 0px; border-color: transparent; border: 0 0 0 0; text-color: red;}' \
-theme-str 'listview {layout: horizontal; columns: 4; lines: 4; background-color: transparent; border: 0 0 0 0; padding: 0 0 35 50; margin: 250px 0px 0px 250px;}' \
-theme-str 'message {border: 0 0 0 0; margin: 0px 0px 0px 0px;}' \
-theme-str 'configuration {hover-select: true; me-accept-entry: "MousePrimary"; me-select-entry: "";}' \
-theme-str 'textbox {horizontal-align: 0.0; vertical-align: 0.0; }' 

}

#-------------------------------------------------------#
# Rofi shutdown
#-------------------------------------------------------#

rofi_shutdown() {

sleep 0.5
rofi -dmenu -i -p '' -mesg "${color1}Confirm shutdown!${end}" -markup-rows -me-select-entry '' -me-accept-entry 'MousePrimary' \
-theme-str 'window {location: center; anchor: center; fullscreen: false; width: 60%; border-radius: 0px; border: 0;}' \
-theme-str 'element-text {background-color: #00000060; padding: 50px; border-radius: 30px; border-color: #04040450; border: 0; }' \
-theme-str 'element {background-color: #F500FF; padding: 100px 30px; border-radius: 0px; }' \
-theme-str 'mainbox {children: [ "message", "listview" ]; padding: 50; border-radius: 0px; }' \
-theme-str 'element.selected.normal {background-color: transparent; padding: 0px 0px 0px 0px; border-color: transparent; border: 0 0 0 0; text-color: red;}' \
-theme-str 'listview {layout: horizontal; columns: 4; lines: 4; background-color: transparent; border: 0 0 0 0; padding: 0 0 35 50; margin: 50px 0px 110px 150px;}' \
-theme-str '* {background: rgba ( 20, 20, 20, 100 % ); lightbg: rgba ( 0, 0, 0, 90 % );}' \
-theme-str 'message {border: 0 0 0 0; margin: 0px 0px 0px 0px;}' \
-theme-str 'configuration {hover-select: true; me-accept-entry: "MousePrimary"; me-select-entry: "";}' \
-theme-str 'textbox {horizontal-align: 0.0; vertical-align: 0.0; }' 

}

#-------------------------------------------------------#
# Rofi restart
#-------------------------------------------------------#

rofi_restart() {

sleep 0.5
rofi -dmenu -i -p '' -mesg "${color1}Confirm restart!${end}" -markup-rows -me-select-entry '' -me-accept-entry 'MousePrimary' \
-theme-str 'window {location: center; anchor: center; fullscreen: false; width: 60%; border-radius: 0px; border: 0;}' \
-theme-str 'element-text {background-color: #00000060; padding: 50px; border-radius: 30px; border-color: #04040450; border: 0; }' \
-theme-str 'element {background-color: #F500FF; padding: 100px 30px; border-radius: 0px; }' \
-theme-str 'mainbox {children: [ "message", "listview" ]; padding: 50; border-radius: 0px; }' \
-theme-str 'element.selected.normal {background-color: transparent; padding: 0px 0px 0px 0px; border-color: transparent; border: 0 0 0 0; text-color: red;}' \
-theme-str 'listview {layout: horizontal; columns: 4; lines: 4; background-color: transparent; border: 0 0 0 0; padding: 0 0 35 50; margin: 50px 0px 110px 150px;}' \
-theme-str '* {background: rgba ( 20, 20, 20, 100 % ); lightbg: rgba ( 0, 0, 0, 90 % );}' \
-theme-str 'message {border: 0 0 0 0; margin: 0px 0px 0px 0px;}' \
-theme-str 'configuration {hover-select: true; me-accept-entry: "MousePrimary"; me-select-entry: "";}' \
-theme-str 'textbox {horizontal-align: 0.0; vertical-align: 0.0; }' 

}

#-------------------------------------------------------#
# Rofi lock
#-------------------------------------------------------#

rofi_lock() {

sleep 0.5
rofi -dmenu -i -p '' -mesg "${color1}Confirm lock!${end}" -markup-rows -me-select-entry '' -me-accept-entry 'MousePrimary' \
-theme-str 'window {location: center; anchor: center; fullscreen: false; width: 60%; border-radius: 0px; border: 0;}' \
-theme-str 'element-text {background-color: #00000060; padding: 50px; border-radius: 30px; border-color: #04040450; border: 0; }' \
-theme-str 'element {background-color: #F500FF; padding: 100px 30px; border-radius: 0px; }' \
-theme-str 'mainbox {children: [ "message", "listview" ]; padding: 50; border-radius: 0px; }' \
-theme-str 'element.selected.normal {background-color: transparent; padding: 0px 0px 0px 0px; border-color: transparent; border: 0 0 0 0; text-color: red;}' \
-theme-str 'listview {layout: horizontal; columns: 4; lines: 4; background-color: transparent; border: 0 0 0 0; padding: 0 0 35 50; margin: 50px 0px 110px 150px;}' \
-theme-str '* {background: rgba ( 20, 20, 20, 100 % ); lightbg: rgba ( 0, 0, 0, 90 % );}' \
-theme-str 'message {border: 0 0 0 0; margin: 0px 0px 0px 0px;}' \
-theme-str 'configuration {hover-select: true; me-accept-entry: "MousePrimary"; me-select-entry: "";}' \
-theme-str 'textbox {horizontal-align: 0.0; vertical-align: 0.0; }' 

}

#-------------------------------------------------------#
# Rofi logout
#-------------------------------------------------------#

rofi_logout() {

sleep 0.5
rofi -dmenu -i -p '' -mesg "${color1}Confirm logout!${end}" -markup-rows -me-select-entry '' -me-accept-entry 'MousePrimary' \
-theme-str 'window {location: center; anchor: center; fullscreen: false; width: 60%; border-radius: 0px; border: 0;}' \
-theme-str 'element-text {background-color: #00000060; padding: 50px; border-radius: 30px; border-color: #04040450; border: 0; }' \
-theme-str 'element {background-color: #F500FF; padding: 100px 30px; border-radius: 0px; }' \
-theme-str 'mainbox {children: [ "message", "listview" ]; padding: 50; border-radius: 0px; }' \
-theme-str 'element.selected.normal {background-color: transparent; padding: 0px 0px 0px 0px; border-color: transparent; border: 0 0 0 0; text-color: red;}' \
-theme-str 'listview {layout: horizontal; columns: 4; lines: 4; background-color: transparent; border: 0 0 0 0; padding: 0 0 35 50; margin: 50px 0px 110px 150px;}' \
-theme-str '* {background: rgba ( 20, 20, 20, 100 % ); lightbg: rgba ( 0, 0, 0, 90 % );}' \
-theme-str 'message {border: 0 0 0 0; margin: 0px 0px 0px 0px;}' \
-theme-str 'configuration {hover-select: true; me-accept-entry: "MousePrimary"; me-select-entry: "";}' \
-theme-str 'textbox {horizontal-align: 0.0; vertical-align: 0.0; }' 

}

#-------------------------------------------------------#
# Colors for menu items (pango markup)
#-------------------------------------------------------#

# Text
color1="<span color='white' font_family='Monospace' weight='bold' font='20' rise='28pt'>"
# Icons
color2="<span font_family='Material Icons' font='80' rise='0pt'>"
# Uptime
color3="<span color='red' font_family='Monospace' weight='bold' font='20' rise='28pt'>"
# Color end
end="</span>"

#-------------------------------------------------------#
# Confirm options
#-------------------------------------------------------#

# Icons
shutdown_yes="${color2}${end}"
shutdown_no="${color2}${end}"
restart_yes="${color2}${end}"
restart_no="${color2}${end}"
lock_yes="${color2}${end}"
lock_no="${color2}${end}"
logout_yes="${color2}${end}"
logout_no="${color2}${end}"

#-------------------------------------------------------#
# Confirm shutdown menu
#-------------------------------------------------------#

shutdown_menu() {

echo "$shutdown_yes"	
echo "$shutdown_no"
	
}

#-------------------------------------------------------#
# Confirm restart menu
#-------------------------------------------------------#

restart_menu() {

echo "$restart_yes"	
echo "$restart_no"
	
}

#-------------------------------------------------------#
# Confirm lock menu
#-------------------------------------------------------#

lock_menu() {

echo "$lock_yes"	
echo "$lock_no"
	
}

#-------------------------------------------------------#
# Confirm logout menu
#-------------------------------------------------------#

logout_menu() {

echo "$logout_yes"	
echo "$logout_no"
	
}

#-------------------------------------------------------#
# Shutdown main
#-------------------------------------------------------#

shutdown_case() {

shutdown_choice=$(shutdown_menu | rofi_shutdown)

case ${shutdown_choice} in

 $shutdown_yes*)
  poweroff --poweroff
  ;;

 $shutdown_no*)
  exit
  ;;
  
 *)
  exit
 ;;
 
esac

}

#-------------------------------------------------------#
# Restart main
#-------------------------------------------------------#

restart_case() {

restart_choice=$(restart_menu | rofi_restart)

case ${restart_choice} in

 $restart_yes*)
  poweroff --reboot
  ;;

 $restart_no*)
  exit
  ;;
  
 *)
  exit
 ;;
 
esac

}

#-------------------------------------------------------#
# Lock main
#-------------------------------------------------------#

lock_case() {

lock_choice=$(lock_menu | rofi_lock)

case ${lock_choice} in

 $lock_yes*)
  slock
  ;;

 $lock_no*)
  exit
  ;;
  
 *)
  exit
 ;;
 
esac

}

#-------------------------------------------------------#
# logout main
#-------------------------------------------------------#

logout_case() {

logout_choice=$(logout_menu)

case ${logout_choice} in

 $logout_yes*)
  xdotool key super+shift+q
  ;;

 $logout_no*)
  exit
  ;;
  
 *)
  exit
 ;;
 
esac

}

#-------------------------------------------------------#
# Power options
#-------------------------------------------------------#

# Icons
shutdown="${color2}${end}"
reboot="${color2}${end}"
lock="${color2}${end}"
logout="${color2}${end}"

#-------------------------------------------------------#
# Greeting
#-------------------------------------------------------#

greet=$(printf '%s' "$USER")

#-------------------------------------------------------#
# Menu
#-------------------------------------------------------#

menu() {
	
echo "${shutdown}"	
echo "${reboot}"	
echo "${lock}"
echo "${logout}"	
	
}

#-------------------------------------------------------#
# Main
#-------------------------------------------------------#

choice=$(menu | _rofi)

case ${choice} in

 $shutdown*)
  shutdown_case
  ;;

 $reboot*)
  restart_case
  ;;
 
 $lock*)
  lock_case
  ;;
  
 $logout*)
  logout_case
  ;; 
  
 *)
  exit
 ;;
  
esac 
