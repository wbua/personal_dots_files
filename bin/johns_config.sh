#!/bin/bash
# shellcheck disable=SC2034

# Program command: johns_config.sh
# Description: Config for johns scripts.
# Contributors: John Mcgrath
# Program_version: 1.5.0
# Program updated: 20-08-2024
# Program_license: GPL 3.0

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Put your choices in double quotes.

#########################################################
#########################################################

# Warning!!!
# This script is not longer used.
# I have moved over to wayland.
# Rofi only works in sway because of layer shell.
# Rofi still however has errors and crashes.

#########################################################
#########################################################

#-------------------------------------------------------#
# Rofi theme
#-------------------------------------------------------#

rofi_config="/home/john/.config/rofi/config4.rasi"

#-------------------------------------------------------#
# Favorite software preferences
#-------------------------------------------------------#

# Browser
browser="$MAIN_BROWSER"

# Filemanager
file_manager="$MAIN_FILE_MANAGER"

# Text editor
texteditor="$MAIN_TEXT_EDITOR"

# Terminal editor
terminal_editor="micro"

# Terminal
myterminal="$MAIN_TERMINAL"

# Video player
video_player="$MAIN_VIDEO_PLAYER"

# Other video player
other_video_player="$MAIN_OTHER_VIDEO_PLAYER"

# Image viewer
image_viewer="$MAIN_IMAGE_VIEWER"

# Image editor
image_editor="$MAIN_IMAGE_EDITOR"

# Pdf viewer
pdf_viewer="$MAIN_PDF_VIEWER"

# Audio player
audio_player="$MAIN_AUDIO_PLAYER"

# Office suite
office_suite="$MAIN_OFFICE"

# Other office suite
other_office_suite="$MAIN_OTHER_OFFICE"

# Sound editor
sound_editor="$MAIN_SOUND_EDITOR"

#-------------------------------------------------------#
# User paths
#-------------------------------------------------------#

# Bash scripts executable path location
bash_dir="$HOME/bin/"

# Audacious now playing path
musicdir="$HOME/Music/MP3_Songs_1"

#-------------------------------------------------------#
# Misc
#-------------------------------------------------------#

# Screencast resolution
recordres="1920x1080"

# Your printer name
my_printer="Brother_DCP_J1050DW"

#-------------------------------------------------------#
# Wallpaper changer (xorg only)
#-------------------------------------------------------#

# Desktop wallpapers location
screendir="$HOME/Pictures/wallpapers/"

# Autostart file
myautostart="$HOME/.xmonad/scripts/autostart.sh"

# Desktop wallpapers location (do not add /home/user/, $HOME or ~)
# Do not add / after wallpapers
locationwall="Pictures/wallpapers"

# Sets the line where feh is in your autostart file
lineset="9s"

#-------------------------------------------------------#
# Wallpaper changer (wayland only)
#-------------------------------------------------------#

# Desktop wallpapers location
screendir2="/home/john/Pictures/sway_wallpapers/"

# Desktop wallpapers location (do not add /home/user/, $HOME or ~)
# Do not add / after wallpapers
locationwall2="Pictures/sway_wallpapers"

# Sets the wallpaper line in your sway config file
lineset2="5s"

# My sway config
my_swaywm_config="$HOME/.config/sway/config"

#-------------------------------------------------------#
# Screenshots
#-------------------------------------------------------#

# Screenshots directory
screenshots_dir="$HOME/Pictures/screenshots/"

# Screenshot directory (do not add /home/user/, $HOME or ~)
# Do not add / after screenshots
locationscreenshots="Pictures/screenshots"

# Delayed screenshot time
delay_time="5"

#-------------------------------------------------------#
# Weather api key
#-------------------------------------------------------#

# To get api key visit https://www.bbc.co.uk/weather/2643743
# The api key has 7 digits 2643743
# When you change api key run update weather
weather_api_key="2640194"

#-------------------------------------------------------#
# Apps containing different class names
#-------------------------------------------------------#

# Applications that appear in root search.

# Gimp image editor
image_editor_description="image editor"
image_editor_class="gimp"
moveto_image_editor="GNU Image Manipulation Program" 
open_image_editor="gimp"

#-------------------------------------------------------#
# File and directory search locations
#-------------------------------------------------------#

locate_dir1="Documents"
locate_dir2="Pictures"
locate_dir3="Videos"
locate_dir4="Music"
locate_dir5="Downloads"
locate_dir6="Templates"
locate_dir7="Public"

#-------------------------------------------------------#
# Other software preferences
#-------------------------------------------------------#

# Applications that appear in root search.

# App 01
app01_description="color picker"
app01_name="gcolor3"

# App 02
app02_description="text editor"
app02_name="mousepad"

# App 03
app03_description="browser"
app03_name="opera"

# App 04
app04_description="free slot"
app04_name="misc"

# App 05
app05_description="free slot"
app05_name="misc"

# App 06
app06_description="calendar"
app06_name="gnome-calendar"

# App 07
app07_description="free slot"
app07_name="misc"

# App 08
app08_description="software store"
app08_name="snap-store"

# App 09
app09_description="zipping"
app09_name="file-roller"

# App 10
app10_description="system fonts"
app10_name="font-manager"

# App 11
app11_description="video calling"
app11_name="zoom"

# App 12
app12_description="free slot"
app12_name="misc"

# App 13
app13_description="free slot"
app13_name="misc"

# App 14
app14_description="calculator"
app14_name="gnome-calculator"

# App 15
app15_description="video editor"
app15_name="kdenlive"

# App 16
app16_description="browser"
app16_name="google-chrome"

# App 17
app17_description="browser"
app17_name="firefox"

# App 18
app18_description="markdown editor"
app18_name="obsidian"

# App 19
app19_description="terminal"
app19_name="xfce4-terminal"

# App 20
app20_description="free slot"
app20_name="misc"

# App 21
app21_description="scan documents"
app21_name="simple-scan"

# App 22
app22_description="free slot"
app22_name="misc"

# App 23
app23_description="free slot"
app23_name="misc"

# App 24
app24_description="free slot"
app24_name="misc"

# App 25
app25_description="free slot"
app25_name="misc"
