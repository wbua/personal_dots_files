#!/bin/bash
# shellcheck disable=SC2090
# shellcheck disable=SC2089
# shellcheck disable=SC2034

# Program command: yad_radio_list.sh
# Description: Select and play radio streams.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 28-08-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad, mpv, xclip, qrencode, ffmpeg, playerctl, mpv-mpris, custom gtk.css, Material Icons, JetBrainsMono Nerd Font

#-------------------------------------------------------#
# General variables
#-------------------------------------------------------#

file_manager="thunar"
export file_manager

##########################

# Information

# This script has been made and tested on Ubuntu 22.04
# It uses a text file containing radio streams.
# You will find this file in /home/user/Documents/mainprefix/

##########################

# Download links

# mpv-mpris: https://github.com/hoyon/mpv-mpris/releases
# material icons: https://github.com/google/material-design-icons/blob/master/font/MaterialIcons-Regular.ttf
# custom gtk.css: https://sourceforge.net/projects/jm-dots/files/gtk_css/alt-dialog3/
# JetBrainsMono Nerd Font: https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/JetBrainsMono.zip

##########################

# Installation

# You will first need to install the dependencies.
# In the terminal type the following below.

# Step 1:
# $ sudo apt update

# Step 2:
# $ sudo apt install yad mpv xclip qrencode ffmpeg playerctl

#-------------------------

# You will need to put yad_radio_list.sh into your executable path.
# This is in /home/user/bin/
# If the directory does not exist then create it.
# Then type the command below.
# $ chmod u+x yad_radio_list.sh

#-------------------------

# You will then need to download the files in the Download links section.
# After you have downloaded them do the following.
# If the directories do not exist, then create them.

# mpv-mpris installation:
# Just copy and paste mpris.so into /home/user/.config/mpv/scripts/

# Material icons installation:
# You should install font-manager, to install the font.
# Step 1: $ sudo apt update
# Step 2: $ sudo apt install font-manager
# Open font manager and press the plus button.
# Select the MaterialIcons-Regular.ttf font and press ok.

# JetBrainsMono Nerd Font:
# Install this font the same way as the material icons font.

# Gtk.css installation:
# Copy and paste gtk.css file into /home/user/.local/share/themes/alt-dialog3/

##########################

# How to use

# Double click on radio station to play stream.
# You must be playing a radio stream, before you press the record button.
# Record button: Press the record button to start recording, and press it again to stop recording.
# Play button: Press the play button to play recording, and press it again to stop playing.
# Mute button: Toggle volume on and off. Mutes mpv only.
# Settings button: Change file manager.
# Help button: Help page.
# Directory button: Select last recording file in file manager.
# Plus button: Add new radio station.
# When adding radio station the format should be: radio 636673 - https://example.stream
# Copy button: Click on radio station, and then press copy. This will copy radio station to clipboard.
# Clip button: Copy current track title to clipboard.
# QR Code button: The qr codes button has 2 modes.
# Mode 1: When not playing any stream. Click on radio station, then press qr code button to create qr code.
# Mode 2: When playing stream. Click on qr code button to create qr code from track title.
# File button: Open text file containing radio links.
# Stop button: Stop mpv.
# Exit button: kill yad

# Colors

# Record button:
# Recording: red
# Not recording: green
# Recording directory is empty: yellow
# Play button:
# Playing: red
# Not playing: green
# No file to play: yellow
# Mute button:
# Unmuted: green
# Muted: red

#-------------------------------------------------------#
# Kills any existing instances of yad
#-------------------------------------------------------#

killall yad

#-------------------------------------------------------#
# Create the text file containing radio names and url's
#-------------------------------------------------------#

   # Create Documents directory
   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   # Create mainprefix directory
   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   # Create text file
   if [ -f "$HOME"/Documents/mainprefix/radiolinks.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/radiolinks.txt
   fi

#-------------------------------------------------------#
# Remove special characters
#-------------------------------------------------------#
# If you do not remove these characters from radiolinks.txt.
# It will cause the script to have errors.

sed -i 's|<||g' "$HOME"/Documents/mainprefix/radiolinks.txt
sed -i 's|&|+|g' "$HOME"/Documents/mainprefix/radiolinks.txt

#-------------------------------------------------------#
# Creates files and directories
#-------------------------------------------------------#

   # Create Pictures directory
   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   # Create temp qr code directory
   if [ -d "$HOME"/Pictures/.temqrcodes ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes
   fi

   # Create main qr code directory
   if [ -d "$HOME"/Pictures/qr_codes ]; then
     :
   else
     mkdir "$HOME"/Pictures/qr_codes
   fi

   # Create radio list
   if [ -f "$HOME"/Documents/mainprefix/radiolinks.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/radiolinks.txt
   fi

   # Create video recording status
   if [ -f "$HOME"/Documents/.video_record.txt ]; then
     :
   else
     touch "$HOME"/Documents/.video_record.txt
     echo "off" > "$HOME"/Documents/.video_record.txt
   fi

   # Create video playing status
   if [ -f "$HOME"/Documents/.video_playing.txt ]; then
     :
   else
     touch "$HOME"/Documents/.video_playing.txt
     echo "off" > "$HOME"/Documents/.video_playing.txt
   fi

   # This is where recorded streams are stored temporary
   if [ -d "$HOME"/Music/.temprecfile ]; then
     :
   else
     mkdir "$HOME"/Music/.temprecfile
   fi

   # This is where the recorded files are stored
   if [ -d "$HOME"/Music/radio_recordings ]; then
     :
   else
     mkdir "$HOME"/Music/radio_recordings
   fi

   # Check mute status
   if [ -f "$HOME"/Documents/.radio_mute.txt ]; then
     :
   else
     touch "$HOME"/Documents/.radio_mute.txt
   fi

#-------------------------------------------------------#
# Text file location
#-------------------------------------------------------#

radio_list=$(cat "$HOME"/Documents/mainprefix/radiolinks.txt)
export radio_list

#-------------------------------------------------------#
# Remove whitespace from text file
#-------------------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/radiolinks.txt

#-------------------------------------------------------#
# Video recording file status
#-------------------------------------------------------#

     recfile=$(cat "$HOME"/Documents/.video_record.txt)
     export recfile
   if [[ -z "$recfile" ]]; then
     echo "off" > "$HOME"/Documents/.video_record.txt
     killall yad
   fi

#-------------------------------------------------------#
# Video playing file status
#-------------------------------------------------------#

     playfile=$(cat "$HOME"/Documents/.video_playing.txt)
     export playfile
   if [[ -z "$playfile" ]]; then
     echo "off" > "$HOME"/Documents/.video_playing.txt
     killall yad
   fi

#-------------------------------------------------------#
# Colors for menu items (pango markup)
#-------------------------------------------------------#

color1="<span color='#25FF00' font_family='Material Icons' font='20'>"
export color1
color2="<span color='#25FF00' font_family='Monospace' font='15' weight='bold'>"
export color2
color3="<span color='#EBFF00' font_family='Monospace' font='15' weight='bold'>"
export color3
color4="<span color='#FF002E' font_family='Material Icons' font='20' weight='bold'>"
export color4
color5="<span color='#22FF00' font_family='Material Icons' font='20' weight='bold'>"
export color5
color6="<span color='#FFF700' font_family='Material Icons' font='20' weight='bold'>"
export color6
color7="<span color='#FF002E' font_family='Monospace' font='15' weight='bold'>"
export color7
end="</span>"
export end

#-------------------------------------------------------#
# Help
#-------------------------------------------------------#

_help_info() {

info_text="${color2}Help information${end}

This program plays radio stations

${color2}How to use${end}

Double click on radio station to play stream.
You must be playing a radio stream, before you press the record button.

${color2}Buttons${end}

${color3}Record button:${end} Press the record button to start recording, and press it again to stop recording.
${color3}Play button:${end} Press the play button to play recording, and press it again to stop playing.
${color3}Mute button:${end} Toggle volume on and off. Mutes mpv only.
${color3}Settings button:${end} Change file manager.
${color3}Help button:${end} Help page.
${color3}Directory button:${end} Select last recording file in file manager.
${color3}Plus button:${end} Add new radio station.
When adding radio station the format should be: radio 636673 - https://example.stream
${color3}Copy button:${end} Click on radio station, and then press copy. This will copy radio station to clipboard.
${color3}Clip button:${end} Copy current track title to clipboard.
${color3}QR Code button:${end} The qr codes button has 2 modes.
Mode 1: When not playing any stream. Click on radio station, press qr code button to create qr code.
Mode 2: When playing stream. Click on qr code button to create qr code from track title.
${color3}File button:${end} Open text file containing radio links.
${color3}Stop button:${end} Stop mpv.
${color3}Exit button:${end} kill yad

${color2}Keys${end}

Press ${color3}control+f${end} to file search.

${color2}About${end}

Program created by ${color3}John Mcgrath${end}
"
export info_text
killall yad
echo "$info_text" | GTK_THEME="alt-dialog4" yad --list --column="help" --height=500 --width=1200 --center \
--no-headers --separator= \
--button="<span color='#FF002A' font_family='Material Icons' font='18'></span>":1

}
export -f _help_info

#-------------------------------------------------------#
# Change browser
#-------------------------------------------------------#

_change_file_manager() {

sed -i "19s|^.*$|file_manager=\"$(yad --entry --center --text="Change file manager")\"|" "$HOME"/bin/yad_radio_list.sh

}
export -f _change_file_manager

#-------------------------------------------------------#
# Send song title to clipboard
#-------------------------------------------------------#

_songtitclip() {

playerctl --player mpv metadata -f "{{title}}" | xclip -selection clipboard
killall yad

}
export -f _songtitclip

#-------------------------------------------------------#
# Stop mpv and ffplay
#-------------------------------------------------------#

_stopmpvffplay() {

   if [[ "$(cat "$HOME"/Documents/.video_playing.txt)" == 'on' ]]; then
     killall yad
     killall ffplay
     echo "off" > "$HOME"/Documents/.video_playing.txt
   elif [[ "$(cat "$HOME"/Documents/.video_record.txt)" == 'on' ]]; then
     killall yad
     killall mpv
     echo "off" > "$HOME"/Documents/.video_record.txt
   elif [[ "$(pidof mpv)" ]]; then
     killall mpv
     killall yad
   else
     :
   fi

}
export -f _stopmpvffplay

#-------------------------------------------------------#
# Play recordings
#-------------------------------------------------------#

_playrecfg() {

     playingfile=$(cat "$HOME"/Documents/.video_playing.txt)
     export playingfile
   if [[ "$playingfile" == 'off' ]]; then
     playectemp=$(find "$HOME"/Music/.temprecfile/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     export playectemp
     echo "on" | tee "$HOME"/Documents/.video_playing.txt
     killall yad
     killall mpv
     killall ffplay
     echo "off" | tee "$HOME"/Documents/.video_record.txt
     sleep 1
     ffplay "$playectemp" -autoexit -nodisp
     killall yad
     echo "off" | tee "$HOME"/Documents/.video_playing.txt
   elif [[ "$playingfile" == 'on' ]]; then
     killall yad
     echo "off" | tee "$HOME"/Documents/.video_playing.txt
     killall mpv
     killall ffplay
   fi

}
export -f _playrecfg

#-------------------------------------------------------#
# Recording radio streams with mpv
#-------------------------------------------------------#

_recordradiompv() {

     vidfile=$(cat "$HOME"/Documents/.video_record.txt)
     export vidfile
   if [[ "$vidfile" == 'off' && "$(pidof mpv)" ]]; then
     killall yad
     echo "on" | tee "$HOME"/Documents/.video_record.txt
     rm -f "$HOME"/Music/.temprecfile/*.*
     radiotrack=$(playerctl --player mpv metadata -f "{{xesam:url}}")
     export radiotrack
     killall mpv
     killall ffplay
     playerctl --player mpv metadata -f "{{title}}" | tee "$HOME"/Documents/.rectitle.txt
     echo "off" | tee "$HOME"/Documents/.video_playing.txt
     mpv --stream-record="$HOME/Music/.temprecfile/$(date +'%d-%m-%Y-%H%M%S').mp4" "$radiotrack"
     cp "$HOME"/Music/.temprecfile/*.* "$HOME"/Music/radio_recordings/
   elif [[ "$vidfile" == 'on' ]]; then
     killall yad
     echo "off" | tee "$HOME"/Documents/.video_record.txt
     killall mpv
     killall ffplay
   else
     :
   fi

}
export -f _recordradiompv

#-------------------------------------------------------#
# Check state of recording
#-------------------------------------------------------#

_check_recording(){

   if [[ -z "$(ls -A "$HOME"/Music/.temprecfile)" ]]; then
     echo "${color6}${end}"
   elif [[ "$recfile" == 'off' ]]; then
	 echo "${color5}${end}"
   elif [[ "$recfile" == 'on' ]]; then
	 echo "${color4}${end}"
   fi

}
export -f _check_recording


#-------------------------------------------------------#
# Check state of playing
#-------------------------------------------------------#

_check_playing(){

   if [[ -z "$(ls -A "$HOME"/Music/.temprecfile)" ]]; then
     echo "${color6}${end}"
   elif [[ "$playfile" == 'off' ]]; then
	 echo "${color5}${end}"
   elif [[ "$playfile" == 'on' ]]; then
	 echo "${color4}${end}"
   fi

}
export -f _check_playing

#-------------------------------------------------------#
# Currently playing
#-------------------------------------------------------#

_currently_playing() {

   if [[ "$(pidof mpv)" ]]; then
     echo "${color2}$(playerctl --player mpv metadata -f "{{title}}" | \
     cut -c 1-60 | tr "&" "+")${end}"
     echo "${color3}$(playerctl --player mpv metadata -f "{{xesam:url}}" | \
     cut -c 1-60 | tr "&" "+")${end}"
   elif [[ "$(pidof ffplay)" ]]; then
     file_title=$(cat "$HOME"/Documents/.rectitle.txt)
     export file_title
     echo "${color7}$(echo "$file_title" | cut -c 1-70 | tr "&" "+")${end}"
   else
     echo "${color2}Not playing${end}"
   fi

}
export -f _currently_playing

#-------------------------------------------------------#
# Mute sound
#-------------------------------------------------------#

_mute() {

     vfile=$(cat "$HOME"/Documents/.radio_mute.txt)
	 export vfile
   if [[ "$vfile" == 'playerctl --player mpv volume 1%' ]]; then
     killall yad
	 echo "playerctl --player mpv volume 0%" > "$HOME"/Documents/.radio_mute.txt
	 playerctl --player mpv volume 0%
   elif [[ "$vfile" == 'playerctl --player mpv volume 0%' ]]; then
	 killall yad
	 echo "playerctl --player mpv volume 1%" > "$HOME"/Documents/.radio_mute.txt
	 playerctl --player mpv volume 1%
   fi

}
export -f _mute

#-------------------------------------------------------#
# Check status of muted sound
#-------------------------------------------------------#

_check_mute() {

	 vlfile=$(cat "$HOME"/Documents/.radio_mute.txt)
     export vlfile
   if [[ -z "$vlfile" ]]; then
	 echo "playerctl --player mpv volume 1%" > "$HOME"/Documents/.radio_mute.txt
   elif [[ "$vlfile" == 'playerctl --player mpv volume 1%' ]]; then
	 echo "${color5}${end}"
   elif [[ "$vlfile" == 'playerctl --player mpv volume 0%' ]]; then
	 echo "${color4}${end}"
   else
	 :
   fi

}
export -f _check_mute

#-------------------------------------------------------#
# Add radio station
#-------------------------------------------------------#

_add_station() {

   add_radio=$(GTK_THEME="alt-dialog3" yad --entry --width=500 --height=80 --no-buttons \
   --text="Add radio station" --text-align="center")
   export add_radio
   if [[ -z "$add_radio" ]]; then
     :
   else
     echo "$add_radio" | tee -a "$HOME"/Documents/mainprefix/radiolinks.txt /dev/null
   fi

}
export -f _add_station

#-------------------------------------------------------#
# Select video recording in file manager
#-------------------------------------------------------#

_dir_radio() {

GTK_THEME="Yaru"
killall yad
chosen=$(find "$HOME"/Music/.temprecfile -type f -iname '*.*' | sort | cut -d '/' -f6-)
export chosen
[ -z "$chosen" ] && exit 0
"$file_manager" "$HOME"/Music/radio_recordings/"$chosen"

}
export -f _dir_radio

#-------------------------------------------------------#
# Open text file containing radio links
#-------------------------------------------------------#

_open_text_file() {

killall yad
GTK_THEME="Yaru"
xdg-open "$HOME"/Documents/mainprefix/radiolinks.txt

}
export -f _open_text_file

#-------------------------------------------------------#
# Close yad
#-------------------------------------------------------#

_close_yad() {

killall yad

}
export -f _close_yad

#-------------------------------------------------------#
# Copy radio station to clipboard
#-------------------------------------------------------#

_copy_to_clip() {

echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | xclip -selection clipboard

}
export -f _copy_to_clip

#-------------------------------------------------------#
# Create QR Code
#-------------------------------------------------------#

_create_qr_code() {

     # Creates qr code from track title in playerctl
   if [[ "$(pidof mpv)" ]]; then
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     playerctl --player mpv metadata -f "{{title}}" | tr -d '\n' | \
     qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
     one3=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     export one3
     notify-send -i "$one3" "Created" "Qr Code"
   else
     # Create qr code from radio link in text file
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     echo -n "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | tr -d '\n' | \
     qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
     one2=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     export one2
     notify-send -i "$one2" "Created" "Qr Code"
   fi

}
export -f _create_qr_code

#-------------------------------------------------------#
# Play radio station
#-------------------------------------------------------#

_play_radio_station() {

echo "off" > "$HOME"/Documents/.video_playing.txt
echo "off" > "$HOME"/Documents/.video_record.txt
killall mpv ; killall ffplay ; echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | \
awk '{print $NF}' | xargs mpv

}
export -f _play_radio_station

#-------------------------------------------------------#
# Yad settings
#-------------------------------------------------------#

_yad() {

GTK_THEME="alt-dialog3" yad --list --search-column=1 --column="list":TEXT --title="Choose radio station" \
--width=800 --height=400 \
--text="$(_currently_playing)\n" --text-align="center" \
--separator= --buttons-layout="center" --borders=10 --no-escape \
--no-headers --align=center \
--button="$(_check_recording)":80 \
--button="$(_check_playing)":90 \
--button="$(_check_mute)":35 \
--button="${color1}${end}":32 \
--button="${color1}${end}":37 \
--button="${color1}${end}":25 \
--button="${color1}${end}":20 \
--button="${color1}${end}":50 \
--button="${color1}${end}":10 \
--button="${color1}${end}":60 \
--button="${color1}${end}":30 \
--button="${color1}${end}":70 \
--button="<span color='#FF2323' font_family='Material Icons' font='20'></span>":40
echo $?

}

export -f _yad

#-------------------------------------------------------#
# Main
#-------------------------------------------------------#

_main() {

choice=$(echo "$radio_list" | sort | _yad)
export choice

   # Adds new radio station to text file
   if [[ "${choice:0-2}" == *"20"* ]]; then
     _add_station
   # Open text file containing radio links
   elif [[ "${choice:0-2}" == *"30"* ]]; then
     _open_text_file
   # Kills yad
   elif [[ "${choice:0-2}" == *"40"* ]]; then
     _close_yad
   # Copy radio link to clipboard
   elif [[ "${choice:0-2}" == *"50"* ]]; then
     _copy_to_clip
   # Creates qr code
   elif [[ "${choice:0-2}" == *"60"* ]]; then
     _create_qr_code
   # Stops mpv and ffplay
   elif [[ "${choice:0-2}" == *"70"* ]]; then
     _stopmpvffplay
   # Records the current radio stream
   elif [[ "${choice:0-2}" == *"80"* ]]; then
     _recordradiompv
   # Play the last recorded stream
   elif [[ "${choice:0-2}" == *"90"* ]]; then
     _playrecfg
   # Send track title to clipboard
   elif [[ "${choice:0-2}" == *"10"* ]]; then
     _songtitclip
   # Select last recording in file manager
   elif [[ "${choice:0-2}" == *"25"* ]]; then
     _dir_radio
   # Mute mpv only
   elif [[ "${choice:0-2}" == *"35"* ]]; then
     _mute
   # Change file manager
   elif [[ "${choice:0-2}" == *"32"* ]]; then
     _change_file_manager
   # Display help page
   elif [[ "${choice:0-2}" == *"37"* ]]; then
     _help_info
   # Plays radio links in text file
   else
     _play_radio_station
   fi

}
export -f _main
_main

#-------------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------------#

# Functions
unset _yad
unset _add_station
unset _currently_playing
unset _recordradiompv
unset _stopmpvffplay
unset _check_playing
unset _songtitclip
unset _dir_radio
unset _check_mute
unset _mute
unset _open_text_file
unset _close_yad
unset _copy_to_clip
unset _create_qr_code
unset _play_radio_station
unset _main
unset _change_file_manager
unset _help_info
unset _playrecfg
unset _check_recording

# Variables
unset choice
unset color1
unset color2
unset color3
unset color4
unset color5
unset color6
unset color7
unset end
unset radio_list
unset add_radio
unset one2
unset one3
unset radiotrack
unset vidfile
unset recfile
unset playectemp
unset playingfile
unset chosen
unset file_manager
unset vlfile
unset vfile
unset file_title
unset playfile
