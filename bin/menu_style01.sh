#!/bin/bash

# Program command: menu_style01.sh
# Description: This script shows different ways to do menu items.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 02-11-2023
# Program_license: GPL 3.0
# Dependencies: rofi

#---------------------------------------------------#
# Colors
#---------------------------------------------------#

# Description
color1="<span color='#8087A2' font_family='Cascadia Mono' font='15' weight='bold' rise='0pt'>"
# Color end
end="</span>"

#---------------------------------------------------#
# Run launcher
#---------------------------------------------------#

_rofi() {

rofi -dmenu -i -p '' -markup-rows -sep '^' -eh 2

	
}

#---------------------------------------------------#
# Menu
#---------------------------------------------------#

menu() {

echo -en "brave\n"
echo -en "${color1}browser${end}^"
echo -en "full\n"
echo -en "${color1}screenshots${end}^"
	
}

#---------------------------------------------------#
# Main
#---------------------------------------------------#

main() {
	
choice=$(menu | _rofi | awk 'NR==1')
[[ -z "$choice" ]] && exit 0

   if [[ "$choice" = 'brave' ]]; then
   notify-send "brave"
   elif [[ "$choice" = 'full' ]]; then
   notify-send "full"
   else
   notify-send "not working!"
   fi 

}
main
