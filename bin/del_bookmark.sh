#!/bin/bash


my_file="$HOME/Documents/del_bookmarks.txt"
sed -i -e 's/\[/ /' -e 's/*/ /' -e 's/|/ /' "$my_file"
choice=$(cat "$HOME"/Documents/del_bookmarks.txt | rofi -dmenu -i)
[ -z "$choice" ] && exit 0
#notify-send "$choice" 
sed -i -e "\|$choice|d" -e '/^$/d' "$my_file"

