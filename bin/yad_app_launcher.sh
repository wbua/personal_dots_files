#!/bin/bash

# Program command: yad_app_launcher.sh
# Description: Search and run your installed programs.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 08-06-2024
# Program_license: GPL 3.0
# Dependencies: yad

#---------------------------------------------------------#
# User preferences
#---------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="alt-dialog22"
export MY_GTK_THEME

#---------------------------------------------------------#
# Location of dot desktop files in system
#---------------------------------------------------------#

desktop_programs() {

find /var/lib/snapd/desktop/applications/ -type f -iname '*.desktop' | sed 's/.desktop$//g' | \
awk -F '_' '{print $2}' | sed 's|firefox||g' | sed '/^[[:space:]]*$/d' 
find /usr/share/applications -iname "*.desktop" | sed 's/.desktop$//g' | \
sed 's|org.gnome.||g' | sed 's|org.xfce.||g' | sed 's|nl.hjdskes.||g' | xargs -n 1 basename

}

#---------------------------------------------------------#
# Yad dialog
#---------------------------------------------------------#

_yad() {

GTK_THEME="$MY_GTK_THEME" yad --list \
--column="Applications" \
--search-column=1 \
--regex-search \
--separator= \
--title="Configs" \
--text="\nApplication launcher\nTo search list just start typing\n" \
--text-align="center" \
--borders=20 \
--no-headers \
--no-markup \
--width=600 \
--height=600 \
--button="_Exit":1
		
}
export _yad

#---------------------------------------------------------#
# Main
#---------------------------------------------------------#

selected=$(desktop_programs | _yad)
[[ -z "$selected" ]] && exit
export selected

   if [[ "$selected" = *"libreoffice-writer"* ]]; then
     libreoffice --writer
   elif [[ "$selected" = *"libreoffice-calc"* ]]; then
     libreoffice --calc
   elif [[ "$selected" = *"libreoffice-draw"* ]]; then
     libreoffice --draw
   elif [[ "$selected" = *"libreoffice-math"* ]]; then
     libreoffice --math
   elif [[ "$selected" = *"libreoffice-impress"* ]]; then
     libreoffice --impress
   elif [[ "$selected" = *"libreoffice-startcenter"* ]]; then
     libreoffice --startcenter
   else
     /bin/bash -c "$selected" || exit 1
   fi

#---------------------------------------------------------#
# Unset variables and functions
#---------------------------------------------------------#

unset selected
unset desktop_programs
unset _yad
unset MY_GTK_THEME
