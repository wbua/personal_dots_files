#!/bin/bash

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

     active_app=$(swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"' \
     | grep -i "$MAIN_BROWSER" | cut -d: -f2)

   if [[ "$active_app" == *"$MAIN_BROWSER"* || "$active_app" == *"${MAIN_BROWSER^}"* ]]; then
     swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"' \
     | grep -i "$MAIN_BROWSER" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' move workspace current
   else
    /bin/bash -c "$MAIN_BROWSER"
   fi
