#!/bin/bash

# Program command: change_background_colors.sh
# Description: Changes background colors.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 07-03-2025
# Program_license: GPL 3.0
# Dependencies: yad, grim, slurp, imagemagick, wl-clipboard

# Exit with non zero status 1-255
set -e

# Master config file
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#--------------------------------------------------------#
# User preferences
#--------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Bemenu background
MY_BEMENU_BGC="$BEMENU_BACKGROUND"
export MY_BEMENU_BGC

# Dunst backgound color
MY_COLOR_BG="$COLOR_BG"
export MY_COLOR_BG

# Dunst text color
MY_COLOR_ICON="$COLOR_ICON"
export MY_COLOR_ICON

# Dunst flame color
MY_COLOR_ALERT="$COLOR_ALERT"
export MY_COLOR_ALERT

# Theme config
MY_THEME_CONFIG="$I3BLOCKS_THEMES_CONFIG"
export MY_THEME_CONFIG

# Profile directory
MY_DEFAULT_DIR="$FIREFOX_PROFILE_DIRECTORY"
export MY_DEFAULT_DIR

# Path to firefox userchrome css file
COLOR_FIREFOX_DIR="$FIREFOX_PATH_TO_COLORS"
export COLOR_FIREFOX_DIR

# Geany directory
GEANY_DIR="$HOME/.config/geany"
export GEANY_DIR

# Geany file
GEANY_FILE="geany.css"
export GEANY_FILE

# Current background
color_bg="<span color='$MY_COLOR_ICON' font_family='Inter' weight='bold' font='13' rise='0'>"
export color_bg 

# Color end
color_end="</span>"
export color_end

# Rofi directory
ROFI_DIR="$HOME/.config/rofi"
export ROFI_DIR

# Rofi file
ROFI_FILE="colors-rofi.rasi"
export ROFI_FILE

# Yad background transparent
YAD_BG_TRANS="$YAD_TRANSPARENT_BG"
export YAD_BG_TRANS

#--------------------------------------------------------#
# Create the dunst color config file
#--------------------------------------------------------#

   if [ ! -d "$HOME"/.config/dunst/dunstrc.d ]; then
     mkdir -p "$HOME"/.config/dunst/dunstrc.d
   fi

   if [ -f "$COLOR_FIREFOX_DIR"/userChrome.css ]; then
     :
   else
     notify-send "Firefox userchrome.css" "does not exist" && exit 1
   fi

   if [ -d "$GEANY_DIR" ]; then
     :
   else
     mkdir "$GEANY_DIR"
   fi
   
   if [ -f "$GEANY_DIR"/"$GEANY_FILE" ]; then
     :
   else
     touch "$GEANY_DIR"/"$GEANY_FILE"
   fi

   if [ -d "$ROFI_DIR" ]; then
     :
   else
     mkdir "$ROFI_DIR"
   fi
   
   if [ -f "$ROFI_DIR"/"$ROFI_FILE" ]; then
     :
   else
     touch "$ROFI_DIR"/"$ROFI_FILE"
   fi

#--------------------------------------------------------#
# Color picker
#--------------------------------------------------------#

color_picker() {

grim -g "$(slurp -p)" -t ppm - | convert - -format '%[pixel:p{0,0}]' txt:- | \
tr -d '\n' | awk '{print $7}' | wl-copy -n

}
export -f color_picker

#--------------------------------------------------------#
# Create the dunst color config file
#--------------------------------------------------------#

create_dunst_file() {

cat <<EOF > ~/.config/dunst/dunstrc.d/colors.conf
[urgency_low]
background = "$field_choice"
foreground = "$MY_COLOR_ICON"
frame_color = "$MY_COLOR_ALERT"

[urgency_normal]
background = "$field_choice"
foreground = "$MY_COLOR_ICON"
frame_color = "$MY_COLOR_ALERT"

[urgency_critical]
background = "$field_choice"
foreground = "#ff0031"
frame_color = "#ff0031"

EOF

# Kill dunst process
killall dunst

}
export -f create_dunst_file

#--------------------------------------------------------#
# Create firefox userchrome file
#--------------------------------------------------------#

create_firefox_userchrome() {

# Create userchrome file
cat <<EOF > "$COLOR_FIREFOX_DIR"/userChrome.css

/* Change the background color of the tab bar */
#TabsToolbar {
    background-color: $field_choice!important;
    color: #000000;
}

/* Change the text color of the tabs */
.tab-label {
    color: #000 !important;
}

/* Change Address Bar Background */
#urlbar {
    background-color: #FFFFFF !important;
    color: black !important; /* Change text color */
}

/* Change Bookmark Bar Background */
#PersonalToolbar {
    background-color: $field_choice!important; /* Green background color */
}

/* Change Search Bar Background */
#searchbar {
    background-color: #333333 !important;
    color: white !important; /* Change text color */
}

/* Change background color of the primary toolbar row (with address bar and navigation buttons) */
#nav-bar {
    background-color: $field_choice!important;
}

/* Change the background color of the primary toolbar row when hovering */
#nav-bar:hover {
    background-color: $field_choice!important;
}

/* Active tab background */
.tab-background:is([selected], [multiselected]) {
    background-color: $MY_COLOR_ICON!important;
    background-image: none !important;
}

/* Inactive tab text */
.tabbrowser-tab:not([selected]) label {
    color: #f8f8f8 !important;
    }

}

/* Change address bar text color to white */
#nav-bar {
    color: white !important;
}

/* Change bookmarks bar text color to white */
#personal-bookmarks {
    color: white !important;
}

/* Change nav-bar buttons text color to white */
#nav-bar .toolbarbutton-text,
#nav-bar .toolbarbutton-icon {
    fill: white !important;
    color: white !important;
}

EOF

}
export -f create_firefox_userchrome

#--------------------------------------------------------#
# Create geany colors
#--------------------------------------------------------#

create_geany_colors() {

cat <<EOF > "$GEANY_DIR"/"$GEANY_FILE"

/* Active tab styles */
tab:checked,
tab:checked .label {
    background-color: $MY_COLOR_ICON;
    color: black;
}

/* Menubar background and text color */
menubar {
    background-color: $field_choice;
    color: white;
}

/* Button bar background and text color */
toolbar {
    background-color: $field_choice;
    color: white;
}

/* Menubar dropdowns background and text color */
menubar menu, menubar menuitem {
    background-color: $field_choice;
    color: white;
}

notebook {
    background-color: $field_choice;
    border: $field_choice;
    
}

/* Highlight color for menu items when hovered */
menubar menuitem:hover {
    background-color: $MY_COLOR_ICON;
    color: black;
}

/* Inactive tab styles */
tab,
tab .label {
    color: white;
}

EOF
	
}
export -f create_geany_colors

#--------------------------------------------------------#
# Change rofi background
#--------------------------------------------------------#

rofi_background() {

cat <<EOF > "$ROFI_DIR"/"$ROFI_FILE"
 
* {
active-background: #784CA0;
active-foreground: #FAE8E1;
normal-background: #181519;
normal-foreground: #FAE8E1;
urgent-background: #CC659A;
urgent-foreground: #FAE8E1;

alternate-active-background: #914B4B;
alternate-active-foreground: #FAE8E1;
alternate-normal-background: #181519;
alternate-normal-foreground: #FAE8E1;
alternate-urgent-background: #181519;
alternate-urgent-foreground: #FAE8E1;

selected-active-background: #CC659A;
selected-active-foreground: #FAE8E1;
selected-normal-background: #CC659A;
selected-normal-foreground: #FAE8E1;
selected-urgent-background: #784CA0;
selected-urgent-foreground: #FAE8E1;

background-color: #181519;
background: $field_choice;
foreground: #000000; /* color icon: selected black text */
foreground-nonactive: #FFFFFF; /* non active white color */
border-color: #784CA0;
background-options: $field_choice;

color0: #3F3C40;
color1: #1A1022;
color2: #492E61;
color3: #6D3838;
color4: #5A3978;
color5: #994C74;
color6: #B58E80;
color7: #F0D6CC;
color8: #A8958F;
color9: #23152D;
color10: #613D81;
color11: $MY_COLOR_ICON;
color12: $MY_COLOR_ICON;
color13: #CC659A;
color14: #F2BDAA;
color15: #F0D6CC;

}

EOF
	
}
export -f rofi_background

#--------------------------------------------------------#
# Yad dialog
#--------------------------------------------------------#

field_choice=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--title="Background color changer" \
--width=450 \
--height=150 \
--text-align="center" \
--text="\nBackground color changer\nDoes not change i3blocks\nCurrent background: ${color_bg}$MY_COLOR_BG${color_end}\n\nEnter hex code...\n" \
--borders=10 \
--button="_Picker:/bin/bash -c 'color_picker'" \
--button="Ok":0 \
--button="Exit":1)
[ -z "$field_choice" ] && exit 0
export field_choice

#--------------------------------------------------------#
# Change colors for config files
#--------------------------------------------------------#

# Check if wofi is installed
if command -v wofi &> /dev/null
then
    # Wofi background color
    sed -i "s|.*\(\@define-color wofi-kappa\).*|@define-color wofi-kappa $(printf '%s' "${field_choice};")|" ~/.config/wofi/colors.css
    
else
    notify-send "Wofi" "is not installed"
fi


# Function to convert hex to decimal
hex_to_dec() {
  echo $((16#$1))
}

# Hex color input
hex_color="$field_choice"

# Strip the leading '#' if present
hex_color="${hex_color#'#'}"

# Extract RGB components
r=$(hex_to_dec ${hex_color:0:2})
g=$(hex_to_dec ${hex_color:2:2})
b=$(hex_to_dec ${hex_color:4:2})

# Default alpha value (change if needed)
a="$YAD_BG_TRANS"

#rgba(${r}, ${g}, ${b}, 0.${a}

# Check if yad is installed
if command -v yad &> /dev/null
then
    # Yad background color
    sed -i "s|.*\(\@define-color yad-background-color\).*|@define-color yad-background-color $(printf '%s' "rgba(${r}, ${g}, ${b}, 0.${a});")|" ~/.local/share/themes/alt-dialog22/gtk-3.0/colors.css
    # Yad powermenu background
    sed -i "s|.*\(define-color power-background-color\).*|@define-color power-background-color $(printf '%s' "rgba(${r}, ${g}, ${b}, 0.${a});")|" ~/.local/share/themes/alt-dialog25/gtk-3.0/colors.css || exit 1
else
    notify-send "Yad" "is not installed"
fi

# Check if thunar is installed
if command -v thunar &> /dev/null
then
    # Thunar background color 
    sed -i "s|.*\(\@define-color thunar-background-color\).*|@define-color thunar-background-color $(printf '%s' "$field_choice");|" ~/.config/gtk-3.0/mytheme.css || exit 1    
else
    notify-send "Thunar" "is not installed"
fi

# Check if fuzzel is installed
if command -v fuzzel &> /dev/null
then
    # Fuzzel background
    sed -i "s|.*\(background=\).*|background=$(printf '%s' "${field_choice}ff" | sed 's|#||')|" ~/.config/fuzzel/fuzzel.ini || exit
    
else
    notify-send "Fuzzel" "is not installed"
fi

# Check if tofi is installed
if command -v tofi &> /dev/null
then
    # Tofi background
    sed -i "s|.*\(background-color =\).*|background-color = $(printf '%s' "${field_choice}FA" | sed 's|#||')|" ~/.config/tofi/themes/colors || exit    
else
    notify-send "Tofi" "is not installed"
fi

# Check if foot is installed
if command -v foot &> /dev/null
then
    # Foot background
    sed -i "s|.*\(background=\).*|background=$(printf '%s' "$field_choice" | tr -d '#')|" ~/.config/foot/foot.ini || exit 1    
else
    notify-send "Foot" "is not installed"
fi

# Check if bemenu is installed
if command -v bemenu &> /dev/null
then
    # Bemenu background
    sed -i "s|.*\(BEMENU_BACKGROUND=\).*|BEMENU_BACKGROUND=\"$(printf '%s' "$field_choice")\"|" "$MY_THEME_CONFIG" || exit 1
else
    echo "Bemenu" "is not installed"
fi

# Background color in i3blocks accent colors
sed -i "s|.*\(COLOR_BG=\).*|COLOR_BG=\"$(printf '%s' "$field_choice")\"|" "$MY_THEME_CONFIG" || exit 1

# Check if dunst is installed
if command -v dunst &> /dev/null
then
    create_dunst_file || exit 1
    
else
    notify-send "Dunst" "is not installed"
fi

# Check if firefox is installed
if command -v firefox &> /dev/null
then
    create_firefox_userchrome || exit 1
else
    notify-send "Firefox" "is not installed"
fi

# Check if geany is installed
if command -v geany &> /dev/null
then
    create_geany_colors || exit 1
else
    notify-send "Geany" "is not installed"
fi

# Check if rofi is installed
if command -v rofi &> /dev/null
then
    rofi_background
else
    notify-send "Rofi" "is not installed"
fi

# Reload
bash ~/bin/sway_load_settings.sh

# Killall thunar
killall Thunar

#--------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------#

unset field_choice
unset MY_GTK_THEME
unset color_picker
unset MY_BEMENU_BGC
unset create_dunst_file
unset MY_COLOR_ALERT
unset MY_COLOR_ICON
unset MY_COLOR_BG
unset MY_THEME_CONFIG
unset MY_DEFAULT_DIR
unset COLOR_FIREFOX_DIR
unset create_firefox_userchrome
unset create_geany_colors
unset GEANY_FILE
unset GEANY_DIR
unset color_end
unset color_bg
unset ROFI_DIR
unset ROFI_FILE
unset rofi_background
unset YAD_BG_TRANS
