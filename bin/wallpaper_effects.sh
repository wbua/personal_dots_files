#!/bin/bash

# Program command: wallpaper_effects.sh
# Description: Does various wallpaper effects on wallpaper.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 10-02-2025
# Program_license: GPL 3.0
# Dependencies: imagemagick, rofi, tofi, fuzzel, bemenu, wofi

set -e

#------------------------------------------------------#
# Sourcing files
#------------------------------------------------------#

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#------------------------------------------------------#
# User preferences
#------------------------------------------------------#

# Current set wallpaper
MY_WALLPAPER="$MAIN_SWAY_WALLPAPER"

# Wallpaper directory
WALLS_DIR="$MAIN_WALLPAPER_DIR"

# Gets the width of the wallpaper
image_info=$(identify -format "%w" "$MY_WALLPAPER")

# This is where it stores the backup wallpaper
temp_dir="$HOME/Pictures/.adjust_wallpaper_effects"

# Background color
MY_COLOR_BG="$COLOR_BG"

# Color icons
MY_COLOR_ICON="$COLOR_ICON"

#------------------------------------------------------#
# Create directories
#------------------------------------------------------#

# Ensure the Pictures directory exists
if [ ! -d "$HOME/Pictures" ]; then
    mkdir -p "$HOME/Pictures"
fi

# Ensure the temp directory exists
if [ ! -d "$temp_dir" ]; then
    mkdir -p "$temp_dir"
fi

#------------------------------------------------------#
# Run launchers
#------------------------------------------------------#

run_launchers() {

   if [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == 'rofi' ]]; then
     rofi -dmenu -i -config "$ROFI_THEME_CONFIG"
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == 'wofi' ]]; then
     wofi --dmenu -i
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "bemenu" ]]; then
     bemenu -i -l 8 -H 40 --fn "Monospace 18" --fb "$MY_COLOR_BG" --ff "#cdd6f4" --nb "$MY_COLOR_BG" --nf "#cdd6f4" --tb "$MY_COLOR_BG" \
     --hb "$MY_COLOR_BG" --tf "$MY_COLOR_ICON" --hf "$MY_COLOR_ICON" --af "#FFFFFF" --ab "$MY_COLOR_BG"
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "yad" ]]; then
     GTK_THEME="$MY_GTK_THEME" yad --list --column="list" --search-column=1 --regex-search --no-headers \
     --borders=10 --width=800 --height=600 --button="Exit":1 
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == 'tofi' ]]; then
     tofi
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "fuzzel" ]]; then
     fuzzel -d
   else
     :
   fi
   
}

#------------------------------------------------------#
# Menu options
#------------------------------------------------------#

option_menu() {

    echo "Backup"
    echo "Blur"
    echo "Blue"
    echo "Charcoal"
    echo "Cyan"
    echo "Decrease"
    echo "Emboss"
    echo "Edge"
    echo "Gradient"
    echo "Greyscale"
    echo "Green"
    echo "Increase"
    echo "Low"
    echo "Negate"
    echo "Orange"
    echo "Paint"
    echo "Posterize"
    echo "Restore"
    echo "Red"
    echo "Save"    
    echo "Sepia"
    echo "Solarize"    
    
}

#------------------------------------------------------#
# Main
#------------------------------------------------------#

my_choice=$(option_menu | run_launchers)
[ -z "$my_choice" ] && exit 0

# Recent file in the temp directory
temp_file=$(find "$temp_dir" -type f -print0 | xargs -0 ls -tr | tail -n 1)

case $my_choice in

  'Gradient') 
    convert "$MY_WALLPAPER" -size "$image_info"x170 gradient:"black-rgba(0,0,0,0)" -compose over -composite "$MY_WALLPAPER"    
    bash ~/bin/sway_load_settings.sh
    ;;

  'Restore')
    cp "$temp_file" "$WALLS_DIR" || exit 1
    bash ~/bin/sway_load_settings.sh
    ;;
    
  'Backup')
    rm -f "$temp_dir"/*.* || exit 1
    cp "$MY_WALLPAPER" "$temp_dir" && {
                notify_temp=$(find "$temp_dir" -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 basename)
                notify-send "Wallpaper backup" "$notify_temp"
            }
    ;;

  'Greyscale')
    convert "$MY_WALLPAPER" -set colorspace Gray -separate -average "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Save')
    cp "$temp_file" ~/Pictures/ && notify-send "Wallpaper copied" "To ~/Pictures"
    ;;

  'Blur')
    convert "$MY_WALLPAPER" -blur 0x5 "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Negate')
    convert "$MY_WALLPAPER" -channel RGB -negate "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Sepia')
    convert "$MY_WALLPAPER" -sepia-tone 80% "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Increase')
    convert "$MY_WALLPAPER" -brightness-contrast 10X10 "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Decrease')
    convert "$MY_WALLPAPER" -brightness-contrast -10X-10 "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Blue')
    convert "$MY_WALLPAPER" -fill blue -tint 70 "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Cyan')
    convert "$MY_WALLPAPER" -fill cyan -tint 70 "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Green')
    convert "$MY_WALLPAPER" -fill green -tint 70 "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Red')
    convert "$MY_WALLPAPER" -fill red -tint 70 "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Orange')
    convert "$MY_WALLPAPER" -fill orange -tint 70 "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Low')
    convert "$MY_WALLPAPER" -brightness-contrast -40% "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Charcoal')
    convert "$MY_WALLPAPER" -charcoal 0x5 "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Emboss')
    convert "$MY_WALLPAPER" -emboss 0x5 "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Paint')
    convert "$MY_WALLPAPER" -paint 4 "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Posterize')
    convert "$MY_WALLPAPER" -posterize 4 "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Solarize')
    convert "$MY_WALLPAPER" -solarize 80% "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  'Edge')
    convert "$MY_WALLPAPER" -edge 1 "$MY_WALLPAPER"
    bash ~/bin/sway_load_settings.sh
    ;;

  *)
    notify-send "Something went wrong!" && exit 1
    ;;
    
esac
