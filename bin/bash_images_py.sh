#!/bin/bash

set -e

# Run the Python script and capture the output (image path) into a variable
image_path=$(python3 ~/bin/py_images_gui.py)

# Check if the output is not empty
#if [ -n "$image_path" ]; then
    # Store the output in a Bash variable and print it
    echo "$image_path" | tee ~/Documents/py_gui_images_selected.txt
#else
 #   echo "No image path selected."
#fi
