#!/bin/bash

launcher=$(echo -e "1\n2\n3\n4\n5\n6\n7\n8\n9\n0" | rofi -dmenu -i -p '' -me-select-entry '' -me-accept-entry 'MousePrimary' \
-theme-str 'listview {lines: 1; columns: 10; padding: 0px 0px 0px 0px;}' \
-theme-str 'window {width: 100%; location: south;}' \
-theme-str '* {background: rgba ( 29, 29, 29, 100 % );}' \
-theme-str 'element.selected.normal {background-color: #000000; padding: 0px 0px 0px 0px; border: 0 0 0 0; text-color: #47FF00;}' \
-theme-str 'configuration {hover-select: true; me-accept-entry: "MousePrimary"; me-select-entry: "";}' \
-theme-str 'inputbar {children:   [ prompt,textbox-prompt-colon,entry,case-indicator ];}' \
-theme-str 'entry {vertical-align: 0.5; placeholder: "Type number 1 through 0 to move window to numbered workspace";}')
[ -z "$launcher" ] && exit

if [[ "$launcher" =~ ^[0-9]+$ ]]
then
    xdotool key super+shift+"$launcher"
else
    :
fi
