#!/bin/bash

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

set -e

MY_WALLPAPER="$MAIN_WALLPAPER_DIR"

find "$MY_WALLPAPER" -type f -iname '*.*' | sort -R | tee ~/Documents/.random_sort_alpha.txt && notify-send "Random wallpaper" "list created"
