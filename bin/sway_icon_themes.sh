#!/bin/bash

# Program command: sway_icon_themes.sh
# Description: Changes icon themes for sway.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 18-08-2024
# Program_license: GPL 3.0
# Dependencies: yad, Material Icons

# Do not worry about the snap notification about missing themes.

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#---------------------------------------------#
# User preferences
#---------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#---------------------------------------------#
# Yad settings
#---------------------------------------------#

_yad() {

GTK_THEME="$MY_GTK_THEME" yad --list \
--width=500 \
--height=400 \
--borders=15 \
--column=1 \
--no-headers \
--text-align="center" \
--text="<span font_family='Monospace' font='15'>Icon theme changer</span>

<span font_family='Monospace' font='12'>Add icon theme to: /usr/share/icons/</span>
<span font_family='Monospace' font='12'>Select theme and press the tick button</span>
<span font_family='Monospace' font='12'>Reload your apps to see the theme</span>
" \
--separator= \
--button="<span font_family='Material Icons' font='20'></span>":0 \
--button="<span color='#FF0031' font_family='Material Icons' font='20'></span>":1

}
export -f _yad

#---------------------------------------------#
# Main
#---------------------------------------------#

main() {

choice=$(ls /usr/share/icons/ | _yad)
[ -z "$choice" ] && exit 0

echo "$choice" > ~/Documents/.sway_icon_theme.txt
sed -i "s|.*\(SWAY_ICON_THEME=\).*|SWAY_ICON_THEME=\"$(cat "$HOME"/Documents/.sway_icon_theme.txt)\"|" ~/bin/sway_user_preferences.sh || exit 1
killall yad
bash ~/bin/sway_load_settings.sh
sleep 5 && dunstctl close
	
}
export -f main
main

#---------------------------------------------#
# Unset variables and functions
#---------------------------------------------#

unset _yad
unset main
unset MY_GTK_THEME
