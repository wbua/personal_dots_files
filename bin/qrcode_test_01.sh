#!/bin/bash

killall yad

choice=$(yad --entry --width=400 --height=400)

launch=$(grep -i "$choice" "$HOME"/Pictures/.qrmixed.txt | tee "$HOME"/Documents/.qrcode_grep.txt /dev/null)
export launch
[ -z "$launch" ] && exit 0

echo "$launch"

unset choice
unset launch

qr_code_search.sh
