#!/bin/bash

set -e

#---------------------------------------------------#
# Sourcing files
#---------------------------------------------------#

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Color config
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#---------------------------------------------------#
# User preferences
#---------------------------------------------------#

# Background color
MY_COLOR_BG="$COLOR_BG"

# Icon color
MY_COLOR_ICON="$COLOR_ICON"

# Sway directory
SWAY_DIR="$HOME/.config/sway"

# Sway file
SWAY_FILE="gnome_transparent"

# Swaybar background color
SWAYBAR_COLOR="$SWAYBAR_BACKGROUND_COLOR"

# Swaybar and i3blocks bar position
I3BLOCKS_POSITION="$I3BLOCKS_BAR_POSITION"

#---------------------------------------------------#
# Create files and directories
#---------------------------------------------------#

   if [ -d "$SWAY_DIR" ]; then
     :
   else
     mkdir "$SWAY_DIR"
   fi
   
   if [ -f "$SWAY_DIR"/"$SWAY_FILE" ]; then
     :
   else
     touch "$SWAY_DIR"/"$SWAY_FILE"
   fi

#---------------------------------------------------#
# Create the geany css file
#---------------------------------------------------#
   
cat <<EOF > "$SWAY_DIR"/"$SWAY_FILE"

bar {
        #swaybar_command waybar
        status_command SCRIPT_DIR=~/.config/i3blocks/scripts/gnome_transparent i3blocks
        status_padding 8
        pango_markup enabled
        workspace_min_width 0
        workspace_buttons no
        tray_output none
        position $I3BLOCKS_POSITION
                
        
        colors {

        statusline #ffffff
        background $SWAYBAR_COLOR
                                
                           #border,background,text
        focused_workspace  #11121B #11121B #FFFFFF
        active_workspace   #11121B #11121B #FFFFFF
        inactive_workspace #11121B #11121B #FFFFFF
        #urgent_workspace   #11121B #11121B #FFFFFF  
            
        }

}

# colour of border, background, text, indicator, and child_border
 
client.focused_inactive     #ff0000 #8B314B #ffffff #8B314B #8B314B
client.urgent               #2f343f #2f343f #ffffff #2f343f #2f343f
client.placeholder          #2f343f #2f343f #ffffff #2f343f #2f343f
client.background           #2f343f

EOF
