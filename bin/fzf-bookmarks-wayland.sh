#!/bin/bash

# Program command: fzf-bookmarks-wayland.sh
# Description: This lets you view and perform actions on bookmarks.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 11-12-2023
# Program_license: GPL 3.0
# Dependencies: fzf, xclip

#------------------------------------------#
# Software preferences
#------------------------------------------#

# Browser
browser="brave"
export browser

# Text editor
text_editor="geany"
export text_editor

#------------------------------------------#
# Creates files and directories
#------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ -f "$HOME"/Documents/mainprefix/bookmarks.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/bookmarks.txt
   fi

   if [ -f "$HOME"/Documents/.fzf_temp_bookmarks.txt ]; then
     :
   else
     touch "$HOME"/Documents/.fzf_temp_bookmarks.txt
   fi

#------------------------------------------#
# Colors
#------------------------------------------#

# Keybindings
color1=$(tput setaf 15)
export color1
# Color end
end=$(tput sgr0)
export end

#------------------------------------------#
# Open in browser
#------------------------------------------#

open_in_browser() {

setsid cat "$HOME"/Documents/.fzf_temp_bookmarks.txt | \
awk '{print $NF}' | xargs -0 -d '\n' "$browser" >/dev/null 2>&1 & disown
sleep 0.6
pkill -f ~/bin/fzf-bookmarks-wayland.sh
	
}
export -f open_in_browser

#------------------------------------------#
# Open bookmarks text file
#------------------------------------------#

open_text_file() {

setsid "$text_editor" "$HOME"/Documents/mainprefix/bookmarks.txt >/dev/null 2>&1 & disown
sleep 0.3
pkill -f ~/bin/fzf-bookmarks-wayland.sh
  	
}
export -f open_text_file

#------------------------------------------#
# Kill script
#------------------------------------------#

kill_script() {

pkill -f ~/bin/fzf-bookmarks-wayland.sh
	
}
export -f kill_script

#------------------------------------------#
# Main
#------------------------------------------#

main() {

bookcheck=$(cat "$HOME"/Documents/mainprefix/bookmarks.txt)
booklist=$(echo "$bookcheck" | sort | fzf --reverse \
--info=inline \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:#660020,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--margin="5" \
--header="
${color1}enter${end} open in browser | ${color1}F3${end} text file | ${color1}F9${end} kill script

" \
--prompt 'bookmarks> ' \
--bind='F9:execute(kill_script {})' \
--bind='F3:execute(open_text_file {})' \
--bind='enter:execute-silent(echo -n {} | tee "$HOME"/Documents/.fzf_temp_bookmarks.txt)+execute(open_in_browser {})')
[ -z "$booklist" ] && exit 0
setsid echo "$booklist" >/dev/null 2>&1 & disown

}
export -f main
main

#------------------------------------------#
# Unset variables and function
#------------------------------------------#

unset main
unset text_editor
unset browser
unset open_in_browser
unset copy_to_clip
unset open_text_file
unset color1
unset end
unset kill_script
