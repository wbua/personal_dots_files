#!/bin/bash

# Program command: fzf-launcher-wayland.sh
# Description: This is a launcher which lets you run all my fzf scripts.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 06-12-2023
# Program_license: GPL 3.0
# Dependencies: fzf

#---------------------------------------#
# Colors
#---------------------------------------#

# Descriptions
color1=$(tput setaf 244)
# User
color2=$(tput setaf 161)
# Color end
end=$(tput sgr0)

#---------------------------------------#
# Greeting
#---------------------------------------#

greeting() {

hour=$(date +%H)
export hour
    
   # Afternoon
   if [ "$hour" -ge 12 ] && [ "$hour" -lt 19 ]; then
     echo "Good afternoon ${color2}$USER${end}"
   fi
   # Morning
   if [ "$hour" -ge 00 ] && [ "$hour" -lt 12 ]; then
     echo "Good morning ${color2}$USER${end}"
   fi
   # Evening
   if [ "$hour" -ge 19 ] && [ "$hour" -lt 24 ]; then
     echo "Good evening ${color2}$USER${end}"
   fi	
	
}

#---------------------------------------#
# Menu
#---------------------------------------#

menu() {

echo "find ${color1} file search${end}"
echo "bookmarks ${color1} opens bookmarks${end}"
echo "radio ${color1} plays online radio streams${end}"
echo "emoji ${color1} copies to clipboard${end}"
echo "configs ${color1} open configuration files${end}"
echo "scripts ${color1} open and create${end}"
echo "web ${color1} various web searches${end}"
echo "hex-colors ${color1} view and create${end}"
echo "modified ${color1} last modified files${end}"
echo "pdf ${color1} create and open pdf files${end}"
echo "notes ${color1} oneline notes${end}"
echo "qr-codes ${color1} create and open qr codes${end}"
echo "download ${color1} download youtube videos${end}"
echo "file ${color1} file download${end}"
echo "dictionary ${color1} local dictionary${end}"
echo "spellchecker ${color1} local spellchecker${end}"
echo "all-documents ${color1} all files in home directory${end}"
	
}

#---------------------------------------#
# Main
#---------------------------------------#

main() {

choice=$(menu | sort | fzf --ansi \
--border="none" \
--header="
$(greeting)

" \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'root search> ' \
--cycle \
--info=inline \
--reverse | awk '{print $1}')

case "$choice" in

 'find')
  fzf-find-wayland.sh
  ;;

 'bookmarks')
  fzf-bookmarks-wayland.sh
  ;;

 'radio')
  fzf-radio-wayland.sh
  ;;

 'emoji')
  fzf-emoji-wayland.sh
  ;;

 'configs')
  fzf-configs-wayland.sh
  ;;

 'scripts')
  fzf-bash-wayland.sh
  ;;

 'web')
  fzf-web_searches-wayland.sh
  ;;

 'hex-colors')
  fzf-color_picker-wayland.sh
  ;;

 'modified')
  fzf-last-modified-wayland.sh
  ;;

 'pdf')
  fzf-pdf-wayland.sh
  ;;

 'notes')
  fzf-notes-wayland.sh
  ;;

 'qr-codes')
  fzf-qrcodes-wayland.sh
  ;;

 'download')
  fzf-download-video-wayland.sh
  ;;

 'file')
  fzf-file-download-wayland.sh
  ;;

 'dictionary')
  fzf-dictionary-wayland.sh
  ;;

 'spellchecker')
  fzf-spellchecker-wayland.sh
  ;;

 'all-documents')
  fzf-search-all-documents-wayland.sh
  ;;

  *)
  echo "something went wrong!" || exit 1
  ;;

esac
	
}
main
