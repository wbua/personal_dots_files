#!/bin/bash

# Program command: i3blocks_weather_update.sh
# Description: Yad BBC weather updater for i3blocks.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 20-03-2024
# Program_license: GPL 3.0
# Dependencies: wget, yad

#-----------------------------------------#
# Create files and directories
#-----------------------------------------#

   if [ -d "$HOME"/Downloads ]; then
     :
   else
     mkdir "$HOME"/Downloads
   fi

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -f "$HOME"/Downloads/.downloadedfile ]; then
     :
   else
     touch "$HOME"/Downloads/.downloadedfile
   fi

   if [ -f "$HOME"/Documents/.weather-api-key.txt ]; then
     :
   else
     touch "$HOME"/Documents/.weather-api-key.txt
   fi

#----------------------------------------------#
# Weather api key
#----------------------------------------------#

weather_api_key=$(cat ~/Documents/.weather-api-key.txt)
export weather_api_key

#----------------------------------------------#
# Yad settings
#----------------------------------------------#

_yad() {

GTK_THEME="Mc-OS-Transparent-1.3" yad --entry --width=450 --height=400 \
--borders=15 \
--separator= \
--text-align="center" \
--text="<span font_family='Monospace' weight='bold' font='16'>BBC weather updater</span>

Visit the site below to get the code
<span color='#FF0031' weight='bold' font_family='Monospace' font='14'>https://www.bbc.co.uk/weather/2643743</span>
The 7 digit code is 2643743

Enter 7 digit code for your region
Left click weather module to update new data
" \
--button="<span font_family='Material Icons' font='20'></span>":0 \
--button="<span color='#FF0031' font_family='Material Icons' font='20'></span>":1

}
export -f _yad

#----------------------------------------------#
# Main
#----------------------------------------------#

main() {

choice=$(_yad)
[ -z "$choice" ] && exit 0

echo "$choice" > ~/Documents/.weather-api-key.txt
wget https://weather-broker-cdn.api.bbci.co.uk/en/forecast/rss/3day/"$weather_api_key" -O "$HOME"/Downloads/.downloadedfile
	
}
export -f main
main

#----------------------------------------------#
# Unset variables and functions
#----------------------------------------------#

unset _yad
unset main
unset weather_api_key
