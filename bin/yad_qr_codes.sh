#!/bin/bash

# Program command: yad_qr_codes.sh
# Description: Creates and shows Qr Codes. Also make them actionable.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 06-01-2025
# Program_license: GPL 3.0
# Dependencies: yad, wl-clipboard, zbar-tools, qrencode, grimshot

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-----------------------------------------------------#
# Error checking
#-----------------------------------------------------#

set -e

#-----------------------------------------------------#
# User preferences
#-----------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# QR Codes directory
MY_CODES_DIR="$MAIN_QR_CODES_DIR"
export MY_CODES_DIR

# Browser
MY_BROWSER="$MAIN_BROWSER"
export MY_BROWSER

# File manager
FILE_MANAGER="$MAIN_FILE_MANAGER"
export FILE_MANAGER

# Terminal
MY_TERMINAL="$MAIN_TERMINAL"
export MY_TERMINAL

# Image viewer
MY_IMAGE_VIEWER="$MAIN_IMAGE_VIEWER"
export MY_IMAGE_VIEWER

# Qr Code hex color
QR_HEX_COLOR="$QR_CODE_HEX_COLOR"
export QR_HEX_COLOR

# Qr Code margin
QR_HEX_MARGIN="$QR_CODE_MARGIN"
export QR_HEX_MARGIN

#-----------------------------------------------------#
# Create files and directories
#-----------------------------------------------------#

   # Create qr codes directory
   if [ "$MY_CODES_DIR" == "disable" ]; then
     :
   elif [ -d "$MY_CODES_DIR" ]; then
     :
   else
     notify-send "Qr Codes directory does not exist!" && exit 1
   fi

   # Create pictures directory
   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi
   
   # Create documents directory
   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   # Temporary qr code directory
   if [ -d "$HOME"/Pictures/.temqrcodes/ ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes/
   fi

   # No qr codes in main directory png file
   if [ -d "$HOME"/Pictures/.qr_codes_empty ]; then
     :
   else
     mkdir "$HOME"/Pictures/.qr_codes_empty
   fi

   # Qr code filesize is too big
   if [ -d "$HOME"/Pictures/.qr_codes_badsize ]; then
     :
   else
     mkdir "$HOME"/Pictures/.qr_codes_badsize
   fi

   # Creates qr code text file
   if [ -f "$HOME"/Pictures/.qrcode_dir.txt ]; then
     :
   else
     touch "$HOME"/Pictures/.qrcode_dir.txt
   fi
  
   # Decode qr code OCR directory 
   if [ -d "$HOME"/Pictures/.ocr/ ]; then
     :
   else
     mkdir "$HOME"/Pictures/.ocr/
   fi

   # Creates qr code combined text file
   if [ -f "$HOME"/Pictures/.qrmixed.txt ]; then
     :
   else
     touch "$HOME"/Pictures/.qrmixed.txt
   fi

   #
   if [ -f "$HOME"/Pictures/.qrmixed_quoted.txt ]; then
     :
   else
     touch "$HOME"/Pictures/.qrmixed_quoted.txt
   fi

#-----------------------------------------------------#
# Enter text to create Qr Code
#-----------------------------------------------------#

text_qrcode() {

killall yad
entry_box=$(GTK_THEME="$MY_GTK_THEME" yad --form \
--title="Add new Qr Code" \
--text="\nCreate Qr Code by entering text\nChoose filename\nYou can leave filename blank\nIf you leave blank it will use date and time for filename\n" \
--text-align="center" \
--center \
--align="left" \
--width=700 \
--height=300 \
--borders=20 \
--separator="#" \
--field="Text ":CE \
--field="Filename ":CE \
--button="Exit":1)
[[ -z "$entry_box" ]] && exit

field1=$(echo "$entry_box" | awk -F '#' '{print $1}')
field2=$(echo "$entry_box" | awk -F '#' '{print $2}')

   if [[ -z "$field2" ]]; then
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     echo "$field1" | tr -d '\n' | sed 's/"//g' | \
     qrencode -m "$QR_HEX_MARGIN" -l L --foreground="$QR_HEX_COLOR" -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$MY_CODES_DIR"
     sleep 0.2
   else
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     echo "$field1" | tr -d '\n' | \
     qrencode -m "$QR_HEX_MARGIN" -l L --foreground="$QR_HEX_COLOR" -o "$HOME/Pictures/.temqrcodes/${field2}_$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$MY_CODES_DIR"
     sleep 0.2
   fi
   
}
export -f text_qrcode

#-----------------------------------------------------#
# Select qr code on screen to create using OCR
#-----------------------------------------------------#

ocr_select() {

killall yad
 
   if [[ -z "$(ls -A "$HOME"/Pictures/.ocr/)" ]]; then
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     rm -f "$HOME"/Pictures/.ocr/*.png
     grimshot save area ~/Pictures/.ocr/ocr.png
     find "$HOME"/Pictures/.ocr/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw | \
     tr -d '\n' | sed 's/"//g' | qrencode -m "$QR_HEX_MARGIN" -l L --foreground="$QR_HEX_COLOR" -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$MY_CODES_DIR"
   else
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     rm -f "$HOME"/Pictures/.ocr/*.png
     grimshot save area ~/Pictures/.ocr/ocr.png
     find "$HOME"/Pictures/.ocr/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw | \
     tr -d '\n' | sed 's/"//g' | qrencode -m "$QR_HEX_MARGIN" -l L --foreground="$QR_HEX_COLOR" -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$MY_CODES_DIR"
   fi

}
export -f ocr_select

#-----------------------------------------------------#
# Open url link in browser
#-----------------------------------------------------#

goto_link() {

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     cat "$HOME"/Documents/.temp_qr_code_choice.txt | \
     awk '{$1="";print $0}' | awk '{print $NF}' | xargs -d '\n' "$MY_BROWSER"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     cat "$HOME"/Documents/.temp_qr_code_choice.txt | \
     awk '{$1="";print $0}' | awk '{print $NF}' | xargs -d '\n' "$MY_BROWSER"
   fi
	
}
export -f goto_link

#-----------------------------------------------------#
# Go to browser after google search 
#-----------------------------------------------------#

goto_browser() {

   killall yad     
   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     search="https://www.google.co.uk/search?q={}"
     google_search_choice=$(cat "$HOME"/Documents/.temp_qr_code_choice.txt)
     echo "$google_search_choice" | awk '{$1="";print $0}' | xargs -d '\n' -I{} "$MY_BROWSER" "$search"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     search="https://www.google.co.uk/search?q={}"
     cat "$HOME"/Documents/.temp_qr_code_choice.txt | awk '{$1="";print $0}' | xargs -d '\n' -I{} "$MY_BROWSER" "$search"
   fi
	
}
export -f goto_browser

#-----------------------------------------------------#
# Open qr code in file manager
#-----------------------------------------------------#

open_qr_code_in_file_manager() {

   if [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     killall yad
     chosen_file=$(cat "$HOME"/Documents/.temp_qr_code_choice.txt | awk '{print $1}')
     [ -z "$chosen_file" ] && exit 0
     "$FILE_MANAGER" "$MY_CODES_DIR"/"$chosen_file"
     bash ~/bin/switch_to_filemanager.sh
   elif [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     killall yad
     chosen_file=$(cat "$HOME"/Documents/.temp_qr_code_choice.txt | awk '{print $1}')
     [ -z "$chosen_file" ] && exit 0
     "$FILE_MANAGER" "$MY_CODES_DIR"/"$chosen_file"
   else
     :
   fi

}
export -f open_qr_code_in_file_manager

#-----------------------------------------------------#
# Open path in file manager
#-----------------------------------------------------#

open_path_in_file_manager() {

   if [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     killall yad
     chosen_file=$(cat "$HOME"/Documents/.temp_qr_code_choice.txt | awk '{$1="";print $0}' | sed 's| ||')
     [ -z "$chosen_file" ] && exit 0
     "$FILE_MANAGER" "$chosen_file"
     bash ~/bin/switch_to_filemanager.sh
   elif [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     killall yad
     chosen_file=$(cat "$HOME"/Documents/.temp_qr_code_choice.txt | awk '{$1="";print $0}' | sed 's| ||')
     [ -z "$chosen_file" ] && exit 0
     "$FILE_MANAGER" "$chosen_file"
   else
     :
   fi
	
}
export -f open_path_in_file_manager

#-----------------------------------------------------#
# Copy png file to clipboard
#-----------------------------------------------------#

copy_qr_code() {

killall yad
cat "$HOME"/Documents/.temp_qr_code_choice.txt | \
awk '{print $1}' | xargs -d '\n' | tee ~/Documents/.qrcode_list_copy_png.txt
copy_png=$(cat ~/Documents/.qrcode_list_copy_png.txt | xargs -0 basename)
wl-copy --type image/png < "$MY_CODES_DIR"/"$copy_png"

}
export -f copy_qr_code

#-----------------------------------------------------#
# Open path in terminal
#-----------------------------------------------------#

open_terminal() {

killall yad
term_text=$(cat "$HOME"/Documents/.temp_qr_code_choice.txt | awk '{$1="";print $0}') 
[ -z "$term_text" ] && exit 0
echo "$term_text" | tee "$HOME"/Documents/.cd_path.txt
path_file=$(cat "$HOME"/Documents/.cd_path.txt | sed 's| ||')
"$MY_TERMINAL" -- /bin/bash -c "cd \"$path_file\"; $SHELL"

}
export -f open_terminal

#-----------------------------------------------------#
# Open qr code png in image editor
#-----------------------------------------------------#

open_file_in_xdg() {

killall yad
xdg_choice=$(cat "$HOME"/Documents/.temp_qr_code_choice.txt | awk '{$1="";print $0}' | sed 's| ||')
xdg-open "$xdg_choice"
	
}
export -f open_file_in_xdg

#-----------------------------------------------------#
# Open qr code png in image editor
#-----------------------------------------------------#

open_qr_code_in_image_viewer() {

killall yad
image_choice=$(cat "$HOME"/Documents/.temp_qr_code_choice.txt | awk '{print $1}' | sed 's| ||')
"$MY_IMAGE_VIEWER" "$MY_CODES_DIR"/"$image_choice"
	
}
export -f open_qr_code_in_image_viewer

#-----------------------------------------------------#
# Send all Qr Code information to clipboard
#-----------------------------------------------------#

send_all_to_clip() {

killall yad
cat ~/Documents/.temp_qr_code_choice.txt | wl-copy -n
	
}
export -f send_all_to_clip

#-----------------------------------------------------#
# Delete Qr Code
#-----------------------------------------------------#

delete_qr_code() {

killall yad

del_choice=$(cat ~/Documents/.temp_qr_code_choice.txt | awk -F ".png" '{print $1}')

rm -f "$MY_CODES_DIR"/"$del_choice".png || exit 1
	
}
export -f delete_qr_code

#-----------------------------------------------------#
# Search history yad dialog
#-----------------------------------------------------#

yad_search_history() {

killall yad
declare -a SEARCH_HISTORY_FILE="($(< ~/Documents/.qrcode_grep.txt))"
printf '%s\n' "${SEARCH_HISTORY_FILE[@]}" | GTK_THEME="$MY_GTK_THEME" yad --list \
--search-column=3 \
--regex-search \
--buttons-layout="center" \
--title="Wallpapers" \
--text-align="center" \
--text="\nQR Codes\n" \
--borders=10 \
--separator=' ' \
--width=1300 \
--no-markup \
--height=700 \
--dclick-action='/bin/bash -c "echo "$@" > "$HOME"/Documents/.temp_qr_code_choice.txt"' \
--column="QR Code:IMG" \
--column="Filenames" \
--column="Contents" \
--button="_Add text:/bin/bash -c 'text_qrcode'" \
--button="_OCR:/bin/bash -c 'ocr_select'" \
--button="_Web:/bin/bash -c 'goto_link'" \
--button="_File:/bin/bash -c 'open_qr_code_in_file_manager'" \
--button="_Copy:/bin/bash -c 'copy_qr_code'" \
--button="_Google:/bin/bash -c 'goto_browser'" \
--button="_Path:/bin/bash -c 'open_path_in_file_manager'" \
--button="_Board:/bin/bash -c 'send_all_to_clip'" \
--button="_Xdg:/bin/bash -c 'open_file_in_xdg'" \
--button="_QR:/bin/bash -c 'open_qr_code_in_image_viewer'" \
--button="_Exit":1

}
export -f yad_search_history

#-----------------------------------------------------#
# Search yad dialog
#-----------------------------------------------------#

yad_search() {

killall yad
declare -a SEARCH_FILE="($(< ~/Documents/.qrcode_grep.txt))"
printf '%s\n' "${SEARCH_FILE[@]}" | GTK_THEME="$MY_GTK_THEME" yad --list \
--search-column=3 \
--regex-search \
--buttons-layout="center" \
--title="Wallpapers" \
--text-align="center" \
--text="\nQR Codes\n" \
--borders=10 \
--separator=' ' \
--width=1300 \
--no-markup \
--height=700 \
--dclick-action='/bin/bash -c "echo "$@" > "$HOME"/Documents/.temp_qr_code_choice.txt"' \
--column="QR Code:IMG" \
--column="Filenames" \
--column="Contents" \
--button="_Add text:/bin/bash -c 'text_qrcode'" \
--button="_OCR:/bin/bash -c 'ocr_select'" \
--button="_Web:/bin/bash -c 'goto_link'" \
--button="_File:/bin/bash -c 'open_qr_code_in_file_manager'" \
--button="_Copy:/bin/bash -c 'copy_qr_code'" \
--button="_Google:/bin/bash -c 'goto_browser'" \
--button="_Path:/bin/bash -c 'open_path_in_file_manager'" \
--button="_Xdg:/bin/bash -c 'open_file_in_xdg'" \
--button="_Board:/bin/bash -c 'send_all_to_clip'" \
--button="_QR:/bin/bash -c 'open_qr_code_in_image_viewer'" \
--button="_Exit":1

}
export -f yad_search

#-----------------------------------------------------#
# Do a specific file search
#-----------------------------------------------------#

file_search() {

killall yad
choice=$(GTK_THEME="$MY_GTK_THEME" yad --entry --width=800 --height=200 \
--no-headers --separator= \
--text="\nType text to search\n" \
--button="Exit":1)
launch=$(grep -i "$choice" "$HOME"/Pictures/.qrmixed.txt | tee "$HOME"/Documents/.qrcode_grep.txt)
[ -z "$choice" ] && exit 0 || echo "$launch" ; yad_search
  
}
export -f file_search

#-----------------------------------------------------#
# Text file where all qr codes text is sent
#-----------------------------------------------------#

file_dir="$HOME/Pictures/.qrcode_dir.txt"
export file_dir

#-----------------------------------------------------#
# Decodes all qr code png's in directory to text file
#-----------------------------------------------------#

decode_qrcode_dir=$(find "$MY_CODES_DIR" -maxdepth 1 -iname "*.*" -printf '%T@ %p\n' | \
sort -rn | cut -f2- -d" ")
export decode_qrcode_dir
echo "$decode_qrcode_dir" | tee ~/Documents/.all_qrcodes_decoded.txt
all_qrcodes_decoded=$(cat ~/Documents/.all_qrcodes_decoded.txt)
export all_qrcodes_decoded 
echo "$all_qrcodes_decoded" | xargs zbarimg -q --raw > "$file_dir"
awk '{ print "\""$0"\""}' ~/Pictures/.qrcode_dir.txt | tee ~/Pictures/.temp_qrcode_dir.txt

#-----------------------------------------------------#
# Send all filenames in qr codes directory to text file
#-----------------------------------------------------#

decode_filenames_dir=$(find "$MY_CODES_DIR" -maxdepth 1 -iname "*.*" -printf '%T@ %p\n' | \
sort -rn | cut -f2- -d" " | xargs -n 1 basename)
export decode_filenames_dir
echo "$decode_filenames_dir" > "$HOME"/Pictures/.only_qrfilenames.txt
awk '{ print "\""$0"\""}' ~/Pictures/.only_qrfilenames.txt | tee ~/Pictures/.temp_only_qrfilenames.txt

#-----------------------------------------------------#
# Send all filename paths in qr codes directory to text file
#-----------------------------------------------------#

decode_filenames_dir=$(find "$MY_CODES_DIR" -maxdepth 1 -iname "*.*" -printf '%T@ %p\n' | \
sort -rn | cut -f2- -d" ")
export decode_filenames_dir
echo "$decode_filenames_dir" > "$HOME"/Pictures/.qrfilenames.txt
awk '{ print "\""$0"\""}' ~/Pictures/.qrfilenames.txt | tee ~/Pictures/.temp_qrfilenames.txt

#-----------------------------------------------------#
# Removes whitespace from text file
#-----------------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$file_dir"

#-----------------------------------------------------#
# Merge text files
#-----------------------------------------------------#

paste "$HOME"/Pictures/.temp_qrfilenames.txt "$HOME"/Pictures/.temp_only_qrfilenames.txt "$HOME"/Pictures/.temp_qrcode_dir.txt > "$HOME"/Pictures/.qrmixed.txt

#-----------------------------------------------------#
# Create array using text file for main
#-----------------------------------------------------#

declare -a TABLE="($(< ~/Pictures/.qrmixed.txt))"

#-----------------------------------------------------#
# Main yad dialog
#-----------------------------------------------------#

printf '%s\n' "${TABLE[@]}" | GTK_THEME="$MY_GTK_THEME" yad --list \
--search-column=3 \
--regex-search \
--buttons-layout="center" \
--title="Wallpapers" \
--text-align="center" \
--text="\nQR Codes Manager\n" \
--borders=10 \
--separator=' ' \
--width=1300 \
--no-markup \
--height=700 \
--dclick-action='/bin/bash -c "echo "$@" > "$HOME"/Documents/.temp_qr_code_choice.txt"' \
--column="QR Code:IMG" \
--column="Filenames" \
--column="Contents" \
--button="_Add text:/bin/bash -c 'text_qrcode'" \
--button="_OCR:/bin/bash -c 'ocr_select'" \
--button="_Del:/bin/bash -c 'delete_qr_code'" \
--button="_Web:/bin/bash -c 'goto_link'" \
--button="_File:/bin/bash -c 'open_qr_code_in_file_manager'" \
--button="_Copy:/bin/bash -c 'copy_qr_code'" \
--button="_Google:/bin/bash -c 'goto_browser'" \
--button="_Path:/bin/bash -c 'open_path_in_file_manager'" \
--button="_Xdg:/bin/bash -c 'open_file_in_xdg'" \
--button="_QR:/bin/bash -c 'open_qr_code_in_image_viewer'" \
--button="_Board:/bin/bash -c 'send_all_to_clip'" \
--button="_Search:/bin/bash -c 'file_search'" \
--button="_Hist:/bin/bash -c 'yad_search_history'" \
--button="_Exit":1

#-----------------------------------------------------#
# Unset variables and functions
#-----------------------------------------------------#

unset MY_CODES_DIR
unset MY_GTK_THEME
unset file_dir
unset decode_qrcode_dir
unset all_qrcodes_decoded
unset decode_filenames_dir
unset text_qrcode
unset ocr_select
unset goto_link
unset MY_BROWSER
unset goto_browser
unset open_qr_code_in_file_manager
unset FILE_MANAGER
unset copy_qr_code
unset open_terminal
unset MY_TERMINAL
unset open_path_in_file_manager
unset open_qr_code_in_image_viewer
unset MY_IMAGE_VIEWER
unset open_file_in_xdg
unset file_search
unset yad_search
unset yad_search_history
unset QR_HEX_COLOR
unset QR_HEX_MARGIN
unset send_all_to_clip
unset delete_qr_code
