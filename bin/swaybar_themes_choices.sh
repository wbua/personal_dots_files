#!/bin/bash

# Program command: swaybar_themes_choices.sh
# Description: Changes swaybar and i3blocks themes.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 11-03-2025
# Program_license: GPL 3.0
# Dependencies: yad

# Settings
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

# Inverted colors
# shellcheck source=/dev/null
source ~/.config/i3blocks/scripts/store/inverted_colors

#-------------------------------------------------------#
# User preferences
#-------------------------------------------------------#

# Background color
MY_COLOR_BG="$COLOR_BG"
export MY_COLOR_BG

# Icon color
MY_COLOR_ICON="$COLOR_ICON"
export MY_COLOR_ICON

# Color alert
MY_COLOR_ALERT="$COLOR_ALERT"
export MY_COLOR_ALERT

# Description
MY_COLOR_DES="$COLOR_DES"
export MY_COLOR_DES

# Swaybar transparency
MY_SWAYBAR_COLOR="$SWAYBAR_BACKGROUND_COLOR"
export MY_SWAYBAR_COLOR

# Swaybar only color
MY_SWAYBAR_ONLY_COLOR="$SWAYBAR_BACKGROUND_ONLY_COLOR"
export MY_SWAYBAR_ONLY_COLOR

# White color
WHITE_COLOR="#FFFFFF"
export WHITE_COLOR

# Black color
BLACK_COLOR="#000000"
export BLACK_COLOR

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# BG transparency
MY_BG_TRANSPARENCY="$BG_TRANSPARENCY"
export MY_BG_TRANSPARENCY

# Corner transparency
MY_CORNER_TRANSPARENCY="$CORNER_TRANSPARENCY"
export MY_CORNER_TRANSPARENCY

# Text transparency
MY_TEXT_TRANSPARENCY="$TEXT_TRANSPARENCY"
export MY_TEXT_TRANSPARENCY

# Colors
MY_THEME_CONFIG="$I3BLOCKS_THEMES_CONFIG"
export MY_THEME_CONFIG

# Inverted colors
INVERT_STORE="$INVERTED_COLORS_STORE"
export INVERT_STORE

# Swaybar and i3blocks bar position
I3BLOCKS_POSITION="$I3BLOCKS_BAR_POSITION"
export I3BLOCKS_POSITION

#-------------------------------------------------------#
# Create files
#-------------------------------------------------------#

   if [ -f "$HOME"/Documents/.random_modules.txt ]; then
     :
   else
     touch "$HOME"/Documents/.random_modules.txt
   fi

   if [ -f "$HOME"/Documents/.i3blocks_inverted_modules ]; then
     :
   else
     touch "$HOME"/Documents/.i3blocks_inverted_modules
   fi
   
#-------------------------------------------------------#
# Swaybar transparency
#-------------------------------------------------------#

bar_theme_two() {

if [[ "${MY_SWAYBAR_COLOR: -2}" == "FF" ]]; then
killall yad
sed -i "s|.*\(SWAYBAR_BACKGROUND_COLOR=\).*|SWAYBAR_BACKGROUND_COLOR=\"$(printf '%s' "${MY_SWAYBAR_ONLY_COLOR}00")\"|" "$MY_THEME_CONFIG" || exit 1
bash ~/bin/sway_load_settings.sh
elif [[ "${MY_SWAYBAR_COLOR: -2}" == "00" ]]; then
killall yad
sed -i "s|.*\(SWAYBAR_BACKGROUND_COLOR=\).*|SWAYBAR_BACKGROUND_COLOR=\"$(printf '%s' "${MY_SWAYBAR_ONLY_COLOR}FF")\"|" "$MY_THEME_CONFIG" || exit 1
bash ~/bin/sway_load_settings.sh
else
killall yad
sed -i "s|.*\(SWAYBAR_BACKGROUND_COLOR=\).*|SWAYBAR_BACKGROUND_COLOR=\"$(printf '%s' "${MY_SWAYBAR_ONLY_COLOR}FF")\"|" "$MY_THEME_CONFIG" || exit 1
bash ~/bin/sway_load_settings.sh
fi

}
export -f bar_theme_two

#-------------------------------------------------------#
# Swaybar 50%
#-------------------------------------------------------#

module_50per() {

killall yad
sed -i "s|.*\(SWAYBAR_BACKGROUND_COLOR=\).*|SWAYBAR_BACKGROUND_COLOR=\"$(printf '%s' "${MY_SWAYBAR_ONLY_COLOR}B3")\"|" "$MY_THEME_CONFIG" || exit 1
bash ~/bin/sway_load_settings.sh

}
export -f module_50per

#-------------------------------------------------------#
# Module transparency
#-------------------------------------------------------#

module_transparency() {

truncate -s 0 ~/Documents/.random_modules.txt || exit
killall yad
if [[ "$MY_BG_TRANSPARENCY" == "03" ]]; then
sed -i "s|.*\(BG_TRANSPARENCY=\).*|BG_TRANSPARENCY=\"$(printf '%s' "FF")\"|" "$MY_THEME_CONFIG" || exit 1
sed -i "s|.*\(CORNER_TRANSPARENCY=\).*|CORNER_TRANSPARENCY=\"$(printf '%s' "FF")\"|" "$MY_THEME_CONFIG" || exit 1
sed -i "s|.*\(TEXT_TRANSPARENCY=\).*|TEXT_TRANSPARENCY=\"$(printf '%s' "FF")\"|" "$MY_THEME_CONFIG" || exit 1
bash ~/bin/sway_load_settings.sh
else
sed -i "s|.*\(BG_TRANSPARENCY=\).*|BG_TRANSPARENCY=\"$(printf '%s' "03")\"|" "$MY_THEME_CONFIG" || exit 1
sed -i "s|.*\(CORNER_TRANSPARENCY=\).*|CORNER_TRANSPARENCY=\"$(printf '%s' "03")\"|" "$MY_THEME_CONFIG" || exit 1
sed -i "s|.*\(TEXT_TRANSPARENCY=\).*|TEXT_TRANSPARENCY=\"$(printf '%s' "03")\"|" "$MY_THEME_CONFIG" || exit 1
bash ~/bin/sway_load_settings.sh
fi
	
}
export -f module_transparency

#-------------------------------------------------------#
# Random module transparency
#-------------------------------------------------------#

random_trans_modules() {

killall yad

truncate -s 0 ~/Documents/.random_modules.txt || exit

modules_text_list="
volume
night
chatbot
picker
apps
search
workspaces
bright
microphone
battery
date
notify
layout
player
powermenu
keyboard
settings
keys"

random_modules=$(echo "$modules_text_list" | shuf -n 7)
echo "$random_modules" | tee ~/Documents/.random_modules.txt || exit 1

bash ~/bin/sway_load_settings.sh

}
export -f random_trans_modules

#-------------------------------------------------------#
# Show random module text file names
#-------------------------------------------------------#

random_module_names() {

killall yad
my_mod_list=$(echo "volume
night
chatbot
picker
apps
search
workspaces
bright
microphone
battery
date
notify
layout
player
powermenu
keyboard
keys
settings
browser")

pick_names_list=$(echo "$my_mod_list" | GTK_THEME="$MY_GTK_THEME" yad --list \
 --multiple \
 --column="list" \
 --separator= \
 --width=500 \
 --height=500 \
 --button="Exit":1)
[ -z "$pick_names_list" ] && exit 0

echo "$pick_names_list" | wl-copy -n

}
export -f random_module_names

#-------------------------------------------------------#
# Open random module text file
#-------------------------------------------------------#

open_random_module_text_file() {

killall yad
xdg-open ~/Documents/.random_modules.txt
	
}
export -f open_random_module_text_file

#-------------------------------------------------------#
# Title inverted color
#-------------------------------------------------------#

title_inverted_color() {

   if [[ "$INVERT_STORE" == "0" ]]; then
     sed -i "s|.*\(INVERTED_COLORS_STORE=\).*|INVERTED_COLORS_STORE=\"$(printf '%s' "1")\"|" ~/.config/i3blocks/scripts/store/inverted_colors || exit 1
     echo "title" | tee "$HOME"/Documents/.i3blocks_inverted_modules || exit 1
     killall yad
   elif [[ "$INVERT_STORE" == "1" ]]; then
     sed -i "s|.*\(INVERTED_COLORS_STORE=\).*|INVERTED_COLORS_STORE=\"$(printf '%s' "0")\"|" ~/.config/i3blocks/scripts/store/inverted_colors || exit 1
     truncate -s 0 "$HOME"/Documents/.i3blocks_inverted_modules
     killall yad
   fi

}
export -f title_inverted_color

#-------------------------------------------------------#
# Rounded edge shapes
#-------------------------------------------------------#

rounded_edge_shapes() {

sed -i "s|.*\(MODULE_LEFT_SHAPE=\).*|MODULE_LEFT_SHAPE=\"$(printf '%s' "")\"|" "$MY_THEME_CONFIG" || exit 1
sed -i "s|.*\(MODULE_RIGHT_SHAPE=\).*|MODULE_RIGHT_SHAPE=\"$(printf '%s' "")\"|" "$MY_THEME_CONFIG" || exit 1
sed -i "s|.*\(BEFORE_AFTER_OUTPUT=\).*|BEFORE_AFTER_OUTPUT=\"$(printf '%s' "")\"|" ~/bin/i3blocks_accent_colors || exit 1
sed -i "s|.*\(SEPARATOR_BLOCK_WIDTH=\).*|SEPARATOR_BLOCK_WIDTH=\"$(printf '%s' "10")\"|" ~/bin/i3blocks_accent_colors || exit 1
bash ~/bin/create_i3blocks_config.sh
bash ~/bin/sway_load_settings.sh
   	
}
export -f rounded_edge_shapes

#-------------------------------------------------------#
# Triangle edge shapes
#-------------------------------------------------------#

triangle_edge_shapes() {

sed -i "s|.*\(MODULE_LEFT_SHAPE=\).*|MODULE_LEFT_SHAPE=\"$(printf '%s' "")\"|" "$MY_THEME_CONFIG" || exit 1
sed -i "s|.*\(MODULE_RIGHT_SHAPE=\).*|MODULE_RIGHT_SHAPE=\"$(printf '%s' "")\"|" "$MY_THEME_CONFIG" || exit 1
sed -i "s|.*\(BEFORE_AFTER_OUTPUT=\).*|BEFORE_AFTER_OUTPUT=\"$(printf '%s' "")\"|" ~/bin/i3blocks_accent_colors || exit 1
sed -i "s|.*\(SEPARATOR_BLOCK_WIDTH=\).*|SEPARATOR_BLOCK_WIDTH=\"$(printf '%s' "10")\"|" ~/bin/i3blocks_accent_colors || exit 1
bash ~/bin/create_i3blocks_config.sh
bash ~/bin/sway_load_settings.sh
        	
}
export -f triangle_edge_shapes

#-------------------------------------------------------#
# Basic bar
#-------------------------------------------------------#

basic_i3blocks() {

# Turn off modules
sed -i "s|.*\(MODULE_ACTIVE_OFFICE=\).*|MODULE_ACTIVE_OFFICE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_AUDIOEDITOR=\).*|MODULE_ACTIVE_AUDIOEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_VIDEOEDITOR=\).*|MODULE_ACTIVE_VIDEOEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_IMAGEEDITOR=\).*|MODULE_ACTIVE_IMAGEEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_SOCIAL=\).*|MODULE_ACTIVE_SOCIAL=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_DEVELOPMENT=\).*|MODULE_ACTIVE_DEVELOPMENT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_TEMPORARY=\).*|MODULE_ACTIVE_TEMPORARY=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_TEXTEDITOR=\).*|MODULE_ACTIVE_TEXTEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_TERMINAL=\).*|MODULE_ACTIVE_TERMINAL=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_FILEMANAGER=\).*|MODULE_ACTIVE_FILEMANAGER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_BROWSER=\).*|MODULE_ACTIVE_BROWSER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_SETTINGS=\).*|MODULE_SETTINGS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_QUICK_ACTIONS=\).*|MODULE_QUICK_ACTIONS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_POWERMENU=\).*|MODULE_POWERMENU=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_CHATBOT=\).*|MODULE_CHATBOT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_LOCAL_SEARCH=\).*|MODULE_LOCAL_SEARCH=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WEATHER=\).*|MODULE_WEATHER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_QUICK_PLAYER=\).*|MODULE_QUICK_PLAYER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIN_TITLE=\).*|MODULE_WIN_TITLE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_PICKER=\).*|MODULE_PICKER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIKI=\).*|MODULE_WIKI=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_APPS=\).*|MODULE_APPS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_LAYOUT_KEYBOARD=\).*|MODULE_LAYOUT_KEYBOARD=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_NOTIFICATIONS=\).*|MODULE_NOTIFICATIONS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_KEYBDS=\).*|MODULE_KEYBDS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIFI=\).*|MODULE_WIFI=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_MONITOR=\).*|MODULE_MONITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_PLAYERS=\).*|MODULE_PLAYERS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_NIGHTLIGHT=\).*|MODULE_NIGHTLIGHT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_LAYOUTS=\).*|MODULE_LAYOUTS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_MULTIPLE_WORKSPACES=\).*|MODULE_MULTIPLE_WORKSPACES=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WORKSPACE=\).*|MODULE_WORKSPACE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BRIGHTNESS=\).*|MODULE_BRIGHTNESS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_VOLUME=\).*|MODULE_VOLUME=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_TIME=\).*|MODULE_TIME=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_DATE=\).*|MODULE_DATE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_MICROPHONE=\).*|MODULE_MICROPHONE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BATTERY=\).*|MODULE_BATTERY=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_SEPARATE_WORKSPACES=\).*|MODULE_SEPARATE_WORKSPACES=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\).*|MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1

# Turn on modules
sed -i "s|.*\(MODULE_POWERMENU=\).*|MODULE_POWERMENU=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_DATE=\).*|MODULE_DATE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_VOLUME=\).*|MODULE_VOLUME=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_OFFICE=\).*|MODULE_ACTIVE_OFFICE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_AUDIOEDITOR=\).*|MODULE_ACTIVE_AUDIOEDITOR=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_VIDEOEDITOR=\).*|MODULE_ACTIVE_VIDEOEDITOR=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_IMAGEEDITOR=\).*|MODULE_ACTIVE_IMAGEEDITOR=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_SOCIAL=\).*|MODULE_ACTIVE_SOCIAL=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_DEVELOPMENT=\).*|MODULE_ACTIVE_DEVELOPMENT=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_TEXTEDITOR=\).*|MODULE_ACTIVE_TEXTEDITOR=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_TERMINAL=\).*|MODULE_ACTIVE_TERMINAL=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_FILEMANAGER=\).*|MODULE_ACTIVE_FILEMANAGER=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_BROWSER=\).*|MODULE_ACTIVE_BROWSER=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WORKSPACE=\).*|MODULE_WORKSPACE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_QUICK_PLAYER=\).*|MODULE_QUICK_PLAYER=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIN_TITLE=\).*|MODULE_WIN_TITLE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_NOTIFICATIONS=\).*|MODULE_NOTIFICATIONS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BATTERY=\).*|MODULE_BATTERY=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1

bash ~/bin/sway_load_settings.sh

}
export -f basic_i3blocks

#-------------------------------------------------------#
# Bar type one
#-------------------------------------------------------#

type_one_bar() {

# Turn off modules
sed -i "s|.*\(MODULE_ACTIVE_OFFICE=\).*|MODULE_ACTIVE_OFFICE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_AUDIOEDITOR=\).*|MODULE_ACTIVE_AUDIOEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_VIDEOEDITOR=\).*|MODULE_ACTIVE_VIDEOEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_IMAGEEDITOR=\).*|MODULE_ACTIVE_IMAGEEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_SOCIAL=\).*|MODULE_ACTIVE_SOCIAL=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_DEVELOPMENT=\).*|MODULE_ACTIVE_DEVELOPMENT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_TEMPORARY=\).*|MODULE_ACTIVE_TEMPORARY=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_TEXTEDITOR=\).*|MODULE_ACTIVE_TEXTEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_TERMINAL=\).*|MODULE_ACTIVE_TERMINAL=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_FILEMANAGER=\).*|MODULE_ACTIVE_FILEMANAGER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_BROWSER=\).*|MODULE_ACTIVE_BROWSER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_SETTINGS=\).*|MODULE_SETTINGS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_QUICK_ACTIONS=\).*|MODULE_QUICK_ACTIONS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_POWERMENU=\).*|MODULE_POWERMENU=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_CHATBOT=\).*|MODULE_CHATBOT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_LOCAL_SEARCH=\).*|MODULE_LOCAL_SEARCH=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WEATHER=\).*|MODULE_WEATHER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_QUICK_PLAYER=\).*|MODULE_QUICK_PLAYER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIN_TITLE=\).*|MODULE_WIN_TITLE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_PICKER=\).*|MODULE_PICKER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIKI=\).*|MODULE_WIKI=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_APPS=\).*|MODULE_APPS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_LAYOUT_KEYBOARD=\).*|MODULE_LAYOUT_KEYBOARD=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_NOTIFICATIONS=\).*|MODULE_NOTIFICATIONS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_KEYBDS=\).*|MODULE_KEYBDS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIFI=\).*|MODULE_WIFI=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_MONITOR=\).*|MODULE_MONITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_PLAYERS=\).*|MODULE_PLAYERS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_NIGHTLIGHT=\).*|MODULE_NIGHTLIGHT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_LAYOUTS=\).*|MODULE_LAYOUTS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_MULTIPLE_WORKSPACES=\).*|MODULE_MULTIPLE_WORKSPACES=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WORKSPACE=\).*|MODULE_WORKSPACE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BRIGHTNESS=\).*|MODULE_BRIGHTNESS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_VOLUME=\).*|MODULE_VOLUME=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_TIME=\).*|MODULE_TIME=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_DATE=\).*|MODULE_DATE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_MICROPHONE=\).*|MODULE_MICROPHONE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BATTERY=\).*|MODULE_BATTERY=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_SEPARATE_WORKSPACES=\).*|MODULE_SEPARATE_WORKSPACES=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\).*|MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1

# Turn on modules
sed -i "s|.*\(MODULE_POWERMENU=\).*|MODULE_POWERMENU=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_DATE=\).*|MODULE_DATE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_VOLUME=\).*|MODULE_VOLUME=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_KEYBDS=\).*|MODULE_KEYBDS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_SETTINGS=\).*|MODULE_SETTINGS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1	
sed -i "s|.*\(MODULE_NIGHTLIGHT=\).*|MODULE_NIGHTLIGHT=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_NOTIFICATIONS=\).*|MODULE_NOTIFICATIONS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BATTERY=\).*|MODULE_BATTERY=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WORKSPACE=\).*|MODULE_WORKSPACE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_QUICK_PLAYER=\).*|MODULE_QUICK_PLAYER=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIN_TITLE=\).*|MODULE_WIN_TITLE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_MICROPHONE=\).*|MODULE_MICROPHONE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BRIGHTNESS=\).*|MODULE_BRIGHTNESS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_QUICK_ACTIONS=\).*|MODULE_QUICK_ACTIONS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh
sed -i "s|.*\(MODULE_APPS=\).*|MODULE_APPS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1

bash ~/bin/sway_load_settings.sh

}
export -f type_one_bar

#-------------------------------------------------------#
# Bar type two
#-------------------------------------------------------#

bar_type_two() {

# Turn off modules
sed -i "s|.*\(MODULE_ACTIVE_OFFICE=\).*|MODULE_ACTIVE_OFFICE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_AUDIOEDITOR=\).*|MODULE_ACTIVE_AUDIOEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_VIDEOEDITOR=\).*|MODULE_ACTIVE_VIDEOEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_IMAGEEDITOR=\).*|MODULE_ACTIVE_IMAGEEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_SOCIAL=\).*|MODULE_ACTIVE_SOCIAL=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_DEVELOPMENT=\).*|MODULE_ACTIVE_DEVELOPMENT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_TEMPORARY=\).*|MODULE_ACTIVE_TEMPORARY=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_TEXTEDITOR=\).*|MODULE_ACTIVE_TEXTEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_TERMINAL=\).*|MODULE_ACTIVE_TERMINAL=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_FILEMANAGER=\).*|MODULE_ACTIVE_FILEMANAGER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_BROWSER=\).*|MODULE_ACTIVE_BROWSER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_SETTINGS=\).*|MODULE_SETTINGS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_QUICK_ACTIONS=\).*|MODULE_QUICK_ACTIONS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_POWERMENU=\).*|MODULE_POWERMENU=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_CHATBOT=\).*|MODULE_CHATBOT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_LOCAL_SEARCH=\).*|MODULE_LOCAL_SEARCH=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WEATHER=\).*|MODULE_WEATHER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_QUICK_PLAYER=\).*|MODULE_QUICK_PLAYER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIN_TITLE=\).*|MODULE_WIN_TITLE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_PICKER=\).*|MODULE_PICKER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIKI=\).*|MODULE_WIKI=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_APPS=\).*|MODULE_APPS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_LAYOUT_KEYBOARD=\).*|MODULE_LAYOUT_KEYBOARD=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_NOTIFICATIONS=\).*|MODULE_NOTIFICATIONS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_KEYBDS=\).*|MODULE_KEYBDS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIFI=\).*|MODULE_WIFI=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_MONITOR=\).*|MODULE_MONITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_PLAYERS=\).*|MODULE_PLAYERS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_NIGHTLIGHT=\).*|MODULE_NIGHTLIGHT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_LAYOUTS=\).*|MODULE_LAYOUTS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_MULTIPLE_WORKSPACES=\).*|MODULE_MULTIPLE_WORKSPACES=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WORKSPACE=\).*|MODULE_WORKSPACE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BRIGHTNESS=\).*|MODULE_BRIGHTNESS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_VOLUME=\).*|MODULE_VOLUME=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_TIME=\).*|MODULE_TIME=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_DATE=\).*|MODULE_DATE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_MICROPHONE=\).*|MODULE_MICROPHONE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BATTERY=\).*|MODULE_BATTERY=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_SEPARATE_WORKSPACES=\).*|MODULE_SEPARATE_WORKSPACES=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\).*|MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1

# Enable modules
sed -i "s|.*\(MODULE_BATTERY=\).*|MODULE_BATTERY=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WORKSPACE=\).*|MODULE_WORKSPACE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\).*|MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_POWERMENU=\).*|MODULE_POWERMENU=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_SETTINGS=\).*|MODULE_SETTINGS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_KEYBDS=\).*|MODULE_KEYBDS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_NOTIFICATIONS=\).*|MODULE_NOTIFICATIONS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_DATE=\).*|MODULE_DATE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_APPS=\).*|MODULE_APPS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_QUICK_ACTIONS=\).*|MODULE_QUICK_ACTIONS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIN_TITLE=\).*|MODULE_WIN_TITLE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1

bash ~/bin/sway_load_settings.sh
	
}
export -f bar_type_two

#-------------------------------------------------------#
# Very basic bar
#-------------------------------------------------------#

very_basic_bar() {

# Turn off modules
sed -i "s|.*\(MODULE_ACTIVE_OFFICE=\).*|MODULE_ACTIVE_OFFICE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_AUDIOEDITOR=\).*|MODULE_ACTIVE_AUDIOEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_VIDEOEDITOR=\).*|MODULE_ACTIVE_VIDEOEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_IMAGEEDITOR=\).*|MODULE_ACTIVE_IMAGEEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_SOCIAL=\).*|MODULE_ACTIVE_SOCIAL=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_DEVELOPMENT=\).*|MODULE_ACTIVE_DEVELOPMENT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_TEMPORARY=\).*|MODULE_ACTIVE_TEMPORARY=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_TEXTEDITOR=\).*|MODULE_ACTIVE_TEXTEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_TERMINAL=\).*|MODULE_ACTIVE_TERMINAL=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_FILEMANAGER=\).*|MODULE_ACTIVE_FILEMANAGER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_ACTIVE_BROWSER=\).*|MODULE_ACTIVE_BROWSER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_SETTINGS=\).*|MODULE_SETTINGS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_QUICK_ACTIONS=\).*|MODULE_QUICK_ACTIONS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_POWERMENU=\).*|MODULE_POWERMENU=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_CHATBOT=\).*|MODULE_CHATBOT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_LOCAL_SEARCH=\).*|MODULE_LOCAL_SEARCH=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WEATHER=\).*|MODULE_WEATHER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_QUICK_PLAYER=\).*|MODULE_QUICK_PLAYER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIN_TITLE=\).*|MODULE_WIN_TITLE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_PICKER=\).*|MODULE_PICKER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIKI=\).*|MODULE_WIKI=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_APPS=\).*|MODULE_APPS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_LAYOUT_KEYBOARD=\).*|MODULE_LAYOUT_KEYBOARD=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_NOTIFICATIONS=\).*|MODULE_NOTIFICATIONS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_KEYBDS=\).*|MODULE_KEYBDS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIFI=\).*|MODULE_WIFI=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_MONITOR=\).*|MODULE_MONITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_PLAYERS=\).*|MODULE_PLAYERS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_NIGHTLIGHT=\).*|MODULE_NIGHTLIGHT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_LAYOUTS=\).*|MODULE_LAYOUTS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_MULTIPLE_WORKSPACES=\).*|MODULE_MULTIPLE_WORKSPACES=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WORKSPACE=\).*|MODULE_WORKSPACE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BRIGHTNESS=\).*|MODULE_BRIGHTNESS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_VOLUME=\).*|MODULE_VOLUME=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_TIME=\).*|MODULE_TIME=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_DATE=\).*|MODULE_DATE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_MICROPHONE=\).*|MODULE_MICROPHONE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BATTERY=\).*|MODULE_BATTERY=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_SEPARATE_WORKSPACES=\).*|MODULE_SEPARATE_WORKSPACES=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\).*|MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1

# Enable modules
sed -i "s|.*\(MODULE_BATTERY=\).*|MODULE_BATTERY=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_SEPARATE_WORKSPACES=\).*|MODULE_SEPARATE_WORKSPACES=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\).*|MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_POWERMENU=\).*|MODULE_POWERMENU=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_NOTIFICATIONS=\).*|MODULE_NOTIFICATIONS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_DATE=\).*|MODULE_DATE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
sed -i "s|.*\(MODULE_WIN_TITLE=\).*|MODULE_WIN_TITLE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1

bash ~/bin/sway_load_settings.sh
	
}
export -f very_basic_bar

#-------------------------------------------------------#
# Swaybar and i3blocks bar position
#-------------------------------------------------------#

bar_position() {

   if [[ "$I3BLOCKS_POSITION" == "top" ]]; then
     sed -i "s|.*\(I3BLOCKS_BAR_POSITION=\).*|I3BLOCKS_BAR_POSITION=\"$(printf '%s' "bottom")\"|" ~/bin/i3blocks_accent_colors || exit 1
   elif [[ "$I3BLOCKS_POSITION" == "bottom" ]]; then
     sed -i "s|.*\(I3BLOCKS_BAR_POSITION=\).*|I3BLOCKS_BAR_POSITION=\"$(printf '%s' "top")\"|" ~/bin/i3blocks_accent_colors || exit 1
   else
     :
   fi

bash ~/bin/sway_load_settings.sh
	
}
export -f bar_position

#-------------------------------------------------------#
# Square module edges
#-------------------------------------------------------#

square_edges() {

sed -i "s|.*\(MODULE_RIGHT_SHAPE=\).*|MODULE_RIGHT_SHAPE=\"$(printf '%s' " ")\"|" ~/bin/i3blocks_accent_colors || exit 1
sed -i "s|.*\(MODULE_LEFT_SHAPE=\).*|MODULE_LEFT_SHAPE=\"$(printf '%s' " ")\"|" ~/bin/i3blocks_accent_colors || exit 1
sed -i "s|.*\(BEFORE_AFTER_OUTPUT=\).*|BEFORE_AFTER_OUTPUT=\"$(printf '%s' " ")\"|" ~/bin/i3blocks_accent_colors || exit 1
sed -i "s|.*\(SEPARATOR_BLOCK_WIDTH=\).*|SEPARATOR_BLOCK_WIDTH=\"$(printf '%s' "0")\"|" ~/bin/i3blocks_accent_colors || exit 1
bash ~/bin/create_i3blocks_config.sh
bash ~/bin/sway_load_settings.sh
	
}
export -f square_edges

#-------------------------------------------------------#
# Yad dialog
#-------------------------------------------------------#

GTK_THEME="$MY_GTK_THEME" yad --form \
 --title="Swaybar themes" \
 --text-align="center" \
 --text="\nSwaybar theme changer\n" \
 --scroll \
 --width=400 \
 --height=500 \
 --borders=20 \
 --columns=1 \
 --field="<b>Minimal bar</b>":fbtn '/bin/bash -c "basic_i3blocks"' \
 --field="<b>Very basic bar</b>":fbtn '/bin/bash -c "very_basic_bar"' \
 --field="<b>Type one bar</b>":fbtn '/bin/bash -c "type_one_bar"' \
 --field="<b>Type two bar</b>":fbtn '/bin/bash -c "bar_type_two"' \
 --field="<b>Bar position</b>":fbtn '/bin/bash -c "bar_position"' \
 --field="<b>Swaybar transparency</b>":fbtn '/bin/bash -c "bar_theme_two"' \
 --field="<b>Module transparency</b>":fbtn '/bin/bash -c "module_transparency"' \
 --field="<b>Swaybar 70% transparency</b>":fbtn '/bin/bash -c "module_50per"' \
 --field="<b>Inverted colors</b>":fbtn '/bin/bash -c "title_inverted_color"' \
 --field="<b>Triangle edge shapes</b>":fbtn '/bin/bash -c "triangle_edge_shapes"' \
 --field="<b>Square edge shapes</b>":fbtn '/bin/bash -c "square_edges"' \
 --field="<b>Rounded edge shapes</b>":fbtn '/bin/bash -c "rounded_edge_shapes"' \
 --field="<b>Random module transparency</b>":fbtn '/bin/bash -c "random_trans_modules"' \
 --field="<b>Open random module text file</b>":fbtn '/bin/bash -c "open_random_module_text_file"' \
 --field="<b>Random module names</b>":fbtn '/bin/bash -c "random_module_names"' \
 --button="Exit":1
 
#-------------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------------#

unset MY_COLOR_BG
unset MY_COLOR_ICON
unset MY_COLOR_ALERT
unset MY_COLOR_DES
unset BLACK_COLOR
unset WHITE_COLOR
unset MY_SWAYBAR_COLOR
unset MY_GTK_THEME
unset MY_SWAYBAR_ONLY_COLOR
unset MY_TEXT_TRANSPARENCY
unset MY_CORNER_TRANSPARENCY
unset MY_BG_TRANSPARENCY
unset module_transparency
unset module_50per
unset MY_THEME_CONFIG
unset random_trans_modules
unset open_random_module_text_file
unset random_module_names
unset title_inverted_color
unset INVERT_STORE
unset triangle_edge_shapes
unset rounded_edge_shapes
unset basic_i3blocks
unset type_one_bar
unset I3BLOCKS_POSITION
unset bar_position
unset square_edges
unset bar_type_two
unset very_basic_bar
