#!/bin/bash

# Program command: interface.sh
# Description: Checks services are running. Then toggles them on/off
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 28-10-2023
# Program_license: GPL 3.0
# Dependencies: yad, google inter font, custom gtk-css theme

#-------------------------------------------------------#
# Autostart_example.sh
#-------------------------------------------------------#

file_contents="picom --config /home/john/.config/picom/picom.conf &
xfce4-power-manager &"
export file_contents

   if [ -f "$HOME"/Documents/autostart_example.sh ]; then
     :
   else
     touch "$HOME"/Documents/autostart_example.sh
     echo "$file_contents" >> "$HOME"/Documents/autostart_example.sh
   fi

#-------------------------------------------------------#
# Text files
#-------------------------------------------------------#

my_autostart_file="$HOME/Documents/autostart_example.sh"
export my_autostart_file

#-------------------------------------------------------#
# Checks files
#-------------------------------------------------------#

# Service one
check_file() {

   if [[ "$(grep -o "^[#]picom.*$" "$my_autostart_file")" == *"#"* ]]; then
     echo "${color5}off${end}"
   else
     echo "${color4}on${end}"
   fi
	
}
export -f check_file 

# Service two
check_file2() {

   if [[ "$(grep -o "^[#]xfce4.*$" "$my_autostart_file")" == *"#"* ]]; then
     echo "${color5}off${end}"
   else
     echo "${color4}on${end}"
   fi
	
}
export -f check_file2 

#-------------------------------------------------------#
# Creates files
#-------------------------------------------------------#

# Service one
create_file() {
	
   if [[ "$(grep -o "^[#]picom.*$" "$my_autostart_file")" == *"#"* ]]; then
     sed -i '1s|^.*$|picom --config /home/john/.config/picom/picom.conf \&|' "$my_autostart_file"
   else
	 sed -i '1s|^.*$|#picom --config /home/john/.config/picom/picom.conf \&|' "$my_autostart_file"
   fi
	
}
export -f create_file

# Service two
create_file2() {
	
   if [[ "$(grep -o "^[#]xfce4.*$" "$my_autostart_file")" == *"#"* ]]; then
     sed -i '2s|^.*$|xfce4-power-manager \&|' "$my_autostart_file"
   else
	 sed -i '2s|^.*$|#xfce4-power-manager \&|' "$my_autostart_file"
   fi
	
}
export -f create_file2

#-------------------------------------------------------#
# Colors for menu items (pango markup)
#-------------------------------------------------------#

color1="<span color='#FFFFFF' font_family='Inter' font='14' rise='0pt'>"
export color1
color2="<span color='#FFFFFF' font_family='Inter' font='12' rise='0pt'>"
export color2
color3="<span color='#FFFFFF' font_family='Inter' font='14' rise='0pt'>"
export color3
color4="<span color='#29FF00' font_family='Inter' font='14' rise='0pt'>"
export color4
color5="<span color='#FF0027' font_family='Inter' font='14' rise='0pt'>"
export color5
end="</span>"
export end

#-------------------------------------------------------#
# Button service on
#-------------------------------------------------------#

button_service_one() {

  killall yad ; create_file ; sleep 0.5 ; interface.sh	
		
}
export -f button_service_one

#-------------------------------------------------------#
# Button service two
#-------------------------------------------------------#

button_service_two() {
	
  killall yad ; create_file2 ; sleep 0.5 ; interface.sh
	
}
export -f button_service_two 

#-------------------------------------------------------#
# Yad settings
#-------------------------------------------------------# 

GTK_THEME="alt-dialog" yad --form --center --align=center --borders=30 --width=1000 --height=600 --columns=2 --image-on-top --title="Welcome" \
--text-align=center \
--text="${color3}Autostart programs${end}\n\n${color2}Click buttons to turn on/off${end}\n" \
--field="${color2}Picom compositor${end}\n":LBL "" \
--field="<b>$(check_file)</b>":fbtn '/bin/bash -c "button_service_one"' \
--field="${color2}Xfce4 power manager${end}\n":LBL "" \
--field="<b>$(check_file2)</b>":fbtn '/bin/bash -c "button_service_two"' \
--button="Ok":0 \
--button="Exit":1

#-------------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------------#

unset color1
unset color2
unset color3
unset color4
unset color5
unset my_autostart_file
unset check_file
unset check_file2
unset create_file
unset create_file2
unset file_contents
unset end
unset button_service_one
unset button_service_two
