#!/bin/bash
# shellcheck disable=SC2090
# shellcheck disable=SC2089

# Program command: yad_news.sh
# Description: Download BBC news feeds and display them.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 22-08-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad, festival, curl, pandoc, textmaker, qrencode, material icons, wl-clipboard

#-----------------------------------------------------------#
# General variables
#-----------------------------------------------------------#

browser="brave"
export browser

#-----------------------------------------------------------#
# Create files and directories
#-----------------------------------------------------------#

   if [ -f "$HOME"/Documents/.updated_news.txt ]; then
     :
   else
     touch "$HOME"/Documents/.updated_news.txt
   fi

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi
 
   if [ -d "$HOME"/Pictures/qr_codes ]; then
     :
   else
     mkdir "$HOME"/Pictures/qr_codes
   fi
   
   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi
   
   if [ -d "$HOME"/Pictures/.temqrcodes/ ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes/
   fi

#-----------------------------------------------------------#
# Colors from text and buttons
#-----------------------------------------------------------#

# Yad buttons
color01="<span color='#25FF00' font_family='Material Icons' font='25'>"
export color01
# Yad close button
color02="<span color='#FF0020' font_family='Material Icons' font='25' rise='0pt'>"
export color02

# Help page title
color2="<span color='#FFA503' font_family='Monospace' font='18' weight='bold'>"
export color2
# Help page and text info highlighted text
color3="<span color='#FF00FE' font_family='Monospace' font='14' weight='bold'>"
export color3
# Help page icons
color5="<span color='#25FF00' font_family='Material Icons' font='25'>"
export color5
# Help page button definition text
color6="<span color='#FFFFFF' font_family='Monospace' font='14' rise='8pt'>"
export color6
# Help page general text
color7="<span color='#FFFFFF' font_family='Monospace' font='14' rise='0pt'>"
export color7
# Close button icon
color8="<span color='#FF0020' font_family='Material Icons' font='25' rise='0pt'>"
export color8
end="</span>"
export end

#-----------------------------------------------------------#
# Yad settings
#-----------------------------------------------------------#

_yad() {

GTK_THEME="Adwaita-dark" yad --list --column="search" --width=950 --height=500 \
--title="BBC News Feeds" \
--buttons-layout="center" \
--center --no-headers --separator= --multiple \
--button="${color01}${end}":90 \
--button="${color01}${end}":80 \
--button="${color01}${end}":44 \
--button="${color01}${end}":88 \
--button="${color01}${end}":35 \
--button="${color01}${end}":30 \
--button="${color01}${end}":20 \
--button="${color02}${end}":1

echo $?
	
}
export -f _yad

#-------------------------------------------------------#
# Help
#-------------------------------------------------------#

help_info() {

info_text="${color2}Help information${end}

${color7}Download BBC news feeds and display them.${end}

${color2}How to use${end}

${color7}If opening url does not work, then change browser.${end}

${color2}Buttons${end}

${color5}${end} ${color6}Show help page.${end}
${color5}${end} ${color6}Click on url once, and then press the qr code button. This will create a qr code.${end}
${color5}${end} ${color6}Update the BBC news feed.${end}
${color5}${end} ${color6}Click on text once, and then press speak button. It will use text to speech${end}
${color5}${end} ${color6}Change your browser used for opening url's.${end}
${color5}${end} ${color6}Click on url once, and then press the clip button. Sends url to clipboard.${end}
${color5}${end} ${color6}Click on url once, and then press the url button. Opens url in browser.${end}
${color5}${end} ${color6}Kills yad.${end}

${color2}About${end}

${color7}Program created by ${color3}John Mcgrath${end}${end}
"
export info_text
killall yad
echo "$info_text" | GTK_THEME="Adwaita-dark" yad --list --column="help" --height=500 --width=1200 --center \
--borders=5 \
--no-headers --separator= \
--button="${color8}${end}":1

}
export -f help_info

#-----------------------------------------------------------#
# Change current browser
#-----------------------------------------------------------#

change_browser() {

killall yad
sed -i "18s|^.*$|browser=\"$(yad --entry --center --text="Change browser")\"|" "$HOME"/bin/yad_news.sh

}
export -f change_browser

#-----------------------------------------------------------#
# Open url in browser
#-----------------------------------------------------------#

open_url() {

   if [[ "$choice" == '20' ]]; then
     :
   else
     echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | awk '{print $1}' | xargs -d '\n' "$browser"
   fi

}
export -f open_url

#-------------------------------------------------------#
# Speak text using text to speech
#-------------------------------------------------------#

text_to_speech() {

   if [[ "$choice" == '88' ]]; then
     :
   else
     echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | festival --tts
   fi

}
export -f text_to_speech

#-----------------------------------------------------------#
#  Download news feeds from BBC
#-----------------------------------------------------------#

update_feed() {

killall yad
curl --silent https://feeds.bbci.co.uk/news/rss.xml | tee "$HOME"/Documents/.news.html	
sleep 0.5
yad_news.sh	
	
}
export -f update_feed

#-------------------------------------------------------#
# Send url to clipboard
#-------------------------------------------------------#

text_clip() {

   if [[ "$choice" == '30' ]]; then
     :
   else
     echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | awk '{print $1}' | wl-copy -n
   fi

}
export -f text_clip

#-------------------------------------------------------#
# Create QR Code from url
#-------------------------------------------------------#

create_qrcode() {

   if [[ "$choice" == '80' ]]; then
     :
   else
     killall yad
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     echo "$choice" | xargs -d '\n' | sed 's/[0-9]\+$//' | \
     awk '{print $1}' | qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
     one=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs ls -tr | tail -n 1)
     export one
     notify-send --icon="$one" "QR Code" "Created"   
   fi
	
}
export -f create_qrcode

#-----------------------------------------------------------#
# Convert html file into text file
#-----------------------------------------------------------#

pandoc "$HOME"/Documents/.news.html -o "$HOME"/Documents/.updated_news.txt

#-----------------------------------------------------------#
# Removes second url link
#-----------------------------------------------------------#

awk '!/campaign=KARANGA/' "$HOME"/Documents/.updated_news.txt > "$HOME"/Documents/.tmpfile && mv "$HOME"/Documents/.tmpfile "$HOME"/Documents/.updated_news.txt

#-----------------------------------------------------------#
# Remove special characters
#-----------------------------------------------------------#

sed -i 's|<||g' "$HOME"/Documents/.updated_news.txt
sed -i 's|&|+|g' "$HOME"/Documents/.updated_news.txt
sed -i 's|\\||g' "$HOME"/Documents/.updated_news.txt

#-----------------------------------------------------------#
# Main
#-----------------------------------------------------------#

main() {
	
launch=$(cat "$HOME"/Documents/.updated_news.txt)
export launch
choice=$(echo "$launch" | _yad)
export choice

   if [[ "${choice:0-2}" == *"20"* ]]; then
     open_url
   elif [[ "${choice:0-2}" == *"35"* ]]; then
     change_browser
   elif [[ "${choice:0-2}" == *"44"* ]]; then
     update_feed
   elif [[ "${choice:0-2}" == *"88"* ]]; then
     text_to_speech
   elif [[ "${choice:0-2}" == *"30"* ]]; then
     text_clip
   elif [[ "${choice:0-2}" == *"80"* ]]; then
     create_qrcode
   elif [[ "${choice:0-2}" == *"90"* ]]; then
     help_info
   else
     :
   fi

}
export -f main
main

#-----------------------------------------------------------#
# Unset variables and functions
#-----------------------------------------------------------#

unset _yad
unset color01
unset color02
unset color2
unset color3
unset color5
unset color6
unset color7
unset color8
unset end
unset launch
unset choice
unset main
unset open_url
unset browser
unset update_feed
unset text_to_speech
unset text_clip
unset one
unset create_qrcode
unset help_info
unset info_text
