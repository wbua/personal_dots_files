#!/bin/bash

#-------------------------------------------------------------#
# Yad settings
#-------------------------------------------------------------#

choice=$(GTK_THEME="Adwaita-dark" yad --form --width=600 --height=200 --title="Music app" \
--borders="20" \
--separator="#" \
--field="Station ":CE "" \
--field="URL":CE "" \
--button="Exit":1

)
export choice

#-------------------------------------------------------------#
# Yad field variables
#-------------------------------------------------------------#

artist=$(echo "$choice" | awk -F '#' '{print $1}')
export artist
title=$(echo "$choice" | awk -F '#' '{print $2}')   
export title
 
#-------------------------------------------------------------#
# main
#-------------------------------------------------------------#

main() {

launch="${artist}${title}"
export launch

   if [[ -z "$launch" ]]; then
     :
   else
     echo "$artist" | tee "$HOME"/Documents/music_app/artist.txt	
     echo "$title" | tee "$HOME"/Documents/music_app/title.txt
   fi
	
}
export -f main
main

#-------------------------------------------------------------#
# unset variables and functions
#-------------------------------------------------------------#
   
unset choice
unset artist
unset title
unset main
unset launch
