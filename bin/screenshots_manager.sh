#!/bin/bash

# Program command: screenshots_manager.sh
# Description: Create and view screenshots. Also has other actions.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 18-01-2025
# Program_license: GPL 3.0
# Dependencies: yad, imagemagick, wl-clipboard

#-------------------------------------------------------#
# Exit with non zero code
#-------------------------------------------------------#

set -e

#-------------------------------------------------------#
# Master config file
#-------------------------------------------------------#

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-------------------------------------------------------#
# User preferences
#-------------------------------------------------------#

# File Manager
MY_FILE_MANAGER="$MAIN_FILE_MANAGER"
export MY_FILE_MANAGER

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Directory containing original images
SOURCE_DIR="$MAIN_SCREENSHOT_DIR"
export SOURCE_DIR

# Directory to save thumbnails
THUMB_DIR="$HOME/Pictures/.srnsht_thumbs"
export THUMB_DIR

# Image editor
MY_IMAGE_EDITOR="$MAIN_IMAGE_EDITOR"
export MY_IMAGE_EDITOR

# Screenshot selay
MY_DELAY="$SCREENSHOT_DELAY"
export MY_DELAY

#-------------------------------------------------------#
# Create thumbnail directory if it doesn't exist
#-------------------------------------------------------#

if [ ! -d "$THUMB_DIR" ]; then
    mkdir -p "$THUMB_DIR"
fi

if [ ! -d "$SOURCE_DIR" ]; then
    mkdir -p "$SOURCE_DIR"
fi

#-------------------------------------------------------#
# Loop through all images in source directory
#-------------------------------------------------------#

for img in "$SOURCE_DIR"/*.{jpg,jpeg,png}; do
  # Check if file exists (avoids errors if no matching files)
  [ -e "$img" ] || continue

  # Get the filename without extension
  filename=$(basename "$img")

  # Check if the thumbnail already exists
  if [ ! -e "$THUMB_DIR/$filename" ]; then
    # Convert image to thumbnail and save it to thumbnail directory
    convert "$img" -resize 320x "$THUMB_DIR/$filename"
  fi
done

#-------------------------------------------------------#
# Clear the output file if it exists
#-------------------------------------------------------#

truncate -s 0 ~/Pictures/.srnshot_recent_list.txt || exit 1
truncate -s 0 ~/Pictures/.scrnshot_filenames.txt || exit 1

#-------------------------------------------------------#
# Find files in source directory, sort them by modification time, and save paths to the output file
#-------------------------------------------------------#

find "$SOURCE_DIR" -type f -printf "%T@ %p\n" | sort -n -r | cut -d' ' -f2- | tee ~/Pictures/.srnshot_recent_list.txt > /dev/null

#-------------------------------------------------------#
# Read recent files and create full paths in the thumbnail directory
#-------------------------------------------------------#

while IFS= read -r line; do
    basename=$(basename "$line")
    echo "\"$THUMB_DIR/$basename\" \"$basename\""
done < ~/Pictures/.srnshot_recent_list.txt > ~/Pictures/.scrnshot_filenames.txt

#-------------------------------------------------------#
# Create array
#-------------------------------------------------------#

declare -a screen_list="($(< ~/Pictures/.scrnshot_filenames.txt))"

#-------------------------------------------------------#
# Enable or disable screenshot notification alert
#-------------------------------------------------------#

notify_alert() {

   if [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     setsid paplay ~/Music/sounds/cameraclick.wav >/dev/null 2>&1 & disown
   elif [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     :
   else
     :
   fi

}
export -f notify_alert

#-------------------------------------------------------#
# Take fullscreen screenshot
#-------------------------------------------------------#

take_screen() {

killall yad

sleep 1
grimshot save screen "$SOURCE_DIR"/"$(date +'%d-%m-%Y-%H%M%S')".png
temp_shot_choice=$(find "$SOURCE_DIR" -type f -printf "%T@ %p\n" | sort -n -r | \
cut -d' ' -f2- | head -n1 | xargs -0 basename)
rm -f ~/Pictures/.tempfullscrns/*.* || exit 1
convert "$SOURCE_DIR"/"$temp_shot_choice" -resize 512x512 ~/Pictures/.tempfullscrns/"$temp_shot_choice" || exit 1
notify_alert
	
}
export -f take_screen

#-------------------------------------------------------#
# Take select screenshot
#-------------------------------------------------------#

take_select() {

killall yad

sleep 1
grimshot save area "$SOURCE_DIR"/"$(date +'%d-%m-%Y-%H%M%S')".png
temp_shot_choice=$(find "$SOURCE_DIR" -type f -printf "%T@ %p\n" | sort -n -r | \
cut -d' ' -f2- | head -n1 | xargs -0 basename)
rm -f ~/Pictures/.tempfullscrns/*.* || exit 1
convert "$SOURCE_DIR"/"$temp_shot_choice" -resize 512x512 ~/Pictures/.tempfullscrns/"$temp_shot_choice" || exit 1
notify_alert

}
export -f take_select

#-------------------------------------------------------#
# Take delayed screenshot
#-------------------------------------------------------#

take_delay() {

killall yad
sleep "$MY_DELAY" && grimshot save screen "$SOURCE_DIR"/"$(date +'%d-%m-%Y-%H%M%S')".png
temp_shot_choice=$(find "$SOURCE_DIR" -type f -printf "%T@ %p\n" | sort -n -r | \
cut -d' ' -f2- | head -n1 | xargs -0 basename)
rm -f ~/Pictures/.tempfullscrns/*.* || exit 1
convert "$SOURCE_DIR"/"$temp_shot_choice" -resize 512x512 ~/Pictures/.tempfullscrns/"$temp_shot_choice" || exit 1
notify_alert

}
export -f take_delay

#-------------------------------------------------------#
# Edit screenshot
#-------------------------------------------------------#

edit_shot() {

killall yad

edit_choice=$(cat ~/Documents/.srnshotfiles_temp.txt)
    
full_path="$SOURCE_DIR/$edit_choice"

"$MY_IMAGE_EDITOR" "$full_path"
	
}
export -f edit_shot

#-------------------------------------------------------#
# Copy png file to clipboard
#-------------------------------------------------------#

copy_png() {

killall yad
    
copy_choice=$(cat ~/Documents/.srnshotfiles_temp.txt)
    
full_path="$SOURCE_DIR/$copy_choice"
    
wl-copy --type image/png < "$full_path"

}
export -f copy_png

#-------------------------------------------------------#
# Go to file
#-------------------------------------------------------#

go_to_file() {

killall yad

file_choice=$(cat ~/Documents/.srnshotfiles_temp.txt)

full_path="$SOURCE_DIR/$file_choice"

"$MY_FILE_MANAGER" "$full_path" 
	
}
export -f go_to_file

#-------------------------------------------------------#
# Open screenshot
#-------------------------------------------------------#

open_image() {

killall yad
    
open_choice=$(cat ~/Documents/.srnshotfiles_temp.txt)
    
full_path="$SOURCE_DIR/$open_choice"
        
xdg-open "$full_path"
	
}
export -f open_image

#-------------------------------------------------------#
# Delete screeshot
#-------------------------------------------------------#

delete_shot() {

delete_choice=$(cat ~/Documents/.srnshotfiles_temp.txt)

killall yad
rm "$SOURCE_DIR"/"$delete_choice" || exit 1
rm "$THUMB_DIR"/"$delete_choice" || exit 1
	
}
export -f delete_shot

#-------------------------------------------------------#
# Yad dialog
#-------------------------------------------------------#

printf '%s\n' "${screen_list[@]}" | GTK_THEME="$MY_GTK_THEME" yad --list \
 --title="Screenshot manager" \
 --width=950 \
 --height=675 \
 --borders=10 \
 --regex-search \
 --search-column=2 \
 --column="Image:IMG" \
 --column="File" \
 --dclick-action='/bin/bash -c "echo "$@" > "$HOME"/Documents/.srnshotfiles_temp.txt"' \
 --button="_Full:/bin/bash -c 'take_screen'" \
 --button="_Select:/bin/bash -c 'take_select'" \
 --button="_Delay:/bin/bash -c 'take_delay'" \
 --button="_Del:/bin/bash -c 'delete_shot'" \
 --button="_Edit:/bin/bash -c 'edit_shot'" \
 --button="_Open:/bin/bash -c 'open_image'" \
 --button="_Go to file:/bin/bash -c 'go_to_file'" \
 --button="_Copy:/bin/bash -c 'copy_png'" \
 --button="_Exit":1

#-------------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------------#

unset copy_png
unset go_to_file
unset open_image
unset MY_FILE_MANAGER
unset MY_GTK_THEME
unset take_select
unset take_screen
unset SOURCE_DIR
unset THUMB_DIR
unset edit_shot
unset MY_IMAGE_EDITOR
unset delete_shot
unset MY_DELAY
unset notify_alert
