#!/bin/bash

# Program command: sway_middleclick_menu.sh
# Description: Right click menu using mod and button2.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 18-01-2025
# Program_license: GPL 3.0
# Dependencies: swaymsg, jq, yad

# User settings
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#--------------------------------------------------------#
# Error checking
#--------------------------------------------------------#

set -e

#--------------------------------------------------------#
# User preferences
#--------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Terminal
MY_TERMINAL="$MAIN_TERMINAL"
export MY_TERMINAL

# Browser
MY_BROWSER="$MAIN_BROWSER"
export MY_BROWSER

# File manager
MY_FILE_MANAGER="$MAIN_FILE_MANAGER"
export MY_FILE_MANAGER

# Development
MY_DEV="$MAIN_DEV"
export MY_DEV

# Other terminal
OTHER_TERMINAL="$MAIN_OTHER_TERMINAL"
export OTHER_TERMINAL

# Search engine
SEARCH_ENGINE="$ACTIVE_APP_SEARCH_ENGINE"
export SEARCH_ENGINE

# Gtk theme
MY_SWAY_THEME="$SWAY_GTK_THEME"
export MY_SWAY_THEME

#--------------------------------------------------------#
# Open thunar location in the terminal
#--------------------------------------------------------#

open_thunar() {

killall yad
choice_home=$(swaymsg -t get_tree | \
jq -r '.. | select(.type?) | select(.focused==true).name' | \
sed 's| - Thunar||' | sed 's/^/"/;s/$/"/')
choice_root=$(swaymsg -t get_tree | \
jq -r '.. | select(.type?) | select(.focused==true).name' | \
sed 's| - Thunar||' | sed 's/^/"/;s/$/"/')

   if [[ "$choice_home" == '/home/' ]]; then
     "$OTHER_TERMINAL" -- /bin/bash -c "cd ~/$choice_home; $SHELL"
     "$OTHER_TERMINAL" -- /bin/bash -c "cd ~/$choice_home; $SHELL"
   elif [[ ! "$choice_root" == '/home/' ]]; then
     "$OTHER_TERMINAL" -- /bin/bash -c "cd $choice_root; $SHELL"      
     :
   else
     notify-send "Sway not detected!"
   fi

}
export -f open_thunar

#--------------------------------------------------------#
# Change theme
#--------------------------------------------------------#

change_theme() {

killall yad
setsid bash ~/bin/yad_theme_gtk.sh >/dev/null 2>&1 & disown & exit
	
}
export -f change_theme

#--------------------------------------------------------#
# Set wallpaper
#--------------------------------------------------------#

set_wallpaper() {

killall yad
setsid bash ~/bin/sway_wallpapers.sh >/dev/null 2>&1 & disown & exit
	
}
export -f set_wallpaper

#--------------------------------------------------------#
# Kill running app
#--------------------------------------------------------#

kill_running_app() {

killall yad
sleep 0.5
choice=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | \
tr -d '"' | tr '[:upper:]' '[:lower:]')
kill $(pidof "$choice") || kill $(pgrep -f "$choice")

}
export -f kill_running_app

#--------------------------------------------------------#
# Open file manager location in the terminal
#--------------------------------------------------------#

open_in_term() {

   if [[ "$MY_FILE_MANAGER" == "thunar" ]]; then
     open_thunar
   else
     notify-send "Not supported" "Only works in thunar"
   fi
	
}
export -f open_in_term

#--------------------------------------------------------#
# File manager app
#--------------------------------------------------------#

file_manager_app() {

GTK_THEME="$MY_GTK_THEME" yad --form \
 --text-align="center" \
 --text="\nFile manager\n" \
 --columns=1 \
 --width=500 \
 --height=500 \
 --borders=10 \
 --field="<b>Change theme</b>":fbtn '/bin/bash -c "change_theme"' \
 --field="<b>Set wallpaper</b>":fbtn '/bin/bash -c "set_wallpaper"' \
 --field="<b>Open in terminal</b>":fbtn '/bin/bash -c "open_in_term"' \
 --field="<b>Float toggle</b>":fbtn '/bin/bash -c "float_toggle"' \
 --field="<b>Kill app</b>":fbtn '/bin/bash -c "kill_running_app"' \
 --button="Exit":1 \
	
}
export -f file_manager_app

#--------------------------------------------------------#
# Browser app
#--------------------------------------------------------#

browser_app() {

browser_choice=$(GTK_THEME="$MY_GTK_THEME" yad --form \
 --text-align="center" \
 --text="\nBrowser\n" \
 --columns=1 \
 --width=500 \
 --height=500 \
 --borders=10 \
 --separator= \
 --align="center" \
 --field="<b>Change theme</b>":fbtn '/bin/bash -c "change_theme"' \
 --field="<b>Set wallpaper</b>":fbtn '/bin/bash -c "set_wallpaper"' \
 --field="<b>Open new tab</b>":fbtn '/bin/bash -c "new_browser_tab"' \
 --field="<b>Float toggle</b>":fbtn '/bin/bash -c "float_toggle"' \
 --field="<b>Kill app</b>":fbtn '/bin/bash -c "kill_running_app"' \
 --field="\nGoogle search text...":LBL "" \
 --field="":CE \
 --button="Exit":1)

   if [[ -z "$browser_choice" ]]; then
     :
   else
     google_search
   fi

}
export -f browser_app

#--------------------------------------------------------#
# Google search
#--------------------------------------------------------#

google_search() {

web_search="https://www.google.co.uk/search?q={}"
echo "$browser_choice" | xargs -I{} "$MY_BROWSER" "$web_search"
      
}
export -f google_search

#--------------------------------------------------------#
# Open new tabs in browser
#--------------------------------------------------------#

new_browser_tab() {

   killall yad
   if [[ "$(grep MAIN_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "brave" ]]; then
     killall yad
     brave --app-url "$SEARCH_ENGINE"
   elif [[ "$(grep MAIN_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "firefox" ]]; then
     killall yad
     firefox --new-tab "$SEARCH_ENGINE"
   elif [[ "$(grep MAIN_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "google-chrome" || "$(grep MAIN_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == *"${MY_BROWSER^}"* ]]; then
     killall yad
     google-chrome --app-url "$SEARCH_ENGINE"   
   else
     notify-send "Browser not supported!"
   fi
     
}
export -f new_browser_tab

#--------------------------------------------------------#
# Float toggle
#--------------------------------------------------------#

float_toggle() {

killall yad
sleep 0.5
setsid swaymsg floating toggle >/dev/null 2>&1 & disown

}
export -f float_toggle

#--------------------------------------------------------#
# Generic menu
#--------------------------------------------------------#

generic_menu() {

GTK_THEME="$MY_GTK_THEME" yad --form \
 --text-align="center" \
 --text="\nMenu\n" \
 --columns=1 \
 --width=500 \
 --height=500 \
 --borders=10 \
 --field="<b>Change theme</b>":fbtn '/bin/bash -c "change_theme"' \
 --field="<b>Set wallpaper</b>":fbtn '/bin/bash -c "set_wallpaper"' \
 --field="<b>Float toggle</b>":fbtn '/bin/bash -c "float_toggle"' \
 --field="<b>Kill app</b>":fbtn '/bin/bash -c "kill_running_app"' \
 --button="Exit":1 \
	
}
export -f generic_menu

#--------------------------------------------------------#
# Checks the focused window to see what app is running 
#--------------------------------------------------------#

check_running_app() {

active_choice=$(swaymsg -t get_tree | \
jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | \
tr -d '"' | tr '[:upper:]' '[:lower:]')

   if [[ "$active_choice" == "$MY_BROWSER" ]]; then
     browser_app
   elif [[ "$active_choice" == "$MY_FILE_MANAGER" ]]; then
     file_manager_app
   else
     generic_menu
   fi
	
}
export -f check_running_app

#--------------------------------------------------------#
# Open terminal
#--------------------------------------------------------#

open_terminal() {

killall yad
/bin/bash -c "$MY_TERMINAL"
	
}
export -f open_terminal

#--------------------------------------------------------#
# Open browser
#--------------------------------------------------------#

open_browser() {

killall yad
/bin/bash -c "$MY_BROWSER"
	
}
export -f open_browser

#--------------------------------------------------------#
# Open file manager
#--------------------------------------------------------#

open_filemanager() {

killall yad
GTK_THEME="$MY_SWAY_THEME"
/bin/bash -c "$MY_FILE_MANAGER"

}
export -f open_filemanager

#--------------------------------------------------------#
# Menu options for empty workspace
#--------------------------------------------------------#

empty_workspace() {

empty_wp_choice=$(GTK_THEME="$MY_GTK_THEME" yad --form \
 --text-align="center" \
 --text="\nEmpty Workspace\n" \
 --columns=1 \
 --width=500 \
 --height=500 \
 --borders=10 \
 --align="center" \
 --separator= \
 --field="<b>Change theme</b>":fbtn '/bin/bash -c "change_theme"' \
 --field="<b>Float toggle</b>":fbtn '/bin/bash -c "float_toggle"' \
 --field="<b>Set wallpaper</b>":fbtn '/bin/bash -c "set_wallpaper"' \
 --field="<b>Launch terminal</b>":fbtn '/bin/bash -c "open_terminal"' \
 --field="<b>Launch file manager</b>":fbtn '/bin/bash -c "open_filemanager"' \
 --field="<b>Launch browser</b>":fbtn '/bin/bash -c "open_browser"' \
 --field="\nEnter app name to execute...":LBL "" \
 --field="":CE \
 --button="Exit":1)

   if [[ -z "$empty_wp_choice" ]]; then
     :
   else
     /bin/bash -c "$empty_wp_choice" 
   fi
	
}
export -f empty_workspace

#--------------------------------------------------------#
# Checks for active app and empty workspace
#--------------------------------------------------------#

active_workspace=$(swaymsg -t "get_workspaces" | jq -r '.[] | select(.focused)' | jq -r '.representation')
export active_workspace

   if [ "$active_workspace" = "null" ] || [ "$active_workspace" = "H[]" ] || [ "$active_workspace" = "V[]" ] ; then
     empty_workspace
   else
     check_running_app
   fi

#--------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------#

unset check_running_app
unset active_workspace
unset empty_workspace
unset MY_GTK_THEME
unset MY_TERMINAL
unset MY_DEV
unset MY_FILE_MANAGER
unset MY_BROWSER
unset file_manager_app
unset open_in_term
unset OTHER_TERMINAL
unset new_browser_tab
unset browser_app
unset google_search
unset open_thunar
unset open_terminal
unset open_browser
unset generic_menu
unset open_filemanager
unset change_theme
unset MY_SWAY_THEME
unset set_wallpaper
unset float_toggle
