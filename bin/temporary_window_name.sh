#!/bin/bash


awchoice=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | tr -d '"')

sed -i "s|.*\(MAIN_TEMP_WIN=\).*|MAIN_TEMP_WIN=\"$(printf '%s' "$awchoice")\"|" ~/bin/sway_user_preferences.sh || exit 1
