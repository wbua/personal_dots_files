#!/bin/bash

# Program command: swaybar_themes.sh
# Description: Changes themes for swaybar.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 18-08-2024
# Program_license: GPL 3.0
# Dependencies: yad, Material Icons

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#--------------------------------------------#
# User preferences
#--------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#--------------------------------------------#
# Create files and directories
#--------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -f "$HOME"/Documents/.swaybar_theme.txt ]; then
     :
   else
     touch "$HOME"/Documents/.swaybar_theme.txt
   fi

#--------------------------------------------#
# Create default theme 
#--------------------------------------------#

   if [[ -z "$(cat ~/Documents/.swaybar_theme.txt)" ]]; then
     echo "Gnome transparent" | tee ~/Documents/.swaybar_theme.txt 
   else
     :
   fi

#--------------------------------------------#
# catppuccin
#--------------------------------------------#

catppuccin() {

echo "catppuccin" | tee "$HOME"/Documents/.swaybar_theme.txt

pkill -f swaybar

# i3blocks --------------------------------------
# Battery
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/battery2
# Brightness
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/brightness
# Date
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/datem
# Keys
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/keys
# Microphone
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/microphone
# Music players
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_players
# My volume
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_volume
# Notifications
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/notifications
# Weather
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/swaywm_weather
# Wifi
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/wifi
# Power
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/menu_power
# Monitor
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/auto_monitor_off
# Extras
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/extras
# Time
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/timem
# Nightlight
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/night_light

# Swaybar --------------------------------------
sed -i "3s|^.*$|include ~/.config/sway/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/sway/sway_bar_themes

killall yad
bash ~/bin/sway_load_settings.sh
	
}
export -f catppuccin

#--------------------------------------------#
# Theme2
#--------------------------------------------#

theme2() {

echo "theme2" | tee "$HOME"/Documents/.swaybar_theme.txt

pkill -f swaybar

# Battery
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/battery2
# Brightness
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/brightness
# Date
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/datem
# Keys
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/keys
# Microphone
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/microphone
# Music players
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_players
# My volume
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_volume
# Notifications
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/notifications
# Weather
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/swaywm_weather
# Wifi
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/wifi
# Power
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/menu_power
# Monitor
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/auto_monitor_off
# Extras
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/extras
# Time
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/timem
# Nightlight
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/night_light


# Swaybar --------------------------------------
sed -i "3s|^.*$|include ~/.config/sway/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/sway/sway_bar_themes

killall yad
bash ~/bin/sway_load_settings.sh
	
}
export -f theme2

#--------------------------------------------#
# Dracula
#--------------------------------------------#

dracula() {

echo "dracula" | tee "$HOME"/Documents/.swaybar_theme.txt

pkill -f swaybar

# Battery
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/battery2
# Brightness
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/brightness
# Date
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/datem
# Keys
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/keys
# Microphone
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/microphone
# Music players
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_players
# My volume
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_volume
# Notifications
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/notifications
# Weather
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/swaywm_weather
# Wifi
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/wifi
# Power
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/menu_power
# Monitor
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/auto_monitor_off
# Extras
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/extras
# Time
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/timem
# Nightlight
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/night_light

# Swaybar --------------------------------------
sed -i "3s|^.*$|include ~/.config/sway/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/sway/sway_bar_themes

killall yad
bash ~/bin/sway_load_settings.sh

}
export -f dracula

#--------------------------------------------#
# Cyberpunk
#--------------------------------------------#

cyberpunk() {

echo "cyberpunk" | tee "$HOME"/Documents/.swaybar_theme.txt

pkill -f swaybar

# Battery
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/battery2
# Brightness
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/brightness
# Date
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/datem
# Keys
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/keys
# Microphone
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/microphone
# Music players
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_players
# My volume
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_volume
# Notifications
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/notifications
# Weather
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/swaywm_weather
# Wifi
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/wifi
# Power
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/menu_power
# Monitor
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/auto_monitor_off
# Extras
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/extras
# Time
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/timem
# Nightlight
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/night_light


# Swaybar --------------------------------------
sed -i "3s|^.*$|include ~/.config/sway/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/sway/sway_bar_themes

killall yad
bash ~/bin/sway_load_settings.sh
	
}
export -f cyberpunk

#--------------------------------------------#
# Nord
#--------------------------------------------#

nord() {

echo "nord" | tee "$HOME"/Documents/.swaybar_theme.txt

pkill -f swaybar

# i3blocks --------------------------------------
# Battery
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/battery2
# Brightness
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/brightness
# Date
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/datem
# Keys
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/keys
# Microphone
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/microphone
# Music players
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_players
# My volume
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_volume
# Notifications
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/notifications
# Weather
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/swaywm_weather
# Wifi
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/wifi
# Power
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/menu_power
# Monitor
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/auto_monitor_off
# Extras
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/extras
# Time
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/timem
# Nightlight
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/night_light


# Swaybar --------------------------------------
sed -i "3s|^.*$|include ~/.config/sway/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/sway/sway_bar_themes

killall yad
bash ~/bin/sway_load_settings.sh
	
}
export -f nord

#--------------------------------------------#
# Sonokai
#--------------------------------------------#

sonokai() {

echo "sonokai" | tee "$HOME"/Documents/.swaybar_theme.txt

pkill -f swaybar

# i3blocks --------------------------------------
# Battery
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/battery2
# Brightness
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/brightness
# Date
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/datem
# Keys
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/keys
# Microphone
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/microphone
# Music players
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_players
# My volume
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_volume
# Notifications
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/notifications
# Weather
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/swaywm_weather
# Wifi
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/wifi
# Power
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/menu_power
# Monitor
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/auto_monitor_off
# Extras
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/extras
# Time
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/timem
# Nightlight
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/night_light


# Swaybar --------------------------------------
sed -i "3s|^.*$|include ~/.config/sway/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/sway/sway_bar_themes

killall yad
bash ~/bin/sway_load_settings.sh
	
}
export -f sonokai

#--------------------------------------------#
# Gruvbox
#--------------------------------------------#

gruvbox() {

echo "gruvbox" | tee "$HOME"/Documents/.swaybar_theme.txt

pkill -f swaybar

# i3blocks --------------------------------------
# Battery
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/battery2
# Brightness
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/brightness
# Date
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/datem
# Keys
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/keys
# Microphone
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/microphone
# Music players
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_players
# My volume
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_volume
# Notifications
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/notifications
# Weather
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/swaywm_weather
# Wifi
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/wifi
# Power
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/menu_power
# Monitor
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/auto_monitor_off
# Extras
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/extras
# Time
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/timem
# Nightlight
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/night_light


# Swaybar --------------------------------------
sed -i "3s|^.*$|include ~/.config/sway/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/sway/sway_bar_themes

killall yad
bash ~/bin/sway_load_settings.sh
	
}
export -f gruvbox

#--------------------------------------------#
# Everforest
#--------------------------------------------#

everforest() {

echo "everforest" | tee "$HOME"/Documents/.swaybar_theme.txt

pkill -f swaybar

# Battery
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/battery2
# Brightness
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/brightness
# Date
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/datem
# Keys
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/keys
# Microphone
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/microphone
# Music players
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_players
# My volume
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_volume
# Notifications
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/notifications
# Weather
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/swaywm_weather
# Wifi
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/wifi
# Power
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/menu_power
# Monitor
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/auto_monitor_off
# Extras
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/extras
# Time
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/timem
# Nightlight
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/night_light

# Swaybar --------------------------------------
sed -i "3s|^.*$|include ~/.config/sway/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/sway/sway_bar_themes

killall yad
bash ~/bin/sway_load_settings.sh
	
}
export -f everforest

#--------------------------------------------#
# Tokyo_night
#--------------------------------------------#

tokyo_night() {

echo "tokyo_night" | tee "$HOME"/Documents/.swaybar_theme.txt

pkill -f swaybar

# i3blocks --------------------------------------
# Battery
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/battery2
# Brightness
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/brightness
# Date
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/datem
# Keys
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/keys
# Microphone
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/microphone
# Music players
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_players
# My volume
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_volume
# Notifications
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/notifications
# Weather
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/swaywm_weather
# Wifi
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/wifi
# Power
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/menu_power
# Monitor
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/auto_monitor_off
# Extras
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/extras
# Time
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/timem
# Nightlight
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/night_light

# Swaybar --------------------------------------
sed -i "3s|^.*$|include ~/.config/sway/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/sway/sway_bar_themes

killall yad
bash ~/bin/sway_load_settings.sh
	
}
export -f tokyo_night

#--------------------------------------------#
# Gnome theme
#--------------------------------------------#

gnome-theme() {

echo "gnome_theme" | tee "$HOME"/Documents/.swaybar_theme.txt

pkill -f swaybar

# i3blocks --------------------------------------
# Battery
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/battery2
# Brightness
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/brightness
# Date
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/datem
# Keys
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/keys
# Microphone
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/microphone
# Music players
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_players
# My volume
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_volume
# Notifications
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/notifications
# Weather
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/swaywm_weather
# Wifi
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/wifi
# Power
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/menu_power
# Monitor
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/auto_monitor_off
# Extras
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/extras
# Time
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/timem
# Nightlight
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/night_light

# Swaybar --------------------------------------
sed -i "3s|^.*$|include ~/.config/sway/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/sway/sway_bar_themes

killall yad
bash ~/bin/sway_load_settings.sh
	
}
export -f gnome-theme

#--------------------------------------------#
# Gnome theme 2
#--------------------------------------------#

gnome-theme2() {

echo "gnome_theme2" | tee "$HOME"/Documents/.swaybar_theme.txt

pkill -f swaybar
#gnome_theme2
# i3blocks --------------------------------------
# Battery
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/battery2
# Brightness
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/brightness
# Date
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/datem
# Keys
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/keys
# Microphone
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/microphone
# Music players
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/my_players
# My volume
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/my_volume
# Notifications
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/notifications
# Weather
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/swaywm_weather
# Wifi
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/wifi
# Power
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/menu_power
# Monitor
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/auto_monitor_off
# Extras
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/extras
# Time
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/timem
# Nightlight
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_theme2/night_light

# Swaybar --------------------------------------
sed -i "3s|^.*$|include ~/.config/sway/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/sway/sway_bar_themes

killall yad
bash ~/bin/sway_load_settings.sh
	
}
export -f gnome-theme2


#--------------------------------------------#
# Gnome transparent
#--------------------------------------------#

gnome_transparent() {

echo "gnome_transparent" | tee "$HOME"/Documents/.swaybar_theme.txt

pkill -f swaybar

# i3blocks --------------------------------------
# Battery
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/battery2
# Brightness
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/brightness
# Date
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/datem
# Keys
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/keys
# Microphone
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/microphone
# Music players
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/my_players
# My volume
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/my_volume
# Notifications
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/notifications
# Weather
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/swaywm_weather
# Wifi
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/wifi
# Power
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/menu_power
# Monitor
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/auto_monitor_off
# Extras
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/extras
# Time
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/timem
# Workspaces
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/workspaces
# Nightlight
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent/night_light

# Swaybar --------------------------------------
sed -i "3s|^.*$|include ~/.config/sway/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/sway/sway_bar_themes

killall yad
bash ~/bin/sway_load_settings.sh
	
}
export -f gnome_transparent

#--------------------------------------------#
# Gnome transparent dark
#--------------------------------------------#

gnome_transparent_dark() {

echo "gnome_transparent_dark" | tee "$HOME"/Documents/.swaybar_theme.txt

pkill -f swaybar

# i3blocks --------------------------------------
# Battery
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/battery2
# Brightness
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/brightness
# Date
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/datem
# Keys
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/keys
# Microphone
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/microphone
# Music players
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/my_players
# My volume
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/my_volume
# Notifications
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/notifications
# Weather
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/swaywm_weather
# Wifi
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/wifi
# Power
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/menu_power
# Monitor
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/auto_monitor_off
# Extras
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/extras
# Time
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/timem
# Workspaces
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/workspaces
# Nightlight
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/gnome_transparent_dark/night_light

# Swaybar --------------------------------------
sed -i "3s|^.*$|include ~/.config/sway/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/sway/sway_bar_themes

killall yad
bash ~/bin/sway_load_settings.sh
	
}
export -f gnome_transparent_dark


#--------------------------------------------#
# Monokai
#--------------------------------------------#

monokai() {

echo "monokai" | tee "$HOME"/Documents/.swaybar_theme.txt

pkill -f swaybar

# i3blocks --------------------------------------
# Battery
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/battery2
# Brightness
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/brightness
# Date
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/datem
# Keys
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/keys
# Microphone
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/microphone
# Music players
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_players
# My volume
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/my_volume
# Notifications
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/notifications
# Weather
sed -i "11s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/swaywm_weather
# Wifi
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/wifi
# Power
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/menu_power
# Monitor
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/auto_monitor_off
# Extras
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/extras
# Time
sed -i "3s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/timem
# Nightlight
sed -i "12s|^.*$|source ~/.config/i3blocks/scripts/themes/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/i3blocks/scripts/night_light

# Swaybar --------------------------------------
sed -i "3s|^.*$|include ~/.config/sway/$(cat "$HOME"/Documents/.swaybar_theme.txt)|" ~/.config/sway/sway_bar_themes

killall yad
bash ~/bin/sway_load_settings.sh
	
}
export -f monokai

#--------------------------------------------#
# Yad settings
#--------------------------------------------#

_yad() {

GTK_THEME="$MY_GTK_THEME" yad --form --column="search" --width=800 --height=550 --columns=3 \
--borders=20 \
--text="<span font_family='JetBrainsMono Nerd Font' font='18'>Swaybar theme changer
</span>
Launch swaybar themes with <span color='#0CFF00' font_family='JetBrainsMono Nerd Font' font='14'>win+shift+t</span>

Current theme: $(cat "$HOME"/Documents/.swaybar_theme.txt)
" \
--text-align="center" \
--field="<b>Catppuccin</b>":fbtn '/bin/bash -c "catppuccin"' \
--field="<b>Gnome theme</b>":fbtn '/bin/bash -c "gnome-theme"' \
--field="<b>Gnome theme 2</b>":fbtn '/bin/bash -c "gnome-theme2"' \
--field="<b>Dracula</b>":fbtn '/bin/bash -c "dracula"' \
--field="<b>Cyberpunk</b>":fbtn '/bin/bash -c "cyberpunk"' \
--field="<b>Gnome dark</b>":fbtn '/bin/bash -c "gnome_transparent_dark"' \
--field="<b>Gnome transparent</b>":fbtn '/bin/bash -c "gnome_transparent"' \
--field="<b>Nord</b>":fbtn '/bin/bash -c "nord"' \
--field="<b>Sonokai</b>":fbtn '/bin/bash -c "sonokai"' \
--field="<b>Gruvbox</b>":fbtn '/bin/bash -c "gruvbox"' \
--field="<b>Everforest</b>":fbtn '/bin/bash -c "everforest"' \
--field="<b>Tokyo night</b>":fbtn '/bin/bash -c "tokyo_night"' \
--field="<b>Monokai</b>":fbtn '/bin/bash -c "monokai"' \
--field="<b>Theme2</b>":fbtn '/bin/bash -c "theme2"' \
--button="<span color='#FF0031' font_family='Material Icons' font='20'></span>":1

}
_yad
export -f _yad

#--------------------------------------------#
# Unset variables and functions
#--------------------------------------------#

unset _yad
unset catppuccin
unset theme2
unset dracula
unset cyberpunk
unset nord
unset sonokai
unset gruvbox
unset everforest
unset gnome-theme
unset gnome-theme2
unset tokyo_night
unset gnome_transparent
unset monokai
unset gnome_transparent_dark
unset MY_GTK_THEME
