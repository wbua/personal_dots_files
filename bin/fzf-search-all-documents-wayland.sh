#!/bin/bash

# Program command: fzf-search-all-documents-wayland.sh
# Description: Searches all files in your home directory.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 06-12-2023
# Program_license: GPL 3.0
# Dependencies: fzf, libreoffice, poppler-utils

choice=$(find ~ -type f -iname '*' | fzf --preview="sleep 0.2 ; less {}" \
--cycle \
--border="none" \
--header="
Press F1 for main menu

" \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'search for documents> ' \
--bind='F1:execute(fzf-launcher-wayland.sh {})' \
--info=inline \
--reverse) 
[ -z "$choice" ] && exit 0

setsid xdg-open "$choice" >/dev/null 2>&1 & disown
fzf-launcher-wayland.sh
