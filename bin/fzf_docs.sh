#!/bin/bash

dirs=( ~/Documents/ ~/bin/)
micro $(find "${dirs[@]}" -type f -iname '*.*' | fzf --cycle --preview 'batcat --color=always {}' --bind shift-up:preview-page-up,shift-down:preview-page-down)
	
