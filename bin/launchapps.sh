#!/bin/bash

# Program command: launchapps.sh 
# Description: View apps in rofi and run them.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 26-04-2023
# Program_license: GPL 3.0
# Dependencies: rofi

#------------------------------------------------------#
# Spaces for menu items
#------------------------------------------------------#

space=$(printf '%0.1s' " "{1..1})

#------------------------------------------------------#
# Color for menu items
#------------------------------------------------------#

# Descriptions
color1="<span color='#AFADB0'>"
# Color end
end="</span>"

#------------------------------------------------------#
# Menu
#------------------------------------------------------#

menu() {
	
echo "geany${color1}${space}text editor${end}"
echo "brave${color1}${space}browser${end}"
echo "firefox${color1}${space}browser${end}"
echo "onlyoffice-desktopeditors${color1}${space}${end}"
echo "qpdfview${color1}${space}pdf reader${end}"
echo "audacity${color1}${space}sound editor${end}"
echo "gnome-terminal${color1}${space}command line${end}"
echo "smplayer${color1}${space}video player${end}"
echo "gimp${color1}${space}image editor${end}"
echo "mousepad${color1}${space}text editor${end}"
echo "opera${color1}${space}browser${end}"
echo "gcolor3${color1}${space}color picker${end}"
echo "simple-scan${color1}${space}scan documents${end}"
echo "obsidian${color1}${space}markdown text editor${end}"
echo "lxappearance${color1}${space}customise look${end}"
echo "webcamoid${color1}${space}webcam${end}"
echo "gnome-calendar${color1}${space}${end}"
echo "nitrogen${color1}${space}wallpaper setter${end}"
echo "snap-store${color1}${space}software${end}"
echo "file-roller${color1}${space}zipping${end}"
echo "font-manager${color1}${space}${end}"
echo "gthumb${color1}${space}image viewer${end}"
echo "zoom${color1}${space}video conferencing${end}"
echo "nautilus${color1}${space}file manager${end}"
echo "pavucontrol${color1}${space}sound mixer${end}"
echo "xfce4-terminal${color1}${space}command line${end}"
echo "htop${color1}${space}system monitor${end}"
echo "thunar${color1}${space}file manager${end}"
echo "google-chrome${color1}${space}browser${end}"
	
}

#------------------------------------------------------#
# Run program
#------------------------------------------------------#

choice=$(menu | sort | rofi -dmenu -i -p '' -markup-rows -me-select-entry '' -me-accept-entry 'MousePrimary' \
-theme-str 'num-filtered-rows {vertical-align: 0.1;}' \
-theme-str 'configuration {hover-select: true; me-accept-entry: "MousePrimary"; me-select-entry: "";}' \
-theme-str 'element.selected.normal {background-color: #000000; padding: 0px 0px 0px 0px; border: 0 0 0 0; text-color: #0FFF00;}' \
-theme-str '* {foreground: white;}' \
-theme-str 'entry {vertical-align: 0.5; placeholder: "Search apps"; padding: 2px 0px 0px 0px; placeholder-color: grey;}' \
-theme-str 'listview {lines: 7; columns: 4;}' | awk -F "${color1}" '{print $1}')
[ -z "$choice" ] && exit 0

  if [[ "$choice" == 'htop' ]]; then
     gnome-terminal -e htop
  else
    sleep 0.1 ; echo "$choice" | xargs /bin/bash -c
  fi
