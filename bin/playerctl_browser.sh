#!/bin/bash

# Program command: playerctl_browser.sh
# Description: Controls video in browser, using playerctl.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 29-08-2023
# Program_license: GPL 3.0
# Dependencies: 
# playerctl, rofi, yad

#---------------------------------------------------------#
# Default browser
#---------------------------------------------------------#

browser="brave"

#---------------------------------------------------------#
# Check browser instance number
#---------------------------------------------------------#

instance_check=$(playerctl -l | grep "$browser" | sed "s/${browser}.instance//")

#---------------------------------------------------------#
# Bash scripts executable path location
#---------------------------------------------------------#

bash_dir="$HOME/bin/"

#---------------------------------------------------------#
# Run launcher
#---------------------------------------------------------#

_rofi() {

  #rofi -dmenu -i -p '' -theme-str 'listview {lines: 5; columns: 3;}'
  wofi --dmenu -i --columns=2 -p "Control brave..."
  	
}

#---------------------------------------------------------#
# Browser class
#---------------------------------------------------------#

browser_class() {

  notify-send "$(playerctl -l | grep instance)"
	
}

#---------------------------------------------------------#
# Current browser
#---------------------------------------------------------#

current_browser() {
	
  notify-send "$(grep -o "^browser=.*$" playerctl_browser.sh)"

}

#---------------------------------------------------------#
# Change browser position 1 minute plus
#---------------------------------------------------------#

browser_position_plus() {

  for counter in {1..12}; do playerctl --player="$browser".instance"$instance_check" position 10+; done

}

#---------------------------------------------------------#
# Change browser position 1 minute minus
#---------------------------------------------------------#

browser_position_minus() {

  for counter in {1..12}; do playerctl --player="$browser".instance"$instance_check" position 10-; done	
   
}

#---------------------------------------------------------#
# Change browser
#---------------------------------------------------------#

change_browser() {
	
  sed -i "16s|^.*$|browser=\"$(yad --entry --center --text="browser")\"|" "$bash_dir"/playerctl_browser.sh
   
}

#---------------------------------------------------------#
# Menu
#---------------------------------------------------------#

choice=$(echo -e "play\npause\nstatus\ntitle\nchange browser\nbrowser class\ncurrent browser\nclipboard\n1 minute plus\n1 minute minus\nnext\nprevious" | _rofi)
[[ -z "$choice" ]] && exit 0

#---------------------------------------------------------#
# Main
#---------------------------------------------------------#

case "$choice" in

 'play')
  playerctl --player="$browser".instance"$instance_check" play
  ;;
 'pause')
  playerctl --player="$browser".instance"$instance_check" pause
  ;;
 'status')
  notify-send "$(playerctl --player="$browser".instance"$instance_check" metadata -f "{{status}}")"
  ;;
 'title')
  notify-send "$(playerctl --player="$browser".instance"$instance_check" metadata -f "{{xesam:title}}")"
  ;;
 'change browser')
  change_browser
  ;;
 'browser class')
  browser_class
  ;;
 'current browser')
  current_browser
  ;;
 'clipboard')
  playerctl --player="$browser".instance"$instance_check" metadata -f "{{xesam:title}}" | xclip -selection clipboard
  ;;
 '1 minute plus')
  browser_position_plus
  ;;
 '1 minute minus')
  browser_position_minus
  ;;
  'next')
  playerctl --player brave next
  ;;
  'previous')
  playerctl --player brave previous
  ;;
  *)
  echo "This is not a recognized service" | _rofi
  ;;
  
esac
