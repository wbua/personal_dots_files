#!/bin/bash

# Program command: yad_mic_status.sh
# Description: Checks and sets microphone. Used with polybar.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 14-07-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad, pactl, material icons, custom gtk.css

#--------------------------------------------------------------------#
# Position of dialog on screen
#--------------------------------------------------------------------#

# Yad box y position
box_y_position="60"
export box_y_position
# Yad box x position
box_x_position="1200"
export box_x_position

#--------------------------------------------------------------------#
# Colors for menu items (pango markup)
#--------------------------------------------------------------------#

# Off
color2="<span color='#FF002E' font_family='Inter' font='18' weight='bold' rise='0pt'>"
export color2
# On
color3="<span color='#22FF00' font_family='Inter' font='18' weight='bold' rise='0pt'>"
export color3
end="</span>"
export end

#--------------------------------------------------------------------#
# Check microphone is on or off
#--------------------------------------------------------------------#

check_mic() {
	
   if [[ "$(pactl get-source-mute @DEFAULT_SOURCE@ | awk -F ':' '{print $2}' | sed 's/ //')" == 'no' ]]; then
     echo "${color3}on${end}"
   else
     echo "${color2}off${end}"
   fi
	
}
export -f check_mic

#--------------------------------------------------------------------#
# Change microphone to on or off
#--------------------------------------------------------------------#

change_mic() {

   if [[ "$(pactl get-source-mute @DEFAULT_SOURCE@ | awk -F ':' '{print $2}' | sed 's/ //')" == 'no' ]]; then
     killall yad
     pactl set-source-mute @DEFAULT_SOURCE@ 1
     yad_mic_status.sh
   else
     killall yad
     pactl set-source-mute @DEFAULT_SOURCE@ 0
     yad_mic_status.sh
   fi
     
}
export -f change_mic

#--------------------------------------------------------------------#
# Yad settings
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog" yad --form --text-info --width=300 --height=150 --image-on-top --borders=10 --buttons-layout="end" --columns=3 \
--posx="$box_x_position" --posy="$box_y_position" \
--buttons-layout="center" \
--title="Microphone" \
--text="Microphone" \
--text-align="center" \
--align="center" \
--field="":LBL "" \
--field="<b><span color='#ffffff' font_family='Inter' font='14'>$(check_mic)</span></b>":fbtn '/bin/bash -c "change_mic"' \
--field="":LBL "" \
--button="<span color='#ffffff' font_family='Material Icons' font='14'></span>":1 & 

#--------------------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------------------#

unset check_mic
unset change_mic
unset box_y_position
unset box_x_position
unset color2
unset color3
unset end
