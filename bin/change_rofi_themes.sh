#!/bin/bash

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#------------------------------------------------------#
# User preferences
#------------------------------------------------------#

# Rofi theme
MY_ROFI_THEME="$ROFI_THEME_CONFIG"

# Background color
MY_COLOR_BG="$COLOR_BG"

# Color icons
MY_COLOR_ICON="$COLOR_ICON"

#------------------------------------------------------#
# Run launchers
#------------------------------------------------------#

run_launchers() {

my_message=$(echo "$MY_ROFI_THEME" | xargs -0 basename)

   if [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == 'rofi' ]]; then
     rofi -dmenu -i -mesg "$my_message" -config "$ROFI_THEME_CONFIG"
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == 'wofi' ]]; then
     wofi --dmenu -i
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "bemenu" ]]; then
     bemenu -i -l 8 -H 40 --fn "Monospace 18" --fb "$MY_COLOR_BG" --ff "#cdd6f4" --nb "$MY_COLOR_BG" --nf "#cdd6f4" --tb "$MY_COLOR_BG" \
     --hb "$MY_COLOR_BG" --tf "$MY_COLOR_ICON" --hf "$MY_COLOR_ICON" --af "#FFFFFF" --ab "$MY_COLOR_BG"
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "yad" ]]; then
     GTK_THEME="$MY_GTK_THEME" yad --list --column="list" --search-column=1 --regex-search --no-headers \
     --borders=10 --width=800 --height=600 --button="Exit":1 
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == 'tofi' ]]; then
     tofi
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "fuzzel" ]]; then
     fuzzel -d
   else
     :
   fi
   
}

#------------------------------------------------------#
# Menu
#------------------------------------------------------#

menu() {

echo "1. KooL_style-8"
echo "2. KooL_style-9"
echo "3. KooL_style-5"
echo "4. KooL_style-3-Fullscreen-v2"
echo "5. adi1090x_style-1"
echo "6. windows11-list-dark"

}

#------------------------------------------------------#
# Main
#------------------------------------------------------#

my_choice=$(menu | run_launchers | awk -F '.' '{print $1}')
[[ -z "$my_choice" ]] && exit

case "$my_choice" in

 '1')
   sed -i "s|.*\(ROFI_THEME_CONFIG=\).*|ROFI_THEME_CONFIG=\"$(printf '%s' "$HOME/.config/rofi/KooL_style-8.rasi")\"|" ~/bin/sway_user_preferences.sh || exit 1
  ;;

 '2')
   sed -i "s|.*\(ROFI_THEME_CONFIG=\).*|ROFI_THEME_CONFIG=\"$(printf '%s' "$HOME/.config/rofi/KooL_style-9.rasi")\"|" ~/bin/sway_user_preferences.sh || exit 1
  ;;

 '3')
   sed -i "s|.*\(ROFI_THEME_CONFIG=\).*|ROFI_THEME_CONFIG=\"$(printf '%s' "$HOME/.config/rofi/KooL_style-5.rasi")\"|" ~/bin/sway_user_preferences.sh || exit 1
  ;;

 '4')
   sed -i "s|.*\(ROFI_THEME_CONFIG=\).*|ROFI_THEME_CONFIG=\"$(printf '%s' "$HOME/.config/rofi/KooL_style-3-Fullscreen-v2.rasi")\"|" ~/bin/sway_user_preferences.sh || exit 1
  ;;

 '5')
   sed -i "s|.*\(ROFI_THEME_CONFIG=\).*|ROFI_THEME_CONFIG=\"$(printf '%s' "$HOME/.config/rofi/adi1090x_style-1.rasi")\"|" ~/bin/sway_user_preferences.sh || exit 1
  ;;

 '6')
   sed -i "s|.*\(ROFI_THEME_CONFIG=\).*|ROFI_THEME_CONFIG=\"$(printf '%s' "$HOME/.config/rofi/windows11-list-dark.rasi")\"|" ~/bin/sway_user_preferences.sh || exit 1
  ;; 

  *)
  notify-send "Something went wrong!!!" && exit 1
  ;;
  
esac
