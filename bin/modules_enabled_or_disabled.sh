#!/bin/bash

# Program command: modules_enabled_or_disabled.sh
# Description: Enable or disable i3blocks modules.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 10-03-2025
# Program_license: GPL 3.0
# Dependencies: yad

set -e

#----------------------------------------------------------#
# Sourcing files
#----------------------------------------------------------#

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Color config
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#----------------------------------------------------------#
# User preferences
#----------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#----------------------------------------------------------#
# Apps
#----------------------------------------------------------#

apps_module() {

   if [[ "$(grep MODULE_APPS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_APPS=\).*|MODULE_APPS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_APPS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_APPS=\).*|MODULE_APPS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f apps_module

#----------------------------------------------------------#
# Battery
#----------------------------------------------------------#

battery_module() {

   if [[ "$(grep MODULE_BATTERY= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_BATTERY=\).*|MODULE_BATTERY=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_BATTERY= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_BATTERY=\).*|MODULE_BATTERY=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

bash ~/bin/sway_load_settings.sh
	
}
export -f battery_module

#----------------------------------------------------------#
# Brightness
#----------------------------------------------------------#

brightness_module() {

   if [[ "$(grep MODULE_BRIGHTNESS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_BRIGHTNESS=\).*|MODULE_BRIGHTNESS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_BRIGHTNESS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_BRIGHTNESS=\).*|MODULE_BRIGHTNESS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f brightness_module

#----------------------------------------------------------#
# Chatbot
#----------------------------------------------------------#

chatbot_module() {

   if [[ "$(grep MODULE_CHATBOT= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_CHATBOT=\).*|MODULE_CHATBOT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_CHATBOT= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_CHATBOT=\).*|MODULE_CHATBOT=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f chatbot_module

#----------------------------------------------------------#
# Color picker
#----------------------------------------------------------#

picker_module() {

   if [[ "$(grep MODULE_PICKER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_PICKER=\).*|MODULE_PICKER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_PICKER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_PICKER=\).*|MODULE_PICKER=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f picker_module

#----------------------------------------------------------#
# Current workspace
#----------------------------------------------------------#

current_workspace_module() {

   if [[ "$(grep MODULE_WORKSPACE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_WORKSPACE=\).*|MODULE_WORKSPACE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_WORKSPACE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_WORKSPACE=\).*|MODULE_WORKSPACE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f current_workspace_module

#----------------------------------------------------------#
# Date module
#----------------------------------------------------------#

date_module() {

   if [[ "$(grep MODULE_DATE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_DATE=\).*|MODULE_DATE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_DATE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_DATE=\).*|MODULE_DATE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

bash ~/bin/sway_load_settings.sh
	
}
export -f date_module

#----------------------------------------------------------#
# File search module
#----------------------------------------------------------#

file_search_module() {

   if [[ "$(grep MODULE_LOCAL_SEARCH= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_LOCAL_SEARCH=\).*|MODULE_LOCAL_SEARCH=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_LOCAL_SEARCH= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_LOCAL_SEARCH=\).*|MODULE_LOCAL_SEARCH=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f file_search_module

#----------------------------------------------------------#
# Keyboard layout
#----------------------------------------------------------#

keyboard_layout_module() {

   if [[ "$(grep MODULE_LAYOUT_KEYBOARD= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_LAYOUT_KEYBOARD=\).*|MODULE_LAYOUT_KEYBOARD=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_LAYOUT_KEYBOARD= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_LAYOUT_KEYBOARD=\).*|MODULE_LAYOUT_KEYBOARD=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f keyboard_layout_module

#----------------------------------------------------------#
# Keyboard shortcuts
#----------------------------------------------------------#

keyboard_shortcuts_module() {

   if [[ "$(grep MODULE_KEYBDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_KEYBDS=\).*|MODULE_KEYBDS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_KEYBDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_KEYBDS=\).*|MODULE_KEYBDS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

bash ~/bin/sway_load_settings.sh
	
}
export -f keyboard_shortcuts_module

#----------------------------------------------------------#
# Layouts
#----------------------------------------------------------#

layouts_module() {

   if [[ "$(grep MODULE_LAYOUTS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_LAYOUTS=\).*|MODULE_LAYOUTS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_LAYOUTS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_LAYOUTS=\).*|MODULE_LAYOUTS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f layouts_module

#----------------------------------------------------------#
# Microphone
#----------------------------------------------------------#

microphone_module() {

   if [[ "$(grep MODULE_MICROPHONE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_MICROPHONE=\).*|MODULE_MICROPHONE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_MICROPHONE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_MICROPHONE=\).*|MODULE_MICROPHONE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

}
export -f microphone_module

#----------------------------------------------------------#
# Monitor
#----------------------------------------------------------#

monitor_module() {

   if [[ "$(grep MODULE_MONITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_MONITOR=\).*|MODULE_MONITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_MONITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_MONITOR=\).*|MODULE_MONITOR=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f monitor_module

#----------------------------------------------------------#
# Multiple workspaces
#----------------------------------------------------------#

multiple_workspaces_module() {

   if [[ "$(grep MODULE_MULTIPLE_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_MULTIPLE_WORKSPACES=\).*|MODULE_MULTIPLE_WORKSPACES=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_MULTIPLE_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_MULTIPLE_WORKSPACES=\).*|MODULE_MULTIPLE_WORKSPACES=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f multiple_workspaces_module

#----------------------------------------------------------#
# Nightlight
#----------------------------------------------------------#

nightlight_module() {

   if [[ "$(grep MODULE_NIGHTLIGHT= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_NIGHTLIGHT=\).*|MODULE_NIGHTLIGHT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_NIGHTLIGHT= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_NIGHTLIGHT=\).*|MODULE_NIGHTLIGHT=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f nightlight_module

#----------------------------------------------------------#
# Notifications
#----------------------------------------------------------#

notifications_module() {

   if [[ "$(grep MODULE_NOTIFICATIONS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_NOTIFICATIONS=\).*|MODULE_NOTIFICATIONS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_NOTIFICATIONS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_NOTIFICATIONS=\).*|MODULE_NOTIFICATIONS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f notifications_module

#----------------------------------------------------------#
# Media player
#----------------------------------------------------------#

player_module() {

   if [[ "$(grep MODULE_QUICK_PLAYER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_QUICK_PLAYER=\).*|MODULE_QUICK_PLAYER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_QUICK_PLAYER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_QUICK_PLAYER=\).*|MODULE_QUICK_PLAYER=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f player_module

#----------------------------------------------------------#
# Power menu
#----------------------------------------------------------#

powermenu_module() {

   if [[ "$(grep MODULE_POWERMENU= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_POWERMENU=\).*|MODULE_POWERMENU=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_POWERMENU= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_POWERMENU=\).*|MODULE_POWERMENU=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

bash ~/bin/sway_load_settings.sh
	
}
export -f powermenu_module

#----------------------------------------------------------#
# Time
#----------------------------------------------------------#

time_module() {

   if [[ "$(grep MODULE_TIME= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_TIME=\).*|MODULE_TIME=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_TIME= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_TIME=\).*|MODULE_TIME=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

bash ~/bin/sway_load_settings.sh
	
}
export -f time_module

#----------------------------------------------------------#
# Title
#----------------------------------------------------------#

title_module() {

   if [[ "$(grep MODULE_WIN_TITLE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_WIN_TITLE=\).*|MODULE_WIN_TITLE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_WIN_TITLE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_WIN_TITLE=\).*|MODULE_WIN_TITLE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f title_module

#----------------------------------------------------------#
# Volume
#----------------------------------------------------------#

volume_module() {

   if [[ "$(grep MODULE_VOLUME= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_VOLUME=\).*|MODULE_VOLUME=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_VOLUME= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_VOLUME=\).*|MODULE_VOLUME=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f volume_module

#----------------------------------------------------------#
# Weather
#----------------------------------------------------------#

weather_module() {

   if [[ "$(grep MODULE_WEATHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_WEATHER=\).*|MODULE_WEATHER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_WEATHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_WEATHER=\).*|MODULE_WEATHER=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

bash ~/bin/sway_load_settings.sh
	
}
export -f weather_module

#----------------------------------------------------------#
# Wifi
#----------------------------------------------------------#

wifi_module() {

   if [[ "$(grep MODULE_WIFI= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_WIFI=\).*|MODULE_WIFI=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_WIFI= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_WIFI=\).*|MODULE_WIFI=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

bash ~/bin/sway_load_settings.sh
	
}
export -f wifi_module

#----------------------------------------------------------#
# Browser app
#----------------------------------------------------------#

browser_module() {

   if [[ "$(grep MODULE_ACTIVE_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_BROWSER=\).*|MODULE_ACTIVE_BROWSER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_ACTIVE_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_BROWSER=\).*|MODULE_ACTIVE_BROWSER=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f browser_module

#----------------------------------------------------------#
# File manager
#----------------------------------------------------------#

filemanager_module() {

   if [[ "$(grep MODULE_ACTIVE_FILEMANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_FILEMANAGER=\).*|MODULE_ACTIVE_FILEMANAGER=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_ACTIVE_FILEMANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_FILEMANAGER=\).*|MODULE_ACTIVE_FILEMANAGER=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f filemanager_module

#----------------------------------------------------------#
# Terminal
#----------------------------------------------------------#

terminal_module() {

   if [[ "$(grep MODULE_ACTIVE_TERMINAL= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_TERMINAL=\).*|MODULE_ACTIVE_TERMINAL=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_ACTIVE_TERMINAL= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_TERMINAL=\).*|MODULE_ACTIVE_TERMINAL=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f terminal_module

#----------------------------------------------------------#
# Text editor
#----------------------------------------------------------#

texteditor_module() {

   if [[ "$(grep MODULE_ACTIVE_TEXTEDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_TEXTEDITOR=\).*|MODULE_ACTIVE_TEXTEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_ACTIVE_TEXTEDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_TEXTEDITOR=\).*|MODULE_ACTIVE_TEXTEDITOR=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f texteditor_module

#----------------------------------------------------------#
# Development
#----------------------------------------------------------#

development_module() {

   if [[ "$(grep MODULE_ACTIVE_DEVELOPMENT= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_DEVELOPMENT=\).*|MODULE_ACTIVE_DEVELOPMENT=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_ACTIVE_DEVELOPMENT= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_DEVELOPMENT=\).*|MODULE_ACTIVE_DEVELOPMENT=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f development_module

#----------------------------------------------------------#
# Social
#----------------------------------------------------------#

social_module() {

   if [[ "$(grep MODULE_ACTIVE_SOCIAL= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_SOCIAL=\).*|MODULE_ACTIVE_SOCIAL=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_ACTIVE_SOCIAL= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_SOCIAL=\).*|MODULE_ACTIVE_SOCIAL=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f social_module

#----------------------------------------------------------#
# Image editor
#----------------------------------------------------------#

imageeditor_module() {

   if [[ "$(grep MODULE_ACTIVE_IMAGEEDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_IMAGEEDITOR=\).*|MODULE_ACTIVE_IMAGEEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_ACTIVE_IMAGEEDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_IMAGEEDITOR=\).*|MODULE_ACTIVE_IMAGEEDITOR=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f imageeditor_module

#----------------------------------------------------------#
# Video editor
#----------------------------------------------------------#

videoeditor_module() {

   if [[ "$(grep MODULE_ACTIVE_VIDEOEDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_VIDEOEDITOR=\).*|MODULE_ACTIVE_VIDEOEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_ACTIVE_VIDEOEDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_VIDEOEDITOR=\).*|MODULE_ACTIVE_VIDEOEDITOR=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f videoeditor_module

#----------------------------------------------------------#
# Audio editor
#----------------------------------------------------------#

audioeditor_module() {

   if [[ "$(grep MODULE_ACTIVE_AUDIOEDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_AUDIOEDITOR=\).*|MODULE_ACTIVE_AUDIOEDITOR=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_ACTIVE_AUDIOEDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_AUDIOEDITOR=\).*|MODULE_ACTIVE_AUDIOEDITOR=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f audioeditor_module

#----------------------------------------------------------#
# Office
#----------------------------------------------------------#

office_module() {

   if [[ "$(grep MODULE_ACTIVE_OFFICE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_OFFICE=\).*|MODULE_ACTIVE_OFFICE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_ACTIVE_OFFICE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_OFFICE=\).*|MODULE_ACTIVE_OFFICE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f office_module

#----------------------------------------------------------#
# Quick actions
#----------------------------------------------------------#

actions_module() {

   if [[ "$(grep MODULE_QUICK_ACTIONS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_QUICK_ACTIONS=\).*|MODULE_QUICK_ACTIONS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_QUICK_ACTIONS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_QUICK_ACTIONS=\).*|MODULE_QUICK_ACTIONS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f actions_module

#----------------------------------------------------------#
# Settings
#----------------------------------------------------------#

settings_module() {

   if [[ "$(grep MODULE_SETTINGS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_SETTINGS=\).*|MODULE_SETTINGS=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_SETTINGS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_SETTINGS=\).*|MODULE_SETTINGS=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

bash ~/bin/sway_load_settings.sh   
	
}
export -f settings_module

#----------------------------------------------------------#
# Misc active app
#----------------------------------------------------------#

misc_module() {

   if [[ "$(grep MODULE_ACTIVE_TEMPORARY= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_TEMPORARY=\).*|MODULE_ACTIVE_TEMPORARY=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_ACTIVE_TEMPORARY= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_ACTIVE_TEMPORARY=\).*|MODULE_ACTIVE_TEMPORARY=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f misc_module

#----------------------------------------------------------#
# Brightness, volume and microphone
#----------------------------------------------------------#

brvomi_module() {

   if [[ "$(grep MODULE_BRIGHTNESS_VOLUME_MICROPHONE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\).*|MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_BRIGHTNESS_VOLUME_MICROPHONE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\).*|MODULE_BRIGHTNESS_VOLUME_MICROPHONE=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f brvomi_module

#----------------------------------------------------------#
# Separate workspace modules
#----------------------------------------------------------#

separate_workspace_modules() {

   if [[ "$(grep MODULE_SEPARATE_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MODULE_SEPARATE_WORKSPACES=\).*|MODULE_SEPARATE_WORKSPACES=\"$(printf '%s' "disable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   elif [[ "$(grep MODULE_SEPARATE_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MODULE_SEPARATE_WORKSPACES=\).*|MODULE_SEPARATE_WORKSPACES=\"$(printf '%s' "enable")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi
	
}
export -f separate_workspace_modules

#----------------------------------------------------------#
# Yad dialog
#----------------------------------------------------------#

_yad() {

GTK_THEME="$MY_GTK_THEME" yad --form \
 --title="Enable/disable modules" \
 --columns=1 \
 --text-align="center" \
 --text="\nEnable/disable modules\n" \
 --width=400 \
 --height=500 \
 --scroll \
 --borders=30 \
 --field="":LBL "" \
 --field="<b>Apps</b>":fbtn '/bin/bash -c "apps_module"' \
 --field="":LBL "" \
 --field="<b>Battery</b>":fbtn '/bin/bash -c "battery_module"' \
 --field="":LBL "" \
 --field="<b>Brightness</b>":fbtn '/bin/bash -c "brightness_module"' \
 --field="":LBL "" \
 --field="<b>Chatbot</b>":fbtn '/bin/bash -c "chatbot_module"' \
 --field="":LBL "" \
 --field="<b>Color Picker</b>":fbtn '/bin/bash -c "picker_module"' \
 --field="":LBL "" \
 --field="<b>Current workspace</b>":fbtn '/bin/bash -c "current_workspace_module"' \
 --field="":LBL "" \
 --field="<b>Date</b>":fbtn '/bin/bash -c "date_module"' \
 --field="":LBL "" \
 --field="<b>File search</b>":fbtn '/bin/bash -c "file_search_module"' \
 --field="":LBL "" \
 --field="<b>Keyboard layout</b>":fbtn '/bin/bash -c "keyboard_layout_module"' \
 --field="":LBL "" \
 --field="<b>Keyboard shortcuts</b>":fbtn '/bin/bash -c "keyboard_shortcuts_module"' \
 --field="":LBL "" \
 --field="<b>Layouts</b>":fbtn '/bin/bash -c "layouts_module"' \
 --field="":LBL "" \
 --field="<b>Microphone</b>":fbtn '/bin/bash -c "microphone_module"' \
 --field="":LBL "" \
 --field="<b>Monitor</b>":fbtn '/bin/bash -c "monitor_module"' \
 --field="":LBL "" \
 --field="<b>Multiple workspaces</b>":fbtn '/bin/bash -c "multiple_workspaces_module"' \
 --field="":LBL "" \
 --field="<b>Nightlight</b>":fbtn '/bin/bash -c "nightlight_module"' \
 --field="":LBL "" \
 --field="<b>Notifications</b>":fbtn '/bin/bash -c "notifications_module"' \
 --field="":LBL "" \
 --field="<b>Player</b>":fbtn '/bin/bash -c "player_module"' \
 --field="":LBL "" \
 --field="<b>Power menu</b>":fbtn '/bin/bash -c "powermenu_module"' \
 --field="":LBL "" \
 --field="<b>Time</b>":fbtn '/bin/bash -c "time_module"' \
 --field="":LBL "" \
 --field="<b>Title</b>":fbtn '/bin/bash -c "title_module"' \
 --field="":LBL "" \
 --field="<b>Volume</b>":fbtn '/bin/bash -c "volume_module"' \
 --field="":LBL "" \
 --field="<b>Weather (wttr)</b>":fbtn '/bin/bash -c "weather_module"' \
 --field="":LBL "" \
 --field="<b>Wifi</b>":fbtn '/bin/bash -c "wifi_module"' \
 --field="":LBL "" \
 --field="<b>Quick actions</b>":fbtn '/bin/bash -c "actions_module"' \
 --field="":LBL "" \
 --field="<b>Settings</b>":fbtn '/bin/bash -c "settings_module"' \
 --field="":LBL "" \
 --field="<b>Brightness/volume/microphone</b>":fbtn '/bin/bash -c "brvomi_module"' \
 --field="":LBL "" \
 --field="<b>Separate workspace modules</b>":fbtn '/bin/bash -c "separate_workspace_modules"' \
 --field="":LBL "" \
 --field="<b>Browser</b>":fbtn '/bin/bash -c "browser_module"' \
 --field="":LBL "" \
 --field="<b>File manager</b>":fbtn '/bin/bash -c "filemanager_module"' \
 --field="":LBL "" \
 --field="<b>Terminal</b>":fbtn '/bin/bash -c "terminal_module"' \
 --field="":LBL "" \
 --field="<b>Text editor</b>":fbtn '/bin/bash -c "texteditor_module"' \
 --field="":LBL "" \
 --field="<b>Development</b>":fbtn '/bin/bash -c "development_module"' \
 --field="":LBL "" \
 --field="<b>Social</b>":fbtn '/bin/bash -c "social_module"' \
 --field="":LBL "" \
 --field="<b>Image editor</b>":fbtn '/bin/bash -c "imageeditor_module"' \
 --field="":LBL "" \
 --field="<b>Video editor</b>":fbtn '/bin/bash -c "videoeditor_module"' \
 --field="":LBL "" \
 --field="<b>Audio editor</b>":fbtn '/bin/bash -c "audioeditor_module"' \
 --field="":LBL "" \
 --field="<b>Office</b>":fbtn '/bin/bash -c "office_module"' \
 --field="":LBL "" \
 --field="<b>Misc</b>":fbtn '/bin/bash -c "misc_module"' \
 --field="":LBL "" \
 --button="Exit":1

 export _yad

}
export -f _yad
_yad

#----------------------------------------------------------#
# Unset variables and functions
#----------------------------------------------------------#

unset _yad
unset MY_GTK_THEME
unset apps_module
unset battery_module
unset brightness_module
unset chatbot_module
unset picker_module
unset current_workspace_module
unset date_module
unset file_search_module
unset keyboard_layout_module
unset keyboard_shortcuts_module
unset layouts_module
unset microphone_module
unset monitor_module
unset multiple_workspaces_module
unset nightlight_module
unset notifications_module
unset player_module
unset powermenu_module
unset time_module
unset title_module
unset volume_module
unset weather_module
unset wifi_module
unset browser_module
unset filemanager_module
unset terminal_module
unset texteditor_module
unset development_module
unset social_module
unset imageeditor_module
unset videoeditor_module
unset audioeditor_module
unset office_module
unset actions_module
unset settings_module
unset misc_module
unset brvomi_module
unset separate_workspace_modules
