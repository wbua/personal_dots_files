#!/bin/bash

# Program command: yad_radio_list.sh
# Description: Select and play radio streams. Used with polybar.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 26-06-2023
# Program_license: GPL 3.0
# Dependencies: yad, mpv

# Used with the resolution of 1920x1080

#--------------------------------------------------------------------#
# Kill new instance
#--------------------------------------------------------------------#

killall yad

#--------------------------------------------------------------------#
# Variables
#--------------------------------------------------------------------#

# yad x position
box_x_position="1100"
export box_x_position
# yad y position
box_y_position="60"
export box_y_position


yad_launcher() {
	
GTK_THEME="alt-dialog" yad --list --title="Choose radio station" --width=700 --height=210 \
--separator= --column="Double click on item to launch it" --buttons-layout="center" --borders=30 \
--no-headers --no-buttons --posx="$box_x_position" --posy="$box_y_position" \
--button="exit":1
	
}
export -f yad_launcher 

#-------------------------------------------------------#
# Colors for menu items (pango markup)
#-------------------------------------------------------#

color1="<span color='#25FF00' font_family='Inter' font='10.5' weight='bold' rise='0pt'>"
export color1
end="</span>"
export end

date_c1() {
	date
}
export -f date_c1

uptime_c2() {
	uptime
	
}
export -f uptime_c2

menu() {
date_c1
uptime_c2
	
}
export -f menu

#-------------------------------------------------------#
# Yad settings
#-------------------------------------------------------#

choice=$(menu | yad_launcher) 
export choice

#-------------------------------------------------------#
# Main
#-------------------------------------------------------#

[ -z "$choice" ] && exit 0 || killall mpv ; echo "$choice" | xclip -selection clipboard

#-------------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------------#

unset choice
unset color1
unset end
unset box_x_position
unset box_y_position
unset date_c1
unset uptime_c2
unset yad_launcher
unset menu
