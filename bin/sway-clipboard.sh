#!/bin/bash

# Program command: sway-clipboard.sh
# Description: Text only clipboard for wayland.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 19-05-2024
# Program_license: GPL 3.0
# Dependencies: wofi, wl-clipboard, wtype

# Make sure you create this file first.
# touch "$HOME"/Documents/johns_wl-clipboard.txt

# Add this to your swaywm config.
# bindsym $mod5+c exec --no-startup-id wtype -M ctrl -k c && wl-paste >> "$HOME"/Documents/johns_wl-clipboard.txt

# Todo
# notify-send message about duplicates

#-----------------------------------------------#
# Paste clipboard contents into text file
#-----------------------------------------------#

wl-paste | tr -d '\n' | tee -a "$HOME"/Documents/johns_wl-clipboard.txt

#-----------------------------------------------#
# Create files and directories
#-----------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -f "$HOME"/Documents/johns_wl-clipboard.txt ]; then
    :
   
     touch "$HOME"/Documents/johns_wl-clipboard.txt
   fi

#-----------------------------------------------#
# Colors
#-----------------------------------------------#

# Message
color1="<span color='#8087A2' font_family='Cascadia Mono' font='14' weight='bold' rise='0pt'>"
# Color end
end="</span>"

#-----------------------------------------------#
# Rofi settings
#-----------------------------------------------#

_rofi() {

rofi -dmenu -i -p ''

}

#-----------------------------------------------#
# Remove whitespace from text file
#-----------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/johns_wl-clipboard.txt

#-----------------------------------------------#
# Remove duplicate lines
#-----------------------------------------------#

gawk -i inplace '!seen[$0]++' "$HOME"/Documents/johns_wl-clipboard.txt

#-----------------------------------------------#
# Main
#-----------------------------------------------#

main() {

clip_text_file=$(tac "$HOME"/Documents/johns_wl-clipboard.txt | _rofi)
[ -z "$clip_text_file" ] && exit 0

echo "$clip_text_file" | wl-copy -n

}
main
