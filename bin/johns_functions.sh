#!/bin/bash
# shellcheck disable=SC2154
# shellcheck disable=SC2002
# shellcheck disable=SC2116

# Program command: johns_functions.sh
# Description: These are johns functions.
# Contributors: John Mcgrath
# Program_version: 2.0.0
# Program updated: 21-04-2024
# Program_license: GPL 3.0

############################################################
############################################################

# This script is currrent moving from xorg to wayland.
# Some functions may not work in wayland yet.

############################################################
############################################################

# Function sections

# Run launchers
# Actions (find a file and then perform an action)
# Actions-downloads (find a file and then perform an action)
# Actions-radio
# Actions-text
# Actions-bookmarks
# Actions-directories
# Poweroff, restart and lock machine (uses zenity)
# Screenshots
# Wallpapers
# Radio
# Audacious
# Configs
# Web searches
# Bookmarks
# File manager
# Window switcher
# Bash
# Color codes
# Sticky note
# Emoji
# Volume
# Time zones
# Convert
# Add apps
# Download folder
# Download videos
# Single note
# Find local files
# Calculator
# Kill processes
# QR Codes
# Run terminal command
# PDF
# Text to speech
# Zipping files
# Open prefixrofi config
# Separate notes
# Record video and audio
# Date
# Uptime
# Spell checker
# Password generator
# Local Dictionary
# Empty trash
# Quick links
# Close program
# Snippets
# Downloading files
# Last created temp files
# Weather
# Apps
# Rofi theme changer
# Last modified files
# Command palettes (sway only)
# Microphone
# Quick text actions
# Control brave with playerctl
# IP address
# Extra information about keywords
# Video cutter
# Add quicklink to text file for root search
# Brave bookmarks
# Reminders
# Switch to already open app (sway only)
# Open new instance of app
# Quicklinks in root search
# Use last sub menu used
# Keywords help page
# Menu

#-------------------------------------------------------#
# Run launchers
#-------------------------------------------------------#

_rofi() {

rofi -dmenu -i -p '' -config "$rofi_config" -matching prefix \
-markup-rows -me-select-entry '' -me-accept-entry 'MousePrimary' \
-theme-str 'configuration {hover-select: true; me-accept-entry: "MousePrimary"; me-select-entry: "";}' \
-theme-str 'entry { placeholder: "Search for apps and commands...";}' \
-theme-str 'element { highlight: none;}' \
-theme-str 'window {width: 55%; padding: 0px 0px 0px 0px;}' \
-theme-str 'message {  margin: -20px 0px 0px 0px; border: 0px 0px 0px 0px; padding: 10px 10px 10px 10px; border-color: #000000; border-radius: 0px;}' \
-theme-str 'listview {lines: 8; columns: 1; padding: 0px 12px 35px 12px;}'

}

_rofi2() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 1; columns: 8;}' \
-theme-str 'window {width: 55%;}'

}

_rofi3() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none;}' \
-theme-str 'window {width: 55%;}'

}

_rofi4() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'window {width: 55%;}'

}

_rofi5() {

rofi -dmenu -i -p '' -markup-rows -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'window {width: 55%;}'

}

_rofi6() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'window {width: 55%;}'

}

_rofi7() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'window {width: 55%;}' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}'

}

_rofi8() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'window {width: 55%;}' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}'

}

_rofi9() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-kb-custom-2 "alt+c" \
-theme-str 'window {width: 70%;}' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}'

}

# Weather
_rofi10() {
	
rofi -dmenu -i -p '' \
-mesg "${color12}${forecast_area}${end}$(weather_info)" \
-markup-rows \
-config "$rofi_config" \
-theme-str 'window {width: 63%;}' \
-theme-str 'window {padding: 25px;}' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'inputbar {enabled: false;}'
	
}

# Web search
_rofi11() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'entry { placeholder: "Web Search...";}' \
-theme-str 'window {width: 50%;}'

}

# file actions
_rofi14() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-mesg "$(action_last_file)" \
-markup-rows \
-theme-str 'entry { placeholder: "Actions search...";}' \
-theme-str 'message { margin: 30px; }' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%; padding: 0px 0px 0px 0px;}'

}

# Find files
_rofi16() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-mesg "${color13}${space}open: ${color30}enter${end} | select file for actions: ${color30}alt+c${end} | actions: ${color30}alt+a${end}${end}" \
-kb-custom-2 "alt+c" \
-kb-custom-3 "alt+a" \
-theme-str 'entry { placeholder: "File search...";}' \
-theme-str 'window {width: 70%;}' \
-theme-str 'message {padding: 20px;}' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}'

}

# Actions for downloads
_rofi15() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-mesg "$(action_last_file_download)" \
-markup-rows \
-theme-str 'entry { placeholder: "Actions download search...";}' \
-theme-str 'message { margin: 30px; }' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%; padding: 0px 0px 0px 0px;}'

}

# Downloads directory
_rofi17() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-mesg "${color14}${space}open: ${color30}enter${end} | file actions: ${color30}alt+c${end} | actions: ${color30}alt+a${end}${end}" \
-kb-custom-2 "alt+c" \
-kb-custom-3 "alt+a" \
-theme-str 'entry { placeholder: "Download file search...";}' \
-theme-str 'window {width: 70%;}' \
-theme-str 'message {padding: 20px;}' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}'

}

# Radio
_rofi18() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-kb-custom-2 "alt+a" \
-mesg "${color14}${space}play: ${color30}enter${end} | actions: ${color30}alt+a${end}${end} ${color14}| status:${end} ${color30}$(mpv_check)${end}" \
-theme-str 'entry { placeholder: "Radio search...";}' \
-theme-str 'window {width: 55%;}' \
-theme-str 'message {padding: 20px;}' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}'

}

# Radio actions
_rofi19() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-mesg "$(action_last_file_radio)" \
-markup-rows \
-theme-str 'entry { placeholder: "Actions radio search...";}' \
-theme-str 'message { margin: 30px; }' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%; padding: 0px 0px 0px 0px;}'

}

# Actions text
_rofi20() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-mesg "$(action_last_file_block)" \
-markup-rows \
-theme-str 'entry { placeholder: "Actions text search...";}' \
-theme-str 'message { margin: 30px; }' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%; padding: 0px 0px 0px 0px;}'

}

# Bookmarks
_rofi21() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-markup-rows \
-mesg "${color13}${space}open: ${color30}enter${end} | link actions: ${color30}alt+c${end} | actions: ${color30}alt+a${end}${end}" \
-kb-custom-2 "alt+c" \
-kb-custom-3 "alt+a" \
-theme-str 'entry { placeholder: "Bookmarks search...";}' \
-theme-str 'message { margin: 30px; }' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%;}'

}

# Actions bookmarks
_rofi22() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-mesg "$(action_last_file_bookmarks)" \
-markup-rows \
-theme-str 'entry { placeholder: "Actions bookmark search...";}' \
-theme-str 'message { margin: 30px; }' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%; padding: 0px 0px 0px 0px;}'

}

# Directories
_rofi23() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-markup-rows \
-mesg "${color13}${space}open: ${color30}enter${end} | path actions: ${color30}alt+c${end} | actions: ${color30}alt+a${end}${end}" \
-kb-custom-2 "alt+c" \
-kb-custom-3 "alt+a" \
-theme-str 'entry { placeholder: "Directories search...";}' \
-theme-str 'message { margin: 30px; }' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%;}'

}

# Actions directories
_rofi24() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-mesg "$(action_last_file_directories)" \
-markup-rows \
-theme-str 'entry { placeholder: "Actions directories search...";}' \
-theme-str 'message { margin: 30px; }' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%; padding: 0px 0px 0px 0px;}'

}

# Quicklinks
_rofi25() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-markup-rows \
-mesg "${color19}${space}open: ${color30}enter${end} | cli path: ${color30}alt+c${end} | clip: ${color30}alt+v${end} | file: ${color30}alt+t${end} | query: ${color30}alt+q${end}${end}" \
-kb-custom-2 "alt+c" \
-kb-custom-3 "alt+v" \
-kb-custom-5 "alt+t" \
-kb-custom-6 "alt+q" \
-theme-str 'entry { placeholder: "Quicklinks search...";}' \
-theme-str 'message { margin: 30px; }' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%;}'

}

# Add quicklink
_rofi26() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'entry { placeholder: "Add quicklink...";}' \
-theme-str 'window {width: 55%;}'

}

# Oneline notes
_rofi27() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-kb-custom-2 "alt+t" \
-kb-custom-3 "alt+n" \
-kb-custom-4 "alt+q" \
-kb-custom-5 "alt+s" \
-mesg "${color14}clip: ${color30}enter${end} | add: ${color30}alt+n${end} | file: ${color30}alt+t${end} | QR Code: ${color30}alt+q${end} | speak: ${color30}alt+s${end}${end}" \
-theme-str 'entry { placeholder: "Notes search...";}' \
-theme-str 'window {width: 55%;}' \
-theme-str 'message {padding: 20px;}' \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}'

}

# Reminders
_rofi28() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-markup-rows \
-mesg " 
 ${color14}clipboard: ${color30}enter${end} | add: ${color30}alt+a${end} | delete: ${color30}alt+d${end} | mark as done: ${color30}alt+c${end} | pin: ${color30}alt+v${end}${end}

 $(cat "$HOME"/Documents/.pin_reminder.txt)" \
-kb-custom-2 "alt+a" \
-kb-custom-3 "alt+d" \
-kb-custom-4 "alt+c" \
-kb-custom-5 "alt+v" \
-theme-str 'element.selected.normal { background-color: transparent; border-color: #CAD3F5; border: 1 1 1 1; text-color: #CAD3F5;}' \
-theme-str 'entry { placeholder: "Search reminders...";}' \
-theme-str 'message { margin: 30px; border-color: #41434E; border: 0px 0px 1px 0px; padding: 0px 0px 30px 0px;}' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'element { highlight: none; }' \
-theme-str 'window {width: 65%;}'

}

# Configs
_rofi29() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-markup-rows \
-mesg "${color13}open: ${color30}enter${end} | open cli: ${color30}alt+c${end} | add config: ${color30}alt+a${end} | text file: ${color30}alt+t${end}${end}" \
-theme-str 'element { highlight: none; }' \
-kb-custom-2 "alt+c" \
-kb-custom-3 "alt+a" \
-kb-custom-4 "alt+t" \
-theme-str 'message {padding: 20px;}' \
-theme-str 'entry { placeholder: "Search configs...";}' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%;}'

}

# Dictionary
_rofi30() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%;}'

}

# Last modified files
_rofi31() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%;}'

}

# Snippets
_rofi32() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%;}'

}

# Create snippet
_rofi33() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-mesg " 
 ${color31}Type keyword, followed by a space and then your text${end}

 ${color32}keyword ${end}${color31}text ${end}

 ${color32}email${end} ${color31}johnsmith@example.com${end}" \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%;}'

}

# Previous files searches
_rofi34() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%;}'

}

# Wallpapers
_rofi35() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%;}'

}

# Timezones
_rofi36() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%;}'

}

# Qr codes
_rofi37() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'entry { placeholder: "type text to create qr code...";}' \
-theme-str 'window {width: 55%;}'

}

# PDF
_rofi38() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'entry { placeholder: "type file path to create pdf...";}' \
-theme-str 'window {width: 55%;}'

}

# Add hex code
_rofi39() {

rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'element { highlight: none; }' \
-theme-str 'entry { placeholder: "type hex code to create image...";}' \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {width: 55%;}'

}


#-------------------------------------------------------#
# Actions for all files
#-------------------------------------------------------#

# Show last file selected by finder
action_last_file() {

my_actions_file=$(cat ~/Documents/.temp_actions.txt)
echo "${my_actions_file##*/}"
    	
}

# Open with selected programs
selected_menu() {
	
echo "$image_editor"
echo "$image_viewer"
echo "$audio_player"
echo "$sound_editor"
echo "$video_player"
echo "$other_video_player"
echo "$file_manager"
echo "$texteditor"
echo "$office_suite"
echo "$other_office_suite"
	
}

# Menu for actions
menu3() {

echo "open ${color6}opens the file${end}"
echo "open-with ${color6}select program to open file${end}"
echo "file ${color6}opens file in file manager${end}"
echo "copy ${color6}copies the file${end}"
echo "move ${color6}moves the file${end}"	
echo "clip ${color6}copies file path to clipboard${end}"
echo "back ${color6}go to main find window${end}"
echo "convert ${color6}converts file from path in clipboard${end}"
echo "previous ${color6}previous select files${end}"
echo "recent ${color6}show recently copied and moved files${end}"
echo "none ${color6}perform no action${end}"
echo "quicklink ${color6}add quicklink${end}"	
		
}

# Perform actions on files
rofi_actions() {

echo "${color26}${end}${space}actions ${color21}${space}file actions${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt	
choice=$(menu3 | _rofi14 | awk '{print $1}')
[[ -z "$choice" ]] && exit
	
case "$choice" in

 'copy')
   actions_file=$(cat "$HOME"/Documents/.temp_actions.txt)
   if [[ -f "$actions_file" ]]; then
   dir_choice=$(find "${dirs[@]}" -maxdepth 9 -type d | _rofi14)
   [[ -z "$dir_choice" ]] && exit
   echo "$dir_choice" | tee "$HOME"/Documents/.dir_actions.txt
   cp "$actions_file" "$dir_choice"
   check_file=$(cat "$HOME"/Documents/.temp_actions.txt | tr -d '\n' | xargs -0 basename)
   check_dir=$(cat "$HOME"/Documents/.dir_actions.txt)
   echo "${check_dir}${check_file}" | tee ~/Documents/.recently_cpmv.txt 
   else
   notify-send "File does not exist in this path!"
   fi
   ;;
 'open')
   text_file=$(cat "$HOME"/Documents/.temp_actions.txt)
   if [[ -f "$text_file" ]]; then
   cat "$HOME"/Documents/.temp_actions.txt | xargs -0 -d '\n' xdg-open
   else
   notify-send "File does not exist in this path!"
   fi
   ;;
  
 'move')
   actions_file=$(cat "$HOME"/Documents/.temp_actions.txt)
   if [[ -f "$actions_file" ]]; then
   dir_choice=$(find "${dirs[@]}" -maxdepth 9 -type d | _rofi14)
   echo "$dir_choice" | tee "$HOME"/Documents/.dir_actions.txt
   mv "$actions_file" "$dir_choice"
   check_file=$(cat "$HOME"/Documents/.temp_actions.txt | tr -d '\n' | xargs -0 basename)
   check_dir=$(cat "$HOME"/Documents/.dir_actions.txt)
   echo "${check_dir}${check_file}" | tee ~/Documents/.recently_cpmv.txt 
   else
   notify-send "File does not exist in this path!"
   fi
   ;;
   
 'file')
   text_file=$(cat "$HOME"/Documents/.temp_actions.txt)
   if [[ -f "$text_file" ]]; then
   cat "$HOME"/Documents/.temp_actions.txt | xargs -0 -d '\n' "$file_manager"
   else
   notify-send "File does not exist in this path!"
   fi
   ;;
 
 'clip')
   text_file=$(cat "$HOME"/Documents/.temp_actions.txt)
   if [[ -f "$text_file" ]]; then
   cat "$HOME"/Documents/.temp_actions.txt | wl-copy -n
   else
   notify-send "File does not exist in this path!"
   fi
   ;;
   
 'back')
   find_files
   ;;
   
 'convert')
   text_file=$(cat "$HOME"/Documents/.temp_actions.txt)
   if [[ -f "$text_file" ]]; then
   cat "$HOME"/Documents/.temp_actions.txt | wl-copy -n
   convert_files
   else
   notify-send "File does not exist in this path!"
   fi
   ;;
   
 'open-with')
   choice_file=$(cat "$HOME"/Documents/.temp_actions.txt)
   choice=$(selected_menu | _rofi14)
   [[ -z "$choice" ]] && exit
  
   if [[ -f "$choice_file" && "$choice" == "$image_editor" ]]; then
     "$image_editor" "$choice_file"
   elif [[ -f "$choice_file" && "$choice" == "$image_viewer" ]]; then
     "$image_viewer" "$choice_file"
   elif [[ -f "$choice_file" && "$choice" == "$audio_player" ]]; then
     "$audio_player" "$choice_file"
   elif [[ -f "$choice_file" && "$choice" == "$sound_editor" ]]; then
     "$sound_editor" "$choice_file"
   elif [[ -f "$choice_file" && "$choice" == "$video_player" ]]; then
     "$video_player" "$choice_file"
   elif [[ -f "$choice_file" && "$choice" == "$other_video_player" ]]; then
     "$other_video_player" "$choice_file"
   elif [[ -f "$choice_file" && "$choice" == "$file_manager" ]]; then
     "$file_manager" "$choice_file"
   elif [[ -f "$choice_file" && "$choice" == "$texteditor" ]]; then
     "$texteditor" "$choice_file"
   elif [[ -f "$choice_file" && "$choice" == "$office_suite" ]]; then
     "$office_suite" "$choice_file"
   elif [[ -f "$choice_file" && "$choice" == "$other_office_suite" ]]; then
     "$other_office_suite" "$choice_file"
   else
     notify-send "File does not exist in this path!"
   fi
 
      
   ;;
   
 'previous') 
   cat "$HOME"/Documents/.temp_actions.txt | tee -a "$HOME"/Documents/.all_actions.txt
   sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.all_actions.txt
   gawk -i inplace '!a[$0]++' "$HOME"/Documents/.all_actions.txt
   choice=$(tac "$HOME"/Documents/.all_actions.txt | _rofi34)
   [[ -z "$choice" ]] && exit
   echo "$choice" | tee "$HOME"/Documents/.temp_actions.txt
   text_file=$(cat "$HOME"/Documents/.temp_actions.txt)
  
     if [[ -f "$text_file" ]]; then
       rofi_actions
     else
       notify-send "File does not exist in this path!"
     fi
     
  ;;

 'recent')
   cat "$HOME"/Documents/.recently_cpmv.txt  | tee -a "$HOME"/Documents/.all_cpmv.txt
   gawk -i inplace '!a[$0]++' "$HOME"/Documents/.all_cpmv.txt
   sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.all_cpmv.txt
   choice=$(tac "$HOME"/Documents/.all_cpmv.txt | _rofi34)
   [[ -z "$choice" ]] && exit
   echo "$choice" | tee "$HOME"/Documents/.temp_actions.txt
   text_file=$(cat "$HOME"/Documents/.temp_actions.txt)
    
       if [[ -f "$text_file" ]]; then
         rofi_actions
       else
         notify-send "File does not exist in this path!"
       fi
       
  ;;
     
 'quicklink')
   text_file=$(cat "$HOME"/Documents/.temp_actions.txt)
   if [[ -f "$text_file" ]]; then
   sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/quick_links.txt
   cat "$HOME"/Documents/.temp_actions.txt | tee -a "$HOME"/Documents/mainprefix/quick_links.txt
   else
   notify-send "File does not exist in this path!"
   fi
   ;;
  
 'none')
   :
   ;;
  
    
  *)
  echo "Something went wrong!" | rofi2 || exit 1
  ;;

esac	
	
}

#-------------------------------------------------------#
# Actions for downloaded files
#-------------------------------------------------------#

# Show last file selected by finder
action_last_file_download() {

my_actions_file=$(cat "$HOME"/Documents/.temp_downloaded_actions.txt)
echo "${my_actions_file##*/}"
    	
}

# Open with selected programs
selected_menu_download() {
	
echo "$image_editor"
echo "$image_viewer"
echo "$audio_player"
echo "$sound_editor"
echo "$video_player"
echo "$other_video_player"
echo "$file_manager"
echo "$texteditor"
echo "$office_suite"
echo "$other_office_suite"
	
}

# Menu for actions
menu3_download() {

echo "open ${color6}opens the file${end}"
echo "open-with ${color6}select program to open file${end}"
echo "file ${color6}opens file in file manager${end}"
echo "copy ${color6}copies the file${end}"
echo "move ${color6}moves the file${end}"	
echo "clip ${color6}copies file path to clipboard${end}"
echo "back ${color6}back to downloads folder${end}"
echo "previous ${color6}previous file searches${end}"
echo "none ${color6}perform no action${end}"
echo "quicklink ${color6}add quicklink${end}"	
		
}

# Perform actions on files
rofi_actions_download() {

echo "${color26}${end}${space}actions-downloads ${color21}${space}downloaded file actions${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt	
choice=$(menu3_download | _rofi15 | awk '{print $1}')
[[ -z "$choice" ]] && exit
	
case "$choice" in

 'copy')
   actions_file=$(cat "$HOME"/Documents/.temp_downloaded_actions.txt)
   dir_choice=$(find "${dirs[@]}" -maxdepth 9 -type d | _rofi15)
   echo "$dir_choice" | tee "$HOME"/Documents/.dir_actions.txt
   cp "$actions_file" "$dir_choice"
   ;;
 'open')
   cat "$HOME"/Documents/.temp_downloaded_actions.txt | xargs -0 -d '\n' xdg-open
   ;;
  
 'move')
   actions_file=$(cat "$HOME"/Documents/.temp_downloaded_actions.txt)
   dir_choice=$(find "${dirs[@]}" -maxdepth 9 -type d | _rofi15)
   echo "$dir_choice" | tee "$HOME"/Documents/.dir_actions.txt
   mv "$actions_file" "$dir_choice"
   ;;
   
 'file')
   cat "$HOME"/Documents/.temp_downloaded_actions.txt | xargs -0 -d '\n' "$file_manager"
   ;;
 
 'clip')
   cat "$HOME"/Documents/.temp_downloaded_actions.txt | xclip -selection clipboard
   ;;
   
 'back')
   download_folder
   ;;
   
 'open-with')
   choice_file=$(cat "$HOME"/Documents/.temp_downloaded_actions.txt)
   choice=$(selected_menu_download | _rofi15)
   [[ -z "$choice" ]] && exit
   if [[ "$choice" == "$image_editor" ]]; then
     "$image_editor" "$choice_file"
   elif [[ "$choice" == "$image_viewer" ]]; then
     "$image_viewer" "$choice_file"
   elif [[ "$choice" == "$audio_player" ]]; then
     "$audio_player" "$choice_file"
   elif [[ "$choice" == "$sound_editor" ]]; then
     "$sound_editor" "$choice_file"
   elif [[ "$choice" == "$video_player" ]]; then
     "$video_player" "$choice_file"
   elif [[ "$choice" == "$other_video_player" ]]; then
     "$other_video_player" "$choice_file"
   elif [[ "$choice" == "$file_manager" ]]; then
     "$file_manager" "$choice_file"
   elif [[ "$choice" == "$texteditor" ]]; then
     "$texteditor" "$choice_file"
   elif [[ "$choice" == "$office_suite" ]]; then
     "$office_suite" "$choice_file"
   elif [[ "$choice" == "$other_office_suite" ]]; then
   "$other_office_suite" "$choice_file"
   else
    :
   fi
   ;;
   
 'previous')
  cat "$HOME"/Documents/.temp_downloaded_actions.txt  | tee -a "$HOME"/Documents/.all_actions_download.txt
  gawk -i inplace '!a[$0]++' "$HOME"/Documents/.all_actions_download.txt
  sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.all_actions_download.txt
  choice=$(tac "$HOME"/Documents/.all_actions_download.txt | _rofi2)
  [[ -z "$choice" ]] && exit
  echo "$choice" | tee "$HOME"/Documents/.temp_downloaded_actions.txt
  rofi_actions_download
  ;;
   
 'quicklink')
   sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/quick_links.txt
   cat "$HOME"/Documents/.temp_downloaded_actions.txt | tee -a "$HOME"/Documents/mainprefix/quick_links.txt
   ;;
  
 'none')
   :
   ;;
  
    
  *)
  echo "Something went wrong!" | rofi2 || exit 1
  ;;

esac	
	
}

#-------------------------------------------------------#
# Actions for radio
#-------------------------------------------------------#

# Show last file selected by finder
action_last_file_radio() {

   if [[ "$(cat "$HOME"/Documents/.mpv_recording.txt)" == 'on' ]]; then
     echo "Recording"
   elif [[ "$(playerctl status -p mpv)" == 'Playing' ]]; then
     my_actions_command=$(playerctl --player mpv metadata -f "{{title}}" | cut -c 1-80 | tr "&" "+")
     echo "${color7}${my_actions_command##*/}${end}"
     echo "$(playerctl --player mpv metadata -f "{{xesam:url}}")"  
   elif	[[ "$(playerctl status -p mpv)" == 'Paused' ]]; then
	 echo "Paused"
   elif	[[ "$(pidof ffplay)" ]]; then
     echo "Playback"
   else
     echo "Not playing!"
   fi
    	
}

# Menu for actions
menu3_radio() {

echo "play-pause ${color17}toggle play and pause${end}"
echo "record ${color17}record currently playing${end}"
echo "play ${color17}playback recording${end}"
echo "stop ${color17}stop radio${end}"
echo "back ${color17}go back to radio url's${end}"
echo "clip ${color17}copies track title to clipboard${end}"
echo "qrcode ${color17}creates qr code from track title${end}"
echo "last ${color17}last radio stream played${end}"
echo "50 ${color17}volume 50%${end}"
echo "70 ${color17}volume 70%${end}"
echo "100 ${color17}volume 100%${end}"
echo "txt ${color17}open text file containing radio url's${end}"
echo "none ${color17}perform no action${end}"
		
}

# Perform actions on files
rofi_actions_radio() {

echo "${color26}${end}${space}actions-radio ${color21}${space}radio actions${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt	
choice=$(menu3_radio | _rofi19 | awk '{print $1}')
[[ -z "$choice" ]] && exit
	
case "$choice" in

 'play-pause')
    if [[ "$(pidof mpv)" ]]; then
      playerctl --player mpv play-pause
    else
      killall mpv
      killall ffplay
      choice=$(cat "$HOME"/Documents/.current_url.txt | awk '{print $NF}')
      mpv "$choice"
    fi
    ;; 
 
 'record')
   recordradiompv
   ;;
   
 'play')
   playrecfg
   ;;
    
 'stop')
   stopmpv
   ;;
   
 'back')
   online_radio
   ;;
   
 'clip')
   playerctl --player mpv metadata -f "{{title}}" | wl-copy -n
   ;;
   
 'qrcode')
   rm -f "$HOME"/Pictures/.temqrcodes/*.png
   playerctl --player mpv metadata -f "{{title}}" | tr -d '\n' | \
   qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
   cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
   one3=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
   notify-send -i "$one3" "Created" "Qr Code"	
   ;;

 'last')
   killall mpv
   killall ffplay
   choice=$(cat "$HOME"/Documents/.last_radio_url | awk '{print $NF}')
   mpv "$choice"
   ;;

 '50')
   ra_vol50
   ;;
   
 '70')
   ra_vol70
   ;;
   
 '100')
   ra_vol100
   ;;
   
 'txt')
   ra_txt
   ;;
        
 'none')
   :
   ;;
  
    
  *)
  echo "Something went wrong!" | rofi2 || exit 1
  ;;

esac	
	
}

#-------------------------------------------------------#
# Actions for text
#-------------------------------------------------------#

# Show last file selected by finder
action_last_file_block() {

my_actions_command=$(cat "$HOME"/Documents/.block_text.txt | tr -d '\n' | cut -c 1-80)
echo "$my_actions_command"        	

}

# Menu for actions
menu3_block() {

echo "add ${color15}add text from clipboard${end}"
echo "search ${color15}do a file search${end}"
echo "google ${color15}google search${end}"
echo "lowercase ${color15}convert uppercase to lowercase, copy to clipboard${end}"
echo "dictionary ${color15}open in dictionary${end}"
echo "spell ${color15}spell checker${end}"
echo "note ${color15}create note${end}"
		
}

# Perform actions on files
rofi_actions_block() {

wl-paste > "$HOME"/Documents/.block_text.txt
echo "${color26}${end}${space}actions-block ${color21}${space}text actions${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt	
choice=$(menu3_block | _rofi20 | awk '{print $1}')
[[ -z "$choice" ]] && exit
	
case "$choice" in

 'add')
   wl-paste > "$HOME"/Documents/.block_text.txt 
   ;;
   
 'lowercase')
   cat "$HOME"/Documents/.block_text.txt | tr '[:upper:]' '[:lower:]' | wl-copy -n
   ;;

 'dictionary')
   text_file=$(cat "$HOME"/Documents/.block_text.txt)
   dict "$text_file" | rofi -dmenu -i -p '' -config "$rofi_config" -theme-str 'listview {lines: 8; columns: 2;}'
   ;;

 'search')
   text_file=$(cat "$HOME"/Documents/.block_text.txt)
   choice=$(find "${dirs[@]}" -type f -iname "*$text_file*" | rofi -dmenu -i -p '' -config "$rofi_config" \
   -theme-str 'listview {lines: 8; columns: 2;}')
   echo "$choice" | xargs -0 -d '\n' xdg-open
   ;;
   
 'google')
   cat "$HOME"/Documents/.block_text.txt > "$HOME"/Documents/mainprefix/.last_web_search.txt
   filesearch="$HOME/Documents/.block_text.txt"
   search="https://www.google.co.uk/search?q={}"
   head "$filesearch" | cut -c 1-90 | xargs -I{} "$browser" "$search"
   ;;

 'spell')
   choice=$(cat "$HOME"/Documents/.block_text.txt )
   launch=$(aspell -a list <<< "$choice" | awk 'BEGIN{FS=","; OFS="\n"} {$1=$1} 1' | \
   rofi -dmenu -i -p '' -config "$rofi_config" \
   -theme-str 'listview {lines: 8; columns: 2;}')
   echo "$launch" | awk -F ':' '{print $NF}' | sed 's| ||' | wl-copy -n
   ;;

 'note')
   cat "$HOME"/Documents/.block_text.txt >> "$HOME"/Documents/mainprefix/quicknotes.txt
   ;;
    
  *)
  echo "Something went wrong!" | rofi2 || exit 1
  ;;

esac	
	
}

#-------------------------------------------------------#
# Actions for bookmarks
#-------------------------------------------------------#

# Show last file selected by finder
action_last_file_bookmarks() {

my_actions_command=$(cat "$HOME"/Documents/.actions_bookmarks.txt | tr -d '\n' | cut -c 1-80 | sed 's|&|+|')
echo "$my_actions_command"        	

}

# Menu for actions
menu3_bookmarks() {

echo "open ${color16}open bookmark in default browser in config${end}"
echo "open-firefox ${color16}open bookmark in firefox${end}"
echo "open-chrome ${color16}open bookmark in google chrome${end}"
echo "clip ${color16}copy link to clipboard${end}"
echo "back ${color16}Go back to main bookmarks${end}"
echo "qr-code ${color16}create qr code from bookmark${end}"
echo "quicklink ${color16}add quicklink${end}"	
		
}

# Perform actions on files
rofi_actions_bookmarks() {

echo "${color26}${end}${space}actions-bookmarks ${color21}${space}bookmarks${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt	
choice=$(menu3_bookmarks | _rofi22 | awk '{print $1}')
[[ -z "$choice" ]] && exit
	
case "$choice" in

 'open')
   choice=$(cat "$HOME"/Documents/.actions_bookmarks.txt | sed 's|+|&|g' | awk '{print $NF}')
   [[ -z "$choice" ]] && exit 0
   echo "$choice" | xargs "$browser"
   ;;
   
 'open-firefox')
   choice=$(cat "$HOME"/Documents/.actions_bookmarks.txt | sed 's|+|&|g' | awk '{print $NF}')
   [[ -z "$choice" ]] && exit 0
   echo "$choice" | xargs firefox
   ;;
   
 'open-chrome')
   choice=$(cat "$HOME"/Documents/.actions_bookmarks.txt | sed 's|+|&|g' | awk '{print $NF}')
   [[ -z "$choice" ]] && exit 0
   echo "$choice" | xargs google-chrome
   ;;
   
 'clip')
   cat "$HOME"/Documents/.actions_bookmarks.txt | wl-copy -n
   ;;

 'qr-code')
   rm -f "$HOME"/Pictures/.temqrcodes/*.png
   cat "$HOME"/Documents/.actions_bookmarks.txt | tr -d '\n' | \
   qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/bookmark_$(date '+%d-%m-%Y-%H-%M-%S')".png
   cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
   one3=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
   notify-send -i "$one3" "Created" "Qr Code"
   ;;

 'quicklink')
   sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/quick_links.txt
   cat "$HOME"/Documents/.actions_bookmarks.txt | tee -a "$HOME"/Documents/mainprefix/quick_links.txt
   ;;

 'back')
   open_bookmarks
   ;;
    
  *)
  echo "Something went wrong!" | rofi2 || exit 1
  ;;

esac	
	
}

#-------------------------------------------------------#
# Actions for directories
#-------------------------------------------------------#

# Show last file selected by finder
action_last_file_directories() {

my_actions_command=$(cat "$HOME"/Documents/.directories_actions.txt | cut -c 1-80 | sed 's|&|+|')
echo "$my_actions_command"        	

}

# Menu for actions
menu3_directories() {

echo "open ${color18}open path in file manager${end}"
echo "clip ${color18}send path to clipboard${end}"
echo "back ${color18}back to main search${end}"
echo "terminal ${color18}open path in terminal${end}"
echo "quicklink ${color18}add quicklink${end}"
		
}

# Perform actions on files
rofi_actions_directories() {

echo "${color26}${end}${space}actions-directories ${color21}${space}actions for directories${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt	
choice=$(menu3_directories | _rofi24 | awk '{print $1}')
[[ -z "$choice" ]] && exit
	
case "$choice" in

 'open')
   choice=$(cat "$HOME"/Documents/.directories_actions.txt)
   [ -z "$choice" ] && exit 0
   "$file_manager" "$choice"
   ;;
   
 'back')
   find_directories
   ;;
   
 'terminal')
   choice=$(cat "$HOME"/Documents/.directories_actions.txt)
  "$myterminal" -- /bin/bash -c "cd \"$choice\"; $SHELL"
   ;;
    
 'clip')
   cat "$HOME"/Documents/.directories_actions.txt | wl-copy -n
   ;;
   
 'quicklink')
   sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/quick_links.txt
   cat "$HOME"/Documents/.directories_actions.txt | tee -a "$HOME"/Documents/mainprefix/quick_links.txt
   ;;
 
  *)
  echo "Something went wrong!" | rofi2 || exit 1
  ;;

esac	
	
}


#-------------------------------------------------------#
# Poweroff, restart and lock machine (uses zenity)
#-------------------------------------------------------#

# Power lock
yadlock () {

   if zenity --question --text "Lock, are you sure"
   then
     swaylock -c '#000000'
   else
     exit 1
   fi

}

# Power reboot
yadreboot () {

   if zenity --question --text "Reboot, are you sure"
   then
     poweroff --reboot
   else
     exit 1
   fi

}

# Power shutdown
yadshutdown () {

   if zenity --question --text "Shutdown, are you sure"
   then
     poweroff --poweroff
   else
     exit 1
   fi

}

#-------------------------------------------------------#
# Screenshots
#-------------------------------------------------------#

# View and open screenshots in rofi
view_screenshots() {

echo "${color26}${end}${space}shots ${color21}${space}view and open screenshots${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
choice=$(find "$screenshots_dir" -maxdepth 1 -iname '*.png' -printf '%T@ %p\n' | sort -rn | cut -f2- -d" " | \
awk -F/ '{print $NF}' | while read -r A ; do  echo -en "$A\x00icon\x1f~/$locationscreenshots/$A\n" ; done | \
rofi -dmenu -i -p '' -config "$rofi_config" -theme-str 'entry {placeholder : "Search screenshots...";}' -show-icons \
-mesg "${color14}open: ${color30}enter${end} | clip path: ${color30}alt+v${end} | file manager: ${color30}alt+c${end}${end}" \
-theme-str 'message {padding: 12px;}' \
-kb-custom-2 "alt+v" \
-kb-custom-3 "alt+c" \
-theme-str 'element-icon { size: 10em;}' \
-theme-str 'window {fullscreen: false; location: center; padding: 10px 10px 10px 10px;}' \
-theme-str 'element {orientation: vertical; }' \
-theme-str 'element.selected.normal {background-color: transparent; padding: 0px 0px 0px 0px; border: 2 2 2 2; border-color: #61AFEF;}' \
-theme-str 'element-text {horizontal-align: 0.5; text-color: #CAD3F5;}' \
-theme-str 'listview {dynamic: false; scrollbar: true; columns: 2; lines: 2; padding: 10px 10px 40px 10px;}')
exit_status=$?
[ -z "$choice" ] && exit 0

   if [[ $exit_status == "11" ]]; then
     echo "$HOME"/Pictures/screenshots/"$choice" | wl-copy -n
   elif [[ $exit_status == "12" ]]; then
     echo "$HOME"/Pictures/screenshots/"$choice" | xargs -0 -d '\n' "$file_manager"
   else
     image=$(printf '%s\n' "${screenshots_dir}${choice}")
     echo "$image" | xargs -0 -d '\n' "$image_viewer"
   fi
   
}

# View and open recent screenshots in rofi
recent_screenshots() {

echo "${color26}${end}${space}recent ${color21}${space}screenshots${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
choice=$(find "$screenshots_dir" -maxdepth 1 -type f -ctime 0 | \
awk -F/ '{print $NF}' | while read -r A ; do  echo -en "$A\x00icon\x1f~/$locationscreenshots/$A\n" ; done | sort | \
rofi -dmenu -i -p '' -config "$rofi_config" -theme-str 'entry {placeholder : "Search container";}' -show-icons -theme-str 'element-icon { size: 10em;}' \
-theme-str 'window {fullscreen: false; location: center; width: 1400px; padding: 0px 0px 0px 0px;}' \
-theme-str 'element.selected.normal {padding: 0px 0px 0px 0px; border: 0 0 0 0;}' \
-theme-str 'element-text {vertical-align: 0.5; }' \
-theme-str 'listview {dynamic: false; scrollbar: true; columns: 2; lines: 3; }')

   if [ "$choice" ]; then
     
     image=$(printf '%s\n' "${screenshots_dir}${choice}")
     echo "$image" | xargs -0 -d '\n' "$image_viewer"
   else
     echo "program terminated" && exit 0
   fi

}

# Check if camera click sound exists
check_sound() {

   if [ -f "$HOME"/Music/sounds/cameraclick.wav ]; then
     ffplay -autoexit -nodisp ~/Music/sounds/cameraclick.wav    
   else
     notify-send "Screenshot taken"     
   fi
	
}

# Take fullscreen screenshot
take_screenshot() {

echo "${color26}${end}${space}full ${color21}${space}screenshot${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
rm -f "$HOME"/Pictures/.tempfullscrns/*.png
sleep 1
grimshot save screen  ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png && check_sound 
cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/screenshots/

}

# Select screenshot
take_select_screenshot() {

echo "${color26}${end}${space}select ${color21}${space}screenshot${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
rm -f "$HOME"/Pictures/.tempfullscrns/*.png
sleep 1
grimshot save area  ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png && check_sound
cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/screenshots/

}

# Delay screenshot
take_delayed_screenshot() {
#notify-send "delay"
echo "${color26}${end}${space}delay ${color21}${space}screenshot${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
rm -f "$HOME"/Pictures/.tempfullscrns/*.png
sleep "$delay_time" && grimshot save screen ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png && check_sound
cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/screenshots/

}

# Open last created screenshot
ss_open() {

choice=$(find ~/Pictures/screenshots/ -type f \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")

"$image_viewer" "$choice"  
   
}

# Take select screenshot
ss_select() {

echo "${color26}${end}${space}select ${color21}${space}screenshots${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt	
rm -f "$HOME"/Pictures/.tempfullscrns/*.png
rm -f "$HOME"/.cvssgtk/*.png
sleep 1
maim --select --hidecursor | tee "$HOME/Pictures/.tempfullscrns/$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
convert "$HOME"/Pictures/.tempfullscrns/*.png -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/screenshots/
  
}

# Take 5 sec delayed fullscreen screenshot
ss_delay() {
	
rm -f "$HOME"/Pictures/.tempfullscrns/*.png
rm -f "$HOME"/.cvssgtk/*.png
sleep 1
maim --hidecursor --delay=5 | tee "$HOME/Pictures/.tempfullscrns/$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
convert "$HOME"/Pictures/.tempfullscrns/*.png -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/screenshots/
one=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
notify-send -i "$one" "Screenshot taken"
  
}

# Open last created screenshot in file manager
ss_dir() {
   
   if [[ -z "$(ls -A "$HOME"/Pictures/.tempfullscrns)" ]]; then
     notify-send "Directory is empty"
   else
     chosen=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -iname '*.*' | sort | cut -d '/' -f6-)
     [ -z "$chosen" ] && exit 0
     "$file_manager" "$HOME"/Pictures/screenshots/"$chosen"
   fi
	
}

# Send last sceeenshot notification image
ss_view() {

   if [[ -z "$(ls -A "$HOME"/Pictures/.tempfullscrns)" ]]; then
     notify-send "Directory is empty"
   else
     one=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     notify-send -i "$one" "Screenshot" "taken"	
   fi	
	
}

# Copy last created screenshot png to clipboard
ss_copy() {

notify-send "Does not work in wayland!"	
#xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"	
	
}

# Open last created screenshot in image editor
ss_gimp() {

   if [[ -z "$(ls -A "$HOME"/Pictures/.tempfullscrns)" ]]; then
     notify-send "Directory is empty"
   else
     temp_shot=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)	
     "$image_editor" "$temp_shot"
   fi	
	
}

#-------------------------------------------------------#
# Wallpapers
#-------------------------------------------------------#

# Sets random wallpaper and changes your autostart script
setrandomwall() {

:
#--------------- xorg xmonad only ---------------# 
#wfile="$HOME/Pictures/.fehwallrandom.txt"
#launch=$(ls "$screendir")
#image=$(echo "$launch" | shuf -n1)
#echo "${screendir}${image}" > "$wfile"
#xargs -L 1 -a "$wfile" -d '\n' feh --bg-scale
#sed -i "$lineset|^.*$|feh --bg-scale \"$(head "$wfile")\"|" "$myautostart"
#------------------------------------------------#

#--------------- sway only ---------------#
wall_file=$(ls ~/Pictures/sway_wallpapers/ | shuf -n1)
sed -i "5s|^.*$|output * bg ~/Pictures/sway_wallpapers/$(echo "$wall_file") fill|" "$my_swaywm_config"
sed -i "39s|^.*$|BackgroundImageFile=~/Pictures/terminal_background/$(echo "$wall_file")|" ~/.config/xfce4/terminal/terminalrc
swaymsg reload
#------------------------------------------------#

}

# Sets wallpaper and changes your autostart script
wallchanger() {

#--------------- sway only ---------------#

echo 'wallchanger' | tee ~/Documents/.last_sub_menu.txt
echo "${color26}${end}${space}wall ${color21}${space}wallpapers${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
choice=$(find "$screendir2" -maxdepth 1 -iname '*.*' -printf '%T@ %p\n' | sort -rn | cut -f2- -d" " | \
awk -F/ '{print $NF}' | while read -r A ; do  echo -en "$A\x00icon\x1f~/$locationwall2/$A\n" ; done | \
rofi -dmenu -i -p '' -config "$rofi_config" -theme-str 'entry {placeholder : "Search container";}' \
-show-icons -theme-str 'element-icon { size: 10em;}' \
-theme-str 'window {fullscreen: false; location: center; padding: 0px 0px 0px 0px;}' \
-theme-str 'element {orientation: vertical; }' \
-theme-str 'element.selected.normal {background-color: transparent; padding: 0px 0px 0px 0px; border: 2 2 2 2; border-color: #61AFEF;}' \
-theme-str 'element-text {horizontal-align: 0.5; text-color: #CAD3F5;}' \
-theme-str 'listview {dynamic: false; scrollbar: true; columns: 2; lines: 2; }')

   # Changes your current wallpaper and your autostart script.
   if [ "$choice" ]; then
     wlfile="$HOME/Pictures/.swaywall.txt"
     image=$(printf '%s\n' "${choice}")
     echo "$image" | tee "$wlfile"
     sed -i "$lineset2|^.*$|output * bg ~/Pictures/sway_wallpapers/$(head "$wlfile") fill|" "$my_swaywm_config"
     sed -i "39s|^.*$|BackgroundImageFile=~/Pictures/terminal_background/$(head "$wlfile")|" ~/.config/xfce4/terminal/terminalrc
     swaymsg reload
   else
     echo "program terminated" && exit 0
   fi
   
#------------------------------------------------#

#--------------- xorg xmonad only ---------------#

#echo "${color26}${end}${space}wall ${color21}${space}wallpapers${end}${color20}${space}${end}" | \
#tee "$HOME"/Documents/.keyword_last_used.txt
#choice=$(find "$screendir" -maxdepth 1 -iname '*.*' -printf '%T@ %p\n' | sort -rn | cut -f2- -d" " | \
#awk -F/ '{print $NF}' | while read -r A ; do  echo -en "$A\x00icon\x1f~/$locationwall/$A\n" ; done | \
#rofi -dmenu -i -p '' -config "$rofi_config" -theme-str 'entry {placeholder : "Search container";}' \
#-show-icons -theme-str 'element-icon { size: 16em;}' \
#-theme-str 'window {fullscreen: true; location: center; padding: 0px 0px 0px 0px;}' \
#-theme-str 'element {orientation: vertical; }' \
#-theme-str 'element.selected.normal {padding: 0px 0px 0px 0px; border: 0 0 0 0;}' \
#-theme-str 'element-text {horizontal-align: 0.5; }' \
#-theme-str 'listview {dynamic: false; scrollbar: true; columns: 2; lines: 2; }')

   # Changes your current wallpaper and your autostart script.
 #  if [ "$choice" ]; then
  #   wfile="$HOME/Pictures/.fehwall.txt"
   #  image=$(printf '%s\n' "${screendir}${choice}")
    # echo "$image" > "$wfile"
     #xargs -L 1 -a "$wfile" -d '\n' feh --bg-scale
     #sed -i "$lineset|^.*$|feh --bg-scale \"$(head "$wfile")\"|" "$myautostart"
   #else
    # echo "program terminated" && exit 0
   #fi
   
#------------------------------------------------#

}

# Wallpaper main menu
walloptions() {

choice=$(echo -e "Set wallpaper\nRandom wallpaper" | _rofi35)
echo "$choice"

   if [[ "$choice" == *"Set"* ]]; then
     wallchanger
   fi

   if [[ "$choice" == *"Random"* ]]; then
     setrandomwall
   fi

}

#-------------------------------------------------------#
# Radio
#-------------------------------------------------------#

# Select and play radio using text file containing url's
online_radio() {

echo "${color26}${end}${space}radio ${color21}${space}plays radio${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
textfile=$(cat "$HOME"/Documents/mainprefix/radiolinks.txt | sort)
launcher=$(echo "$textfile" | _rofi18)
exit_status=$?
[[ -z "$launcher" ]] && exit 0

   if [[ $exit_status == "11" ]]; then
     echo "$launcher" > /dev/null
     rofi_actions_radio
   elif [[ "$(pidof mpv)" ]]; then
     killall mpv ; killall ffplay ; echo "$launcher" | awk '{print $NF}' | tee "$HOME"/Documents/.current_url.txt
     cat "$HOME"/Documents/.current_url.txt > ~/Documents/.last_radio_url
     url_stream=$(cat "$HOME"/Documents/.current_url.txt)
     "$other_video_player" "$url_stream"  
   else
     killall mpv
     killall ffplay
     echo "$launcher" | tee "$HOME"/Documents/.current_url.txt
     cat "$HOME"/Documents/.current_url.txt > ~/Documents/.last_radio_url
     echo "$launcher" | awk '{print $NF}' | xargs -0 -d '\n' "$other_video_player"
   fi

}

# Currently playing track
radio_status() {

   if [[ "$(pidof mpv)" ]]; then
      echo ""
      echo "${color7}${space}$(playerctl --player mpv metadata -f "{{title}}")${end}"
   else
     echo ""
     echo "Not playing"
   fi	
	
}

# Recording radio streams with mpv
recordradiompv () {

rm -f "$HOME"/Music/.temprecfile/*.*
radiotrack=$(playerctl --player mpv metadata -f "{{xesam:url}}")
killall ffplay
killall mpv
echo "on" | tee "$HOME"/Documents/.mpv_recording.txt
mpv --stream-record="$HOME/Music/.temprecfile/$(date +'%d-%m-%Y-%H%M%S').mp4" "$radiotrack"
cp "$HOME"/Music/.temprecfile/*.* "$HOME"/Music/radio_recordings/

}

# Play recordings
playrecfg () {

killall mpv
playectemp=$(find "$HOME"/Music/.temprecfile/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
ffplay "$playectemp" -autoexit -nodisp

}

# Stop mpv and ffplay
stopmpv () {

   if pgrep -x "mpv" > /dev/null
   then
     killall mpv
     echo "off" > "$HOME"/Documents/.mpv_recording.txt
   elif pgrep -x "ffplay" > /dev/null
   then
     killall ffplay
   else
     echo "Stopped"
   fi

}

# Open up text file
ra_txt() {
	
"$texteditor" "$HOME"/Documents/mainprefix/radiolinks.txt	
	
}

# Remove whitespace
ra_space() {

sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/radiolinks.txt	
	
}

# Send track title to clipboard
ra_clip() {

playerctl --player mpv metadata -f "{{title}}" | wl-copy -n
	
}

# Send track title to notify-send
ra_notify() {

playerctl --player mpv metadata -f "{{title}}" | xargs -d '\n' notify-send	
	
}

# Create qr code from radio track title
ra_qrcode() {
	
rm -f "$HOME"/Pictures/.temqrcodes/*.png
playerctl --player mpv metadata -f "{{title}}" | tr -d '\n' | \
qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/radio_$(date '+%d-%m-%Y-%H-%M-%S')".png
cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
one3=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
notify-send -i "$one3" "Created" "Qr Code"	
	
}

# Type radio link to send to text file
ra_out() {

choice=$(echo "" | _rofi2)	
checkbmfile2="$HOME/Documents/mainprefix/radiolinks.txt"
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/radiolinks.txt
grep -q "$choice" "$checkbmfile2" || printf '%s\n' "$choice" >> "$HOME"/Documents/mainprefix/radiolinks.txt	
	
}

# Volume 50%
ra_vol50() {

playerctl --player mpv volume 0.5%	
	
}

# Volume 70%
ra_vol70() {

playerctl --player mpv volume 0.7%	
	
}

# Volume 100%
ra_vol100() {

playerctl --player mpv volume 1%	
	
}

# Check mpv status
mpv_check() {
	
   if [[ "$(cat "$HOME"/Documents/.mpv_recording.txt)" == 'on' ]]; then
     echo "Recording"
   elif [[ "$(playerctl status -p mpv)" == 'Playing' ]]; then
     echo "Playing"
   elif	[[ "$(playerctl status -p mpv)" == 'Paused' ]]; then
	 echo "Paused"
   elif	[[ "$(pidof ffplay)" ]]; then
     echo "Playback"
   else
     echo "Not Playing"
   fi  
     
}

#-------------------------------------------------------#
# Audacious
#-------------------------------------------------------#

nowplaylist() {

list=$(find "${musicdir}" -iname \*.m4a -o -iname \*.mp3 -o -iname \*.wma -o -iname \*.wav)
choice=$(printf '%s\n' "${list[@]}" | \
sort | \
cut -d '/' -f6- | \
_rofi4) || exit

   if [ "$choice" ]; then
     image=$(printf '%s\n' "${choice}")
     audacious -H "${musicdir}"/"$image"
   else
     printf '%s\n' "program terminated" && exit 0
   fi

}

# Stop
au_stop() {
	
audacious --stop
	
}

# Next
au_next() {
	
audacious --fwd	
	
}

# Prev
au_prev() {
	
audacious --rew

}

# Play (toggle)
au_play() {

echo "${color26}${end}${space}au play-pause ${color21}${space}audacious player${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt	
audacious -H --play-pause

}

# Playlist 1
au_p1() {

audtool --set-current-playlist 1

}

# Playlist 2
au_p2() {
	
audtool --set-current-playlist 2

}

# Playlist 3
au_p3() {
	
audtool --set-current-playlist 3

}

# Playlist 4
au_p4() {
	
audtool --set-current-playlist 4

}

# Playlist 5
au_p5() {
	
audtool --set-current-playlist 5

}

# Seek 0%
au_s0() {
	
audtool --playback-seek 0

}

# Seek 25%
au_s25() {
	
audtool --playback-seek 25

}

# Seek 50%
au_s50() {
	
audtool --playback-seek 50

}

# Seek 75%
au_s75() {
	
audtool --playback-seek 75

}

# Volume 100%
au_vol_100() {
	
audtool --set-volume 100

}

# Volume 100%
au_vol_50() {
	
audtool --set-volume 50

}


# Kill audacious
au_kill() {
	
killall audacious

}

# Current playlist
au_playlist() {
	
notify-send "$(audtool --current-playlist)"

}	

# Playback status
au_status() {
	
notify-send "$(audtool --playback-status)"

}

# Repeat
au_repeat() {
	
audtool --playlist-repeat-toggle

}

# Shuffle
au_shuffle() {
	
audtool --playlist-shuffle-toggle

}

# Current song
au_current() {
	
notify-send "$(audtool --current-song)"

}

#-------------------------------------------------------#
# Configs
#-------------------------------------------------------#

# open configs
system_configs() {

echo "${color26}${end}${space}configs ${color21}${space}system configs${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
launcher4=$(_rofi29 < "$HOME"/Documents/mainprefix/configs.txt)
exit_status=$?
[[ -z "$launcher4" ]] && exit

   if [[ $exit_status == "11" ]]; then
     "$myterminal" -- /bin/bash -c "$terminal_editor $launcher4; $SHELL"
   elif [[ $exit_status == "12" ]]; then
     sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/configs.txt
     choice=$(echo "" | _rofi2)	
     checkbmfile="$HOME/Documents/mainprefix/configs.txt"
     grep -q "$choice" "$checkbmfile" || printf '%s\n' "$choice" >> "$HOME"/Documents/mainprefix/configs.txt
   elif [[ $exit_status == "13" ]]; then
     "$texteditor" "$HOME/Documents/mainprefix/configs.txt"
   else
     echo "$launcher4" | awk '{print $NF}' | xargs -r "$texteditor"     
   fi

}

#-------------------------------------------------------#
# Web searches
#-------------------------------------------------------#

# Google search
web_google() {
	
echo "${color26}${end}${space}go ${color21}${space}google web search${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://www.google.co.uk/search?q={}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
 
}

# Google search
google_recent() {
	
search="https://www.google.co.uk/search?q={}"
tail -n 1 "$HOME"/Documents/mainprefix/.last_web_search.txt | sed 's|&||' | xargs -I{} "$browser" "$search"

}

# Google images
google_images() {

truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://www.google.com/search?hl=en&tbm=isch&q={}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
   	
}

# Google images recent search
google_images_recent() {

search="https://www.google.com/search?hl=en&tbm=isch&q={}"
tail -n 1 "$HOME"/Documents/mainprefix/.last_web_search.txt | sed 's|&||' | xargs -I{} "$browser" "$search"	
	
}

# Collings
web_collins() {

truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://www.collinsdictionary.com/dictionary/english/{}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
  	
}

# Duckduckgo
duck_duck_go() {

truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://duckduckgo.com/?q={}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
		
}

# Duckduckgo recent
duckduckgo_recent() {

search="https://duckduckgo.com/?q={}"
tail -n 1 "$HOME"/Documents/mainprefix/.last_web_search.txt | sed 's|&||' | xargs -I{} "$browser" "$search"	

}

# Youtube
web_youtube() {

truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://www.youtube.com/results?search_query={}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
	
}

# Youtube recent
youtube_recent() {

search="https://www.youtube.com/results?search_query={}"
tail -n 1 "$HOME"/Documents/mainprefix/.last_web_search.txt | sed 's|&||' | xargs -I{} "$browser" "$search"	

}

# Wikipedia
web_wiki() {
	
truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://en.wikipedia.org/wiki/{}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
  
}

# Arch wiki
arch_wiki() {

truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://wiki.archlinux.org/index.php?search={}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
   	
}

# Google maps
google_maps() {

truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://www.google.com/maps/search/{}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
  
}

# Reddit
reddit() {

truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://www.reddit.com/search/?q={}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
   			
}

# Reddit recent
reddit_recent() {
	
search="https://www.reddit.com/search/?q={}"
tail -n 1 "$HOME"/Documents/mainprefix/.last_web_search.txt | sed 's|&||' | xargs -I{} "$browser" "$search"

}

# Google news
google_news() {

truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://news.google.com/search?q={}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
   		
}

# Amazon search
amazon() {
 
truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://www.amazon.co.uk/s?k={}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
   		
}

# Stackoverflow
stackoverflow() {
	
truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://stackoverflow.com/search?q={}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
	
}

# Imbd
imdb() {

truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://www.imdb.com/find?q={}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
   
}

# Translate english to french
google_trans_fr() {
	  
truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://translate.google.com/?sl=auto&tl=fr&text={}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
   	
}

# Translate english to spanish
google_trans_es() {
 
truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://translate.google.com/?sl=auto&tl=es&text={}"
echo "" | _rofi11 | tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
   		
}

#-------------------------------------------------------#
# Bookmarks
#-------------------------------------------------------#

# Open bookmarks with firefox
open_firefox() {
	
bookcheck=$(cat "$HOME"/Documents/mainprefix/bookmarks.txt)
booklist=$(echo "$bookcheck" | sort | _rofi4)
echo "$booklist" | awk '{print $NF}' | xargs -r firefox	
	
}

# Open up bookmarks in text file
open_bookmarks() {

echo "${color26}${end}${space}bookmarks ${color21}${space}bookmarks${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
bookcheck=$(cat "$HOME"/Documents/mainprefix/bookmarks.txt)
booklist=$(echo "$bookcheck" | sort | sed 's|&|+|g' | _rofi21)
exit_status=$?
[[ -z "$booklist" ]] && exit 0

   if [[ $exit_status == "11" ]]; then
   choice=$(echo "$booklist" | sed 's|+|&|g' | awk '{print $NF}')
   echo "$choice" | tee "$HOME"/Documents/.actions_bookmarks.txt
   rofi_actions_bookmarks
   elif [[ $exit_status == "12" ]]; then
   echo "$booklist"
   rofi_actions_bookmarks
   else
   choice=$(echo "$booklist" | sed 's|+|&|g' | awk '{print $NF}')
   [[ -z "$choice" ]] && exit 0
   launch=$(echo "$choice" | tee "$HOME"/Documents/.actions_bookmarks.txt)
   [[ -z "$launch" ]] && exit 0
   echo "$launch" | xargs "$browser"
   fi
   
}

# Remove whitespace
bo_space() {
	
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/bookmarks.txt	
	
}

# Open bookmarks text file
bo_txt() {
	
"$texteditor" "$HOME/Documents/mainprefix/bookmarks.txt"	
	
}

# Type url to send to text file
bo_out() {

choice=$(echo "" | _rofi2)	
checkbmfile="$HOME/Documents/mainprefix/bookmarks.txt"
grep -q "$choice" "$checkbmfile" || printf '%s\n' "$choice" >> "$HOME"/Documents/mainprefix/bookmarks.txt	
	
}

# Copy bookmark to clipboard
bo_clip() {

choice=$(cat "$HOME/Documents/mainprefix/bookmarks.txt" | _rofi2)	
echo "$choice" | awk '{print $NF}'| wl-copy
	
}

#-------------------------------------------------------#
# File manager
#-------------------------------------------------------#

# Show last file selected by finder
action_last_filemanager() {

cat "$HOME"/Documents/.temp_file_actions.txt
  	
}

# Menu for actions
menu4() {

echo "open ${color6}opens the file${end}"
echo "file ${color6}opens file in file manager${end}"
echo "copy ${color6}copies the file${end}"
echo "move ${color6}moves the file${end}"	
echo "none ${color6}perform no action${end}"
			
}

# Perform actions on files
file_manager_actions() {
	
choice=$(menu4 | rofi -dmenu -i -p '' -config "$rofi_config" -mesg "$(action_last_filemanager)" -markup-rows \
-theme-str 'message {padding: 20px;}' | awk '{print $1}')
[[ -z "$choice" ]] && exit
	
case "$choice" in

 'copy')
   actions_file=$(cat "$HOME"/Documents/.temp_file_actions.txt)
   dir_choice=$(find "${dirs[@]}" -maxdepth 9 -type d | rofi -dmenu -i -p '' -config "$rofi_config")
   echo "$dir_choice" | tee "$HOME"/Documents/.dir_actions.txt
   cp "$actions_file" "$dir_choice"
   ;;
 'open')
   cat "$HOME"/Documents/.temp_file_actions.txt | xargs -0 -d '\n' xdg-open
   ;;
  
 'move')
   actions_file=$(cat "$HOME"/Documents/.temp_file_actions.txt)
   dir_choice=$(find "${dirs[@]}" -maxdepth 9 -type d | rofi -dmenu -i -p '' -config "$rofi_config")
   echo "$dir_choice" | tee "$HOME"/Documents/.dir_actions.txt
   mv "$actions_file" "$dir_choice"
   ;;
   
 'file')
   cat "$HOME"/Documents/.temp_file_actions.txt | xargs -0 -d '\n' "$file_manager"
   ;;  
 
 'none')
   :
   ;;
      
  *)
  echo "Something went wrong!" | rofi2 || exit 1
  ;;

esac	
	
}

# File manager
rofifilemanager() {

echo "${color26}${end}${space}file-manager ${color21}${space}rofi file manager${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt

     select=1
 while [ "$select" ]; do
     choice=$(ls -1a --group-directories-first)
     select="$(echo "$choice" | rofi -dmenu -i -p '' -config "$rofi_config" -kb-custom-2 "alt+c" \
     -theme-str 'message {padding: 20px;}' \
     -mesg "${color13}${space}open: ${color30}enter${end} | file action: ${color30}alt+c${end}${end}" \
     -theme-str 'entry {vertical-align: 0.8; placeholder: "File search..."; padding:  0px 10px;}' -l 14 -p "$(basename "$(pwd)")")"
     exit_status=$?
     
   if [[ -f "$select" && $exit_status = *"11"* ]]; then
     echo "$select" | tee "$HOME"/Documents/.temp_file_actions.txt 
     file_manager_actions && exit
   elif [[ -f "$select" ]]; then
     xdg-open "$select" && exit
   elif [[ -d "$select" ]]; then
     cd "$select" || exit
   else
     :
   fi
 done

}

#-------------------------------------------------------#
# Window switcher for sway
#-------------------------------------------------------#
# Contributors: shapeoflambda, kristoferus75, john mcgrath

window_switcher() {

echo "${color26}${end}${space}W ${color21}${space}window switcher${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt

# Get regular windows
regular_windows=$(swaymsg -t get_tree | jq -r '.nodes[1].nodes[].nodes[] | .. | (.id|tostring) + " " + .name?' | grep -e "[0-9]* ."  )

# Get floating windows
floating_windows=$(swaymsg -t get_tree | jq '.nodes[1].nodes[].floating_nodes[] | (.id|tostring) + " " + .name?'| grep -e "[0-9]* ." | tr -d '"')

enter=$'\n'
if [[ $regular_windows && $floating_windows ]]; then
  all_windows="$regular_windows$enter$floating_windows"
elif [[ $regular_windows ]]; then
  all_windows=$regular_windows
else
  all_windows=$floating_windows
fi

# Select window with rofi
selected=$(echo "$all_windows" | rofi -dmenu -i -config "$rofi_config" -p "" \
-theme-str 'listview {lines: 8; columns: 1;}' | \
awk '{print $1}')
[[ -z "$selected" ]] && exit 0

# Tell sway to focus said window
swaymsg [con_id="$selected"] focus

}

#-------------------------------------------------------#
# Bash
#-------------------------------------------------------#

# view and open bash scripts (gui)
bash_scripts_gui() {

echo "${color26}${end}${space}bin ${color21}${space}open scripts${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
choice=$(ls "$bash_dir")
launcher=$(echo "$choice" | _rofi7)
[ -z "$launcher" ] && exit 0
"$texteditor" "$bash_dir/$launcher"

}

# view and open bash scripts (cli)
bash_scripts_cli() {

choice=$(ls "$bash_dir")
launcher=$(echo "$choice" | _rofi7)
[ -z "$launcher" ] && exit 0
"$myterminal" -- /bin/bash -c "$terminal_editor $bash_dir/$launcher; $SHELL"

}

# exec bash scripts within the last 24 hours
exec_scripts() {

echo "${color26}${end}${space}E ${color21}${space}exec bash scripts created within 24 hours${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
launcher=$(find "$bash_dir" -maxdepth 5 -type f -mtime -1 | sort | xargs -n 1 basename | _rofi7)
[ -z "$launcher" ] && exit 0
/bin/bash -c "$bash_dir"/"$launcher"

}

# Exec select bash script
te_exec() {
	
choice=$(ls "$bash_dir")
launcher=$(echo "$choice" | _rofi7)
[ -z "$launcher" ] && exit 0
/bin/bash -c "$bash_dir"/"$launcher"

}

# Create bash script
te_out() {
 
choice=$(echo "" | _rofi2)	
bashfile="$bash_dir/$choice".sh
echo "#!/bin/bash" >> "$bashfile"
chmod u+x "$bashfile"
"$texteditor" "$bashfile"
     
}

#-------------------------------------------------------#
# Color picker
#-------------------------------------------------------#

hex_codes() {

echo "${color26}${end}${space}hex-codes ${color21}${space}color codes (hex and rgb)${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
screendir="$HOME/Pictures/rofi_colors/"
choice=$(find ~/Pictures/rofi_colors/ -maxdepth 1 -iname '*.png' -printf '%T@ %p\n' | sort -rn | cut -f2- -d" " | \
awk -F/ '{print $NF}' | while read -r A ; do  echo -en "$A\x00icon\x1f~/Pictures/rofi_colors/$A\n" ; done | sed 's/\.png//' | \
rofi -dmenu -i -p '' -config "$rofi_config" \
-mesg "${color13}${space}clip hex: ${color30}enter${end} | clip rgb: ${color30}alt+c${end} | add hex: ${color30}alt+n${end} | picker: ${color30}alt+p${end} | delete: ${color30}alt+d${end}${end}" \
-kb-custom-2 "alt+c" \
-kb-custom-3 "alt+p" \
-kb-custom-4 "alt+n" \
-kb-custom-5 "alt+d" \
-theme-str 'element-text {horizontal-align: 0.5; text-color: #CAD3F5;}' \
-theme-str 'element-icon { size: 8em;}' \
-theme-str 'entry { placeholder: "color code search...";}' \
-theme-str 'message { margin: -20px 0px 0px 0px; border: 0px 0px 0px 0px; padding: 10px 10px 10px 10px; border-color: #000000; border-radius: 0px;}' \
-theme-str 'window {width: 55%; padding: 0px 0px 0px 0px;}' \
-theme-str 'element.selected.normal {background-color: transparent; padding: 0px 0px 0px 0px; border: 2 2 2 2; border-color: #61AFEF;}' \
-theme-str 'element { highlight: none; orientation: vertical; }' \
-theme-str 'listview {lines: 2; columns: 4; padding: 50px;}')
exit_status=$?

   if [[ $exit_status == "11" ]]; then
     image=$(printf '%s\n' "${screendir}${choice}" | cut -d '/' -f6- | sed 's|#||')
     [ -z "$image" ] && exit 0
     hex="$image"
     printf "%d %d %d\n" 0x"${hex:0:2}" 0x"${hex:2:2}" 0x"${hex:4:2}" | wl-copy -n
   elif [[ $exit_status == "12" ]]; then
     grim -g "$(slurp -p)" -t ppm - | convert - -format '%[pixel:p{0,0}]' txt:- | \
     tail -n 1 | awk '{print $3}' | tee ~/Pictures/.temp_hex_code.txt
     code_text_file=$(cat ~/Pictures/.temp_hex_code.txt)
     convert -size 200x200 xc:"$code_text_file" "$HOME/Pictures/rofi_colors/$code_text_file".png && notify-send "Hex color added!"	
   elif [[ $exit_status == "13" ]]; then
     add_hex_rofi     
   elif [[ $exit_status == "14" ]]; then
     image=$(printf '%s\n' "${screendir}${choice}")
     rm "$image".png || exit 1
   else
     image=$(printf '%s\n' "${screendir}${choice}")
     echo "$image" | cut -d '/' -f6- | wl-copy -n
   fi	
		
}

# Add hex code by typing hex code
add_hex_rofi() {

echo 'add_hex_rofi' | tee ~/Documents/.last_sub_menu.txt
choice=$(echo "" | _rofi39)
[ -z "$choice" ] && exit 0	
convert -size 200x200 xc:"$choice" "$HOME/Pictures/rofi_colors/$choice".png && notify-send "Hex color added!"

}

# Color picker from root search
root_pick() {

gpick -o -s | tee "$HOME"/Documents/.pick_color.txt
color_file=$(cat "$HOME"/Documents/.pick_color.txt)
convert -size 200x200 xc:"$color_file" "$HOME/Pictures/rofi_colors/$color_file".png && notify-send "Hex color added!"	
cat "$HOME"/Documents/.pick_color.txt | wl-copy	
	
}

#-------------------------------------------------------#
# Sticky note (xorg only)
#-------------------------------------------------------#

# Show number of characters and words
note_count() {
	
choice_char=$(cat "$HOME"/Documents/.yadnote.txt | wc -m)
choice_word=$(cat "$HOME"/Documents/.yadnote.txt | wc -w)

echo "${color8}characters:${end} $choice_char ${color9}words:${end} $choice_word"

}
export -f note_count

# Clear note
clear_note() {

tee "$HOME"/Documents/.yadnote.txt
notify-send "Note cleared! refresh note"
	
}
export -f clear_note	

# Copy sticky note contents to clipboard
sticky_copy_note() {

xclip -sel c < "$HOME"/Documents/.yadnote.txt
		
}
export -f sticky_copy_note

# Show main note
show_note() {

gawk -i inplace '!seen[$0]++' "$HOME"/Documents/.yadnote.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.yadnote.txt
launch=$(sort -u "$HOME"/Documents/.yadnote.txt)
choice=$(echo -en "$launch" | GTK_THEME="alt-dialog16" yad --text-info --show-uri \
--text="$(note_count)" \
--text-align="center" \
--borders=10 \
--buttons-layout="center" \
--width=500 --height=200 --center --editable \
--back=#20252C --fore=#ffffff --uri-color=#46FF23 --fontname="Monospace 11" --wrap \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='30'></span>:/bin/bash -c 'clear_note'" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='20'></span>:/bin/bash -c 'sticky_copy_note'" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='20'></span>:/bin/bash -c 'killall yad'" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='20'></span>:0" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='20'></span>:1")

gawk -i inplace '!a[$0]++' "$HOME"/Documents/.yadnote.txt ; echo "$choice" | tee -a "$HOME"/Documents/.yadnote.txt


}
export -f show_note

#-------------------------------------------------------#
# Emoji
#-------------------------------------------------------#

emojilist() {

echo "${color26}${end}${space}emoji ${color21}${space}copy to clipboard${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
fileoutone=$(cat "$HOME"/Documents/mainprefix/emoji-data.txt)
chosen=$(echo "$fileoutone" | rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'window {width: 55%;}' \
-theme-str 'listview {lines: 8; columns: 2;}' \
-theme-str 'entry { placeholder: "Emoji search...";}' | awk '{print $1}')
[ -z "$chosen" ] && exit

   if [ -n "${1-}" ]; then
     xdotool type "$chosen"
   else
     printf '%s' "$chosen" | wl-copy -n
     notify-send "'$chosen' copied to clipboard"
   fi

}

#-------------------------------------------------------#
# Volume
#-------------------------------------------------------#

# Send volume status to notification daemon
sendnotifyvol() {

# Arbitrary but unique message tag
msgTag="myvolume"

# Change the volume using alsa(might differ if you use pulseaudio)
amixer -D pulse "$@" > /dev/null

# Query amixer for the current volume and whether or not the speaker is muted
volume="$(amixer -D pulse get Master | tail -1 | awk '{print $5}')"
mute="$(amixer -D pulse get Master | tail -1 | awk '{print $6}')"

   if [[ $volume == 0 || "$mute" == "[off]" ]]; then
     # Show the sound muted notification
     dunstify -a "changeVolume" -u low -i audio-volume-muted -h string:x-dunst-stack-tag:$msgTag "Volume muted"
   else
     # Show the volume notification
     dunstify -a "changeVolume" -u low -i audio-volume-high -h string:x-dunst-stack-tag:$msgTag \
     -h int:value:"$volume" "Volume: ${volume}%"
   fi

# Play the volume changed sound
canberra-gtk-play -i audio-volume-change -d "changeVolume"

}

# Volume up 
vol_plus() {
	
amixer -D pulse sset Master 10%+	
	
}

# Volume down
vol_minus() {

amixer -D pulse sset Master 10%-	
	
}

# Volume mute toggle
vol_mute() {
	
amixer -D pulse set Master 1+ toggle	
	
}

# Volume 100%
vol_100() {
	
amixer -D pulse sset Master 100%	
	
}

# Volume 70%
vol_70() {
	
amixer -D pulse sset Master 70%	
	
}

# Volume 50%
vol_50() {
	
amixer -D pulse sset Master 50%	
	
}

#-------------------------------------------------------#
# Time zones
#-------------------------------------------------------#

time_zones() {

echo "${color26}${end}${space}zones ${color21}${space}timezones${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt

# Timezone commands

zoneone=$(TZ=America/Los_Angeles date)
zonetwo=$(TZ=America/New_York date)
zonethree=$(TZ=Australia/Sydney date)
zonefour=$(TZ=Asia/Tokyo date)
zonefive=$(TZ=Europe/London date)
zonesix=$(TZ=Asia/Jerusalem date)

# Only text output

textone="America/Los Angeles"
texttwo="America/New York"
textthree="Australia/Sydney"
textfour="Japan/Tokyo"
textfive="Europe/London"
textsix="Asia/Jerusalem"

# Menu

com01=$(echo -e "${zoneone}${space} ${textone}")
com02=$(echo -e "${zonetwo}${space} ${texttwo}")
com03=$(echo -e "${zonethree}${space} ${textthree}")
com04=$(echo -e "${zonefour}${space} ${textfour}")
com05=$(echo -e "${zonefive}${space} ${textfive}")
com06=$(echo -e "${zonesix}${space} ${textsix}")

choice=$(echo -e "$com01\n$com02\n$com03\n$com04\n$com05\n$com06" | _rofi36)
[ -z "$choice" ] && exit 0
echo "$choice" | wl-copy -n

}

#-------------------------------------------------------#
# Convert images files
#-------------------------------------------------------#

# Main
convert_files() {

wl-paste > "$HOME"/Pictures/.temp_resize_image.txt
sed -i 's|file:\//||' "$HOME"/Pictures/.temp_resize_image.txt
cat "$HOME"/Pictures/.temp_resize_image.txt | xargs | tr -d '\r' | \
sed 's|%20| |g' | tee "$HOME"/Pictures/.temp_resize_image.txt
echo "${color26}${end}${space}convert ${color21}${space}convert images, saves images to pictures${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
launcher=$(find "$HOME"/Pictures/converted_preview_images/ -maxdepth 1 -iname '*.*' | \
awk -F/ '{print $NF}' | while read -r A ; do  echo -en "$A\x00icon\x1f~/Pictures/converted_preview_images/$A\n" ; done | sort | \
sed 's/\.png//' | rofi -dmenu -i -p '' -config "$rofi_config" -theme-str 'entry {placeholder : "Search convert...";}' -show-icons \
-mesg "$(cat "$HOME"/Pictures/.temp_resize_image.txt)" \
-theme-str 'element-icon { size: 10em;}' \
-theme-str 'element.selected.normal {background-color: transparent; padding: 0px 0px 0px 0px; border: 2 2 2 2; border-color: #61AFEF;}' \
-theme-str 'element-text {horizontal-align: 0.5; text-color: #CAD3F5;}' \
-theme-str 'window {fullscreen: false; location: center; padding: 10px 10px 10px 10px;}' \
-theme-str 'element {orientation: vertical; }' \
-theme-str 'listview {dynamic: false; scrollbar: true; columns: 2; lines: 2;}' | sed 's|.png||')
[ -z "$launcher" ] && exit 0

   choice=$(cat "$HOME"/Pictures/.temp_resize_image.txt)
   check_image=$(file --mime-type "$(cat "$HOME"/Pictures/.temp_resize_image.txt)" | awk -F ':' '{print $NF}' | sed 's| ||')
   if [[ "$launcher" == 'greyscale' && "$check_image" == 'image/png' || "$check_image" == 'image/jpeg' ]]; then
     convert "$choice" -set colorspace Gray -separate -average "$HOME/Pictures/greyscale_$(date +'%d-%m-%Y-%H%M%S')".png
     notify-send "converted image $choice"
   elif [[ "$launcher" == 'negate' && "$check_image" == 'image/png' || "$check_image" == 'image/jpeg' ]]; then
     convert "$choice" -channel RGB -negate "$HOME/Pictures/negate_$(date +'%d-%m-%Y-%H%M%S')".png
     notify-send "converted image $choice"
   elif [[ "$launcher" == 'blur' && "$check_image" == 'image/png' || "$check_image" == 'image/jpeg' ]]; then
     convert "$choice" -blur 0x5 "$HOME/Pictures/blur_$(date +'%d-%m-%Y-%H%M%S')".png
     notify-send "converted image $choice"
   elif [[ "$launcher" == 'sepia' && "$check_image" == 'image/png' || "$check_image" == 'image/jpeg' ]]; then
     convert "$choice" -sepia-tone 80% "$HOME/Pictures/sepia_$(date +'%d-%m-%Y-%H%M%S')".png
     notify-send "converted image $choice"
   elif [[ "$launcher" == 'thumbnail' && "$check_image" == 'image/png' || "$check_image" == 'image/jpeg' ]]; then
     convert "$choice" -thumbnail 1280x720 "$HOME/Pictures/thumbnail_$(date +'%d-%m-%Y-%H%M%S')".png
     notify-send "converted image $choice"
   elif [[ "$launcher" == 'emboss' && "$check_image" == 'image/png' || "$check_image" == 'image/jpeg' ]]; then
     convert "$choice" -emboss 10 "$HOME/Pictures/emboss_$(date +'%d-%m-%Y-%H%M%S')".png
     notify-send "converted image $choice"
   elif [[ "$launcher" == 'vignette' && "$check_image" == 'image/png' || "$check_image" == 'image/jpeg' ]]; then
     convert "$choice" -background black -vignette 0x30+10+10 "$HOME/Pictures/vignette_$(date +'%d-%m-%Y-%H%M%S')".png
     notify-send "converted image $choice"
   elif [[ "$launcher" == 'black border' && "$check_image" == 'image/png' || "$check_image" == 'image/jpeg' ]]; then
     convert "$choice" -bordercolor black -border 20 "$HOME/Pictures/black_border_$(date +'%d-%m-%Y-%H%M%S')".png
     notify-send "converted image"
   elif [[ "$launcher" == 'rotate minus' && "$check_image" == 'image/png' || "$check_image" == 'image/jpeg' ]]; then
     convert "$choice" -rotate -90 "$HOME/Pictures/minus_$(date +'%d-%m-%Y-%H%M%S')".png
     notify-send "converted image $choice"
   elif [[ "$launcher" == 'rotate plus' && "$check_image" == 'image/png' || "$check_image" == 'image/jpeg' ]]; then
     convert "$choice" -rotate 90 "$HOME/Pictures/plus_$(date +'%d-%m-%Y-%H%M%S')".png
     notify-send "converted image $choice"
   elif [[ "$launcher" == 'resize' && "$check_image" == 'image/png' || "$check_image" == 'image/jpeg' ]]; then
     convert_resize
   elif [[ "$launcher" == 'png to jpg' && "$check_image" == 'image/png' || "$check_image" == 'image/jpeg' ]]; then
     convert "$choice" "$HOME/Pictures/plus_$(date +'%d-%m-%Y-%H%M%S')".jpg
     notify-send "converted image $choice"
   else
     notify-send "Not an image file or not supported image type!"
   fi

}

# Inputs image resize width and height
convert_resize() {

echo 'convert_resize' | tee ~/Documents/.last_sub_menu.txt
pick_choice=$(echo "" | rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'listview {dynamic: false; scrollbar: true; columns: 1; lines: 2;}' \
-mesg "$(cat ~/Pictures/.temp_resize_image.txt)" \
-theme-str 'entry {placeholder : "format 1920x1080 or 1920x keep aspect ratio"; placeholder-color: grey;}')
[ -z "$pick_choice" ] && exit 0

convert "$choice" -resize "$pick_choice" "$HOME/Pictures/resize_$(date +'%d-%m-%Y-%H%M%S')".png
notify-send "converted image $choice"

}

# Resize image in root search
resize_root() {

echo "${color26}${end}${space}resize ${color21}${space}resize image, saves to picture directory${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
wl-paste > "$HOME"/Pictures/.temp_resize_image.txt
sed -i 's|file:\//||' "$HOME"/Pictures/.temp_resize_image.txt
cat "$HOME"/Pictures/.temp_resize_image.txt | xargs | tr -d '\r' | \
sed 's|%20| |g' | tee "$HOME"/Pictures/.temp_resize_image.txt
convert_file=$(cat "$HOME"/Pictures/.temp_resize_image.txt)
pick_choice=$(echo "" | rofi -dmenu -i -p '' -config "$rofi_config" \
-mesg "$(cat ~/Pictures/.temp_resize_image.txt)" \
-theme-str 'window {padding: 10px 10px 10px 10px;}' \
-theme-str 'listview {dynamic: false; scrollbar: true; columns: 1; lines: 8;}' \
-theme-str 'entry {placeholder : "format 1920x1080 or 1920x keep aspect ratio"; placeholder-color: grey;}')
[ -z "$pick_choice" ] && exit 0

check_image=$(file --mime-type "$(cat "$HOME"/Pictures/.temp_resize_image.txt)" | awk -F ':' '{print $NF}' | sed 's| ||')

   if [[ "$check_image" == *"image/png"* ]]; then
     convert "$convert_file" -resize "$pick_choice" "$HOME/Pictures/resize_$(date +'%d-%m-%Y-%H%M%S')".png
     notify-send "converted image $convert_file"
   elif [[ "$check_image" == *"image/jpeg"* ]]; then
     convert "$convert_file" -resize "$pick_choice" "$HOME/Pictures/resize_$(date +'%d-%m-%Y-%H%M%S')".png
     notify-send "converted image $convert_file"
   else
     notify-send "Not an image file or not supported image type!"
   fi

}

#-------------------------------------------------------#
# Download folder
#-------------------------------------------------------# 

download_folder() {

echo "${color26}${end}${space}downloads ${color21}${space}folder${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
choice=$(find ~/Downloads/ -not -path '*/.*' -type f -iname "*" -print0 | xargs -0 ls -t | cut -d '/' -f5- | _rofi17)
exit_status=$?
[ -z "$choice" ] && exit 0

   if [[ $exit_status == "11" ]]; then
     echo "$HOME"/Downloads/"$choice" | tee "$HOME"/Documents/.temp_downloaded_actions.txt
     rofi_actions_download
   elif [[ $exit_status == '12' ]]; then
     echo "$HOME"/Downloads/"$choice" > /dev/null
     rofi_actions_download
   else
     echo "$HOME"/Downloads/"$choice" | xargs -0 -d '\n' xdg-open
   fi
	
}

#-------------------------------------------------------#
# Download videos
#-------------------------------------------------------# 

# Open downloaded video
do_open() {

"$video_player" "$HOME"/Videos/.ytdownvids/*.webm	
	
}

# Paste in link to download
do_out() {

choice=$(echo "" | _rofi2)
rm -f "$HOME"/Videos/.ytdownvids/*.webm
yt-dlp "$choice" -o "$HOME/Videos/.ytdownvids/%(title)s.%(ext)s" && notify-send "Download completed"
cp "$HOME"/Videos/.ytdownvids/*.webm "$HOME"/Videos/youtube/	
	
}

#-------------------------------------------------------# 
# Single note
#-------------------------------------------------------#

# Copy line to clipboard
oneline_notes() {

echo "${color26}${end}${space}oneline-notes ${color21}${space}oneline note${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
launcher="$(tac "$HOME"/Documents/mainprefix/quicknotes.txt | _rofi27)"
exit_status=$?
[ -z "$launcher" ] && exit 0

   if [[ $exit_status == "11" ]]; then
     "$texteditor" "$HOME"/Documents/mainprefix/quicknotes.txt
   elif [[ $exit_status == "12" ]]; then 
     sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/quicknotes.txt
     choice=$(echo "" | _rofi2)	
     [ -z "$choice" ] && exit 0
     printf '%s\n' "$choice" >> "$HOME"/Documents/mainprefix/quicknotes.txt
   elif [[ $exit_status == "13" ]]; then
     rm -f "$HOME"/Pictures/.temqrcodes/*.png
     echo "$launcher" | tr -d '\n' | qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
     one2=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     notify-send -i "$one2" "Created QR Code"
   elif [[ $exit_status == "14" ]]; then
     echo -n "$launcher" | xargs -d '\n' | festival --tts	
   else
     printf '%s' "$launcher" | wl-copy -n
   fi	
	
}

#-------------------------------------------------------#
# Find local files
#-------------------------------------------------------#

# Find files on local system live search
find_files() {

echo "${color26}${end}${space}find ${color21}${space}search files and open${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
choice=$(find "${dirs[@]}" -type f -iname '*' | sort | cut -d '/' -f4- | _rofi16)
exit_status=$?
[ -z "$choice" ] && exit 0

   if [[ $exit_status == "11" ]]; then
     echo "$HOME"/"$choice" | tee "$HOME"/Documents/.temp_actions.txt
     rofi_actions
   elif [[ $exit_status == '12' ]]; then
     echo "$HOME"/"$choice" > /dev/null
     rofi_actions
   else
     echo "$HOME"/"$choice" | xargs -0 -d '\n' xdg-open
   fi
	
}

# Open directories in file manager
find_directories() {

echo "${color26}${end}${space}directories ${color21}${space}search for directories${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
choice=$(find "${dirs[@]}" -maxdepth 9 -type d | _rofi23)
exit_status=$?
[ -z "$choice" ] && exit 0

   if [[ $exit_status == "11" ]]; then
     echo "$choice" | tee "$HOME"/Documents/.directories_actions.txt
     rofi_actions_directories
   elif [[ $exit_status == "12" ]]; then
     echo "$choice" > /dev/null
     rofi_actions_directories
   else 
     "$file_manager" "$choice"
   fi

}

# Find files on local system live search
find_all_files() {

chosen=$(find ~ -type f -iname '*' | sort | cut -d '/' -f4- | _rofi9)
[ -z "$chosen" ] && exit 0
echo "$HOME"/"$chosen" | tee "$HOME"/Documents/.temp_actions.txt	
rofi_actions
	
}


# Open files in file manager
find_file_manager() {

	
chosen=$(find "${dirs[@]}" -type f -iname '*.*' | sort | cut -d '/' -f4- | _rofi9)
[ -z "$chosen" ] && exit 0
echo "$HOME"/"$chosen" | xargs -0 -d '\n' "$file_manager"	
	
}

# Files modified with 24 hours
find_recent() {
   
recent_file=$(find "${dirs[@]}" -maxdepth 5 -type f -ctime 0 | sort | cut -d '/' -f4- | _rofi9)	
[ -z "$recent_file" ] && exit 0
echo "$HOME"/"$recent_file" | xargs -0 -d '\n' xdg-open	
  
}

# Images modified with 24 hours
images_recent() {
   
recent_file=$(find "${dirs[@]}" \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -ctime 0 | sort | cut -d '/' -f4- | _rofi9)	
[ -z "$recent_file" ] && exit 0
echo "$HOME"/"$recent_file" | xargs -0 -d '\n' "$image_editor"
  
}

# Copy directories to clipboard
clip_directories() {

choice=$(find "${dirs[@]}" -maxdepth 9 -type d | _rofi2)
echo "$choice" | wl-copy -n

}

# Open directories in terminal
find_terminal_directories() {
	
choice=$(find "${dirs[@]}" -maxdepth 9 -type d | _rofi2)
"$myterminal" -- /bin/bash -c "cd $choice; $SHELL"
	
}

# Copy file path to clipboard
copy_file_path() {

chosen=$(find "${dirs[@]}" -type f -iname '*.*' | sort | cut -d '/' -f4- | _rofi9)
[ -z "$chosen" ] && exit 0
echo "$HOME"/"$chosen" | xargs -0 -d '\n' | wl-copy -n
	
}

# Find files without extension
find_without_extension() {

chosen=$(find "${dirs[@]}" -type f ! -iname '*.*' | sort | cut -d '/' -f4- | _rofi9)
[ -z "$chosen" ] && exit 0
echo "$HOME"/"$chosen" | xargs -0 -d '\n' xdg-open	

}

#-------------------------------------------------------#
# Calculator
#-------------------------------------------------------#

# Calculator history
calculator_history() {
	
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.sums_mixed.txt
choice=$(tac "$HOME"/Documents/.sums_mixed.txt | rofi -dmenu -i -p '' -config "$rofi_config" \
-mesg "${color13}${space}clipboard: enter | calculator: alt+c${end}" \
-kb-custom-2 "alt+c" \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'message { margin: 30px; }' \
-theme-str 'entry { placeholder: "Sums history...";}')
exit_status=$?
[ -z "$choice" ] && exit 0

   if [[ $exit_status == "11" ]]; then
     my_calculator
   else
     [ -z "$choice" ] && exit 0
     echo "$choice" | awk '{print $NF}' | wl-copy -n
   fi

}

# Main calculator
my_calculator() {

echo "${color26}${end}${space}cal ${color21}${space}calculator${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.temp_sums.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.combine_sums.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.sums_mixed.txt
launcher=$(echo "" | rofi -dmenu -i -p '' -config "$rofi_config" \
-mesg "${color13}${space}history: alt+c${end}" \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'message { margin: 30px; }' \
-theme-str 'entry { placeholder: "Type sum...";}' \
-kb-custom-2 "alt+c")
exit_status=$?

   if [[ $exit_status == "11" ]]; then
     calculator_history
   else
     translate=$(echo "$launcher" | sed 's/=//' | tee "$HOME"/Documents/.temp_sums.txt)
     [ -z "$launcher" ] && exit 0 || echo "$translate" && cat "$HOME"/Documents/.temp_sums.txt | bc | \
     tee "$HOME"/Documents/.combine_sums.txt && paste "$HOME"/Documents/.temp_sums.txt "$HOME"/Documents/.combine_sums.txt | \
     sed -E 's/[[:space:]]+/ /' | \
     tee -a "$HOME"/Documents/.sums_mixed.txt
     calculator_history
   fi

}

#-------------------------------------------------------#
# Kill processes
#-------------------------------------------------------#

# Killall
kill_process() {

choice=$(echo "" | _rofi2)
killall "$choice"

}

# Xkill 
xkill_process() {
	
xkill	
	
}

#-------------------------------------------------------#
# QR Codes
#-------------------------------------------------------#

# View and open QR Codes in rofi
qr_code_gallery() {

qr_codes_dir="$HOME/Pictures/qr_codes/"
qr_codes_loc="Pictures/qr_codes"
echo "${color26}${end}${space}qr-gallery ${color21}${space}view and open QR Codes${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
choice=$(find "$qr_codes_dir" -maxdepth 1 -iname '*.*' -printf '%T@ %p\n' | sort -rn | cut -f2- -d" " | \
awk -F/ '{print $NF}' | while read -r A ; do  echo -en "$A\x00icon\x1f~/$qr_codes_loc/$A\n" ; done | \
rofi -dmenu -i -p '' -config "$rofi_config" -theme-str 'entry {placeholder : "Search qr codes...";}' -show-icons \
-mesg "${color14}open: ${color30}enter${end} | decode: ${color30}alt+c${end} | filemanager: ${color30}alt+x${end} | clip path: ${color30}alt+v${end} | clip contents: ${color30}alt+n${end}${end}" \
-theme-str 'message {padding: 20px;}' \
-kb-custom-2 "alt+c" \
-kb-custom-3 "alt+x" \
-kb-custom-4 "alt+v" \
-kb-custom-5 "alt+n" \
-theme-str 'element-icon { size: 10em;}' \
-theme-str 'window {fullscreen: false; location: center; width: 60%; padding: 0px 0px 0px 0px;}' \
-theme-str 'element {orientation: vertical; }' \
-theme-str 'element.selected.normal {background-color: transparent; padding: 0px 0px 0px 0px; border: 2 2 2 2; border-color: #61AFEF;}' \
-theme-str 'element-text {horizontal-align: 0.5; text-color: #CAD3F5;}' \
-theme-str 'listview {dynamic: false; scrollbar: true; columns: 2; lines: 2;}')
exit_status=$?
[ -z "$choice" ] && exit 0

   if [[ $exit_status == "11" ]]; then
     echo "$HOME"/Pictures/qr_codes/"$choice" | xargs zbarimg --raw | \
     yad --text-info --wrap --height=500 --width=800 --center --back="#1a1818" --fore="#ffffff"
   elif [[ $exit_status == "12" ]]; then 
     echo "$HOME"/Pictures/qr_codes/"$choice" | xargs -0 -d '\n' "$file_manager"
   elif [[ $exit_status == "13" ]]; then 
     echo "$HOME"/Pictures/qr_codes/"$choice" | wl-copy -n
   elif [[ $exit_status == "14" ]]; then
     echo "$HOME"/Pictures/qr_codes/"$choice" | xargs zbarimg --raw | wl-copy -n
   elif [[ $exit_status == "15" ]]; then 
     notify-send "Does not work in wayland!"
     #xclip -selection clipboard -t image/png -i < "$(echo "$HOME"/Pictures/qr_codes/"$choice")"
   else
     image=$(printf '%s\n' "${qr_codes_dir}${choice}")
     echo "$image" | xargs -0 -d '\n' "$image_viewer"
   fi

}

# Decode qr code
qr_decode() {

find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw | \
yad --text-info --wrap --height=500 --width=800 --center --back="#1a1818" --fore="#ffffff"	
	
}

# Create qr code from clipboard contents
qr_clip() {

rm -f "$HOME"/Pictures/.temqrcodes/*.png	
wl-paste | tr -d '\n' | qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
one2=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
notify-send -i "$one2" "Created QR Code"

}

# Image notification of last qr code created
qr_view() {

   if [[ -z "$(ls -A "$HOME"/Pictures/.temqrcodes)" ]]; then
     notify-send "Directory is empty"
   else
     one=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     notify-send -i "$one" "Qrcode"	
   fi	
	
}

# Open last created qr code in image viewer
qr_open() {

   if [[ -z "$(ls -A "$HOME"/Pictures/.temqrcodes)" ]]; then
     notify-send "QR Code directory is empty"
   else
     find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 -d '\n' "$image_viewer"
   fi	
	
}

# Open last qr code in file manager
qr_dir() {
	
chosen=$(find "$HOME"/Pictures/.temqrcodes/ -type f -iname '*.*' | sort | cut -d '/' -f6-)
[ -z "$chosen" ] && exit 0
"$file_manager" "$HOME"/Pictures/qr_codes/"$chosen"	
	
}

# Decode last qr code and speak contents using text to speech
qr_speak() {
	
find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw | festival --tts	
	
}

# Select qr code on screen and create qr code (OCR)
qr_ocr() {
	
rm -f "$HOME"/Pictures/.temqrcodes/*.png
rm -f "$HOME"/Pictures/.ocr/*.png
grimshot save area ~/Pictures/.ocr/ocr.png
find "$HOME"/Pictures/.ocr/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg --raw | \
tr -d '\n' | qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
one2=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
notify-send -i "$one2" "Created QR Code"	
	
}

# Type text to create qr code
qr_out() {
	
choice=$(echo "" | _rofi37)
[ -z "$choice" ] && exit 0
rm -f "$HOME"/Pictures/.temqrcodes/*.png
printf '%s\n' "$choice" | tr -d '\n' | qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
one2=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
notify-send -i "$one2" "Created QR Code"
     
}

#-------------------------------------------------------#
# Run terminal command
#-------------------------------------------------------#

run_term() {

echo "${color26}${end}${space}ter ${color21}${space}run terminal commands${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
choice=$(echo "" | _rofi2)
[ -z "$choice" ] && exit 0
echo "$choice" | tee "$HOME"/Documents/.temp_shell_command.txt
temp_shell_file=$(cat "$HOME"/Documents/.temp_shell_command.txt )
"$myterminal" -- /bin/bash -c "$temp_shell_file; $SHELL"
#"$myterminal" --command="/bin/bash -c '$choice; $SHELL'"

}

#-------------------------------------------------------#
# PDF
#-------------------------------------------------------#

pdf_dir() {

# Open last created pdf in file manager	
chosen5=$(find "$HOME"/Documents/.pdftemp/ -type f -iname '*.*' | sort | cut -d '/' -f6-)
[ -z "$chosen5" ] && exit 0
"$file_manager" "$HOME"/Documents/pdf/"$chosen5"	
	
}

# Open last created pdf
pdf_open() {

   if [[ -z "$(ls -A "$HOME"/Documents/.pdftemp)" ]]; then	
     notify-send "Pdf directory is empty"
   else
     find "$HOME"/Documents/.pdftemp/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 -d '\n' "$pdf_viewer"
   fi	
	
}

# Print last pdf created
pdf_print() {

   if [[ -z "$(ls -A "$HOME"/Documents/.pdftemp)" ]]; then
     notify-send "Pdf directory is empty"
   else
     lp -d "$my_printer" "$HOME"/Documents/.pdftemp/*.pdf	
   fi	
	
}

# Type path to file to create pdf
pdf_out() {
	
choice=$(echo "" | _rofi38)
[ -z "$choice" ] && exit 0
rm -f "$HOME"/Documents/.pdftemp/*.*
libreoffice --headless --convert-to pdf "$choice" --outdir "$HOME"/Documents/.pdftemp/ && notify-send "PDF created"
cp "$HOME"/Documents/.pdftemp/*.* "$HOME"/Documents/pdf/	
	
}

# Create pdf from clipboard
pdf_clip() {

wl-paste > "$HOME"/Documents/.temp_pdf_clip.txt
sed -i 's|file:\//||' "$HOME"/Documents/.temp_pdf_clip.txt
cat "$HOME"/Documents/.temp_pdf_clip.txt | xargs | tr -d '\r' | \
sed 's|%20| |g' | tee "$HOME"/Documents/.temp_pdf_clip.txt
choice=$(cat "$HOME"/Documents/.temp_pdf_clip.txt)
[ -z "$choice" ] && exit 0
rm -f "$HOME"/Documents/.pdftemp/*.*
libreoffice --headless --convert-to pdf "$choice" --outdir "$HOME"/Documents/.pdftemp/ && notify-send "PDF created"
cp "$HOME"/Documents/.pdftemp/*.* "$HOME"/Documents/pdf/	
	
}

#-------------------------------------------------------#
# Text to speech
#-------------------------------------------------------#

# Send clipboard contents to text to speech engine
text_clip() {
	
wl-paste > ~/Documents/.clip_text_tts
cat ~/Documents/.clip_text_tts | festival --tts	
	
}

# Stop text to speech engine
text_stop() {

pkill -f audsp	
	
}

# Clip path to file to speak contents
text_out() {

wl-paste > "$HOME"/Documents/.temp_clip_tts.txt
sed -i 's|file:\//||' "$HOME"/Documents/.temp_clip_tts.txt
cat "$HOME"/Documents/.temp_clip_tts.txt | xargs | tr -d '\r' | \
sed 's|%20| |g' | tee "$HOME"/Documents/.temp_clip_tts.txt
choice=$(cat "$HOME"/Documents/.temp_clip_tts.txt)
festival --tts "$choice"	
	
}

# Type text to speak it
text_speak() {

choice=$(echo "" | _rofi2)	
echo "$choice" | festival --tts
	
}

#-------------------------------------------------------#
# Zipping files
#-------------------------------------------------------#

# Create zip file from clipboard contents
zi_clip() {

rm -f "$HOME"/Documents/.zips/*.*
wl-paste > "$HOME"/Documents/.zipfile.txt
sed -i 's|file://||' "$HOME"/Documents/.zipfile.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.zipfile.txt
zip_text=$(cat "$HOME"/Documents/.zipfile.txt)
echo "$zip_text" | tr -d '\r' | tee "$HOME"/Documents/.zipfile.txt
sed -i 's|%20| |g' "$HOME"/Documents/.zipfile.txt
xargs -L 1 -a "$HOME"/Documents/.zipfile.txt -d '\n' zip -j "$HOME"/Documents/.zips/"$(date +'%d-%m-%Y-%H%M%S')".zip	
cp "$HOME"/Documents/.zips/*.* "$HOME"/Documents/zips/
	
}

# Open zip directory in file manager
zi_dir() {
	
"$file_manager" "$HOME"/Documents/zips/

}

# Type path to file to create zip file
zi_out() {

choice=$(echo "" | _rofi2)
rm -f "$HOME"/Documents/.zips/*.*	
zip -j "$HOME/Documents/.zips/$(date +'%d-%m-%Y-%H%M%S')".zip "$choice" && notify-send "File zipped!"
cp "$HOME"/Documents/.zips/*.* "$HOME"/Documents/zips/
	
}

# Open last created zip file
zi_open() {

   if [[ -z "$(ls -A "$HOME"/Documents/.zips)" ]]; then
     notify-send "Zip directory is empty"
   else	
     find "$HOME"/Documents/.zips/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 -d '\n' xdg-open	
   fi
	
}

#-------------------------------------------------------#
# Open johns config
#-------------------------------------------------------#

open_config() {

choice=$(find ~/bin/ -type f -iname "*johns_config.sh*")
"$texteditor" "$choice"

}

#-------------------------------------------------------#
# Separate notes
#-------------------------------------------------------#

# Open note in text editor
no_open() {

echo "${color26}${end}${space}no open ${color21}${space}separate notes${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt	
launcher=$(grep -m 1 "" "$HOME"/Documents/prefix_notes/* | sed 's%:%: >  %' | cut -d '/' -f6- | cut -c 1-80 | _rofi4)
[ -z "$launcher" ] && exit 0
echo "$HOME"/Documents/prefix_notes/"$launcher" | awk -F ':' '{print $1}' | xargs -0 -d '\n' "$texteditor"
	
}

# Open notes directory
no_dir() {

"$file_manager" "$HOME"/Documents/prefix_notes/	
	
}

# Copy note contents to clipboard
no_copy() {
	
copynote=$(find "$HOME"/Documents/prefix_notes/ -type f | _rofi4)
copy_command=$(cat "$copynote")
[ -z "$copynote" ] && exit 0
echo "$copy_command" | xclip -sel clip	
	
}

# Create note
no_out() {

choice=$(echo "" | _rofi2)
[ -z "$choice" ] && exit 0
notedir="$HOME/Documents/prefix_notes"
notefile="$notedir/$choice"
"$texteditor" "$notefile".md	
	
}

#-------------------------------------------------------#
# Record video and audio
#-------------------------------------------------------#

# Fullscreen recording
video_record() {

echo "on" | tee "$HOME"/Documents/.rofi_record_status.txt	
rm -f "$HOME"/Videos/.yadvidrec/*.*
notify-send "Recording started" && ffmpeg -video_size "$recordres" -framerate 30 -f x11grab -i :0.0+0 -f pulse -ac 2 -i default "$HOME/Videos/.yadvidrec/video_$(date +'%d-%m-%Y-%H%M%S').mkv"

}

# Audio recording
audio_record() {
	
rm -f "$HOME"/Music/.yadaudiorec/*.*
notify-send "Recording audio only" ; arecord -f S16_LE -t wav "$HOME/Music/.yadaudiorec/audio_$(date '+%d%m%Y-%H%M-%S').wav"

}

# Stop video and audio recording
stop_recording() {

   if [[ "$(pidof ffmpeg)" ]]; then
     echo "off" | tee "$HOME"/Documents/.rofi_record_status.txt
     killall ffmpeg
     cp "$HOME"/Videos/.yadvidrec/*.* "$HOME"/Videos/recordings/
     notify-send "Recording stopped"
   elif [[ "$(pidof arecord)" ]]; then
     killall arecord
     cp "$HOME"/Music/.yadaudiorec/*.* "$HOME"/Music/recordings/ 
     notify-send "Audio recording stopped"
   else
     :
   fi

}

# Opens last video recording
video_play() {

   if [[ -z "$(ls -A "$HOME"/Videos/.yadvidrec)" ]]; then
     notify-send "Recording directory is empty"
   else
    find "$HOME"/Videos/.yadvidrec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 -d '\n' "$video_player"
   fi	

}

# Open last audio recording
audio_play() {

   if [[ -z "$(ls -A "$HOME"/Music/.yadaudiorec)" ]]; then
     notify-send "Audio directory is empty"
   else
     find "$HOME"/Music/.yadaudiorec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 -d '\n' "$audio_player"
   fi	
	
}

# Screencast status
record_status() {
	
   if [[ "$(cat "$HOME"/Documents/.rofi_record_status.txt)" == 'on' ]]; then
     notify-send "Recording!"
   elif [[ "$(cat "$HOME"/Documents/.rofi_record_status.txt)" == 'off' ]]; then
     notify-send "Not recording"
   else
     :
   fi	
	
}

#-------------------------------------------------------#
# Date
#-------------------------------------------------------#

date_out() {
	
calcul=$(date | _rofi2)
[ -z "$launchercontent" ] && exit 0
echo "$calcul" | wl-copy -n
	
}

#-------------------------------------------------------#
# Uptime
#-------------------------------------------------------#

uptime_out() {
	
upt=$(uptime | _rofi2)
[ -z "$launchercontent" ] && exit 0
echo "$upt" | wl-copy -n
	
}

#-------------------------------------------------------#
# Spell checker
#-------------------------------------------------------#

spell_checker() {

echo "${color26}${end}${space}spell ${color21}${space}spell checker${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt	
choice=$(echo "" | rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'entry { placeholder: "Spellcheck word...";}')
[ -z "$choice" ] && exit 0
launch=$(aspell -a list <<< "$choice" | awk 'BEGIN{FS=","; OFS="\n"} {$1=$1} 1' | rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'listview {lines: 8; columns: 2;}')
echo "$launch" | awk -F ':' '{print $NF}' | sed 's| ||' | wl-copy -n

}

#-------------------------------------------------------#
# Password generator
#-------------------------------------------------------#

pass_gen() {

echo "${color26}${end}${space}pass ${color21}${space}password generator${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
choice=$(echo "" | rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'entry { placeholder: "Type number...";}' \ )
tr -dc A-Za-z0-9 </dev/urandom | head -c "$choice" | wl-copy -n

}

#-------------------------------------------------------#
# Local Dictionary
#-------------------------------------------------------#

dict_local() {

launch=$(echo "" | _rofi30)	
choice=$(echo "$launch" | xargs dict)
echo "$choice" | _rofi30
	
}

#-------------------------------------------------------#
# Empty trash
#-------------------------------------------------------#

trash_clear() {

   if [[ -z "$(ls -A "$HOME"/.local/share/Trash)" ]]; then	
     notify-send "empty"
   else
     notify-send "trash" && rm -rf "$HOME"/.local/share/Trash/*	
   fi
	
}

#-------------------------------------------------------#
# Quicklinks
#-------------------------------------------------------#

# Send quick link to file
ql_out() {

choice=$(echo "" | _rofi26)
[ -z "$choice" ] && exit 0
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/quick_links.txt	
echo "$choice" | tee -a "$HOME"/Documents/mainprefix/quick_links.txt	
	
}

# Type your query search term
query_rofi() {

echo 'query_rofi' | tee ~/Documents/.last_sub_menu.txt
choice=$(echo "" | rofi -dmenu -i -p '' \
-mesg "$(cat "$HOME"/Documents/.temp_query.txt)" \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'window {padding: 10px 10px 10px 10px;}' \
-config "$rofi_config" \
-theme-str 'entry {placeholder : "Enter Query...";}')
echo "$choice" | tee "$HOME"/Documents/.temp_query_choice.txt
open_query_link   
	
}

# View quick links and exec link
quick_links() {

echo "${color26}${end}${space}ql links ${color21}${space}view quicklinks${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
links_text=$(cat "$HOME"/Documents/mainprefix/quick_links.txt | _rofi25) 
exit_status=$?
[ -z "$links_text" ] && exit 0
  
   if [[ $exit_status = "11" && -d "$links_text" ]]; then
     echo "$links_text" | tee "$HOME"/Documents/.cd_path.txt
     path_file=$(cat "$HOME"/Documents/.cd_path.txt)
     "$myterminal" -- /bin/bash -c "cd \"$path_file\"; $SHELL"
   elif [[ $exit_status = "12" ]]; then
     echo "$links_text" | wl-copy -n
   elif [[ $exit_status = "14" ]]; then
     echo "$links_text" > /dev/null
     "$texteditor" "$HOME"/Documents/mainprefix/quick_links.txt
   elif [[ $exit_status = "15" && "$links_text" = *"http"* ]]; then
     echo "$links_text" | tee "$HOME"/Documents/.temp_query.txt
     query_rofi  
   elif [[ -f "$links_text" ]]; then
     xdg-open "$links_text"
   elif [[ -d "$links_text" ]]; then
     "$file_manager" "$links_text"
   elif [[ "$links_text" = *"http"* && "$links_text" =~ "="$ ]] || [[ "$links_text" = *"http"* && "$links_text" =~ "/"$ ]]; then
     notify-send "This is a query!"
   elif [[ "$links_text" = *"http"* ]]; then
     echo "$links_text" | awk '{print $NF}' | xargs -r "$browser"
   else
     search="https://www.google.co.uk/search?q={}"
     echo "$links_text" | tee ~/Documents/mainprefix/.last_web_search.txt
     echo "$links_text" | xargs -I{} "$browser" "$search"
   fi 
 		
}

# Open query link
open_query_link() {
   
   if [[ -z "$(cat "$HOME"/Documents/.temp_query_choice.txt)" ]]; then
     :
   else
     query_file_link=$(cat "$HOME"/Documents/.temp_query.txt)
     query_file_choice=$(cat "$HOME"/Documents/.temp_query_choice.txt) 
     echo "${query_file_link}${query_file_choice}" | tee "$HOME"/Documents/.temp_query_combine.txt
     query_combine=$(cat "$HOME"/Documents/.temp_query_combine.txt | tail -n 1)
     sed -i 's| |+|g' "$HOME"/Documents/.temp_query_combine.txt
     "$browser" "$query_combine" && exit 0
   fi

}

# Display quick links help page for root search
ql_info_root() {

linkstext=$(printf '%s' "${color1}link ${end}${color2}name - http://example.com${end}${space}${color3}open web link${end}
${color1}file ${end}${color2}/home/user/Documents/example.txt${end}${space}${color3}open file${end}
${color1}path ${end}${color2}/home/user/Pictures/${end}${space}${color3}open directory in file manager${end}
${color1}query ${end}${color2}https://www.youtube.com/results?search_query=${end}${space}${color3}query search within web site${end}
")

echo "${linkstext[@]}" | sort | _rofi5

}	

# Display quick links help page
ql_info() {

linkstext=$(printf '%s' "${color1}format: ${end}${color2}name - http://example.com${end}${space}${color3}open web link${end}
${color1}format: ${end}${color2}/home/user/Documents/example.txt${end}${space}${color3}open file${end}
${color1}format: ${end}${color2}linux distro${end}${space}${color3}google search${end}
${color1}format: ${end}${color2}/home/user/Pictures/${end}${space}${color3}open directory in file manager${end}
${color1}format: ${end}${color2}https://www.youtube.com/results?search_query=${end}${space}${color3}query search within web site${end}
")
	
echo "${linkstext[@]}" | sort | _rofi5	
	
}

#-------------------------------------------------------#
# Close program
#-------------------------------------------------------#

close_app() {

xdotool key alt+F4	
	
}

#-------------------------------------------------------#
# Snippets
#-------------------------------------------------------#

# Create snippet
create_snip() {

echo "${color26}${end}${space}sni ${color21}${space}create snippet${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
choice=$(echo "" | _rofi33)
[ -z "$choice" ] && exit 0
echo "$choice" | tee "$HOME"/Documents/mainprefix/.add_temp_snippet.txt	
awk '{$2=":" OFS $2} 1' "$HOME"/Documents/mainprefix/.add_temp_snippet.txt | sed 's| ||' | \
tee "$HOME"/Documents/mainprefix/.snip_add_to_main.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/snippets.txt
cat "$HOME"/Documents/mainprefix/.snip_add_to_main.txt | tee -a "$HOME"/Documents/mainprefix/snippets.txt 

}

# Send snippet to clipboard via a keyword
open_snip() {

echo "${color26}${end}${space}sn ${color21}${space}paste snippet${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
choice=$(echo "" | _rofi32)
grep "$choice\:" "$HOME"/Documents/mainprefix/snippets.txt | awk -F ':' '{$1="";print $0}' | sed 's/^[ \t]*//' | wl-copy -n

}

# View snippets text file and copy selected item to clipboard
view_snips() {

echo "${color26}${end}${space}sni view ${color21}${space}view snippets${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
choice=$(cat "$HOME"/Documents/mainprefix/snippets.txt | sed 's|:||g' | _rofi32)
[ -z "$choice" ] && exit 0
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/snippets.txt
echo "$choice" | awk -F ':' '{$1="";print $0}' | sed 's/ //' | wl-copy -n
		
}

# Open snippets text file
sni_txt() {

"$texteditor" "$HOME"/Documents/mainprefix/snippets.txt	
	
}

#-------------------------------------------------------#
# Downloading files
#-------------------------------------------------------#

# Download file to temp directory
download_file() {

choice=$(echo "" | _rofi2)
[ -z "$choice" ] && exit 0
rm -f "$HOME"/Downloads/.temp_downloads/downloadedfile
wget "$choice" -O "$HOME"/Downloads/.temp_downloads/downloadedfile
cp "$HOME"/Downloads/.temp_downloads/downloadedfile "$HOME"/Downloads/"$(date '+%d%m%Y-%H%M-%S')"
notify-send	"File downloaded"

}

# Open downloaded file
open_download() {

   if [[ -z "$(ls -A "$HOME"/Downloads/.temp_downloads)" ]]; then
     notify-send "Download directory is empty"
   else
     find "$HOME"/Downloads/.temp_downloads/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 -d '\n' xdg-open	
   fi	
	
}

# If download contains image, then set wallpaper
down_set_wall() {

   notify-send "Does not work in wayland yet!"
   #if [[ "$(file --mime-type ~/Downloads/.temp_downloads/downloadedfile)" == *"jpeg"* ]]; then
   #down_file=$(echo "$HOME"/Downloads/.temp_downloads/downloadedfile)
   #feh --bg-scale "$down_file"
   #sed -i "$lineset|^.*$|feh --bg-scale \"$(echo "$down_file")\"|" "$myautostart"	
   #elif [[ "$(file --mime-type ~/Downloads/.temp_downloads/downloadedfile)" == *"png"* ]]; then
   #down_file=$(echo "$HOME"/Downloads/.temp_downloads/downloadedfile)
   #feh --bg-scale "$down_file"
   #sed -i "$lineset|^.*$|feh --bg-scale \"$(echo "$down_file")\"|" "$myautostart"
   #else
   #notify-send "No images in download directory!"
   #fi
		
}

#-------------------------------------------------------#
# Last created temp files
#-------------------------------------------------------#

open_last() {

echo "${color26}${end}${space}last ${color21}${space}show all temporary files${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt	
last_one="$(find "$HOME"/Downloads/.temp_downloads/ -type f -print0 | xargs -0 ls -t | head -n 1)"	
last_two="$(find "$HOME"/Documents/.pdftemp/ -type f -print0 | xargs -0 ls -t | head -n 1)"
last_three="$(find "$HOME"/Videos/.ytdownvids/ -type f -print0 | xargs -0 ls -t | head -n 1)"	
last_four="$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -t | head -n 1)"
last_five="$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -t | head -n 1)"
last_six="$(find "$HOME"/Music/.yadaudiorec/ -type f -print0 | xargs -0 ls -t | head -n 1)"
last_seven="$(find "$HOME"/Documents/.zips/ -type f -print0 | xargs -0 ls -t | head -n 1)"

options=$(echo -en "${last_one}\n${last_two}\n${last_three}\n${last_four}\n${last_five}\n${last_six}\n${last_seven}")
	
choice=$(printf '%s\n' "${options[@]}" | _rofi31) || exit 1

    if [ "$choice" ]; then
    image=$(printf '%s\n' "${choice}")
      xdg-open "$image"
    else
      echo "program terminated" && exit 0
    fi	
	
}

#-------------------------------------------------------#
# Weather
#-------------------------------------------------------#

# Update and download weather data
weather_download() {

   if [[ -z "$(ls -A "$HOME"/Downloads/.temp_weather)" ]]; then
     notify-send "No weather data!"
   else
     rm -rf "$HOME"/Downloads/.temp_weather/*
   fi
           
wget https://weather-broker-cdn.api.bbci.co.uk/en/forecast/rss/3day/"$weather_api_key" -O "$HOME"/Downloads/.temp_weather/downloadedfile
notify-send	"Weather updated"	
	
sed -i 's|Temperature:||g' "$HOME"/Downloads/.temp_weather/downloadedfile
	
}

# Menu
menu_weather() {
	
echo "update weather"	
		
}

# Parse weather data
weather_info() {
	
grep Today: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||' | sed 's|Temperature||g' | sed 's| ||'
grep Tonight: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||' | sed 's|Temperature||g' | sed 's| ||'
grep Monday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||' | sed 's|Temperature||g' | sed 's| ||'
grep Tuesday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||' | sed 's|Temperature||g' | sed 's| ||'
grep Wednesday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||' | sed 's|Temperature||g' | sed 's| ||'
grep Thursday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||' | sed 's|Temperature||g' | sed 's| ||'
grep Friday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||' | sed 's|Temperature||g' | sed 's| ||'
grep Saturday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||' | sed 's|Temperature||g' | sed 's| ||'
grep Sunday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||' | sed 's|Temperature||g' | sed 's| ||' 

}	

# Main
main_weather() {
	
choice=$(menu_weather | _rofi10)

case "$choice" in

 'update weather')
   weather_download
  ;;
  
  *)
  echo "Something with wrong!" || exit
  ;;
  
esac

}

#-------------------------------------------------------#
# Apps with different class names
#-------------------------------------------------------#

# Office
select_gimp() {

   if pgrep -f "$image_editor_class" > /dev/null; then
     echo -e "${color26}${end}${space}${image_editor} ${color11}${space}${image_editor_description}${end}"
   else
     echo -e "${color26}${end}${space}${image_editor} ${color11}${space}${image_editor_description}${end}"
   fi
   
}

#-------------------------------------------------------#
# Rofi theme changer
#-------------------------------------------------------#

rofi_theme_changer() {

echo "${color26}${end}${space}theme ${color21}${space}theme changer${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt		
choice=$(find "$HOME"/Pictures/rofi_themes_pics/ -maxdepth 1 -iname '*.*' | \
awk -F/ '{print $NF}' | while read -r A ; do  echo -en "$A\x00icon\x1f~/Pictures/rofi_themes_pics/$A\n" ; done | sort | \
rofi -dmenu -i -p '' -config "$rofi_config" -theme-str 'entry {placeholder : "Search themes...";}' -show-icons -theme-str 'element-icon { size: 30em;}' \
-theme-str 'window {fullscreen: false; width: 1300px; location: center; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;}' \
-theme-str 'element.selected.normal {padding: 0px 0px 0px 0px; border: 0 0 0 0;}' \
-theme-str 'element-text {vertical-align: 0.5; }' \
-theme-str 'element {padding: 10px; }' \
-theme-str 'listview {dynamic: false; scrollbar: true; columns: 1; lines: 1; }')

   if [ "$choice" ]; then
     image=$(printf '%s\n' "${screenshots_dir}${choice}")
     echo "$image" | tee "$HOME"/Pictures/.changing_rofi_theme.txt
   else
     echo "program terminated" && exit 0
   fi
   
   if [[ "$(cat "$HOME"/Pictures/.changing_rofi_theme.txt)" == *"catppuccin"* ]]; then
     sed -i "18s|^.*$|rofi_config=\"$(echo "$HOME/.config/rofi/config.rasi")\"|" "$HOME"/bin/prefixrofi_config.sh
   elif [[ "$(cat "$HOME"/Pictures/.changing_rofi_theme.txt)" == *"tokyonight"* ]]; then
     sed -i "18s|^.*$|rofi_config=\"$(echo "$HOME/.config/rofi/config2.rasi")\"|" "$HOME"/bin/prefixrofi_config.sh
   else
     :
   fi
   	
}

#-------------------------------------------------------#
# Last modified files
#-------------------------------------------------------#

# Open last modified Documents
last_mod_docs() {

choice=$(find "${dirs[@]}" -type f \( -iname '*.doc' -o -iname '*.docx' -o -iname '*.pdf' -o -iname '*.ods' -o -iname '*.xlsx' -o -iname '*.odt' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
xdg-open "$choice"	
	
}

# Open last downloaded file in downloads directory
last_mod_downloaded_file() {

choice=$(find ~/Downloads/ -not -path '*/.*' -type f -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")	
echo "$choice" | xargs -0 -d '\n' xdg-open
	
}

# Open last downloaded image in image editor
last_mod_downloaded() {

choice=$(find "${dirs[@]}" -type f \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
"$image_editor" "$choice"	
	
}

# Open last image
last_mod_image() {

choice=$(find "${dirs[@]}" -type f \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
"$image_viewer" "$choice"	
	
}

# Open last video
last_mod_video() {

choice=$(find "${dirs[@]}" -type f \( -iname '*.mp4' -o -iname '*.mkv' -o -iname '*.webm' -o -iname '*.avi' -o -iname '*.wmv' -o -iname '*.mov' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
"$video_player" "$choice"

}

# Open last sound file
last_mod_sound() {

choice=$(find "${dirs[@]}" -type f \( -iname '*.mp3' -o -iname '*.wav' -o -iname '*.aiff' -o -iname '*.acc' -o -iname '*.flac' -o -iname '*.wma' -o -iname '*.m4a' -o -iname '*.ogg' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
"$audio_player" "$choice"

}

#-------------------------------------------------------#
# Command palettes (sway only)
#-------------------------------------------------------#

# Main
main_command() {

##############################################
# Gimp
# When you first launch gimp, the class name will not by gimp.
# After you have created a new image or opened a image, it will change class name to gimp.
# 2 mod keys and single key does not work in gimp.
############################################## 

echo "${color26}${end}${space}keys ${color21}${space}command palettes${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
active_window=$(swaymsg -t get_tree | \
jq -r '.. | select(.type?) | select(.focused==true).name' | \
tr "&" "+" | tr -d '<>' | tr -d '#' | tr -d '""' | \
tr '[:upper:]' '[:lower:]' | \
tr -dC '[:print:]\t\n' | awk '{print $NF}')
[ -z "$active_window" ] && exit 0

   if [[ "$active_window" == *"$browser"* ]]; then
     launch_browser
   else
     notify-send "not working"
   fi
	
}

# Browser
launch_browser() {

fileone="$(cat ~/Documents/palettes/brave.txt)"
choice=$(echo -e "$fileone" | sort | rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'entry { placeholder: "Brave shortcut keys...";}' \
-theme-str 'listview {lines: 8; columns: 1;}')
[ -z "$choice" ] && exit 0
select_choice=$(echo -e "$choice" | awk -F ':' '{print $NF}' | sed 's/ //' | tr -d '\n')

   if [[ "$select_choice" == *"ctrl"* && "$select_choice" == *"shift"* ]]; then
     my_choice01=$(echo "$select_choice" | awk '{print $1}')
     my_choice02=$(echo "$select_choice" | awk '{print $2}')
     my_choice03=$(echo "$select_choice" | awk '{print $3}')
     sleep 0.5 && wtype -M $my_choice01 -M $my_choice02 -k $my_choice03
   elif [[ "$select_choice" == *"ctrl"* ]]; then
     my_choice01=$(echo "$select_choice" | awk '{print $1}')
     my_choice02=$(echo "$select_choice" | awk '{print $2}')
     sleep 0.5 && wtype -M $my_choice01 -k $my_choice02
   elif [[ "$select_choice" == *"shift"* ]]; then
     my_choice01=$(echo "$select_choice" | awk '{print $1}')
     my_choice02=$(echo "$select_choice" | awk '{print $2}')
     sleep 0.5 && wtype -M $my_choice01 -k $my_choice02
   else
     my_choice01=$(echo "$select_choice" | awk '{print $1}')
     sleep 0.5 && wtype -k $my_choice01
   fi
	
}

#-------------------------------------------------------#
# Microphone
#-------------------------------------------------------#

# Microphone on
check_microphone() {
	
   if [[ "$(pactl get-source-mute @DEFAULT_SOURCE@ | awk -F ':' '{print $2}' | sed 's/ //')" == 'no' ]]; then
     notify-send "Microphone is on"
   else
     notify-send "Microphone is off"
   fi

}

# Microphone off
microphone_toggle() {

   if [[ "$(pactl get-source-mute @DEFAULT_SOURCE@ | awk -F ':' '{print $2}' | sed 's/ //')" == 'no' ]]; then
     pactl set-source-mute @DEFAULT_SOURCE@ 1
     notify-send "Microphone is off"
   else
     pactl set-source-mute @DEFAULT_SOURCE@ 0
     notify-send "Microphone is on"
   fi

}

#-------------------------------------------------------#
# Quick text actions
#-------------------------------------------------------#

# Google search
clip_search() {
	
xclip -o | tee "$HOME"/Documents/.block_text.txt
filesearch="$HOME/Documents/.block_text.txt"
search="https://www.google.co.uk/search?q={}"
head "$filesearch" | cut -c 1-90 | xargs -I{} "$browser" "$search"
   
}
  
# File search
clip_find() {

xclip -o | tee "$HOME"/Documents/.block_text.txt	 
text_file=$(cat "$HOME"/Documents/.block_text.txt)
choice=$(find "${dirs[@]}" -type f -iname "*$text_file*" | rofi -dmenu -i -p '' -config "$rofi_config")
echo "$choice" | xargs -0 -d '\n' xdg-open

}

# Dictionary search
clip_dictionary() {

xclip -o | tee "$HOME"/Documents/.block_text.txt	
launch=$(cat "$HOME"/Documents/.block_text.txt)	
choice=$(echo "$launch" | xargs dict)
echo "$choice" | _rofi2
	
}

#-------------------------------------------------------#
# Control brave with playerctl
#-------------------------------------------------------#

# Change browser position 5 minutes plus

browser_position_plus() {

instance_check2=$(playerctl -l | grep "$browser" | sed "s/${browser}.instance//")
for counter in {1..60}; do playerctl --player="$browser".instance"$instance_check2" position 10+; done

}

# Change browser position 5 minutes minus

browser_position_minus() {

instance_check2=$(playerctl -l | grep "$browser" | sed "s/${browser}.instance//")
for counter in {1..60}; do playerctl --player="$browser".instance"$instance_check2" position 10-; done	
   
}

# Brave status check
brave_media_status() {

   if [[ "$(playerctl status -p brave)" = 'Playing' ]]; then
     echo "Playing"
   elif [[ "$(playerctl status -p brave)" = 'Paused' ]]; then
     echo "Pause"
   else
     echo "Not playing"
   fi
	
}

# Main
media_brave() {

echo "${color26}${end}${space}brave-playerctl ${color21}${space}control brave with playerctl${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt

item1="play-pause ${color23}toggle${end}"
item2="5+ ${color23}skip forward 5 minutes${end}"
item3="5- ${color23}skip backwards 5 minutes${end}"
item4="next ${color23}skips to next video in playlist${end}"
item5="previous ${color23}skips to previous video in playlist${end}"

brave_title=$(playerctl --player="brave" metadata -f "{{xesam:title}}" | cut -c 1-80 | tr "&" "+")
instance_check=$(playerctl -l | grep "$browser" | sed "s/${browser}.instance//")
choice=$(echo -e "${item1}\n${item2}\n${item3}\n${item4}\n${item5}" | rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'message { margin: 30px; }' \
-theme-str 'entry { placeholder: "Control brave...";}' \
-markup-rows -mesg "${space}${color22}${brave_title}${end}
${space}${color24}$(brave_media_status)${end}" | awk '{print $1}')

   if [[ "$choice" = 'play-pause' ]]; then
     playerctl --player="$browser".instance"$instance_check" play-pause
   elif [[ "$choice" = 'stop' ]]; then
     playerctl --player="$browser".instance"$instance_check" stop
   elif [[ "$choice" = '5+' ]]; then
     browser_position_plus
   elif [[ "$choice" = '5-' ]]; then
     browser_position_minus
   elif [[ "$choice" = 'next' ]]; then
     playerctl --player brave next
   elif [[ "$choice" = 'previous' ]]; then
     playerctl --player brave previous
   else
     :
   fi
	
}

#-------------------------------------------------------#
# IP address
#-------------------------------------------------------#

your_ip_address() {

hostname -I | awk '{print $1}' | xclip -selection clipboard	
	
}

#-------------------------------------------------------#
# Extra information about keywords
#-------------------------------------------------------#

# Date of last screenshot taken
full_info01() {
 
echo "$(find ~/Pictures/.tempfullscrns/ -type f -iname '*' -print0 | xargs -0 basename)"
   	
}

# Check the current status of the radio
radio_info02() {

   if [[ "$(cat "$HOME"/Documents/.mpv_recording.txt)" == 'on' ]]; then
     echo "Recording"
   elif [[ "$(playerctl status -p mpv)" == 'Playing' ]]; then
     echo "Playing" 
   elif	[[ "$(playerctl status -p mpv)" == 'Paused' ]]; then
	 echo "Paused"
   elif	[[ "$(pidof ffplay)" ]]; then
     echo "Playback"
   else
     echo "Not playing!"
   fi
    	
}

# Screencast status
record_info03() {
	
   if [[ "$(cat "$HOME"/Documents/.rofi_record_status.txt)" == 'on' ]]; then
     echo "Recording!"
   elif [[ "$(cat "$HOME"/Documents/.rofi_record_status.txt)" == 'off' ]]; then
     echo "Not recording"
   else
     :
   fi	
	
}

# Show last google search
go_info04() {

choice=$(cat "$HOME"/Documents/mainprefix/.last_web_search.txt | cut -c 1-40 | tr "&" "+")
[ -z "$choice" ] && exit 0
echo "$choice" | sed 's|&|+|'
	
}

# Last modified file
mod_file_info05() {

choice=$(find "${dirs[@]}" -type f -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
file --mime-type "$choice" | awk '{print $NF}'
	
}

# Volume status
volume_info06() {

volume="$(amixer -D pulse get Master | tail -1 | awk '{print $5}')"
mute="$(amixer -D pulse get Master | tail -1 | awk '{print $6}')"

echo "${volume} ${mute}"

}

# Date
date_info07() {
	
date '+%H:%M %d-%m-%Y'

}

# Audacious status
audacious_status_info08() {
	
   if [[ "$(audtool --playback-status)" = 'playing' ]]; then
   echo "playing"
   elif [[ "$(audtool --playback-status)" = 'paused' ]]; then
   echo "paused"
   else
   echo "Not Playing"
   fi	
	
}

# Weather
weather_info09() {
	
grep Today: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||' | \
sed 's| ||' | awk '{ print $(NF-1) }'
grep Tonight: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||' | \
sed 's| ||' | awk '{ print $(NF-1) }'	
	
}

# Print pdf file
pdf_info10() {

   if [[ -z "$(ls -A "$HOME"/Documents/.pdftemp)" ]]; then
     echo "Pdf directory is empty"
   else
     find "$HOME"/Documents/.pdftemp -type f -iname '*' -print0 | xargs -0 basename | cut -c 1-40 | sed 's|&|+|'
   fi
	
}

# Empty trash
empty_trash_info11() {
	
   if [[ -z "$(ls -A "$HOME"/.local/share/Trash)" ]]; then	
     echo "Empty"
   else
     echo "Full"	
   fi

}

# Downloaded file
download_file_info12() {

choice=$(find "$HOME"/Downloads/.temp_downloads/ -type f -iname '*')
file --mime-type "$choice" | awk '{print $NF}'
		
}

# Recent document
mod_document_info14() {
	
choice=$(find "${dirs[@]}" -type f \( -iname '*.doc' -o -iname '*.docx' -o -iname '*.pdf' -o -iname '*.ods' -o -iname '*.xlsx' -o -iname '*.odt' \) -printf '%T@ %p\n' | \
sort -n | tail -1 | cut -f2- -d" ")
echo "${choice##*/}" | sed 's|&|+|'

}

# Last downloaded file
mod_downloaded_info15() {
	
choice=$(find ~/Downloads/ -not -path '*/.*' -type f -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")	
echo "${choice##*/}" | sed 's|&|+|'

}

# Open last modified image in gimp
mod_gimp_info16() {
	
choice=$(find "${dirs[@]}" -type f \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
echo "${choice##*/}" | sed 's|&|+|'

}

# Last modified image
mod_image_info17() {
	
choice=$(find "${dirs[@]}" -type f \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
echo "${choice##*/}" | sed 's|&|+|'

}

# Last modified video
mod_video_info18() {

choice=$(find "${dirs[@]}" -type f \( -iname '*.mp4' -o -iname '*.mkv' -o -iname '*.webm' -o -iname '*.avi' -o -iname '*.wmv' -o -iname '*.mov' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
echo "${choice##*/}" | sed 's|&|+|'	
	
}

# Last modified sound file
mod_sound_info19() {

choice=$(find "${dirs[@]}" -type f \( -iname '*.mp3' -o -iname '*.wav' -o -iname '*.aiff' -o -iname '*.acc' -o -iname '*.flac' -o -iname '*.wma' -o -iname '*.m4a' -o -iname '*.ogg' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
echo "${choice##*/}" | sed 's|&|+|'
	
}

# Selected image file for resizing
mod_resize_info20() {

wl-paste > ~/Documents/.mod_resize_info.txt
choice=$(cat ~/Documents/.mod_resize_info.txt)
echo "${choice##*/}" | sed 's|&|+|'

}

# Open last pdf created
mod_pdf_open21() {
	
choice=$(find ~/Documents/.pdftemp/ -type f -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
echo "${choice##*/}" | sed 's|&|+|'

}

# Uptime
mod_uptime22() {

uptime -p
	
}

# Last downloaded video file
mod_downloaded_video23() {

choice=$(find ~/Videos/.ytdownvids/ -type f -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
echo "${choice##*/}" | sed 's|&|+|' | tr -d '<>' | tr -d '#' | tr -d '""' | tr -dC '[:print:]\t\n'
	
}

#-------------------------------------------------------#
# Video cutter
#-------------------------------------------------------#

video_cutter() {

echo "${color26}${end}${space}video-cutter ${color21}${space}video cutter${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt	
choice=$(GTK_THEME="Material-Black-Mango-LA" yad --form --width=550 --height=300 --title="Video cutter" \
--borders=20 \
--text-align="center" \
--align="center" \
--text="${color27}Start / ${color30}00:00${end} | End / ${color30}00:00${end} | Input / ${color30}Select file${end}\n${end}" \
--separator="#" \
--field="${color28}Start ${end}":CE \
--field="${color28}End ${end}":CE \
--field="${color28}Input ${end}":FL \
--field="\n${color27}Saves to video directory${end}":LBL "" \
--button="Cut":0 \
--button="Exit":1)
[[ -z "$choice" ]] && exit

start=$(echo "$choice" | awk -F '#' '{print $1}')
export start
end=$(echo "$choice" | awk -F '#' '{print $2}')
export end
file=$(echo "$choice" | awk -F '#' '{print $3}')
export file

ffmpeg -ss 00:"$start" -to 00:"$end" -i "$file" -c copy "$HOME"/Videos/"$(date '+%d-%m-%Y-%H-%M-%S')".mp4 && notify-send "Video had been cut"
		
}

#-------------------------------------------------------#
# Add quicklink for root search
#-------------------------------------------------------#
# Before text put the following keywords:
# file /home/user/Documents/text01.txt
# path /home/user/Documents/
# link https://gitlab.com/example
# query https://www.google.com/search?q=

add_quicklink() {

text_item01="<span color='#646B86' font_family='Material Icons Outlined' font='22' rise='-8pt'></span>"
choice=$(echo "" | _rofi2)
[ -z "$choice" ] && exit 0
echo "$choice" | tee "$HOME"/Documents/.add_quicklink.txt
text_file=$(cat "$HOME"/Documents/.add_quicklink.txt)
echo "$text_file" | sed "s|^|${text_item01} |" | tee "$HOME"/Documents/.add_quicklink.txt
cat "$HOME"/Documents/.add_quicklink.txt | tee -a "$HOME"/Documents/.ql_main_root.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.ql_main_root.txt
	
}

#-------------------------------------------------------#
# Brave bookmarks
#-------------------------------------------------------#

brave_bookmarks() {

echo "${color26}${end}${space}brave-bookmarks ${color21}${space}open brave bookmarks${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt

# Location of brave bookmarks
brave_file="$HOME/snap/brave/current/.config/BraveSoftware/Brave-Browser/Default/Bookmarks"

# Greps the titles
awk -F '"name":' '{print $2}' "$brave_file" | sed '/^[[:space:]]*$/d' | \
tr -d '"' | sed 's| ||' | tee ~/Documents/.brave_bookmarks_title.txt
sed -i -e 's|Bookmarks bar,||g' -e '/^$/d' ~/Documents/.brave_bookmarks_title.txt

# Greps the url's
awk -F '"url":' '{print $2}' "$brave_file" | sed '/^[[:space:]]*$/d' | \
tr -d '"' | sed 's| ||' | tee ~/Documents/.brave_bookmarks_url.txt

# Combines both titles and url's
paste ~/Documents/.brave_bookmarks_title.txt ~/Documents/.brave_bookmarks_url.txt | \
tee "$HOME"/Documents/.bookmarks_brave_mixed.txt

# Main
file_combined=$(cat "$HOME"/Documents/.bookmarks_brave_mixed.txt)
choice=$(echo "$file_combined" | rofi -dmenu -i -p '' -config "$rofi_config" \
-theme-str 'listview {lines: 8; columns: 1;}' \
-theme-str 'element { highlight: none; }' \
-theme-str 'window {width: 60%;}')
[[ -z "$choice" ]] && exit

echo "$choice" | awk '{print $NF}' | xargs -d '\n' "$browser"

}

#-------------------------------------------------------#
# Reminders
#-------------------------------------------------------#

# Add reminder
add_reminder() {
	
# Yad settings
choice=$(GTK_THEME="Yaru-dark" yad --form --width=650 --height=400 --title="Reminders" \
--borders=20 \
--text-align="center" \
--align="center" \
--text="Reminders\n" \
--separator="#" \
--field="Title ":CE \
--field="Notes ":CE \
--field="Due date ":CE \
--field="Priority ":CE \
--field="\nCreated by John Mcgrath":LBL "" \
--button="Create":0 \
--button="Exit":1)
export choice
[[ -z "$choice" ]] && exit

# Entry fields
# Title
title=$(echo "$choice" | awk -F '#' '{print $1}')
export title
# Notes
notes=$(echo "$choice" | awk -F '#' '{print $2}')
export notes
# Date
dates=$(echo "$choice" | awk -F '#' '{print $3}')
export dates
# priority
priority=$(echo "$choice" | awk -F '#' '{print $4}')
export priority

# Color field separators
color_field="<span color='#646B86' font_family='Material Icons Outlined' font='22' rise='-8pt'></span>"
export color_field
color_field2="<span color='#0CFF00' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"
export color_field2
color_field3="<span color='#00ECFF' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"
export color_field3
color_field4="<span color='#FFA200' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"
export color_field4
color_field5="<span color='#FF0068' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"
export color_field5
end="</span>"
export end

# Reminder text file template
text_file="${color_field}${color_field2} TITLE${end} ${title}${color_field3} NOTES${end} ${notes}${color_field4} DUE DATE${end} ${dates}${color_field5} PRIORITY${end} ${priority}"
export text_file

# Main
sed -i '/^[[:space:]]*$/d' "$HOME/Documents/.reminders.txt"
echo "$text_file" | tee -a "$HOME/Documents/.reminders.txt"	
		
}

# Show reminders
show_reminders() {
	
     status_file=$(tac "$HOME"/Documents/.reminders.txt)
     export status_file
   if [[ -z "$status_file" ]]; then
     echo "<span color='#646B86' font_family='Material Icons Outlined' font='22' rise='-8pt'></span><span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'> TEMPLATE</span> shopping<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'> NOTES</span> get water<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'> DUE DATE</span> 18-10-2023<span color='#8087A2' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'> PRIORITY</span> medium" | tee -a "$HOME"/Documents/.reminders.txt
   else
     :
   fi 	
	
echo "${color26}${end}${space}reminder ${color21}${space}show and add reminders${end}${color20}${space}${end}" | \
tee "$HOME"/Documents/.keyword_last_used.txt
choice=$(tac "$HOME"/Documents/.reminders.txt | _rofi28)
exit_status=$?
[ -z "$choice" ] && exit 0

   if [[ $exit_status == "11" ]]; then
     echo "$choice" > /dev/null 
     add_reminder
   elif [[ $exit_status == "12" ]]; then
     sed -i -e "s%^.*${choice}$%%" -e '/^$/d' "$HOME"/Documents/.reminders.txt
   elif [[ $exit_status == "13" ]]; then
     echo "$choice" | tee "$HOME"/Documents/.done_reminder.txt
     sed -i -e "s%^.*${choice}$%%" -e '/^$/d' "$HOME"/Documents/.reminders.txt
     sed -i 's|#646B86|#FF004E|' "$HOME"/Documents/.done_reminder.txt
     cat "$HOME"/Documents/.done_reminder.txt | tee -a "$HOME"/Documents/.reminders.txt
   elif [[ $exit_status == "14" ]]; then
     echo "$choice" | tee "$HOME"/Documents/.pin_reminder.txt
   else
     echo "$choice" | tee "$HOME"/Documents/.edit_reminders.txt
     sed -i "s|<span color='#646B86' font_family='Material Icons Outlined' font='22' rise='-8pt'></span>||g" "$HOME"/Documents/.edit_reminders.txt
     sed -i "s|<span color='#FF004E' font_family='Material Icons Outlined' font='22' rise='-8pt'></span>||g" "$HOME"/Documents/.edit_reminders.txt
     sed -i "s|<span color='#0CFF00' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>||g" "$HOME"/Documents/.edit_reminders.txt
     sed -i "s|<span color='#00ECFF' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>||g" "$HOME"/Documents/.edit_reminders.txt
     sed -i "s|<span color='#FFA200' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>||g" "$HOME"/Documents/.edit_reminders.txt
     sed -i "s|<span color='#FF0068' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>||g" "$HOME"/Documents/.edit_reminders.txt
     sed -i "s|</span>||g" "$HOME"/Documents/.edit_reminders.txt
     cat "$HOME"/Documents/.edit_reminders.txt | wl-copy -n
   fi
	
}

#-------------------------------------------------------#
# Switch to already open apps in root search
#-------------------------------------------------------#
# This function only works in sway.
# Some apps might not work, because they have different class names

app_running() {

   if [[ "$(echo "$DESKTOP_SESSION")" == 'sway' ]]; then
     swaymsg -t get_tree \
       | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class):\(.name)"' \
       | grep -i "$bang" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' focus
   else
     :
   fi

}

#-------------------------------------------------------#
# Open and move to gimp
#-------------------------------------------------------#

gimp_class_name() {

   if pgrep -f "$image_editor_class" > /dev/null; then
     choice=$(swaymsg -t get_tree | jq -r '.nodes[1].nodes[].nodes[] | .. | (.id|tostring) + " " + .name?' | \
     grep -e "[0-9]* ." | \
     grep -i "$moveto_image_editor" | awk '{print $1}')
     swaymsg [con_id="$choice"] focus
   else
     "$open_image_editor"
   fi

}

#-------------------------------------------------------#
# Open new instance of app
#-------------------------------------------------------#

new_instance() {

   if [[ "$bang" == 'foot' ]]; then
   /bin/bash -c foot
   else
   /bin/bash -c "$bang" || exit 1
   fi

}

#-------------------------------------------------------#
# Processing quicklinks in root search
#-------------------------------------------------------#

# Link
root_link() {

echo "$extra_content" | tee "$HOME"/Documents/.run_quicklink_root.txt
cat "$HOME"/Documents/.run_quicklink_root.txt | awk '{print $NF}' | xargs -r "$browser"
	
}

# File
root_file() {

echo "$extra_content" | tee "$HOME"/Documents/.run_quicklink_root.txt
choice=$(cat "$HOME"/Documents/.run_quicklink_root.txt | xargs | sed 's/^/"/;s/$/"/')
echo "$choice" | xargs xdg-open
	
}

# Query
root_query() {

echo "$extra_content" | tee "$HOME"/Documents/.temp_query_root.txt
sed -i "s|$|\"$(echo "" | rofi -dmenu -i -p '' \
-config "$rofi_config" \
-theme-str 'entry {placeholder : "Enter Query...";}')\"|" "$HOME"/Documents/.temp_query_root.txt ; sleep 0.5
cat "$HOME"/Documents/.temp_query_root.txt | tr -d '"' | xargs | tee "$HOME"/Documents/.temp_query_root.txt
choice=$(cat "$HOME"/Documents/.temp_query_root.txt)
[ -z "$choice" ] && exit 0
"$browser" "$choice"
	
}

# Path
root_path() {

echo "$extra_content" | tee "$HOME"/Documents/.run_quicklink_root.txt
cat "$HOME"/Documents/.run_quicklink_root.txt | xargs | sed 's/^/"/;s/$/"/' | xargs xdg-open
	
}

#-------------------------------------------------------#
# Use last sub menu used
#-------------------------------------------------------#

last_sub_menu() {

text_file=$(cat ~/Documents/.last_sub_menu.txt | tail -n 1)
$text_file
	
}

#-------------------------------------------------------#
# Keywords
#-------------------------------------------------------#
# If you get different icons appearing, than the ones you have chosen. This means there is a error.
# To locate the error, simply type keywords, and if the keyword you type does not show.
# Then this is the keyword that is faulty. Check issues at the top of this script.
# If you see things commented out, this means that it does not work in wayland yet.
# It also just might be that I am no longer using them.
# If apps are commented out, this means I am not using them.

key_words() {

echo -e "${color26}${end}${space}go ${color4}${space}google web search${end}"
echo -e "${color26}${end}${space}file-manager ${color4}${space}rofi file manager ${end}"
echo -e "${color26}${end}${space}mod-image ${color4}${space}modified image${end}${color25}${space2}$(mod_image_info17)${end}"
echo -e "${color26}${end}${space}mod-sound ${color4}${space}modified sound file${end}${color25}${space2}$(mod_sound_info19)${end}"
echo -e "${color26}${end}${space}gi ${color4}${space}google images web search${end}"
echo -e "${color26}${end}${space}actions ${color4}${space}file actions${end}"
echo -e "${color26}${end}${space}reminder ${color4}${space}show and add reminders${end}"
echo -e "${color26}${end}${space}oneline-notes ${color4}${space}oneline notes${end}"
echo -e "${color26}${end}${space}directories ${color4}${space}search for directories${end}"
echo -e "${color26}${end}${space}actions-directories ${color4}${space}actions for directories${end}"
echo -e "${color26}${end}${space}di ${color4}${space}dictionary web search${end}"
echo -e "${color26}${end}${space}ddg ${color4}${space}duckduckgo web search${end}"
echo -e "${color26}${end}${space}yt ${color4}${space}youtube web search${end}"
echo -e "${color26}${end}${space}wik ${color4}${space}wiki web search${end}"
echo -e "${color26}${end}${space}aw ${color4}${space}arch wiki web search${end}"
echo -e "${color26}${end}${space}gm ${color4}${space}google web maps${end}"
echo -e "${color26}${end}${space}gn ${color4}${space}google web news${end}"
echo -e "${color26}${end}${space}so ${color4}${space}stackoverflow web search${end}"
echo -e "${color26}${end}${space}im ${color4}${space}imdb web search${end}"
echo -e "${color26}${end}${space}gtf ${color4}${space}google translate french web search${end}"
echo -e "${color26}${end}${space}gts ${color4}${space}google translate spanish web search${end}"
echo -e "${color26}${end}${space}rd ${color4}${space}reddit web search${end}"
echo -e "${color26}${end}${space}radio ${color4}${space}plays radio${end}${color25}${space2}$(radio_info02)${end}"
echo -e "${color26}${end}${space}actions-radio ${color4}${space}radio actions${end}"
echo -e "${color26}${end}${space}gi-search ${color4}${space}google images web search${end}${color25}${space2}$(go_info04)${end}"
echo -e "${color26}${end}${space}shots ${color4}${space}view and open screenshots${end}"
#echo -e "${color26}${end}${space}ss dir ${color4}${space}screenshots${end}"
#echo -e "${color26}${end}${space}ss view ${color4}${space}screenshots${end}"
#echo -e "${color26}${end}${space}ss copy ${color4}${space}screenshots${end}"
echo -e "${color26}${end}${space}mod-gimp ${color4}${space}modified image in gimp${end}${color25}${space2}$(mod_gimp_info16)${end}"
#echo -e "${color26}${end}${space}ss gimp ${color4}${space}screenshots${end}"
echo -e "${color26}${end}${space}open ${color4}${space}last screenshot${end}"
echo -e "${color26}${end}${space}ter ${color4}${space}run terminal commands${end}"
echo -e "${color26}${end}${space}find ${color4}${space}search files and open${end}"
echo -e "${color26}${end}${space}all ${color4}${space}search all files in home${end}"
#echo -e "${color26}${end}${space}recent ${color4}${space}screenshots${end}${color25}${space2}$(full_info01)${end}"
echo -e "${color26}${end}${space}clip-search ${color4}${space}google search from text${end}"
echo -e "${color26}${end}${space}clip-find ${color4}${space}find file from text${end}"
echo -e "${color26}${end}${space}clip-dictionary ${color4}${space}search dictionary from text${end}"
echo -e "${color26}${end}${space}fi recent ${color4}${space}local file search${end}"
echo -e "${color26}${end}${space}fi gimp ${color4}${space}file search image and open${end}"
echo -e "${color26}${end}${space}fi dir ${color4}${space}file search directories${end}"
echo -e "${color26}${end}${space}fi ter ${color4}${space}file search directories (cli)${end}"
echo -e "${color26}${end}${space}fi clip ${color4}${space}file search path to clipboard${end}"
echo -e "${color26}${end}${space}fi clip-dir ${color4}${space}directory search path to clipboard${end}"
echo -e "${color26}${end}${space}fi ext ${color4}${space}file search open files without extension${end}"
echo -e "${color26}${end}${space}fi man ${color4}${space}file search open in file manager${end}"
echo -e "${color26}${end}${space}add-quicklink ${color4}${space}add quicklink for root search${end}"
echo -e "${color26}${end}${space}text ${color4}${space}clip file path to file to read contents tts${end}"
echo -e "${color26}${end}${space}text clip ${color4}${space}text to speech from clipboard${end}"
echo -e "${color26}${end}${space}text stop ${color4}${space}stop text to speech${end}"
echo -e "${color26}${end}${space}speak ${color4}${space}text to speech type text${end}"
echo -e "${color26}${end}${space}last ${color4}${space}show all temporary files${end}"
echo -e "${color26}${end}${space}no ${color4}${space}separate notes${end}"
echo -e "${color26}${end}${space}no open ${color4}${space}separate notes${end}"
echo -e "${color26}${end}${space}no dir ${color4}${space}separate notes${end}"
echo -e "${color26}${end}${space}no copy ${color4}${space}separate notes${end}"
echo -e "${color26}${end}${space}wall ${color4}${space}wallpapers${end}"
echo -e "${color26}${end}${space}configs ${color4}${space}system configs${end}"
echo -e "${color26}${end}${space}config ${color4}${space}johns config${end}"
echo -e "${color26}${end}${space}convert ${color4}${space}convert images, saves converted images to pictures directory${end}"
echo -e "${color26}${end}${space}zones ${color4}${space}timezones${end}"
echo -e "${color26}${end}${space}full ${color4}${space}screenshots${end}"
echo -e "${color26}${end}${space}record ${color4}${space}screencast${end}${color25}${space2}$(record_info03)${end}"
#echo -e "${color26}${end}${space}status ${color4}${space}screencast${end}"
echo -e "${color26}${end}${space}audio ${color4}${space}records audio${end}"
echo -e "${color26}${end}${space}stop ${color4}${space}video and audio recordings${end}"
echo -e "${color26}${end}${space}au play-pause ${color4}${space}audacious player${end}${color25}${space2}$(audacious_status_info08)${end}"
echo -e "${color26}${end}${space}vplay ${color4}${space}playback video recording${end}"
echo -e "${color26}${end}${space}aplay ${color4}${space}playback audio recording${end}"
#echo -e "${color26}${end}${space}select ${color4}${space}screenshots${end}"
#echo -e "${color26}${end}${space}delay ${color4}${space}screenshots${end}"
echo -e "${color26}${end}${space}vol ${color4}${space}system volume${end}${color25}${space2}$(volume_info06)${end}"
echo -e "${color26}${end}${space}mute ${color4}${space}volume${end}"
echo -e "${color26}${end}${space}qr ${color4}${space}create qr codes${end}"
echo -e "${color26}${end}${space}qr-gallery ${color4}${space}view and open qr codes${end}"
echo -e "${color26}${end}${space}qr clip ${color4}${space}create qr codes from clipboard${end}"
echo -e "${color26}${end}${space}qr ocr ${color4}${space}create qr codes by selecting${end}"
#echo -e "${color26}${end}${space}qr decode ${color4}${space}qr codes${end}"
#echo -e "${color26}${end}${space}qr view ${color4}${space}qr codes${end}"
#echo -e "${color26}${end}${space}qr open ${color4}${space}qr codes${end}"
#echo -e "${color26}${end}${space}qr dir ${color4}${space}qr codes open directory${end}"
#echo -e "${color26}${end}${space}qr speak ${color4}${space}qr codes${end}"
echo -e "${color26}${end}${space}100 ${color4}${space}system volume 100%${end}"
echo -e "${color26}${end}${space}70 ${color4}${space}system volume 70%${end}"
echo -e "${color26}${end}${space}50 ${color4}${space}system volume 50%${end}"
echo -e "${color26}${end}${space}ra txt ${color4}${space}radio text file${end}"
echo -e "${color26}${end}${space}ra ${color4}${space}add radio url${end}"
echo -e "${color26}${end}${space}ra record ${color4}${space}radio record${end}"
echo -e "${color26}${end}${space}ra play ${color4}${space}radio playback recording${end}"
echo -e "${color26}${end}${space}ra space ${color4}${space}radio remove whitespace${end}"
echo -e "${color26}${end}${space}ra stop ${color4}${space}radio stop and recording stop${end}"
echo -e "${color26}${end}${space}ra notify ${color4}${space}radio title notification${end}"
echo -e "${color26}${end}${space}ra qrcode ${color4}${space}radio create qr code from title${end}"
echo -e "${color26}${end}${space}ra 50 ${color4}${space}radio volume 50%${end}"
echo -e "${color26}${end}${space}ra 70 ${color4}${space}radio volume 70%${end}"
echo -e "${color26}${end}${space}ra 100 ${color4}${space}radio volume 100%${end}"
echo -e "${color26}${end}${space}bookmarks ${color4}${space}bookmarks${end}"
echo -e "${color26}${end}${space}actions-bookmarks ${color4}${space}actions for bookmarks${end}"
echo -e "${color26}${end}${space}bo ${color4}${space}add bookmark to text file${end}"
echo -e "${color26}${end}${space}bo space ${color4}${space}bookmarks${end}"
echo -e "${color26}${end}${space}bo txt ${color4}${space}bookmarks${end}"
echo -e "${color26}${end}${space}bo clip ${color4}${space}bookmarks${end}"
echo -e "${color26}${end}${space}downloads ${color4}${space}folder${end}"
echo -e "${color26}${end}${space}actions-downloads ${color4}${space}downloaded file actions${end}"
echo -e "${color26}${end}${space}mod-downloaded ${color4}${space}modified downloaded${end}${color25}${space2}$(mod_downloaded_info15)${end}"
echo -e "${color26}${end}${space}do ${color4}${space}download video file${end}"
echo -e "${color26}${end}${space}do open ${color4}${space}downloaded video file${end}${color25}${space2}$(mod_downloaded_video23)${end}"
echo -e "${color26}${end}${space}shutdown ${color4}${space}${end}"
echo -e "${color26}${end}${space}restart ${color4}${space}${end}"
echo -e "${color26}${end}${space}lock ${color4}${space}${end}"
echo -e "${color26}${end}${space}W ${color4}${space}window switcher${end}"
echo -e "${color26}${end}${space}xkill ${color4}${space}kill process${end}"
echo -e "${color26}${end}${space}bin ${color4}${space}open scripts${end}"
echo -e "${color26}${end}${space}X ${color4}${space}open script in terminal editor${end}"
echo -e "${color26}${end}${space}E ${color4}${space}exec script modified within 24 hours${end}"
echo -e "${color26}${end}${space}te ${color4}${space}create bash script${end}"
echo -e "${color26}${end}${space}te e ${color4}${space}exec any script${end}"
echo -e "${color26}${end}${space}pdf ${color4}${space}add pdf document${end}"
echo -e "${color26}${end}${space}pdf dir ${color4}${space}pdf directory${end}"
echo -e "${color26}${end}${space}pdf open ${color4}${space}pdf document${end}${color25}${space2}$(mod_pdf_open21)${end}"
echo -e "${color26}${end}${space}pdf clip ${color4}${space}create pdf from clipboard${end}"
echo -e "${color26}${end}${space}pdf print ${color4}${space}pdf document${end}${color25}${space2}$(pdf_info10)${end}"
#echo -e "${color26}${end}${space}Q ${color4}${space}sticky note${end}"
echo -e "${color26}${end}${space}emoji ${color4}${space}${end}"
echo -e "${color26}${end}${space}zi ${color4}${space}zipping paste file path${end}"
echo -e "${color26}${end}${space}zi clip ${color4}${space}zipping files${end}"
echo -e "${color26}${end}${space}zi dir ${color4}${space}zipping files${end}"
echo -e "${color26}${end}${space}zi open ${color4}${space}zipping files${end}"
echo -e "${color26}${end}${space}date ${color4}${space}${end}${color25}$(date_info07)${end}"
echo -e "${color26}${end}${space}uptime ${color4}${space}${end}${color25}$(mod_uptime22)${end}"
echo -e "${color26}${end}${space}kill ${color4}${space}killall processes${end}"
echo -e "${color26}${end}${space}au stop ${color4}${space}audacious player${end}"
echo -e "${color26}${end}${space}au next ${color4}${space}audacious player${end}"
echo -e "${color26}${end}${space}au prev ${color4}${space}audacious player${end}"
echo -e "${color26}${end}${space}au current ${color4}${space}audacious player${end}"
echo -e "${color26}${end}${space}au now ${color4}${space}audacious player${end}"
echo -e "${color26}${end}${space}au playlist ${color4}${space}audacious player${end}"
echo -e "${color26}${end}${space}au status ${color4}${space}audacious player${end}"
echo -e "${color26}${end}${space}au repeat ${color4}${space}audacious player${end}"
echo -e "${color26}${end}${space}au shuffle ${color4}${space}audacious player${end}"
echo -e "${color26}${end}${space}au p1 ${color4}${space}audacious player playlist${end}"
echo -e "${color26}${end}${space}au p2 ${color4}${space}audacious player playlist${end}"
echo -e "${color26}${end}${space}au p3 ${color4}${space}audacious player playlist${end}"
echo -e "${color26}${end}${space}au p4 ${color4}${space}audacious player playlist${end}"
echo -e "${color26}${end}${space}au p5 ${color4}${space}audacious player playlist${end}"
echo -e "${color26}${end}${space}au s0 ${color4}${space}audacious player seek${end}"
echo -e "${color26}${end}${space}au s25 ${color4}${space}audacious player seek${end}"
echo -e "${color26}${end}${space}au s50 ${color4}${space}audacious player seek${end}"
echo -e "${color26}${end}${space}au s75 ${color4}${space}audacious player seek${end}"
echo -e "${color26}${end}${space}au kill ${color4}${space}audacious player${end}"
echo -e "${color26}${end}${space}au v100 ${color4}${space}audacious player volume${end}"
echo -e "${color26}${end}${space}au v50 ${color4}${space}audacious player volume${end}"
echo -e "${color26}${end}${space}empty ${color4}${space}wastebasket${end}${color25}${space2}$(empty_trash_info11)${end}"
echo -e "${color26}${end}${space}ql links ${color4}${space}quicklinks${end}"
echo -e "${color26}${end}${space}ql ${color4}${space}add quicklink${end}"
echo -e "${color26}${end}${space}ql info ${color4}${space}quicklink${end}"
echo -e "${color26}${end}${space}quicklinks-help ${color4}${space}help page for quicklinks root search${end}"
echo -e "${color26}${end}${space}close ${color4}${space}${end}"
echo -e "${color26}${end}${space}sni ${color4}${space}add snippets${end}"
echo -e "${color26}${end}${space}sni view ${color4}${space}snippets${end}"
echo -e "${color26}${end}${space}sni txt ${color4}${space}snippets${end}"
echo -e "${color26}${end}${space}wg ${color4}${space}download file${end}"
echo -e "${color26}${end}${space}wg open ${color4}${space}open downloaded file${end}${color25}${space2}$(download_file_info12)${end}"
#echo -e "${color26}${end}${space}wg wallpaper ${color4}${space}download file and set wallpaper${end}"
echo -e "${color26}${end}${space}weather ${color4}${space}3 day forecast${end}${color25}${space2}$(weather_info09)${end}"
#echo -e "${color26}${end}${space}theme ${color4}${space}theme changer${end}"
echo -e "${color26}${end}${space}dict ${color4}${space}local dictionary${end}"
echo -e "${color26}${end}${space}cal ${color4}${space}calculator${end}"
echo -e "${color26}${end}${space}select ${color4}${space}screenshot${end}"
echo -e "${color26}${end}${space}delay ${color4}${space}screenshot${end}"
echo -e "${color26}${end}${space}hex-codes ${color4}${space}color codes (hex and rgb)${end}"
echo -e "${color26}${end}${space}pass ${color4}${space}password generator${end}"
echo -e "${color26}${end}${space}spell ${color4}${space}spell checker${end}"
echo -e "${color26}${end}${space}actions-block ${color4}${space}actions for text${end}"
echo -e "${color26}${end}${space}keys ${color4}${space}command palettes${end}"
echo -e "${color26}${end}${space}video-cutter ${color4}${space}video cutter${end}"
echo -e "${color26}${end}${space}brave-bookmarks ${color4}${space}open brave bookmarks${end}"
echo -e "${color26}${end}${space}exit-rofi ${color4}${space}exit rofi launcher${end}"
echo -e "${color26}${end}${space}brave-playerctl ${color4}${space}control brave with playerctl${end}"
echo -e "${color26}${end}${space}microphone-toggle ${color4}${space}toggles microphone on and off${end}"
echo -e "${color26}${end}${space}check-microphone ${color4}${space}checks if microphone is on or off${end}"
echo -e "${color26}${end}${space}resize ${color4}${space}resize image, saves to pictures${end}${color25}${space2}$(mod_resize_info20)${end}"
echo -e "${color26}${end}${space}ip-address ${color4}${space}your ip address, copy to clipboard${end}"
echo -e "${color26}${end}${space}sub ${color4}${space}use last sub menu${end}"
echo -e "${color26}${end}${space}go-search ${color4}${space}google web search${end}${color25}${space2}$(go_info04)${end}"
echo -e "${color26}${end}${space}ddg-search ${color4}${space}duckduckgo web search${end}${color25}${space2}$(go_info04)${end}"
echo -e "${color26}${end}${space}yt-search ${color4}${space}youtube web search${end}${color25}${space2}$(go_info04)${end}"
echo -e "${color26}${end}${space}rd-search ${color4}${space}reddit web search${end}${color25}${space2}$(go_info04)${end}"

}

#-------------------------------------------------------#
# Menu
#-------------------------------------------------------#

menu() {

cat "$HOME"/Documents/.keyword_last_used.txt
#cat "$HOME"/Documents/.ql_main_root.txt
select_gimp
echo -e "${color26}${end}${space}$audio_player ${color5}${space}audio player${end}"
echo -e "${color26}${end}${space}${browser} ${color5}${space}browser${end}"
echo -e "${color26}${end}${space}${office_suite} ${color5}${space}office suite${end}"
echo -e "${color26}${end}${space}$image_viewer ${color5}${space}image viewer${end}"
echo -e "${color26}${end}${space}${myterminal} ${color5}${space}terminal${end}"
echo -e "${color26}${end}${space}$sound_editor ${color5}${space}sound editor${end}"
echo -e "${color26}${end}${space}$video_player ${color5}${space}video player${end}"
echo -e "${color26}${end}${space}mod-video ${color4}${space}modified video${end}${color25}${space2}$(mod_video_info18)${end}"
echo -e "${color26}${end}${space}$file_manager ${color5}${space}file manager${end}"
echo -e "${color26}${end}${space}$texteditor ${color5}${space}text editor${end}"
echo -e "${color26}${end}${space}$pdf_viewer ${color5}${space}pdf viewer${end}"
echo -e "${color26}${end}${space}pick ${color4}${space}color picker${end}"
echo -e "${color26}${end}${space}sn ${color4}${space}paste snippets${end}"
echo -e "${color26}${end}${space}$app01_name ${color5}${space}$app01_description${end}"
echo -e "${color26}${end}${space}$app02_name ${color5}${space}$app02_description${end}"
echo -e "${color26}${end}${space}$app03_name ${color5}${space}$app03_description${end}"
echo -e "${color26}${end}${space}$app04_name ${color5}${space}$app04_description${end}"
echo -e "${color26}${end}${space}$app05_name ${color5}${space}$app05_description${end}"
echo -e "${color26}${end}${space}$app06_name ${color5}${space}$app06_description${end}"
echo -e "${color26}${end}${space}$app07_name ${color5}${space}$app07_description${end}"
echo -e "${color26}${end}${space}$app08_name ${color5}${space}$app08_description${end}"
echo -e "${color26}${end}${space}$app10_name ${color5}${space}$app10_description${end}"
echo -e "${color26}${end}${space}$app11_name ${color5}${space}$app11_description${end}"
echo -e "${color26}${end}${space}$app12_name ${color5}${space}$app12_description${end}"
echo -e "${color26}${end}${space}$app13_name ${color5}${space}$app13_description${end}"
echo -e "${color26}${end}${space}$app14_name ${color5}${space}$app14_description${end}"
echo -e "${color26}${end}${space}$app15_name ${color5}${space}$app15_description${end}"
echo -e "${color26}${end}${space}$app17_name ${color5}${space}$app17_description${end}"
echo -e "${color26}${end}${space}$app18_name ${color5}${space}$app18_description${end}"
echo -e "${color26}${end}${space}$app19_name ${color5}${space}$app19_description${end}"
echo -e "${color26}${end}${space}mod-documents ${color4}${space}modified document${end}${color25}${space2}$(mod_document_info14)${end}"
echo -e "${color26}${end}${space}$app20_name ${color5}${space}$app20_description${end}"
echo -e "${color26}${end}${space}$app21_name ${color5}${space}$app21_description${end}"
echo -e "${color26}${end}${space}$app22_name ${color5}${space}$app22_description${end}"
echo -e "${color26}${end}${space}$app23_name ${color5}${space}$app23_description${end}"
echo -e "${color26}${end}${space}$app24_name ${color5}${space}$app24_description${end}"
echo -e "${color26}${end}${space}$app25_name ${color5}${space}$app25_description${end}"
key_words
echo -e "${color26}${end}${space}$app09_name ${color5}${space}$app09_description${end}"
echo -e "${color26}${end}${space}$app16_name ${color5}${space}$app16_description${end}"

}
