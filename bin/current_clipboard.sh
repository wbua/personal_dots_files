#!/bin/bash

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

my_run_launchers() {

   if [[ "$SELECT_RUN_LAUNCHER" == 'rofi' ]]; then
     echo "rofi"
   elif [[ "$SELECT_RUN_LAUNCHER" == 'wofi' ]]; then
     echo "wofi"
   elif [[ "$SELECT_RUN_LAUNCHER" == "bemenu" ]]; then
     echo "bemenu"
   elif [[ "$SELECT_RUN_LAUNCHER" == "fuzzel" ]]; then
     echo "fuzzel"
   elif [[ "$SELECT_RUN_LAUNCHER" == 'tofi' ]]; then
     echo "tofi"
   else
     :
   fi

}
my_run_launchers
   
