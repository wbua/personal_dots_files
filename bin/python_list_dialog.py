import sys
import tkinter as tk
from tkinter import messagebox

class ListDialog:
    def __init__(self, root, title, list_data):
        self.root = root
        self.title = title
        self.list_data = list_data
        self.filtered_data = list_data  # Initially, no filtering
        self.selected_item = None

        # Set custom background color of the window
        self.root.config(bg="#e1f5fe")  # Light blue background for the window

        # Add search box to filter items
        self.search_label = tk.Label(root, text="Search:", bg="#e1f5fe", font=("Arial", 12))
        self.search_label.pack(padx=20, pady=(10, 0))

        self.search_box = tk.Entry(root, font=("Arial", 12))
        self.search_box.pack(padx=20, pady=(0, 10))
        self.search_box.bind("<KeyRelease>", self.filter_list)  # Bind the search box to filter list on key release

        # Create the listbox with custom background color
        self.listbox = tk.Listbox(
            root, 
            height=10, 
            width=30, 
            selectmode=tk.SINGLE, 
            bg="#0277bd",  # Blue background for the listbox
            fg="white",    # White text
            font=("Arial", 12),  # Font style and size
            selectbackground="#004d40",  # Dark green background for selected items
            selectforeground="white"  # White text for selected items
        )
        self.update_listbox()  # Initial update to show all items
        self.listbox.pack(padx=20, pady=10)

        # Create the select button with a custom background color
        self.select_button = tk.Button(
            root, 
            text="Select", 
            command=self.select_item,
            bg="#4CAF50",   # Green button
            fg="white",     # White text
            font=("Arial", 12),
            activebackground="#45a049",  # Darker green on hover
            relief="raised",  # Raised button effect
            padx=10, pady=5  # Padding inside the button
        )
        self.select_button.pack(padx=20, pady=10)

        # Create the exit button with a custom background color
        self.exit_button = tk.Button(
            root, 
            text="Exit", 
            command=self.exit_application,
            bg="#f44336",  # Red button
            fg="white",    # White text
            font=("Arial", 12),
            activebackground="#e53935",  # Slightly darker red when hovered
            relief="raised",  # Raised button effect
            padx=10, pady=5  # Padding inside the button
        )
        self.exit_button.pack(padx=20, pady=5)

        # Create the cancel button with a custom background color
        self.cancel_button = tk.Button(
            root, 
            text="Cancel", 
            command=self.cancel,
            bg="#FF9800",  # Orange button
            fg="white",    # White text
            font=("Arial", 12),
            activebackground="#fb8c00",  # Slightly darker orange when hovered
            relief="raised",  # Raised button effect
            padx=10, pady=5  # Padding inside the button
        )
        self.cancel_button.pack(padx=20, pady=5)

        # Bind the Enter key to the select_item method
        self.root.bind("<Return>", self.enter_key_select)

        # Bind the Escape key to the exit_application method
        self.root.bind("<Escape>", self.exit_key_select)

        # Bind Up and Down arrow keys to navigate the list
        self.root.bind("<Up>", self.navigate_up)
        self.root.bind("<Down>", self.navigate_down)

        # Bind the '/' key to activate the search box (focus)
        self.root.bind("/", self.focus_search_box)

    def select_item(self):
        # Get the selected item from the listbox
        selection_index = self.listbox.curselection()
        if selection_index:
            self.selected_item = self.filtered_data[selection_index[0]]
            print(self.selected_item)  # Output selected item to stdout
            self.root.quit()
        else:
            messagebox.showwarning("No Selection", "Please select an item from the list.")

    def enter_key_select(self, event):
        # This method will be called when the Enter key is pressed
        self.select_item()  # Call the same method as when the select button is clicked

    def exit_application(self):
        # This method is called when the Exit button is clicked or Escape key is pressed
        print("Exiting application...")
        self.root.quit()

    def exit_key_select(self, event):
        # This method will be called when the Escape key is pressed
        self.exit_application()

    def navigate_up(self, event):
        # Move the selection up when the Up arrow key is pressed
        current_selection = self.listbox.curselection()
        if current_selection:
            index = current_selection[0]
            if index > 0:
                self.listbox.select_clear(index)
                self.listbox.select_set(index - 1)
            else:
                self.listbox.select_clear(index)
                self.listbox.select_set(len(self.filtered_data) - 1)
        else:
            # If nothing is selected, select the last item by default
            self.listbox.select_set(len(self.filtered_data) - 1)

    def navigate_down(self, event):
        # Move the selection down when the Down arrow key is pressed
        current_selection = self.listbox.curselection()
        if current_selection:
            index = current_selection[0]
            if index < len(self.filtered_data) - 1:
                self.listbox.select_clear(index)
                self.listbox.select_set(index + 1)
            else:
                self.listbox.select_clear(index)
                self.listbox.select_set(0)
        else:
            # If nothing is selected, select the first item by default
            self.listbox.select_set(0)

    def filter_list(self, event=None):
        # Get the text from the search box
        search_term = self.search_box.get().lower()

        # Filter the list based on the search term
        if search_term:
            self.filtered_data = [item for item in self.list_data if search_term in item.lower()]
        else:
            self.filtered_data = self.list_data  # Show all items if search box is empty

        # Update the listbox to show the filtered list
        self.update_listbox()

    def update_listbox(self):
        # Clear current listbox items
        self.listbox.delete(0, tk.END)

        # Insert the filtered items into the listbox
        for item in self.filtered_data:
            self.listbox.insert(tk.END, item)

    def focus_search_box(self, event=None):
        # Focus on the search box when '/' is pressed
        self.search_box.focus_set()  # Focus the search entry widget
        self.search_box.select_range(0, tk.END)  # Select all text in the search box to clear it if desired

    def cancel(self):
        self.root.quit()

# Main entry point
if __name__ == "__main__":
    # Get the list of items from the command-line argument
    list_data = sys.argv[1].split(',')

    # Create the tkinter window
    root = tk.Tk()
    root.title("List Dialog Box")

    # Create the list dialog with the data passed from Bash
    dialog = ListDialog(root, "Select a Fruit", list_data)

    # Start the tkinter main loop
    root.mainloop()
