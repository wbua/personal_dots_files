#!/bin/bash

browser="brave"
myterminal="gnome-terminal"

   if [ -f "$HOME"/Documents/.quick_links.txt ]; then
     :
   else
     touch "$HOME"/Documents/.quick_links.txt
   fi
   

echo "" | rofi -dmenu -i -p '' | tee -a "$HOME"/Documents/.quick_links.txt
	
links_text=$(cat "$HOME"/Documents/.quick_links.txt | rofi -dmenu -i -p '') 
[ -z "$links_text" ] && exit 0
  
   if [[ "$links_text" == *"web:"* ]]; then
     echo "$links_text" | sed 's|web:||' | awk '{print $NF}' | xargs -r "$browser"
   elif [[ "$links_text" == *"home:"* ]]; then
     echo "$links_text" | sed 's|home:||' | xargs -0 -d '\n' xdg-open
   elif [[ "$links_text" == *"gs:"* ]]; then
     search="https://www.google.co.uk/search?q={}"
     echo "$links_text" | sed 's|gs:||' | xargs -I{} "$browser" "$search"
   elif [[ "$links_text" == *"ter:"* ]]; then
     echo "$links_text" | sed 's|ter:||' | tee "$HOME"/Documents/.cd_path.txt
     path_file=$(cat "$HOME"/Documents/.cd_path.txt)
     "$myterminal" -- /bin/bash -c "cd $path_file; $SHELL"
   else
     :
   fi
	
