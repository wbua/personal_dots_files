#!/bin/bash



_rofi() {

rofi -dmenu -i -p ''	
	
}




menu3() {

echo "open"
echo "copy"
echo "move"	
		
}

# Perform actions on files
rofi_actions() {

	
	
choice=$(menu3 | _rofi)
	
case "$choice" in

 'copy')
   actions_file=$(cat "$HOME"/Documents/.temp_actions.txt)
   dir_choice=$(find "${dirs[@]}" -maxdepth 9 -type d | _rofi)
   echo "$dir_choice" | tee "$HOME"/Documents/.dir_actions.txt
   cp "$actions_file" "$dir_choice"
  ;;
 'open')
   :
   cat "$HOME"/Documents/.temp_actions.txt | xargs -0 -d '\n' xdg-open
  ;;
  
 'move')
   actions_file=$(cat "$HOME"/Documents/.temp_actions.txt)
   dir_choice=$(find "${dirs[@]}" -maxdepth 9 -type d | _rofi)
   echo "$dir_choice" | tee "$HOME"/Documents/.dir_actions.txt
   mv "$actions_file" "$dir_choice"
   ;;
  
  *)
  echo "Something went wrong!" || exit 1
  ;;

esac	
	
	
}
rofi_actions
