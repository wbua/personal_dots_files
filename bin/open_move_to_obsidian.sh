#!/bin/bash

  active_app=$(swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class):\(.name)"' \
     | grep -i "obsidian" | cut -d: -f2)
 
   if [[ "$active_app" == *"Obsidian"* || "$active_app" == *"obsidian"* ]]; then
     swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class):\(.name)"' \
     | grep -i "obsidian" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' focus
   else
     /bin/bash -c "obsidian"
   fi
