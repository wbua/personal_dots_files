#!/bin/bash

# Program command: open_move_to_filemanager.sh
# Description: Opens program or move to already open instance of program.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 14-12-2024
# Program_license: GPL 3.0
# Dependencies: swaymsg, jq

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#----------------------------------------------------------------#
# Error checking
#----------------------------------------------------------------#

set -e

#----------------------------------------------------------------#
# User preferences
#----------------------------------------------------------------#

# File manager
MY_FILEMANAGER="$MAIN_FILE_MANAGER"

#----------------------------------------------------------------#
# Checks for different file managers
#----------------------------------------------------------------#

check_diff_filemanager() {

   if [[ "$MY_FILEMANAGER" == "nautilus" ]]; then
     setsid /bin/bash -c "nautilus" >/dev/null 2>&1 & disown && exit
   else
     setsid /bin/bash -c "$MY_FILEMANAGER" >/dev/null 2>&1 & disown && exit
   fi
	
}

#----------------------------------------------------------------#
# Checks if text file contains workspace number
#----------------------------------------------------------------#

empty_number() {

set_choice=$(cat "$HOME"/Documents/.set_workspace_filemanager.txt)

   if [[ -z "$set_choice" ]]; then
     /bin/bash -c "$MY_FILEMANAGER"
   else
     swaymsg workspace "$set_choice"
     setsid /bin/bash -c "$MY_FILEMANAGER" >/dev/null 2>&1 & disown && exit  
   fi
	
}

#----------------------------------------------------------------#
# Check if set workspaces variable is enabled
#----------------------------------------------------------------#

filemanager_workspace() {

   if [[ "$(grep SPECIFIC_APP_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     empty_number 
   elif [[ "$(grep SPECIFIC_APP_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     check_diff_filemanager
   else
     :
   fi	

}

#----------------------------------------------------------------#
# Open program or move to already open instance
#----------------------------------------------------------------#
 
     active_app=$(swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"' \
     | grep -i "$MY_FILEMANAGER" | cut -d: -f2)

   if [[ "$active_app" == *"$MY_FILEMANAGER"* || "$active_app" == *"${MY_FILEMANAGER^}"* ]]; then
     swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"' \
     | grep -i "$MY_FILEMANAGER" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' focus
   elif [[ "$active_app" == *"nautilus"* ]]; then
     swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"' \
     | grep -i "org.gnome.Nautilus" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' focus
   else
     filemanager_workspace
   fi  
	   
