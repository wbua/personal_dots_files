#!/bin/bash

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

DOCS_DIR="$HOME/Documents"

# Firefox profile directory
if [ ! -f "$DOCS_DIR"/my_file_profile ]; then
   touch "$DOCS_DIR"/my_file_profile
fi

# Emojis
if [ ! -f "$MAIN_EMOJI_FILE" ]; then
   touch "$MAIN_EMOJI_FILE"
fi

# Radio links
if [ ! -f "$MAIN_RADIO_FILE" ]; then
   touch "$MAIN_RADIO_FILE"
fi

# Quicknotes
if [ ! -f "$MAIN_QNOTES_FILE" ]; then
   touch "$MAIN_QNOTES_FILE"
fi

# Configs
if [ ! -f "$MAIN_CONFIGS_FILE" ]; then
   touch "$MAIN_CONFIGS_FILE"
fi

# Bookmarks
if [ ! -f "$MAIN_BOOKMARKS_FILE" ]; then
   touch "$MAIN_BOOKMARKS_FILE"
fi

# Quick links
if [ ! -f "$MAIN_QUICKLINKS_FILE" ]; then
   touch "$MAIN_QUICKLINKS_FILE"
fi

# Keyboard shortcuts
if [ ! -f "$MAIN_KEYBINDING_FILE" ]; then
   touch "$MAIN_KEYBINDING_FILE"
fi
