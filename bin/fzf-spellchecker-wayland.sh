#!/bin/bash

# Program command: fzf-spellchecker-wayland.sh
# Description: Local spellchecker.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 04-01-2024
# Program_license: GPL 3.0
# Dependencies: fzf, aspell, xclip

#-----------------------------------------------#
# Kill script
#-----------------------------------------------#

kill_script() {

pkill -f ~/bin/fzf-spellchecker-wayland.sh
	
}
export -f kill_script

#-----------------------------------------------#
# Type word you want to spellcheck
#-----------------------------------------------#

choice=$(echo "" | fzf --print-query \
--cycle \
--padding 5% \
--border="none" \
--header="
Search spelling: enter | Kill script: F9

" \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'add word to be spellchecked> ' \
--bind='F9:execute(kill_script {})' \
--info=inline \
--reverse)
export choice
[ -z "$choice" ] && exit 0

#-----------------------------------------------#
# Display results of spellcheck
#-----------------------------------------------#

main() {

launch=$(aspell -a list <<< "$choice" | awk 'BEGIN{FS=","; OFS="\n"} {$1=$1} 1' | fzf --cycle \
--padding=0% \
--border="none" \
--header="
Kill script: F9

" \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'spellcheck results> ' \
--bind='F9:execute(kill_script {})' \
--info=inline \
--reverse)
[ -z "$launch" ] && exit 0
echo "$launch" | awk -F ':' '{print $NF}' | sed 's| ||' | tr -d '\n' | tee "$HOME"/Documents/.temp_spell_check.txt
setsid ~/bin/copy_spellcheck.sh >/dev/null 2>&1 & disown
pkill -f ~/bin/fzf-spellchecker-wayland.sh

}
main
export -f main

#-----------------------------------------------#
# Unset variables and functions
#-----------------------------------------------#

unset kill_script
unset choice
unset main
