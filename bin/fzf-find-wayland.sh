#!/bin/bash

# Program command: fzf-find-wayland.sh
# Description: Searches for files and performs actions on them.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-08-2024
# Program_license: GPL 3.0
# Dependencies: fzf, wl-clipboard

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$THEME_COLOR_CONFIG"

#-------------------------------------#
# User preferences
#-------------------------------------#

# File manager
FILE_MANAGER="$MAIN_FILE_MANAGER"
export FILE_MANAGER

# Color theme file
THEME_COLOR_CONFIG="$I3BLOCKS_THEMES_CONFIG"
export THEME_COLOR_CONFIG

#-------------------------------------#
# File search locations
#-------------------------------------#

dirs=( ~/Documents/ ~/Pictures/ ~/Videos/ ~/Downloads/ ~/Music/ )
export dirs

#---------------------------------------#
# Colors
#---------------------------------------#

# Descriptions
color1=$(tput setaf 244)
export color1
# Current file
color2=$(tput setaf 255)
export color2
# Color end
end=$(tput sgr0)
export end

#-------------------------------------#
# Current temp file
#-------------------------------------#

current_file() {

cat "$HOME"/Documents/.temp_actions.txt
	
}
export -f current_file

#-------------------------------------#
# Copy file to another location
#-------------------------------------#

file_copy() {

actions_file=$(cat "$HOME"/Documents/.temp_actions.txt)
dir_choice=$(find ~ -maxdepth 9 -type d | fzf)
[[ -z "$dir_choice" ]] && exit 0
echo "$dir_choice" | tee "$HOME"/Documents/.dir_actions.txt
cp "$actions_file" "$dir_choice"
sleep 0.2
setsid wtype -M win -M shift -k f >/dev/null 2>&1 & disown
	
}
export -f file_copy

#-------------------------------------#
# Move file to another location
#-------------------------------------#

file_move() {

actions_file=$(cat "$HOME"/Documents/.temp_actions.txt)
dir_choice=$(find ~ -maxdepth 9 -type d | fzf)
[[ -z "$dir_choice" ]] && exit 0
echo "$dir_choice" | tee "$HOME"/Documents/.dir_actions.txt
mv "$actions_file" "$dir_choice"
sleep 0.2
setsid wtype -M win -M shift -k f >/dev/null 2>&1 & disown
	
}
export -f file_move

#-------------------------------------#
# Open file
#-------------------------------------#

file_open() {

choice=$(cat "$HOME"/Documents/.temp_actions.txt | xargs -r -0 -d '\n')
[[ -z "$choice" ]] && exit 0 
setsid wtype -M win -M shift -k f && xdg-open "$choice" >/dev/null 2>&1 & disown 
	
}
export -f file_open

#-------------------------------------#
# Open file in file manager
#-------------------------------------#

open_in_file_manager() {

setsid cat "$HOME"/Documents/.temp_actions.txt | xargs -r -0 -d '\n' "$FILE_MANAGER" >/dev/null 2>&1 & disown

   if [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     setsid wtype -M win -M shift -k f >/dev/null 2>&1 & disown     
     bash ~/bin/switch_to_filemanager.sh
   elif [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     setsid wtype -M win -M shift -k f >/dev/null 2>&1 & disown
   fi
	
}
export -f open_in_file_manager

#-------------------------------------#
# Copy path to clipboard
#-------------------------------------#

file_path_to_clip() {

cat "$HOME"/Documents/.temp_actions.txt | wl-copy -n
	
}
export -f file_path_to_clip

#-------------------------------------#
# Kill script
#-------------------------------------#

kill_script() {

pkill -f ~/bin/fzf-find-wayland.sh
	
}
export -f kill_script

#-------------------------------------#
# Main
#-------------------------------------#

main() {

choice=$(find "${dirs[@]}" -type f -iname '*' | sort | fzf --reverse \
--multi \
--padding=0 \
--ansi \
--cycle \
--info=inline \
--color="bg:$COLOR_BG,prompt:$COLOR_ICON,header:#929292,info:#FFFFFF,hl+:-1,hl:-1" \
--color="gutter:$COLOR_BG,fg:#FFFFFF,bg+:$COLOR_ALERT,query:#FFFFFF,fg+:-1" \
--pointer=">" \
--prompt 'find> ' \
--bind='F9:execute(kill_script {})' \
--bind='F3:execute-silent(echo -n {} | tee "$HOME"/Documents/.temp_actions.txt)+execute(open_in_file_manager {})' \
--bind='F2:execute-silent(echo -n {} | tee "$HOME"/Documents/.temp_actions.txt)+execute(file_move {})' \
--bind='F1:execute-silent(echo -n {} | tee "$HOME"/Documents/.temp_actions.txt)+execute(file_copy {})' \
--bind='enter:execute-silent(echo -n {} | tee "$HOME"/Documents/.temp_actions.txt)+execute(file_open {})' \
--header="
Press ${color2}enter${end} to open file | Press ${color2}F1${end} to copy | Press ${color2}F2${end} to move
Press ${color2}F3${end} to open in file manager | Press ${color2}F9${end} kill script

")
[ -z "$choice" ] && exit 0

setsid xdg-open "$choice" >/dev/null 2>&1 & disown 

}
export -f main
main

#-------------------------------------#
# Unset variables and functions
#-------------------------------------#

unset dirs
unset main
unset FILE_MANAGER
unset current_file
unset color1
unset color2
unset end
unset file_copy
unset file_move
unset file_open
unset open_in_file_manager
unset file_path_to_clip
unset kill_script
unset THEME_COLOR_CONFIG
