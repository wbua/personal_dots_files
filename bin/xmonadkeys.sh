#!/bin/bash

# Program command: xmonadkeys.sh 
# Description: Show commands and keybinds in rofi. Then execute keybinds in xmonad.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 17-11-2023
# Program_license: GPL 3.0
# Dependencies: rofi, google material icons font, xdotool

# Material icons font - https://github.com/google/material-design-icons/blob/master/font/MaterialIcons-Regular.ttf
# Pango markup - https://docs.gtk.org/Pango/pango_markup.html

#-------------------------------------------------------#
# General variables
#-------------------------------------------------------#

browser="brave"

#-------------------------------------------------------#
# Google search
#-------------------------------------------------------#

google_search() {
  
  choice=$(echo "" | rofi -dmenu -i -p '' -theme-str 'entry {placeholder : "Search google"; placeholder-color: grey;}')
  [ -z "$choice" ] && exit 0
  search="https://www.google.co.uk/search?q={}"
  echo "$choice" | xargs -I{} "$browser" "$search"
  	
}

#-------------------------------------------------------#
# Spaces between menu items
#-------------------------------------------------------#

space=$(printf '%0.1s' " "{1..1})

#-------------------------------------------------------#
# Colors for menu items (pango markup)
#-------------------------------------------------------#

# Prefix
color1="<span color='#85C2A7' font_family='Material Icons' font='20' rise='0pt'>"
# Descriptions
color2="<span color='#FFFFFF' font_family='Monospace' weight='bold' font='14' rise='7pt'>"
# Keys
color3="<span color='#FFED00' font_family='Monospace' weight='bold' font='14' rise='7pt'>"
# Descriptions
color4="<span color='#FFFFFF' font_family='Monospace' weight='bold' font='14' rise='1pt'>"
# Descriptions
color5="<span color='#FFFFFF' font_family='Monospace' weight='bold' font='14' rise='7pt'>"
# keys
color6="<span color='#EADDA0' font_family='Monospace' weight='bold' font='14' rise='7pt'>"
# Descriptions
color7="<span color='#FFFFFF' font_family='Monospace' weight='bold' font='14' rise='7pt'>"
# keys
color8="<span color='#FFED00' font_family='Monospace' weight='bold' font='14' rise='8pt'>"
# Prefix
color9="<span color='#EE99A0' font_family='Material Icons' font='20' rise='0pt'>"
# Prefix
color10="<span color='#C6AAE8' font_family='Material Icons' font='20' rise='0pt'>"
# Prefix
color11="<span color='#FFFFFF' font_family='Material Icons' font='20' rise='0pt'>"
# Prefix
color12="<span color='#61AFEF' font_family='Material Icons' weight='bold' font='20' rise='0pt'>"
# Prefix
color13="<span color='#28FF00' font_family='Material Icons' weight='bold' font='20' rise='0pt'>"
# Color end
end="</span>"

#-------------------------------------------------------#
# Shows menu items in rofi
#-------------------------------------------------------#

launch="${color10}${end}${space}${color7}Menu button${end}${space}${color6}super+space${end}
${color10}${end}${space}${color7}Shortcuts${end}${space}${color6}super+i${end}
${color10}${end}${space}${color7}Autostart programs${end}${space}${color6}ISO_Level3_Shift+a${end}
${color10}${end}${space}${color7}Backup my vault${end}${space}${color6}ISO_Level3_Shift+b${end}
${color10}${end}${space}${color7}Notification history${end}${space}${color6}ISO_Level3_Shift+h${end}
${color10}${end}${space}${color2}Float window (toggle)${end}${space}${color6}super+shift+f${end}
${color10}${end}${space}${color2}Close window${end}${space}${color6}super+shift+c${end}
${color10}${end}${space}${color2}Send focused window to numbered workspace${end}${space}${color6}super+m${end}
${color10}${end}${space}${color2}Change layout${end}${space}${color6}super+alt+space${end}
${color10}${end}${space}${color2}Reset layout${end}${space}${color6}super+shift+space${end}
${color10}${end}${space}${color2}Resize window to the left${end}${space}${color6}control+super+Left${end}
${color10}${end}${space}${color2}Resize window to the right${end}${space}${color6}control+super+Right${end}
${color10}${end}${space}${color2}Next window${end}${space}${color6}super+Up${end}
${color10}${end}${space}${color2}Prev window${end}${space}${color6}super+Down${end}
${color10}${end}${space}${color2}Send focused window to empty workspace${end}${space}${color6}super+shift+s${end}
${color10}${end}${space}${color2}Go to empty workspace${end}${space}${color6}super+shift+v${end}
${color10}${end}${space}${color2}Moves focused window to next workspace on the left${end}${space}${color6}super+shift+Left${end}
${color10}${end}${space}${color2}Moves focused window to next workspace on the right${end}${space}${color6}super+shift+Right${end}
${color10}${end}${space}${color5}Swap the focused window with the next window${end}${space}${color6}super+shift+Up${end}
${color10}${end}${space}${color5}Swap the focused window with the previous window${end}${space}${color6}super+shift+Down${end}
${color10}${end}${space}${color5}Move to next workspace on the left${end}${space}${color6}super+Left${end}
${color10}${end}${space}${color5}Move to next workspace on the right${end}${space}${color6}super+Right${end}
${color10}${end}${space}${color2}Swap windows${end}${space}${color6}super+y${end}
${color10}${end}${space}${color2}Find files (search local files)${end}${space}${color6}super+s${end}
${color10}${end}${space}${color2}Screenshot tool${end}${space}${color6}Print${end}
${color10}${end}${space}${color2}Create and decode qr codes${end}${space}${color6}super+c${end}
${color10}${end}${space}${color2}Gmrun (application launcher)${end}${space}${color6}alt+F2${end}
${color10}${end}${space}${color5}Reload xmonad${end}${space}${color6}super+q${end}
${color10}${end}${space}${color5}Power menu (shutdown, restart, lock and logout)${end}${space}${color6}super+g${end}
${color10}${end}${space}${color5}Dmenu (apps)${end}${space}${color6}super+p${end}
${color10}${end}${space}${color5}Rofi drun (apps)${end}${space}${color6}super+d${end}
${color10}${end}${space}${color5}Window switcher${end}${space}${color6}alt+Tab${end}
${color10}${end}${space}${color5}Switch back and forth between workspaces${end}${space}${color6}super+Tab${end}
${color10}${end}${space}${color5}Gpaste clipboard manager${end}${space}${color6}super+v${end}
${color10}${end}${space}${color5}Gpaste reexec daemon (clipboard)${end}${space}${color6}super+u${end}
${color10}${end}${space}${color5}Recompile xmonad${end}${space}${color6}super+q${end}
${color10}${end}${space}${color5}Logout xmonad${end}${space}${color6}super+shift+q${end}
${color10}${end}${space}${color5}Restarts machine${end}${space}${color6}super+z r${end}
${color10}${end}${space}${color5}Shutdown machine${end}${space}${color6}super+z s${end}
${color10}${end}${space}${color5}Lock machine${end}${space}${color6}super+z l${end}
${color9}${end}${space}${color2}ISO_Level3_Shift means the alt gr key${end}
${color9}${end}${space}${color2}Left click or right click the panel icons for different actions${end}
${color9}${end}${space}${color2}Floating a window, hold win key and left click window to focus it${end}
${color9}${end}${space}${color2}Floating a window, hold down win key, left click and drag mouse${end}
${color9}${end}${space}${color2}Resize a floating window, hold down win key, right click and drag mouse ${end}
${color9}${end}${space}${color2}When typing keybinds ignore uppercase${end}
${color9}${end}${space}${color2}Move pointer on workspaces and use mouse wheel to move through them${end}
${color9}${end}${space}${color2}Super means the windows key${end}
${color9}${end}${space}${color2}Super+1 to move to workspace 1, super+2 to move to workspace 2, ect${end}"

#-------------------------------------------------------#
# Runs keybind through xdotool
#-------------------------------------------------------#

choice=$(echo "$launch" | rofi -dmenu -i -markup-rows -p '' -me-select-entry '' -me-accept-entry 'MousePrimary' \
-theme-str 'listview {lines: 10; columns: 2;}' \
-theme-str 'entry {vertical-align: 0.9; placeholder: "Search"; placeholder-color: grey; background-color: #1E2030;}' \
-theme-str 'element-text  {vertical-align: 0.2;}' \
-theme-str 'configuration {hover-select: true; me-accept-entry: "MousePrimary"; me-select-entry: "";}' \
-theme-str '* {background: rgba ( 30, 32, 48, 100 % );}' \
-theme-str 'window {location: north; anchor: north; fullscreen: false; width: 100%; border-radius: 0px;}' \
-theme-str 'element.selected.normal {background-color: #24273A; padding: 0px 0px 0px 0px; border: 0 0 0 0; text-color: #47FF00;}' \
-theme-str 'inputbar {children: [ prompt,textbox-prompt-colon,entry,case-indicator ]; background-color: #1E2030;}' \
-theme-str 'element { highlight: none; padding: 10;}' | awk -F "${color6}+|${color2}" '{print $NF}' | sed 's|</span>||')

   if [[ "$choice" == 'google search' ]]; then
     google_search
   else
     sleep 0.1 ; echo "$choice" | xargs xdotool key
   fi
