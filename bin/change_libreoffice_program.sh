#!/bin/bash

# Program command: change_libreoffice_program.sh 
# Description: Changes libreoffice programs.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-01-2025
# Program_license: GPL 3.0
# Dependencies: yad, wofi, rofi, dmenu

# User preferences
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#------------------------------------------------------#
# Error checking
#------------------------------------------------------#

set -e

#------------------------------------------------------#
# User preferences
#------------------------------------------------------#

LIBREOFFICE_CURRENT="$LIBREOFFICE_PROGRAM"
export LIBREOFFICE_CURRENT

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Background color
MY_COLOR_BG="$COLOR_BG"
export MY_COLOR_BG

# Icon color
MY_COLOR_ICON="$COLOR_ICON"
export MY_COLOR_ICON

#------------------------------------------------------#
# Run launchers
#------------------------------------------------------#

choice_launcher() {

   if [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "rofi" ]]; then
     rofi -dmenu -i -config "$ROFI_THEME_CONFIG" -p "Set on $LIBREOFFICE_CURRENT" 
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "wofi" ]]; then
     wofi --dmenu -i -p "Set on $LIBREOFFICE_CURRENT"
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "bemenu" ]]; then
     bemenu -i -l 8 -H 40 --fn "Monospace 18" --fb "$MY_COLOR_BG" --ff "#cdd6f4" --nb "$MY_COLOR_BG" --nf "#cdd6f4" --tb "$MY_COLOR_BG" \
     --hb "$MY_COLOR_BG" --tf "$MY_COLOR_ICON" --hf "$MY_COLOR_ICON" --af "#FFFFFF" --ab "$MY_COLOR_BG" -p "Set on $LIBREOFFICE_CURRENT"
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "fuzzel" ]]; then
     fuzzel --dmenu -p "$LIBREOFFICE_CURRENT "
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "tofi" ]]; then
     tofi
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "yad" ]]; then
     GTK_THEME="$MY_GTK_THEME" yad --list --column="list" --search-column=1 --regex-search --no-headers \
     --text="\nSet on $LIBREOFFICE_CURRENT\n" --text-align="center" --separator= \
     --borders=10 --width=800 --height=600 --button="Exit":1 
   else
     :
   fi
	
}
export -f choice_launcher

#------------------------------------------------------#
# Libreoffice programs
#------------------------------------------------------#

office_choice=( 
"libreoffice-writer" 
"libreoffice-calc" 
"libreoffice-impress" 
"libreoffice-draw"
)
export office_choice

#------------------------------------------------------#
# Main
#------------------------------------------------------#

field_choice=$(printf '%s\n' "${office_choice[@]}" | choice_launcher)
export field_choice
[ -z "$field_choice" ] && exit 0

sed -i "s|.*\(LIBREOFFICE_PROGRAM=\).*|LIBREOFFICE_PROGRAM=\"$(printf '%s' "$field_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

#------------------------------------------------------#
# Unset variables and functions
#------------------------------------------------------#

unset field_choice
unset office_choice
unset choice_launcher
unset LIBREOFFICE_CURRENT
unset MY_GTK_THEME
unset MY_COLOR_ICON
unset MY_COLOR_BG
