#!/bin/bash

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

set -e

MY_CUSTOM="$CUSTOM_GTK_MODE_TOGGLE"

   if [[ "$MY_CUSTOM" == "light" ]]; then
     sed -i "s|.*\(CUSTOM_GTK_MODE_TOGGLE=\).*|CUSTOM_GTK_MODE_TOGGLE=\"$(printf '%s' "dark")\"|" ~/bin/sway_user_preferences.sh || exit 1
     bash ~/bin/create_dark_geany_theme.sh
     bash ~/bin/create_dark_gtk_css.sh
     bash ~/bin/sway_load_settings.sh
     notify-send "Dark mode"
   elif [[ "$MY_CUSTOM" == "dark" ]]; then  
     sed -i "s|.*\(CUSTOM_GTK_MODE_TOGGLE=\).*|CUSTOM_GTK_MODE_TOGGLE=\"$(printf '%s' "light")\"|" ~/bin/sway_user_preferences.sh || exit 1     
     rm -f ~/.config/geany/colorschemes/*.* || exit 1
     bash ~/bin/create_light_gtk_css.sh
     bash ~/bin/sway_load_settings.sh
     notify-send "Light mode"
   else
     notify-send "No matches found"
   fi
