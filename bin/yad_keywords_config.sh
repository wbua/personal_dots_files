#!/bin/bash

# Program command: yad_keyswords_config.sh
# Description: Config for yad KEYWORDs launcher.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-01-2025
# Program_license: GPL 3.0

# How to use:

# Put your choice's in double quotes.

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#------------------------------------------------------#
# User software preferences
#------------------------------------------------------#

# Terminal
MYTERMINAL="$MAIN_TERMINAL"
export MYTERMINAL

# Other terminal
OTHER_TERMINAL="$MAIN_OTHER_TERMINAL"
export OTHER_TERMINAL

# Browser
MYBROWSER="$MAIN_BROWSER"
export MYBROWSER

# File manager
FILE_MANAGER="$MAIN_FILE_MANAGER"
export FILE_MANAGER

# PDF viewer
PDF_VIEWER="$MAIN_PDF_VIEWER"
export PDF_VIEWER

# Image viewer
IMAGE_VIEWER="$MAIN_IMAGE_VIEWER"
export IMAGE_VIEWER

# Video player
VIDEO_PLAYER="$MAIN_VIDEO_PLAYER"
export VIDEO_PLAYER

# Audio_player
AUDIO_PLAYER="$MAIN_AUDIO_PLAYER"
export AUDIO_PLAYER

# Text editor
TEXT_EDITOR="$MAIN_TEXT_EDITOR"
export TEXT_EDITOR

# Image editor
IMAGE_EDITOR="$MAIN_IMAGE_EDITOR"
export IMAGE_EDITOR

#------------------------------------------------------#
# User path's to files and directories
#------------------------------------------------------#

# Radio stations text file
RADIO_LINKS="$MAIN_RADIO_FILE"
export RADIO_LINKS

# Emoji text file
EMOJI_TEXT="$MAIN_EMOJI_FILE"
export EMOJI_TEXT

# Screenshots directory
SCREENSHOTS_DIR="$MAIN_SCREENSHOT_DIR"
export SCREENSHOTS_DIR

# PDF directory
PDF_DIR="$MAIN_PDF_DIR"
export PDF_DIR

# Screencast directory
SCREENCAST_DIR="$MAIN_SCREENCAST_DIR" 
export SCREENCAST_DIR

# Audio recordings directory
AUDIO_DIR="$MAIN_AUDIO_DIR" 
export AUDIO_DIR

# QR Code directory 
QR_CODE_DIR="$MAIN_QR_CODES_DIR"
export QR_CODE_DIR

# Zips directory
ZIPS_DIR="$MAIN_ZIPS_DIR"
export ZIPS_DIR

# Quick notes file
QNOTES_FILE="$MAIN_QNOTES_FILE"
export QNOTES_FILE

#------------------------------------------------------#
# Misc
#------------------------------------------------------#

# Yad GTK theme
GTK_THEMES="$ALL_YAD_GTK"
export GTK_THEMES

# My printer
MY_PRINTER="$MAIN_PRINTER"
export MY_PRINTER

# Delay screenshot time
DELAY_TIME="$SCREENSHOT_DELAY"
export DELAY_TIME

# Preview audio time
PREVIEW_AUDIO_TIME="10"
export PREVIEW_AUDIO_TIME

# Preview video time
PREVIEW_VIDEO_TIME="[10]"
export PREVIEW_VIDEO_TIME

# Dunst background
MY_COLOR_BG="$COLOR_BG"
export MY_COLOR_BG

# Dunst text
MY_COLOR_ICON="$COLOR_ICON"
export MY_COLOR_ICON

# Dunst frame
MY_COLOR_ALERT="$COLOR_ALERT"
export MY_COLOR_ALERT

#------------------------------------------------------#
# Keywords
#------------------------------------------------------#

# Youtube search
KEYWORD01="yt"
export KEYWORD01 

# Google translate 
KEYWORD02="t"
export KEYWORD02 

# Find local files
KEYWORD03="f"
export KEYWORD03

# Find local files (history)
KEYWORD04="fh"
export KEYWORD04 

# QR Codes
KEYWORD05="qr"
export KEYWORD05

# Calculator
KEYWORD06="="
export KEYWORD06

# Calculator history
KEYWORD07="=h"
export KEYWORD07

# Dictionary
KEYWORD08="di"
export KEYWORD08

# Shutdown
KEYWORD09="shutdown"
export KEYWORD09

# Restart
KEYWORD10="restart"
export KEYWORD10

# Mute volume toggle
KEYWORD11="mute"
export KEYWORD11

# Set the volume
KEYWORD12="v"
export KEYWORD12

# Text to speech (speak)
KEYWORD13="spk"
export KEYWORD13

# Text to speech (file)
KEYWORD14="text"
export KEYWORD14

# Text to speech (kill)
KEYWORD15="ttsk"
export KEYWORD15

# Kill processes with killall
KEYWORD16="kill"
export KEYWORD16

# Kill processes with pkill
KEYWORD17="ki"
export KEYWORD17

# Spell checker
KEYWORD18="sp"
export KEYWORD18

# Password generator
KEYWORD19="pass"
export KEYWORD19

# QR Code image notification
KEYWORD20="view"
export KEYWORD20

# Google search
KEYWORD21="g"
export KEYWORD21

# Create PDF
KEYWORD22="pdf"
export KEYWORD22

# PDF open file
KEYWORD23="pdfo"
export KEYWORD23

# PDF print
KEYWORD24="pdfp"
export KEYWORD24

# Record screen (screencast)
KEYWORD25="rec"
export KEYWORD25

# Stop screen recording (screencast)
KEYWORD26="stop"
export KEYWORD26

# Play screen recording (screencast)
KEYWORD27="play"
export KEYWORD27

# Take fullscreen screenshot
KEYWORD28="full"
export KEYWORD28

# Select screenshot
KEYWORD29="select"
export KEYWORD29

# Delay screenshot
KEYWORD30="delay"
export KEYWORD30

# Open last screenshot
KEYWORD31="open"
export KEYWORD31

# Lock machine
KEYWORD32="lock"
export KEYWORD32

# Exec terminal commands
KEYWORD33="#"
export KEYWORD33

# Resize image
KEYWORD34="res"
export KEYWORD34

# Emoji
KEYWORD35="emo"
export KEYWORD35

# One line note
KEYWORD36="n"
export KEYWORD36

# One line note history
KEYWORD37="nh"
export KEYWORD37

# Decode qr code
KEYWORD38="qrd"
export KEYWORD38

# Keywords help page
KEYWORD39="keys"
export KEYWORD39

# Color Picker
KEYWORD40="pick"
export KEYWORD40

# RGB to HEX
KEYWORD41="rgb"
export KEYWORD41

# HEX to RGB
KEYWORD42="hex"
export KEYWORD42

# Brave command palette
KEYWORD43="br"
export KEYWORD43

# Control music players with playerctl
KEYWORD44="p"
export KEYWORD44

# Volume status
KEYWORD45="status"
export KEYWORD45

# Open terminal here in thunar
KEYWORD46="here"
export KEYWORD46

# Change uppercase text to lowercase
KEYWORD47="lo"
export KEYWORD47

# Timezone los angeles
KEYWORD48="la"
export KEYWORD48

# Timezone new york
KEYWORD49="ny"
export KEYWORD49

# Timezone in Sydney
KEYWORD50="sy"
export KEYWORD50

# All timezones
KEYWORD51="zones"
export KEYWORD51

# Notifications
KEYWORD52="not"
export KEYWORD52

# Microphone
KEYWORD53="mic"
export KEYWORD53

# Zip files
KEYWORD54="com"
export KEYWORD54

# Plays videos in mpv via http link
KEYWORD55="pv"
export KEYWORD55

# Last modified files
KEYWORD56="mod"
export KEYWORD56

# Record from microphone
KEYWORD57="rmic"
export KEYWORD57

# Radio
KEYWORD58="radio"
export KEYWORD58

# Duckduckgo web search
KEYWORD59="ddg"
export KEYWORD59

# Reddit web search
KEYWORD60="rd"
export KEYWORD60

# Download video
KEYWORD61="do"
export KEYWORD61

# Wikipedia web search
KEYWORD62="wik"
export KEYWORD62

# Empty trash
KEYWORD63="trash"
export KEYWORD63

# Open KEYWORDs config
KEYWORD64="config"
export KEYWORD64

# Audacious player
KEYWORD65="au"
export KEYWORD65

# Audacious player playlist
KEYWORD66="aup"
export KEYWORD66

# Emoji search
KEYWORD67="em"
export KEYWORD67
