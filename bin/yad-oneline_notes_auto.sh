#!/bin/bash

# Program command: yad-oneline_notes_auto.sh
# Description: Views and creates oneline notes.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 25-04-2024
# Program_license: GPL 3.0
# Dependencies: yad, wl-clipboard, festival, gawk, material icons, qrencode, jetbrainsmono nerd font

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#---------------------------------------------#
# Error checking
#---------------------------------------------#

set -e

#---------------------------------------------#
# Checks if you are running a wayland session
#---------------------------------------------#

   if [[ "$(echo $WAYLAND_DISPLAY)" == "" ]]; then
     yad --error \
     --width 500 \
     --title "Wayland session not found!" \
     --text "Cannot run color picker, wayland only tool!"
     exit 1
   else
     :
   fi

#---------------------------------------------#
# Create files and directories
#---------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/qr_codes ]; then
     :
   else
     mkdir "$HOME"/Pictures/qr_codes
   fi

   if [ -d "$HOME"/Pictures/.temqrcodes ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes
   fi

   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ -f "$HOME"/Documents/mainprefix/quicknotes.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/quicknotes.txt
   fi

#---------------------------------------------#
# Note text file
#---------------------------------------------#

note_file="$HOME/Documents/mainprefix/quicknotes.txt"
export note_file

#---------------------------------------------#
# Colors
#---------------------------------------------#

# Text
color1="<span color='#929292' font_family='Cascadia Mono' font='14' rise='0pt'>"
export color1
# Icons
color2="<span color='#FFFFFF' font_family='Material Icons' font='22' rise='0pt'>"
export color2
# Close icon
color3="<span color='#FF002E' font_family='Material Icons' font='22' rise='0pt'>"
export color3
# Icons
color4="<span color='#FFFFFF' font_family='JetbrainsMono Nerd Font' font='22' rise='0pt'>"
export color4
# Color end
end="</span>"
export end

#---------------------------------------------#
# Remove spaces from text file
#---------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/quicknotes.txt

#---------------------------------------------#
# Remove duplicate strings
#---------------------------------------------#

gawk -i inplace '!seen[$0]++' "$HOME"/Documents/mainprefix/quicknotes.txt

#---------------------------------------------#
# Copy note to temp file
#---------------------------------------------#

copy_note() {

echo "$1" > "$HOME"/Documents/.yad_oneline_note_temp.txt
	
}
export -f copy_note

#---------------------------------------------#
# Copy note to clipboard
#---------------------------------------------#

clip_note() {
  
cat "$HOME"/Documents/.yad_oneline_note_temp.txt | wl-copy -n
	
}
export -f clip_note

#---------------------------------------------#
# Text to speech
#---------------------------------------------#

text_to_speech() {

cat "$HOME"/Documents/.yad_oneline_note_temp.txt | festival --tts

}
export -f text_to_speech

#---------------------------------------------#
# Add new note
#---------------------------------------------#

update_note() {

update_choice=$(GTK_THEME="$ALL_YAD_GTK" yad --entry --width=500 --height=100 >> "$note_file")
[[ -z "$update_choice" ]] && exit 0
	
}
export -f update_note

#---------------------------------------------#
# Create QR Code from note
#---------------------------------------------#

create_qr_code() {

qrcode_text=$(cat "$HOME"/Documents/.yad_oneline_note_temp.txt)
cat "$HOME"/Documents/.yad_oneline_note_temp.txt | tr -d '\n' | \
qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
notification=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
notify-send -i "$notification" "$qrcode_text"

}
export -f create_qr_code

#---------------------------------------------#
# Open text notes file
#---------------------------------------------#

open_note() {

xdg-open "$HOME"/Documents/mainprefix/quicknotes.txt
killall yad
	
}
export -f open_note

#---------------------------------------------#
# Text information
#---------------------------------------------#

text_info() {

echo "${color1}Double click on text line then press button.${end}
${color1}When adding new note, just click button, no double click${end}
${color1}To search for a note type control+f${end}\n"
	
}
export -f text_info

#---------------------------------------------#
# Main
#---------------------------------------------#

main() {

tail -f "$note_file" | GTK_THEME="$ALL_YAD_GTK" yad --list \
--title="Yad notes" \
--column="note" \
--search-column=1 \
--text="$(text_info)" \
--text-align="center" \
--no-headers \
--borders=20 \
--width=700 \
--separator= \
--height=550 \
--border=20 \
--buttons-layout="spread" \
--dclick-action='/bin/bash -c "copy_note %s"' \
--button="${color2}${end}":"bash -c create_qr_code" \
--button="${color2}${end}":"bash -c clip_note" \
--button="${color4}󰔊${end}":"bash -c text_to_speech" \
--button="${color4}󰎝${end}":"bash -c update_note" \
--button="${color2}${end}":"bash -c open_note" \
--button="${color3}${end}:0" >> "$note_file" --tail

}
export -f main
main

#---------------------------------------------#
# Unset variables and functions
#---------------------------------------------#

unset note_file
unset update_note
unset main
unset color1
unset end
unset copy_note
unset clip_note
unset text_to_speech
unset text_info
unset color2
unset color3
unset color4
unset create_qr_code
unset open_note
