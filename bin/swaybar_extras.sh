#!/bin/bash

# Program command: swaybar_extras.sh
# Description: This gives you extra commands to use with swaybar.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 01-04-2024
# Program_license: GPL 3.0
# Dependencies: yad

#---------------------------------------------#
# Swaywm config
#---------------------------------------------#

my_swaywm_config="$HOME/.config/sway/sway_wallpapers"
export my_swaywm_config

#---------------------------------------------#
# Colors
#---------------------------------------------#

# Keybinding
color01="<span color='#16FF00' font_family='JetBrainsMono Nerd Font' font='14'>"
# Text
color02="<span color='#FFFFFF' font_family='JetBrainsMono Nerd Font' font='14'>"
# Color end
end="</span>"

#---------------------------------------------#
# Change gtk themes
#---------------------------------------------#

change_gtk_theme() {

pkill -f yad
sleep 0.5
setsid bash ~/bin/sway_gtk_themes.sh >/dev/null 2>&1 & disown

	
}
export -f change_gtk_theme

#---------------------------------------------#
# Custom launcher
#---------------------------------------------#

custom_launcher() {

pkill -f yad
sleep 0.5
setsid bash ~/bin/yad_keywords_launcher.sh >/dev/null 2>&1 & disown
	
}
export -f custom_launcher

#---------------------------------------------#
# Random wallpaper
#---------------------------------------------#

change_wallpaper() {

wall_file=$(ls ~/Pictures/sway_wallpapers/ | shuf -n1)
sed -i "3s|^.*$|output * bg ~/Pictures/sway_wallpapers/$(echo "$wall_file") fill|" "$my_swaywm_config"
swaymsg reload

}
export -f change_wallpaper

#---------------------------------------------#
# Change to track title (i3blocks)
#---------------------------------------------#

track_title() {

echo "on" > ~/Documents/.switch_mode_i3blocks.txt
killall yad
	
}
export -f track_title

#---------------------------------------------#
# Change to app title (i3blocks)
#---------------------------------------------#

app_title() {

echo "off" > ~/Documents/.switch_mode_i3blocks.txt
killall yad
	
}
export -f app_title

#---------------------------------------------#
# Change swaybar theme
#---------------------------------------------#

swaybar_theme() {

killall yad
sleep 0.5
setsid bash ~/bin/swaybar_themes.sh >/dev/null 2>&1 & disown

}
export -f swaybar_theme

#---------------------------------------------#
# Module info
#---------------------------------------------#

my_modules() {

text_info="Swaybar modules

Clickable actions

There are 5 clickable actions
Some modules might only use 2 actions.

1st module:
Shows program title and song title.

Supported music programs:
Mpv, Rhythmbox, Audacious, Cmus, Vlc, Youtube

Actions for 1st module:
Left click: Play-pause toggle
Right click: Next track
Middle click: Change between program title and song title.
Scroll down: Skip 1 minute ahead
Scroll up: Muted and unmute toggle

"

killall yad
sleep 0.5
echo "$text_info" | yad --text-info --width=1000 --height=500 \
--button="<span color='#FF0031' font_family='Material Icons' font='20'></span>":1
	
}
export -f my_modules

#---------------------------------------------#
# Update system
#---------------------------------------------#

update_system() {

killall yad
gnome-terminal -- /bin/bash -c "sudo apt update && sudo apt full-upgrade; $SHELL"

}
export -f update_system

#---------------------------------------------#
# App launcher
#---------------------------------------------#

app_launcher() {

killall yad
sleep 0.2
setsid wofi drun  >/dev/null 2>&1 & disown
	
}
export -f app_launcher

#---------------------------------------------#
# System info
#---------------------------------------------#

system_info() {

killall yad
gnome-terminal -- /bin/bash -c "neofetch; $SHELL"
	
}
export -f system_info

#---------------------------------------------#
# My yad scripts
#---------------------------------------------#

yad_guis() {

killall yad
setsid bash ~/bin/yad_hub_scripts.sh >/dev/null 2>&1 & disown

}
export -f yad_guis

#---------------------------------------------#
# Window switcher
#---------------------------------------------#

window_switch() {

killall yad
setsid bash ~/bin/sway-goto-windows.sh >/dev/null 2>&1 & disown
	
}
export -f window_switch

#---------------------------------------------#
# Bring window to current workspace
#---------------------------------------------#

bring_window() {

killall yad
setsid bash ~/bin/sway-bring-window.sh >/dev/null 2>&1 & disown
	
}
export -f bring_window

#---------------------------------------------#
# Move window to empty workspace
#---------------------------------------------#

move_window() {

killall yad
setsid bash ~/bin/sway_move_window_to_empty_workspace.sh >/dev/null 2>&1 & disown
	
}
export -f move_window

#---------------------------------------------#
# Go to empty workspace
#---------------------------------------------#

goto_empty_workspace() {

killall yad
setsid bash ~/bin/sway_goto_empty_workspace.sh >/dev/null 2>&1 & disown
	
}
export -f goto_empty_workspace

#---------------------------------------------#
# Autostart programs
#---------------------------------------------#

autostart_apps() {

killall yad
setsid wtype -M altgr -k a >/dev/null 2>&1 & disown
	
}
export -f autostart_apps

#---------------------------------------------#
# Set wallpaper
#---------------------------------------------#

set_wallpaper() {

killall yad
setsid bash ~/bin/sway_wallpapers.sh >/dev/null 2>&1 & disown

}
export -f set_wallpaper

#---------------------------------------------#
# Emojis
#---------------------------------------------#

yad_emoji() {

killall yad
setsid bash ~/bin/yad_emoji.sh >/dev/null 2>&1 & disown

}
export -f yad_emoji

#---------------------------------------------#
# Pdf Creator
#---------------------------------------------#

create_pdf() {

killall yad
setsid bash ~/bin/pdf_creator.sh >/dev/null 2>&1 & disown
	
}
export -f create_pdf

#---------------------------------------------#
# Wallpaper effects
#---------------------------------------------#

wall_effects() {

killall yad
setsid bash ~/bin/yad_wallpaper_effects.sh >/dev/null 2>&1 & disown

}
export -f wall_effects

#---------------------------------------------#
# Configs
#---------------------------------------------#

sway_configs() {

killall yad
setsid bash ~/bin/yad_configs.sh >/dev/null 2>&1 & disown

}
export -f sway_configs

#---------------------------------------------#
# Keyboard layout
#---------------------------------------------#

keyboard_layout() {

killall yad
setsid bash ~/bin/keyboard_layout.sh >/dev/null 2>&1 & disown

}
export -f keyboard_layout

#---------------------------------------------#
# Main
#---------------------------------------------#

main() {

GTK_THEME="alt-dialog22" yad --form --column="search" --width=900 --height=500 --columns=3 \
--borders=20 \
--text="<span font_family='JetBrainsMono Nerd Font' font='20'>Sway extras</span>
" \
--text-align="center" \
--field="<b>${color02}Random wallpaper ${end}</b>":fbtn '/bin/bash -c "change_wallpaper"' \
--field="<b>${color02}Set wallpaper ${color01}ALTGR+W${end}${end}</b>":fbtn '/bin/bash -c "set_wallpaper"' \
--field="<b>${color02}Wallpaper effects ${end}${color01}WIN+SHIFT+W${end}</b>":fbtn '/bin/bash -c "wall_effects"' \
--field="<b>${color02}Swaybar theme ${end}${color01}WIN+SHIFT+T${end}</b>":fbtn '/bin/bash -c "swaybar_theme"' \
--field="<b>${color02}Gtk theme ${end}${color01}WIN+T${end}</b>":fbtn '/bin/bash -c "change_gtk_theme"' \
--field="<b>${color02}App title (i3blocks) ${end}${color01}WIN+G${end}</b>":fbtn '/bin/bash -c "app_title"' \
--field="<b>${color02}Swaybar info ${end}</b>":fbtn '/bin/bash -c "my_modules"' \
--field="<b>${color02}Update system ${end}</b>":fbtn '/bin/bash -c "update_system"' \
--field="<b>${color02}Track title (i3blocks) ${end}${color01}WIN+SHIFT+G${end}</b>":fbtn '/bin/bash -c "track_title"' \
--field="<b>${color02}Window to empty workspace ${end}${color01}ALTGR+M${end}</b>":fbtn '/bin/bash -c "move_window"' \
--field="<b>${color02}Go to empty workspace ${end}${color01}ALTGR+E${end}</b>":fbtn '/bin/bash -c "goto_empty_workspace"' \
--field="<b>${color02}App launcher ${end}${color01}WIN+D${end}</b>":fbtn '/bin/bash -c "app_launcher"' \
--field="<b>${color02}Custom launcher ${end}${color01}WIN+PERIOD${end}</b>":fbtn '/bin/bash -c "custom_launcher"' \
--field="<b>${color02}System information ${end}</b>":fbtn '/bin/bash -c "system_info"' \
--field="<b>${color02}Yad GUI apps ${end}</b>":fbtn '/bin/bash -c "yad_guis"' \
--field="<b>${color02}Window switcher ${end}${color01}ALT+TAB${end}</b>":fbtn '/bin/bash -c "window_switch"' \
--field="<b>${color02}Bring window ${end}${color01}ALT+ESCAPE${end}</b>":fbtn '/bin/bash -c "bring_window"' \
--field="<b>${color02}Autostart programs ${end}${color01}ALTGR+A${end}</b>":fbtn '/bin/bash -c "autostart_apps"' \
--field="<b>${color02}Emoji's ${end}</b>":fbtn '/bin/bash -c "yad_emoji"' \
--field="<b>${color02}PDF Creator ${end}</b>":fbtn '/bin/bash -c "create_pdf"' \
--field="<b>${color02}Configuration files ${end}${color01}WIN+C${end}</b>":fbtn '/bin/bash -c "sway_configs"' \
--field="<b>${color02}Keyboard layout ${end}</b>":fbtn '/bin/bash -c "keyboard_layout"' \
--button="<span color='#FF0031' font_family='Material Icons' font='20'></span>":1
	
}
export -f main
main

#---------------------------------------------#
# Unset variables and functions
#---------------------------------------------#

unset main
unset change_wallpaper
unset my_swaywm_config
unset change_gtk_theme
unset app_title
unset track_title
unset swaybar_theme
unset my_modules
unset update_system
unset app_launcher
unset system_info
unset color001
unset color002
unset end_color
unset custom_launcher
unset yad_guis
unset window_switch
unset bring_window
unset move_window
unset goto_empty_workspace
unset autostart_apps
unset set_wallpaper
unset yad_emoji
unset create_pdf
unset wall_effects
unset sway_configs
unset keyboard_layout
