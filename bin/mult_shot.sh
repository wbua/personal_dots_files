#!/bin/bash

#--------------------------------------------------------#
# General variables
#--------------------------------------------------------#

file_manager="thunar"
export file_manager

#--------------------------------------------------------#
# Kill new yad instance
#--------------------------------------------------------#

killall yad

#-------------------------------------------------------#
# Help (type qr_code_list.sh -h in prompt)
#-------------------------------------------------------#

help(){
cat << EOF
...

#-----------------------------------------#
# Script information
#-----------------------------------------#

Program command: mult_shot.sh
Description: Takes screenshots.
Contributors: John Mcgrath
Program_version: 1.0.0
Program updated: 18-08-2023
Program_license: GPL 3.0
Website: https://sourceforge.net/projects/jm-dots/files/
Dependencies: yad, maim, xclip, imagemagick

EOF
}

while getopts ":h" option; do
  case $option in
    h) # display help
    help
    exit;;
   \?) # incorrect option
    echo "Error: invalid option"
    exit;;
  esac
done

#--------------------------------------------------------#
# Create files and directories
#--------------------------------------------------------#
 
   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/.tempfullscrns ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempfullscrns
   fi
   
   if [ -d "$HOME"/Pictures/.tempfullscrns2 ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempfullscrns2
   fi

   if [ -d "$HOME"/Pictures/screenshots ]; then
     :
   else
     mkdir "$HOME"/Pictures/screenshots
   fi
   
   if [ -d "$HOME"/.cvssgtk ]; then
     :
   else
     mkdir "$HOME"/.cvssgtk
   fi

#-------------------------------------------------------#
# Change file manager
#-------------------------------------------------------#

change_file_manager() {

killall yad
sed -i "7s|^.*$|file_manager=\"$(GTK_THEME="Adwaita-dark" yad --entry --center --text="Change file manager")\"|" "$HOME"/bin/mult_shot.sh

}
export -f change_file_manager

#--------------------------------------------------------#
# Select screenshot in file manager
#--------------------------------------------------------#
  
file_dir() {

killall yad 
chosen=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -iname '*.*' | sort | cut -d '/' -f6-)
export chosen
[ -z "$chosen" ] && exit 0
"$file_manager" "$HOME"/Pictures/screenshots/"$chosen" 

}
export -f file_dir

#--------------------------------------------------------#
# Open last taken screenshot
#--------------------------------------------------------#

last_screenshot() {

killall yad ; find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open

}
export -f last_screenshot

#--------------------------------------------------------#
# Copy screenshot png to clipboard
#--------------------------------------------------------#

shot_to_clip() {

   killall yad 
   if [[ -z "$(ls -A "$HOME"/Pictures/.tempfullscrns)" ]]; then
     :
   elif [[ "$(ls -A "$HOME"/Pictures/.tempfullscrns)" == *".jpeg"* ]]; then	
     killall yad
     xclip -selection clipboard -t image/jpeg -i < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"  
   elif [[ "$(ls -A "$HOME"/Pictures/.tempfullscrns)" == *".png"* ]]; then
     killall yad
     xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)" 
   else
     :
   fi
   
}
export -f shot_to_clip

#-------------------------------------------------------#
# Settings button
#-------------------------------------------------------#

settings_menu() {

killall yad	
GTK_THEME="Yaru-dark" yad --form --column="search" --center --width=500 --height=200 --borders=20 --columns=2 \
--field="<b><span color='#FFFFFF' font_family='Monospace' font='14'>Change File manager</span></b>":fbtn '/bin/bash -c "change_file_manager"' \
--button="<span color='#FF0029' font_family='Monospace' font='20'></span>":1

}
export -f settings_menu

#--------------------------------------------------------#
# Yad settings
#--------------------------------------------------------#

choice=$(GTK_THEME="Yaru-dark" yad --form --center --width=400 --height=450 \
--image-on-top \
--borders=20 \
--separator= \
--image="$(find "$HOME"/.cvssgtk/ -type f -print0 | xargs -0 ls -tr | tail -n 1)" \
--field="":LBL "" \
--field="":LBL "" \
--field="<span color='#2AFF00' font_family='Monospace' font='14'>Convert </span>:CB" 'None !Greyscale !Negate ' \
--field="<span color='#2AFF00' font_family='Monospace' font='14'>Type</span>:CB" 'Full !None !Select !Delay ' \
--field="<span color='#2AFF00' font_family='Monospace' font='14'>File</span>:CB" 'Png !Jpg !None ' \
--field="":LBL "" \
--field="":LBL "" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='23'></span>:/bin/bash -c 'settings_menu'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='23'></span>:/bin/bash -c 'file_dir'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='20'></span>:/bin/bash -c 'last_screenshot'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='20'></span>:/bin/bash -c 'shot_to_clip'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='20'></span>":0 \
--button="<span color='#FF0029' font_family='Material Icons' font='20'></span>":1)
export choice
  
#--------------------------------------------------------#
# Main
#--------------------------------------------------------#

main() {
	 
   # Section 1
     
   # Covert:None, Type:Fullscreen, File:Png
   if [[ "$choice" == *"None"* && "$choice" == *"Full"* && "$choice" == *"Png"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --hidecursor | tee "$HOME/Pictures/.tempfullscrns/$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
   # Covert:None, Type:Select, File:Png
   elif [[ "$choice" == *"None"* && "$choice" == *"Select"* && "$choice" == *"Png"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --select --hidecursor | tee "$HOME/Pictures/.tempfullscrns/$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x288 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
   # Covert:None, Type:Delay, File:Png
   elif [[ "$choice" == *"None"* && "$choice" == *"Delay"* && "$choice" == *"Png"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     maim --hidecursor --delay=5 | tee "$HOME/Pictures/.tempfullscrns/$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
        
   # Section 2
    
   # Covert:None, Type:Fullscreen, File:Jpg  
   elif [[ "$choice" == *"None"* && "$choice" == *"Full"* && "$choice" == *"Jpg"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --hidecursor ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
   # Covert:None, Type:Select, File:Jpg
   elif [[ "$choice" == *"None"* && "$choice" == *"Select"* && "$choice" == *"Jpg"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --select --hidecursor ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x288 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
   # Covert:None, Type:Delay, File:Jpg
   elif [[ "$choice" == *"None"* && "$choice" == *"Delay"* && "$choice" == *"Jpg"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     maim --hidecursor --delay=5 ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
      
   # Section 3
   
   # Covert:Greyscale, Type:Full, File:Png
   elif [[ "$choice" == *"Greyscale"* && "$choice" == *"Full"* && "$choice" == *"Png"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/Pictures/.tempfullscrns2/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --hidecursor ~/Pictures/.tempfullscrns2/"$(date +'%d-%m-%Y-%H%M%S')".png
     convert "$HOME"/Pictures/.tempfullscrns2/*.* -colorspace Gray "$HOME"/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
   # Covert:Greyscale, Type:Select, File:Png
   elif [[ "$choice" == *"Greyscale"* && "$choice" == *"Select"* && "$choice" == *"Png"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/Pictures/.tempfullscrns2/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --select --hidecursor ~/Pictures/.tempfullscrns2/"$(date +'%d-%m-%Y-%H%M%S')".png
     convert "$HOME"/Pictures/.tempfullscrns2/*.* -colorspace Gray "$HOME"/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x288 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
   # Covert:Greyscale, Type:Delay, File:Png
   elif [[ "$choice" == *"Greyscale"* && "$choice" == *"Delay"* && "$choice" == *"Png"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/Pictures/.tempfullscrns2/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --hidecursor --delay=5 ~/Pictures/.tempfullscrns2/"$(date +'%d-%m-%Y-%H%M%S')".png
     convert "$HOME"/Pictures/.tempfullscrns2/*.* -colorspace Gray "$HOME"/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
     
   # Section 4
   
   # Covert:Greyscale, Type:Full, File:Jpg
   elif [[ "$choice" == *"Greyscale"* && "$choice" == *"Full"* && "$choice" == *"Jpg"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/Pictures/.tempfullscrns2/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --hidecursor ~/Pictures/.tempfullscrns2/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns2/*.* -colorspace Gray "$HOME"/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
   # Covert:Greyscale, Type:Select, File:Jpg
   elif [[ "$choice" == *"Greyscale"* && "$choice" == *"Select"* && "$choice" == *"Jpg"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/Pictures/.tempfullscrns2/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --select --hidecursor ~/Pictures/.tempfullscrns2/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns2/*.* -colorspace Gray "$HOME"/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x288 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
   # Covert:Greyscale, Type:Delay, File:Jpg
   elif [[ "$choice" == *"Greyscale"* && "$choice" == *"Delay"* && "$choice" == *"Jpg"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/Pictures/.tempfullscrns2/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --hidecursor --delay=5 ~/Pictures/.tempfullscrns2/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns2/*.* -colorspace Gray "$HOME"/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh  
     
   # Section 5
   
   # Covert:Negate, Type:Full, File:Png
   elif [[ "$choice" == *"Negate"* && "$choice" == *"Full"* && "$choice" == *"Png"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/Pictures/.tempfullscrns2/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --hidecursor ~/Pictures/.tempfullscrns2/"$(date +'%d-%m-%Y-%H%M%S')".png
     convert "$HOME"/Pictures/.tempfullscrns2/*.* -channel RGB -negate "$HOME"/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
   # Covert:Negate, Type:Select, File:Png 
   elif [[ "$choice" == *"Negate"* && "$choice" == *"Select"* && "$choice" == *"Png"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/Pictures/.tempfullscrns2/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --select --hidecursor ~/Pictures/.tempfullscrns2/"$(date +'%d-%m-%Y-%H%M%S')".png
     convert "$HOME"/Pictures/.tempfullscrns2/*.* -channel RGB -negate "$HOME"/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
   # Covert:Negate, Type:Delay, File:Png
   elif [[ "$choice" == *"Negate"* && "$choice" == *"Delay"* && "$choice" == *"Png"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/Pictures/.tempfullscrns2/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --hidecursor --delay=5 ~/Pictures/.tempfullscrns2/"$(date +'%d-%m-%Y-%H%M%S')".png
     convert "$HOME"/Pictures/.tempfullscrns2/*.* -channel RGB -negate "$HOME"/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
     
   # Section 6
   
   # Covert:Negate, Type:Full, File:Jpg
   elif [[ "$choice" == *"Negate"* && "$choice" == *"Full"* && "$choice" == *"Jpg"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/Pictures/.tempfullscrns2/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --hidecursor ~/Pictures/.tempfullscrns2/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns2/*.* -channel RGB -negate "$HOME"/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
   # Covert:Negate, Type:Select, File:Jpg 
   elif [[ "$choice" == *"Negate"* && "$choice" == *"Select"* && "$choice" == *"Jpg"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/Pictures/.tempfullscrns2/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --select --hidecursor ~/Pictures/.tempfullscrns2/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns2/*.* -channel RGB -negate "$HOME"/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
   # Covert:Negate, Type:Delay, File:Jpg
   elif [[ "$choice" == *"Negate"* && "$choice" == *"Delay"* && "$choice" == *"Jpg"* ]]; then
     killall yad
     rm -f "$HOME"/Pictures/.tempfullscrns/*.*
     rm -f "$HOME"/Pictures/.tempfullscrns2/*.*
     rm -f "$HOME"/.cvssgtk/*.*
     sleep 1
     maim --hidecursor --delay=5 ~/Pictures/.tempfullscrns2/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns2/*.* -channel RGB -negate "$HOME"/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".jpeg
     convert "$HOME"/Pictures/.tempfullscrns/*.* -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
     cp "$HOME"/Pictures/.tempfullscrns/*.* "$HOME"/Pictures/screenshots/
     mult_shot.sh
   else
     :
   fi

}
export -f main
main

#--------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------#

unset choice
unset main
unset shot_to_clip
unset last_screenshot
unset file_dir
unset file_manager
unset change_file_manager
unset settings_menu
unset chosen
