#!/bin/bash

# Program command: yad_online_search.sh 
# Description: Online searches.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 02-03-2025
# Program_license: GPL 3.0
# Dependencies: yad

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
source "$I3BLOCKS_THEMES_CONFIG"

#-----------------------------------------------------------#
# User preferences
#-----------------------------------------------------------#

# Browser
MY_BROWSER="$MAIN_BROWSER"
export MY_BROWSER

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Search engine
MY_ENGINE="$MAIN_ENGINE_SEARCH"
export MY_ENGINE

# Previous web search
MY_PREVIOUS="$PREVIOUS_ONLINE_SEARCH"
export MY_PREVIOUS

# Current search engine
MY_COLOR="$COLOR_ICON"
export MY_COLOR

# Current search engine
curr_color="<span color='$MY_COLOR' font_family='Inter' weight='bold' font='14' rise='0'>"
export curr_color

# Color end
color_end="</span>"
export color_end

#-----------------------------------------------------------#
# Engine list
#-----------------------------------------------------------#

engine_list() {

echo "https://www.google.co.uk/search?q="
echo "https://duckduckgo.com/?q="
echo "https://www.bing.com/search?q="
echo "https://search.yahoo.com/search?q="

	
}
export -f engine_list

#-----------------------------------------------------------#
# Change engine
#-----------------------------------------------------------#

change_engine() {

killall yad
engine_choice=$(engine_list | GTK_THEME="$MY_GTK_THEME" yad --list \
 --column="List" \
 --width=700 \
 --separator= \
 --height=400 \
 --button="Ok":0 \
 --button="Exit":1)
[ -z "$engine_choice" ] && exit 0

sed -i "s|.*\(MAIN_ENGINE_SEARCH=\).*|MAIN_ENGINE_SEARCH=\"$(printf '%s' "$engine_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

}
export -f change_engine

#-----------------------------------------------------------#
# Current engine
#-----------------------------------------------------------#

current_engine() {

   if [[ "$MY_ENGINE" == *"google"* ]]; then
     echo "google"
   elif [[ "$MY_ENGINE" == *"duck"* ]]; then
     echo "duckduckgo"
   elif [[ "$MY_ENGINE" == *"bing"* ]]; then
     echo "bing"
   elif [[ "$MY_ENGINE" == *"yahoo"* ]]; then
     echo "yahoo"
   else
     :
   fi
	
}
export -f current_engine

#-----------------------------------------------------------#
# Main
#-----------------------------------------------------------#

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     my_choice=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
     --text-align="center" \
     --text="\nCurrent engine: ${curr_color}$(current_engine)${color_end}\nSearch: ${curr_color}$MY_PREVIOUS${color_end}\nEnter text for search...\n" \
     --width=600 \
     --borders=5 \
     --separator= \
     --button="_Engine:/bin/bash -c 'change_engine'" \
     --button="Exit":1) || exit
     [ -z "$my_choice" ] && exit 0
     web_search="$MY_ENGINE{}"
     sed -i "s|.*\(PREVIOUS_ONLINE_SEARCH=\).*|PREVIOUS_ONLINE_SEARCH=\"$(printf '%s' "$my_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
     echo "$my_choice" | xargs -I{} "$MY_BROWSER" "$web_search" > /dev/null && bash ~/bin/switch_to_browser.sh > /dev/null
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     my_choice=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
     --text-align="center" \
     --text="\nCurrent: ${curr_color}$(current_engine)${color_end}\nEnter text for search...\n" \
     --width=600 \
     --borders=5 \
     --separator= \
     --button="_Engine:/bin/bash -c 'change_engine'" \
     --button="Exit":1) || exit
     [ -z "$my_choice" ] && exit 0
     web_search="$MY_ENGINE{}"
     sed -i "s|.*\(PREVIOUS_ONLINE_SEARCH=\).*|PREVIOUS_ONLINE_SEARCH=\"$(printf '%s' "$my_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
     echo "$my_choice" | xargs -I{} "$MY_BROWSER" "$web_search" > /dev/null
   else
     :
   fi

#-----------------------------------------------------------#
# Unset variables and functions
#-----------------------------------------------------------#

unset MY_GTK_THEME
unset MY_BROWSER
unset MY_ENGINE
unset change_engine
unset engine_list
unset MY_COLOR
unset curr_color
unset color_end
unset current_engine
unset MY_PREVIOUS
