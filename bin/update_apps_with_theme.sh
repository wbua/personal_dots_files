#!/bin/bash

# Program command: update_apps_with_theme.sh
# Description: Programs that get themed.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 12-02-2025
# Program_license: GPL 3.0
# Dependencies: 

# User preferences
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Config for all theme colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

# Description color
MY_COLOR_DES="$COLOR_DES"

# Yad text selection color
MY_YAD_TXT="$YAD_TEXT_COLOR"

# Run launcher text selection color
RUN_LAUNCHER_SELECT_COLOR="$LAUNCHER_SELECT_TEXTCOLOR"

# Colors
MY_THEME_CONFIG="$I3BLOCKS_THEMES_CONFIG"

# Background
MY_COLOR_BG="$COLOR_BG"

# Yad background transparent
YAD_BG_TRANS="$YAD_TRANSPARENT_BG"

   if [[ "$check_icon" == " " && "$check_bg" == " " ]]; then
     sed -i "s|.*\(COLOR_ICON=\).*|COLOR_ICON=\"$(printf '%s' "3bff00")\"|" "$MY_THEME_CONFIG" || exit 1
     sed -i "s|.*\(COLOR_BG=\).*|COLOR_BG=\"$(printf '%s' "3bff00")\"|" "$MY_THEME_CONFIG" || exit 1
   else
     :
   fi

# Sway focused border color
sed -i "s|.*\(client.focused\).*|client.focused $(printf '%s' "${COLOR_ICON} ${COLOR_ICON} ${COLOR_BG}")|" ~/.config/sway/border_color || exit 1
# Sway unfocused border color
sed -i "s|.*\(client.unfocused\).*|client.unfocused $(printf '%s' "${UNFOCUSED_FILLER} ${COLOR_BG}")|" ~/.config/sway/border_color || exit 1

# Function to convert hex to decimal
hex_to_dec() {
  echo $((16#$1))
}

# Hex color input
hex_color="$MY_COLOR_BG"

# Strip the leading '#' if present
hex_color="${hex_color#'#'}"

# Extract RGB components
r=$(hex_to_dec ${hex_color:0:2})
g=$(hex_to_dec ${hex_color:2:2})
b=$(hex_to_dec ${hex_color:4:2})

# Default alpha value (change if needed)
a="$YAD_BG_TRANS"

# Output the rgba format
#echo "rgba(${r}, ${g}, ${b}, 0.${a})"

# Checks if yad is installed
if command -v yad &> /dev/null
then
    # Yad button focused color
    sed -i "s|.*\(define-color yad-button-color\).*|@define-color yad-button-color $(printf '%s' "${COLOR_ALERT};")|" ~/.local/share/themes/alt-dialog22/gtk-3.0/colors.css || exit 1 
    # Yad selection color
    sed -i "s|.*\(define-color yad-selection-color\).*|@define-color yad-selection-color $(printf '%s' "${COLOR_ICON};")|" ~/.local/share/themes/alt-dialog22/gtk-3.0/colors.css || exit 1
    # Yad text selection color
    sed -i "s|.*\(define-color yad-printtext-color\).*|@define-color yad-printtext-color $(printf '%s' "${MY_YAD_TXT};")|" ~/.local/share/themes/alt-dialog22/gtk-3.0/colors.css || exit 1
    # Yad background color
    sed -i "s|.*\(define-color yad-background-color\).*|@define-color yad-background-color $(printf '%s' "rgba(${r}, ${g}, ${b}, 0.${a});")|" ~/.local/share/themes/alt-dialog22/gtk-3.0/colors.css || exit 1
    # Yad background color 2
    sed -i "s|.*\(define-color yad-auxiliary-color\).*|@define-color yad-auxiliary-color $(printf '%s' "rgba(${r}, ${g}, ${b}, 0.${a});")|" ~/.local/share/themes/alt-dialog22/gtk-3.0/colors.css || exit 1
    # Yad powermenu button box shadow
    sed -i "s|.*\(define-color power-shadow-color\).*|@define-color power-shadow-color $(printf '%s' "${COLOR_ICON};")|" ~/.local/share/themes/alt-dialog25/gtk-3.0/colors.css || exit 1
    # Yad powermenu selection
    sed -i "s|.*\(define-color power-glowing-color\).*|@define-color power-glowing-color $(printf '%s' "${COLOR_ICON};")|" ~/.local/share/themes/alt-dialog25/gtk-3.0/colors.css || exit 1
    # Yad powermenu background
    sed -i "s|.*\(define-color power-background-color\).*|@define-color power-background-color $(printf '%s' "${COLOR_BG};")|" ~/.local/share/themes/alt-dialog25/gtk-3.0/colors.css || exit 1
    
else
    notify-send "Yad" "is not installed"
fi

# Checks if wofi is installed
if command -v wofi &> /dev/null
then
    # Wofi window border
    sed -i "s|.*\(\@define-color wofi-epsilon\).*|@define-color wofi-epsilon $(printf '%s' "${COLOR_ALERT};")|" ~/.config/wofi/colors.css || exit 1
    # Wofi select color
    sed -i "s|.*\(\@define-color wofi-omicron\).*|@define-color wofi-omicron $(printf '%s' "${COLOR_ICON};")|" ~/.config/wofi/colors.css || exit 1
    # Wofi entry color
    sed -i "s|.*\(\@define-color wofi-upsilon\).*|@define-color wofi-upsilon $(printf '%s' "${COLOR_MID};")|" ~/.config/wofi/colors.css || exit 1
    # Wofi background
    sed -i "s|.*\(\@define-color wofi-kappa\).*|@define-color wofi-kappa $(printf '%s' "${COLOR_BG};")|" ~/.config/wofi/colors.css || exit 1
    # Wofi text selection color
    sed -i "s|.*\(\@define-color wofi-lambda\).*|@define-color wofi-lambda $(printf '%s' "${RUN_LAUNCHER_SELECT_COLOR};")|" ~/.config/wofi/colors.css || exit 1
else
    notify-send "Wofi" "is not installed"
fi

# Checks if fuzzel is installed
if command -v fuzzel &> /dev/null
then
    # Fuzzel background
    sed -i "s|.*\(background=\).*|background=$(printf '%s' "${COLOR_BG}ff" | sed 's|#||')|" ~/.config/fuzzel/fuzzel.ini || exit 1
    # Fuzzel selection
    sed -i "s|.*\(selection=\).*|selection=$(printf '%s' "${COLOR_ICON}ff" | sed 's|#||')|" ~/.config/fuzzel/fuzzel.ini || exit 1
    # Fuzzel border
    sed -i "s|.*\(border=\).*|border=$(printf '%s' "${COLOR_ICON}ff" | sed 's|#||')|" ~/.config/fuzzel/fuzzel.ini || exit 1
    # Fuzzel text selection color
    sed -i "s|.*\(selection-text=\).*|selection-text=$(printf '%s' "${RUN_LAUNCHER_SELECT_COLOR}ff" | sed 's|#||')|" ~/.config/fuzzel/fuzzel.ini || exit 1    
else
    notify-send "Fuzzel" "is not installed"
fi

# Checks if tofi is installed
if command -v tofi &> /dev/null
then
    # Tofi background
    sed -i "s|.*\(background-color =\).*|background-color = $(printf '%s' "${COLOR_BG}FA" | sed 's|#||')|" ~/.config/tofi/themes/colors || exit 1
    # Tofi selection
    sed -i "s|.*\(selection-color =\).*|selection-color = $(printf '%s' "${COLOR_ICON}" | sed 's|#||')|" ~/.config/tofi/themes/colors || exit 1
    # Tofi border
    sed -i "s|.*\(border-color =\).*|border-color = $(printf '%s' "${COLOR_ALERT}" | sed 's|#||')|" ~/.config/tofi/themes/colors || exit 1
else
    notify-send "Tofi" "is not installed"
fi

# Checks if thunar is installed
if command -v thunar &> /dev/null
then
    # Thunar selection color
    sed -i "s|.*\(\@define-color thunar-alternative-color\).*|@define-color thunar-alternative-color $(printf '%s' "$COLOR_ICON");|" ~/.config/gtk-3.0/mytheme.css || exit 1
    # Thunar background color
    sed -i "s|.*\(\@define-color thunar-background-color\).*|@define-color thunar-background-color $(printf '%s' "$COLOR_BG");|" ~/.config/gtk-3.0/mytheme.css || exit 1
else
    notify-send "Thunar" "is not installed"
fi

# Checks if firefox is installed
if command -v firefox &> /dev/null
then
    bash ~/bin/firefox_userchrome.sh
else
    notify-send "Firefox" "is not installed"
fi

# Checks if dunst is installed
if command -v dunst &> /dev/null
then
    bash ~/bin/create_dunst_config.sh
else
    notify-send "Dunst" "is not installed"
fi

# Checks if foot is installed
if command -v foot &> /dev/null
then
    bash ~/bin/create_foot_colors.sh
else
    notify-send "Foot" "is not installed"
fi

# Checks if geany is installed
if command -v geany &> /dev/null
then
    bash ~/bin/create_geany_colors.sh
else
    notify-send "Geany" "is not installed"
fi

# Checks if rofi is installed
if command -v rofi &> /dev/null
then
    bash ~/bin/create_rofi_colors.sh
else
    notify-send "Rofi" "is not installed"
fi

# Create i3blocks config
bash ~/bin/create_i3blocks_config.sh

# Create yad css background
bash ~/bin/create_yad_css_background.sh

# Create rofi background
bash ~/bin/create_rofi_background.sh

# Reload sway
bash ~/bin/sway_load_settings.sh
