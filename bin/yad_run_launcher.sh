#!/bin/bash

# Program command: yad_run_launcher.sh
# Description: Type or select app to run in yad gui.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 27-04-2024
# Program_license: GPL 3.0
# Dependencies: yad, gawk

#----------------------------------------------------#
# Error checking
#----------------------------------------------------#

set -e

#----------------------------------------------------#
# Checks if your running a wayland session
#----------------------------------------------------#

   if [[ "$(echo $WAYLAND_DISPLAY)" == "" ]]; then
     yad --error \
     --width 500 \
     --title "Wayland session not found!" \
     --text "Run launcher error, wayland only tool!"
     exit 1
   else
     :
   fi

#----------------------------------------------------#
# Create files and directories
#----------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -f "$HOME"/Documents/apps.txt ]; then
     :
   else
     touch "$HOME"/Documents/apps.txt
   fi   

#----------------------------------------------------#
# Apps text file
#----------------------------------------------------#

my_apps_file="$HOME/Documents/apps.txt"
export my_apps_file

#----------------------------------------------------#
# Checks if apps text file is empty
#----------------------------------------------------#

   if [[ -z "$(cat ~/Documents/apps.txt )" ]]; then 
     echo "brave" >> "$my_apps_file"
   else
     :
   fi

#----------------------------------------------------#
# Removes spaces from text file
#----------------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/apps.txt

#----------------------------------------------------#
# Remove duplicate strings
#----------------------------------------------------#

gawk -i inplace '!seen[$0]++' "$HOME"/Documents/apps.txt

#----------------------------------------------------#
# Add app to text file
#----------------------------------------------------#

open_app_text() {

killall yad
xdg-open "$HOME"/Documents/apps.txt

}
export -f open_app_text

#----------------------------------------------------#
# Yad settings
#----------------------------------------------------#

launcher=$(GTK_THEME="Adwaita-dark" yad --entry \
--title="Run launcher" \
--width=600 \
--text-align="center" \
--height=100 \
--text="Run launcher" \
--center \
--editable \
--borders=20 \
--rest="$my_apps_file" \
--button="Text file:/bin/bash -c 'open_app_text'" \
--button="Cancel:1" \
--button="Execute:0")
export launcher
[[ -z "$launcher" ]] && exit

#----------------------------------------------------#
# Excutes program
#----------------------------------------------------#

   if [[ -z "$launcher" ]]; then
     :
   else
     /bin/bash -c "$launcher" || exit 1
   fi

#----------------------------------------------------#
# Unset variables and functions
#----------------------------------------------------#
  
unset launcher
unset my_apps_file
unset open_app_text
