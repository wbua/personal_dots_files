#!/bin/bash

# Program command: rofi_weather.sh
# Description: Show weather information.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 14-08-2024
# Program_license: GPL 3.0
# Dependencies: yad, wget

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#----------------------------------------------------------------#
# User preferences
#----------------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Weather api key
WEATHER_API_KEY="$MAIN_WEATHER_API_KEY"
export WEATHER_API_KEY

#----------------------------------------------------------------#
# Error checking
#----------------------------------------------------------------#

set -e

#----------------------------------------------------------------#
# Create file and directories
#----------------------------------------------------------------#
 
   if [ -d "$HOME"/Downloads ]; then
     :
   else
     mkdir "$HOME"/Downloads
   fi
 
   if [ -d "$HOME"/Downloads/.temp_weather ]; then
     :
   else
     mkdir "$HOME"/Downloads/.temp_weather
   fi  
      
#----------------------------------------------------------------#
# Colors
#----------------------------------------------------------------#

color10="<span color='#0EFF00' font_family='Inter' font='18' rise='0pt'>"
end="</span>"

#----------------------------------------------------------------#
# Check if weather directory is empty
#----------------------------------------------------------------#	
	
   if [[ -z "$(ls -A "$HOME"/Downloads/.temp_weather)" ]]; then
     notify-send "No weather data, press update"
   else
     :
   fi
     
#----------------------------------------------------------------#
# 3-day forecast for city
#----------------------------------------------------------------#

forecast_area=$(grep "3-day forecast for" "$HOME"/Downloads/.temp_weather/downloadedfile | \
sed -e 's|<description>||' -e 's|</description>||' | \
sed 's|from BBC Weather, including weather, temperature and wind information||')
export forecast_area
	
#----------------------------------------------------------------#
# Update and download weather data
#----------------------------------------------------------------#

weather_download() {

killall yad
rm -rf "$HOME"/Downloads/.temp_weather/* || exit 1
wget https://weather-broker-cdn.api.bbci.co.uk/en/forecast/rss/3day/"$WEATHER_API_KEY" -O "$HOME"/Downloads/.temp_weather/downloadedfile
notify-send	"Weather updated"	
	
}
export -f weather_download

#----------------------------------------------------------------#
# Parse weather data
#----------------------------------------------------------------#

weather_info() {

echo ""	
grep Today: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'
grep Tonight: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'		
grep Monday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'
grep Tuesday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'
grep Wednesday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'
grep Thursday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'
grep Friday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'
grep Saturday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'
grep Sunday: "$HOME"/Downloads/.temp_weather/downloadedfile | sed -e 's|<title>||' -e 's|</title>||'

}	
export -f weather_info

#----------------------------------------------------------------#
# Yad dialog
#----------------------------------------------------------------#

weather_info | GTK_THEME="$MY_GTK_THEME" yad --list \
--column="weather" \
--search-column=1 \
--regex-search \
--separator= \
--title="BBC Weather" \
--text="$forecast_area" \
--text-align="center" \
--borders=20 \
--no-headers \
--no-markup \
--width=1300 \
--height=400 \
--button="_Update:/bin/bash -c 'weather_download'" \
--button="_Exit":1

#----------------------------------------------------------------#
# Unset variables and functions
#----------------------------------------------------------------#

unset color10
unset end
unset weather_info
unset weather_download
unset forecast_area
unset WEATHER_API_KEY
unset MY_GTK_THEME
