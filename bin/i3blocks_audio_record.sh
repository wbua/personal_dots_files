#!/bin/bash

# Program command: i3blocks_audio_record.sh
# Description: Records audio with i3blocks module.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 04-03-2025
# Program_license: GPL 3.0
# Dependencies: yad, arecord, paplay

# List all audio sources (input devices) to identify their names:
# $ pactl list sources short

#Set the desired microphone as the default audio source using its name 
#(replace your-source-name with the actual name of the source):
# $ pactl set-default-source your-source-name

# To set the built-in microphone as the default:
# $ pactl set-default-source alsa_input.pci-0000_00_1b.0.analog-stereo

# To set the USB webcam microphone as the default:
# $ pactl set-default-source alsa_input.usb-046d_0825_12345678-02.analog-stereo

# Show the current default microphone
# $ pactl info | grep "Default Source"

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-----------------------------------------------------#
# Error checking
#-----------------------------------------------------#

set -e

#-----------------------------------------------------#
# User preferences
#-----------------------------------------------------#

MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Audio directory
MY_AUDIO_DIR="$MAIN_AUDIO_DIR"
export MY_AUDIO_DIR

# Audio player
MY_AUDIO_PLAYER="$MAIN_AUDIO_PLAYER"
export MY_AUDIO_PLAYER

# File manager
MY_FILE_MANAGER="$MAIN_FILE_MANAGER"
export MY_FILE_MANAGER

#-----------------------------------------------------#
# Create file and directories
#-----------------------------------------------------#

   if [ -d "$HOME"/Music ]; then
     :
   else
     mkdir "$HOME"/Music
   fi

   if [ -d "$HOME"/Music/.yadaudiorec ]; then
     :
   else
     mkdir "$HOME"/Music/.yadaudiorec
   fi
   
   if [ -d "$MAIN_AUDIO_DIR" ]; then
     :
   else
     notify-send "Recording directory does not exist!" && exit 1
   fi

#-----------------------------------------------------#
# Start recording
#-----------------------------------------------------#

start_record() {

killall yad
rm -f "$HOME"/Music/.yadaudiorec/*.*
arecord -f S16_LE -t wav "$HOME/Music/.yadaudiorec/audio_$(date '+%d%m%Y-%H%M-%S').wav"
	
}
export -f start_record

#-----------------------------------------------------#
# Stop recording
#-----------------------------------------------------#

stop_record() {

   if [[ "$(pidof arecord)" ]]; then
     killall arecord
     cp "$HOME"/Music/.yadaudiorec/*.* "$MY_AUDIO_DIR"
   elif [[ "$(pidof paplay)" ]]; then
     killall paplay
   else
     :
   fi
	
}
export -f stop_record

#-----------------------------------------------------#
# Open recording
#-----------------------------------------------------#

open_recording() {

audio_choice=$(find "$HOME"/Music/.yadaudiorec/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
paplay "$audio_choice"
	
}
export -f open_recording

#-----------------------------------------------------#
# Move to file manager
#-----------------------------------------------------#

move_to_file_manager() {

file_choice=$(find "$HOME"/Music/.yadaudiorec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 basename)

   if [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     "$MY_FILE_MANAGER" "$MY_AUDIO_DIR"/"$file_choice"
     bash ~/bin/switch_to_filemanager.sh
   elif [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     "$MY_FILE_MANAGER" "$MY_AUDIO_DIR"/"$file_choice"
   else
     :
   fi

}
export -f move_to_file_manager

#-----------------------------------------------------#
# Show in file manager
#-----------------------------------------------------#

show_in_file_manager() {

killall yad
file_choice=$(find "$HOME"/Music/.yadaudiorec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 basename)

   if [[ -f "$MY_AUDIO_DIR"/"$file_choice" ]]; then
     move_to_file_manager
   else
     notify-send "File does not exist" && exit 1
   fi
	
}
export -f show_in_file_manager

#-----------------------------------------------------#
# Pick device
#-----------------------------------------------------#

pick_device() {

killall yad

device_list=$(pactl list sources short | awk '{print $2}')

pick_choice=$(echo "$device_list" | GTK_THEME="$MY_GTK_THEME" yad --list \
 --column="Devices" \
 --width=600 \
 --height=600 \
 --separator= \
 --button="Ok":0 \
 --button="Exit":1)
[[ -z "$pick_choice" ]] && exit 0

#pactl set-default-source "$pick_choice"
echo "$pick_choice"
	
}
export -f pick_device

#-----------------------------------------------------#
# Main
#-----------------------------------------------------#

# Default device
default_choice=$(pactl info | grep "Default Source")

# Yad dialog
GTK_THEME="$MY_GTK_THEME" yad --form \
--width=500 \
--height=150 \
--columns=1 \
--borders=10 \
--text-align="center" \
--text="\nAudio recorder\n$default_choice\n" \
--buttons-layout="center" \
--button="File:/bin/bash -c 'show_in_file_manager'" \
--button="Record:/bin/bash -c 'start_record'" \
--button="Stop:/bin/bash -c 'stop_record'" \
--button="Play:/bin/bash -c 'open_recording'" \
--button="Select device:/bin/bash -c 'pick_device'" \
--button="Exit":1
   
#-----------------------------------------------------#
# Unset variables and functions
#-----------------------------------------------------#

unset MY_GTK_THEME
unset start_record
unset stop_record
unset open_recording
unset MY_AUDIO_DIR
unset MY_AUDIO_PLAYER
unset MY_FILE_MANAGER
unset show_in_file_manager
unset move_to_file_manager
unset pick_device
