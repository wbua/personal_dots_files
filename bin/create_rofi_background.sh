#!/bin/bash

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Color config
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

set -e

MY_WALLPAPER="$MAIN_SWAY_WALLPAPER"

   if [ -d "$HOME"/Pictures/.rofi_background ]; then
     :
   else
     mkdir "$HOME"/Pictures/.rofi_background
   fi

ROFI_DIR="$HOME/Pictures/.rofi_background"   

rm -f "$ROFI_DIR"/*.* || exit 1

setsid convert "$MY_WALLPAPER" -quality 50 "$ROFI_DIR"/rofi_background.jpg >/dev/null 2>&1 & disown

