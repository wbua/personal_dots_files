#!/bin/bash

# Program command: sway_wallpapers.sh
# Description: Sway wallpaper changer.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-01-2025
# Program_license: GPL 3.0
# Website: https://gitlab.com/wbua/
# Dependencies: yad, imagemagick, zenity

# Master config file
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#----------------------------------------------------#
# Error checking
#----------------------------------------------------#

set -e

#----------------------------------------------------#
# User preference
#----------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Text font
MY_TEXT_FONT="JetBrainsMono Nerd Font"
export MY_TEXT_FONT

# Wallpaper directory
WALLPAPER_DIR="$MAIN_WALLPAPER_DIR"
export WALLPAPER_DIR

# Current set sway wallpaper
MY_SWAY_WALLPAPER="$MAIN_SWAY_WALLPAPER"
export MY_SWAY_WALLPAPER

# Description color
MY_COLOR_DES="$COLOR_DES"
export MY_COLOR_DES

#---------------------------------------------------#
# Check description colors
#---------------------------------------------------#

   if [[ "$MY_COLOR_DES" == "#000000" ]]; then
     notify-send "Invert colors" "win shift x" && exit 1
   fi

#----------------------------------------------------#
# Create files and directories
#----------------------------------------------------#

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/walls_list ]; then
     :
   else
     mkdir "$HOME"/Pictures/walls_list
   fi

   if [ -d "$HOME"/Pictures/thumb_walls ]; then
     :
   else
     mkdir "$HOME"/Pictures/thumb_walls
   fi

   if [ "$WALLPAPER_DIR" == "disable" ]; then
     notify-send "Wallpapers has been disabled" && exit 1
   elif [ -d "$WALLPAPER_DIR" ]; then
     :
   else
     notify-send "Wallpaper directory does not exist!" && exit 1
   fi

   if [ -f "$HOME"/Pictures/walls_list/main_list.txt ]; then
     :
   else
     touch "$HOME"/Pictures/walls_list/main_list.txt
   fi

#----------------------------------------------------#
# Help file
#----------------------------------------------------#

help_file() {

echo "Flags avalilable:

Help: sway_wallpaper.sh -h
Set wallpaper: sway_wallpaper.sh -w /home/user/Pictures/wall01.png

You can set a wallpaper from any location."
exit 0
	
}
export -f help_file

#----------------------------------------------------#
# Set wallpaper using flag
#----------------------------------------------------#

flag_set_wallpaper() {

filechoice=$(echo "$filename")
sed -i "s|.*\(MAIN_SWAY_WALLPAPER=\).*|MAIN_SWAY_WALLPAPER=\"$(printf '%s' "$filechoice")\"|" ~/bin/sway_user_preferences.sh || exit 1
bash ~/bin/sway_load_settings.sh
exit 0
	
}
export -f flag_set_wallpaper

#----------------------------------------------------#
# flags
#----------------------------------------------------#

while getopts "hw:" flag; do
 case $flag in
   h) # Handle the -h flag
   # Display script help information
   help_file
   ;;
   w) # Handle the -v flag
   # Set wallpaper
   filename=$(echo "${@:2}") 
   flag_set_wallpaper
   ;;
   \?)
   # Handle invalid options
   ;;
 esac
done

#---------------------------------------------------#
# Check update colors script variable
#---------------------------------------------------#

   if [[ "$(grep UPDATE_COLORS_SCRIPT= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     notify-send "UPDATE_COLORS_SCRIPT variable is enable" && exit 1
   elif [[ "$(grep UPDATE_COLORS_SCRIPT= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     :
   else
     :
   fi

#----------------------------------------------------#
# Colors
#----------------------------------------------------#

# Titles
COLOR1=$(printf '%s' "<span color='#FFFFFF' font_family='$MY_TEXT_FONT' font='18' rise='0pt'>")
export COLOR1
# Buttons
COLOR2=$(printf '%s' "<span color='#10FF00' font_family='$MY_TEXT_FONT' font='15' rise='0pt'>")
export COLOR2
# Color end
END=$(printf '%s' "</span>")
export END

#----------------------------------------------------#
# Dialog list information
#----------------------------------------------------#

declare -a TABLE="($(< ~/Pictures/walls_list/main_list.txt))"

#----------------------------------------------------#
# Remove whitespace
#----------------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$HOME"/Pictures/walls_list/main_list.txt

#----------------------------------------------------#
# Help page
#----------------------------------------------------#

help_page() {

killall yad
info_page="What the buttons do!

${COLOR2}Random:${END} Selects a random wallpaper.
${COLOR2}Convert:${END} Add's all the wallpapers in wallpaper directory.
${COLOR2}Add:${END} Add a single wallpaper.
${COLOR2}Set:${END} Double click wallpaper and press the set button. This will change your background.
${COLOR2}Open:${END} Double click wallpaper and press the set button. This will open your wallpaper.
${COLOR2}Help:${END} Help page.
${COLOR2}Exit:${END} Exit from program.
"

echo "$info_page" | yad --list \
--column="help" \
--no-headers \
--height=700 \
--borders=20 \
--width=1100 \
--title="Help page" \
--text="\nHelp page\n" \
--text-align="center" \
--button="Exit":1
	
}
export -f help_page

#----------------------------------------------------#
# Add wallpaper one at a time
#----------------------------------------------------#

add_wallpaper() {

killall yad
add_choice=$(GTK_THEME="$MY_GTK_THEME" yad --form --title="Add wallpaper" \
--text="\nAdd wallpaper\nWallpapers must be in png format\n" \
--text-align="center" \
--center \
--align="left" \
--width=600 \
--height=500 \
--borders=20 \
--separator= \
--field="File":FL \
--button="Add":0 \
--button="Exit":1)
[[ -z "$add_choice" ]] && exit

echo "$add_choice" | tee ~/Pictures/add_temp_wall_image.txt
field1=$(cat ~/Pictures/add_temp_wall_image.txt)
thumb_path="$HOME/Pictures/thumb_walls"

   if [[ "$(file --mime-type "$field1")" == *"png"* ]]; then
     image_name=$(echo "$field1" | xargs -0 basename)
     image_name2=$(echo "$field1" | sed 's|.png||' | xargs -0 basename)
     cp "$field1" "$WALLPAPER_DIR"
     setsid convert "$field1" -resize 250x "$thumb_path"/"$image_name" >/dev/null 2>&1 & disown
     echo  "\"$thumb_path"/"$image_name\"" "\"$image_name2\"" >> "$HOME"/Pictures/walls_list/main_list.txt
     find "$WALLPAPER_DIR" -type f -iname '*.*' | sort -R | tee ~/Documents/.random_sort_alpha.txt
   else
    notify-send "No png image detected!" && exit 1
   fi
	
}
export -f add_wallpaper

#----------------------------------------------------#
# Convert all wallpapers in directory to thumbnails
#----------------------------------------------------#

convert_all() {

killall yad

# Remove existing main list file contents
truncate -s 0 ~/Pictures/walls_list/main_list.txt | tr -d '\r'

# Checks if there are jpgs in the wallpaper directory
file_check=$(find "$WALLPAPER_DIR" -type f -iname "*.jpg" | tail -n 1)

if [[ "$(file --mime-type "$file_check")" == *"jpeg"* ]]; then
  notify-send "Directory contains jpg!" && exit 1
else
  :
fi

# Checks if there are webp in the wallpaper directory
file_check=$(find "$WALLPAPER_DIR" -type f -iname "*.webp" | tail -n 1)

if [[ "$(file --mime-type "$file_check")" == *"webp"* ]]; then
  notify-send "Directory contains webp!" && exit 1
else
  :
fi

# Remove existing thumbnails from thumb walls
rm -f "$HOME"/Pictures/thumb_walls/*.*

# Copy wallpapers to thumbnail directory
cp "$WALLPAPER_DIR"/*.* "$HOME"/Pictures/thumb_walls/

# Copy thumbnails to text file
find "$HOME"/Pictures/thumb_walls/ -type f -iname '*.*' | sort | tee "$HOME"/Pictures/walls_list/.temp_list.txt | tr -d '\r'

# Removes tilde symbols
sed -i 's|~||g' "$HOME"/Pictures/walls_list/.temp_list.txt

# Puts double quotes around file paths
input_file="$HOME/Pictures/walls_list/.temp_list.txt"
output_file="$HOME/Pictures/walls_list/.quoted_file.txt"
 
while IFS= read -r line; do
  echo "\"$line\""
done < "$input_file" > "$output_file"

# Removes images extensions and removes file paths, leaving only filenames
remove_path=$(cat "$HOME"/Pictures/walls_list/.temp_list.txt | sed 's|.png||g' | cut -d'/' -f6-)
echo "$remove_path" | tee ~/Pictures/walls_list/.file_names.txt

# Puts doubles quotes around the filenames
input_filenames="$HOME/Pictures/walls_list/.file_names.txt"
output_quote_files="$HOME/Pictures/walls_list/.quoted_filenames.txt"

while IFS= read -r line; do
  echo "\"$line\""
done < "$input_filenames" > "$output_quote_files"

# Combines the file paths with the filenames into text file
paste "$output_file" "$output_quote_files" > "$HOME"/Pictures/walls_list/main_list.txt

# Non-printable
cat "$HOME"/Pictures/walls_list/main_list.txt | tr -d '\r'

# Resizes the thumbnails using zenity
find "$HOME"/Pictures/thumb_walls/ -type f -iname '*.png' -print0 | \
xargs -0 mogrify -format png -thumbnail 250x $1 | \
zenity --progress --width=500 --height=100 \
--title="Sway wallpapers" --text="Converting..." --auto-close \
--auto-kill --pulsate && zenity --info --width=500 --height=100 \
--title="Sway wallpapers" --text="Convert completed"

# Create new random wallpaper list
find "$WALLPAPER_DIR" -type f -iname '*.*' | sort -R | tee ~/Documents/.random_sort_alpha.txt

}
export -f convert_all

#----------------------------------------------------#
# Open wallpaper
#----------------------------------------------------#

open_wallpaper() {

killall yad
wall_file=$(cat "$HOME"/Documents/.walls_selected.txt)
xdg-open "$WALLPAPER_DIR"/"$wall_file".png
	
}
export -f open_wallpaper

#----------------------------------------------------#
# Selected random wallpaper
#----------------------------------------------------#

selected_random_choice() {

killall yad
ran_wall_file=$(ls "$WALLPAPER_DIR" | shuf -n1)
echo "$ran_wall_file" | tee "$HOME"/Documents/.walls_selected.txt
wall_file=$(cat "$HOME"/Documents/.walls_selected.txt)
sed -i 's|.png||g' ~/Documents/.walls_selected.txt
sed -i 's|$|.png|' ~/Documents/.walls_selected.txt
sed -i "s|.*\(MAIN_SWAY_WALLPAPER=\).*|MAIN_SWAY_WALLPAPER=\"$(printf '%s' "$WALLPAPER_DIR/$wall_file")\"|" ~/bin/sway_user_preferences.sh || exit 1

   if [[ "$(grep COLOR_AUTO= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     bash ~/bin/wallpaper_auto_colors.sh
   elif [[ "$(grep COLOR_AUTO= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     bash ~/bin/sway_load_settings.sh
   fi

}
export -f selected_random_choice

#----------------------------------------------------#
# Random wallpaper
#----------------------------------------------------#

random_wallpaper() {

check_wall_dir=$(echo "$MY_SWAY_WALLPAPER" | xargs -0 dirname)

   if [[ ! "$check_wall_dir" == "$WALLPAPER_DIR" ]]; then
     notify-send "Sway wallpaper path does not contain wallpaper directory path" "in sway_user_preferences.sh" && exit 1
   else
     selected_random_choice
   fi
	
}
export -f random_wallpaper

#----------------------------------------------------#
# Set wallpaper
#----------------------------------------------------#

set_wallpaper() {

killall yad
wall_file=$(cat "$HOME"/Documents/.walls_selected.txt)
sed -i 's|.png||g' ~/Documents/.walls_selected.txt
sed -i 's|$|.png|' ~/Documents/.walls_selected.txt

sed -i "s|.*\(MAIN_SWAY_WALLPAPER=\).*|MAIN_SWAY_WALLPAPER=\"$(printf '%s' "$WALLPAPER_DIR/${wall_file}.png")\"|" ~/bin/sway_user_preferences.sh || exit 1

   if [[ "$(grep COLOR_AUTO= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     bash ~/bin/wallpaper_auto_colors.sh
   elif [[ "$(grep COLOR_AUTO= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     bash ~/bin/sway_load_settings.sh
   fi
	
}
export -f set_wallpaper

#----------------------------------------------------#
# Main
#----------------------------------------------------#

main() {

# Dialog	
printf '%s\n' "${TABLE[@]}" | GTK_THEME="$MY_GTK_THEME" yad --list \
--search-column=2 \
--regex-search \
--buttons-layout="center" \
--title="Wallpapers" \
--text-align="center" \
--text="\n${COLOR1}Sway Wallpapers${END}\n$(echo "$MAIN_SWAY_WALLPAPER" | \
sed 's|.png||' | sed 's|.jpg||' | sed 's|.jpeg||' | xargs -0 basename)\n" \
--borders=40 \
--separator=' ' \
--width=820 \
--height=750 \
--dclick-action='/bin/bash -c "echo "$1" > "$HOME"/Documents/.walls_selected.txt"' \
--column="Wallpaper:IMG" \
--column="Name" \
--button="_Random:/bin/bash -c 'random_wallpaper'" \
--button="_Convert:/bin/bash -c 'convert_all'" \
--button="_Add:/bin/bash -c 'add_wallpaper'" \
--button="_Set:/bin/bash -c 'set_wallpaper'" \
--button="_Open:/bin/bash -c 'open_wallpaper'" \
--button="_Help:/bin/bash -c 'help_page'" \
--button="_Exit":1

}
export -f main
main

#----------------------------------------------------#
# Unset variables and functions
#----------------------------------------------------#

unset choice
unset main
unset MY_GTK_THEME
unset COLOR1
unset COLOR2
unset END
unset MY_TEXT_FONT
unset TABLE
unset open_wallpaper
unset set_wallpaper
unset add_wallpaper
unset convert_all
unset random_wallpaper
unset help_page
unset WALLPAPER_DIR
unset help_file
unset flag_set_wallpaper
unset selected_random_choice
unset MY_SWAY_WALLPAPER
unset MY_COLOR_DES
