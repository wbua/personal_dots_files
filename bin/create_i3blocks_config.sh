#!/bin/bash

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Color config
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

set -e

SEPARATOR_WIDTH="$SEPARATOR_BLOCK_WIDTH"

SCRIPTS_VAR='$SCRIPT_DIR'

# i3blocks directory
i3blocks_dir="$HOME/.config/i3blocks"

# i3blocks file
i3blocks_file="config"


   if [ -d "$i3blocks_dir" ]; then
     :
   else
     notify-send "Foot directory" "does not exist" && exit 1
   fi

   if [ -f "$i3blocks_dir"/"$i3blocks_file" ]; then
     :
   else
     notify-send "Foot.ini file" "does not exist" && exit 1
   fi

# Create userchrome file
cat <<EOF > "$i3blocks_dir"/"$i3blocks_file" 

separator_block_width=$SEPARATOR_WIDTH
separator=" "
markup=pango

[recording]
command=$SCRIPTS_VAR/record_status.sh
interval=1

[title]
command=$SCRIPTS_VAR/title
interval=2

[quick_actions]
command=$SCRIPTS_VAR/quick_actions
interval=2

[chatbot]
command=$SCRIPTS_VAR/chatbot
interval=2

[quick_player]
command=$SCRIPTS_VAR/quick_player
interval=2

[multiple_workspace]
command=$SCRIPTS_VAR/multiple_workspaces
interval=2

[development_button]
command=$SCRIPTS_VAR/development_app
interval=2

[office_button]
command=$SCRIPTS_VAR/office_app
interval=2

[imageeditor_button]
command=$SCRIPTS_VAR/imageeditor_app
interval=2

[audioeditor_button]
command=$SCRIPTS_VAR/audioeditor_app
interval=2

[videoeditor_button]
command=$SCRIPTS_VAR/videoeditor_app
interval=2

[social_button]
command=$SCRIPTS_VAR/social_app
interval=2

[text_editor_button]
command=$SCRIPTS_VAR/texteditor_app
interval=2

[terminal_button]
command=$SCRIPTS_VAR/terminal_app
interval=2

[browser_button]
command=$SCRIPTS_VAR/browser_app
interval=2

[filemanager_button]
command=$SCRIPTS_VAR/filemanager_app
interval=2

[temporary_button]
command=$SCRIPTS_VAR/temporary_app
interval=2

[eyedropper]
command=$SCRIPTS_VAR/color_picker
interval=2

[apps]
command=$SCRIPTS_VAR/apps
interval=2

[Search files]
command=$SCRIPTS_VAR/search_files
interval=2

[layouts]
command=$SCRIPTS_VAR/sway_layout.sh
interval=2

[focused_workspace]
command=$SCRIPTS_VAR/workspaces
interval=2

[workspace_one]
command=$SCRIPTS_VAR/workspace_one
interval=2

[workspace_two]
command=$SCRIPTS_VAR/workspace_two
interval=2

[workspace_three]
command=$SCRIPTS_VAR/workspace_three
interval=2

[workspace_four]
command=$SCRIPTS_VAR/workspace_four
interval=2

[workspace_five]
command=$SCRIPTS_VAR/workspace_five
interval=2

[workspace_six]
command=$SCRIPTS_VAR/workspace_six
interval=2

[workspace_seven]
command=$SCRIPTS_VAR/workspace_seven
interval=2

[workspace_eight]
command=$SCRIPTS_VAR/workspace_eight
interval=2

[workspace_nine]
command=$SCRIPTS_VAR/workspace_nine
interval=2

[workspace_ten]
command=$SCRIPTS_VAR/workspace_ten
interval=2

[workspace_eleven]
command=$SCRIPTS_VAR/workspace_eleven
interval=2

[workspace_twelve]
command=$SCRIPTS_VAR/workspace_twelve
interval=2

[bri_vol_mic]
command=$SCRIPTS_VAR/brightness_volume_microphone
interval=1

[bright]
command=$SCRIPTS_VAR/brightness
interval=2

[volume]
command=$SCRIPTS_VAR/my_volume
interval=1

[microphone]
command=$SCRIPTS_VAR/second_microphone
interval=2

[battery2]
command=$SCRIPTS_VAR/battery2
interval=30

[time]
command=$SCRIPTS_VAR/timem
interval=30

[date]
command=$SCRIPTS_VAR/datem
interval=30

[weather_wttr]
command=$SCRIPTS_VAR/swaywm_weather
interval=3

[night-light]
command=$SCRIPTS_VAR/night_light
interval=3

[notifications]
command=$SCRIPTS_VAR/notifications
interval=2

[clickme]
command=$SCRIPTS_VAR/keys
interval=50

[my_wiki]
command=$SCRIPTS_VAR/my_wiki
interval=30

[wifi]
command=$SCRIPTS_VAR/wifi
interval=2

[auto_monitor]
command=$SCRIPTS_VAR/auto_monitor_off
interval=2

[extras]
command=$SCRIPTS_VAR/extras
interval=30

[keyboard]
command=$SCRIPTS_VAR/keyboard_layout
interval=2

[settings]
command=$SCRIPTS_VAR/settings_app
interval=30

[power]
command=$SCRIPTS_VAR/menu_power
interval=30

EOF
