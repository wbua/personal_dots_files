#!/bin/bash

# Command: wofi-launcher.sh
# Description: This script runs all of my scripts.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 17-12-2023
# Program_license: GPL 3.0
# Dependencies: rofi, mpv, playerctl, mpris, xclip

#----------------------------------------------#
# Software preferences
#----------------------------------------------#

# Browser
browser="brave"

# Text editor
text_editor="geany"

# File manager
file_manager="thunar"

#----------------------------------------------#
# Create files and directories
#----------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ -f "$HOME"/Documents/mainprefix/bookmarks.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/bookmarks.txt
   fi

   if [ -f "$HOME"/Documents/mainprefix/radiolinks.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/radiolinks.txt
   fi

   if [ -f "$HOME"/Documents/.temp_actions.txt ]; then
     :
   else
     touch "$HOME"/Documents/.temp_actions.txt
   fi

#----------------------------------------------#
# Colors
#----------------------------------------------#

# Description
color1="<span color='#8087A2' font_family='Cascadia Mono' font='14' weight='bold' rise='0pt'>"
# End color
end="</span>"

#----------------------------------------------#
# Wofi settings
#----------------------------------------------#

_wofi() {

rofi -dmenu -i -markup-rows -p ''
	
}

#----------------------------------------------#
# Radio
#----------------------------------------------#

# Radio menu
radio_menu() {

echo "list"
echo "play-pause"
echo "stop"
echo "stream-url"
echo "status"
echo "clip-title"
echo "file"
	
}

# Radio main
radio_main() {

launch=$(radio_menu | sort | _wofi | awk '{print $1}')
[ -z "$launch" ] && exit 0

case "$launch" in

 'list')
 plays_radio
 ;;

 'play-pause')
 play_toggle
 ;;

 'stop')
 kill_mpv
 ;;

 'stream-url')
 radio_url
 ;;

 'status')
 mpv_check
 ;;

 'clip-title')
 clip_title
 ;;

 'file')
 open_text
 ;;
 
 *)
  echo "Something went wrong!" | _wofi || exit
 ;;

esac
	
}

# Play-pause radio
play_toggle() {

   if [[ "$(pidof mpv)" ]]; then
     playerctl --player mpv play-pause
   else
     notify-send "Not playing!"
   fi
	
}

# Play radio stream
plays_radio() {

choice=$(cat "$HOME"/Documents/mainprefix/radiolinks.txt | _wofi | awk '{print $NF}')
[ -z "$choice" ] && exit 0
killall mpv
mpv --no-video "$choice"

}

# Stop radio
kill_mpv() {

killall mpv
	
}

# Currently playing url
radio_url() {

   if [[ "$(pidof mpv)" ]]; then
     notify-send "$(playerctl --player mpv metadata -f "{{xesam:url}}")"
   else
     notify-send "Not playing!"
   fi
	
}

# Check mpv status
mpv_check() {
	   
   if [[ "$(playerctl status -p mpv)" == 'Playing' ]]; then
     notify-send "Playing"
   elif	[[ "$(playerctl status -p mpv)" == 'Paused' ]]; then
	 notify-send "Paused"
   else
     notify-send "Not Playing"
   fi  
     
}

# Send track title to clipboard
clip_title() {

playerctl --player mpv metadata -f "{{title}}" | xclip -selection clipboard

}

# Open radio links text file
open_text() {
	
"$text_editor" "$HOME"/Documents/mainprefix/radiolinks.txt

}

#----------------------------------------------#
# Find files
#----------------------------------------------#

# Menu
file_menu() {

echo "copy"
echo "move"
echo "open"
echo "file"
echo "clip"
	
}

# Main
file_actions() {

choice=$(file_menu | sort | _wofi)

case "$choice" in

 'copy')
 :
 ;;

 'move')
 :
 ;;

 'open')
 file_open
 ;;

 'file')
 :
 ;;

 'clip')
 :
 ;;

 *)
 echo "Something went wrong!" | _wofi || exit 1
 ;;

esac 
	
}

# Main
file_main() {

dirs=( ~/Documents/ ~/Pictures/ ~/Videos/ ~/Downloads/ ~/Music/ )
find "${dirs[@]}" | tee ~/Documents/.all_files_in_dirs.txt
files_dirs=$(cat ~/Documents/.all_files_in_dirs.txt)
choice=$(echo "$files_dirs" | sort | _wofi)
[ -z "$choice" ] && exit 0
echo "$choice" | tee "$HOME"/Documents/.temp_actions.txt
file_actions
	
}

# Copy file to another location
file_copy() {

actions_file=$(cat "$HOME"/Documents/.temp_actions.txt)
dir_choice=$(find "${dirs[@]}" -maxdepth 9 -type d | _wofi)
[[ -z "$dir_choice" ]] && exit 0
echo "$dir_choice" | tee "$HOME"/Documents/.dir_actions.txt
cp "$actions_file" "$dir_choice"
	
}

# Move file to another location
file_move() {

actions_file=$(cat "$HOME"/Documents/.temp_actions.txt)
dir_choice=$(find ~ -maxdepth 9 -type d | _wofi)
[[ -z "$dir_choice" ]] && exit 0
echo "$dir_choice" | tee "$HOME"/Documents/.dir_actions.txt
mv "$actions_file" "$dir_choice"
	
}

# Open file
file_open() {

choice=$(cat "$HOME"/Documents/.temp_actions.txt)
[[ -z "$choice" ]] && exit 0
echo "$choice" | xargs xdg-open || echo "$choice" | xargs -d '\n' xdg-open 
	
}

# Open file in file manager
open_in_file_manager() {

cat "$HOME"/Documents/.temp_actions.txt | xargs -r -0 -d '\n' "$file_manager"
	
}

# Copy path to clipboard
file_path_to_clip() {

cat "$HOME"/Documents/.temp_actions.txt | xclip -selection clipboard
	
}

#----------------------------------------------#
# Bookmarks
#----------------------------------------------#

# Open bookmarks in browser
open_in_browser() {

launch=$(cat "$HOME"/Documents/mainprefix/bookmarks.txt | _wofi)
[ -z "$launch" ] && exit 0
echo "$launch" | awk '{print $NF}' | xargs -0 -d '\n' "$browser"
	
}

#----------------------------------------------#
# Menu
#----------------------------------------------#

menu () {

echo "bookmark ${color1} test1${end}"
echo "radio ${color1} test2${end}"
echo "find ${color1} test2${end}"
	
}

#----------------------------------------------#
# Main
#----------------------------------------------#

main() {

choice=$(menu | sort | _wofi | awk '{print $1}')
[ -z "$choice" ] && exit 0

case "$choice" in

 'bookmark')
 open_in_browser
 ;;

 'radio')
 radio_main
 ;;

 'find')
 file_main
 ;;

 *)
 echo "Something went wrong!" | _wofi || exit 1
 ;;

esac 
	
}
main
