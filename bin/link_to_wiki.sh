#!/bin/bash

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

MY_BROWSER="$MAIN_BROWSER"

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     setsid "$MY_BROWSER" https://gitlab.com/wbua/personal_dots_files/-/wikis/home && bash ~/bin/switch_to_browser.sh >/dev/null 2>&1 & disown && exit
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     setsid "$MY_BROWSER" https://gitlab.com/wbua/personal_dots_files/-/wikis/home >/dev/null 2>&1 & disown && exit
   fi


