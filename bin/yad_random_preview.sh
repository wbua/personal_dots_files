#!/bin/bash

# Program command: yad_random_preview.sh
# Description: Yad random wallpaper preview gui.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 14-09-2024
# Program_license: GPL 3.0
# Website: https://gitlab.com/wbua/
# Dependencies: yad, imagemagick

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#---------------------------------------------------#
# Error checking
#---------------------------------------------------#

set -e 

#---------------------------------------------------#
# User preferences
#---------------------------------------------------#

# Wallpaper directory
WALLPAPER_DIR="$MAIN_WALLPAPER_DIR"
export WALLPAPER_DIR

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Wallpaper file
TEMP_WALLPAPER_RANDOM="$MAIN_SWAY_WALLPAPER"
export TEMP_WALLPAPER_RANDOM 

# Temporary wallpaper
TEMP_RAN_WALL="$RANDOM_TEMPORARY_WALLPAPER"
export TEMP_RAN_WALL

#---------------------------------------------------#
# Create files and directories
#---------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/.tempfullscrns ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempfullscrns
   fi

   if [ -d "$HOME"/Pictures/.temp_image_convertion ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temp_image_convertion
   fi

   if [ -f "$HOME"/Documents/.temp_random_store.txt ]; then
     :
   else
     touch "$HOME"/Documents/.temp_random_store.txt
   fi

   if [ -f "$HOME"/Documents/.random_sort_alpha.txt ]; then
     :
   else
     touch "$HOME"/Documents/.random_sort_alpha.txt
   fi

#---------------------------------------------------#
# Creates text file from your wallpapers
#---------------------------------------------------#

   if [ -z "$HOME"/Documents/.random_sort_alpha.txt ]; then
     find "$WALLPAPER_DIR" -type f -iname '*.*' | sort -R | tee ~/Documents/.random_sort_alpha.txt
   else
     :
   fi

#---------------------------------------------------#
# Generate random list file
#---------------------------------------------------#

   if [[ "$(grep RANDOM_GENERATE_LIST= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     find "$WALLPAPER_DIR" -type f -iname '*.*' | sort -R | tee ~/Documents/.random_sort_alpha.txt
   elif [[ "$(grep RANDOM_GENERATE_LIST= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     :
   else
     :
   fi

#---------------------------------------------------#
# Check update colors script variable
#---------------------------------------------------#

   if [[ "$(grep UPDATE_COLORS_SCRIPT= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     notify-send "UPDATE_COLORS_SCRIPT variable is enable" && exit 1
   elif [[ "$(grep UPDATE_COLORS_SCRIPT= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     :
   else
     :
   fi

#---------------------------------------------------#
# Random wallpaper file does not exist
#---------------------------------------------------#

random_list_text() {

ACTION=$(dunstify --action="default,Reply" --action="forwardAction,Forward" "Random wallpaper does not exist in file" "Middle click notification!")

case "$ACTION" in
"default")
    cat ~/Documents/random_wallist_error.txt | wl-copy -n
    xdg-open ~/Documents/.random_sort_alpha.txt && exit 1
    ;;
"forwardAction")
    exit 1
    ;;
"2")
    exit 1
    ;;
esac

}
export -f random_list_text

#---------------------------------------------------#
# Checks if wallpaper file in list exists
#---------------------------------------------------#

check_wall_file_exists() {

random_wall_tempfile=$(cat "$HOME"/Documents/.temp_random_store.txt)

   if [ -f "$random_wall_tempfile" ]; then
     :
   else
     echo "$random_wall_tempfile" | tee ~/Documents/random_wallist_error.txt
     random_list_text      
   fi

}
export -f check_wall_file_exists

#---------------------------------------------------#
# Randomly picks wallpaper alphabetical
#---------------------------------------------------#

random_alphabetical() {

end_of_list=$(tail -n 1 ~/Documents/.random_sort_alpha.txt | tr -d '\n' | xargs -0 basename)
start_of_list=$(head -n 1 ~/Documents/.random_sort_alpha.txt)
wall_choice=$(echo "$TEMP_RAN_WALL" | xargs -0 basename)
temp_edit_random=$(echo "$TEMP_RAN_WALL" | xargs -0 basename)
mychoice=$(awk "/$temp_edit_random/{getline; print}" ~/Documents/.random_sort_alpha.txt)

   if [[ "$wall_choice" == "$end_of_list" ]]; then
     killall yad
     echo "$start_of_list" | tee ~/Documents/.temp_random_store.txt
     check_wall_file_exists
     convert "$(cat ~/Documents/.temp_random_store.txt)" -resize 512x512 "$HOME"/Pictures/.temp_image_convertion/current_screenshot.png   
     sed -i "s|.*\(RANDOM_TEMPORARY_WALLPAPER=\).*|RANDOM_TEMPORARY_WALLPAPER=\"$(printf '%s' "$(cat ~/Documents/.temp_random_store.txt)")\"|" ~/bin/sway_user_preferences.sh
     setsid bash ~/bin/yad_random_preview.sh >/dev/null 2>&1 & disown
   else
     killall yad
     echo "$mychoice" | tee ~/Documents/.temp_random_store.txt
     check_wall_file_exists
     convert "$(cat ~/Documents/.temp_random_store.txt)" -resize 512x512 "$HOME"/Pictures/.temp_image_convertion/current_screenshot.png   
     sed -i "s|.*\(RANDOM_TEMPORARY_WALLPAPER=\).*|RANDOM_TEMPORARY_WALLPAPER=\"$(printf '%s' "$(cat ~/Documents/.temp_random_store.txt)")\"|" ~/bin/sway_user_preferences.sh
     setsid bash ~/bin/yad_random_preview.sh >/dev/null 2>&1 & disown   
   fi

}
export -f random_alphabetical

#---------------------------------------------------#
# Choose another random wallpaper
#---------------------------------------------------#

random_wallpaper() {

   if [[ "$(grep RANDOM_WALLPAPER_SHUFFLE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     random_alphabetical
   elif [[ "$(grep RANDOM_WALLPAPER_SHUFFLE= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     killall yad
     wall_file=$(ls "$WALLPAPER_DIR" | shuf -n1)
     echo "$WALLPAPER_DIR/$wall_file" | tee ~/Documents/.temp_random_store.txt
     convert "$(cat ~/Documents/.temp_random_store.txt)" -resize 512x512 "$HOME"/Pictures/.temp_image_convertion/current_screenshot.png   
     setsid bash ~/bin/yad_random_preview.sh >/dev/null 2>&1 & disown
   else
     :
   fi

}
export -f random_wallpaper

#---------------------------------------------------#
# Set wallpaper
#---------------------------------------------------#

set_wallpaper() {

killall yad
temp_image=$(head ~/Documents/.temp_random_store.txt | xargs -0 basename)

   if [[ "$(grep COLOR_AUTO= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     sed -i "s|.*\(MAIN_SWAY_WALLPAPER=\).*|MAIN_SWAY_WALLPAPER=\"$(printf '%s' "$WALLPAPER_DIR/$temp_image")\"|" ~/bin/sway_user_preferences.sh || exit 1
     echo "$temp_image" > "$HOME"/Documents/.walls_selected.txt
     bash ~/bin/wallpaper_auto_colors.sh
     bash ~/bin/sway_load_settings.sh
   elif [[ "$(grep COLOR_AUTO= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     sed -i "s|.*\(MAIN_SWAY_WALLPAPER=\).*|MAIN_SWAY_WALLPAPER=\"$(printf '%s' "$WALLPAPER_DIR/$temp_image")\"|" ~/bin/sway_user_preferences.sh || exit 1
     bash ~/bin/sway_load_settings.sh
   fi
	
}
export -f set_wallpaper

#---------------------------------------------------#
# Create temp wallpaper thumbnail for preview
#---------------------------------------------------#

convert "$(cat ~/Documents/.temp_random_store.txt)" -resize 512x512 "$HOME"/Pictures/.temp_image_convertion/current_screenshot.png

#---------------------------------------------------#
# Yad dialog
#---------------------------------------------------#

GTK_THEME="$MY_GTK_THEME" yad --form \
--image-on-top \
--borders=20 \
--image="$HOME/Pictures/.temp_image_convertion/current_screenshot.png" \
--width=500 \
--height=500 \
--align="center" \
--field="\n$(cat ~/Documents/.temp_random_store.txt | xargs -0 basename)\n":LBL "" \
--button="_Set:/bin/bash -c 'set_wallpaper'" \
--button="_Random:/bin/bash -c 'random_wallpaper'" \
--button="_Exit"

#---------------------------------------------------#
# Unset variables and functions
#---------------------------------------------------#

unset WALLPAPER_DIR
unset MY_GTK_THEME
unset set_wallpaper
unset random_wallpaper
unset random_alphabetical
unset TEMP_WALLPAPER_RANDOM
unset TEMP_RAN_WALL
unset random_list_text
unset check_wall_file_exists
