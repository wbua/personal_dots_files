#!/bin/bash

set -e

# To search for next and previous
# Use up and down arrows

copy_dir_main() {

echo "$1" > "$HOME"/Documents/.copy_file_destination.txt
select_copy_source=$(cat "$HOME"/Documents/.yad_search_results.txt)
select_copy_destination=$(cat "$HOME"/Documents/.copy_file_destination.txt)
file_copied_notify=$(cat "$HOME"/Documents/.yad_search_results.txt | xargs -0 basename)

cp "$select_copy_source" ~/"$select_copy_destination"
notify-send "$file_copied_notify" "File copied"

}
export -f copy_dir_main

dir_destination() {

dirs5=( ~/Documents/ ~/Downloads/ ~/Videos/ ~/Pictures/ ~/Music/ )
export dirs

find "${dirs5[@]}" -type d -iname '*' | yad --list \
--column="find" \
--search-column=1 \
--regex-search \
--width=1000 \
--height=500
--dclick-action='/bin/bash -c "copy_dir %s"' \
--button="File manager:/bin/bash -c 'open_file_manager'" \
--button="Close":1

}
export -f dir_destination
dir_destination

unset copy_dir_main
unset dir_destination
