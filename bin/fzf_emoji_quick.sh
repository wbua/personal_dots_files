#!/bin/bash

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

MY_EMOJI="$TEMPORARY_EMOJI"

main_choice=$(echo "$MY_EMOJI" | tr " " "\n" | ${MENU_FZF})
[ -z "$main_choice" ] && exit 0
echo "$main_choice" | tr -d "\n" | wl-copy -n
pkill -f ~/bin/test_script_28-08-24.sh

