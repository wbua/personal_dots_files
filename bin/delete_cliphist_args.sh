#!/bin/bash

# Sway user preferences 
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

# Background color
MY_COLOR_BG="$COLOR_BG"

# Icon color
MY_COLOR_ICON="$COLOR_ICON"

   # Fuzzel
   if [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == 'fuzzel' ]]; then
     cliphist list | fuzzel -d | cliphist delete
   # Wofi
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == 'wofi' ]]; then
     cliphist list | wofi --dmenu | cliphist delete
   # Tofi
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == 'tofi' ]]; then
     cliphist list | tofi --fuzzy-match=true | cliphist delete
   # Rofi
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == 'rofi' ]]; then
     cliphist list | rofi -dmenu -config "$ROFI_THEME_CONFIG" | cliphist delete
   # Bemenu
   elif [[ "$(grep SELECT_RUN_LAUNCHER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == 'bemenu' ]]; then
     cliphist list | bemenu -i -l 8 -H 40 --fn "Monospace 18" --fb "$MY_COLOR_BG" --ff "#cdd6f4" --nb "$MY_COLOR_BG" --nf "#cdd6f4" --tb "$MY_COLOR_BG" \
     --hb "$MY_COLOR_BG" --tf "$MY_COLOR_ICON" --hf "$MY_COLOR_ICON" --af "#FFFFFF" --ab "$MY_COLOR_BG" | cliphist delete
   else
     :
   fi
