#!/bin/bash

 active_app=$(swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class):\(.name)"' \
     | grep -i "libreoffice" | cut -d: -f2)
 
   if [[ "$active_app" == *"Libreoffice"* || "$active_app" == *"libreoffice"* ]]; then
     swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class):\(.name)"' \
     | grep -i "libreoffice" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' focus
   else
     /bin/bash -c "libreoffice"
   fi 
