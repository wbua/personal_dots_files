#!/bin/bash
message="${1:-"Confirm close?"}"
response=$(echo -e "No\nYes" | rofi -dmenu -i -p "$message " -me-select-entry '' -me-accept-entry 'MousePrimary' \
-theme-str 'window {location: north; fullscreen: false; width: 100%;}' \
-theme-str 'entry {vertical-align: 0.1; placeholder: ""; placeholder-color: grey;}' \
-theme-str 'element.selected.normal {background-color: #000000; padding: 0px 0px 0px 0px; border: 0 0 0 0; text-color: #0FFF00;}' \
-theme-str 'configuration {hover-select: true; me-accept-entry: "MousePrimary"; me-select-entry: "";}')

if [ "$response" = "Yes" ]; then
  xdotool key super+control+shift+F4
else
  exit 1;
fi
