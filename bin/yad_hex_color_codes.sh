#!/bin/bash

# Program command: yad_hex_color_codes.sh
# Description: Hex color codes in yad gui.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 15-06-2024
# Program_license: GPL 3.0
# Dependencies: yad, imagemagick, wl-clipboard, slurp, grim

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#------------------------------------------------------------#
# Error checking
#------------------------------------------------------------#

set -e

#------------------------------------------------------------#
# User preferences
#------------------------------------------------------------#

# Yad gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Text font
MY_TEXT_FONT="Cascadia Mono"
export MY_TEXT_FONT

# Icons
MY_ICON_FONT="JetBrainsMono Nerd Font"
export MY_ICON_FONT

#------------------------------------------------------------#
# Create files and directories
#------------------------------------------------------------#

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/.yad_colors ]; then
     :
   else
     mkdir "$HOME"/Pictures/.yad_colors
   fi

   if [ -d "$HOME"/Pictures/yad_colors ]; then
     :
   else
     mkdir "$HOME"/Pictures/yad_colors
   fi

#------------------------------------------------------------#
# Colors (pango markup)
#------------------------------------------------------------#

# Titles
COLOR1=$(printf '%s' "<span color='#FFFFFF' font_family='$MY_TEXT_FONT' font='18' rise='0pt'>")
export COLOR1
# Heading 1
COLOR2=$(printf '%s' "<span color='#F7FF00' font_family='$MY_TEXT_FONT' font='17' rise='0pt'>")
export COLOR2
# Help buttons
COLOR3=$(printf '%s' "<span color='#28FF00' font_family='$MY_TEXT_FONT' font='16' rise='0pt'>")
export COLOR3
# Icons
COLOR4=$(printf '%s' "<span color='#FFFFFF' font_family='$MY_ICON_FONT' font='20' rise='0pt'>")
export COLOR4
# Icons help
COLOR5=$(printf '%s' "<span color='#F7FF00' font_family='$MY_ICON_FONT' font='20' rise='0pt'>")
export COLOR5
# Color end
END=$(printf '%s' "</span>")
export END

#------------------------------------------------------------#
# Help page
#------------------------------------------------------------#

Help_info() {

killall yad
echo "${COLOR1}Help Page${END}

Welcome to Color Codes Manager

${COLOR2}Buttons${END}

${COLOR3}Picker: ${COLOR5} ${END}${END} Select a color on screen to add hex color.
${COLOR3}Add hex: ${COLOR5} ${END}${END} Type in hex code to add hex color.
${COLOR3}Rgb to clipboard: ${COLOR2}RGB ${END}${END} Single click hex image, and then press RGB button.
${COLOR3}Hex to clipboard: ${COLOR2}HEX ${END}${END} Single click hex image, and then press HEX button.
${COLOR3}Delete: ${COLOR5}󰆴 ${END}${END} Single click hex image, and then press Delete button.
" | yad --list --column="help" \
--width=900 \
--height=600 \
--borders=20 \
--no-headers \
--button="_Close":1
	
}
export -f Help_info

#------------------------------------------------------------#
# Creates the dot desktop file
#------------------------------------------------------------#

main_choice() {

# Name	
choice01=$(find ~/Pictures/.yad_colors/ -type f -iname '*.png' -print0 | sed 's|.png||' | xargs -n 1 basename)

# Icon
choice02=$(find ~/Pictures/.yad_colors/ -type f -iname '*.png')

# Exec
choice03=$(printf '%s' "/bin/bash -c 'echo \"${choice01}\" | tee ~/Documents/.temp_hex_yad_copy.txt'")

# Desktop file
choice_main=$(cat << EOF 
[Desktop Entry]
Version=1.0
Type=Application
Name=$choice01
Comment=
Icon=$choice02
Exec=$choice03
StartupNotify=false
Terminal=false

EOF
)

# Creates dot desktop file
echo "$choice_main" | tee "$HOME"/Pictures/.yad_colors/"$choice01".desktop
find ~/Pictures/.yad_colors/ -type f -name "*.desktop" -print0 | xargs -0 sed -i "s/.yad_colors/yad_colors/g"
cp "$HOME"/Pictures/.yad_colors/*.* "$HOME"/Pictures/yad_colors/

}
export -f main_choice

#------------------------------------------------------------#
# Add hex code by typing
#------------------------------------------------------------#

add_hex_code() {

# Deletes all files in temp directory
rm -f "$HOME"/Pictures/.yad_colors/*

# Yad entry dialog
yad_add_choice=$(yad --entry --width=600 \
--borders=10 \
--text="\nType in hex code...\n" \
--height=100 --button="Close":1 | tr '[:upper:]' '[:lower:]' | tee ~/Documents/.temp_hex_yad.txt)
[ -z "$yad_add_choice" ] && exit 0
hex_text_file=$(cat ~/Documents/.temp_hex_yad.txt)

# Creates duplicate text file
duplicate_text2=$(find ~/Pictures/yad_colors/ -type f -iname "*.desktop" -print0 | xargs -0 grep "Name=$hex_text_file")
echo "$duplicate_text2" | tee ~/Documents/.hex_duplicates.txt
head ~/Documents/.hex_duplicates.txt | awk -F ':' '{print $NF}' | \
sed 's|Name=||' | tee ~/Documents/.hex_duplicates.txt

   # Checks if there are duplicate hex color codes 
   if grep -F -f ~/Documents/.temp_hex_yad.txt ~/Documents/.hex_duplicates.txt; then
     notify-send "Color code already exists!"
   else
     convert -size 112x112 xc:"$hex_text_file" "$HOME/Pictures/.yad_colors/$hex_text_file".png
     main_choice
   fi

}
export -f add_hex_code

#------------------------------------------------------------#
# Copy hex to clipboard
#------------------------------------------------------------#

copy_to_clip() {

head ~/Documents/.temp_hex_yad_copy.txt | sed 's|.png||' | wl-copy -n
	
}
export -f copy_to_clip

#------------------------------------------------------------#
# Color picker
#------------------------------------------------------------#

pick_color() {

# Deletes all files in temp directory
rm -f "$HOME"/Pictures/.yad_colors/*

# Selects the color pixel
select_color=$(grim -g "$(slurp -p)" -t ppm - | convert - -format '%[pixel:p{0,0}]' txt:- | \
tr '[:upper:]' '[:lower:]' | tail -n 1 | awk '{print $3}') 

# Yad color dialog
yad_color_choice=$(yad --color --borders=20 --init-color="$select_color")
[[ -z "$yad_color_choice" ]] && echo "$select_color" | \
tee ~/Documents/.temp_hex_yad.txt || echo "$yad_color_choice" | tee ~/Documents/.temp_hex_yad.txt
color_choice=$(cat ~/Documents/.temp_hex_yad.txt)

# Creates duplicate text file
duplicate_text=$(find ~/Pictures/yad_colors/ -type f -iname "*.desktop" -print0 | xargs -0 grep "Name=$color_choice")
echo "$duplicate_text" > ~/Documents/.hex_duplicates.txt
head ~/Documents/.hex_duplicates.txt | awk -F ':' '{print $NF}' | \
sed 's|Name=||' | tail -n 1 | tee ~/Documents/.hex_duplicates.txt

   # Checks if there are duplicate hex color codes
   if grep -F -f ~/Documents/.temp_hex_yad.txt ~/Documents/.hex_duplicates.txt; then
     notify-send "Color code already exists!"
   else
     convert -size 112x112 xc:"$color_choice" "$HOME/Pictures/.yad_colors/$color_choice".png
     main_choice
   fi

}
export -f pick_color

#------------------------------------------------------------#
# Hex to rgb (copies to clipboard)
#------------------------------------------------------------#

hex_to_rgb() {

rgb_text_file=$(cat ~/Documents/.temp_hex_yad_copy.txt)
my_hex_choice=$(printf '%s' "$rgb_text_file" | sed 's|#||')
hex="$my_hex_choice"
printf "%d %d %d\n" 0x"${hex:0:2}" 0x"${hex:2:2}" 0x"${hex:4:2}" | wl-copy -n

}
export -f hex_to_rgb

#------------------------------------------------------------#
# Delete hex color
#------------------------------------------------------------#

delete_hex_color() {

del_text=$(head ~/Documents/.temp_hex_yad_copy.txt)
del_choice=$(find ~/Pictures/yad_colors/ -type f -iname "*.desktop" -print0 | xargs -0 grep "Name=$del_text" | \
head -n 1 | xargs -0 basename | awk -F ':' '{print $1}' | tr -d '\n')
[[ -z "$del_choice" ]] && exit 0

rm -f ~/Pictures/yad_colors/"$del_choice"
rm -f ~/Pictures/yad_colors/"$del_text".png

}
export -f delete_hex_color

#------------------------------------------------------------#
# Dialog
#------------------------------------------------------------#

GTK_THEME="$MY_GTK_THEME" yad --icons \
--read-dir="$HOME/Pictures/yad_colors/" \
--monitor \
--descend \
--item-width=0 \
--image-on-top \
--text="\n${COLOR1}Color Codes Manager${END}\n" \
--text-align="center" \
--single-click \
--buttons-layout="center" \
--borders=20 \
--width="730" \
--height="590" \
--title="Code codes" \
--center \
--button="${COLOR4}${END}:/bin/bash -c 'pick_color'" \
--button="${COLOR4}${END}:/bin/bash -c 'add_hex_code'" \
--button="RGB:/bin/bash -c 'hex_to_rgb'" \
--button="HEX:/bin/bash -c 'copy_to_clip'" \
--button="${COLOR4}󰆴${END}:/bin/bash -c 'delete_hex_color'" \
--button="${COLOR4}󰋗${END}:/bin/bash -c 'Help_info'" \
--button="${COLOR4}${END}":1 

#------------------------------------------------------------#
# Unset variables and functions
#------------------------------------------------------------#

unset create_file
unset main_choice
unset choice01
unset choice02
unset choice03
unset add_hex_code
unset copy_to_clip
unset pick_color
unset hex_to_rgb
unset COLOR1
unset COLOR2
unset COLOR3
unset COLOR4
unset COLOR5
unset END
unset Help_info
unset TMPFILE01
unset MY_TEXT_FONT
unset MY_ICON_FONT
unset delete_hex_color
