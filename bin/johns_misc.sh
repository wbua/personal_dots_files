#!/bin/bash

# Program command: prefixrofi_misc.sh
# Description: Misc actions for prefixrofi.sh
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 21-04-2024
# Program_license: GPL 3.0

#-------------------------------------------------------#
# Search directories for files
#-------------------------------------------------------#

dirs=( ~/"$locate_dir1"/ ~/"$locate_dir2"/ ~/"$locate_dir3"/ ~/"$locate_dir4"/ ~/"$locate_dir5"/ ~/"$locate_dir6"/ ~/"$locate_dir7"/ )

#-------------------------------------------------------#
# 3-day forecast for city
#-------------------------------------------------------#

forecast_area=$(grep "3-day forecast for" "$HOME"/Downloads/.temp_weather/downloadedfile | \
sed -e 's|<description>||' -e 's|</description>||' | \
sed 's|from BBC Weather, including weather, temperature and wind information||') 
        
#-------------------------------------------------------#
# Spaces between menu items
#-------------------------------------------------------#

space=$(printf '%0.1s' " "{1..2})
space2=$(printf '%0.1s' " "{1..3})
space3=$(printf '%0.1s' " "{1..60})

#-------------------------------------------------------#
# Misc
#-------------------------------------------------------#

misc_functions() {

# Check if record status file is empty
     status_file=$(cat "$HOME"/Documents/.rofi_record_status.txt)
     export status_file
   if [[ -z "$status_file" ]]; then
     echo "off" | tee "$HOME"/Documents/.rofi_record_status.txt
     killall rofi
   else
     :
   fi   

# Check mpv recording file status
     recfile=$(cat "$HOME"/Documents/.mpv_recording.txt)
	 export recfile
   if [[ -z "$recfile" ]]; then
     echo "off" > "$HOME"/Documents/.mpv_recording.txt
     killall rofi
   else
     :
   fi

# Check if weather directory is empty	
   if [[ -z "$(ls -A "$HOME"/Downloads/.temp_weather)" ]]; then
     rm -rf "$HOME"/Downloads/.temp_weather/*
     wget https://weather-broker-cdn.api.bbci.co.uk/en/forecast/rss/3day/"$weather_api_key" -O "$HOME"/Downloads/.temp_weather/downloadedfile
   else
     :
   fi
 
# Separate notes
notehelp="grep command
If only one note in prefix_notes directory, it will not show filenames.
The directory must contain at least 2 files.
If only one file is in the directory, it will not open files.
This is just how the grep command works."

noteexample="Put title on the first line
Then you can put anything you want after."

   if [ -f "$HOME"/Documents/prefix_notes/help.txt ]; then
     :
   else
     touch "$HOME"/Documents/prefix_notes/help.txt
     echo "$notehelp" >> "$HOME"/Documents/prefix_notes/help.txt
   fi
  
   if [ -f "$HOME"/Documents/prefix_notes/example.txt ]; then
     :
   else
     touch "$HOME"/Documents/prefix_notes/example.txt
     echo "$noteexample" >> "$HOME"/Documents/prefix_notes/example.txt
   fi

}
