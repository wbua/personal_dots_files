#!/bin/bash

# Program command: fzf-wallpapers.sh
# Description: View and set wallpapers in fzf.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 16-05-2024
# Program_license: GPL 3.0
# Dependencies: fzf, kitty terminal, webp

# Convert image to png for kitty terminal background.
# Use gimp to convert the file.
# Do not use mogrify from imagemagick.
# This is because it crashes wayland.

#-----------------------------------------------#
# Create files and directories
#-----------------------------------------------#

   if [ -d "$HOME"/Pictures/.image_notifications ]; then
     :
   else
     mkdir "$HOME"/Pictures/.image_notifications
   fi

   if [ -d "$HOME"/Pictures/sway_wallpapers ]; then
     :
   else
     mkdir "$HOME"/Pictures/sway_wallpapers
   fi

#-----------------------------------------------#
# My swaywm config
#-----------------------------------------------#

my_swaywm_config="$HOME/.config/sway/config"
export my_swaywm_config

#-----------------------------------------------#
# Line thats sets the wallpaper in your config
#-----------------------------------------------#

lineset="5s"
export lineset

#-----------------------------------------------#
# Wallpapers location
#-----------------------------------------------#

wallpaper_dir="$HOME/Pictures/sway_wallpapers/"
export wallpaper_dir

#-----------------------------------------------#
# Set wallpaper
#-----------------------------------------------#

set_wallpaper() {

wall_file="$HOME/Documents/.temp_set_wallpaper"
sed -i "$lineset|^.*$|output * bg ~/Pictures/sway_wallpapers/$(head "$wall_file") fill|" "$my_swaywm_config"
sed -i "39s|^.*$|BackgroundImageFile=/home/john/Pictures/terminal_background/$(head "$wall_file")|" /home/john/.config/xfce4/terminal/terminalrc
swaymsg reload
pkill -f ~/bin/fzf-wallpapers.sh
	
}
export -f set_wallpaper

#-----------------------------------------------#
# Kill script
#-----------------------------------------------#

kill_script() {

pkill -f ~/bin/fzf-wallpapers.sh
	
}
export -f kill_script

#-----------------------------------------------#
# Image preview notification
#-----------------------------------------------#

image_notify() {

killall dunst
rm -f ~/Pictures/.image_notifications/*
image_notify_choice=$(cat "$HOME"/Documents/.temp_set_wallpaper)
echo "~/Pictures/sway_wallpapers/$image_notify_choice" | tee "$HOME"/Documents/.temp_set_wallpaper2
image_notify_choice2=$(cat "$HOME"/Documents/.temp_set_wallpaper2)

   if [[ "$(file --mime-type "$image_notify_choice2")" == *"webp"* ]]; then
     dwebp "$image_notify_choice2" -o ~/Pictures/.image_notifications/webp_image.png
     setsid notify-send -i ~/Pictures/.image_notifications/webp_image.png selected image >/dev/null 2>&1 & disown
   else
     setsid notify-send -i "$image_notify_choice2" selected image >/dev/null 2>&1 & disown
   fi
	
}
export -f image_notify

#-----------------------------------------------#
# fzf settings
#-----------------------------------------------#

_fzf() {

fzf --info=inline \
--reverse \
--border="right" \
--multi \
--ansi \
--margin="5" \
--cycle \
--header="
Set wallpaper: enter | Preview wallpaper: F2 | kill script: F9

" \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:#660020,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'search wallpapers...> ' \
--bind='F2:execute-silent(echo -n {} | tee "$HOME"/Documents/.temp_set_wallpaper)+execute(image_notify {})' \
--bind='enter:execute-silent(echo -n {} | tee "$HOME"/Documents/.temp_set_wallpaper)+execute(set_wallpaper {})' \
--bind='F9:execute(kill_script {})' 

}
export -f _fzf

#-----------------------------------------------#
# Main
#-----------------------------------------------#

main() {

find "$wallpaper_dir" -type f -iname '*.png' | sort | cut -d '/' -f6- | _fzf

}
export -f main
main

#-----------------------------------------------#
# Unset variables and functions
#-----------------------------------------------#

unset _fzf
unset kill_script
unset main
unset set_wallpaper
unset wallpaper_dir
unset my_swaywm_config
unset lineset
unset color1
unset end
unset image_notify
