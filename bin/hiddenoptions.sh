#!/bin/bash

# Program command: prefixrofi.sh
# Description: Quick unicode shortcuts. Also uses keywords for other actions.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 11-05-2023
# Program_license: GPL 3.0
# Dependencies: 
#rofi, zenity, slock, xdotool, playerctl, yt-dlp, mpv, mpv-mpris, ffmpeg, yt-dlp, xclip, xsel,
#maim, amixer, wmctrl, yad, feh, imagemagick, qrencode, zbar-tools ,libreoffice, festival, arecord
#pulseaudio, amixer, audacious, google material icons font, rofi themes by Aditya Shakya.

# https://github.com/hoyon/mpv-mpris/releases
# https://github.com/google/material-design-icons/blob/master/font/MaterialIcons-Regular.ttf
# rofi themes by: Author : Aditya Shakya (adi1090x) (Github : @adi1090x)

#-------------------------------------------------------#
# Remember
#-------------------------------------------------------#

# This script uses prefixes. To see all prefixes type ?
# Script is made to work with ubuntu 22.04.

#-------------------------------------------------------#
# Variables
#-------------------------------------------------------#

# Screencast resolution
recordres="1920x1080"

# Browser
browser="brave"

# Bash scripts executable path location
bash_dir="$HOME/bin/"

# Open file location (filemanager)
file_manager="thunar"

# Text editor
texteditor="geany"

# Terminal
myterminal="gnome-terminal"

#-------------------------------------------------------#

# Desktop wallpapers location (wallpaper)
screendir="$HOME/Pictures/wallpapers"

# Autostart file (wallpaper)
myautostart="$HOME/.xmonad/scripts/autostart.sh"

# Sets the line where feh is in your autostart file (wallpaper)
lineset="9s"

#-------------------------------------------------------#

# Terminal (menu apps)
terminal_app="gnome-terminal-server" # class
label_terminal="" # label
moveto_terminal="gnome-terminal" # wmctrl
open_terminal="gnome-terminal" # run

# Browser (menu apps)
browser_app="brave" # class
label_browser="" # label
moveto_browser="brave" # wmctrl
open_browser="brave" # run

# File manager (menu apps)
filemanager_app="thunar" # class
label_filemanager="" # label
moveto_filemanager="thunar" # wmctrl
open_filemanager="thunar" # run

# Development program (menu apps)
dev_app="geany" # class
label_dev="" # label
moveto_dev="geany" # wmctrl
open_dev="geany" # run

# Office (menu apps)
office_app="DesktopEditors" # class
label_office="" # label
moveto_office="DesktopEditors" # wmctrl
open_office="onlyoffice-desktopeditors" # run

# Browser used for cloud services (menu apps)
cloud_app="chrome" # class
label_cloud="" # label
moveto_cloud="google-chrome" # wmctrl
open_cloud="google-chrome" # run

# Social media app (menu apps)
social_app="discord" # class
label_social="" # label
moveto_social="discord" # wmctrl
open_social="discord" # run

# Image editor (menu apps)
image_app="gimp" # class
label_image="" # label
moveto_image="gimp" # wmctrl
open_image="gimp" # run

# Sound editor (menu apps) 
audio_app="audacity" # class
label_audio="" # label
moveto_audio="audacity" # wmctrl
open_audio="audacity" # run

# Video editor
video_app="kdenline" # class
label_video="" # label
moveto_video="kdenline" # wmctrl
open_video="kdenline" # run

#-------------------------------------------------------#
# Create directories and files
#-------------------------------------------------------#

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Pictures/rofi_colors ]; then
     :
   else
     mkdir "$HOME"/Pictures/rofi_colors
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Pictures/.temqrcodes ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes
   fi
   
# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Pictures/qr_codes ]; then
     :
   else
     mkdir "$HOME"/Pictures/qr_codes
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Documents ]; then
     : 
   else
     mkdir "$HOME"/Documents
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Pictures ]; then
     : 
   else
     mkdir "$HOME"/Pictures
   fi
   
# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Videos ]; then
     : 
   else
     mkdir "$HOME"/Videos
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Downloads ]; then
     : 
   else
     mkdir "$HOME"/Downloads
   fi

# This is where the full size screenshots are stored

   if [ -d "$HOME"/Pictures/wallpapers ]; then
     :
   else
     mkdir "$HOME"/Pictures/wallpapers
   fi

#
   if [ -f "$HOME"/Pictures/.fehwallrandom.txt ]; then
     :
   else
     touch "$HOME"/Pictures/.fehwallrandom.txt
   fi

# This is where full size screenshots are stored temporary

   if [ -d "$HOME"/Pictures/.tempfullscrns ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempfullscrns
   fi

# This is where the full size screenshots are stored

   if [ -d "$HOME"/Pictures/screenshots ]; then
     :
   else
     mkdir "$HOME"/Pictures/screenshots
   fi

# This is where the video temporary recordings are stored

   if [ -d "$HOME"/Videos/.yadvidrec ]; then
     :
   else
     mkdir "$HOME"/Videos/.yadvidrec
   fi

# This is where audio recordings are stored temporary

   if [ -d "$HOME"/Music/.yadaudiorec ]; then
     :
   else
     mkdir "$HOME"/Music/.yadaudiorec
   fi

# This is where bash scripts are stored

   if [ -d "$HOME"/bin ]; then
     :
   else
     mkdir "$HOME"/bin
   fi

# This is where the video recordings are stored

   if [ -d "$HOME"/Videos/recordings ]; then
     :
   else
     mkdir "$HOME"/Videos/recordings
   fi
 
# Checks if the recordings directory exists, if not creates it

   if [ -d "$HOME"/Music/recordings ]; then
     :
   else
     mkdir "$HOME"/Music/recordings
   fi 
 
 # Checks if directory exists, if not creates it

   if [ -d "$HOME"/Documents/mainprefix ]; then
     : 
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Documents/pdf ]; then
     :
   else
     mkdir "$HOME"/Documents/pdf
   fi
   
# Checks if the temp pdf directory exists, if not creates it

   if [ -d "$HOME"/Documents/.pdftemp ]; then
     :
   else
     mkdir "$HOME"/Documents/.pdftemp
   fi

# Checks if the file bookmarks.txt exists, if not creates it

   if [ -f "$HOME"/Documents/mainprefix/bookmarks.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/bookmarks.txt
   fi
   
# Checks if the file radiolinks.txt exists, if not creates it

   if [ -f "$HOME"/Documents/mainprefix/radiolinks.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/radiolinks.txt
   fi
 
 # Checks if the file rofi_apps.txt exists, if not creates it

   if [ -f "$HOME"/Documents/mainprefix/rofi_apps.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/rofi_apps.txt
   fi
 
# Checks if the file quicklinks.txt exists, if not creates it

   if [ -f "$HOME"/Documents/mainprefix/quicklinks.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/quicklinks.txt
   fi

# Checks if the file configs.txt exists, if not creates it

   if [ -f "$HOME"/Documents/mainprefix/configs.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/configs.txt
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Videos/.ytdownvids ]; then
     :
   else
     mkdir "$HOME"/Videos/.ytdownvids
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Videos/youtube ]; then
     :
   else
     mkdir "$HOME"/Videos/youtube
   fi
  
# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Documents/zips ]; then
     :
   else
     mkdir "$HOME"/Documents/zips
   fi

# Checks if the file zipfile.txt exists, if not creates it

   if [ -f "$HOME"/Documents/.zipfile.txt ]; then
     :
   else
     touch "$HOME"/Documents/.zipfile.txt
   fi
  
#-------------------------------------------------------#
# Run launchers
#-------------------------------------------------------#

_rofi() {

rofi -dmenu -i -p 'dmenu' -markup-rows -me-select-entry '' -me-accept-entry 'MousePrimary' -config "$HOME"/Documents/rofi_themes/style-5.rasi \
-theme-str 'configuration {hover-select: true; me-accept-entry: "MousePrimary"; me-select-entry: "";}' \
-theme-str 'window {width: 50%;}' \
-theme-str 'element {padding: 10px -10px;}' 

}

_rofi2() {
	
rofi -dmenu -i -p 'dmenu' -config "$HOME"/Documents/rofi_themes/style-5b.rasi \
-theme-str 'window {width: 50%;}'
	
}

_rofi3() {
	
rofi -dmenu -i -p 'dmenu' -config "$HOME"/Documents/rofi_themes/style-5c.rasi \
-theme-str 'window {width: 50%;}'
		
}

_rofi4() {

rofi -dmenu -i -p 'dmenu' -config "$HOME"/Documents/rofi_themes/style-5b.rasi \
-theme-str 'window {width: 60%;}'
	
}

_rofi5() {

rofi -dmenu -i -p 'dmenu' -markup-rows -config "$HOME"/Documents/rofi_themes/style-5b.rasi \
-theme-str 'window {width: 60%;}'
		
}

_rofi6() {

rofi -dmenu -i -p 'dmenu' -config "$HOME"/Documents/rofi_themes/style-5d.rasi \
-theme-str 'window {width: 50%;}'
		
}

_rofi7() {
	
rofi -dmenu -i -p 'dmenu' -config "$HOME"/Documents/rofi_themes/style-5b.rasi \
-theme-str 'window {width: 50%;}' \
-theme-str 'listview {lines: 8; columns: 1;}' \

}

#-------------------------------------------------------#
# Colors for menu items (pango markup)
#-------------------------------------------------------#

# Misc buttons
color1="<span color='#E4FF00' font_family='Material Icons' font='27' rise='0pt'>"
# Xmonad controls
color2="<span color='#FF9D00' font_family='Material Icons' font='27' rise='0pt'>"
# Capture
color3="<span color='#00B9FF' font_family='Material Icons' font='27' rise='0pt'>"
# Music
color4="<span color='#FF22F4' font_family='Material Icons' font='27' rise='0pt'>"
# Prefix (recording on)
color5="<span color='#FF002D' font_family='Material Icons' font='27' rise='0pt'>"
# Prefix descriptions
#color6="<span color='#FF002D' font_family='Material Icons' font='27' rise='0pt'>"
# Prefix (app is off)
color7="<span color='#FFFFFF' font_family='Material Icons' font='27' rise='0pt'>"
# Prefix (app is on)
color8="<span color='#10FF00' font_family='Material Icons' font='27' rise='0pt'>"
# Modes text prefix
color9="<span color='#FE00FF' font_family='Monospace' weight='bold' font='14' rise='0pt'>"
# Modes text descriptions
color10="<span color='#7A7A7A' font_family='Monospace' weight='bold' font='13' rise='0pt'>"
# Modes text flags
color11="<span color='#FFFFFF' font_family='Monospace' weight='bold' font='13' rise='0pt'>"
# Modes text unicodes
color12="<span color='#FE00FF' font_family='Material Icons' font='19' rise='-6pt'>"
# Modes text unicodes
color13="<span color='#00FFD9' font_family='Material Icons' font='27' rise='-6pt'>"
# Modes text unicodes
color14="<span color='#FF31A3' font_family='Material Icons' font='27' rise='-6pt'>"
# Color end
end="</span>"

#-------------------------------------------------------#
# Prefix text
#-------------------------------------------------------#

modestext=$(printf '%s' "${color12}${end} ${color10}modes text${end}
${color12}${end} ${color10}randomly change wallpaper${end}
${color12}${end} ${color10}video editor${end}
${color12}${end} ${color10}browser${end}
${color12}${end} ${color10}show all apps${end}
${color12}${end} ${color10}sound editor${end}
${color12}${end} ${color10}image editor${end}
${color12}${end} ${color10}social media${end}
${color12}${end} ${color10}office${end}
${color12}${end} ${color10}browser used for cloud services${end}
${color12}${end} ${color10}file manager${end}
${color12}${end} ${color10}terminal${end}
${color12}${end} ${color10}fullscreen screenshot${end}
${color12}${end} ${color10}select screenshot${end}
${color12}${end} ${color10}delay 5 seconds fullscreen screenshot${end}
${color12}${end} ${color10}dev program${end}
${color12}${end} ${color10}open last created screenshot${end}
${color12}${end} ${color10}start recording screen${end}
${color12}${end} ${color10}audio recording${end}
${color12}${end} ${color10}stop video and audio recordings${end}
${color12}${end} ${color10}open last created video recording${end}
${color12}${end} ${color10}open last created audio recording${end}
${color12}${end} ${color10}randomly plays radio stations${end}
${color12}${end} ${color10}stop radio${end}
${color12}${end} ${color10}volume up${end}
${color12}${end} ${color10}volume down${end}
${color12}${end} ${color10}volume mute${end}
${color12}${end} ${color10}send title of radio track to clipboard${end}
${color12}${end} ${color10}play and pause | audacious${end}
${color12}${end} ${color10}prev track | audacious${end}
${color12}${end} ${color10}next track | audacious${end}
${color12}${end} ${color10}change playlists | audacious${end}
${color9}r${end} ${color10}select and play radio stream | radio${end}
${color9}ra${end} ${color11}txt${end} ${color10}open radio list text file | radio${end}
${color9}ra${end} ${color11}space${end} ${color10}remove whitespace from text file | radio${end}
${color9}ra${end} ${color11}stop${end} ${color10}stop radio | radio${end}
${color9}ra${end} ${color11}radio 3445 - http//example.stream${end} ${color10}add radio stream to list | radio${end}
${color9}b${end} ${color10}select and open bookmark | bookmarks${end}
${color9}bo${end} ${color11}imdb - http//imdb.com${end} ${color10}add bookmark to text file | bookmarks${end}
${color9}bo${end} ${color11}space${end} ${color10}remove whitespace from text file | bookmarks${end}
${color9}bo${end} ${color11}txt${end} ${color10}open bookmarks text file | bookmarks${end}
${color9}s${end} ${color10}select and open app | apps${end}
${color9}sp${end} ${color11}txt${end} ${color10}open apps list text file | apps${end}
${color9}sp${end} ${color11}space${end} ${color10}remove whitespace from text file | apps${end}
${color9}sp${end} ${color11}firefox${end} ${color10}add app to list | apps${end}
${color9}nl${end} ${color11}paste${end} ${color10}paste text into any text field | note${end}
${color9}nl${end} ${color11}txt${end} ${color10}open note | note${end}
${color9}nl${end} ${color11}space${end} ${color10}remove whitespace from text file | note${end}
${color9}nl${end} ${color11}type text${end} ${color10}add text to text file | note${end}
${color9}c${end} ${color10}select and open config${end}
${color9}co${end} ${color11}xmonad - /home/user/.xmonad/xmonad.hs${end} ${color10}add config file to list | configs${end}
${color9}co${end} ${color11}space${end} ${color10}remove whitespace from text file | configs${end}
${color9}co${end} ${color11}txt${end} ${color10}open configs text file${end}
${color9}g${end} ${color11}type text${end} ${color10}google search | web search${end}
${color9}gi${end} ${color11}type text${end} ${color10}google images search | web search${end}
${color9}di${end} ${color11}type text${end} ${color10}dictionary search | web search${end}
${color9}ddg${end} ${color11}type text${end} ${color10}duckduckgo search | web search${end}
${color9}yt${end} ${color11}type text${end} ${color10}youtube search | web search${end}
${color9}wik${end} ${color11}type text${end} ${color10}wiki search | web search${end}
${color9}aw${end} ${color11}type text${end} ${color10}arch wiki search | web search${end}
${color9}gtf${end} ${color11}type text${end} ${color10}translate english to french | web search${end}
${color9}gts${end} ${color11}type text${end} ${color10}translate english to spanish | web search${end}
${color9}shutdown${end} ${color10}shutdown machine${end}
${color9}restart${end} ${color10}restart machine${end}
${color9}lock${end} ${color10}lock machine${end}
${color9}#${end} ${color11}type text${end} ${color10}find files | local file search${end}
${color9}#${end} ${color10}find files (just type # and press enter to see all files | local file search${end}
${color9}o${end} ${color10}file manager${end}
${color9}=${end} ${color11}242+52${end} ${color10}calculator (after it shows your result press enter to copy to clipboard)${end}
${color9}w${end} ${color10}window switcher${end}
${color9}kill${end} ${color11}firefox${end} ${color10}killall app${end}
${color9}t${end} ${color10}select and open bash script | bash${end}
${color9}e${end} ${color10}excute bash script (within 24 hours) | bash${end}
${color9}te${end} ${color11}e${end} ${color10}show all bash scripts and excute selected | bash${end}
${color9}te${end} ${color11}filename${end} ${color10}create bash script (adds .sh, does the chmod and opens script in text editor | bash${end}
${color9}qr${end} ${color11}http//example.com${end} ${color10}create qr code | qr code${end}
${color9}qr${end} ${color11}decode${end} ${color10}decode qr code in yad | qr code${end}
${color9}qr${end} ${color11}view${end} ${color10}send image notification | qr code${end}
${color9}p${end} ${color10}select hex code and copy to clipboard | color picker${end}
${color9}pi${end} ${color11}#242424${end} ${color10}add hex code | color picker${end}
${color9}>${end} ${color10}run terminal commands in rofi${end}
${color9}pdf${end} ${color11}/home/user/Documents/example.docx${end} ${color10}create pdf file | pdf${end}
${color9}pdf${end} ${color11}dir${end} ${color10}select pdf file and open it | pdf${end}
${color9}pdf${end} ${color11}open${end} ${color10}open last created pdf${end}
${color9}text${end} ${color11}clip${end} ${color10}read text from clipboard with festival | text to speech${end}
${color9}text${end} ${color11}stop${end} ${color10}stop playback | text to speech${end}
${color9}text${end} ${color11}/home/user/Documents/file.txt${end} ${color10}read text from text file | text to speech${end}
${color9}speak${end} ${color11}type text${end} ${color10}reads text input | text to speech${end}
${color9}q${end} ${color10}yad sticky note${end}
${color9}em${end} ${color10}emoji${end}
${color9}zi${end} ${color11}clip${end}${color10} zip contents from the clipboard${end}
${color9}zi${end} ${color11}dir${end}${color10} open zip directory${end}
${color9}zi${end} ${color11}/home/john/Documents/doc1.docx${end}${color10} create zip file${end}
${color9}vol${end} ${color10}send current volume level to clipboard${end}
${color9}date${end}
${color9}uptime${end}
${color9}spell${end} ${color11}type text${end} ${color10}spell checker${end}
")

#-------------------------------------------------------#
# Poweroff, restart and lock machine (uses zenity)
#-------------------------------------------------------#

# Power lock 

yadlock () {

   if zenity --question --text "Lock, are you sure" 
   then
     slock
   else
     exit 1
   fi
   
}

# Power reboot 

yadreboot () {

   if zenity --question --text "Reboot, are you sure" 
   then
     poweroff --reboot
   else
     exit 1
   fi
   
}

# Power shutdown 

yadshutdown () {

   if zenity --question --text "Shutdown, are you sure" 
   then
     poweroff --poweroff
   else
     exit 1
   fi
   
}

#-------------------------------------------------------#
# Audacious player
#-------------------------------------------------------#

audacious_play() {
  audacious -H --play-pause
}

audacious_prev() {
  audacious --rew
}

audacious_next() {
  audacious --fwd
}

current_playlist() {

if [[ "$(audtool --current-playlist)" == *"1"* ]]; then
	audtool --set-current-playlist 2
elif [[ "$(audtool --current-playlist)" == *"2"* ]]; then
	audtool --set-current-playlist 3
elif [[ "$(audtool --current-playlist)" == *"3"* ]]; then
    audtool --set-current-playlist 1
fi
}

#-------------------------------------------------------#
# Take screenshot
#-------------------------------------------------------#

take_screenshot() {

rm -f "$HOME"/Pictures/.tempfullscrns/*.png 
sleep 1
maim --hidecursor | tee "$HOME/Pictures/.tempfullscrns/scn_$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/screenshots/
one=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
notify-send -i "$one" Screenshot taken
  
}

#-------------------------------------------------------#
# Sets wallapaper and changes your autostart script
#-------------------------------------------------------#

setrandomwall() {
	
wfile="$HOME/Pictures/.fehwallrandom.txt"
launch=$(ls "$screendir")
image=$(echo "$launch" | shuf -n1)
echo "$screendir/$image" > "$wfile"
xargs -L 1 -a "$wfile" -d '\n' feh --bg-scale
sed -i "$lineset|^.*$|feh --bg-scale \"$(head "$wfile")\"|" "$myautostart"

}

#-------------------------------------------------------#
# Radio
#-------------------------------------------------------#

online_radio() {

textfile=$(cat "$HOME"/Documents/mainprefix/radiolinks.txt)	
launcher4=$(echo "$textfile" | _rofi4)
[[ -z "$launcher4" ]] && exit
killall mpv ; killall ffplay ; echo -n "${launcher4[@]}" | awk '{print $NF}' | xargs mpv
  
}

#-------------------------------------------------------#
# Spawn apps
#-------------------------------------------------------#

open_apps() {
	
launch=$(cat "$HOME"/Documents/mainprefix/rofi_apps.txt)
choice=$(echo "$launch" | sort | _rofi6)
[ -z "$choice" ] && exit 0
echo "$choice" | xargs /bin/bash -c

}

#-------------------------------------------------------#
# open configs
#-------------------------------------------------------#

system_configs() {
	
launcher4=$(_rofi7 < "$HOME"/Documents/mainprefix/configs.txt)
[[ -z "$launcher4" ]] && exit
echo "${launcher4[@]}" | awk '{print $NF}' | xargs -r "$texteditor"

}

#-------------------------------------------------------#
# Syncing bookmarks
#-------------------------------------------------------#

# Default browser uses "$browser"
default_browser() {

bookcheck=$(cat "$HOME"/Documents/mainprefix/bookmarks.txt)
bookoutone=$(echo "${bookcheck[@]}" | sort | _rofi4 | awk '{print $NF}' | xargs -r "$browser")
echo "$bookoutone"  
	
 }

# Firefox browser
firefox_browser() {
	
bookcheck=$(cat "$HOME"/Documents/mainprefix/bookmarks.txt)
bookoutone=$(echo "${bookcheck[@]}" | sort | _rofi4 | awk '{print $NF}' | xargs -r firefox)
echo "$bookoutone"  
	
}

# Brave browser
brave_browser() {
	
bookcheck=$(cat "$HOME"/Documents/mainprefix/bookmarks.txt)
bookoutone=$(echo "${bookcheck[@]}" | sort | _rofi4 | awk '{print $NF}' | xargs -r brave)
echo "$bookoutone"  
	
} 

# Vivaldi browser
vivaldi_browser() {
	
bookcheck=$(cat "$HOME"/Documents/mainprefix/bookmarks.txt)
bookoutone=$(echo "${bookcheck[@]}" | sort | _rofi4 | awk '{print $NF}' | xargs -r "$browser")
echo "$bookoutone"  	
	
}

# Opera browser
opera_browser() {

bookcheck=$(cat "$HOME"/Documents/mainprefix/bookmarks.txt)
bookoutone=$(echo "${bookcheck[@]}" | sort | _rofi4 | awk '{print $NF}' | xargs -r opera)
echo "$bookoutone"  

}

#-------------------------------------------------------#
# Bookmarks
#-------------------------------------------------------#

open_bookmarks() {

curr=$(xdotool getactivewindow)
desktop=$(xdotool search -class desktop)
firefox=$(xdotool search -class firefox)
brave=$(xdotool search -class brave)
vivaldi=$(xdotool search -class vivaldi-stable)
opera=$(xdotool search -class opera)

   # Opens default browser when on blank desktop.
   if [[ "$desktop" = *"$curr"* ]]; then
     default_browser
   # Open links in firefox
   elif [[ "$firefox" = *"$curr"* ]]; then
     firefox_browser
   # Open links in brave
   elif [[ "$brave" = *"$curr"* ]]; then
     brave_browser 
   # Open links in vivaldi
   elif [[ "$vivaldi" = *"$curr"* ]]; then
     vivaldi_browser
   # Open links in opera
   elif [[ "$opera" = *"$curr"* ]]; then
     opera_browser
   # If browser is not in script, then it opens link with default browser. Default browser is "$browser"
   else
     bookcheck=$(cat "$HOME"/Documents/mainprefix/bookmarks.txt)
     bookoutone=$(echo "${bookcheck[@]}" | sort | _rofi4 | awk '{print $NF}' | xargs -r "$browser")
     echo "$bookoutone"  
   fi
		
}

#-------------------------------------------------------#
# File manager
#-------------------------------------------------------#

rofifilemanager() {
	
select=1
   while [ "$select" ]; do
   choice=$(ls -1a --group-directories-first)
   select="$(echo "$choice" | _rofi2 \
   -theme-str 'entry {vertical-align: 0.0; placeholder: ""; padding:  0px 0px;}' -l 10 -p "Folder: $(basename "$(pwd)")")"
   if [ -f "$select" ]; then
     xdg-open "$select" && exit
   elif [ -d "$select" ]; then
     cd "$select" || exit
   fi
   done
   
} 

#-------------------------------------------------------#
# window switcher
#-------------------------------------------------------#

window_switcher() {
	
open=$(wmctrl -l | awk '{print $NF}')
workspace=$(printf '%s\n' "${open[@]}" | _rofi2)
[ -z "$workspace" ] && exit 0
wmctrl -a "$workspace"  

}

#-------------------------------------------------------#
# view and open bash scripts
#-------------------------------------------------------#

bash_scripts() {

choice=$(ls "$bash_dir")
launcher=$(echo "$choice" | _rofi7)
[ -z "$launcher" ] && exit 0
"$texteditor" "$bash_dir"/"$launcher"	
	
}

#-------------------------------------------------------#
# exec bash scripts (within the last 24 hours)
#-------------------------------------------------------#

exec_scripts() {

launcher=$(find "$bash_dir" -maxdepth 5 -type f -ctime 0 | sort | xargs -n 1 basename | _rofi7)
[ -z "$launcher" ] && exit 0
/bin/bash -c "$bash_dir"/"$launcher"	
	
}

#-------------------------------------------------------#
# color picker
#-------------------------------------------------------#

color_picker() {

screendir="$HOME/Pictures/rofi_colors/"
choice=$(find ~/Pictures/rofi_colors/ -maxdepth 1 -iname '*.png' | \
awk -F/ '{print $NF}' | while read -r A ; do  echo -en "$A\x00icon\x1f~/Pictures/rofi_colors/$A\n" ; done | sed 's/\.png//' | _rofi3)

echo "$choice"
   
   if [ "$choice" ]; then
     image=$(printf '%s\n' "${screendir}${choice}")
     echo "$image" | cut -d '/' -f6- | xclip -selection clipboard
   else
     echo "program terminated" && exit 0
   fi	
	
}

#-------------------------------------------------------#
# yad sticky note
#-------------------------------------------------------#

show_note() {
   
awk -i inplace '!seen[$0]++' "$HOME"/Documents/.yadnote.txt
sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/.yadnote.txt
launch=$(sort -u "$HOME"/Documents/.yadnote.txt)
choice=$(echo -en "$launch" | yad --text-info --show-uri --button="save:0" --button="close:1" \
--back=#1a1818 --fore=#ffffff --uri-color=#46FF23 --center --fontname="Monospace 11" --wrap \
--button="Delete:/bin/bash -c 'rm -f ~/Documents/.yadnote.txt'" \
--button="Clear:/bin/bash -c 'tee ~/Documents/.yadnote.txt'" \
--button="Quit:/bin/bash -c 'killall yad'" \
--geometry=400x400-0-0 --editable)
		
echo "$choice" | tee -a "$HOME"/Documents/.yadnote.txt
	
}
export -f show_note

#-------------------------------------------------------#
# Emoji
#-------------------------------------------------------#

emojilist() {

fileoutone=$(cat "$HOME"/Documents/mainprefix/emoji-data.txt)
chosen=$(echo "$fileoutone" | _rofi2 | awk '{print $1}')
[ -z "$chosen" ] && exit

   if [ -n "$1" ]; then
     xdotool type "$chosen"
  else
    printf '%s' "$chosen" | xclip -selection clipboard
    notify-send "'$chosen' copied to clipboard" 
  fi

}

#-------------------------------------------------------#
# Send volume status to notification daemon
#-------------------------------------------------------#

sendnotifyvol() {
	
# Arbitrary but unique message tag
msgTag="myvolume"

# Change the volume using alsa(might differ if you use pulseaudio)
amixer -D pulse "$@" > /dev/null

# Query amixer for the current volume and whether or not the speaker is muted
volume="$(amixer -D pulse get Master | tail -1 | awk '{print $5}')"
mute="$(amixer -D pulse get Master | tail -1 | awk '{print $6}')"

   if [[ $volume == 0 || "$mute" == "[off]" ]]; then
     # Show the sound muted notification
     dunstify -a "changeVolume" -u low -i audio-volume-muted -h string:x-dunst-stack-tag:$msgTag "Volume muted" 
   else
     # Show the volume notification
     dunstify -a "changeVolume" -u low -i audio-volume-high -h string:x-dunst-stack-tag:$msgTag \
     -h int:value:"$volume" "Volume: ${volume}%"
   fi

# Play the volume changed sound
canberra-gtk-play -i audio-volume-change -d "changeVolume"

}

#-------------------------------------------------------#
# Show different colors for apps when running and closed
#-------------------------------------------------------#

select_terminal() {

  if pidof -qx "$terminal_app"; then
    echo "${color8}${label_terminal}${end}"
  else
    echo "${color7}${label_terminal}${end}"
  fi
}

select_browser() {

  if pidof -qx "$browser_app"; then
    echo "${color8}${label_browser}${end}"
  else
    echo "${color7}${label_browser}${end}"
  fi
}

select_filemanager() {

  if pidof -qx "$filemanager_app"; then
    echo "${color8}${label_filemanager}${end}"
  else
    echo "${color7}${label_filemanager}${end}"
  fi
}

select_dev() {

  if pidof -qx "$dev_app"; then
    echo "${color8}${label_dev}${end}"
  else
    echo "${color7}${label_dev}${end}"
  fi
}

select_office() {

  if pidof -qx "$office_app"; then
    echo "${color8}${label_office}${end}"
  else
    echo "${color7}${label_office}${end}"
  fi
}

select_cloud() {

  if pidof -qx "$cloud_app"; then
    echo "${color8}${label_cloud}${end}"
  else
    echo "${color7}${label_cloud}${end}"
  fi
}

select_image() {

  if pidof -qx "$image_app"; then
    echo "${color8}${label_image}${end}"
  else
    echo "${color7}${label_image}${end}"
  fi
}

select_audio() {

  if pidof -qx "$audio_app"; then
    echo "${color8}${label_audio}${end}"
  else
    echo "${color7}${label_audio}${end}"
  fi
}

select_video() {

  if pidof -qx "$video_app"; then
    echo "${color8}${label_video}${end}"
  else
    echo "${color7}${label_video}${end}"
  fi
}

select_social() {
	
  if pidof -qx "$social_app"; then
    echo "${color8}${label_social}${end}"
  else
    echo "${color7}${label_social}${end}"
  fi
	
}

select_recording() {
	
  if pidof -qx "ffmpeg"; then
    echo "${color5}${end}"
  else
    echo "${color2}${end}"
  fi
	
}

mic_recording() {
	
  if pidof -qx "arecord"; then
    echo "${color5}${end}"
  else
    echo "${color2}${end}"
  fi
	
}

radio_recording() {
	
  if pidof -qx "mpv"; then
    echo "${color5}${end}"
  else
    echo "${color4}${end}"
  fi
	
}

mutetoggle() {

  if [[ $(amixer -D pulse sget Master | awk 'NR==6 {print $6}') == *"[on]"* ]]; then
    echo "${color13}${end}"
  elif [[ $(amixer -D pulse sget Master | awk 'NR==6 {print $6}') == *"[off]"* ]]; then
    echo "${color5}${end}"
  fi	
	
}

#-------------------------------------------------------#
# Search directories for files
#-------------------------------------------------------#

dirs=( ~/Documents/ ~/Pictures/ ~/Videos/ ~/Music/ ~/Downloads/)

#-------------------------------------------------------#
# # Stop mpv
#-------------------------------------------------------#

stopmpv () {

   if pgrep -x "mpv" > /dev/null
   then
     killall mpv 
   elif pgrep -x "audacious" > /dev/null
   then
     killall audacious
   else
     :
   fi
	
}

#-------------------------------------------------------#
# Menu
#-------------------------------------------------------#

menu() {

select_terminal
select_browser
select_filemanager
select_dev
select_office
select_cloud
select_social
select_image
select_audio
select_video
echo "${color3}${end}"
echo "${color3}${end}"
echo "${color3}${end}"
echo "${color3}${end}"
select_recording
mic_recording
echo "${color2}${end}"
echo "${color2}${end}"
echo "${color2}${end}"
radio_recording
echo "${color4}${end}"
echo "${color13}${end}"
echo "${color13}${end}"
mutetoggle
echo "${color14}${end}"
echo "${color14}${end}"
echo "${color14}${end}"
echo "${color14}${end}"
echo "${color1}${end}"
echo "${color1}${end}"
echo "${color1}${end}"
echo "${color1}${end}"


}	

#-------------------------------------------------------#
# Main
#-------------------------------------------------------#

selected=$(menu | _rofi | awk -F "${color1}+|${color2}+|${color3}+|${color4}+|${color7}+|${color8}+|${color5}+|${color13}+|${color14}" '{print $NF}' | sed 's|</span>||')
bang=$(echo "${selected[@]}" | awk '{print $1}')
launchercontent=$(echo "$selected" | cut -d' ' -f2-)
[ -z "$launchercontent" ] && exit 0


case "$bang" in

  # Send notification
 'ns')
  notify-send "$launchercontent"
  ;;
  # Download videos
 'do')
  if [[ "$launchercontent" == 'open' ]]; then
  xdg-open "$HOME"/Videos/.ytdownvids/*.webm
  else
  rm -f "$HOME"/Videos/.ytdownvids/*.webm
  yt-dlp "$launchercontent" -o "$HOME/Videos/.ytdownvids/%(title)s.%(ext)s" && notify-send "Download completed"
  cp "$HOME"/Videos/.ytdownvids/*.webm "$HOME"/Videos/youtube/
  fi
  ;;
 '')
  audacious_play
  ;;
 '')
  audacious_prev
  ;;
 '')
  audacious_next
  ;;
 '')
  current_playlist
  ;;
  # Modes text
 '')
  sleep 0.1 ; echo "${modestext[@]}" | sort | _rofi5
  ;;
  # Set wallpaper and change autostart script  
 '')
  setrandomwall
  ;;
  # Video editor
 '')
  if pidof -qx "$video_app"; then
    wmctrl -x -a "$moveto_video"
  else
    "$open_video"
  fi
  ;;
  # Browser
 '')
  if pidof -qx "$browser_app"; then
    wmctrl -x -a "$moveto_browser"
  else
    "$open_browser"
  fi
  ;;
  # Show all apps
 '')
  sleep 0.1 ; open_apps
  ;;
  # Sound editor
 '')
  if pidof -qx "$audio_app"; then
    wmctrl -x -a "$moveto_audio"
  else
    "$open_audio"
  fi
  ;;
  # Image editor
 '')
  if pidof -qx "$image_app"; then
    wmctrl -x -a "$moveto_image"
  else
    "$open_image"
  fi
  ;;
  # Social media
 '')
  if pidof -qx "$social_app"; then
    wmctrl -x -a "$moveto_social"
  else
    "$open_social"
  fi
  ;;
  # Office
 '')
  if pidof -qx "$office_app"; then
    wmctrl -x -a "$moveto_office"
  else
    "$open_office"
  fi
  ;;
  # Browser used for cloud services
 '')
  if pidof -qx "$cloud_app"; then
    wmctrl -x -a "$moveto_cloud"
  else
   "$open_cloud"
  fi
  ;;
  # File manager
 '')
  if pidof -qx "$filemanager_app"; then
    wmctrl -x -a "$moveto_filemanager"
  else
    "$open_filemanager"
  fi
  ;;
  # Terminal
 '')
  if pidof -qx "$terminal_app"; then
    wmctrl -x -a "$moveto_terminal"
  else
    "$open_terminal"
  fi
  ;;
  # Take fullscreen screenshot
 '')
  take_screenshot  
  ;;
  # Dev
 '')
  if pidof -qx "$dev_app"; then
    wmctrl -x -a "$moveto_dev"
  else
    "$open_dev"
  fi
  ;;
  # Open last created screenshot
 '')
  find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open
  ;;
  # Video recording
 '')
  rm -f "$HOME"/Videos/.yadvidrec/*.*
  notify-send "Recording started" && ffmpeg -video_size "$recordres" -framerate 30 -f x11grab -i :0.0+0 -f pulse -ac 2 -i default "$HOME/Videos/.yadvidrec/video_$(date +'%d-%m-%Y-%H%M%S').mkv"
  ;;
  # Audio recording
 '')
  rm -f "$HOME"/Music/.yadaudiorec/*.*
  notify-send "Recording audio only" ; arecord -f S16_LE -t wav "$HOME/Music/.yadaudiorec/audio_$(date '+%d%m%Y-%H%M-%S').wav"
  ;;
  # Stop video and audio recording
 '')
  vid="$HOME/Videos/.yadvidrec/"
  mainvid="$HOME/Videos/recordings/"
  snd="$HOME/Music/.yadaudiorec/"
  mainsnd="$HOME/Music/recordings/"
  killall ffmpeg
  killall arecord
  grep -q "$vid" "$mainvid" || cp "$HOME"/Videos/.yadvidrec/*.* "$HOME"/Videos/recordings/ && notify-send "Recording stopped"
  grep -q "$snd" "$mainsnd" || cp "$HOME"/Music/.yadaudiorec/*.* "$HOME"/Music/recordings/ && notify-send "Recording stopped"
  ;;
  # Opens last video recording 
 '')
  find "$HOME"/Videos/.yadvidrec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open
  ;;
  # Open last audio recording
 '')
  find "$HOME"/Music/.yadaudiorec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open
  ;;
  # Randomly plays radio stations
 '')
  file=$(cat "$HOME"/Documents/mainprefix/radiolinks.txt)
  killall mpv ; echo "$file" | shuf -n1 | awk '{print $NF}' | xargs mpv
  ;;
  # Stop radio
 '')
  stopmpv
  ;;
  # Sends title of radio track to clipboard
 '')
  playerctl --player mpv metadata -f "{{title}}" | xclip -selection clipboard
  ;;
  # Select screenshot
 '') 
  rm -f "$HOME"/Pictures/.tempfullscrns/*.png 
  sleep 1
  maim --select --hidecursor | tee "$HOME/Pictures/.tempfullscrns/scn_$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
  cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/screenshots/
  one=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
  notify-send -i "$one" Screenshot taken
  ;;
  # Delay 5 seconds screenshot
 '')  
  rm -f "$HOME"/Pictures/.tempfullscrns/*.png 
  sleep 1
  maim --hidecursor --delay=5 | tee "$HOME/Pictures/.tempfullscrns/scn_$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
  cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/screenshots/
  one=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
  notify-send -i "$one" Screenshot taken
  ;;
  # Volume up
 '')
  amixer -D pulse sset Master 10%+
  ;;
 '')
  amixer -D pulse sset Master 10%-
  ;;
 '')
  amixer -D pulse set Master 1+ toggle
  ;;
  # Plays radio stations
 'r')
  online_radio
  ;;
  # Add radio station
 'ra')
  if [[ "$launchercontent" == 'txt' ]]; then
  xdg-open "$HOME"/Documents/mainprefix/radiolinks.txt
  elif [[ "$launchercontent" == 'space' ]]; then
  sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/radiolinks.txt
  elif [[ "$launchercontent" == 'stop' ]]; then
  killall mpv
  else
  checkbmfile2="$HOME/Documents/mainprefix/radiolinks.txt" 
  grep -q "$launchercontent" "$checkbmfile2" || printf '%s\n' "$launchercontent" >> "$HOME"/Documents/mainprefix/radiolinks.txt
  fi
  ;;
 # Open bookmarks
 'b')
  open_bookmarks
  ;;
  # Add bookmark
 'bo')
  if [[ "$launchercontent" == 'space' ]]; then
  sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/bookmarks.txt
  elif [[ "$launchercontent" == 'txt' ]]; then
  xdg-open "$HOME/Documents/mainprefix/bookmarks.txt"
  else
  checkbmfile="$HOME/Documents/mainprefix/bookmarks.txt"
  grep -q "$launchercontent" "$checkbmfile" || printf '%s\n' "$launchercontent" >> "$HOME"/Documents/mainprefix/bookmarks.txt
  fi
  ;;
  # Spawn apps
 's')
  open_apps
  ;;
  # Add app
  'sp')
  if [[ "$launchercontent" == 'txt' ]]; then
  xdg-open "$HOME"/Documents/mainprefix/rofi_apps.txt
  elif [[ "$launchercontent" == 'space' ]]; then
  sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/rofi_apps.txt
  else
  checkbmfile="$HOME/Documents/mainprefix/rofi_apps.txt"
  grep -q "$launchercontent" "$checkbmfile" || printf '%s\n' "$launchercontent" >> "$HOME"/Documents/mainprefix/rofi_apps.txt
  fi
  ;;
  # Add text to single note
 'nl')
  if [[ "$launchercontent" == 'paste' ]]; then
  launcher="$(_rofi2 < "$HOME"/Documents/mainprefix/quicknotes.txt)"
  [ -z "$launcher" ] && exit 0
  printf '%s' "$launcher" | xclip -selection clipboard 
  sleep 0.1
  xdotool key control+v
  elif [[ "$launchercontent" == 'txt' ]]; then
  xdg-open "$HOME"/Documents/mainprefix/quicknotes.txt
  elif [[ "$launchercontent" == 'space' ]]; then
  sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/quicknotes.txt
  else
  printf '%s\n' "$launchercontent" >> "$HOME"/Documents/mainprefix/quicknotes.txt
  fi
  ;;
  # Configs
 'c')
  system_configs
  ;;
 'co')
  if [[ "$launchercontent" == 'space' ]]; then
  sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/mainprefix/configs.txt
  elif [[ "$launchercontent" == 'txt' ]]; then
  xdg-open "$HOME/Documents/mainprefix/configs.txt"
  else
  checkbmfile="$HOME/Documents/mainprefix/configs.txt"
  grep -q "$launchercontent" "$checkbmfile" || printf '%s\n' "$launchercontent" >> "$HOME"/Documents/mainprefix/configs.txt
  fi
  ;;
  # Google search
 'g')
  search="https://www.google.co.uk/search?q={}"
  echo "$launchercontent" | xargs -I{} "$browser" "$search"
  ;;
  # Google images
 'gi')
  search="https://www.google.com/search?hl=en&tbm=isch&q={}"
  echo "$launchercontent" | xargs -I{} "$browser" "$search"
  ;;
  # Collins dictionary
 'di')
  search="https://www.collinsdictionary.com/dictionary/english/{}"
  echo "$launchercontent" | xargs -I{} "$browser" "$search"
  ;;
  # DuckDuckGo
 'ddg')
  search="https://duckduckgo.com/?q={}"
  echo "$launchercontent" | xargs -I{} "$browser" "$search"
  ;;
  # Youtube search
 'yt')
  search="https://www.youtube.com/results?search_query={}"
  echo "$launchercontent" | xargs -I{} "$browser" "$search"
  ;;
  # Wiki search
 'wik')
  search="https://en.wikipedia.org/wiki/{}"
  echo "$launchercontent" | xargs -I{} "$browser" "$search"
  ;;
  # Arch wiki
 'aw')
  search="https://wiki.archlinux.org/index.php?search={}"
  echo "$launchercontent" | xargs -I{} "$browser" "$search"
  ;;
  # Translate english to french
 'gtf')
  search="https://translate.google.com/?sl=auto&tl=fr&text={}"
  echo "$launchercontent" | xargs -I{} "$browser" "$search"
  ;;
  # Translate english to spanish
 'gts')
  search="https://translate.google.com/?sl=auto&tl=es&text={}"
  echo "$launchercontent" | xargs -I{} "$browser" "$search"
  ;;
  # Poweroff
 'shutdown')
  yadshutdown
  ;;
  # Restart
 'restart')
  yadreboot
  ;; 
  # Lock (slock) 
 'lock')
  yadlock
  ;;
  # Find local files
 '#')
  if [[ "$launchercontent" == '#' ]]; then
  chosen=$(find "${dirs[@]}" -type f -iname '*.*' | sort | cut -d '/' -f4- | _rofi4)
  [ -z "$chosen" ] && exit 0
  echo "$HOME"/"$chosen" | xargs -0 xdg-open
  else
  launchertwo=$(find "${dirs[@]}" -type f -iname "*$launchercontent*" | sort | cut -d '/' -f4- | _rofi4)
  [ -z "$launchertwo" ] && exit 0
  echo "$HOME"/"$launchertwo" | xargs -0 xdg-open
  fi
  ;;
  # File manager
 'o')
  rofifilemanager
  ;;
  # Calculator
 '=')
  translate=$(echo "$launchercontent" | bc | _rofi2)
  [ -z "$launchercontent" ] && exit 0
  echo "$translate" | xclip -selection clipboard
  ;;
 'w')   
  window_switcher
  ;;
  # Kill app
 'kill')
  killall "$launchercontent"   
  ;;
  # Open bash scripts
 't')
  bash_scripts
  ;;
  # Exec bash scripts
 'e')
  exec_scripts
  ;;
  # Bash scripts
 'te')
  if [[ "$launchercontent" == 'e' ]]; then
  choice=$(ls "$bash_dir")
  launcher=$(echo "$choice" | _rofi7)
  [ -z "$launcher" ] && exit 0
  /bin/bash -c "$bash_dir"/"$launcher"
  else
  bashfile="$bash_dir/$launchercontent".sh
  echo "#!/bin/bash" >> "$bashfile"
  chmod u+x "$bashfile"
  "$texteditor" "$bashfile"
  fi
  ;;
  # QR Codes
 'qr')
  if [[ "$launchercontent" == 'decode' ]]; then
  find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs zbarimg | \
  yad --text-info --wrap --height=500 --width=800 --center --back="#1a1818" --fore="#ffffff"
  elif [[ "$launchercontent" == 'view' ]]; then
  one=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
  notify-send -i "$one" "Qrcode"
  else
  rm -f "$HOME"/Pictures/.temqrcodes/*.png
  printf '%s\n' "$launchercontent" | qrencode -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
  cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
  one2=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
  notify-send -i "$one2" Screenshot taken
  fi
  ;;
  # Copy hex code to clipboard
 'p')
  color_picker
  ;;
  # Create hex color
 'pi')
  convert -size 200x200 xc:"$launchercontent" "$HOME/Pictures/rofi_colors/$launchercontent".png && notify-send "Hex color added!"
  ;;
  # Run terminal commands
 '>')
  "$myterminal" -- /bin/bash -c "$launchercontent; $SHELL"
  #"$myterminal" --command="/bin/bash -c '$launchercontent; $SHELL'"
  ;;
  # PDF
 'pdf')
  if [[ "$launchercontent" == 'dir' ]]; then
  chosen5=$(find "$HOME"/Documents/pdf/ -type f -iname '*.*' | sort | cut -d '/' -f6- | _rofi4)
  [ -z "$chosen5" ] && exit 0
  "$file_manager" "$HOME"/Documents/pdf/"$chosen5"
  elif [[ "$launchercontent" == 'open' ]]; then
  find "$HOME"/Documents/.pdftemp/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open
  else
  rm -f "$HOME"/Documents/.pdftemp/*.*
  lowriter --convert-to pdf "$launchercontent" --outdir "$HOME"/Documents/.pdftemp/ && notify-send "PDF created"
  cp "$HOME"/Documents/.pdftemp/*.* "$HOME"/Documents/pdf/
  fi
  ;;
  # Text to speech
 'text')
  if [[ "$launchercontent" == 'clip' ]]; then 
  xsel | festival --tts
  elif [[ "$launchercontent" == 'stop' ]]; then
  pkill -f audsp
  else
  festival --tts "$launchercontent"
  fi
  ;;
  # Text to speech
 'speak')
  echo "$launchercontent" | festival --tts
  ;;
  # Sticky note
 'q')
  yad --notification --image="text-editor" --command "/bin/bash -c show_note"
  ;;
  # Emoji
 'em')
  emojilist "$@"
  ;;
 'zi')
  file="$HOME/Documents/.zipfile.txt"
  outfile="$HOME/Documents/zips/$(date +'%d-%m-%Y-%H%M%S')"
  if [[ "$launchercontent" == 'clip' ]]; then
  wl-paste > "$file"
  xargs -L 1 -a "$file" -d '\n' zip -j "$outfile".zip
  elif [[ "$launchercontent" == 'dir' ]]; then
  "$file_manager" "$HOME"/Documents/zips/
  else
  zip -j "$HOME/Documents/zips/$(date +'%d-%m-%Y-%H%M%S')".zip "$launchercontent"
  fi
  ;;
 'date')
  # Date
  calcul=$(date | _rofi2)
  [ -z "$launchercontent" ] && exit 0
  echo "$calcul" | xclip -selection clipboard  
  ;;
  # Uptime
 'uptime')  
  upt=$(uptime | _rofi2)
  [ -z "$launchercontent" ] && exit 0
  echo "$upt" | xclip -selection clipboard
  ;;
  # Spell checker
 'spell')
  aspell -a <<< "$launchercontent" | yad --text-info --wrap --fontname="Monospace 12" --button="Exit":1 --height=500 \
  --center --width=800 --back=#1a1818 --fore=#ffffff
  ;;
  # Check volume
 'vol') 
  sendnotifyvol
  ;;
  # Modes text
 '?')
  echo "${modestext[@]}" | sort | _rofi5
  ;;
  
  *)
  echo "This is not a recognized service" | _rofi4
  ;;
  
esac

#-------------------------------------------------------#
# Unset
#-------------------------------------------------------#

unset show_note
