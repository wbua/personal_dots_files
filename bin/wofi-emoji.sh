#!/bin/bash

cat ~/Documents/mainprefix/emoji-data.txt | wofi --dmenu -i | awk '{print $1}' | xargs -r wl-copy
