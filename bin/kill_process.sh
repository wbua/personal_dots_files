#!/bin/bash

choice=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | \
tr -d '"' | tr '[:upper:]' '[:lower:]')
killall "$choice" ; killall "${choice^}" ; pkill -f "$choice" 
