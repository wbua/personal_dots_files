#!/bin/bash

# Program command: yad_find.sh
# Description: Find files on your local system.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 29-06-2024
# Program_license: GPL 3.0
# Dependencies: yad

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#----------------------------------------------------------#
# Error checking
#----------------------------------------------------------#

set -e

#----------------------------------------------------------#
# User preferences
#----------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# File manager
FILE_MANAGER="$MAIN_FILE_MANAGER"
export FILE_MANAGER

#----------------------------------------------------------#
# Directory search locations
#----------------------------------------------------------#

dirs_paths=( ~/Documents/ ~/Downloads/ ~/Videos/ ~/Pictures/ ~/Music/ )
export dirs_paths

#----------------------------------------------------------#
# Open file in file manager
#----------------------------------------------------------#

open_file_in_file_manager() {

killall yad
open_file_fm=$(cat "$HOME"/Documents/.temp_yad_find_files.txt | sed 's|^|/home/john|')

   if [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     "$FILE_MANAGER" "$open_file_fm"
     bash ~/bin/switch_to_filemanager.sh
   elif [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     "$FILE_MANAGER" "$open_file_fm"
   fi
	
}
export -f open_file_in_file_manager

#----------------------------------------------------------#
# Open file
#----------------------------------------------------------#

open_file() {

killall yad
open_choice=$(cat "$HOME"/Documents/.temp_yad_find_files.txt | sed 's|^|/home/john|')

xdg-open "$open_choice"
	
}
export -f open_file

#----------------------------------------------------------#
# Selected choice from main search
#----------------------------------------------------------#

select_files_main() {

echo "$1" > "$HOME"/Documents/.temp_yad_find_files.txt

}
export -f select_files_main

#----------------------------------------------------------#
# Yad dialog
#----------------------------------------------------------#

find "${dirs_paths[@]}" -type f -iname '*.*' | sed 's|/home/john||g' | GTK_THEME="$MY_GTK_THEME" yad --list \
--column="search" \
--search-column=1 \
--regex-search \
--separator= \
--borders=20 \
--no-headers \
--title="Search" \
--text="\nSearch for files\n" \
--text-align="center" \
--no-markup \
--width=1100 \
--height=600 \
--dclick-action='/bin/bash -c "select_files_main %s"' \
--button="_File manager:/bin/bash -c 'open_file_in_file_manager'" \
--button="_Open file:/bin/bash -c 'open_file'" \
--button="_Exit":1

#----------------------------------------------------------#
# Unset variables and functions
#----------------------------------------------------------#

unset dirs_paths
unset MY_GTK_THEME
unset select_files_main
unset open_file
unset open_file_in_file_manager
unset FILE_MANAGER
