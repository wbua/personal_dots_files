#!/bin/bash
# shellcheck disable=SC2317

# Program command: yad_screenshots.sh
# Description: Takes various screenshots.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 24-04-2024
# Program_license: GPL 3.0
# Dependencies: yad, wl-clipboard, grim, imagemagick, material icons, empty_shot.png

# Copy empty_shot.png into "$HOME"/.cvssgtk2/

# Google material icons link:
# https://github.com/google/material-design-icons/blob/master/font/MaterialIcons-Regular.ttf

#--------------------------------------------------------------------#
# General variables
#--------------------------------------------------------------------#

# File manager
file_manager="nautilus"
export file_manager

# Terminal
myterminal="gnome-terminal"
export myterminal

#--------------------------------------------------------------------#
# Position of yad on screen
#--------------------------------------------------------------------#

# Box x position
x_position="1100" 
export x_position 
# Box y position
y_position="55"
export y_position

#--------------------------------------------------------------------#
# Kill new yad instance
#--------------------------------------------------------------------#

killall yad

#--------------------------------------------------------------------#
# Creates picture directory
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

#--------------------------------------------------------------------#
# This is where the full size screenshots are stored
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Pictures/screenshots ]; then
     :
   else
     mkdir "$HOME"/Pictures/screenshots
   fi

#--------------------------------------------------------------------#
# This is where the temp thumbnails for previews are stored
#--------------------------------------------------------------------#

   if [ -d "$HOME"/.cvssgtk ]; then
     :
   else
     mkdir "$HOME"/.cvssgtk
   fi

#--------------------------------------------------------------------#
# This is where the empty thumbnail is stored
#--------------------------------------------------------------------#

   if [ -d "$HOME"/.cvssgtk2 ]; then
     :
   else
     mkdir "$HOME"/.cvssgtk2
   fi
   
#--------------------------------------------------------------------#
# This is where full size screenshots are stored temporary
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Pictures/.tempfullscrns ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempfullscrns
   fi

#--------------------------------------------------------------------#
# Check if image exists
#--------------------------------------------------------------------#

check_image() {
	
   if [ -z "$(ls -A "$HOME"/Pictures/.tempfullscrns/)" ]; then
     find "$HOME"/.cvssgtk2/ -type f -print0 | xargs -0 ls -tr | tail -n 1
   else
     find "$HOME"/.cvssgtk/ -type f -print0 | xargs -0 ls -tr | tail -n 1
   fi
	
}
export -f check_image

#--------------------------------------------------------------------#
# Takes a fullscreen screenshot
#--------------------------------------------------------------------#

fullscreenshots () {
    killall yad
    rm -f "$HOME"/Pictures/.tempfullscrns/*.png
    rm -f "$HOME"/.cvssgtk/*.png
    sleep 1
    grimshot save screen ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png
    convert "$HOME"/Pictures/.tempfullscrns/*.png -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
    cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/screenshots/
    setsid ~/bin/yad_screenshots.sh >/dev/null 2>&1 & disown
    
}
export -f fullscreenshots

#--------------------------------------------------------------------#
# Takes a select screenshot
#--------------------------------------------------------------------#

select_screenshots () {

    killall yad
    rm -f "$HOME"/Pictures/.tempfullscrns/*.png
    rm -f "$HOME"/.cvssgtk/*.png
    sleep 1
    grimshot save area ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png
    convert "$HOME"/Pictures/.tempfullscrns/*.png -resize 512x288 "$HOME"/.cvssgtk/current_screenshot.png
    cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/screenshots/
    setsid ~/bin/yad_screenshots.sh >/dev/null 2>&1 & disown
    
}
export -f select_screenshots

#--------------------------------------------------------------------#
# Takes a fullscreen screenshot delayed 5 seconds
#--------------------------------------------------------------------#

fullscreenshotsdelay () {
    killall yad
    rm -f "$HOME"/Pictures/.tempfullscrns/*.png
    rm -f "$HOME"/.cvssgtk/*.png
    sleep 1
    sleep 4 && grimshot save screen ~/Pictures/.tempfullscrns/"$(date +'%d-%m-%Y-%H%M%S')".png
    convert "$HOME"/Pictures/.tempfullscrns/*.png -resize 512x512 "$HOME"/.cvssgtk/current_screenshot.png
    cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/screenshots/
    setsid ~/bin/yad_screenshots.sh >/dev/null 2>&1 & disown
    
}
export -f fullscreenshotsdelay


#--------------------------------------------------------------------#
# Open last taken screenshot 
#--------------------------------------------------------------------#

opensrnyadhub () {

killall yad ; find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open

}
export -f opensrnyadhub 


#--------------------------------------------------------------------#
# Copy screenshot png to clipboard
#--------------------------------------------------------------------#

shot_to_clip() {
    	
killall yad
wl-copy --type image/png < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"  
  
}
export -f shot_to_clip

#--------------------------------------------------------------------#
# Select screenshot in file manager
#--------------------------------------------------------------------#  
  
dir_shot() {

GTK_THEME="Yaru"
killall yad 
chosen=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -iname '*.*' | sort | cut -d '/' -f6-)
export chosen
[ -z "$chosen" ] && exit 0
"$file_manager" "$HOME"/Pictures/screenshots/"$chosen"  
  
}
export -f dir_shot

#--------------------------------------------------------------------#
# Open screenshot in gimp
#--------------------------------------------------------------------# 

open_gimp() {

temp_shot=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)	
export temp_shot
killall yad ; gimp "$temp_shot"
	
}
export -f open_gimp

#--------------------------------------------------------------------#
# Send screenshot filename to clipboard
#--------------------------------------------------------------------# 

send_clip() {
	
killall yad	
find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | wl-copy -n	
	
}
export -f send_clip

#--------------------------------------------------------------------#
# Delete screenshot
#--------------------------------------------------------------------# 

delete_shot() {
	
killall yad
myfilename=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | cut -d '/' -f6-)
export myfilename
rm -f "$HOME"/Pictures/screenshots/"$myfilename"
rm -f "$HOME"/Pictures/.tempfullscrns/*.*
rm -f "$HOME"/.cvssgtk/*.*	
yad_screenshots.sh	
	
}
export -f delete_shot

#--------------------------------------------------------------------#
# Open screenshot directory in file manager (gui)
#--------------------------------------------------------------------#

dir_gui() {
	
killall yad
xdg-open "$HOME"/Pictures/screenshots/

}
export -f dir_gui

#--------------------------------------------------------------------#
# Open screenshot directory in terminal (cli)
#--------------------------------------------------------------------#

dir_cli() {
	
killall yad
"$myterminal" -- /bin/bash -c "cd ; cd Pictures/screenshots ; ls -l -t -r; $SHELL"

}
export -f dir_cli

#--------------------------------------------------------------------#
# Yad settings
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog22" yad --form --width=600 --height=249 --posx="$x_position" --posy="$y_position" \
--columns=2 --buttons-layout="center" --borders=20 \
--text-align="center" --title="Screenshot tool" \
--image="$(check_image)" \
--field=" ":LBL "" \
--field=" ":LBL "" \
--field=" ":LBL "" \
--field=" ":LBL "" \
--field=" ":LBL "" \
--field=" ":LBL "" \
--field=" ":LBL "" \
--field=" ":LBL "" \
--field=" ":LBL "" \
--field=" ":LBL "" \
--field="<span color='#ffffff' font_family='Material Icons' font='24'><b></b></span>":fbtn '/bin/bash -c "open_gimp"' \
--field="<span color='#ffffff' font_family='Material Icons' font='24'><b></b></span>":fbtn '/bin/bash -c "send_clip"' \
--field="<span color='#ffffff' font_family='Material Icons' font='24'><b></b></span>":fbtn '/bin/bash -c "delete_shot"' \
--field="<span color='#ffffff' font_family='Material Icons' font='24'><b></b></span>":fbtn '/bin/bash -c "dir_gui"' \
--field="<span color='#ffffff' font_family='Material Icons' font='24'><b></b></span>":fbtn '/bin/bash -c "dir_cli"' \
--field=" ":LBL "" \
--field=" ":LBL "" \
--field=" ":LBL "" \
--field=" ":LBL "" \
--button="<span color='#27FF00' font_family='Material Icons' font='24'></span>:/bin/bash -c 'fullscreenshots'" \
--button="<span color='#27FF00' font_family='Material Icons' font='24'>󰩬</span>:/bin/bash -c 'select_screenshots'" \
--button="<span color='#27FF00' font_family='Material Icons' font='24'></span>:/bin/bash -c 'fullscreenshotsdelay'" \
--button="<span color='#ffffff' font_family='Material Icons' font='24'></span>:/bin/bash -c 'dir_shot'" \
--button="<span color='#ffffff' font_family='Material Icons' font='24'></span>:/bin/bash -c 'shot_to_clip'" \
--button="<span color='#ffffff' font_family='Material Icons' font='24'></span>:/bin/bash -c 'opensrnyadhub'" \
--button="<span color='#ffffff' font_family='Material Icons' font='24'></span>:/bin/bash -c 'killall yad'"

#--------------------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------------------#

unset fullscreenshots
unset opensrnyadhub
unset fullscreenshotsdelay
unset select_screenshots
unset y_position
unset x_position
unset shot_to_clip
unset dir_shot
unset chosen
unset open_gimp
unset temp_shot
unset send_clip
unset delete_shot
unset myfilename
unset check_image
unset dir_gui
unset myterminal
unset dir_cli
