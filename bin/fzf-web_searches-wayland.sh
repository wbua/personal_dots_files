#!/bin/bash

# Program command: fzf-web_searches-wayland.sh
# Description: This script lets you do various web searches.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 06-12-2023
# Program_license: GPL 3.0
# Dependencies: fzf

#------------------------------------------#
# Software preferences
#------------------------------------------#

browser="brave"

#------------------------------------------#
# Create files and directories
#------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ -f "$HOME"/Documents/mainprefix/.last_web_search.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/.last_web_search.txt
   fi
      
#------------------------------------------#
# Web searches
#------------------------------------------#

# Google search
google_search() {

truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://www.google.co.uk/search?q={}"
echo "" | fzf --print-query \
--cycle \
--info=inline \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--reverse \
--header='
Press F1 for main menu

' \
--prompt 'google search> ' \
--bind='F1:execute(fzf-launcher-wayland.sh {})' | \
tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
fzf-launcher-wayland.sh

}

# Youtube search
youtube_search() {

truncate -s 0 "$HOME"/Documents/mainprefix/.last_web_search.txt
filesearch="$HOME/Documents/mainprefix/.last_web_search.txt"
search="https://www.youtube.com/results?search_query={}"
echo "" | fzf --print-query \
--reverse \
--info=inline \
--cycle \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--header='
Press F1 for main menu

' \
--prompt 'google search> ' \
--bind='F1:execute(fzf-launcher-wayland.sh {})' | \
tee ~/Documents/mainprefix/.last_web_search.txt > /dev/null
head "$filesearch" | xargs -I{} "$browser" "$search"
fzf-launcher-wayland.sh
	
}

# Google recent search
google_recent() {
	
search="https://www.google.co.uk/search?q={}"
tail -n 1 "$HOME"/Documents/mainprefix/.last_web_search.txt | xargs -I{} "$browser" "$search"
fzf-launcher-wayland.sh

}

# Youtube recent search
youtube_recent() {

search="https://www.youtube.com/results?search_query={}"
tail -n 1 "$HOME"/Documents/mainprefix/.last_web_search.txt | xargs -I{} "$browser" "$search"	
fzf-launcher-wayland.sh

}


#------------------------------------------#
# Menu
#------------------------------------------#

menu() {

echo "google"
echo "youtube"
echo "google-recent"
echo "youtube-recent"
	
}

#------------------------------------------#
# Main
#------------------------------------------#

main() {

choice=$(menu | fzf --reverse \
--cycle \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--info=inline \
--header='
Press F1 for main menu

' \
--prompt 'Search the web> ' \
--bind='F1:execute(fzf-launcher-wayland.sh {})')

case "$choice" in

 'google')
   google_search
  ;;

 'youtube')
   youtube_search
  ;;

 'google-recent')
   google_recent
  ;;

 'youtube-recent')
   youtube_recent
  ;;

 *)
   echo "something went wrong!" || exit 1
  ;;

esac

	
}
main
