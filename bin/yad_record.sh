#!/bin/bash
# shellcheck disable=SC2089
# shellcheck disable=SC2090

# Program command: yad_record.sh
# Description: Screen recorder.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 28-04-2024
# Program_license: GPL 3.0
# Dependencies: yad, wf-recorder, imagemagick, material icons

#------------------------------------------------------------------#
# General variables
#------------------------------------------------------------------#

# File manager
file_manager="thunar"
export file_manager

# Screencast resolution
recordres="1920x1080"
export recordres

# Scripts location
bin_dir="$HOME/bin"
export bin_dir

#------------------------------------------------------------------#
# Checks to see if you are running a wayland session
#------------------------------------------------------------------#

   if [[ "$(echo $WAYLAND_DISPLAY)" == "" ]]; then
     yad --error \
     --width 500 \
     --title "Wayland session not found!" \
     --text "Run launcher error, wayland only tool!"
     exit 1
   else
     :
   fi

#------------------------------------------------------------------#
# Help (type radioicons.sh -h in terminal)
#------------------------------------------------------------------#

_help(){
cat << EOF
...

## Tracking

    * Program command: yad_record.sh
    * Description: Screen recorder.
    * Contributors: John Mcgrath
    * Program_version: 1.0.0
    * Program updated: 07-09-2023
    * Program_license: GPL 3.0
    * Website: https://sourceforge.net/projects/jm-dots/files/
    * Dependencies: yad, ffmpeg, material icons

## Description

    * File: plays the last recorded file.
    * Directory: Shows the last recorded file in file manager.
    * Record: Start screen recording.
    * Stop: Stops screen recording.

## Dependencies
   
    * Yad
    * wf-recorder
    * Material Icons (see in other information)
    
## Installation

   When I use double quotes, it is just for emphasis.

   Do not type the double quotes, in less told to do so.
   
   Do not type the "$" character, in less told to do so.

   ---

   Put script into the directory below.

   Example:

   "/home/user/.local/bin/"

   ---
   
   To make the script executable follow the example below.

   Example:

   $ chmod u+x yad_record.sh
   
   ---
   
   Now in the terminal, run this command "yad_record.sh -h".
   
   It will list out a help file. You will need to install the dependencies.
   
   ---

   Always update your system first.

   Example:

   $ sudo apt update

   ---

   You can now install the dependencies.

   Example:

   $ sudo apt install yad ffmpeg

   ---
   
## Other information

   * Download material icons: https://github.com/google/material-design-icons/tree/master/font
     
EOF
}

while getopts ":h" option; do
  case $option in
    h) # display help
    _help
    exit;;
   \?) # incorrect option
    echo "Error: invalid option"
    exit;;
  esac
done

#------------------------------------------------------------------#
# Check for installed dependencies
#------------------------------------------------------------------#

   if [[ "$(dpkg -s ffmpeg | grep "Status:" | awk '{$1="";print $0}' | sed 's/ //')" != *"install ok"* ]]; then
     notify-send "Dependency ffmpeg missing"
     killall yad
     sleep 5
   else
     :
   fi
      
#------------------------------------------------------------------#
# Create files and directories
#------------------------------------------------------------------#

# Create videos directory

   if [ -d "$HOME"/Videos ]; then
     :
   else
     mkdir "$HOME"/Videos
   fi
   
# Create documents directory

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi
   
# Create recordings directory

   if [ -d "$HOME"/Videos/recordings ]; then
     :
   else
     mkdir "$HOME"/Videos/recordings
   fi

# This is where the video temporary recordings are stored

   if [ -d "$HOME"/Videos/.yadvidrec ]; then
     :
   else
     mkdir "$HOME"/Videos/.yadvidrec
   fi

# Thumbnail from video

   if [ -d "$HOME"/Videos/.temp_record_image ]; then
     :
   else
     mkdir "$HOME"/Videos/.temp_record_image
   fi


# Create radio status text
   if [ -f "$HOME"/Documents/.record_status.txt ]; then
     :
   else
     touch "$HOME"/Documents/.record_status.txt
     echo "off" > "$HOME"/Documents/.record_status.txt
   fi
   
#------------------------------------------------------------------#
# Colors for help page (pango markup)
#------------------------------------------------------------------#

# Selected
color1="<span color='#25FF00' font_family='Monospace' font='12' weight='bold'>"
export color1
# Heading
color2="<span color='#EBFF00' font_family='Monospace' font='14' weight='bold'>"
export color2
# Normal text
color3="<span color='#FFFFFF' font_family='Monospace' font='12' weight='bold'>"
export color3
# Selected 2
color4="<span color='#F500FF' font_family='Monospace' font='12' weight='bold'>"
export color4
# Color end
end="</span>"
export end

#------------------------------------------------------------------#
# Video playing file status
#------------------------------------------------------------------#

     playfile=$(cat "$HOME"/Documents/.record_status.txt)
     export playfile
   if [[ -z "$playfile" ]]; then
     echo "off" > "$HOME"/Documents/.record_status.txt
   else
     :
   fi

#------------------------------------------------------------------#
# Change file manager
#------------------------------------------------------------------#

_change_file_manager() {

killall yad
sed -i "18s|^.*$|file_manager=\"$(GTK_THEME="Adwaita-dark" yad --entry --center --text="Change file manager")\"|" "$bin_dir"/yad_record.sh

}
export -f _change_file_manager

#------------------------------------------------------------------#
# Change screen resolution
#------------------------------------------------------------------#

_change_srn_res() {

killall yad
sed -i "22s|^.*$|recordres=\"$(GTK_THEME="Adwaita-dark" yad --entry --center --text="Change screen resolution")\"|" "$bin_dir"/yad_record.sh

}
export -f _change_srn_res

#------------------------------------------------------------------#
# Start recording
#------------------------------------------------------------------# 

_start_recording() {
     
   if [[ "$playfile" == 'off' ]]; then
     echo "on" > "$HOME"/Documents/.record_status.txt
     killall yad
     rm -f "$HOME"/Videos/.yadvidrec/*.*
     notify-send "Recording started!" && wf-recorder -f "$HOME"/Videos/.yadvidrec/"$(date +'%d-%m-%Y-%H%M%S')".mkv
   elif [[ "$playfile" == 'on' ]]; then
     killall yad
     echo "off" > "$HOME"/Documents/.record_status.txt
     killall -s SIGINT wf-recorder
     cp "$HOME"/Videos/.yadvidrec/*.* "$HOME"/Videos/recordings/
   else
     :
   fi

}
export -f _start_recording

#------------------------------------------------------------------#
# Check state of recording
#------------------------------------------------------------------#

_check_recording(){
 
   if [[ "$playfile" == 'off' ]]; then
	 echo "<span color='#FFFFFF' font_family='Material Icons' font='20'></span>"
   elif [[ "$playfile" == 'on' ]]; then
	 echo "<span color='#FF002E' font_family='Material Icons' font='20'></span>"
   fi

}
export -f _check_recording

#------------------------------------------------------------------#
# Kill recording
#------------------------------------------------------------------#

_kill_recording() {

   if [[ "$(cat "$HOME"/Documents/.record_status.txt)" == 'on' ]]; then
     killall yad
     echo "off" > "$HOME"/Documents/.record_status.txt
     killall -s SIGINT wf-recorder
     cp "$HOME"/Videos/.yadvidrec/*.* "$HOME"/Videos/recordings/
     rm -f "$HOME"/Videos/.temp_record_image/*
     convert "$HOME"/Videos/.yadvidrec/*.mkv[10] "$HOME"/Videos/.temp_record_image/thumbnail.png
     convert "$HOME"/Videos/.temp_record_image/thumbnail.png -resize 512x288 "$HOME"/Videos/.temp_record_image/thumbnail01.png   
     notify-send "Recording stopped!"
   else
     :
   fi

}
export -f _kill_recording

#------------------------------------------------------------------#
# Open recording
#------------------------------------------------------------------#

_open_recording() {
	
killall yad
find "$HOME"/Videos/.yadvidrec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open

}
export -f _open_recording

#------------------------------------------------------------------#
# Open last recording in file manager
#------------------------------------------------------------------#

_open_last_file() {

killall yad	
chosen=$(find "$HOME"/Videos/.yadvidrec/ -type f -iname '*.*' | sort | cut -d '/' -f6-)
[ -z "$chosen" ] && exit 0
"$file_manager" "$HOME"/Videos/recordings/"$chosen"

}
export -f _open_last_file


#------------------------------------------------------------------#
# Display thumbnail from image
#------------------------------------------------------------------#

_display_thumbnail() {

:
	
}
export -f _display_thumbnail

#------------------------------------------------------------------#
# Settings button
#------------------------------------------------------------------#

_settings_menu() {

killall yad
	
GTK_THEME="Adwaita-dark" yad --form --width=400 --height=300 --columns=1 --borders=20 \
--field="<b><span color='#FFFFFF' font_family='Monospace' font='14'>Change resolution</span></b>":fbtn '/bin/bash -c "_change_srn_res"' \
--field="<b><span color='#FFFFFF' font_family='Monospace' font='14'>Change File manager</span></b>":fbtn '/bin/bash -c "_change_file_manager"' \
--button="<span color='#ffffff' font_family='Material Icons' font='20'></span>":1

}
export -f _settings_menu

#------------------------------------------------------------------#
# Main
#------------------------------------------------------------------#

_main() {

# Main page
GTK_THEME="Adwaita-dark" yad --form --width=450 --height=400 \
--title="Screen Recorder" \
--image-on-top \
--columns=4 \
--borders=50 \
--image-on-top \
--image="$HOME/Videos/.temp_record_image/thumbnail01.png" \
--text-align="center" \
--buttons-layout="center" \
--field="<b><span color='#ffffff' font_family='Material Icons' font='25'></span></b>":fbtn '/bin/bash -c "_open_recording"' \
--field="<b><span color='#ffffff' font_family='Material Icons' font='25'></span></b>":fbtn '/bin/bash -c "_open_last_file"' \
--field="<b><span color='#ffffff' font_family='Material Icons' font='25'>$(_check_recording)</span></b>":fbtn '/bin/bash -c "_start_recording"' \
--field="<b><span color='#ffffff' font_family='Material Icons' font='25'></span></b>":fbtn '/bin/bash -c "_kill_recording"' \
--button="<span color='#ffffff' font_family='Material Icons' font='25'></span>:/bin/bash -c '_settings_menu'" \
--button="<span color='#ffffff' font_family='Material Icons' font='25'></span>":1
	
}	
export -f _main
_main

#------------------------------------------------------------------#
# Unset variables and functions
#------------------------------------------------------------------#

# Functions
unset _kill_recording
unset _yad
unset _start_recording
unset _main
unset _check_recording
unset _open_recording
unset _open_last_file
unset _change_file_manager
unset _settings_menu
unset _change_srn_res
unset _help
unset _display_thumbnail

# Variables
unset recordres
unset file_manager
unset bin_dir
unset playfile
unset help_page
unset color1
unset color2
unset color3
unset color4
unset end
