#!/bin/bash

# Program command: johns_files_directories.sh
# Description: Creates files and directories.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 11-04-2024
# Program_license: GPL 3.0

#-------------------------------------------------------#
# Create directories and files
#-------------------------------------------------------#

create_file_and_directories() {

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Videos ]; then
     :
   else
     mkdir "$HOME"/Videos
   fi
   
# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Music ]; then
     :
   else
     mkdir "$HOME"/Music
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Downloads ]; then
     :
   else
     mkdir "$HOME"/Downloads
   fi

# This is where bash scripts are stored

   if [ -d "$HOME"/bin ]; then
     :
   else
     mkdir "$HOME"/bin
   fi

# Checks if directory exists, if not creates it

   if [ -d  "$HOME"/Pictures/.root_resize ]; then
     :
   else
     mkdir  "$HOME"/Pictures/.root_resize
   fi

 # Checks if directory exists, if not creates it

   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Documents/prefix_notes ]; then
     :
   else
     mkdir "$HOME"/Documents/prefix_notes
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Pictures/rofi_colors ]; then
     :
   else
     mkdir "$HOME"/Pictures/rofi_colors
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Pictures/rofi_themes_pics ]; then
     :
   else
     mkdir "$HOME"/Pictures/rofi_themes_pics
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Pictures/.temqrcodes ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Pictures/qr_codes ]; then
     :
   else
     mkdir "$HOME"/Pictures/qr_codes
   fi
   
# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Downloads/.temp_downloads ]; then
     :
   else
     mkdir "$HOME"/Downloads/.temp_downloads
   fi   
   
# This is where the full size screenshots are stored

   if [ -d "$HOME"/Pictures/wallpapers ]; then
     :
   else
     mkdir "$HOME"/Pictures/wallpapers
   fi

# Random wallpaper

   if [ -f "$HOME"/Pictures/.fehwallrandom.txt ]; then
     :
   else
     touch "$HOME"/Pictures/.fehwallrandom.txt
   fi

# Quick links

   if [ -f "$HOME"/Documents/mainprefix/quick_links.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/quick_links.txt
   fi
   
# Sway wallpapers

   if [ -f "$HOME"/Pictures/.swaywall.txt ]; then
     :
   else
     touch "$HOME"/Pictures/.swaywall.txt
   fi
   
# Snippets

   if [ -f "$HOME"/Documents/mainprefix/snippets.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/snippets.txt
   fi
   
# This is where full size screenshots are stored temporary

   if [ -d "$HOME"/Pictures/.tempfullscrns ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempfullscrns
   fi

# This is where the full size screenshots are stored

   if [ -d "$HOME"/Pictures/screenshots ]; then
     :
   else
     mkdir "$HOME"/Pictures/screenshots
   fi

# This is where the video temporary recordings are stored

   if [ -d "$HOME"/Videos/.yadvidrec ]; then
     :
   else
     mkdir "$HOME"/Videos/.yadvidrec
   fi

# This is where audio recordings are stored temporary

   if [ -d "$HOME"/Music/.yadaudiorec ]; then
     :
   else
     mkdir "$HOME"/Music/.yadaudiorec
   fi

# This is where the video recordings are stored

   if [ -d "$HOME"/Videos/recordings ]; then
     :
   else
     mkdir "$HOME"/Videos/recordings
   fi

# Checks if the recordings directory exists, if not creates it

   if [ -d "$HOME"/Music/recordings ]; then
     :
   else
     mkdir "$HOME"/Music/recordings
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Documents/pdf ]; then
     :
   else
     mkdir "$HOME"/Documents/pdf
   fi

# Checks if the temp pdf directory exists, if not creates it

   if [ -d "$HOME"/Documents/.pdftemp ]; then
     :
   else
     mkdir "$HOME"/Documents/.pdftemp
   fi

# Checks if the file bookmarks.txt exists, if not creates it

   if [ -f "$HOME"/Documents/mainprefix/bookmarks.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/bookmarks.txt
   fi
   
# Checks if the file quicknotes.txt exists, if not creates it

   if [ -f "$HOME"/Documents/mainprefix/quicknotes.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/quicknotes.txt
   fi

# Checks if the file rofi_keywords.txt exists, if not creates it

   if [ -f "$HOME"/Documents/mainprefix/rofi_keywords.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/rofi_keywords.txt
   fi
   
   
# Checks if the file .temp_sums.txt exists, if not creates it

   if [ -f "$HOME"/Documents/.temp_sums.txt ]; then
     :
   else
     touch "$HOME"/Documents/.temp_sums.txt 
   fi
   
# Checks if the file .combine_sums.txt exists, if not creates it

   if [ -f "$HOME"/Documents/.combine_sums.txt ]; then
     :
   else
     touch "$HOME"/Documents/.combine_sums.txt 
   fi
   
# Checks if the file .combine_sums.txt exists, if not creates it

   if [ -f "$HOME"/Documents/.sums_mixed.txt ]; then
     :
   else
     touch "$HOME"/Documents/.sums_mixed.txt
   fi

# Checks if the file radiolinks.txt exists, if not creates it

   if [ -f "$HOME"/Documents/mainprefix/radiolinks.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/radiolinks.txt
   fi

# Checks if the file configs.txt exists, if not creates it

   if [ -f "$HOME"/Documents/mainprefix/configs.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/configs.txt
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Videos/.ytdownvids ]; then
     :
   else
     mkdir "$HOME"/Videos/.ytdownvids
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Videos/youtube ]; then
     :
   else
     mkdir "$HOME"/Videos/youtube
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Documents/zips ]; then
     :
   else
     mkdir "$HOME"/Documents/zips
   fi
   
# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Documents/.zips ]; then
     :
   else
     mkdir "$HOME"/Documents/.zips
   fi

# Checks if the file zipfile.txt exists, if not creates it

   if [ -f "$HOME"/Documents/.zipfile.txt ]; then
     :
   else
     touch "$HOME"/Documents/.zipfile.txt
   fi

# Adds app to text file

   if [ -f "$HOME"/Documents/mainprefix/rofi_apps.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/rofi_apps.txt
   fi
   
# Screencast

   if [ -f "$HOME"/Documents/.rofi_record_status.txt ]; then
     :
   else
     touch "$HOME"/Documents/.rofi_record_status.txt
     echo "off" > "$HOME"/Documents/.rofi_record_status.txt
   fi
   
# Send radio recording to main directory

   if [ -d "$HOME"/Music/radio_recordings ]; then
     :
   else
     mkdir "$HOME"/Music/radio_recordings
   fi

# This is where recorded streams are stored temporary

   if [ -d "$HOME"/Music/.temprecfile ]; then
     :
   else
     mkdir "$HOME"/Music/.temprecfile
   fi

#  This is where temp screenshot preview is stored

   if [ -d "$HOME"/.cvssgtk ]; then
     :
   else
     mkdir "$HOME"/.cvssgtk
   fi
   
# Decode qr code OCR directory
 
   if [ -d "$HOME"/Pictures/.ocr/ ]; then
     :
   else
     mkdir "$HOME"/Pictures/.ocr/
   fi   

# Checks if directory exists, if not creates it
   if [ -d "$HOME"/Downloads/.temp_weather ]; then
     :
   else
     mkdir "$HOME"/Downloads/.temp_weather
   fi  
   
# Last web search
   if [ -f "$HOME"/Documents/mainprefix/.last_web_search.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/.last_web_search.txt
   fi

# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Pictures/converted_preview_images ]; then
     :
   else
     mkdir "$HOME"/Pictures/converted_preview_images
   fi

# All actions

   if [ -f "$HOME"/Documents/.all_actions.txt ]; then
     :
   else
     touch "$HOME"/Documents/.all_actions.txt
   fi
   
# Actions downloads

   if [ -f "$HOME"/Documents/.all_actions_download.txt ]; then
     :
   else
     touch "$HOME"/Documents/.all_actions_download.txt
   fi

# Create mpv recording status
   if [ -f "$HOME"/Documents/.mpv_recording.txt ]; then
     :
   else
     touch "$HOME"/Documents/.mpv_recording.txt
     echo "off" > "$HOME"/Documents/.mpv_recording.txt
   fi
   
# Checks if directory exists, if not creates it

   if [ -d "$HOME"/Documents/palettes ]; then
     :
   else
     mkdir "$HOME"/Documents/palettes
   fi   

# Last used keywords

   if [ -f "$HOME"/Documents/.keyword_last_used.txt ]; then
     :
   else
     touch "$HOME"/Documents/.keyword_last_used.txt
   fi

# Quicklink root search
   if [ -f "$HOME"/Documents/.ql_main_root.txt ]; then
     :
   else
     touch "$HOME"/Documents/.ql_main_root.txt
   fi
   
# Reminders text
   if [ -f "$HOME"/Documents/.reminders.txt ]; then
     :
   else
     touch "$HOME"/Documents/.reminders.txt
   fi
   
# Reminders temp text
   if [ -f "$HOME"/Documents/.edit_reminders.txt ]; then
     :
   else
     touch "$HOME"/Documents/.edit_reminders.txt
   fi

# Create temp file for resizing image
   if [ -f "$HOME"/Pictures/.temp_resize_image.txt ]; then
     :
   else
     touch "$HOME"/Pictures/.temp_resize_image.txt
   fi

# Create temp file for resizing image
   if [ -f "$HOME"/Pictures/.resize_root_temp ]; then
     :
   else
     touch "$HOME"/Pictures/.resize_root_temp
   fi

# All files for copied and moved
   if [ -f "$HOME"/Documents/.all_cpmv.txt ]; then
     :
   else
     touch "$HOME"/Documents/.all_cpmv.txt
   fi   

}
