#!/bin/bash

# Program command: image_converter.sh
# Description: Convert images using imagemagick.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 29-08-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad, imagemagick

#---------------------------------------------------------#
# Create files and directories
#---------------------------------------------------------#

   if [ -d "$HOME"/Pictures/.convert_pics ]; then
     :
   else
     mkdir "$HOME"/Pictures/.convert_pics
   fi

#---------------------------------------------------------#
# List of imagemagick effects
#---------------------------------------------------------#

conversions="!Greyscale !Negate !Blur !Sepia !Emboss !Black border !Minus rotate !Plus rotate"
export conversions

#---------------------------------------------------------#
# Open last converted image in image viewer
#---------------------------------------------------------#

open_image() {

killall yad
find "$HOME"/Pictures/.convert_pics/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open	

}
export -f open_image

#---------------------------------------------------------#
# Yad settings
#---------------------------------------------------------#

choice=$(GTK_THEME="Yaru-dark" yad --form --width=800 --height=300 --title="Imagemagick Converter" \
--borders=20 \
--separator="#" \
--field="<span color='#FFFFFF' font_family='Monospace' font='11'>Convert </span>:CB" "$conversions" \
--field="<span color='#FFFFFF' font_family='Monospace' font='11'>Input </span>":FL \
--field="<span color='#FFFFFF' font_family='Monospace' font='11'>Output </span>":DIR \
--button="<span color='#FFFFFF' font_family='Monospace' font='11'>Image</span>:/bin/bash -c 'open_image'" \
--button="<span color='#FFFFFF' font_family='Monospace' font='11'>Convert</span>":0 \
--button="<span color='#FFFFFF' font_family='Monospace' font='11'>Exit</span>":1)
export choice

#---------------------------------------------------------#
# Yad field variables
#---------------------------------------------------------#

type=$(echo "$choice" | awk -F '#' '{print $1}')
export type
file=$(echo "$choice" | awk -F '#' '{print $2}')
export file
dir=$(echo "$choice" | awk -F '#' '{print $3}')
export dir

#---------------------------------------------------------#
# Main
#---------------------------------------------------------#

main() {

   if [[ "$type" == *"Greyscale"* ]]; then
     rm -f "$HOME"/Pictures/.convert_pics/*.*
     convert "$file" -set colorspace Gray -separate -average "$HOME"/Pictures/.convert_pics/"$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.convert_pics/*.png "$dir"
   elif [[ "$type" == *"Negate"* ]]; then
     rm -f "$HOME"/Pictures/.convert_pics/*.*
     convert "$file" -channel RGB -negate "$HOME"/Pictures/.convert_pics/"$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.convert_pics/*.png "$dir"
   elif [[ "$type" == *"Blur"* ]]; then
     rm -f "$HOME"/Pictures/.convert_pics/*.*
     convert "$file" -blur 0x5 "$HOME"/Pictures/.convert_pics/"$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.convert_pics/*.png "$dir"
   elif [[ "$type" == *"Sepia"* ]]; then
     rm -f "$HOME"/Pictures/.convert_pics/*.*
     convert "$file" -sepia-tone 80% "$HOME"/Pictures/.convert_pics/"$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.convert_pics/*.png "$dir"
   elif [[ "$type" == *"Emboss"* ]]; then
     rm -f "$HOME"/Pictures/.convert_pics/*.*
     convert "$file" -emboss 10 "$HOME"/Pictures/.convert_pics/"$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.convert_pics/*.png "$dir"
   elif [[ "$type" == *"Black border"* ]]; then
     rm -f "$HOME"/Pictures/.convert_pics/*.*
     convert "$file" -bordercolor black -border 20 "$HOME"/Pictures/.convert_pics/"$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.convert_pics/*.png "$dir"
   elif [[ "$type" == *"Minus rotate"* ]]; then
     rm -f "$HOME"/Pictures/.convert_pics/*.*
     convert "$file" -rotate -90 "$HOME"/Pictures/.convert_pics/"$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.convert_pics/*.png "$dir"
   elif [[ "$type" == *"Plus rotate"* ]]; then
     rm -f "$HOME"/Pictures/.convert_pics/*.*
     convert "$file" -rotate 90 "$HOME"/Pictures/.convert_pics/"$(date '+%d-%m-%Y-%H-%M-%S')".png
     cp "$HOME"/Pictures/.convert_pics/*.png "$dir"
   else
     :
   fi 
    
}
export -f main
main

#---------------------------------------------------------#
# Unset variables and functions
#---------------------------------------------------------#

unset choice 
unset file
unset dir
unset main
unset type
unset open_image
unset conversions
