#!/bin/bash

choice=$(echo "" | rofi -dmenu -i -p '' | sed 's|#||')
hex="$choice"
printf "%d %d %d\n" 0x${hex:0:2} 0x${hex:2:2} 0x${hex:4:2} | xclip -selection clipboard
