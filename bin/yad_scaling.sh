#!/bin/bash

# Program command: yad_dialog_resolution.sh
# Description: Sets the yad dialog size for width, height and font size. 
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 18-05-2024
# Program_license: GPL 3.0
# Dependencies: yad, wlr-randr

#--------------------------------------------------------------#
# Error checking
#--------------------------------------------------------------#

set -e

#--------------------------------------------------------------#
# Main
#--------------------------------------------------------------#

# Checks the current screen resolution of your monitor
current_mode=$(wlr-randr | grep "current" | awk '{print $1}')

   # 1920 x 1080
   if [[ "$current_mode" == "1920x1080" ]]; then
     YAD_WIDTH="800"
     YAD_HEIGHT="600"
     TEXT_FONT_SIZE="18"
     ICON_FONT_SIZE="20"
     MAIN_GTK_THEME="alt-dialog20"
   # 2560 x 1440
   elif [[ "$current_mode" == "2560x1440" ]]; then
     YAD_WIDTH="800"
     YAD_HEIGHT="600"
     TEXT_FONT_SIZE="18"
     ICON_FONT_SIZE="20"
     MAIN_GTK_THEME="alt-dialog21"
   # 3840 x 2160
   elif [[ "$current_mode" == "3840x2160" ]]; then
     YAD_WIDTH="800"
     YAD_HEIGHT="600"
     TEXT_FONT_SIZE="18"
     ICON_FONT_SIZE="20"
     MAIN_GTK_THEME="alt-dialog21"
   # 1280 x 720
   elif [[ "$current_mode" == "1280x720" ]]; then
     YAD_WIDTH="800"
     YAD_HEIGHT="600"
     TEXT_FONT_SIZE="18"
     ICON_FONT_SIZE="20"
     MAIN_GTK_THEME="alt-dialog21"
   else
     :
   fi
