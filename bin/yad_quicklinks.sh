#!/bin/bash

# Program command: yad_quicklinks.sh
# Description: Quick links to files, paths, web sites, google searches and web query searches.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 22-08-2024
# Program_license: GPL 3.0
# Dependencies: yad, wl-clipboard

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-----------------------------------------------------#
# Error checking
#-----------------------------------------------------#

set -e

#-----------------------------------------------------#
# User preferences
#-----------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# File manager
FILE_MANAGER="$MAIN_FILE_MANAGER"
export FILE_MANAGER

# Browser
MY_BROWSER="$MAIN_BROWSER"
export MY_BROWSER

# Terminal
MY_TERMINAL="$MAIN_TERMINAL"
export MY_TERMINAL

# Text editor
TEXT_EDITOR="$MAIN_TEXT_EDITOR"
export TEXT_EDITOR

# Quickslink file
QUICKLINKS_FILE="$MAIN_QUICKLINKS_FILE"
export QUICKLINKS_FILE

#-----------------------------------------------------#
# Create files and directories
#-----------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ "$QUICKLINKS_FILE" = "disable" ]; then
     :
   elif [ -f "$QUICKLINKS_FILE" ]; then
     :
   else
     notify-send "Quicklinks file does not exist!" && exit 1
   fi

#-----------------------------------------------------#
# Remove whitespace
#-----------------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$QUICKLINKS_FILE"

#-----------------------------------------------------#
# Add item to text file
#-----------------------------------------------------#

add_file() {

killall yad
add_choice=$( GTK_THEME="$MY_GTK_THEME" yad --entry \
--text="\nAdd item to text file...\n" \
--text-align="center" \
--width=800 \
--height=100 \
--borders=20 \
--button="_Exit":1)
[ -z "$add_choice" ] && exit 0

echo "$add_choice" | tee -a "$QUICKLINKS_FILE"
	
}
export -f add_file

#-----------------------------------------------------#
# Open quicklinks text file
#-----------------------------------------------------#

open_text_file() {

killall yad
links_text=$(echo "$QUICKLINKS_FILE") 
[ -z "$links_text" ] && exit 0
echo "$links_text" > /dev/null

   if [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     "$TEXT_EDITOR" "$QUICKLINKS_FILE"
     bash ~/bin/switch_to_texteditor.sh
   elif [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     "$TEXT_EDITOR" "$QUICKLINKS_FILE"
   else
     :
   fi

}
export -f open_text_file

#-----------------------------------------------------#
# Copy item to clipboard
#-----------------------------------------------------#

copy_to_clip() {

links_text=$(cat "$HOME"/Documents/.temp_yad_quicklink.txt) 
[ -z "$links_text" ] && exit 0
echo "$links_text" | wl-copy -n
	
}
export -f copy_to_clip

#-----------------------------------------------------#
# Open path in terminal
#-----------------------------------------------------#

open_terminal() {

killall yad
links_text=$(cat "$HOME"/Documents/.temp_yad_quicklink.txt) 
[ -z "$links_text" ] && exit 0
echo "$links_text" | tee "$HOME"/Documents/.cd_path.txt
path_file=$(cat "$HOME"/Documents/.cd_path.txt)
"$MY_TERMINAL" -- /bin/bash -c "cd \"$path_file\"; $SHELL"

}
export -f open_terminal

#-----------------------------------------------------#
# Query search web link
#-----------------------------------------------------#

query_search() {

killall yad

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     links_text=$(cat "$HOME"/Documents/.temp_yad_quicklink.txt) 
     [ -z "$links_text" ] && exit 0
     echo "$links_text" | tee "$HOME"/Documents/.temp_query.txt
     sed -i "s|$|\"$(echo "" |  GTK_THEME="$MY_GTK_THEME" yad --entry --width=600 --height=100 \
     --text="\nAdd query search term...\n" \
     --text-align="center" \
     --button="_Exit":1)\"|" "$HOME"/Documents/.temp_query.txt
     cat "$HOME"/Documents/.temp_query.txt | tr -d '"' | tee "$HOME"/Documents/.temp_query.txt
     sed -i 's| |+|g' "$HOME"/Documents/.temp_query.txt
     choice=$(cat "$HOME"/Documents/.temp_query.txt)
     [ -z "$choice" ] && exit 0
     "$MY_BROWSER" "$choice" 
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     links_text=$(cat "$HOME"/Documents/.temp_yad_quicklink.txt) 
     [ -z "$links_text" ] && exit 0
     echo "$links_text" | tee "$HOME"/Documents/.temp_query.txt
     sed -i "s|$|\"$(echo "" |  GTK_THEME="$MY_GTK_THEME" yad --entry --width=600 --height=100 \
     --text="\nAdd query search term...\n" \
     --text-align="center" \
     --button="_Exit":1)\"|" "$HOME"/Documents/.temp_query.txt
     cat "$HOME"/Documents/.temp_query.txt | tr -d '"' | tee "$HOME"/Documents/.temp_query.txt
     sed -i 's| |+|g' "$HOME"/Documents/.temp_query.txt
     choice=$(cat "$HOME"/Documents/.temp_query.txt)
     [ -z "$choice" ] && exit 0
     "$MY_BROWSER" "$choice" 
   fi
   
}
export -f query_search

#-----------------------------------------------------#
# Open file in file manager
#-----------------------------------------------------#

open_in_file_manager() {

links_text=$(cat "$HOME"/Documents/.temp_yad_quicklink.txt)
[ -z "$links_text" ] && exit 0

   if [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     "$FILE_MANAGER" "$links_text"
     bash ~/bin/switch_to_filemanager.sh
   elif [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     "$FILE_MANAGER" "$links_text"
   else
     :
   fi

}
export -f open_in_file_manager

#-----------------------------------------------------#
# Open file or directory
#-----------------------------------------------------#

open_file_dir() {

killall yad
links_text=$(cat "$HOME"/Documents/.temp_yad_quicklink.txt) 
[ -z "$links_text" ] && exit 0

   if [[ -f "$links_text" ]]; then
     xdg-open "$links_text"
   elif [[ -d "$links_text" ]]; then
     open_in_file_manager
   else
     notify-send "File or directory" "Does not exist!"
   fi

}
export -f open_file_dir

#-----------------------------------------------------#
# Open link in web browser
#-----------------------------------------------------#

web_link() {

killall yad

   if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     links_text=$(cat "$HOME"/Documents/.temp_yad_quicklink.txt) 
     [ -z "$links_text" ] && exit 0
     echo "$links_text" | awk '{print $NF}' | xargs -r "$MY_BROWSER"
     bash ~/bin/switch_to_browser.sh
   elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     links_text=$(cat "$HOME"/Documents/.temp_yad_quicklink.txt) 
     [ -z "$links_text" ] && exit 0
     echo "$links_text" | awk '{print $NF}' | xargs -r "$MY_BROWSER"
   fi

}
export -f web_link

#-----------------------------------------------------#
# Selected item
#-----------------------------------------------------#

select_item() {

echo "$1" > "$HOME"/Documents/.temp_yad_quicklink.txt
	
}
export -f select_item

#-----------------------------------------------------#
# Main
#-----------------------------------------------------#

main() {

   if [[ "$QUICKLINKS_FILE" == "disable" ]]; then
     notify-send "Quicklinks script has been disable"
   else
     links_text=$(echo "$QUICKLINKS_FILE") 
     tac "$links_text" | sort | GTK_THEME="$MY_GTK_THEME" yad --list \
     --column="quicklinks" \
     --search-column=1 \
     --regex-search \
     --separator= \
     --title="Quicklinks" \
     --text="\nQuicklinks\n" \
     --text-align="center" \
     --borders=20 \
     --no-headers \
     --no-markup \
     --width=1000 \
     --height=700 \
     --dclick-action='/bin/bash -c "select_item %s"' \
     --button="_Add:/bin/bash -c 'add_file'" \
     --button="_File-dir:/bin/bash -c 'open_file_dir'" \
     --button="_Web:/bin/bash -c 'web_link'" \
     --button="_Query:/bin/bash -c 'query_search'" \
     --button="_Term:/bin/bash -c 'open_terminal'" \
     --button="_Copy:/bin/bash -c 'copy_to_clip'" \
     --button="_Linksrc:/bin/bash -c 'open_text_file'" \
     --button="_Exit":1
   fi

}
export -f main
main

#-----------------------------------------------------#
# Unset variables and functions 
#-----------------------------------------------------#
	
unset MY_GTK_THEME
unset main
unset select_item
unset open_file_dir
unset FILE_MANAGER
unset web_link
unset MY_BROWSER
unset query_search
unset open_terminal
unset MY_TERMINAL
unset copy_to_clip
unset open_text_file
unset TEXT_EDITOR
unset add_file
unset open_in_file_manager
