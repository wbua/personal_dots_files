#!/bin/bash

# Program command: change_accent_colors.sh
# Description: Changes accent colors for i3blocks themes.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-01-2025
# Program_license: GPL 3.0
# Dependencies: yad, grim, slurp, wl-clipboard, imagemagick

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#---------------------------------------------------#
# Error checking
#---------------------------------------------------#

set -e

#---------------------------------------------------#
# User preferences
#---------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Theme config file
THEME_CONFIG_FILE="$I3BLOCKS_THEMES_CONFIG"
export THEME_CONFIG_FILE

#---------------------------------------------------#
# Create files and directories
#---------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Documents/.accent_dir ]; then
     :
   else
     mkdir "$HOME"/Documents/.accent_dir
   fi

   if [ -d "$HOME"/Documents/.accent_dir_temp ]; then
     :
   else
     mkdir "$HOME"/Documents/.accent_dir_temp
   fi

#---------------------------------------------------#
# Color picker
#---------------------------------------------------#

color_picker() {

grim -g "$(slurp -p)" -t ppm - | convert - -format '%[pixel:p{0,0}]' txt:- | \
tr -d '\n' | awk '{print $7}' | wl-copy -n

}
export -f color_picker

#---------------------------------------------------#
# Save theme
#---------------------------------------------------#

save_theme() {

check_theme_choice=$(echo "$THEME_CONFIG_FILE" | xargs -0 basename)

   if [[ "$check_theme_choice" == "i3blocks_accent_colors" ]]; then
     killall yad
     accent_dir="$HOME/Documents/.accent_dir"
     my_bin="$HOME/bin"
     yad_filename=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
     --text="\nEnter filename for saved theme\n" \
     --text-align="center" \
     --width=400 --borders=20 --height=100 --button="_Exit":1)
     [[ -z "$yad_filename" ]] && exit
     
     cp "$my_bin"/i3blocks_accent_colors "$accent_dir"/"$yad_filename"
   else
     notify-send "Error file i3blocks_accent_colors does not exist" && exit
   fi
	
}
export -f save_theme

#---------------------------------------------------#
# Load theme
#---------------------------------------------------#

load_theme() {

check_theme_choice=$(echo "$THEME_CONFIG_FILE" | xargs -0 basename)

   if [[ "$check_theme_choice" == "i3blocks_accent_colors" ]]; then 
     killall yad
     rm -f "$HOME"/Documents/.accent_dir_temp/*.*
     source_accent_dir=$(find "$HOME"/Documents/.accent_dir/ -type f -iname '*' | xargs -n 1 basename)
     yad_accent_list=$(echo "$source_accent_dir" | yad --list --column="List" \
     --search-column=1 \
     --regex-search \
     --borders=20 \
     --text="\nLoad saved theme\n" \
     --text-align="center" \
     --separator= \
     --height=500 \
     --width=500 \
     --button="_Exit":1)
     [[ -z "$yad_accent_list" ]] && exit

     echo "$yad_accent_list" | tee ~/Documents/.temp_accent.txt

     temp_accent_file=$(tail -n 1 ~/Documents/.temp_accent.txt)
     temp_accent_dir="$HOME/Documents/.accent_dir_temp"
     source_path_dir="$HOME/Documents/.accent_dir"

     cp "$source_path_dir"/"$temp_accent_file" "$temp_accent_dir"/i3blocks_accent_colors
     chmod u+x "$HOME"/Documents/.accent_dir_temp/i3blocks_accent_colors
     rm -f ~/bin/i3blocks_accent_colors || exit 1
     mv "$temp_accent_dir"/i3blocks_accent_colors ~/bin/
     bash ~/bin/sway_load_settings.sh
   else
     notify-send "Error file i3blocks_accent_colors does not exist" && exit
   fi
	
}
export -f load_theme

#---------------------------------------------------#
# Main
#---------------------------------------------------#

main() {

choice=$(GTK_THEME="$MY_GTK_THEME" yad --form --title="Accent colors tool" \
--text="\nChange colors manually\n" \
--text-align="center" \
--center \
--align="left" \
--width=450 \
--height=550 \
--borders=20 \
--separator="|" \
--field="Mode :CB" 'None!All colors!Background!Description!Icon!Alert!Swaybar BG' \
--field="Background ":CE \
--field="Description ":CE \
--field="Icon ":CE \
--field="Alert ":CE \
--field="Swaybar BG ":CE \
--button="_Load:/bin/bash -c 'load_theme'" \
--button="_Save:/bin/bash -c 'save_theme'" \
--button="_Picker:/bin/bash -c 'color_picker'" \
--button="_Change":0 \
--button="_Exit":1)
[[ -z "$choice" ]] && exit

# Mode
field1=$(echo "$choice" | awk -F '|' '{print $1}')
# Background
field2=$(echo "$choice" | awk -F '|' '{print $2}')
# Description
field3=$(echo "$choice" | awk -F '|' '{print $3}')
# Icon
field4=$(echo "$choice" | awk -F '|' '{print $4}')
# Alert
field5=$(echo "$choice" | awk -F '|' '{print $5}')
# Swaybar background
field6=$(echo "$choice" | awk -F '|' '{print $6}')


   # None
   if [[ "$field1" == *"None"* ]]; then
     :
     
   # All colors
   elif [[ "$field1" == *"All colors"* ]]; then
   
     # Background
     sed -i "s|.*\(I3BLOCKS_BGD=\).*|I3BLOCKS_BGD=\"$(printf '%s' "$field2")\"|" "$THEME_CONFIG_FILE" || exit 1
     # Description
     sed -i "s|.*\(COLOR_DES=\).*|COLOR_DES=\"$(printf '%s' "$field3")\"|" "$THEME_CONFIG_FILE" || exit 1
     # Icon
     sed -i "s|.*\(COLOR_ICON=\).*|COLOR_ICON=\"$(printf '%s' "$field4")\"|" "$THEME_CONFIG_FILE" || exit 1
     # Alert
     sed -i "s|.*\(COLOR_ALERT=\).*|COLOR_ALERT=\"$(printf '%s' "$field5")\"|" "$THEME_CONFIG_FILE" || exit 1
     # Reload config
     bash ~/bin/sway_load_settings.sh
     # Update colors
     bash ~/bin/update_apps_with_theme.sh
    
   # Background
   elif [[ "$field1" == *"Background"* ]]; then
     :
     # Background
     sed -i "s|.*\(I3BLOCKS_BGD=\).*|I3BLOCKS_BGD=\"$(printf '%s' "$field2")\"|" "$THEME_CONFIG_FILE" || exit 1
     # Reload config
     bash ~/bin/sway_load_settings.sh
     
   # Description
   elif [[ "$field1" == *"Description"* ]]; then

     # Description
     sed -i "s|.*\(COLOR_DES=\).*|COLOR_DES=\"$(printf '%s' "$field3")\"|" "$THEME_CONFIG_FILE" || exit 1
     # Reload config
     bash ~/bin/sway_load_settings.sh
     # Update colors
     bash ~/bin/update_apps_with_theme.sh
   
   # Icon
   elif [[ "$field1" == *"Icon"* ]]; then

     # Icon
     sed -i "s|.*\(COLOR_ICON=\).*|COLOR_ICON=\"$(printf '%s' "$field4")\"|" "$THEME_CONFIG_FILE" || exit 1
     # Reload config
     bash ~/bin/sway_load_settings.sh
     # Update colors
     bash ~/bin/update_apps_with_theme.sh
   
   # Alert
   elif [[ "$field1" == *"Alert"* ]]; then

     # Alert
     sed -i "s|.*\(COLOR_ALERT=\).*|COLOR_ALERT=\"$(printf '%s' "$field5")\"|" "$THEME_CONFIG_FILE" || exit 1     
     # Reload config
     bash ~/bin/sway_load_settings.sh      
     # Update colors
     bash ~/bin/update_apps_with_theme.sh

   # Swaybar background
   elif [[ "$field1" == *"Swaybar BG"* ]]; then

   sed -i "s|.*\(SWAYBAR_BACKGROUND_COLOR=\).*|SWAYBAR_BACKGROUND_COLOR=\"$(printf '%s' "${field6}FF")\"|" "$THEME_CONFIG_FILE" || exit 1
   # Reload config
   bash ~/bin/sway_load_settings.sh 
   
   else
   :
   fi

}
export -f main
main

#---------------------------------------------------#
# Unset variables and functions
#---------------------------------------------------#

unset main
unset MY_GTK_THEME
unset color_picker
unset save_theme
unset load_theme
unset THEME_CONFIG_FILE
