#!/bin/bash

# Program command: yad_convert.sh
# Description: Plays online radio streams.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 29-12-2024
# Program_license: GPL 3.0
# Dependencies: yad, jq 

#----------------------------------------------#
# Shell attributes and positional parameters
#----------------------------------------------#

set -e

#----------------------------------------------#
# Master config file for my preferences
#----------------------------------------------#

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#----------------------------------------------#
# User preferences
#----------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#----------------------------------------------#
# Define the JSON file to store radio stations
#----------------------------------------------#

JSON_FILE="$HOME/Music/radio_stations.json"
export JSON_FILE

#----------------------------------------------#
# Ensure the JSON file exists
#----------------------------------------------#

if [ ! -f "$JSON_FILE" ]; then
    echo '[]' > "$JSON_FILE"
fi

#----------------------------------------------#
# Function to update the list of radio stations
#----------------------------------------------#

update_radio_list() {

radio_list=$(jq -r '.[] | "\"\(.name)\" \"\(.url)\""' "$JSON_FILE")
echo "$radio_list"
    
}
export -f update_radio_list

#----------------------------------------------#
# Function to add a new radio station to the JSON file
#----------------------------------------------#

add_radio_station() {

# Kill the yad dialog
killall yad

# Yad list dialog
result=$(yad --form \
 --title="Add Radio Station" \
 --field="Name:CE" \
 --field="Link:CE" \
 --button="Add:0" \
 --button="Cancel:1")

# If canceled, return early
if [ $? -eq 1 ]; then
    return
fi

# Extract the name and link from the result
name=$(echo "$result" | cut -d'|' -f1)
radio_url=$(echo "$result" | cut -d'|' -f2)

# Add the new station to the JSON file using jq
jq --arg name "$name" --arg url "$radio_url" \
    '. += [{"name": $name, "url": $url}]' "$JSON_FILE" > ~/Music/tmp.json && mv ~/Music/tmp.json "$JSON_FILE" || exit 1
  
}
    
export -f add_radio_station

#----------------------------------------------#
# Stop radio stream
#----------------------------------------------#

stop_player() {

pkill -f http

}
export -f stop_player

#----------------------------------------------#
# Declares an array
#----------------------------------------------#

declare -a radiostations="($(update_radio_list))"

#----------------------------------------------#
# Show the main list dialog with control buttons
#----------------------------------------------#

selected_station=$(printf '%s\n' "${radiostations[@]}" | GTK_THEME="$MY_GTK_THEME" yad --list \
 --title="Radio Stations" \
 --height=500 \
 --width=900 \
 --column="Name" \
 --column="Url" \
 --separator="|" \
 --button="Add station:/bin/bash -c 'add_radio_station'" \
 --button="Stop:/bin/bash -c 'stop_player'" \
 --button="Exit":1)
[[ -z "$selected_station" ]] && exit 0

#----------------------------------------------#
# Selects the url field to print
#----------------------------------------------#

url_choice=$(echo "$selected_station" | awk -F '|' '{print $2}')    
export url_choice

#----------------------------------------------#
# Main
#----------------------------------------------#

if [[ "$(pgrep -f mpv)" || "$(pidof mpv)" ]]; then
pkill -f mpv
else
mpv "$url_choice" --no-video
fi
  
#----------------------------------------------#
# Unset variables and functions
#----------------------------------------------#

unset add_radio_station
unset JSON_FILE
unset delete_radio_station
unset url_choice
unset update_radio_list
unset MY_GTK_THEME
unset stop_player
