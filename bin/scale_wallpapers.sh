#!/bin/bash

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-----------------------------------------------------#
# Gtk theme
#-----------------------------------------------------#

MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#-----------------------------------------------------#
# Yad dialog
#-----------------------------------------------------#

yad_choice=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--width=350 \
--height=50 \
--text-align="center" \
--text="\nAdd scaling type...\nExample: fill,tile,fit" \
--borders=10 \
--button="Exit":1)
[ -z "$yad_choice" ] && exit 0

#-----------------------------------------------------#
# Main
#-----------------------------------------------------#

sed -i "s|.*\(WALLPAPER_FILL_MODE=\).*|WALLPAPER_FILL_MODE=\"$(printf '%s' "$yad_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
bash ~/bin/sway_load_settings.sh
