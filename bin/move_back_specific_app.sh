#!/bin/bash

# Program command: move_back_specific_app.sh
# Description: Move focused app back to specific workspace.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 24-09-2024
# Program_license: GPL 3.0
# Dependencies: swaymsg, jq 

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-------------------------------------------------#
# Error checking
#-------------------------------------------------#

set -e

#-------------------------------------------------#
# User preferences
#-------------------------------------------------#

# Browser
MY_BROWSER="$MAIN_BROWSER"

# File manager
MY_FILEMANAGER="$MAIN_FILE_MANAGER"

# Terminal
MY_TERMINAL="$MAIN_TERMINAL"

# Development
MY_DEV="$MAIN_DEV"

# Social
MY_SOCIAL="$MAIN_SOCIAL"

# Office
MY_OFFICE="$MAIN_OFFICE"

# Video editor
MY_VIDEOEDITOR="$MAIN_VIDEO_EDITOR"

# Audio editor
MY_SOUNDEDITOR="$MAIN_SOUND_EDITOR"

# Image editor
MY_IMAGEEDITOR="$MAIN_IMAGE_EDITOR"

# Text editor
MY_TEXTEDITOR="$MAIN_TEXT_EDITOR"

#-------------------------------------------------#
# 
#-------------------------------------------------#

# Gets the class name of focused window
class_name=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | \
tr -d '"')

   # Browser
   if [[ "$class_name" == "$MY_BROWSER" || "$class_name" == "${MY_BROWSER^}" ]]; then
     send_to_workspace=$(cat "$HOME"/Documents/.set_workspace_browser.txt)
     setsid swaymsg -t get_tree | \
     jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"'| \
     grep -i "$MY_BROWSER" | \
     cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' move to workspace "$send_to_workspace" >/dev/null 2>&1 & disown
   # File manager
   elif [[ "$class_name" == "$MY_FILEMANAGER" || "$class_name" == "${MY_FILEMANAGER^}" ]]; then
     send_to_workspace=$(cat "$HOME"/Documents/.set_workspace_filemanager.txt)
     setsid swaymsg -t get_tree | \
     jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"'| \
     grep -i "$MY_FILEMANAGER" | \
     cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' move to workspace "$send_to_workspace" >/dev/null 2>&1 & disown
   # Terminal
   elif [[ "$class_name" == "$MY_TERMINAL" || "$class_name" == "${MY_TERMINAL^}" ]]; then
     send_to_workspace=$(cat "$HOME"/Documents/.set_workspace_terminal.txt)
     setsid swaymsg -t get_tree | \
     jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"'| \
     grep -i "$MY_TERMINAL" | \
     cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' move to workspace "$send_to_workspace" >/dev/null 2>&1 & disown
   # Development
   elif [[ "$class_name" == "$MY_DEV" || "$class_name" == "${MY_DEV^}" ]]; then
     send_to_workspace=$(cat "$HOME"/Documents/.set_workspace_dev.txt)
     setsid swaymsg -t get_tree | \
     jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"'| \
     grep -i "$MY_DEV" | \
     cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' move to workspace "$send_to_workspace" >/dev/null 2>&1 & disown
   # Social
   elif [[ "$class_name" == "$MY_SOCIAL" || "$class_name" == "${MY_SOCIAL^}" ]]; then
     send_to_workspace=$(cat "$HOME"/Documents/.set_workspace_social.txt)
     setsid swaymsg -t get_tree | \
     jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"'| \
     grep -i "$MY_SOCIAL" | \
     cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' move to workspace "$send_to_workspace" >/dev/null 2>&1 & disown
   # Office
   elif [[ "$class_name" == "$MY_OFFICE" || "$class_name" == "${MY_OFFICE^}" ]]; then
     send_to_workspace=$(cat "$HOME"/Documents/.set_workspace_office.txt)
     setsid swaymsg -t get_tree | \
     jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"'| \
     grep -i "$MY_OFFICE" | \
     cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' move to workspace "$send_to_workspace" >/dev/null 2>&1 & disown
   # Video editor
   elif [[ "$class_name" == "$MY_VIDEOEDITOR" || "$class_name" == "${MY_VIDEOEDITOR^}" ]]; then
     send_to_workspace=$(cat "$HOME"/Documents/.set_workspace_videoeditor.txt)
     setsid swaymsg -t get_tree | \
     jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"'| \
     grep -i "$MY_VIDEOEDITOR" | \
     cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' move to workspace "$send_to_workspace" >/dev/null 2>&1 & disown
   # Sound editor
   elif [[ "$class_name" == "$MY_SOUNDEDITOR" || "$class_name" == "${MY_SOUNDEDITOR^}" ]]; then
     send_to_workspace=$(cat "$HOME"/Documents/.set_workspace_audioeditor.txt)
     setsid swaymsg -t get_tree | \
     jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"'| \
     grep -i "$MY_SOUNDEDITOR" | \
     cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' move to workspace "$send_to_workspace" >/dev/null 2>&1 & disown
   # Image editor
   elif [[ "$class_name" == "$MY_IMAGEEDITOR" || "$class_name" == "${MY_IMAGEEDITOR^}" ]]; then
     send_to_workspace=$(cat "$HOME"/Documents/.set_workspace_imageeditor.txt)
     setsid swaymsg -t get_tree | \
     jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"'| \
     grep -i "$MY_IMAGEEDITOR" | \
     cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' move to workspace "$send_to_workspace" >/dev/null 2>&1 & disown
   # Text editor
   elif [[ "$class_name" == "$MY_TEXTEDITOR" || "$class_name" == "${MY_TEXTEDITOR^}" ]]; then
     send_to_workspace=$(cat "$HOME"/Documents/.set_workspace_texteditor.txt)
     setsid swaymsg -t get_tree | \
     jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"'| \
     grep -i "$MY_TEXTEDITOR" | \
     cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' move to workspace "$send_to_workspace" >/dev/null 2>&1 & disown
   else
     notify-send "Class names" "do not match"
   fi
