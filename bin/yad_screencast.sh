#!/bin/bash

# Program command: yad_screencast.sh
# Description: Screen recorder.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 21-09-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad, ffmpeg

#------------------------------------------------------------------#
# Choose global variables
#------------------------------------------------------------------#

# File manager
file_manager="thunar"
export file_manager

# Screen resolution
recordres="1920x1080"
export recordres

#------------------------------------------------------------------#
# Create files and directories
#------------------------------------------------------------------#

# Create videos directory

   if [ -d "$HOME"/Videos ]; then
     :
   else
     mkdir "$HOME"/Videos
   fi

# Create recordings directory

   if [ -d "$HOME"/Videos/recordings ]; then
     :
   else
     mkdir "$HOME"/Videos/recordings
   fi
   
# This is where the video temporary recordings are stored

   if [ -d "$HOME"/Videos/.yadvidrec ]; then
     :
   else
     mkdir "$HOME"/Videos/.yadvidrec
   fi
   
# Contains record status
   if [ -f "$HOME"/Documents/.yad_record_status.txt ]; then
     :
   else
     touch "$HOME"/Documents/.yad_record_status.txt
     echo "off" > "$HOME"/Documents/.yad_record_status.txt
   fi

#------------------------------------------------------------------#
# Check if record status file is empty
#------------------------------------------------------------------#

     status_file=$(cat "$HOME"/Documents/.yad_record_status.txt)
     export status_file
   if [[ -z "$status_file" ]]; then
     echo "off" | tee "$HOME"/Documents/.yad_record_status.txt
     killall yad
   fi

#------------------------------------------------------------------#
# Help page
#------------------------------------------------------------------#

_help_page() {

killall yad
	
page_info="Welcome to screen recorder

Work in progress
"
export page_info	
	
echo "$page_info" | GTK_THEME="alt-dialog9" yad --text-info --width=600 --height=600 --wrap \
--borders=10 \
--button="<span color='#ffffff' font_family='Material Icons' font='16'></span>":1

}
export -f _help_page
 
#------------------------------------------------------------------#
# Change file manager
#------------------------------------------------------------------#

_change_file_manager() {

killall yad
sed -i "17s|^.*$|file_manager=\"$(GTK_THEME="Yaru-dark" yad --entry --center --text="Change file manager")\"|" "$HOME"/bin/yad_screencast.sh

}
export -f _change_file_manager

#------------------------------------------------------------------#
# Change ffmpeg video size
#------------------------------------------------------------------#

_change_video_size() {

killall yad
sed -i "21s|^.*$|recordres=\"$(GTK_THEME="Yaru-dark" yad --entry --center --text="format: 1920x1080\nChange video size")\"|" "$HOME"/bin/yad_screencast.sh

}
export -f _change_video_size

#------------------------------------------------------------------#
# Start recording
#------------------------------------------------------------------# 

_start_recording() {

   if [[ "$(pidof ffmpeg)" ]]; then
     notify-send "ffmpeg already in use"
   else
     echo "on" | tee "$HOME"/Documents/.yad_record_status.txt
     rm -f "$HOME"/Videos/.yadvidrec/*.*
     notify-send "Recording started" && ffmpeg -video_size "$recordres" -framerate 30 -f x11grab -i :0.0+0 -f pulse -ac 2 -i default "$HOME/Videos/.yadvidrec/$(date +'%d-%m-%Y-%H%M%S').mkv" || exit
   fi

}
export -f _start_recording

#------------------------------------------------------------------#
# Kill recording
#------------------------------------------------------------------#

_kill_recording() {

   if [[ "$(pidof ffmpeg)" ]]; then
     echo "off" | tee "$HOME"/Documents/.yad_record_status.txt
     killall ffmpeg
     cp "$HOME"/Videos/.yadvidrec/*.* "$HOME"/Videos/recordings/
     notify-send "Recording stopped"
   else
     :
   fi

}
export -f _kill_recording

#------------------------------------------------------------------#
# Play recording
#------------------------------------------------------------------#

_open_recording() {
	
killall yad
find "$HOME"/Videos/.yadvidrec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open

}
export -f _open_recording

#------------------------------------------------------------------#
# Open last recording in file manager
#------------------------------------------------------------------#

_open_last_file() {

killall yad	
chosen=$(find "$HOME"/Videos/.yadvidrec/ -type f -iname '*.*' | sort | cut -d '/' -f6-)
[ -z "$chosen" ] && exit 0
"$file_manager" "$HOME"/Videos/recordings/"$chosen"

}
export -f _open_last_file

#------------------------------------------------------------------#
# Check status of recording
#------------------------------------------------------------------#

_check_status() {

   if [[ "$(cat "$HOME"/Documents/.yad_record_status.txt)" == 'on' ]]; then
     notify-send "Recording!"
   elif [[ "$(cat "$HOME"/Documents/.yad_record_status.txt)" == 'off' ]]; then
     notify-send "Not recording"
   else
     :
   fi	
	
}
export -f _check_status

#------------------------------------------------------------------#
# Close program
#------------------------------------------------------------------#

_close_yad() {
	
killall yad	
	
}
export -f _close_yad

#------------------------------------------------------------------#
# Generate random numbers
#------------------------------------------------------------------#

key=$RANDOM

#------------------------------------------------------------------#
# Yad settings
#------------------------------------------------------------------# 

_yad() {

# Top buttons	
GTK_THEME="alt-dialog14" yad --plug="$key" --tabnum=1 --form \
--borders=20 \
--columns=3 \
--field="<b><span color='#ffffff' font_family='Monospace' font='16'>REC</span></b>":fbtn '/bin/bash -c "_start_recording"' \
--field="<b><span color='#ffffff' font_family='Material Icons' font='25'></span></b>":fbtn '/bin/bash -c "_kill_recording"' \
--field="<b><span color='#ffffff' font_family='Material Icons' font='25'></span></b>":fbtn '/bin/bash -c "_open_recording"' &

# Middle buttons
GTK_THEME="alt-dialog12" yad --plug="$key" --tabnum=2 --form  \
--borders=30 \
--columns=3 \
--align="center" \
--field="<b><span color='#ffffff' font_family='Material Icons' font='30'></span></b>":fbtn '/bin/bash -c "_change_file_manager"' \
--field="<b><span color='#ffffff' font_family='Material Icons' font='30'></span></b>":fbtn '/bin/bash -c "_change_video_size"' \
--field="":LBL "" \
--field="<b><span color='#ffffff' font_family='Material Icons' font='30'></span></b>":fbtn '/bin/bash -c "_open_last_file"' \
--field="<b><span color='#ffffff' font_family='Material Icons' font='30'></span></b>":fbtn '/bin/bash -c "_check_status"' \
--field="":LBL "" \
--field="<b><span color='#ffffff' font_family='Material Icons' font='30'></span></b>":fbtn '/bin/bash -c "_help_page"' \
--field="<b><span color='#ffffff' font_family='Material Icons' font='30'></span></b>":fbtn '/bin/bash -c "_close_yad"' \
--field="":LBL "" \
--button="<span color='#ffffff' font_family='Material Icons' font='16'></span>":1 &

# Paned
GTK_THEME="alt-dialog15" yad --paned --key="$key" --width=600 --height=490 \
--borders=10 \
--splitter=90 \
--buttons-layout="center" \
--text="<span color='#ffffff' font_family='Monospace' font='16'>SCREEN</span><span color='#E73B38' font_family='Monospace' font='16'> RECORDER</span>" \
--text-align="Center" \
--title="Screen recorder" \
--button="<span color='#ffffff' font_family='Inter' font='11'>Welcome to screen recorder</span>:/bin/bash -c ''"
	
}
export -f _yad 
_yad

#------------------------------------------------------------------#
# Unset variables and functions
#------------------------------------------------------------------# 

# Functions
unset _yad
unset _start_recording
unset _kill_recording
unset _open_last_file
unset _check_status
unset _close_yad
unset _change_file_manager
unset _change_video_size
unset _help_page

# Variables
unset file_manager
unset recordres
unset key
unset page_info
unset status_file
