#!/bin/bash

# Program command: speak_radio_track.sh
# Description: Speaks the current track playing.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 28-07-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: playerctl, amixer, mpv, festival, mpv-mpris

# Stores the current volume
current_vol=$(amixer -D pulse sget Master | grep 'Left:' | awk -F'[][]' '{ print $2 }')
echo "$current_vol" > "$HOME"/Documents/.curr_vol.txt

# Main
amixer -D pulse set Master 70%
text_three=$(cat "$HOME"/Documents/.curr_vol.txt)
playerctl --player mpv volume 0.4%
text_one="your listening to"
song_title=$(playerctl --player mpv metadata -f "{{title}}")
echo "$song_title" > "$HOME"/Documents/.listento.txt
text_two=$(cat "$HOME"/Documents/.listento.txt)
echo "${text_one}" "${text_two}" | festival --tts
amixer -D pulse set Master "$text_three"
playerctl --player mpv volume 1%
