#!/bin/bash

# Program command: wallpaper_auto_colors.sh
# Description: Gets hex colors from wallpaper for theming.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 12-02-2025
# Program_license: GPL 3.0
# Dependencies: Imagemagick

# User preferences
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Config for all theme colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#-------------------------------------------------------#
# Error checking
#-------------------------------------------------------#

set -e

#-------------------------------------------------------#
# User preferences
#-------------------------------------------------------#

# Color config file
theme_colors_file="$I3BLOCKS_THEMES_CONFIG"

# Wallpaper directory
MY_WALLPAPERS="$MAIN_WALLPAPER_DIR"

# Description color
MY_COLOR_DES="$COLOR_DES"

#-------------------------------------------------------#
# Wallpaper file
#-------------------------------------------------------#

auto_wall_file=$(cat "$HOME"/Documents/.walls_selected.txt)
export auto_wall_file

#-------------------------------------------------------#
# Check description color 
#-------------------------------------------------------#

   if [[ "$MY_COLOR_DES" == "#000000" ]]; then
     notify-send "Invert colors" "win shift x" && exit 1
   fi

#-------------------------------------------------------#
# Get the colors from the wallpaper
#-------------------------------------------------------#

# Gets the lightest and darkest colors using imagemagick (method 1) 
#convert "$MY_WALLPAPERS"/"$auto_wall_file" -depth 8 +dither -colors 6 -format "%c" histogram:info: | \
#sed -n 's/^[ ]*\(.*\):.*[#]\([0-9a-fA-F]*\) .*$/\1,#\2/p' | sort -r -n -k1,1 -t "," | \
#awk -F ',' '{print $2}' | tr -d '\n' | tee ~/Documents/.wall_auto_colors.txt

# Gets the lightest and darkest colors using imagemagick (method 2) 
convert $(convert "$MY_WALLPAPERS"/"$auto_wall_file" +dither -colors 6 -depth 8 +repage -format %c histogram:info:- | sed -n 's/^.*: \(.*\) #.*$/\1/p' | \
tr -cs "0-9\n" " " | \
sed -n 's/^ *//p' | \
sed -n 's/ *$//p' | \
awk '{ i=NR; red[i]=$1; grn[i]=$2; blu[i]=$3; } END { for (i=1; i<=NR; i++) { lum[i]=int(0.29900*red[i]+0.58700*grn[i]+0.11400*blu[i]); print red[i], grn[i], blu[i], lum[i]; } } ' 2>/dev/null | \
sort -n -k 4,4 | \
awk '{ list=""; i=NR; red[i]=$1; grn[i]=$2; blu[i]=$3; } END { for (i=1; i<=NR; i++) { list=(list "\ " "xc:rgb("red[i]","grn[i]","blu[i]")"); }{ print list; } } ' 2>/dev/null) +append "miff:-" | \
convert - -depth 8 txt: | \
tail -n +2 | sed 's/^[ ]*//' | \
sed 's/[ ][ ]*/ /g' | cut -d\  -f3 | sort -d -u | tr -d '\n' | tee ~/Documents/.wall_auto_colors.txt

#-------------------------------------------------------#
# Error colors
#-------------------------------------------------------#

# Error icon color
error_iconcolor="$COLOR_ERROR_ICON"

# Error alert color
error_alertcolor="$COLOR_ERROR_ALERT"

# Error mid color
error_midcolor="$COLOR_ERROR_MID"

# Error background color
error_backgroundcolor="$COLOR_ERROR_BACKGROUND"

# Error second background color
error_second_bgd_color="$COLOR_ERROR_SECOND_BG"

#-------------------------------------------------------#
# Sends colors to color config file
#-------------------------------------------------------#

   if [[ -z "$(cat ~/Documents/.wall_auto_colors.txt)" ]]; then
     echo "#c43f5f#4285f4#8b6155#1e3059#440101#000000" | tee ~/Documents/.wall_auto_colors.txt
     notify-send "Error colors"
   else
     :
   fi

#-------------------------------------------------------#
# Sends colors to color config file
#-------------------------------------------------------#

   # Temp background color
   #if [[ "$(cat ~/Documents/.wall_auto_colors.txt | awk -F '#' '{print $2}' | sed 's|^|#|')" == '#' ]]; then
    # sed -i "s|.*\(COLOR_BG=\).*|COLOR_BG=\"$(printf '%s' "$error_backgroundcolor")\"|" "$theme_colors_file" || exit 1
   #else   
    # temp_background_color=$(cat ~/Documents/.wall_auto_colors.txt | awk -F '#' '{print $2}' | sed 's|^|#|')
     #sed -i "s|.*\(COLOR_BG=\).*|COLOR_BG=\"$(printf '%s' "$temp_background_color")\"|" "$theme_colors_file" || exit 1
   #fi

   # Temp mid color
   if [[ "$(cat ~/Documents/.wall_auto_colors.txt | awk -F '#' '{print $4}' | sed 's|^|#|')" == '#' ]]; then
     sed -i "s|.*\(COLOR_MID=\).*|COLOR_MID=\"$(printf '%s' "$error_midcolor")\"|" "$theme_colors_file" || exit 1
   else
     temp_mid_color=$(cat ~/Documents/.wall_auto_colors.txt | awk -F '#' '{print $4}' | sed 's|^|#|')
     sed -i "s|.*\(COLOR_MID=\).*|COLOR_MID=\"$(printf '%s' "$temp_mid_color")\"|" "$theme_colors_file" || exit 1
   fi

   # Temp icon color
   if [[ "$(cat ~/Documents/.wall_auto_colors.txt | awk -F '#' '{print $7}' | sed 's|^|#|')" == '#' ]]; then
     sed -i "s|.*\(COLOR_ICON=\).*|COLOR_ICON=\"$(printf '%s' "$error_iconcolor")\"|" "$theme_colors_file" || exit 1
   else
     temp_icon_color=$(cat ~/Documents/.wall_auto_colors.txt | awk -F '#' '{print $7}' | sed 's|^|#|') 
     sed -i "s|.*\(COLOR_ICON=\).*|COLOR_ICON=\"$(printf '%s' "$temp_icon_color")\"|" "$theme_colors_file" || exit 1
   fi

   # Temp alert color
   if [[ "$(cat ~/Documents/.wall_auto_colors.txt | awk -F '#' '{print $6}' | sed 's|^|#|')" == '#' ]]; then
     sed -i "s|.*\(COLOR_ALERT=\).*|COLOR_ALERT=\"$(printf '%s' "$error_alertcolor")\"|" "$theme_colors_file" || exit 1
   else
     temp_alert_color=$(cat ~/Documents/.wall_auto_colors.txt | awk -F '#' '{print $6}' | sed 's|^|#|')
     sed -i "s|.*\(COLOR_ALERT=\).*|COLOR_ALERT=\"$(printf '%s' "$temp_alert_color")\"|" "$theme_colors_file" || exit 1
   fi

   # temp second background
   #if [[ "$(cat ~/Documents/.wall_auto_colors.txt | awk -F '#' '{print $3}' | sed 's|^|#|')" == '#' ]]; then
    # sed -i "s|.*\(COLOR_SECOND_BG=\).*|COLOR_SECOND_BG=\"$(printf '%s' "$error_second_bgd_color")\"|" "$theme_colors_file" || exit 1
   #else
    # temp_background_color_second=$(cat ~/Documents/.wall_auto_colors.txt | awk -F '#' '{print $3}' | sed 's|^|#|')
     #sed -i "s|.*\(COLOR_SECOND_BG=\).*|COLOR_SECOND_BG=\"$(printf '%s' "$temp_background_color_second")\"|" "$theme_colors_file" || exit 1
   #fi

#-------------------------------------------------------#
# Main
#-------------------------------------------------------#

bash ~/bin/update_apps_with_theme.sh
