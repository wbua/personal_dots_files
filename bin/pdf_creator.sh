#!/bin/bash

# Program command: pdf_creator.sh
# Description: Creates pdf files.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 22-08-2024
# Program_license: GPL 3.0
# Dependencies: yad, libreoffice

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#----------------------------------------------------------#
# General variables
#----------------------------------------------------------#

# File manager
FILE_MANAGER="$MAIN_FILE_MANAGER"
export FILE_MANAGER

# Your printer name
MY_PRINTER="$MAIN_PRINTER"
export MY_PRINTER

# PDF files directory
PDF_DIR="$MAIN_PDF_DIR"
export PDF_DIR

#-------------------------------------------------------#
# Creates files and directories
#-------------------------------------------------------#
   
   # Create documents directory
   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi
   
   # Create temp documents directory
   if [ -d "$HOME"/Documents/.pdftemp ]; then
     :
   else
     mkdir "$HOME"/Documents/.pdftemp
   fi
   
   # Create pdf directory
   if [ "$PDF_DIR" == "disable" ]; then
     :
   elif [ -d "$PDF_DIR" ]; then
     :
   else
     notify-send "PDF directory does not exist!" && exit 1
   fi
  
#-------------------------------------------------------#
# Colors for text and buttons (pango markup)
#-------------------------------------------------------#

# Normal buttons
color1="<span color='#FFFFFF' font_family='Material Icons' font='20'>"
export color1
# Close buttons
color2="<span color='#FF002A' font_family='Material Icons' font='20'>"
export color2
# Convert to pdf
color3="<span color='#3FFF00' font_family='Material Icons' font='20'>"
export color3
# Settings buttons
color4="<span color='#FFFFFF' font_family='Monospace' font='14'>"
export color4
end="</span>"
export end

#-------------------------------------------------------#
# Print pdf file
#-------------------------------------------------------#

print_pdf() {

lp -d "$MY_PRINTER" "$HOME"/Documents/.pdftemp/*.pdf	
	
}
export -f print_pdf

#----------------------------------------------------------#
# Open pdf file in file manager
#----------------------------------------------------------#

select_file() {

killall yad	
chosen=$(find "$HOME"/Documents/.pdftemp/ -type f -iname '*.*' | sort | cut -d '/' -f6-)
[ -z "$chosen" ] && exit 0
"$FILE_MANAGER" "$PDF_DIR"/"$chosen"
	
}
export -f select_file

#----------------------------------------------------------#
# Open pdf file in pdf viewer
#----------------------------------------------------------#

pdf_open() {
	
killall yad
find "$HOME"/Documents/.pdftemp/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open	
	
}
export -f pdf_open

#----------------------------------------------------------#
# Yad main settings
#----------------------------------------------------------#

   if [[ "$PDF_DIR" == "disable" ]]; then
     notify-send "PDF script has been disabled" && exit 1 
   else
     choice=$(GTK_THEME="alt-dialog22" yad --form --title="PDF Creator" \
     --separator= --width=500 --height=200 --borders=20 \
     --field="Select":FL "" \
     --button="${color1}${end}:/bin/bash -c 'print_pdf'" \
     --button="${color1}${end}:/bin/bash -c 'select_file'" \
     --button="${color1}${end}:/bin/bash -c 'pdf_open'" \
     --button="${color3}${end}":0 \
     --button="${color2}${end}":1)
    export choice
   fi

#----------------------------------------------------------#
# Main
#----------------------------------------------------------#

main() {

   if [[ "$PDF_DIR" == "disable" ]]; then
     notify-send "PDF script has been disabled" && exit 1	
   elif [[ -z "$choice" ]]; then
     :
   else
     rm -f "$HOME"/Documents/.pdftemp/*.*
     libreoffice --headless --convert-to pdf "$choice" --outdir "$HOME"/Documents/.pdftemp/ && notify-send "PDF created"
     cp "$HOME"/Documents/.pdftemp/*.* "$PDF_DIR"    
   fi

}
export -f main
main

#----------------------------------------------------------#
# Unset variables and functions
#----------------------------------------------------------#

unset choice
unset pdf_open
unset select_file
unset chosen
unset FILE_MANAGER
unset main
unset color1
unset color2
unset color3
unset color4
unset end
unset print_pdf
unset MY_PRINTER
unset help
unset PDF_DIR
