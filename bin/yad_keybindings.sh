#!/bin/bash

# Program command: yad_keybindings.sh
# Description: Shows sway config keyboard shortcuts.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 23-08-2024
# Program_license: GPL 3.0
# Dependencies: yad, wtype
# Website:

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-------------------------------------------------
# Keysrc means the text file containing all your keyboard shortcuts.
# When describing keyboard shortcut use the word and not &.
#-------------------------------------------------
# This is only if you add them directly to keysrc.
# keysrc text file format:
# Put double quotes around description and shortcut.
# "terminal" "win return"
#-------------------------------------------------
# Buttons:
# Keysrc: open text file containing keyboard shortcuts.
# Add: Add new keyboard shortcut to keysrc. Do not add double quotes, it adds them automatically.
#-------------------------------------------------
# Other information:
# Press the enter key on selected item to excute keyboard shortcut.
# When adding keyboard shortcuts use ctrl not control.
# To search just start typing.
# While in search mode use up and down arrows for next and prev search.

#-------------------------------------------------#
# Error checking
#-------------------------------------------------#

set -e

#-------------------------------------------------#
# User preferences
#-------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Keybindings file
KEYBINDING_FILE="$MAIN_KEYBINDING_FILE"
export KEYBINDING_FILE

# Text editor
TEXT_EDITOR="$MAIN_TEXT_EDITOR"
export TEXT_EDITOR

#-------------------------------------------------#
# Colors
#-------------------------------------------------#

# Title
COLOR1=$(printf '%s' "<span color='#FFFFFF' font_family='JetBrainsMono Nerd Font' font='16' rise='0pt'>")
export COLOR1
# Color end
END=$(printf '%s' "</span>")
export END

#-------------------------------------------------#
# Create files and directories
#-------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi
 
   if [ -f "$KEYBINDING_FILE" ]; then
     :
   else
     notify-send "Keybindings file does not exist!" && exit 1
   fi

#-------------------------------------------------#
# Keysrc text file array
#-------------------------------------------------#

declare -a KEYBINDS="($(< "$KEYBINDING_FILE"))"

#-------------------------------------------------#
# Remove whitespace from keysrc
#-------------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$KEYBINDING_FILE"

#-------------------------------------------------#
# Help page
#-------------------------------------------------#

help_page() {

killall yad
info_text=$(printf '%s' "Information about yad dialogs

To search just start typing.
While in search mode, use up and down arrows for next and prev search.

First method: 
Once you have selected your shortcut, you can press escape, and then press shortcut.

Second method:
Once you have selected your shortcut, just press enter to excute shortcut.

Button method 1:
You can press the button with the mouse.

Button method 2:
Hold down alt key, and press the first letter of the button.

")

echo "$info_text" | yad --list \
--column="info" \
--borders=20 \
--width=1100 \
--height=600 \
--button="Exit":1

	
}
export -f help_page

#-------------------------------------------------#
# Open keysrc text file
#-------------------------------------------------#

keysrc() {

killall yad

   if [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     setsid "$TEXT_EDITOR" "$KEYBINDING_FILE" >/dev/null 2>&1 & disown && exit
     bash ~/bin/switch_to_texteditor.sh
   elif [[ "$(grep SWITCH_TO_TEXT_EDITOR= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     setsid "$TEXT_EDITOR" "$KEYBINDING_FILE" >/dev/null 2>&1 & disown && exit
   else
     :
   fi
	
}
export -f keysrc

#-------------------------------------------------#
# Add new keyboard shortcut to keysrc
#-------------------------------------------------#

add_shortcut() {

killall yad
add_choice=$(GTK_THEME="$MY_GTK_THEME" yad --form \
--title="Add new shortcut" \
--text="\nAdd new shortcut\n" \
--text-align="center" \
--center \
--align="left" \
--width=650 \
--height=300 \
--borders=20 \
--separator="#" \
--field="Description ":CE \
--field="Shortcut ":CE \
--button="Exit":1)
[[ -z "$add_choice" ]] && exit

field1=$(echo "$add_choice" | awk -F '#' '{print $1}')
field2=$(echo "$add_choice" | awk -F '#' '{print $2}')

echo "\"$field1\"" "\"$field2\"" >> "$KEYBINDING_FILE"

}
export -f add_shortcut

#-------------------------------------------------#
# Yad dialog
#-------------------------------------------------#

main_choice=$(printf '%s\n' "${KEYBINDS[@]}" | GTK_THEME="$MY_GTK_THEME" yad --list \
--column="Description" \
--column="Shortcuts" \
--search-column=1 \
--regex-search \
--buttons-layout="center" \
--title="Sway Keybindings" \
--text-align="center" \
--text="\n${COLOR1}Sway keyboard shortcuts${END}\n" \
--borders=20 \
--separator="#" \
--width=1000 \
--height=700 \
--button="_Keysrc:/bin/bash -c 'keysrc'" \
--button="_Add:/bin/bash -c 'add_shortcut'" \
--button="_Help:/bin/bash -c 'help_page'" \
--button="_Exit":1)

#-------------------------------------------------#
# Main
#-------------------------------------------------#

main_field1=$(echo "$main_choice" | awk -F '#' '{print $2}')

   if [[ "$main_field1" == 'print' ]]; then
     setsid -f wtype -k "$main_field1" >/dev/null 2>&1 & disown
   elif [[ "$(echo "$main_field1" | awk '{print $2}')" == *"shift"* ]]; then
     my_choice01=$(echo "$main_field1" | awk '{print $1}')
     my_choice02=$(echo "$main_field1" | awk '{print $2}')
     my_choice03=$(echo "$main_field1" | awk '{print $3}')
     setsid -f wtype -M "$my_choice01" -M "$my_choice02" -k "$my_choice03" >/dev/null 2>&1 & disown && exit
   elif [[ "$(echo "$main_field1" | awk '{print $2}')" == *"alt"* ]]; then
     my_choice01=$(echo "$main_field1" | awk '{print $1}')
     my_choice02=$(echo "$main_field1" | awk '{print $2}')
     my_choice03=$(echo "$main_field1" | awk '{print $3}')
     setsid -f wtype -M "$my_choice01" -M "$my_choice02" -k "$my_choice03" >/dev/null 2>&1 & disown && exit
   elif [[ "$(echo "$main_field1" | awk '{print $2}')" == *"ctrl"* ]]; then
     my_choice01=$(echo "$main_field1" | awk '{print $1}')
     my_choice02=$(echo "$main_field1" | awk '{print $2}')
     my_choice03=$(echo "$main_field1" | awk '{print $3}')
     setsid -f wtype -M "$my_choice01" -M "$my_choice02" -k "$my_choice03" >/dev/null 2>&1 & disown && exit
   elif [[ "$(echo "$main_field1" | awk '{print $2}')" == *"win"* ]]; then
     my_choice01=$(echo "$main_field1" | awk '{print $1}')
     my_choice02=$(echo "$main_field1" | awk '{print $2}')
     my_choice03=$(echo "$main_field1" | awk '{print $3}')
     setsid -f wtype -M "$my_choice01" -M "$my_choice02" -k "$my_choice03" >/dev/null 2>&1 & disown && exit
   elif [[ "$(echo "$main_field1" | awk '{print $2}')" == *"altgr"* ]]; then
     my_choice01=$(echo "$main_field1" | awk '{print $1}')
     my_choice02=$(echo "$main_field1" | awk '{print $2}')
     my_choice03=$(echo "$main_field1" | awk '{print $3}')
     setsid -f wtype -M "$my_choice01" -M "$my_choice02" -k "$my_choice03" >/dev/null 2>&1 & disown && exit
   else
     my_choice01=$(echo "$main_field1" | awk '{print $1}')
     my_choice02=$(echo "$main_field1" | awk '{print $2}')
     setsid -f wtype -M "$my_choice01" -k "$my_choice02" >/dev/null 2>&1 & disown && exit
   fi

#-------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------#

unset MY_GTK_THEME
unset COLOR1
unset END
unset add_shortcut
unset keysrc
unset help_page
unset KEYBINDING_FILE
unset TEXT_EDITOR
