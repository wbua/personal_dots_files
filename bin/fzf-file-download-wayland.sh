#!/bin/bash

# Program command: fzf-file-download-wayland.sh
# Description: Downloads a file from the internet.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 05-12-2023
# Program_license: GPL 3.0
# Dependencies: fzf 

#----------------------------------------#
# Colors
#----------------------------------------#

# Descriptions
color1=$(tput setaf 244)
# Color end
end=$(tput sgr0)

#----------------------------------------#
# Download file to temp directory
#----------------------------------------#

download_file() {

choice=$(echo "" | fzf --print-query \
--cycle \
--padding=0% \
--border="none" \
--header="
Press F1 for main menu

" \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'download file> ' \
--bind='esc:execute(fzf-launcher-wayland.sh {})' \
--bind='F1:execute(fzf-launcher-wayland.sh {})' \
--info=inline \
--reverse)
[ -z "$choice" ] && exit 0
rm -f "$HOME"/Downloads/.temp_downloads/downloadedfile
nohup wget "$choice" -O "$HOME"/Downloads/.temp_downloads/downloadedfile &
notify-send "File downloaded"
fzf-launcher-wayland.sh

}

#----------------------------------------#
# Open downloaded file
#----------------------------------------#

open_download() {

   if [[ -z "$(ls -A "$HOME"/Downloads/.temp_downloads)" ]]; then
     notify-send "Download directory is empty"
     fzf-launcher-wayland.sh
   else
     nohup find "$HOME"/Downloads/.temp_downloads/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 -d '\n' xdg-open &
     fzf-launcher-wayland.sh
   fi	
	
}

#----------------------------------------#
# Copy file to downloads directory
#----------------------------------------#

copy_file() {

nohup cp "$HOME"/Downloads/.temp_downloads/downloadedfile "$HOME"/Downloads/"$(date '+%d%m%Y-%H%M-%S')" &
fzf-launcher-wayland.sh
	
}

#----------------------------------------#
# Menu
#----------------------------------------#

menu() {

echo "download ${color1} downloads a file${end}"
echo "open ${color1} open downloaded file${end}"
echo "copy ${color1} copy download to downloads directory${end}"
	
}

#----------------------------------------#
# Main
#----------------------------------------#

main() {

choice=$(menu | fzf --cycle \
--ansi \
--border="none" \
--header="
Press F1 for main menu

" \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'file download> ' \
--bind='esc:execute(fzf-launcher-wayland.sh {})' \
--bind='F1:execute(fzf-launcher-wayland.sh {})' \
--info=inline \
--reverse | awk '{print $1}' )

case "$choice" in

 'download')
   download_file
  ;;

 'open')
   open_download
  ;;

 'copy')
   copy_file
  ;;

  *)
   echo "Something went wrong!" || exit 1
  ;;

esac
	
}
main

