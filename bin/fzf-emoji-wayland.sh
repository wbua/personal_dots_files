#!/bin/bash

# Program command: fzf-emoji-wayland.sh
# Description: Lets you paste emoji's to clipboard
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-12-2023
# Program_license: GPL 3.0
# Dependencies: fzf, wl-clipboard

# Create second script and call it "copy_emoji.sh".
# Inside of this script add the following below.
# Remove the # before cat in copy_emoji.sh script.
# Put this script into "~/bin/".
#################################################
#!/bin/bash

#cat "$HOME"/Documents/.selected_emoji.txt | wl-copy

#################################################

#-----------------------------------------------#
# Create files and directories
#-----------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi
   
   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi

   if [ -f "$HOME"/Documents/mainprefix/emoji-data.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/emoji-data.txt
   fi

#-----------------------------------------------#
# Colors
#-----------------------------------------------#

# Keybindings
color1=$(tput setaf 15)
export color1
# Color end
end=$(tput sgr0)
export end

#-----------------------------------------------#
# Kill script
#-----------------------------------------------#

kill_script() {

pkill -f ~/bin/fzf-emoji-wayland.sh
	
}
export -f kill_script

#-----------------------------------------------#
# fzf settings
#-----------------------------------------------#

_fzf() {

fzf --print-query \
--reverse \
--ansi \
--info=inline \
--bind='F9:execute(kill_script {})' \
--color='prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:#660020,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'Search emoji...> ' \
--header="
${color1}enter${end} copy emoji to clipboard | ${color1}F9${end} kill script

"

}
export -f _fzf

#-----------------------------------------------#
# Main
#-----------------------------------------------#

main() {

emoji_pick=$(cat "$HOME"/Documents/mainprefix/emoji-data.txt)
export emoji_pick
chosen=$(echo "$emoji_pick" | _fzf)
export chosen
[ -z "$chosen" ] && exit

printf "%s" "$chosen" | awk '{print $1}' | tr -d '\n' | tee "$HOME"/Documents/.selected_emoji.txt
setsid ~/bin/copy_emoji.sh >/dev/null 2>&1 & disown
pkill -f ~/bin/fzf-emoji-wayland.sh
   
}
export -f main
main

#-----------------------------------------------#
# Unset variables and functions
#-----------------------------------------------#

unset main
unset chosen
unset emoji_pick
unset kill_script
unset color1
unset end
unset _fzf

