#!/bin/bash

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Color config
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

set -e

# Background color
MY_COLOR_BG="$COLOR_BG"

# Icon color
MY_COLOR_ICON="$COLOR_ICON"

# Foot directory
FOOT_DIR="$HOME/.config/foot"

# Foot file
FOOT_FILE="foot.ini"

# Foot font
MY_FOOT_FONT="$FONT_FOR_FOOT"

background_hash=$(echo "$MY_COLOR_BG" | sed 's|#||')
icon_hash=$(echo "$MY_COLOR_ICON" | sed 's|#||')


   if [ -d "$FOOT_DIR" ]; then
     :
   else
     notify-send "Foot directory" "does not exist" && exit 1
   fi

   if [ -f "$FOOT_DIR"/"$FOOT_FILE" ]; then
     :
   else
     notify-send "Foot.ini file" "does not exist" && exit 1
   fi

# Create userchrome file
cat <<EOF > "$FOOT_DIR"/"$FOOT_FILE" 

# Font
font=$MY_FOOT_FONT

[colors]
alpha=1
foreground=ffffff
background=$background_hash
regular0=45475a   # Surface 1
regular1=f38ba8   # red
regular2=a6e3a1   # green
regular3=f9e2af   # yellow
regular4=$icon_hash
regular5=f5c2e7   # pink
regular6=94e2d5   # teal
regular7=bac2de   # Subtext 1
bright0=585b70    # Surface 2
bright1=f38ba8    # red
bright2=a6e3a1    # green
bright3=f9e2af    # yellow
bright4=$icon_hash
bright5=f5c2e7    # pink
bright6=94e2d5    # teal
bright7=a6adc8    # Subtext 0

[key-bindings]
clipboard-copy=Control+Shift+c XF86Copy
clipboard-paste=Control+Shift+v XF86Paste

EOF
