#!/bin/bash

choice=$(GTK_THEME="alt-dialog22" yad --entry \
--text-align="center" \
--text="\nEnter keyboard layout letters...

example: gb, us\n" \
--width=400 \
--height=150 \
--borders=30 \
--button="Exit":1)
[[ -z "$choice" ]] && exit

#echo "$choice" > "$HOME"/Documents/.sway_layout.txt

#sed -i "5s|^.*$|xkb_layout \"$(cat "$HOME"/Documents/.sway_layout.txt)\"|" ~/.config/sway/keyboard_layout || exit 1
#swaymsg reload
:
