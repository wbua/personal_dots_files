#!/bin/bash

set -e

#---------------------------------------------------#
# Sourcing files
#---------------------------------------------------#

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Color config
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#---------------------------------------------------#
# User preferences
#---------------------------------------------------#

# Background color
MY_COLOR_BG="$COLOR_BG"

# Icon color
MY_COLOR_ICON="$COLOR_ICON"

# Rofi directory
ROFI_DIR="$HOME/.config/rofi"

# Rofi file
ROFI_FILE="colors-rofi.rasi"

#---------------------------------------------------#
# Create files and directories
#---------------------------------------------------#

   if [ -d "$ROFI_DIR" ]; then
     :
   else
     mkdir "$ROFI_DIR"
   fi
   
   if [ -f "$ROFI_DIR"/"$ROFI_FILE" ]; then
     :
   else
     touch "$ROFI_DIR"/"$ROFI_FILE"
   fi

#---------------------------------------------------#
# Create the geany css file
#---------------------------------------------------#
   
cat <<EOF > "$ROFI_DIR"/"$ROFI_FILE"
 
* {
active-background: #784CA0;
active-foreground: #FAE8E1;
normal-background: #181519;
normal-foreground: #FAE8E1;
urgent-background: #CC659A;
urgent-foreground: #FAE8E1;

alternate-active-background: #914B4B;
alternate-active-foreground: #FAE8E1;
alternate-normal-background: #181519;
alternate-normal-foreground: #FAE8E1;
alternate-urgent-background: #181519;
alternate-urgent-foreground: #FAE8E1;

selected-active-background: #CC659A;
selected-active-foreground: #FAE8E1;
selected-normal-background: #CC659A;
selected-normal-foreground: #FAE8E1;
selected-urgent-background: #784CA0;
selected-urgent-foreground: #FAE8E1;

background-color: #181519;
background: $MY_COLOR_BG;
foreground: #000000; /* color icon: selected black text */
foreground-nonactive: #FFFFFF; /* non active white color */
border-color: #784CA0;
background-options: $MY_COLOR_BG;

color0: #3F3C40;
color1: #1A1022;
color2: #492E61;
color3: #6D3838;
color4: #5A3978;
color5: #994C74;
color6: #B58E80;
color7: #F0D6CC;
color8: #A8958F;
color9: #23152D;
color10: #613D81;
color11: $MY_COLOR_ICON;
color12: $MY_COLOR_ICON;
color13: #CC659A;
color14: #F2BDAA;
color15: #F0D6CC;

}

EOF
