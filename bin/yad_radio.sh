#!/bin/bash
# shellcheck disable=SC2090
# shellcheck disable=SC2089
# shellcheck disable=SC2009

# Program command: yad_radio.sh
# Description: Select and listen to radio streams. Used with polybar.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 01-07-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad, playerctl, mpv, xclip, ffmpeg, mpv-mpris, material icons

# mpv-mpris link:
# https://github.com/hoyon/mpv-mpris/releases
# Google material icons link:
# https://github.com/google/material-design-icons/blob/master/font/MaterialIcons-Regular.ttf

#--------------------------------------------------------------------#
# Kills new instance
#--------------------------------------------------------------------#

killall yad

#--------------------------------------------------------------------#
# Variables
#--------------------------------------------------------------------#

# Yad box y position
box_y_position="60"
export box_y_position
# Yad box x position
box_x_position="1150"
export box_x_position

#--------------------------------------------------------------------#
# Create music directory
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Music ]; then
     :
   else
     mkdir "$HOME"/Music
   fi

#--------------------------------------------------------------------#
# Create Documents directory
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

#--------------------------------------------------------------------#
# This is where the recorded files are stored
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Music/radio_recordings ]; then
     :
   else
     mkdir "$HOME"/Music/radio_recordings
   fi

#--------------------------------------------------------------------#
# This is where recorded streams are stored temporary
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Music/.temprecfile ]; then
     :
   else
     mkdir "$HOME"/Music/.temprecfile
   fi

#--------------------------------------------------------------------#
# Create status of mpv volume
#--------------------------------------------------------------------#

   if [ -f "$HOME"/Documents/.radio_mute.txt ]; then
     :
   else
     touch "$HOME"/Documents/.radio_mute.txt
   fi

#--------------------------------------------------------------------#
# Create video record status
#--------------------------------------------------------------------#

   if [ -f "$HOME"/Documents/.video_record2.txt ]; then
     :
   else
     touch "$HOME"/Documents/.video_record2.txt
   fi

#--------------------------------------------------------------------#
# Colors for menu items (pango markup)
#--------------------------------------------------------------------#

color1="<span color='#FFFFFF' font_family='Inter' font='12' weight='bold' rise='0pt'>"
export color1
# Off
color2="<span color='#FF002E' font_family='Material Icons' font='18' weight='bold' rise='0pt'>"
export color2
# On
color3="<span color='#22FF00' font_family='Material Icons' font='18' weight='bold' rise='0pt'>"
export color3
# mpv off
color4="<span color='#FFFFFF' font_family='Material Icons' font='18' rise='0pt'>"
export color4
end="</span>"
export end

#--------------------------------------------------------------------#
# Shuffle radio streams
#--------------------------------------------------------------------

shuffle_radio() {

text_file=$(cat "$HOME"/Documents/mainprefix/lofi_stations.txt)
export text_file
killall yad
killall mpv
killall ffplay
echo "$text_file" | shuf -n1 | tee "$HOME"/Documents/.temp_radio_shuf.txt
cat "$HOME"/Documents/.temp_radio_shuf.txt | xargs mpv

}
export -f shuffle_radio

#--------------------------------------------------------------------#
# Recording radio streams with mpv
#--------------------------------------------------------------------#

recordradiompv () {

     vidfile=$(cat "$HOME"/Documents/.video_record2.txt)
     export vidfile
   if [[ "$vidfile" == 'off' ]]; then
     killall yad
     echo "on" > "$HOME"/Documents/.video_record2.txt
     rm -f "$HOME"/Music/.temprecfile/*.*
     radiotrack=$(playerctl --player mpv metadata -f "{{xesam:url}}")
     export radiotrack
     killall mpv
     mpv --stream-record="$HOME/Music/.temprecfile/$(date +'%d-%m-%Y-%H%M%S').mp4" "$radiotrack"
     cp "$HOME"/Music/.temprecfile/*.* "$HOME"/Music/radio_recordings/
     yad_radio.sh
   elif [[ "$vidfile" == 'on' ]]; then
     killall yad
     echo "off" > "$HOME"/Documents/.video_record2.txt
     killall mpv
     yad_radio.sh
   fi

}
export -f recordradiompv

#--------------------------------------------------------------------#
# Check state of recording
#--------------------------------------------------------------------#

check_recording(){

	 recfile=$(cat "$HOME"/Documents/.video_record2.txt)
	 export recfile
   if [[ -z "$recfile" ]]; then
     echo "off" > "$HOME"/Documents/.video_record2.txt
   elif [[ "$recfile" == 'off' ]]; then
	 echo "${color3}${end}"
   elif [[ "$recfile" == 'on' ]]; then
	 echo "${color2}${end}"
   fi

}
export -f check_recording

#--------------------------------------------------------------------#
# Play recordings
#--------------------------------------------------------------------#

playrecfg () {

playectemp=$(find "$HOME"/Music/.temprecfile/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
export playectemp
killall yad
killall mpv
killall ffplay
ffplay "$playectemp" -autoexit -nodisp

}
export -f playrecfg

#--------------------------------------------------------------------#
# Send song title to clipboard
#--------------------------------------------------------------------#

songtitclip () {

playerctl --player mpv metadata -f "{{title}}" | xclip -selection clipboard
killall yad

}
export -f songtitclip

#--------------------------------------------------------------------#
# Stop mpv and ffplay
#--------------------------------------------------------------------#

stopmpvffplay () {

   if pgrep -x "mpv" > /dev/null
   then
     killall mpv
     killall yad
     echo "off" > "$HOME"/Documents/.video_record2.txt
   elif pgrep -x "ffplay" > /dev/null
   then
     killall ffplay
     killall yad
   else
     :
   fi
}
export -f stopmpvffplay

#--------------------------------------------------------------------#
# Currently playing
#--------------------------------------------------------------------#

currently_playing() {

   if [[ "$(ps aux | grep mpv | grep -o "record" | head -n1)" == 'record' ]]; then
     echo "Recording"
   elif pgrep -x "mpv" > /dev/null
   then
     echo "${color1}$(playerctl --player mpv metadata -f "{{title}}" | cut -c 1-50 | tr "&" "+")${end}"
     echo ""
     playerctl --player mpv metadata -f "{{xesam:url}}" | cut -c 1-50
   elif pgrep -x "ffplay" > /dev/null
   then
     echo "Playing recording"
   else
     echo "Not playing"
   fi

}
export -f currently_playing

#--------------------------------------------------------------------#
# Mute sound
#--------------------------------------------------------------------#

_mute() {

     vfile=$(cat "$HOME"/Documents/.radio_mute.txt)
	 export vfile
   if [[ "$vfile" == 'playerctl --player mpv volume 1%' ]]; then
     killall yad
	 echo "playerctl --player mpv volume 0%" > "$HOME"/Documents/.radio_mute.txt
	 playerctl --player mpv volume 0%
	 yad_radio.sh
   elif [[ "$vfile" == 'playerctl --player mpv volume 0%' ]]; then
	 killall yad
	 echo "playerctl --player mpv volume 1%" > "$HOME"/Documents/.radio_mute.txt
	 playerctl --player mpv volume 1%
	 yad_radio.sh
   fi

}
export -f _mute

#--------------------------------------------------------------------#
# check status of muted sound
#--------------------------------------------------------------------#

check_mute() {

	 vlfile=$(cat "$HOME"/Documents/.radio_mute.txt)
     export vlfile
   if [[ -z "$vlfile" ]]; then
	 echo "playerctl --player mpv volume 1%" > "$HOME"/Documents/.radio_mute.txt
   elif [[ "$vlfile" == 'playerctl --player mpv volume 1%' ]]; then
	 echo "${color3}${end}"
   elif [[ "$vlfile" == 'playerctl --player mpv volume 0%' ]]; then
	 echo "${color2}${end}"
   else
	 :
   fi

}
export -f check_mute

#--------------------------------------------------------------------#
# Refresh yad
#--------------------------------------------------------------------#

yad_refresh() {
	
killall yad ; sleep 1 ; yad_radio.sh
	
}
export -f yad_refresh

#--------------------------------------------------------------------#
# Yad settings
#--------------------------------------------------------------------#

GTK_THEME="alt-dialog" yad --form --text-info --image-on-top --borders=10 --buttons-layout="end" --columns=3 \
--posx="$box_x_position" --posy="$box_y_position" \
--buttons-layout="center" \
--title="Radio player" \
--text-align="center" \
--align="center" \
--text="$(currently_playing)" \
--button="<span>${color4}${end}</span>:/bin/bash -c 'shuffle_radio'" \
--button="<span>$(check_recording)</span>:/bin/bash -c 'recordradiompv'" \
--button="<span>$(check_mute)</span>:/bin/bash -c '_mute'" \
--button="<span>${color4}${end}</span>:/bin/bash -c 'songtitclip'" \
--button="<span>${color4}${end}</span>:/bin/bash -c 'playrecfg'" \
--button="<span>${color4}${end}</span>:/bin/bash -c 'stopmpvffplay'" \
--button="<span color='#ffffff' font_family='Material Icons' font='18'></span>:/bin/bash -c 'yad_refresh'" \
--button="<span color='#ffffff' font_family='Material Icons' font='18'></span>":1 &

#--------------------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------------------#

unset recordradiompv
unset playrecfg
unset playectemp
unset radiotrack
unset songtitclip
unset stopmpvffplay
unset currently_playing
unset color1
unset color2
unset color3
unset color4
unset end
unset box_y_position
unset box_x_position
unset _mute
unset check_mute
unset vfile
unset vlfile
unset vidfile
unset check_recording
unset recfile
unset yad_refresh
unset shuffle_radio
