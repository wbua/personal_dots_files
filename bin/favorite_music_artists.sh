#!/bin/bash

# Program command: favorite_music_artists.sh
# Description: Show's my favorite music artists and info about them.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 24-05-2024
# Program_license: GPL 3.0
# Dependencies: yad

#----------------------------------------------------#
# Error checking
#----------------------------------------------------#

set -e

#----------------------------------------------------#
# Create files and directories
#----------------------------------------------------#

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/artist_list ]; then
     :
   else
     mkdir "$HOME"/Pictures/artist_list
   fi

   if [ -d "$HOME"/Pictures/artist_list/sound_cloud ]; then
     :
   else
     mkdir "$HOME"/Pictures/artist_list/sound_cloud
   fi

   if [ -d "$HOME"/Pictures/artist_list/wiki ]; then
     :
   else
     mkdir "$HOME"/Pictures/artist_list/wiki
   fi

#----------------------------------------------------#
# User preference
#----------------------------------------------------#

# Gtk theme
MY_GTK_THEME="alt-dialog22"
export MY_GTK_THEME

# Text font
MY_TEXT_FONT="JetBrainsMono Nerd Font"
export MY_TEXT_FONT

# Browser
MY_BROWSER="brave"
export MY_BROWSER

#----------------------------------------------------#
# Colors
#----------------------------------------------------#

# Titles
COLOR1=$(printf '%s' "<span color='#FFFFFF' font_family='$MY_TEXT_FONT' font='18' rise='0pt'>")
export COLOR1
# Color end
END=$(printf '%s' "</span>")
export END

#----------------------------------------------------#
# Dialog list information
#----------------------------------------------------#

declare -a TABLE="($(< ~/Pictures/artist_list/main_list.txt))"

#----------------------------------------------------#
# Open link in wiki
#----------------------------------------------------#

open_browser() {

link_file=$(head ~/Documents/.artist_selected.txt)
[[ -z "$link_file" ]] && exit 0

wiki_choice=$(find ~/Pictures/artist_list/wiki/ -type f -iname "${link_file[@]}" -print0 | xargs -0 head) 

"$MY_BROWSER" "$wiki_choice"

}
export -f open_browser

#----------------------------------------------------#
# Open link in sound cloud
#----------------------------------------------------#

sound_cloud() {

sc_file=$(cat "$HOME"/Documents/.artist_selected.txt)
[[ -z "$sc_file" ]] && exit 0

sc_choice=$(find ~/Pictures/artist_list/sound_cloud/ -type f -iname "${sc_file[@]}" -print0 | xargs -0 head) 

"$MY_BROWSER" "$sc_choice"
	
}
export -f sound_cloud

#----------------------------------------------------#
# Speak artist name
#----------------------------------------------------#

speak_name() {

speak_file=$(cat "$HOME"/Documents/.artist_selected.txt | sed 's| ||' | sed 's|Singer||')
echo "$speak_file" | festival --tts  
	
}
export -f speak_name

#----------------------------------------------------#
# Main
#----------------------------------------------------#
#mogrify -format png -path ~/Pictures/test_walls/ -thumbnail 250x *.png
main() {

# Dialog	
printf '%s\n' "${TABLE[@]}" | GTK_THEME="$MY_GTK_THEME" yad --list \
--search-column=2 \
--regex-search \
--title="Favorite Artists" \
--text-align="center" \
--text="\n${COLOR1}Favorite Artists${END}\n" \
--borders=10 \
--separator=' ' \
--width=900 \
--height=650 \
--dclick-action='/bin/bash -c "echo "$1" > "$HOME"/Documents/.artist_selected.txt"' \
--column="Cover image:IMG" \
--column="Name" \
--column="details" \
--button="_Sound Cloud:/bin/bash -c 'sound_cloud'" \
--button="_Speak:/bin/bash -c 'speak_name'" \
--button="_Wiki:/bin/bash -c 'open_browser'" \
--button="_Exit":1

}
export -f main
main

#----------------------------------------------------#
# Unset variables and functions
#----------------------------------------------------#

unset choice
unset main
unset MY_GTK_THEME
unset COLOR1
unset END
unset MY_TEXT_FONT
unset TABLE
unset open_browser
unset MY_BROWSER
unset speak_name
unset sound_cloud
