#!/bin/bash
# shellcheck disable=SC2317

program_command="newscrshtoolgtk.sh"
program_version="1.0.0"
program_updated="31-05-2023"
program_license="GPL 3.0"
contributors="John Mcgrath"
program_website="none"
program_contact="none"

#--------------------------------------------------------------------#
# Checks if ffmpeg is running for screen record
#--------------------------------------------------------------------#

if [[ "$(ps aux | grep ffmpeg | grep -o "x11grab")" == *"x11grab"* ]]; then
       killall ffmpeg
       cp "$HOME"/Videos/.yadvidrec/*.mkv "$HOME"/Videos/yad_recordings/ && notify-send "Recording stopped"      
else
       :
fi

#--------------------------------------------------------------------#
# Help (type newscrshtoolgtk.sh -h in prompt)
#--------------------------------------------------------------------#

help(){
cat << EOF
...

## Tracking

    * Command:      $program_command
    * Version:      $program_version
    * Updated:      $program_updated
    * License:      $program_license
    * Website:      $program_website
    * Contact:      $program_contact
    * Contributors: $contributors

## Description

    * Take fullscreen, select, window and delayed screenshots
    * Open last created screenshot
    * Copy to clipboard
    * Open main container where screenshots are kept
    * Send screenshot filename to clipboard
    * Open screenshot in gimp
    * Delete last screenshot
    * Record screen and stop recording
    * Convert select screenshots to black border, greyscale,
      negate, blur, rotate +90 and rotate -90

## Dependencies

    * yad
    * ffmpeg
    * maim
    * xclip
    * imagemagick
    * xdotool

## Installation

   ### Always update your system first

   Example:

   $ sudo apt update

   ---

   ### You can now install the dependencies

   Example:

   $ sudo apt install yad ffmpeg maim xclip imagemagick xdotool

   ---

   To make the script executable follow the example below

   Example:

   $ chmod u+x newscrshtoolgtk.sh

   ---

   Install script into your executable path

   Example:

   /home/user/.local/bin/ or /home/user/bin/


EOF
}

while getopts ":h" option; do
  case $option in
    h) # display help
    help
    exit;;
   \?) # incorrect option
    echo "Error: invalid option"
    exit;;
  esac
done

#--------------------------------------------------------------------#
# Create pictures directory
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

#--------------------------------------------------------------------#
# Create videos directory
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Videos ]; then
     :
   else
     mkdir "$HOME"/Videos
   fi

#--------------------------------------------------------------------#
# This is where the full size screenshots are stored
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Pictures/yad_screenshots ]; then
     :
   else
     mkdir "$HOME"/Pictures/yad_screenshots
   fi

#--------------------------------------------------------------------#
# This is where the video recordings are stored
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Videos/yad_recordings ]; then
     :
   else
     mkdir "$HOME"/Videos/yad_recordings
   fi

#--------------------------------------------------------------------#
# This is where the video temporary recordings are stored
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Videos/.yadvidrec ]; then
     :
   else
     mkdir "$HOME"/Videos/.yadvidrec
   fi

#--------------------------------------------------------------------#
# This is where the temp thumbnails for previews are stored
#--------------------------------------------------------------------#

   if [ -d "$HOME"/.cvssgtk ]; then
     :
   else
     mkdir "$HOME"/.cvssgtk
   fi

#--------------------------------------------------------------------#
# This is where full size screenshots are stored temporary
#--------------------------------------------------------------------#

   if [ -d "$HOME"/Pictures/.tempfullscrns ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempfullscrns
   fi

#--------------------------------------------------------------------#
# Records screen with sound
#--------------------------------------------------------------------#

record_screen () {
	killall yad
	rm -f "$HOME"/Videos/.yadvidrec/*.mkv
    notify-send "Recording started" && ffmpeg -video_size 1920x1080 -framerate 30 -f x11grab -i :0.0+0 -f pulse -ac 2 -i default "$HOME/Videos/.yadvidrec/video_$(date +'%d-%m-%Y-%H%M%S').mkv"
}
export -f record_screen

#--------------------------------------------------------------------#
# Plays recording
#--------------------------------------------------------------------#

play_record () {
   killall yad
   find "$HOME"/Videos/.yadvidrec/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open
}
export -f play_record

#--------------------------------------------------------------------#
# Takes a fullscreen screenshot
#--------------------------------------------------------------------#

fullscreenshots () {
    killall yad
    rm -f "$HOME"/Pictures/.tempfullscrns/*.png
    rm -f "$HOME"/Pictures/.cvssgtk/*.png
    sleep 1
    maim --hidecursor | tee "$HOME/Pictures/.tempfullscrns/scn_$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
    xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"
    xclip -selection clipboard -t image/png -o | convert png:- -resize 384x384 png:- | tee "$HOME/.cvssgtk/$(date +'%d-%m-%Y-%H%M%S')".png
    cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/yad_screenshots/
    newscrshtoolgtk.sh
}
export -f fullscreenshots

#--------------------------------------------------------------------#
# Takes a select screenshot
#--------------------------------------------------------------------#

selectscreenshot () {
    killall yad
    rm -f "$HOME"/Pictures/.tempfullscrns/*.png
    rm -f "$HOME"/Pictures/.cvssgtk/*.png
    sleep 1
    maim --select --hidecursor | tee "$HOME/Pictures/.tempfullscrns/scn_$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
    xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"
    xclip -selection clipboard -t image/png -o | convert png:- -resize 384x256 png:- | tee "$HOME/.cvssgtk/$(date +'%d-%m-%Y-%H%M%S')".png
    cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/yad_screenshots/
    newscrshtoolgtk.sh
}
export -f selectscreenshot

#--------------------------------------------------------------------#
# Takes a window screenshot
#--------------------------------------------------------------------#

windowscreenshot () {
    killall yad
    rm -f "$HOME"/Pictures/.tempfullscrns/*.png
    rm -f "$HOME"/Pictures/.cvssgtk/*.png
    sleep 5
    maim --hidecursor --window="$(xdotool getactivewindow)" | tee "$HOME/Pictures/.tempfullscrns/scn_$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
    xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"
    xclip -selection clipboard -t image/png -o | convert png:- -resize 384x256 png:- | tee "$HOME/.cvssgtk/$(date +'%d-%m-%Y-%H%M%S')".png
    cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/yad_screenshots/
    newscrshtoolgtk.sh
}
export -f windowscreenshot

#--------------------------------------------------------------------#
# Takes a delayed 5 second screenshot
#--------------------------------------------------------------------#

delayscreenshot () {
    killall yad
    rm -f "$HOME"/Pictures/.tempfullscrns/*.png
    rm -f "$HOME"/Pictures/.cvssgtk/*.png
    sleep 1
    maim --hidecursor --delay=5 | tee "$HOME/Pictures/.tempfullscrns/scn_$(date +'%d-%m-%Y-%H%M%S')".png | xclip -selection clipboard -t image/png
    xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"
    xclip -selection clipboard -t image/png -o | convert png:- -resize 384x256 png:- | tee "$HOME/.cvssgtk/$(date +'%d-%m-%Y-%H%M%S')".png
    cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/yad_screenshots/
    newscrshtoolgtk.sh
}
export -f delayscreenshot

#--------------------------------------------------------------------#
# Opens last created screenshot
#--------------------------------------------------------------------#

openscreenst () {

killall yad
find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open

}
export -f openscreenst

#--------------------------------------------------------------------#
# Variables for drop downs
#--------------------------------------------------------------------#

# Container
item1="Container"
export item1
# Filename
item2="Filename"
export item2
# None
item3="None"
export item3
# File
item4="File"
export item4
# Clipboard
item5="Clipboard"
export item5
# Black border
item6="Black border"
export item6
# Greyscale
item7="Greyscale"
export item7
# Negate
item8="Negate"
export item8
# Blur
item9="Blur"
export item9
# Rotate +90
item10="Rotateplus90"
export item10
# Rotate -90
item11="Rotateminus90"
export item11
# Gimp
item12="Gimp"
export item12
# Delete
item13="Delete"
export item13

#--------------------------------------------------------------------#
# Yad settings
#--------------------------------------------------------------------#

choice=$(GTK_THEME="alt-dialog" yad --title='Screenshot tool' --borders=20 --center --image-on-top --columns=2 --width=300 --height=400 \
--image="$(find "$HOME"/.cvssgtk/ -type f -print0 | xargs -0 ls -tr | tail -n 1)" \
--text-align="center" \
--field=' :LBL' '' \
--form \
--field='Convert:CB' 'None!Black border!Greyscale!Negate!Blur!Rotateplus90!Rotateminus90' --separator= \
--field='Command :CB' 'None!File!Clipboard!Container!Filename!Gimp!Delete' --separator= \
--field=' :LBL' '' \
--text="<span color='#ffffff' font_family='Inter' font='11'>$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | \
tail -n 1 | cut -d '/' -f6-)</span>" \
--field=' :LBL' '' \
--button="Fullscreen:/bin/bash -c 'fullscreenshots'" \
--button="Select:/bin/bash -c 'selectscreenshot'" \
--button="Window:/bin/bash -c 'windowscreenshot'" \
--button="Delay 5 sec:/bin/bash -c 'delayscreenshot'" \
--button="File:/bin/bash -c 'openscreenst'" \
--field="<b>Record</b>":fbtn '/bin/bash -c "record_screen"' \
--field="<b>Play</b>":fbtn '/bin/bash -c "play_record"' \
--button="Ok":0 \
--button="Exit":1)
export choice

#--------------------------------------------------------------------#
# Convert and command drop downs
#--------------------------------------------------------------------#

# Open container
if [[ "$choice" == *"$item1"* ]]; then
     xdg-open "$HOME"/Pictures/yad_screenshots/
# Copy filename to clipboard
elif [[ "$choice" == *"$item2"* ]]; then
     killall yad
     find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xclip -selection clipboard
# Opens last created screenshot
elif [[ "$choice" == *"$item4"* ]]; then
    find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 xdg-open
# Copy screenshot to clipboard
elif [[ "$choice" == *"$item5"* ]]; then
    xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"
# Converts select screenshot to Black border
elif [[ "$choice" == *"$item6"* ]]; then
    killall yad
    sleep 1
    rm -f "$HOME"/Pictures/.tempfullscrns/*.png
    rm -f "$HOME"/Pictures/.cvssgtk/*.png
    maim --select --hidecursor | convert png:- -bordercolor black -border 20 png:- | tee "$HOME/Pictures/.tempfullscrns/im_$(date +'%d-%m-%Y-%H%M%S')".png | \
    xclip -selection clipboard -t image/png
    xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"
    xclip -selection clipboard -t image/png -o | convert png:- -resize 384x256 png:- | tee "$HOME/.cvssgtk/$(date +'%d-%m-%Y-%H%M%S')".png
    cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/yad_screenshots/
    newscrshtoolgtk.sh
# Converts select screenshot to greyscale
elif [[ "$choice" == *"$item7"* ]]; then
    killall yad
    sleep 1
    rm -f "$HOME"/Pictures/.tempfullscrns/*.png
    rm -f "$HOME"/Pictures/.cvssgtk/*.png
    maim --select --hidecursor | convert png:- -colorspace Gray png:- | tee "$HOME/Pictures/.tempfullscrns/im_$(date +'%d-%m-%Y-%H%M%S')".png | \
    xclip -selection clipboard -t image/png
    xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"
    xclip -selection clipboard -t image/png -o | convert png:- -resize 384x256 png:- | tee "$HOME/.cvssgtk/$(date +'%d-%m-%Y-%H%M%S')".png
    cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/yad_screenshots/
    newscrshtoolgtk.sh
# Converts select screenshot to negate
elif [[ "$choice" == *"$item8"* ]]; then
    killall yad
    sleep 1
    rm -f "$HOME"/Pictures/.tempfullscrns/*.png
    rm -f "$HOME"/Pictures/.cvssgtk/*.png
    maim --select --hidecursor | convert png:- -channel RGB -negate png:- | tee "$HOME/Pictures/.tempfullscrns/im_$(date +'%d-%m-%Y-%H%M%S')".png | \
    xclip -selection clipboard -t image/png
    xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"
    xclip -selection clipboard -t image/png -o | convert png:- -resize 384x256 png:- | tee "$HOME/.cvssgtk/$(date +'%d-%m-%Y-%H%M%S')".png
    cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/yad_screenshots/
    newscrshtoolgtk.sh
# Converts select screenshot to blur
elif [[ "$choice" == *"$item9"* ]]; then
    killall yad
    sleep 1
    rm -f "$HOME"/Pictures/.tempfullscrns/*.png
    rm -f "$HOME"/Pictures/.cvssgtk/*.png
    maim --select --hidecursor | convert png:- -blur 0x10 png:- | tee "$HOME/Pictures/.tempfullscrns/im_$(date +'%d-%m-%Y-%H%M%S')".png | \
    xclip -selection clipboard -t image/png
    xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"
    xclip -selection clipboard -t image/png -o | convert png:- -resize 384x256 png:- | tee "$HOME/.cvssgtk/$(date +'%d-%m-%Y-%H%M%S')".png
    cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/yad_screenshots/
    newscrshtoolgtk.sh
# Convert select screenshot to rotate +90
elif [[ "$choice" == *"$item10"* ]]; then
    killall yad
    sleep 1
    rm -f "$HOME"/Pictures/.tempfullscrns/*.png
    rm -f "$HOME"/Pictures/.cvssgtk/*.png
    maim --select --hidecursor | convert png:- -rotate 90 png:- | tee "$HOME/Pictures/.tempfullscrns/im_$(date +'%d-%m-%Y-%H%M%S')".png | \
    xclip -selection clipboard -t image/png
    xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"
    xclip -selection clipboard -t image/png -o | convert png:- -resize 384x256 png:- | tee "$HOME/.cvssgtk/$(date +'%d-%m-%Y-%H%M%S')".png
    cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/yad_screenshots/
    newscrshtoolgtk.sh
# Convert select screenshot to rotate -90
elif [[ "$choice" == *"$item11"* ]]; then
    killall yad
    sleep 1
    rm -f "$HOME"/Pictures/.tempfullscrns/*.png
    rm -f "$HOME"/Pictures/.cvssgtk/*.png
    maim --select --hidecursor | convert png:- -rotate -90 png:- | tee "$HOME/Pictures/.tempfullscrns/im_$(date +'%d-%m-%Y-%H%M%S')".png | \
    xclip -selection clipboard -t image/png
    xclip -selection clipboard -t image/png -i < "$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1)"
    xclip -selection clipboard -t image/png -o | convert png:- -resize 384x256 png:- | tee "$HOME/.cvssgtk/$(date +'%d-%m-%Y-%H%M%S')".png
    cp "$HOME"/Pictures/.tempfullscrns/*.png "$HOME"/Pictures/yad_screenshots/
    newscrshtoolgtk.sh
# Open last screenshot in Gimp
elif [[ "$choice" == *"$item12"* ]]; then
    find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs gimp
# Delete last created screenshot
elif [[ "$choice" == *"$item13"* ]]; then
    killall yad
    myfilename=$(find "$HOME"/Pictures/.tempfullscrns/ -type f -print0 | xargs -0 ls -tr | tail -n 1 | cut -d '/' -f6-)
    export myfilename
    rm -f "$HOME"/Pictures/yad_screenshots/"$myfilename"
    rm -f "$HOME"/Pictures/.tempfullscrns/*.*
    rm -f "$HOME"/.cvssgtk/*.*
# When none is selected
elif [[ "$choice" == *"$item3"* ]]; then
      echo "None"
else
   echo "$choice"
fi

#--------------------------------------------------------------------#
# Unsets variables and functions
#--------------------------------------------------------------------#

unset choice
unset item1
unset item2
unset item3
unset item4
unset item5
unset item6
unset item7
unset item8
unset item9
unset item10
unset item11
unset item12
unset item13
unset imaconvo
unset yadcon
unset fullscreenshots
unset openscreenst
unset selectscreenshot
unset windowscreenshot
unset delayscreenshot
unset myfilename
unset record_screen
unset recvidir
unset play_record
