#!/bin/bash

# Program command: yad_previous_online_search.sh 
# Description: Previous online searches
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 02-03-2025
# Program_license: GPL 3.0
# Dependencies: yad

#----------------------------------------------------#
# Sourcing files
#----------------------------------------------------#

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
source "$I3BLOCKS_THEMES_CONFIG"

#----------------------------------------------------#
# User preferences
#----------------------------------------------------#

# Previous online serach
MY_PREVIOUS="$PREVIOUS_ONLINE_SEARCH"
export MY_PREVIOUS

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Browser
MY_BROWSER="$MAIN_BROWSER"
export MY_BROWSER

# Color icon
MY_COLOR="$COLOR_ICON"
export MY_COLOR

# Highlight color
highlight="<span color='$COLOR_ICON' font_family='Inter' weight='bold' font='14' rise='0'>"
export highlight

# Color end
color_end="</span>"
export color_end

#----------------------------------------------------#
# Yad dialog 
#----------------------------------------------------#

previous_choice=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
 --text-align="center" \
 --text="\n${highlight}gi${color_end} google images
 ${highlight}yt${color_end} youtube search 
 ${highlight}go${color_end} google search
 ${highlight}du${color_end} duckduckgo search 
 ${highlight}bi${color_end} bing search
 ${highlight}ya${color_end} yahoo search

 Enter code...
 " \
 --width=300 \
 --height=100 \
 --borders=10 \
 --separator= \
 --button="Ok":0 \
 --button="Exit":1)
export previous_choice 
[ -z "$previous_choice" ] && exit 0

#----------------------------------------------------#
# Open choice in browser
#----------------------------------------------------#

open_search_in_browser() {

   if [[ "$previous_choice" == *"gi"* ]]; then
     web_search="https://www.google.com/search?hl=en&tbm=isch&q={}"
     echo "$MY_PREVIOUS" | xargs -I{} "$MY_BROWSER" "$web_search" > /dev/null
   elif [[ "$previous_choice" == *"yt"* ]]; then
     web_search="https://www.youtube.com/results?search_query={}"
     echo "$MY_PREVIOUS" | xargs -I{} "$MY_BROWSER" "$web_search" > /dev/null
   elif [[ "$previous_choice" == *"go"* ]]; then
     web_search="https://www.google.co.uk/search?q={}"
     echo "$MY_PREVIOUS" | xargs -I{} "$MY_BROWSER" "$web_search" > /dev/null
   elif [[ "$previous_choice" == *"du"* ]]; then
     web_search="https://duckduckgo.com/?q={}"
     echo "$MY_PREVIOUS" | xargs -I{} "$MY_BROWSER" "$web_search" > /dev/null
   elif [[ "$previous_choice" == *"bi"* ]]; then
     web_search="https://www.bing.com/search?q={}"
     echo "$MY_PREVIOUS" | xargs -I{} "$MY_BROWSER" "$web_search" > /dev/null
   elif [[ "$previous_choice" == *"ya"* ]]; then
     web_search="https://search.yahoo.com/search?q={}"
     echo "$MY_PREVIOUS" | xargs -I{} "$MY_BROWSER" "$web_search" > /dev/null
   else
     :
   fi

}
export -f open_search_in_browser

#----------------------------------------------------#
# Main
#----------------------------------------------------#

    if [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
      open_search_in_browser
      bash ~/bin/switch_to_browser.sh
    elif [[ "$(grep SWITCH_TO_BROWSER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
      open_search_in_browser
    else
      :
    fi

#----------------------------------------------------#
# Unset variables and functions
#----------------------------------------------------#

unset MY_PREVIOUS
unset previous_choice
unset MY_GTK_THEME
unset MY_BROWSER
unset highlight
unset MY_COLOR
unset color_end
unset open_search_in_browser
