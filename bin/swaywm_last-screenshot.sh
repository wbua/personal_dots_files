#!/bin/bash

# Program command: swaywm_last-screenshot.sh 
# Description: Open the last screenshot you have taken.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 11-01-2025
# Program_license: GPL 3.0
# Dependencies: yad

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#----------------------------------------------------------#
# Error checking
#----------------------------------------------------------#

set -e

#----------------------------------------------------------#
# User Preferences
#----------------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# File manager
FILE_MANAGER="$MAIN_FILE_MANAGER"
export FILE_MANAGER

# Image viewer
MY_IMAGE_VIEWER="$MAIN_IMAGE_VIEWER"
export MY_IMAGE_VIEWER

# Screenshot directory
SOURCE_DIR="$MAIN_SCREENSHOT_DIR"
export SOURCE_DIR

# Temporary screenshot directory
TEMP_DIR="$HOME/Pictures/.tempfullscrns"
export TEMP_DIR

#----------------------------------------------------------#
# Create directories
#----------------------------------------------------------#

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/.tempfullscrns ]; then
     :
   else
     mkdir "$HOME"/Pictures/.tempfullscrns
   fi

   if [ -d "$HOME"/Pictures/.lastscr_thumbnails ]; then
     :
   else
     mkdir "$HOME"/Pictures/.lastscr_thumbnails
   fi

#----------------------------------------------------------#
# Creates thumbnail
#----------------------------------------------------------#

# Get the latest file in TEMP_DIR
thumb_choice=$(find "$TEMP_DIR" -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 basename | xargs)

# Get the latest file in SOURCE_DIR
main_thumb_choice=$(find "$SOURCE_DIR" -type f -print0 | xargs -0 ls -tr | tail -n 1 | xargs -0 basename | xargs)

# Check if the latest files in TEMP_DIR and SOURCE_DIR match
if [[ "$thumb_choice" != "$main_thumb_choice" ]]; then
    # Files don't match, create a new thumbnail from SOURCE_DIR to TEMP_DIR
    rm -f ~/Pictures/.tempfullscrns/*.* || exit 1
    convert "$SOURCE_DIR"/"$main_thumb_choice" -resize 512x512 "$TEMP_DIR"/"$main_thumb_choice" || exit 1
    thumb_choice=$main_thumb_choice
fi

# Clear previous thumbnails and generate a new one
rm -f ~/Pictures/.lastscr_thumbnails/*.* || exit 1
convert "$TEMP_DIR"/"$thumb_choice" -resize 512x512 ~/Pictures/.lastscr_thumbnails/"$thumb_choice"

#----------------------------------------------------------#
# Filename and directory path
#----------------------------------------------------------#

# Temp screenshot directory filename only, no directory path.
temp_file=$(find "$TEMP_DIR" -type f \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -printf '%T@ %p\n' | \
sort -n | tail -1 | cut -f2- -d" " | xargs -0 basename)
export temp_file

# Main screenshot directory path only, no filename.
main_file=$(find "$SOURCE_DIR" -type f \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -printf '%T@ %p\n' | \
sort -n | tail -1 | cut -f2- -d" " | xargs -0 dirname)
export main_file

#----------------------------------------------------------#
# Open last screenshot in image viewer
#----------------------------------------------------------#

open_file_in_image_viewer() {

killall yad
"$MY_IMAGE_VIEWER" "$main_file"/"$temp_file"
	
}
export -f open_file_in_image_viewer

#----------------------------------------------------------#
# Open last screenshot in file manager
#----------------------------------------------------------#

open_file_in_filemanager() {

   killall yad	
   if [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
    "$FILE_MANAGER" "$main_file"/"$temp_file"
     bash ~/bin/switch_to_filemanager.sh
   elif [[ "$(grep SWITCH_TO_FILE_MANAGER= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     "$FILE_MANAGER" "$main_file"/"$temp_file"
   fi

}
export -f open_file_in_filemanager

#----------------------------------------------------------#
# Copy screenshot png to clipboard
#----------------------------------------------------------#

copy_png() {

killall yad
wl-copy --type image/png < "$(find "$TEMP_DIR" -type f -print0 | xargs -0 ls -tr | tail -n 1)"

}
export -f copy_png

#----------------------------------------------------------#
# Check if temp screenshot directory is empty
#----------------------------------------------------------#

   if [ -z "$(ls -A ~/Pictures/.tempfullscrns)" ]; then
     notify-send "Directory is empty"
   else
     :
   fi
   
#----------------------------------------------------------#
# Yad dialog
#----------------------------------------------------------#

     GTK_THEME="$ALL_YAD_GTK" yad --form \
     --title="Last taken screenshot" \
     --image-on-top \
     --image="$(find "$HOME"/Pictures/.lastscr_thumbnails/ -type f -print0 | xargs -0 ls -tr | tail -n 1)" \
     --width=500 \
     --height=150 \
     --borders=10 \
     --text-align="center" \
     --title="Last screenshot" \
     --align="center" \
     --field="\nFile: $thumb_choice\n":LBL "" \
     --buttons-layout="center" \
     --button="_Open file:/bin/bash -c 'open_file_in_image_viewer'" \
     --button="_Copy:/bin/bash -c 'copy_png'" \
     --button="_Go to file:/bin/bash -c 'open_file_in_filemanager'" \
     --button="Exit":1
   
#----------------------------------------------------------#
# Unset variables and functions
#----------------------------------------------------------#

unset MY_GTK_THEME
unset FILE_MANAGER
unset open_file_in_filemanager
unset open_file_in_image_viewer
unset MY_IMAGE_VIEWER
unset main_file
unset temp_file
unset copy_png
unset thumb_choice
unset SOURCE_DIR
unset TEMP_DIR
