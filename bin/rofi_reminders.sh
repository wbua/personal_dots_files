#!/bin/bash

color1="<span color='#04A5E5' font_family='Cascadia Mono' font='16' weight='bold' rise='0pt'>"
end="</span>"

choice=$(cat "$HOME"/Documents/reminders/* | sed 's|title#|title/|g' | \
sed 's|notes#|notes/|g' | \
sed 's|date#|date/|g' | \
sed 's|priority#|priority/|g' | \
sed 's|:|  |g')

menu() {
	
echo "$choice"	
	
}


_rofi() {
	
launch=$(menu | rofi -dmenu -i -p '' -markup-rows)	
echo "$launch"


	
}
_rofi


