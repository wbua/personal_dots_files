#!/bin/bash

# Program command: yad_suspend.sh
# Description: Automatically turns monitor screen on or off.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 08-03-2025
# Program_license: GPL 3.0
# Dependencies: yad, Material Icons

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Create new script use example 1.0
# Call this new script sway_suspend.sh
#------------------------------------------#
# example 1.0:

#while true
#do
 # sleep "$(cat ~/Documents/.suspend_time.txt)"
 # notify-send "Monitor will turn off!" "Press win+shift+m to turn back on!"
 # sleep 10
 # swaymsg "output * dpms off"
#done

#------------------------------------------#

#------------------------------------------#
# User preferences
#------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Suspend amount
MY_SUS_TIME="$SUSPEND_TIME_AMOUNT"
export MY_SUS_TIME

#------------------------------------------#
# Colors
#------------------------------------------#

# Main title of script
color01="<span color='#FFFFFF' font_family='JetBrainsMono Nerd Font' font='16' rise='0pt'>"
# Current time
color02="<span color='#21FF00' font_family='JetBrainsMono Nerd Font' font='16' rise='0pt'>"
# Keybindings
color03="<span color='#21FF00' font_family='JetBrainsMono Nerd Font' font='14' rise='0pt'>"
# Check monitor status
color04="<span color='#FF0031' font_family='JetBrainsMono Nerd Font' font='14' rise='0pt'>"
# Color off
end="</span>"

#------------------------------------------#
# Turn on auto screen on
#------------------------------------------#

sway_suspend_on() {

killall yad && bash ~/bin/sway_suspend.sh
	
}
export -f sway_suspend_on

#------------------------------------------#
# Turn off auto screen off
#------------------------------------------#

sway_suspend_off() {

pkill -f ~/bin/sway_suspend.sh && killall yad
	
}
export -f sway_suspend_off

#------------------------------------------#
# Check if auto monitor is running
#------------------------------------------#

check_status() {

   if [[ "$(pgrep -f sway_suspend.sh)" ]]; then
    echo "ON"
   elif [[ ! "$(pgrep -f sway_suspend.sh)" ]]; then
    echo "${color04}OFF${end}"
   else
    :
   fi

}
export -f check_status

#------------------------------------------#
# Change time
#------------------------------------------#

change_time() {

killall yad
time_choice=$(GTK_THEME="$MY_GTK_THEME" yad --form \
 --width=500 --height=350 \
 --borders=60 \
 --separator= \
 --text="Enter wait time before monitor turns off
 " \
 --text-align="center" \
 --field="Time ":CE \
 --button="Ok":0 \
 --button="<span color='#FF0031' font_family='Material Icons' font='20'></span>":1)
[[ -z "$time_choice" ]] && exit

sed -i "s|.*\(SUSPEND_TIME_AMOUNT=\).*|SUSPEND_TIME_AMOUNT=\"$(printf '%s' "$time_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
	
}
export -f change_time

#------------------------------------------#
# Currently set time
#------------------------------------------#

current_time() {

echo "$MY_SUS_TIME"

}
export -f current_time

#------------------------------------------#
# Yad settings
#------------------------------------------#

GTK_THEME="$MY_GTK_THEME" yad --form \
 --width=500 \
 --height=350 \
 --borders=20 \
 --text="${color01}Automatically turn off monitor${end}

current time: ${color02}$(current_time)${end}
Auto monitor status: ${color02}$(check_status)${end}

Press ${color03}win+m${end} to turn off monitor
Press ${color03}win+shift+m${end} to turn on monitor

" \
 --text-align="center" \
 --field="<b>Change time</b>":fbtn '/bin/bash -c "change_time"' \
 --field="<b>Auto monitor off</b>":fbtn '/bin/bash -c "sway_suspend_off"' \
 --field="<b>Auto monitor on</b>":fbtn '/bin/bash -c "sway_suspend_on"' \
 --button="<span color='#FF0031' font_family='Material Icons' font='20'></span>":1

#------------------------------------------#
# Unset variables and functions
#------------------------------------------#

unset sway_suspend_off
unset sway_suspend_on
unset change_time
unset current_time
unset color01
unset end
unset check_status
unset MY_GTK_THEME
unset MY_SUS_TIME
