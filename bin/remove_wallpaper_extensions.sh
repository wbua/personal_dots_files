#!/bin/bash

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

set -e

MY_WALLPAPER="$MAIN_SWAY_WALLPAPER"

   if [[ "$MY_WALLPAPER" = *".png.png"* ]]; then
     change_ext=$(echo "$MY_WALLPAPER" | sed 's|.png||')
     sed -i "s|.*\(MAIN_SWAY_WALLPAPER=\).*|MAIN_SWAY_WALLPAPER=\"$(printf '%s' "$change_ext")\"|" ~/bin/sway_user_preferences.sh || exit 1
   else
     :
   fi

