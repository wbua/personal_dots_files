#!/bin/bash

# Program command: xxxscript.sh
# Description: Plays videos.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 03-12-2024
# Program_license: GPL 3.0
# Dependencies: yad, imagemagick, ffmpeg

# Extracting a single frame
# ffmpeg -i example_video.mp4 -ss 00:3:00 -vframes 1 ~/Videos/frame_out.jpg

# Will take 1 frame of video every minute of the video.
# ffmpeg -i NOWATERMARK_500-3.mp4 -vf fps=1/60 img%03d.jpg

# Will create a montage from many images.
# montage *.jpg -tile 3x3 result.jpg

# Generate many images into one image.
# This may not work depending on different versions of mpv.
# To generate the thumbnails use smplayer thumbnail generator

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#--------------------------------------------------------------#
# Error checking
#--------------------------------------------------------------#

set -e

#--------------------------------------------------------------#
# User preferences
#--------------------------------------------------------------#

# Video player
MY_VIDEO_PLAYER="smplayer"
export MY_VIDEO_PLAYER

# Thumbnail size
THUMBNAIL_SIZE="600x"
export THUMBNAIL_SIZE

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

#--------------------------------------------------------------#
# Create file and directories
#--------------------------------------------------------------#

   if [ -d "$HOME"/Videos ]; then
     :
   else
     mkdir "$HOME"/Videos
   fi

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -d "$HOME"/Videos/.xxxmovies ]; then
     :
   else
     mkdir "$HOME"/Videos/.xxxmovies
   fi

   if [ -f "$HOME"/Documents/.xxxmovies.txt ]; then
     :
   else
     touch "$HOME"/Documents/.xxxmovies.txt
   fi

#--------------------------------------------------------------#
# Last video played
#--------------------------------------------------------------#

last_video_played() {

killall yad
last_video_choice=$(cat ~/Documents/.xxx_previous_video.txt)
"$MY_VIDEO_PLAYER" "$last_video_choice"
	
}
export -f last_video_played

#--------------------------------------------------------------#
# Resize thumbnail images
#--------------------------------------------------------------#

check_size=$(find ~/Videos/.xxxmovies/ -type f -iname "*.jpg" -print0 | xargs -0 identify -size "$THUMBNAIL_SIZE" | \
tail -n 1 | xargs -0 basename | awk -F "JPEG" '{print $2}' | sed 's| ||' | awk '{print $1}' | tr -d '\n')

   if [[ "$check_size" == *"$THUMBNAIL_SIZE"* ]]; then
     :
   else
     find "$HOME"/Videos/.xxxmovies/ -type f -iname '*.jpg' -print0 | \
     xargs -0 mogrify -format jpg -thumbnail "$THUMBNAIL_SIZE" $1
   fi

#--------------------------------------------------------------#
# Creates the array text file
#--------------------------------------------------------------#

# Full path to image
find ~/Videos/.xxxmovies/ -type f -iname "*.jpg" | \
awk '{ print "\""$0"\""}' | tee ~/Documents/.xxx_image_paths.txt

# Only filenames
find ~/Videos/.xxxmovies/ -type f -iname "*.jpg" -print0 | \
xargs -0 -n 1 basename | awk '{ print "\""$0"\""}' | sed 's|.jpg||' | tee ~/Documents/.xxx_filename.txt

# Combine full path to image and filenames to text file
paste ~/Documents/.xxx_image_paths.txt ~/Documents/.xxx_filename.txt > ~/Documents/.xxxmovies.txt

#--------------------------------------------------------------#
# Declares the array text file
#--------------------------------------------------------------#

declare -a MOVIE_FILES="($(< ~/Documents/.xxxmovies.txt))"

#--------------------------------------------------------------#
# Yad dialog
#--------------------------------------------------------------#

movie_choice=$(printf '%s\n' "${MOVIE_FILES[@]}" | GTK_THEME="$MY_GTK_THEME" yad --list \
--search-column=2 \
--regex-search \
--buttons-layout="center" \
--title="Wallpapers" \
--text-align="center" \
--text="\nMovies\n" \
--borders=10 \
--separator= \
--width=1400 \
--no-markup \
--height=900 \
--column="Movie:IMG" \
--column="Filename" \
--button="_Last played:/bin/bash -c 'last_video_played'" \
--button="_Exit":1)
[ -z "$movie_choice" ] && exit 0

#--------------------------------------------------------------#
# Opens the select movie from thumbnail
#--------------------------------------------------------------#

movie_dir="$HOME/Videos/.xxxmovies/"

   if [ "$movie_choice" ]; then
     select_movie=$(printf '%s\n' "${movie_dir}${movie_choice}.mp4")
     export select_movie
     echo "$select_movie" | tee ~/Documents/.xxx_previous_video.txt
     "$MY_VIDEO_PLAYER" "$select_movie" && killall yad
   else
     echo "program terminated" && exit 0
   fi

#--------------------------------------------------------------#
# Unset variables and functions
#--------------------------------------------------------------#

unset check_size
unset movie_dir
unset select_movie
unset MY_VIDEO_PLAYER
unset THUMBNAIL_SIZE
unset last_video_played
unset MY_GTK_THEME
