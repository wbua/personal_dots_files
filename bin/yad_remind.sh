#!/bin/bash

# Program command: yad_remind.sh
# Description: Creates reminder text file.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 18-11-2023
# Program_license: GPL 3.0
# Dependencies: yad

#---------------------------------------------#
# Yad settings
#---------------------------------------------#

choice=$(GTK_THEME="Material-Black-Mango-LA" yad --form --width=650 --height=400 --title="Reminders" \
--borders=20 \
--text-align="center" \
--align="center" \
--text="Reminders\n" \
--separator="#" \
--field="Title ":CE \
--field="Notes ":CE \
--field="Date ":CE \
--field="Priority ":CE \
--field="\nCreated by John Mcgrath":LBL "" \
--button="Create":0 \
--button="Exit":1)
[[ -z "$choice" ]] && exit

#---------------------------------------------#
# Entry fields 
#---------------------------------------------#

title=$(echo "$choice" | awk -F '#' '{print $1}')
export title
notes=$(echo "$choice" | awk -F '#' '{print $2}')
export notes
date=$(echo "$choice" | awk -F '#' '{print $3}')
export date
priority=$(echo "$choice" | awk -F '#' '{print $4}')
export priority

#---------------------------------------------#
# Color field separators
#---------------------------------------------#

color_field="<span color='#646B86' font_family='Material Icons Outlined' font='22' rise='-8pt'></span>"
color_field2="<span color='#0CFF00' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"
color_field3="<span color='#00ECFF' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"
color_field4="<span color='#FFA200' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"
color_field5="<span color='#FF0068' font_family='Cascadia Mono' weight='medium' font='16' rise='0pt'>"
end="</span>"

#---------------------------------------------#
# Reminder text file template
#---------------------------------------------#

text_file="${color_field}${color_field2} TITLE${end} ${title}${color_field3} NOTES${end} ${notes}${color_field4} DATE${end} ${date}${color_field5} PRIORITY${end} ${priority}"

#---------------------------------------------#
# Main
#---------------------------------------------#

sed -i '/^[[:space:]]*$/d' "$HOME/Documents/.reminders.txt"
echo "$text_file" | tee -a "$HOME/Documents/.reminders.txt"

#---------------------------------------------#
# Unset variables and functions
#---------------------------------------------#

unset title
unset notes
unset date
unset priority
