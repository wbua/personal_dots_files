#!/bin/bash

find "$HOME"/Pictures/thumb_walls/ -type f -iname '*.png' -print0 | \
xargs -0 mogrify -format png -thumbnail 250x *.png | \
zenity --progress --width=500 --height=100 \
--title="Sway wallpapers" --text="Converting..." --auto-close \
--auto-kill --pulsate && zenity --info --width=500 --height=100 \
--title="Sway wallpapers" --text="Convert completed"
