#!/bin/bash

# Initialize an empty variable to store document file paths
document_files=""

# Loop through each file in dropover_list.txt
while IFS= read -r filepath; do
  # Skip empty lines in the file
  if [[ -z "$filepath" ]]; then
    continue
  fi

  # Check if the file exists and is a regular file (not a directory)
  if [[ -f "$filepath" ]]; then
    # Get the file extension to determine the document type
    file_extension="${filepath##*.}"
    
    # Check if the file is one of the supported document types
    case "$file_extension" in
      odt)
        document_files+="$filepath"$'\n'
        ;;
      ods)
        document_files+="$filepath"$'\n'
        ;;
      odp)
        document_files+="$filepath"$'\n'
        ;;
      txt)
        document_files+="$filepath"$'\n'
        ;;
      pdf)
        document_files+="$filepath"$'\n'
        ;;
      docx)
        document_files+="$filepath"$'\n'
        ;;
      doc)
        document_files+="$filepath"$'\n'
        ;;
      html)
        document_files+="$filepath"$'\n'
        ;;
      csv)
        document_files+="$filepath"$'\n'
        ;;
      *)
        echo "$filepath is not a supported document type, skipping..."
        ;;
    esac
  else
    echo "$filepath does not exist or is not a regular file."
  fi
done < ~/Documents/dropover_list.txt

# Output the list of document files and open them in LibreOffice
if [[ -n "$document_files" ]]; then
  echo "Opening the following document files:"

  # Open each document file in the appropriate LibreOffice application
  while IFS= read -r document; do
    # Skip empty lines (just in case)
    if [[ -z "$document" ]]; then
      continue
    fi

    # Get the file extension to determine the LibreOffice application to use
    file_extension="${document##*.}"

    # Determine the LibreOffice application based on file extension
    case "$file_extension" in
      odt)
        libreoffice_cmd="libreoffice.writer"
        ;;
      ods)
        libreoffice_cmd="libreoffice.calc"
        ;;
      odp)
        libreoffice_cmd="libreoffice.impress"
        ;;
      txt | pdf | docx | doc | html | csv)
        libreoffice_cmd="libreoffice.writer"
        ;; # Default to writer for text-based formats
      *)
        echo "$document is not a recognized document type, skipping."
        continue
        ;;
    esac

    # Ensure the LibreOffice command uses the correct app name
    if [[ "$libreoffice_cmd" == "libreoffice.writer" ]]; then
      # Open document with LibreOffice Writer
      "$libreoffice_cmd" "$document" >/dev/null 2>&1 &
    elif [[ "$libreoffice_cmd" == "libreoffice.calc" ]]; then
      # Open document with LibreOffice Calc
      "$libreoffice_cmd" "$document" >/dev/null 2>&1 &
    elif [[ "$libreoffice_cmd" == "libreoffice.impress" ]]; then
      # Open document with LibreOffice Impress
      "$libreoffice_cmd" "$document" >/dev/null 2>&1 &
    fi
  done <<< "$document_files"
else
  echo "No valid document files found."
fi
