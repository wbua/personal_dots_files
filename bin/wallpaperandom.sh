#!/bin/bash

# Program command: wallpaperandom.sh 
# Description: Randomly sets wallpaper.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 29-04-2023
# Program_license: GPL 3.0
# Dependencies: rofi, feh

#--------------------------------------------------------------------#
# Create directories and files
#--------------------------------------------------------------------#

# This is where the full size screenshots are stored

   if [ -d "$HOME"/Pictures/wallpapers ]; then
     :
   else
     mkdir "$HOME"/Pictures/wallpapers
   fi

#
   if [ -f "$HOME"/Pictures/.fehwallrandom.txt ]; then
     :
   else
     touch "$HOME"/Pictures/.fehwallrandom.txt
   fi

#--------------------------------------------------------------------#
# Wallpaper changer (prefix: w) (rofi only)
#--------------------------------------------------------------------#

# Desktop wallpapers location
screendir="$HOME/Pictures/wallpapers"

# Autostart file
myautostart="$HOME/.xmonad/scripts/autostart.sh"

# Sets the line where feh is in your autostart file
lineset="9s"

#--------------------------------------------------------------------#
# Sets wallapaper and changes your autostart script
#--------------------------------------------------------------------#

wfile="$HOME/Pictures/.fehwallrandom.txt"
launch=$(ls "$screendir")
image=$(echo "$launch" | shuf -n1)
echo "$screendir/$image" > "$wfile"
xargs -L 1 -a "$wfile" -d '\n' feh --bg-scale
sed -i "$lineset|^.*$|feh --bg-scale \"$(head "$wfile")\"|" "$myautostart"
