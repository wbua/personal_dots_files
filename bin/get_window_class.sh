#!/bin/bash

# Get window class name and copy it to the clipboard
swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | \
tr -d '"' | wl-copy -n
