#!/bin/bash

# Program command: firefox_userchrome.sh 
# Description: Creates custom userchrome css file for firefox.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 14-02-2025
# Program_license: GPL 3.0
# Dependencies: firefox

# Setting up firefox custom colors

# - Type 'about:support' into firefox address bar and press enter.
# - In the section called 'Application Basics' look for Profile Directory. Then open directory.
# - Open your profile directory. Inside of your profile directory create a directory called 'chrome'.
# - Go into chrome directory. Create a file called 'userChrome.css'. 

# - Make sure the `userChrome.css` file is in the correct location: `~/snap/firefox/common/.mozilla/firefox/<your_profile_folder>/chrome/`.
# - The `<your_profile_folder>` should be the actual profile folder name (e.g., `abc123.default-release`).

# - Type `about:config` in the Firefox address bar and press Enter.
# - Accept the risk and continue if prompted.
# - In the search bar, type `toolkit.legacyUserProfileCustomizations.stylesheets`.
# - Ensure this preference is set to `true`. If it isn't, double-click on it to set it to `true`.

# Exit with non zero status
set -e

#--------------------------------------------------------#
# Sourcing files
#--------------------------------------------------------#

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Color config
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#--------------------------------------------------------#
# User preferences
#--------------------------------------------------------#

# Background color
MY_COLOR_BG="$COLOR_BG"

# Icon color
MY_COLOR_ICON="$COLOR_ICON"

# Alert color
MY_COLOR_ALERT="$COLOR_ALERT"

# Profile directory
MY_DEFAULT_DIR="$FIREFOX_PROFILE_DIRECTORY"

# Path to firefox userchrome css file
COLOR_FIREFOX_DIR="$FIREFOX_PATH_TO_COLORS"

#--------------------------------------------------------#
# Check if userchrome css file exists
#--------------------------------------------------------#

   if [ -f "$COLOR_FIREFOX_DIR"/userChrome.css ]; then
     :
   else
     notify-send "Firefox userchrome.css" "does not exist" && exit 1
   fi

#--------------------------------------------------------#
# Create the userchrome css file
#--------------------------------------------------------#

# Create userchrome file
cat <<EOF > "$COLOR_FIREFOX_DIR"/userChrome.css

/* Change the background color of the tab bar */
#TabsToolbar {
    background-color: $MY_COLOR_BG!important;
    color: #000000;
}

/* Change the text color of the tabs */
.tab-label {
    color: #000 !important;
}

/* Change Address Bar Background */
#urlbar {
    background-color: #FFFFFF !important;
    color: black !important; /* Change text color */
}

/* Change Bookmark Bar Background */
#PersonalToolbar {
    background-color: $MY_COLOR_BG!important; /* Green background color */
}

/* Change Search Bar Background */
#searchbar {
    background-color: #333333 !important;
    color: white !important; /* Change text color */
}

/* Change background color of the primary toolbar row (with address bar and navigation buttons) */
#nav-bar {
    background-color: $MY_COLOR_BG!important;
}

/* Change the background color of the primary toolbar row when hovering */
#nav-bar:hover {
    background-color: $MY_COLOR_BG!important;
}

/* Active tab background */
.tab-background:is([selected], [multiselected]) {
    background-color: $MY_COLOR_ICON!important;
    background-image: none !important;
}

/* Inactive tab text */
.tabbrowser-tab:not([selected]) label {
    color: #f8f8f8 !important;
    }

}

/* Change address bar text color to white */
#nav-bar {
    color: white !important;
}

/* Change bookmarks bar text color to white */
#personal-bookmarks {
    color: white !important;
}

/* Change nav-bar buttons text color to white */
#nav-bar .toolbarbutton-text,
#nav-bar .toolbarbutton-icon {
    fill: white !important;
    color: white !important;
}

EOF
