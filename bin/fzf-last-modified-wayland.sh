#!/bin/bash

# Program command: fzf-last-modified-wayland.sh
# Description: Opens last modified files on your system.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 01-12-2023
# Program_license: GPL 3.0
# Dependencies: fzf

#-------------------------------------------#
# Search directories
#-------------------------------------------#

dirs=( ~/Documents/ ~/Pictures/ ~/Videos/ ~/Downloads/ ~/Music/ )

#-------------------------------------------#
# Last modified files
#-------------------------------------------#

# Open last modified Documents
last_mod_docs() {

choice=$(find "${dirs[@]}" -type f \( -iname '*.doc' -o -iname '*.docx' -o -iname '*.pdf' -o -iname '*.ods' -o -iname '*.xlsx' -o -iname '*.odt' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
nohup xdg-open "$choice" &	
main
	
}

# Open last downloaded file in downloads directory
last_mod_downloaded_file() {

choice=$(find ~/Downloads/ -not -path '*/.*' -type f -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")	
nohup echo "$choice" | xargs -0 -d '\n' xdg-open &
main
	
}

# Open last image
last_mod_image() {

choice=$(find "${dirs[@]}" -type f \( -iname '*.jpg' -o -iname '*.png' -o -iname '*.jpeg' -o -iname '*.webp' -o -iname '*.tiff' -o -iname '*.bmp' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
nohup xdg-open "$choice" &
main
	
}

# Open last video
last_mod_video() {

choice=$(find "${dirs[@]}" -type f \( -iname '*.mp4' -o -iname '*.mkv' -o -iname '*.webm' -o -iname '*.avi' -o -iname '*.wmv' -o -iname '*.mov' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
nohup xdg-open "$choice" &
main

}

# Open last sound file
last_mod_sound() {

choice=$(find "${dirs[@]}" -type f \( -iname '*.mp3' -o -iname '*.wav' -o -iname '*.aiff' -o -iname '*.acc' -o -iname '*.flac' -o -iname '*.wma' -o -iname '*.m4a' -o -iname '*.ogg' \) -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
nohup xdg-open "$choice" &
main

}

#-------------------------------------------#
# Menu
#-------------------------------------------#

menu() {

echo "documents"
echo "download"
echo "image"
echo "video"
echo "sound"
	
}

#-------------------------------------------#
# Main
#-------------------------------------------#

main() {

choice=$(menu | fzf --reverse \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--info=inline \
--pointer=">" \
--prompt 'last modified> ' \
--header='
Press F1 for main menu

' \
--bind='F1:execute(fzf-launcher-wayland.sh {})')

case "$choice" in

 'documents')
   last_mod_docs
 ;;

 'download')
   last_mod_downloaded_file
 ;;

 'image')
   last_mod_image
 ;;

 'video')
   last_mod_video
 ;;

 'sound')
   last_mod_sound
 ;;

 *)
   echo "Something went wrong!" || exit 1
  ;;


esac
	
}
main
