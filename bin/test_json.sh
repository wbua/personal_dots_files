#!/bin/bash

#----------------------------------------------------------------#
#
#----------------------------------------------------------------#

set -e

#----------------------------------------------------------------#
#
#----------------------------------------------------------------#

# Add reminder
add_reminder() {
	
# Yad settings
choice=$(GTK_THEME="Yaru-dark" yad --form \
--width=650 \
--height=400 \
--title="Crete json file" \
--borders=20 \
--text-align="center" \
--align="center" \
--text="Json creator\n" \
--separator="#" \
--field="STATION_NAME ":CE \
--field="URL_LINK ":CE \
--field="GENRES ":CE \
--field="\nCreated by John Mcgrath":LBL "" \
--button="Create":0 \
--button="Exit":1)
export choice
[[ -z "$choice" ]] && exit

}
export -f add_reminder


# Reminder text file template
#text_file=""
#export text_file

# Main
#echo "$text_file" | tee -a "$HOME/Documents/.reminders.txt"	

#----------------------------------------------------------------#
#
#----------------------------------------------------------------#

# Station name
STATION_NAME=$(echo "$choice" | awk -F '#' '{print $1}')
export STATION_NAME
# Station urk
URL_LINK=$(echo "$choice" | awk -F '#' '{print $2}')
export URL_LINK 
# Radio station genre
GENRES=$(echo "$choice" | awk -F '#' '{print $3}')
export GENRES

# jq template
JSON_STRING=$( jq -n \
                  --arg bn "$STATION_NAME" \
                  --arg on "$URL_LINK" \
                  --arg ge "$GENRES" \
                  '{station_name: $bn, url_link: $on, genres: $ge}' )

# Create jq file
echo "$JSON_STRING" > ~/Documents/"radio_$(date '+%d-%m-%Y-%H-%M-%S')".json

#----------------------------------------------------------------#
#
#----------------------------------------------------------------#

unset add_reminder
unset text_file
unset choice
unset STATION_NAME
unset URL_LINK
unset GENRES
