#!/bin/bash

# Settings config
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

sleep 0.3
echo "$SWAY_URGENT_WORKSPACE" "$COLOR_ALERT"

