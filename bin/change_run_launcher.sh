#!/bin/bash

# Program command: change_run_launcher.sh
# Description: Change run launchers for scripts.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 28-05-2024
# Program_license: GPL 3.0
# Dependencies: yad, imagemagick, webp

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#-----------------------------------------------------#
# Gtk theme
#-----------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Run launcher
MY_LAUNCHER="$SELECT_RUN_LAUNCHER"
export MY_LAUNCHER

# Color icon
MY_COLOR="$COLOR_ICON"
export MY_COLOR

#-----------------------------------------------------#
# Colors
#-----------------------------------------------------#

# Currently run launcher
COLOR001="<span color='$MY_COLOR' font_family='Inter' weight='bold' font='13' rise='0pt'>"
# Color end
END="</span>"

#-----------------------------------------------------#
# Yad dialog
#-----------------------------------------------------#

yad_choice=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--width=400 \
--height=50 \
--text-align="center" \
--text="\nCurrent launcher: $(echo ${COLOR001}${MY_LAUNCHER}${END})\nExample: rofi, wofi, fuzzel, tofi, bemenu\nEnter run launcher...\n" \
--borders=10 \
--button="Exit":1)
[ -z "$yad_choice" ] && exit 0

#-----------------------------------------------------#
# Main
#-----------------------------------------------------#

sed -i "s|.*\(SELECT_RUN_LAUNCHER=\).*|SELECT_RUN_LAUNCHER=\"$(printf '%s' "$yad_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1

#-----------------------------------------------------#
# Unset variables and functions
#-----------------------------------------------------#

unset MY_LAUNCHER
unset yad_choice
unset COLOR001
unset END
unset MY_GTK_THEME
unset MY_COLOR
