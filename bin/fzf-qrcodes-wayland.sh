#!/bin/bash

# Program command: fzf-qrcodes-wayland.sh
# Description: Creates and does various things with QR Codes.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 04-12-2023
# Program_license: GPL 3.0
# Dependencies: fzf, xclip, zbar-tools, qrencode

#-------------------------------------------#
# Software preferences
#-------------------------------------------#

# Image viewer
image_viewer="gthumb"

# File manager
file_manager="thunar"

#-------------------------------------------#
# Create files and directories
#-------------------------------------------#

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/.temqrcodes ]; then
     :
   else
     mkdir "$HOME"/Pictures/.temqrcodes
   fi

   if [ -d "$HOME"/Pictures/qr_codes ]; then
     :
   else
     mkdir "$HOME"/Pictures/qr_codes
   fi
 
#-------------------------------------------#
# Colors
#-------------------------------------------#

# Descriptions
color1=$(tput setaf 244)
# Color end
end=$(tput sgr0)

#-------------------------------------------#
# Decode qr code
#-------------------------------------------#

qr_decode() {

find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | \
tail -n 1 | xargs zbarimg --raw | tee "$HOME"/Documents/.temp_decoded_qr_code.txt
choice=$(cat "$HOME"/Documents/.temp_decoded_qr_code.txt | fzf --print-query \
--cycle \
--padding=0% \
--border="none" \
--header="
Press F1 for main menu

" \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:#660020,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'decoded qr code> ' \
--bind='F1:execute(fzf-launcher-wayland.sh {})' \
--info=inline \
--reverse)
[ -z "$choice" ] && exit 0
echo "$choice" | xclip -selection clipboard	
fzf-launcher-wayland.sh
	
}

#-------------------------------------------#
# Create qr code from clipboard contents
#-------------------------------------------#

qr_clip() {

rm -f "$HOME"/Pictures/.temqrcodes/*.png	
xclip -o | tr -d '\n' | qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
one=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
notify-send -i "$one" "Qrcode"
fzf-launcher-wayland.sh

}

#-------------------------------------------#
# Image notification of last qr code created
#-------------------------------------------#

qr_view() {

   if [[ -z "$(ls -A "$HOME"/Pictures/.temqrcodes)" ]]; then
     notify-send "Directory is empty"
     fzf-launcher-wayland.sh
   else
     one=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
     notify-send -i "$one" "Qrcode"
     fzf-launcher-wayland.sh
   fi	
	
}

#-------------------------------------------#
# Open last created qr code in image viewer
#-------------------------------------------#

qr_open() {

   if [[ -z "$(ls -A "$HOME"/Pictures/.temqrcodes)" ]]; then
     notify-send "QR Code directory is empty"
     fzf-launcher-wayland.sh
   else
     nohup find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | \
     xargs -0 ls -tr | tail -n 1 | xargs -0 -d '\n' "$image_viewer" &
     fzf-launcher-wayland.sh
   fi	
	
}

#-------------------------------------------#
# Open last qr code in file manager
#-------------------------------------------#

qr_dir() {
	
chosen=$(find "$HOME"/Pictures/.temqrcodes/ -type f -iname '*.*' | sort | cut -d '/' -f6-)
[ -z "$chosen" ] && exit 0
nohup "$file_manager" "$HOME"/Pictures/qr_codes/"$chosen" &	
fzf-launcher-wayland.sh
	
}

#-------------------------------------------#
# Type text to create qr code
#-------------------------------------------#

qr_out() {
	
choice=$(echo "" | fzf --print-query \
--border="none" \
--header="
Press F1 for main menu

" \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'create qr code by typing> ' \
--cycle \
--info=inline \
--bind='F1:execute(fzf-launcher-wayland.sh {})' \
--reverse)	
rm -f "$HOME"/Pictures/.temqrcodes/*.png
printf '%s\n' "$choice" | tr -d '\n' | qrencode -m 10 -l L -o "$HOME/Pictures/.temqrcodes/$(date '+%d-%m-%Y-%H-%M-%S')".png
cp "$HOME"/Pictures/.temqrcodes/*.png "$HOME"/Pictures/qr_codes/
one=$(find "$HOME"/Pictures/.temqrcodes/ -type f -print0 | xargs -0 ls -tr | tail -n 1)
notify-send -i "$one" "Qrcode"
fzf-launcher-wayland.sh
     
}

#-------------------------------------------#
# Menu
#-------------------------------------------#

menu() {

echo "create ${color1} create by typing text${end}"
echo "dir ${color1} open last qr code in file manager${end}"
echo "open ${color1} open qr code in image viewer${end}"
echo "notify ${color1} send last qr code to notification manager${end}"
echo "clip ${color1} create qr code from clipboard contents${end}"
echo "decode ${color1}decode last qr code created${end}"
	
}

#-------------------------------------------#
# Menu
#-------------------------------------------#

main() {

choice=$(menu | fzf --ansi \
--border="none" \
--header="
Press F1 for main menu

" \
--color='bg:#191919,prompt:#FF0051,header:#929292,info:#FFFFFF,hl+:-1,hl:-1' \
--color='gutter:-1,fg:#FFFFFF,bg+:-1,query:#FFFFFF,fg+:-1' \
--pointer=">" \
--prompt 'qr codes> ' \
--cycle \
--info=inline \
--bind='F1:execute(fzf-launcher-wayland.sh {})' \
--reverse | awk '{print $1}')

case "$choice" in

 'create')
  qr_out
 ;;

 'dir')
  qr_dir
 ;;

 'open')
  qr_open
 ;;

 'notify')
  qr_view
 ;;

 'clip')
  qr_clip
 ;;

 'decode')
  qr_decode
 ;;

 *)
   echo "Something went wrong!" || exit 1
 ;;
 
esac
	
}
main
