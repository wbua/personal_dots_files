#!/bin/bash

# Program command: radio_list_lofi.sh
# Description: Plays lofi radio streams.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 04-10-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad, mpv, playerctl, mpv-mpris, material icons

#------------------------------------------------------------#
# Error checking
#------------------------------------------------------------#

set -euo pipefail

#------------------------------------------------------------#
# Create files and directories
#------------------------------------------------------------#

   # Documents directory
   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi
   # Directory where text file url's is stored
   if [ -d "$HOME"/Documents/mainprefix ]; then
     :
   else
     mkdir "$HOME"/Documents/mainprefix
   fi
   # Contains radio station url's
   if [ -f "$HOME"/Documents/mainprefix/lofi_stations.txt ]; then
     :
   else
     touch "$HOME"/Documents/mainprefix/lofi_stations.txt
   fi
   # Contains playerctl volume command
   if [ -f "$HOME"/Documents/.lofi_volume.txt ]; then
     :
   else
     touch "$HOME"/Documents/.lofi_volume.txt
     echo "playerctl --player mpv volume 1%" > "$HOME"/Documents/.lofi_volume.txt
   fi
   # Contains mpv volume level
   if [ -f "$HOME"/Documents/.mpv_volume.txt ]; then
     :
   else
     touch "$HOME"/Documents/.mpv_volume.txt
   fi
   
   # Send title data from playerctl to text file
   if [ -f "$HOME"/Documents/track_log.txt ]; then
     :
   else
     touch "$HOME"/Documents/track_log.txt
   fi

#------------------------------------------------------------#
# Add volume 100% to .lofi_volume.txt, if text is empty
#------------------------------------------------------------#

     volume_file=$(cat "$HOME"/Documents/.lofi_volume.txt)
     export volume_file
   if [[ -z "$volume_file" ]]; then
     echo "playerctl --player mpv volume 1%" | tee "$HOME"/Documents/.lofi_volume.txt
     killall yad
   else
     :
   fi

#------------------------------------------------------------#
# Check .lofi_volume.txt, and convert to 0 - 100 for .mpv_volume.txt
#------------------------------------------------------------#
# .lofi_volume.txt contains playerctl command, example: "playerctl --player mpv volume 1%".
# .mpv_volume.txt contains mpv command for volume, example "100".
# playerctl volume is "volume 1%".
# mpv volume is "--volume=100".

   mpv_vol_text=$(cat "$HOME"/Documents/.lofi_volume.txt)
   export mpv_vol_text
   # Volume 100%
   if [[ "$(echo "$mpv_vol_text" | awk '{print $NF}')" == '1%' ]]; then
     echo "100" | tee "$HOME"/Documents/.mpv_volume.txt
   # Volume 70%
   elif [[ "$(echo "$mpv_vol_text" | awk '{print $NF}')" == '0.7%' ]]; then
     echo "70" | tee "$HOME"/Documents/.mpv_volume.txt
   # Volume 50%
   elif [[ "$(echo "$mpv_vol_text" | awk '{print $NF}')" == '0.5%' ]]; then
     echo "50" | tee "$HOME"/Documents/.mpv_volume.txt
   # Volume 30%
   elif [[ "$(echo "$mpv_vol_text" | awk '{print $NF}')" == '0.3%' ]]; then
     echo "30" | tee "$HOME"/Documents/.mpv_volume.txt
   else
     :
   fi
     
#------------------------------------------------------------#
# Greeting
#------------------------------------------------------------#

_greeting() {

hour=$(date +%H)
export hour
    
   # Afternoon
   if [ "$hour" -ge 12 ] && [ "$hour" -lt 19 ]; then
     echo "<span color='#FFFFFF' font_family='Monospace' font='17'>Good afternoon</span>" "<span color='#F6B381' font_family='Monospace' weight='bold' font='18'>$USER</span>"
   fi
   # Morning
   if [ "$hour" -ge 00 ] && [ "$hour" -lt 12 ]; then
     echo "<span color='#FFFFFF' font_family='Monospace' font='17'>Good morning</span>" "<span color='#F6B381' font_family='Monospace' weight='bold' font='18'>$USER</span>"
   fi
   # Evening
   if [ "$hour" -ge 19 ] && [ "$hour" -lt 24 ]; then
     echo "<span color='#FFFFFF' font_family='Monospace' font='17'>Good evening</span>" "<span color='#F6B381' font_family='Monospace' weight='bold' font='18'>$USER</span>"
   fi	
	
}
export -f _greeting

#------------------------------------------------------------#
# Play radio URL with mpv (contains mpv volume level)
#------------------------------------------------------------#

_play_mpv() {

vol_file=$(cat "$HOME"/Documents/.mpv_volume.txt)
export vol_file
killall mpv
mpv --volume="$vol_file" "$1"
	
}
export -f _play_mpv

#------------------------------------------------------------#
# If mpv is paused it plays mpv 
#------------------------------------------------------------#

_play_radio() {

   if [[ "$(pidof mpv)" ]]; then
     playerctl --player mpv play
   else
     :
   fi

}
export -f _play_radio

#------------------------------------------------------------#
# Stop mpv
#------------------------------------------------------------#

_stop_mpv() {

killall yad
killall mpv	
	
}
export -f _stop_mpv

#------------------------------------------------------------#
# Pause mpv
#------------------------------------------------------------#

_pause_mpv() {

   if [[ "$(pidof mpv)" ]]; then	
     playerctl --player mpv pause	
   else
     :
   fi  
     
}
export -f _pause_mpv

#------------------------------------------------------------#
# Change volume
#------------------------------------------------------------#

_change_volume() {
	
   volume_file=$(cat "$HOME"/Documents/.lofi_volume.txt)
   export volume_file
   # Volume 70%
   if [[ "$volume_file" == 'playerctl --player mpv volume 1%' && "$(pidof mpv)" ]]; then
     echo "playerctl --player mpv volume 0.7%" | tee "$HOME"/Documents/.lofi_volume.txt
	 playerctl --player mpv volume 0.7%
	 notify-send "Volume 70%"
   # Volume 50%
   elif [[ "$volume_file" == 'playerctl --player mpv volume 0.7%' && "$(pidof mpv)" ]]; then
     echo "playerctl --player mpv volume 0.5%" | tee "$HOME"/Documents/.lofi_volume.txt
	 playerctl --player mpv volume 0.5%
	 notify-send "Volume 50%"
   # Volume 30%
   elif [[ "$volume_file" == 'playerctl --player mpv volume 0.5%' && "$(pidof mpv)" ]]; then
     echo "playerctl --player mpv volume 0.3%" | tee "$HOME"/Documents/.lofi_volume.txt
	 playerctl --player mpv volume 0.3%
	 notify-send "Volume 30%"
   # Volume 100%
   elif [[ "$volume_file" == 'playerctl --player mpv volume 0.3%' && "$(pidof mpv)" ]]; then
     echo "playerctl --player mpv volume 1%" | tee "$HOME"/Documents/.lofi_volume.txt
	 playerctl --player mpv volume 1%
	 notify-send "Volume 100%"
   else
     notify-send "Not playing!"
   fi
   
}
export -f _change_volume

#------------------------------------------------------------#
# Check current volume level
#------------------------------------------------------------#

_check_volume() {

   # Volume 100%
   if [[ "$(cat "$HOME"/Documents/.lofi_volume.txt)" == 'playerctl --player mpv volume 1%' ]]; then
     notify-send "Volume 100%"
   # Volume 70%
   elif [[ "$(cat "$HOME"/Documents/.lofi_volume.txt)" == 'playerctl --player mpv volume 0.7%' ]]; then
     notify-send "Volume 70%"
   # Volume 50%
   elif [[ "$(cat "$HOME"/Documents/.lofi_volume.txt)" == 'playerctl --player mpv volume 0.5%' ]]; then
     notify-send "Volume 50%"
   # Volume 30%
   elif [[ "$(cat "$HOME"/Documents/.lofi_volume.txt)" == 'playerctl --player mpv volume 0.3%' ]]; then  
     notify-send "Volume 30%"
   else
     :
   fi  
          
}
export -f _check_volume

#------------------------------------------------------------#
# Check status of mpv
#------------------------------------------------------------#

_mpv_check() {
	
   if [[ "$(playerctl status -p mpv)" == 'Playing' ]]; then
     notify-send "Playing"
   elif	[[ "$(playerctl status -p mpv)" == 'Paused' ]]; then
	 notify-send "Paused"
   else
     notify-send "Not Playing"
   fi  
     
}
export -f _mpv_check

#------------------------------------------------------------#
# Current track for radio stream using playerctl
#------------------------------------------------------------#

_current_track() {

   if [[ "$(pidof mpv)" ]]; then	
      notify-send "$(playerctl --player mpv metadata -f "{{title}}" | tr "&" "+")"
      notify-send "$(playerctl --player mpv metadata -f "{{xesam:url}}" | tr "&" "+")"
   else
     notify-send "Not playing"
   fi
	
}
export -f _current_track

#------------------------------------------------------------#
# Send title data from playerctl to text file
#------------------------------------------------------------#

_clipboard() {

   if [[ "$(pidof mpv)" ]]; then
     sed -i '/^[[:space:]]*$/d' "$HOME"/Documents/track_log.txt
     playerctl --player mpv metadata -f "{{title}}" | tee -a "$HOME"/Documents/track_log.txt /dev/null
     notify-send "Track copied!"
   else
     :
   fi
}
export -f _clipboard

#------------------------------------------------------------#
# Open text file containing track titles
#------------------------------------------------------------#

_title_log() {
	
killall yad	
xdg-open "$HOME"/Documents/track_log.txt
	
}
export -f _title_log

#------------------------------------------------------------#
# Open text file containing radio stations url's
#------------------------------------------------------------#

_radio_textfile() {

killall yad	
xdg-open "$HOME"/Documents/mainprefix/lofi_stations.txt
	
}
export -f _radio_textfile

#------------------------------------------------------------#
# Generate random numbers
#------------------------------------------------------------#
	
key=$RANDOM

#------------------------------------------------------------#
# Main
#------------------------------------------------------------#

_main() {

# Top buttons
GTK_THEME="alt-dialog9" yad --plug="$key" --tabnum=1 \
--separator= \
--borders=0 \
--columns=18 \
--form \
--field="":LBL "" \
--field="":LBL "" \
--field="":LBL "" \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='23'></span></b>":fbtn "bash -c '_mpv_check'" \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='23'></span></b>":fbtn "bash -c '_current_track'" \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='23'></span></b>":fbtn "bash -c '_clipboard'" \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='23'></span></b>":fbtn "bash -c '_title_log'" \
--field="<b><span color='#FFFFFF' font_family='Material Icons' font='23'></span></b>":fbtn "bash -c '_radio_textfile'" \
--field="":LBL "" \
--field="":LBL "" \
--field="":LBL "" &

# Radio streams list
list_view=$(cat "$HOME"/Documents/mainprefix/lofi_stations.txt)
export list_view
echo "$list_view" | GTK_THEME="alt-dialog9B" yad --plug="$key" --tabnum=2 --list --column="radio" \
--no-markup \
--dclick-action="/bin/bash -c '_play_mpv %s'" \
--no-headers &

# Main yad section 
GTK_THEME="alt-dialog9" yad --paned --key="$key" --width=800 --height=490 \
--orient=vertical \
--text="<span color='#FFFFFF' font_family='Monospace' font='25'>$(date +%H:%M)</span>\n$(_greeting)\n<span color='#FFFFFF' font_family='Monospace' font='12'>double click on link to play radio</span>" \
--text-align="center" \
--splitter=70 \
--borders=12 \
--separator= \
--buttons-layout="center" \
--title="Lofi radio player" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='25'></span>:/bin/bash -c '_play_radio'" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='25'></span>:/bin/bash -c '_pause_mpv'" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='25'></span>:/bin/bash -c '_stop_mpv'" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='25'></span>:/bin/bash -c '_change_volume'" \
--button="<span color='#FFFFFF' font_family='Material Icons Outlined' font='25'></span>:/bin/bash -c '_check_volume'" \
--button="<span color='#FFFFFF' font_family='Material Icons' font='25'></span>":1 

}
export -f _main
_main

#------------------------------------------------------------#
# Unset variables and functions
#------------------------------------------------------------#

# Functions
unset _play_mpv
unset _main
unset _stop_mpv
unset _play_radio
unset _pause_mpv
unset _greeting
unset _change_volume
unset _check_volume
unset _mpv_check
unset _current_track
unset _clipboard
unset _title_log
unset _radio_textfile

# Variables
unset color1
unset color2
unset end
unset list_view
unset vol_file
unset volume_file
unset mpv_vol_text
