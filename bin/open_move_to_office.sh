#!/bin/bash

# Program command: open_move_to_office.sh
# Description: Opens program or move to already open instance of program.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 29-09-2024
# Program_license: GPL 3.0
# Dependencies: swaymsg, jq

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-----------------------------------------------------------#
# Error checking
#-----------------------------------------------------------#

set -e

#-----------------------------------------------------------#
# User preferences
#-----------------------------------------------------------#

# Office
MY_OFFICE="$MAIN_OFFICE"

# Libreoffice program
MY_LIBREAPP="$LIBREOFFICE_PROGRAM"

#-----------------------------------------------------------#
# Create files and directories
#-----------------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -f "$HOME"/Documents/.set_workspace_office.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_office.txt
     echo "1" > "$HOME"/Documents/.set_workspace_office.txt
   fi

#-----------------------------------------------------------#
# Check text file is empty
#-----------------------------------------------------------#

check_text_file=$(cat "$HOME"/Documents/.set_workspace_office.txt)

   if [[ -z "$check_text_file" ]]; then
     echo "1" > "$HOME"/Documents/.set_workspace_office.txt
   else
     :
   fi

#-----------------------------------------------------------#
# Libreoffice check
#-----------------------------------------------------------#

libreoffice_check() {

   if [[ "$MY_LIBREAPP" == 'libreoffice-writer' ]]; then
     setsid /bin/bash -c "libreoffice.writer" >/dev/null 2>&1 & disown && exit
   elif [[ "$MY_LIBREAPP" == 'libreoffice-calc' ]]; then
     setsid /bin/bash -c "libreoffice.calc" >/dev/null 2>&1 & disown && exit
   elif [[ "$MY_LIBREAPP" == 'libreoffice-impress' ]]; then
     setsid /bin/bash -c "libreoffice.impress" >/dev/null 2>&1 & disown && exit
   elif [[ "$MY_LIBREAPP" == 'libreoffice-draw' ]]; then
     setsid /bin/bash -c "libreoffice.draw" >/dev/null 2>&1 & disown && exit
   else
     setsid /bin/bash -c "$MAIN_OFFICE" >/dev/null 2>&1 & disown && exit
   fi

}

#-----------------------------------------------------------#
# Checks if text file contains workspace number
#-----------------------------------------------------------#

empty_number() {

set_choice=$(cat "$HOME"/Documents/.set_workspace_office.txt)

   if [[ -z "$set_choice" ]]; then
     libreoffice_check
   else
     swaymsg workspace "$set_choice"
     libreoffice_check
   fi
	
}

#-----------------------------------------------------------#
# Check if set workspaces variable is enabled
#-----------------------------------------------------------#

office_workspace() {

   if [[ "$(grep SPECIFIC_APP_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     empty_number 
   elif [[ "$(grep SPECIFIC_APP_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     libreoffice_check     
   else
     :
   fi	
}

#-----------------------------------------------------------#
# Open program or move to already open instance
#-----------------------------------------------------------#

     active_app=$(swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"' \
     | grep -i "$MAIN_OFFICE" | cut -d: -f2)

     libreoffice_app=$(swaymsg -t get_tree \
        | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"' \
        | grep -i "$LIBREOFFICE_PROGRAM" | cut -d: -f2)

   if [[ "$libreoffice_app" == *"libreoffice"* ]]; then
     swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"' \
     | grep -i "$LIBREOFFICE_PROGRAM" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' focus
   elif [[ "$active_app" == *"$MAIN_OFFICE"* || "$active_app" == *"${MAIN_OFFICE^}"* ]]; then
     swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"' \
     | grep -i "$MAIN_OFFICE" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' focus
   else
     office_workspace
   fi 
