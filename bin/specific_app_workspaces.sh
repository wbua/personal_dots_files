#!/bin/bash

# Program command: specific_app_workspaces.sh
# Description: Set and open app on specific workspace. Only works with main programs.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 22-09-2024
# Program_license: GPL 3.0
# Dependencies: swaymsg, jq 

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-------------------------------------------------#
# Error checking
#-------------------------------------------------#

set -e

#-------------------------------------------------#
# User preferences
#-------------------------------------------------#

# Browser
MY_BROWSER="$MAIN_BROWSER"

# File manager
MY_FILEMANAGER="$MAIN_FILE_MANAGER"

# Terminal
MY_TERMINAL="$MAIN_TERMINAL"

# Development
MY_DEV="$MAIN_DEV"

# Social
MY_SOCIAL="$MAIN_SOCIAL"

# Office
MY_OFFICE="$MAIN_OFFICE"

# Video editor
MY_VIDEOEDITOR="$MAIN_VIDEO_EDITOR"

# Audio editor
MY_SOUNDEDITOR="$MAIN_SOUND_EDITOR"

# Image editor
MY_IMAGEEDITOR="$MAIN_IMAGE_EDITOR"

# Text editor
MY_TEXTEDITOR="$MAIN_TEXT_EDITOR"

#-------------------------------------------------#
# Create files and directories
#-------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   # Browser
   if [ -f "$HOME"/Documents/.set_workspace_browser.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_browser.txt
   fi

   # File manager
   if [ -f "$HOME"/Documents/.set_workspace_filemanager.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_filemanager.txt
   fi

   # File terminal
   if [ -f "$HOME"/Documents/.set_workspace_terminal.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_terminal.txt
   fi

   # Development
   if [ -f "$HOME"/Documents/.set_workspace_dev.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_dev.txt
   fi

   # Social
   if [ -f "$HOME"/Documents/.set_workspace_social.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_social.txt
   fi

   # Office
   if [ -f "$HOME"/Documents/.set_workspace_office.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_office.txt
   fi

   # Video editor
   if [ -f "$HOME"/Documents/.set_workspace_videoeditor.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_videoeditor.txt
   fi

   # Audio editor
   if [ -f "$HOME"/Documents/.set_workspace_audioeditor.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_audioeditor.txt
   fi

   # Image editor
   if [ -f "$HOME"/Documents/.set_workspace_imageeditor.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_imageeditor.txt
   fi

   # Text editor
   if [ -f "$HOME"/Documents/.set_workspace_texteditor.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_texteditor.txt
   fi

#-------------------------------------------------#
# Check specific workspace variable 
#-------------------------------------------------#

   if [[ "$(grep SPECIFIC_APP_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     :
   elif [[ "$(grep SPECIFIC_APP_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     notify-send "Specific app workspaces" "variable is disable" && exit 1
   else
     :
   fi

#-------------------------------------------------#
# Main
#-------------------------------------------------#

# Get window class name
class_name=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | "\(.app_id // .window_properties.class)"' | tr -d '"')

# Show the current workspace number
workspace_number=$(swaymsg -t get_workspaces | jq '.[] | select(.focused==true) | .name' | tr -d '"')

   # Browser
   if [[ "$class_name" == "$MY_BROWSER" || "$class_name" == "${MY_BROWSER^}" ]]; then
     echo "$workspace_number" > ~/Documents/.set_workspace_browser.txt && notify-send "Set browser on" "this workspace"
   # File manager
   elif [[ "$class_name" == "$MY_FILEMANAGER" || "$class_name" == "${MY_FILEMANAGER^}" ]]; then
     echo "$workspace_number" > ~/Documents/.set_workspace_filemanager.txt && notify-send "Set file manager" "on this workspace"
   # Terminal
   elif [[ "$class_name" == "$MY_TERMINAL" || "$class_name" == "${MY_TERMINAL^}" ]]; then
     echo "$workspace_number" > ~/Documents/.set_workspace_terminal.txt && notify-send "Set terminal" "on this workspace"
   # Development
   elif [[ "$class_name" == "$MY_DEV" || "$class_name" == "${MY_DEV^}" ]]; then
     echo "$workspace_number" > ~/Documents/.set_workspace_dev.txt && notify-send "Set dev" "on this workspace"
   # Social
   elif [[ "$class_name" == "$MY_SOCIAL" || "$class_name" == "${MY_SOCIAL^}" ]]; then
     echo "$workspace_number" > ~/Documents/.set_workspace_social.txt && notify-send "Set social" "on this workspace"
   # Libreoffice
   elif [[ "$class_name" == *"libreoffice"* ]]; then
     echo "$workspace_number" > ~/Documents/.set_workspace_office.txt && notify-send "Set office" "on this workspace"
   # Office
   elif [[ "$class_name" == "$MY_OFFICE" || "$class_name" == "${MY_OFFICE^}" ]]; then
     echo "$workspace_number" > ~/Documents/.set_workspace_office.txt && notify-send "Set office" "on this workspace"
   # Video editor
   elif [[ "$class_name" == "$MY_VIDEOEDITOR" || "$class_name" == "${MY_VIDEOEDITOR^}" ]]; then
     echo "$workspace_number" > ~/Documents/.set_workspace_videoeditor.txt && notify-send "Set video editor" "on this workspace"
   # Sound editor
   elif [[ "$class_name" == "$MY_SOUNDEDITOR" || "$class_name" == "${MY_SOUNDEDITOR^}" ]]; then
     echo "$workspace_number" > ~/Documents/.set_workspace_audioeditor.txt && notify-send "Set audio editor" "on this workspace"
   # Image editor
   elif [[ "$class_name" == "$MY_IMAGEEDITOR" || "$class_name" == "${MY_IMAGEEDITOR^}" ]]; then
     echo "$workspace_number" > ~/Documents/.set_workspace_imageeditor.txt && notify-send "Set image editor" "on this workspace"
   # Text editor
   elif [[ "$class_name" == "$MY_TEXTEDITOR" || "$class_name" == "${MY_TEXTEDITOR^}" ]]; then
     echo "$workspace_number" > ~/Documents/.set_workspace_texteditor.txt && notify-send "Set text editor" "on this workspace"
   else
     notify-send "Program not supported"
   fi
    
