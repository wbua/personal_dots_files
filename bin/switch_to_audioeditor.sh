#!/bin/bash

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

#-----------------------------------------------------------#
# Error checking
#-----------------------------------------------------------#

set -e

#-----------------------------------------------------------#
# User preferences
#-----------------------------------------------------------#

# Audio editor
MY_SOUNDEDITOR="$MAIN_SOUND_EDITOR"

#-----------------------------------------------------------#
# Create files and directories
#-----------------------------------------------------------#

   if [ -d "$HOME"/Documents ]; then
     :
   else
     mkdir "$HOME"/Documents
   fi

   if [ -f "$HOME"/Documents/.set_workspace_audioeditor.txt ]; then
     :
   else
     touch "$HOME"/Documents/.set_workspace_audioeditor.txt
     echo "1" > "$HOME"/Documents/.set_workspace_audioeditor.txt
   fi

#-----------------------------------------------------------#
# Check text file is empty
#-----------------------------------------------------------#

check_text_file=$(cat "$HOME"/Documents/.set_workspace_audioeditor.txt)

   if [[ -z "$check_text_file" ]]; then
     echo "1" > "$HOME"/Documents/.set_workspace_audioeditor.txt
   else
     :
   fi

#-----------------------------------------------------------#
# Checks if text file contains workspace number
#-----------------------------------------------------------#

empty_number() {

set_choice=$(cat "$HOME"/Documents/.set_workspace_audioeditor.txt)

   if [[ -z "$set_choice" ]]; then
     /bin/bash -c "$MY_SOUNDEDITOR"
   else
     swaymsg workspace "$set_choice"
     setsid /bin/bash -c "$MY_SOUNDEDITOR" >/dev/null 2>&1 & disown && exit
   fi
	
}

#-----------------------------------------------------------#
# Check if set workspaces variable is enabled
#-----------------------------------------------------------#

audioeditor_workspace() {

   if [[ "$(grep SPECIFIC_APP_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     empty_number 
   elif [[ "$(grep SPECIFIC_APP_WORKSPACES= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     /bin/bash -c "$MY_SOUNDEDITOR"
   else
     :
   fi	
}

#-----------------------------------------------------------#
# Open program or move to already open instance
#-----------------------------------------------------------#

     active_app=$(swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"' \
     | grep -i "$MAIN_SOUND_EDITOR" | cut -d: -f2)

   if [[ "$active_app" == *"$MAIN_SOUND_EDITOR"* || "$active_app" == *"${MAIN_SOUND_EDITOR^}"* ]]; then
     swaymsg -t get_tree \
     | jq -r 'recurse(.nodes[], .floating_nodes[]) | select(.shell) | "\(.id):\(.app_id // .window_properties.class)"' \
     | grep -i "$MAIN_SOUND_EDITOR" | cut -d: -f1 | xargs -I{} swaymsg '[con_id={}]' focus
   else
     audioeditor_workspace
   fi
