#!/bin/bash

# Program command: record_notify.sh
# Description: Screen recorder and notification.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 04-09-2023
# Program_license: GPL 3.0
# Website: https://sourceforge.net/projects/jm-dots/files/
# Dependencies: yad, ffmpeg, material icons

#------------------------------------------------------------------#
# Create files and directories
#------------------------------------------------------------------#

# Create videos directory

   if [ -d "$HOME"/Videos ]; then
     :
   else
     mkdir "$HOME"/Videos
   fi
   
# Create recordings directory

   if [ -d "$HOME"/Videos/recordings ]; then
     :
   else
     mkdir "$HOME"/Videos/recordings
   fi

# This is where the video temporary recordings are stored

   if [ -d "$HOME"/Videos/.yadvidrec ]; then
     :
   else
     mkdir "$HOME"/Videos/.yadvidrec
   fi

#---------------------------------------------------------#
# Check service
#---------------------------------------------------------#

_pro_one() {

   if [[ "$(pidof ffmpeg)" ]]; then
     echo "Recording"
   else
     echo "Not recording"
   fi

}
export -f _pro_one

#------------------------------------------------------------------#
# Start recording
#------------------------------------------------------------------# 

_start_recording() {

killall yad
_main
rm -f "$HOME"/Videos/.yadvidrec/*.*
ffmpeg -video_size 1920x1080 -framerate 30 -f x11grab -i :0.0+0 -f pulse -ac 2 -i default "$HOME/Videos/.yadvidrec/$(date +'%d-%m-%Y-%H%M%S').mkv"

}
export -f _start_recording

#------------------------------------------------------------------#
# Kill recording
#------------------------------------------------------------------#

_kill_recording() {

killall yad
_main	
killall ffmpeg
cp "$HOME"/Videos/.yadvidrec/*.* "$HOME"/Videos/recordings/


}
export -f _kill_recording

#---------------------------------------------------------#
# Yad box
#---------------------------------------------------------#

#_yad() {
	
#yad --form --width=500 --height=200	\
#--text="$(_pro_one)" \
#--button="Close:/bin/bash -c 'killall yad'" \
#--button="Stop:/bin/bash -c '_kill_recording'" \
#--button="Record:/bin/bash -c '_start_recording'" \
#--button="Minimise":0 
	
#}
#export -f _yad
#_yad

#---------------------------------------------------------#
# Main
#---------------------------------------------------------#

_main() {

yad --notification \
--image="record" \
--no-middle \
--separator="#" \
--menu="$(_pro_one)#mousepad!mousepad#record!/bin/bash -c _start_recording#stop!/bin/bash -c _kill_recording" 
#--command="/bin/bash -c '_yad'"

}
export -f _main
_main

#---------------------------------------------------------#
# Unset variables and functions
#---------------------------------------------------------#

# Functions
unset _main
unset _yad
unset _pro_one
unset _start_recording
unset _kill_recording

# Variables


