#!/bin/bash

# Program command: swaykeys.sh 
# Description: Show commands and keybinds in rofi. Then execute keybinds in xmonad.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 27-12-2023
# Program_license: GPL 3.0
# Dependencies: rofi, google material icons font, wtype

#-------------------------------------------------------#
# General variables
#-------------------------------------------------------#

browser="brave"

#-------------------------------------------------------#
# Google search
#-------------------------------------------------------#

google_search() {
  
  choice=$(echo "" | rofi -dmenu -i -p '' -theme-str 'entry {placeholder : "Search google"; placeholder-color: grey;}')
  [ -z "$choice" ] && exit 0
  search="https://www.google.co.uk/search?q={}"
  echo "$choice" | xargs -I{} "$browser" "$search"
  	
}

#-------------------------------------------------------#
# Message
#-------------------------------------------------------#

info_keys() {

echo "${color14}Press enter on selected item to run${end}"

	
}

#-------------------------------------------------------#
# Spaces between menu items
#-------------------------------------------------------#

space=$(printf '%0.1s' " "{1..1})

#-------------------------------------------------------#
# Colors for menu items (pango markup)
#-------------------------------------------------------#

# Prefix
color1="<span color='#85C2A7' font_family='Material Icons' font='20' rise='0pt'>"
# Descriptions
color2="<span color='#FFFFFF' font_family='Cascadia Mono' weight='bold' font='16' rise='7pt'>"
# Keys
color3="<span color='#FFED00' font_family='Cascadia Mono' weight='bold' font='16' rise='7pt'>"
# Descriptions
color4="<span color='#FFFFFF' font_family='Cascadia Mono' weight='bold' font='16' rise='1pt'>"
# Descriptions
color5="<span color='#FFFFFF' font_family='Cascadia Mono' weight='bold' font='16' rise='7pt'>"
# keys
color6="<span color='#FF004D' font_family='Cascadia Mono' weight='bold' font='16' rise='7pt'>"
# Descriptions
color7="<span color='#FFFFFF' font_family='Cascadia Mono' weight='bold' font='16' rise='7pt'>"
# keys
color8="<span color='#FFED00' font_family='Cascadia Mono' weight='bold' font='16' rise='8pt'>"
# Prefix
color9="<span color='#EE99A0' font_family='Material Icons' font='20' rise='0pt'>"
# Prefix
color10="<span color='#FF004D' font_family='Material Icons' font='20' rise='0pt'>"
# Prefix
color11="<span color='#FFFFFF' font_family='Material Icons' font='20' rise='0pt'>"
# Prefix
color12="<span color='#61AFEF' font_family='Material Icons' weight='bold' font='20' rise='0pt'>"
# Prefix
color13="<span color='#28FF00' font_family='Material Icons' weight='bold' font='20' rise='0pt'>"
# Descriptions
color14="<span color='#808080' font_family='Cascadia Mono' weight='bold' font='16' rise='0pt'>"
# keys
color15="<span color='#FF004D' font_family='Cascadia Mono' weight='bold' font='16' rise='7pt'>"
# Color end
end="</span>"

#-------------------------------------------------------#
# Shows menu items in rofi
#-------------------------------------------------------#

launch="${color10}${end}${space}${color7}Start a terminal${end}${space}${color6}win return${end}
${color10}${end}${space}${color7}Kill focused window (close)${end}${space}${color6}win shift q${end}
${color10}${end}${space}${color7}Create pdf file${end}${space}${color6}altgr alt p${end}
${color10}${end}${space}${color7}Create and open bash scripts${end}${space}${color6}altgr s${end}
${color10}${end}${space}${color7}Bookmarks${end}${space}${color6}altgr b${end}
${color10}${end}${space}${color7}Emoji's (copies to clipboard)${end}${space}${color6}altgr e${end}
${color10}${end}${space}${color7}Autostart programs${end}${space}${color6}altgr a${end}
${color10}${end}${space}${color7}Configuration files${end}${space}${color6}altgr c${end}
${color10}${end}${space}${color7}Find files${end}${space}${color6}altgr f${end}
${color10}${end}${space}${color7}Quicklinks${end}${space}${color6}altgr q${end}
${color10}${end}${space}${color7}Main script launcher (use keybind directly)${end}${space}${color6}win space${end}
${color10}${end}${space}${color7}Change wallpaper, press enter to set${end}${space}${color6}altgr w${end}
${color10}${end}${space}${color7}Restart gpaste daemon (clipboard)${end}${space}${color6}altgr x${end}
${color10}${end}${space}${color7}Terminal scratchpad (use keybind directly)${end}${space}${color6}altgr p${end}
${color10}${end}${space}${color7}Create QR Codes${end}${space}${color6}win q${end}
${color10}${end}${space}${color7}Control brave with playerctl${end}${space}${color6}altgr y${end}
${color10}${end}${space}${color7}Screencast (record screen)${end}${space}${color6}win shift r${end}
${color10}${end}${space}${color7}App launcher (drun)${end}${space}${color6}win d${end}
${color10}${end}${space}${color7}Internet radio${end}${space}${color6}altgr r${end}
${color10}${end}${space}${color7}Refresh weather data${end}${space}${color6}altgr d${end}
${color10}${end}${space}${color7}Gmrun${end}${space}${color6}win F2${end}
${color10}${end}${space}${color7}Lock Screen${end}${space}${color6}win alt l${end}
${color10}${end}${space}${color7}Window switcher${end}${space}${color6}alt tab${end}
${color10}${end}${space}${color7}Bring window to current workspace${end}${space}${color6}alt escape${end}
${color10}${end}${space}${color7}Fullscreen screenshot${end}${space}${color6}print${end}
${color10}${end}${space}${color7}Fullscreen delay 5 second screenshot${end}${space}${color6}win shift print${end}
${color10}${end}${space}${color7}Select screenshot${end}${space}${color6}win Print${end}
${color10}${end}${space}${color7}open last screenshot taken${end}${space}${color6}win o${end}
${color10}${end}${space}${color7}Move to workspace on the left${end}${space}${color6}win ctrl left${end}
${color10}${end}${space}${color7}Move to workspace on the right${end}${space}${color6}win ctrl right${end}
${color10}${end}${space}${color7}Gpaste clipboard${end}${space}${color6}win shift v${end}
${color10}${end}${space}${color7}Move back and forth on workspace${end}${space}${color6}win tab${end}
${color10}${end}${space}${color7}Reload the configuration file${end}${space}${color6}win shift c${end}
${color10}${end}${space}${color7}Move window to next workspace and follow${end}${space}${color6}win alt left${end}
${color10}${end}${space}${color7}Exit sway${end}${space}${color6}win shift e${end}
${color10}${end}${space}${color7}Layout tabbed${end}${space}${color6}win w${end}
${color10}${end}${space}${color7}Layout toggle split${end}${space}${color6}win e${end}
${color10}${end}${space}${color7}Fullscreen${end}${space}${color6}win f${end}
${color10}${end}${space}${color7}Floating toggle${end}${space}${color6}win shift space${end}
${color10}${end}${space}${color7}Move scratchpad${end}${space}${color6}win shift minus${end}
${color10}${end}${space}${color7}Scratchpad show${end}${space}${color6}win minus${end}
${color10}${end}${space}${color7}Resize windows mode${end}${space}${color6}win r${end}
${color10}${end}${space}${color7}Left resize shrink width (resize mode)${end}${space}${color6}left${end}
${color10}${end}${space}${color7}Down resize grow height (resize mode)${end}${space}${color6}down${end}
${color10}${end}${space}${color7}Up resize shrink height (resize mode)${end}${space}${color6}up${end}
${color10}${end}${space}${color7}Right resize grow (resize mode)${end}${space}${color6}right${end}
${color10}${end}${space}${color7}Move focus left${end}${space}${color6}win left${end}
${color10}${end}${space}${color7}Move focus down${end}${space}${color6}win down${end}
${color10}${end}${space}${color7}Move focus up${end}${space}${color6}win up${end}
${color10}${end}${space}${color7}Move focus right${end}${space}${color6}win right${end}
${color10}${end}${space}${color7}Swap window left${end}${space}${color6}win shift left${end}
${color10}${end}${space}${color7}Swap window down${end}${space}${color6}win shift down${end}
${color10}${end}${space}${color7}Swap window up${end}${space}${color6}win shift up${end}
${color10}${end}${space}${color7}Swap window right${end}${space}${color6}win shift right${end}
${color10}${end}${space}${color7}Swap focus between tiling and floating area${end}${space}${color6}alt space${end}"

#-------------------------------------------------------#
# Runs keybind through rofi
#-------------------------------------------------------#

choice=$(echo "$launch" | sort | rofi -dmenu -i -markup-rows -p '' -me-select-entry '' -me-accept-entry 'MousePrimary' \
-mesg 'Press enter on selected item' \
-theme-str 'listview {lines: 16; columns: 2;}' \
-theme-str 'message { margin: 30px; }' \
-theme-str 'entry {vertical-align: 0.9; placeholder: "Search keybindings..."; placeholder-color: #FF004D; background-color: #E6000000;}' \
-theme-str 'element-text  {vertical-align: 0.2;}' \
-theme-str 'configuration {hover-select: true; me-accept-entry: "MousePrimary"; me-select-entry: "";}' \
-theme-str '* {background: rgba ( 0, 0, 0, 90 % );}' \
-theme-str 'window {location: center; anchor: center; fullscreen: false; width: 80%; border-radius: 0px; border: 0 0 0 0; }' \
-theme-str 'element.selected.normal {background-color: transparent; border-radius: 5; border-color: #FF004D; padding: 0px 0px 0px 0px; border: 2 2 2 2; text-color: #47FF00;}' \
-theme-str 'inputbar {children: [ prompt,textbox-prompt-colon,entry,case-indicator ]; background-color: #E6000000; border: 0 0 0 0;}' \
-theme-str 'element { highlight: none; padding: 10;}' | awk -F "${color6}+|${color2}" '{print $NF}' | sed 's|</span>||')
[[ -z "$choice" ]] && exit 0

   if [[ "$choice" == 'Exit' ]]; then
     killall yad
   elif [[ "$choice" == 'print' ]]; then
     wtype -k "$choice"
   elif [[ "$choice" == 'altgr p' ]]; then
     :
   elif [[ "$choice" == 'win space' ]]; then
     :
   elif [[ "$choice" == 'altgr alt p' ]]; then
     wtype -M altgr -M alt -k p
   elif [[ "$(echo \"$choice)\" | awk '{print $2}')" == *"shift"* ]]; then
     my_choice01=$(echo "$choice" | awk '{print $1}')
     my_choice02=$(echo "$choice" | awk '{print $2}')
     my_choice03=$(echo "$choice" | awk '{print $3}')
     wtype -M $my_choice01 -M $my_choice02 -k $my_choice03 || exit 1
   elif [[ "$(echo \"$choice)\" | awk '{print $2}')" == *"ctrl"* ]]; then
     my_choice01=$(echo "$choice" | awk '{print $1}')
     my_choice02=$(echo "$choice" | awk '{print $2}')
     my_choice03=$(echo "$choice" | awk '{print $3}')
     wtype -M $my_choice01 -M $my_choice02 -k $my_choice03 || exit 1
   elif [[ "$(echo \"$choice\" | awk '{print $2}')" == *"alt"* && "$choice" != *"altgr"* ]]; then
     my_choice01=$(echo "$choice" | awk '{print $1}')
     my_choice02=$(echo "$choice" | awk '{print $2}')
     my_choice03=$(echo "$choice" | awk '{print $3}')
     wtype -M $my_choice01 -M $my_choice02 -k $my_choice03 || exit 1
   elif [[ "$choice" == 'Google' ]]; then
     google_search
   else
     my_choice01=$(echo "$choice" | awk '{print $1}')
     my_choice02=$(echo "$choice" | awk '{print $2}')
     wtype -M $my_choice01 -k $my_choice02 || exit 1
   fi
   
