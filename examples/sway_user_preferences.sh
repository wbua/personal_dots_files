#!/bin/bash

# Program command: sway_user_preferences.sh
# Description: Contains all my user preferences for sway.
# Contributors: John Mcgrath
# Program_version: 50
# Program updated: 10-03-2025
# Program_license: GPL 3.0

# Put your choices in double quotes.
# Do not put / after the last directory.
# When changing sway variables reload sway. 
# You must exit this file first, before you reload.
# Do not move the variables.

# Image viewer
MAIN_IMAGE_VIEWER="gthumb"

# Image Editor
MAIN_IMAGE_EDITOR="gimp"

# Text editor
MAIN_TEXT_EDITOR="mousepad"

# Video player
MAIN_VIDEO_PLAYER="mpv"

# Other Video player
MAIN_OTHER_VIDEO_PLAYER="smplayer"

# Terminal
MAIN_TERMINAL="foot"

# Other terminal
MAIN_OTHER_TERMINAL="gnome-terminal"

# Browser
MAIN_BROWSER="brave"

# Other browser
MAIN_OTHER_BROWSER="firefox"

# File manager
MAIN_FILE_MANAGER="thunar"

# Other file manager
MAIN_OTHER_FILE_MANAGER="nautilus"

# Pdf Viewer
MAIN_PDF_VIEWER="evince"

# Audio Player
MAIN_AUDIO_PLAYER="audacious"

# Other audio player
MAIN_OTHER_AUDIO_PLAYER="Rhythmbox"

# Sound editor
MAIN_SOUND_EDITOR="audacity"

# Office suite (if libreoffice then type libreoffice)
MAIN_OFFICE="libreoffice"

# libreoffice-calc, libreoffice-writer, libreoffice-impress, libreoffice-draw
LIBREOFFICE_PROGRAM="libreoffice-writer"

# Other office suite
MAIN_OTHER_OFFICE="libreoffice"

# Video editor
MAIN_VIDEO_EDITOR="kdenlive"

# Development
MAIN_DEV="geany"

# Social media
MAIN_SOCIAL="discord"

# Temporary main program
MAIN_TEMP_WIN="firefox"

# Foot font
FONT_FOR_FOOT="Cascadia Code:size=20"

# Yad gtk theme
ALL_YAD_GTK="alt-dialog22"

# Set theme colors with wallpaper (enable/disable)
COLOR_AUTO="enable"

# Where you store all your text files
ALL_TEXT_STORE="$HOME/Documents/mainprefix"

# Music files location (add path to directory to enable or disable)
# Example: /home/user/Music/my music
MAIN_SONGS_DIR="/home/john/Music/MP3_Songs_1"

# Screencast recording sound (on/off)
SCREENCAST_SOUND="on"

# Screencast recording directory
# Example: $HOME/Videos/recordings
MAIN_SCREENCAST_DIR="$HOME/Videos/recordings"

# Screenshots directory
# Example: $HOME/Pictures/screenshots
MAIN_SCREENSHOT_DIR="$HOME/Pictures/screenshots"

# Screenshot delay time
SCREENSHOT_DELAY="10"

# Rename screenshot (enable/disable) 
SCREENSHOT_FILENAME="disable"

# Screenshot file type (jpg,png) 
SCREENSHOT_FILE_TYPE="png"

# Open last screenshot in image viewer or in file manager (open/file)
SCREENSHOT_NOTIFICATION="open"

# Firefox profile directory name
FIREFOX_PROFILE_DIRECTORY="$(cat ~/Documents/my_file_profile)"

# Directory path for firefox user chrome css file
FIREFOX_PATH_TO_COLORS="$HOME/snap/firefox/common/.mozilla/firefox/$FIREFOX_PROFILE_DIRECTORY/chrome"

# Printer
MAIN_PRINTER="Brother_DCP_J1050DW"

# Audio recordings directory
# Example: $HOME/Music/recordings
MAIN_AUDIO_DIR="$HOME/Music/recordings"

# PDF files directory (add path to directory to enable or disable)
# Example: $HOME/Documents/pdf
MAIN_PDF_DIR="$HOME/Documents/pdf"

# Qr Codes directory (add path to directory to enable or disable)
# Example: $HOME/Pictures/qr_codes
MAIN_QR_CODES_DIR="$HOME/Pictures/qr_codes"

# Qr Code color (do not put the # symbol)
QR_CODE_HEX_COLOR="000000"

# Qr Code margin
QR_CODE_MARGIN="5"

# Night light temperature
COLOR_TEMPERATURE_VALUE="6000"

# Emoji file path
# Example: $HOME/Documents/emoji-data.txt
MAIN_EMOJI_FILE="$HOME/Documents/mainprefix/emoji-data.txt"

# Radio streams file (add path to file to enable or disable)
# Example: $HOME/Documents/radiolinks.txt
MAIN_RADIO_FILE="$HOME/Documents/mainprefix/radiolinks.txt"

# Zip files directory (add path to file to enable or disable)
# Example: $HOME/Documents/zips
MAIN_ZIPS_DIR="$HOME/Documents/zips"

# Quick notes file (add path to file to enable or disable)
# Example: $HOME/Documents/quicknotes.txt
MAIN_QNOTES_FILE="$HOME/Documents/quicknotes.txt"

# Config files
# Example: $HOME/Documents/configs.txt
MAIN_CONFIGS_FILE="$HOME/Documents/configs.txt"

# Bookmarks file (add path to file to enable or disable)
# Example: $HOME/Documents/bookmarks.txt
MAIN_BOOKMARKS_FILE="$HOME/Documents/mainprefix/bookmarks.txt"

# Switch to browser when opening link (enable/disable)
SWITCH_TO_BROWSER="enable"

# Switch to text editor when opening a file (enable/disable)
SWITCH_TO_TEXT_EDITOR="enable"

# Switch to file manager when opening file (enable/disable)
SWITCH_TO_FILE_MANAGER="enable"

# Choose your weather service provider. (bbc/wttr)
# Middle click the date module to toggle between date or weather.
# When you are showing the weather module. Right click to update weather information.
# Only used with date and weather toggle module.
WEATHER_SERVICE_PROVIDER="bbc"

# BBC weather api key (website: https://www.bbc.co.uk/weather)
# Enter your city, for example london.
# You will get this result: https://www.bbc.co.uk/weather/2643743
# The api key is 7 digits, enter numbers into MAIN WEATHER API KEY.
# Only used with date and weather toggle module.
MAIN_WEATHER_API_KEY="2643743"

# Wttr weather key. Just input your city location. (example: london or new york)
# If there are cities with the same name as yours. Just put nothing. It will use IP address.
MAIN_WTTR_API_KEY="new-york"

# Shows random wallpaper preview gui (enable/disable)
RANDOM_PREVIEW="enable"

# Set random wallpaper by true shuffle or shuf command (enable/disable)
RANDOM_WALLPAPER_SHUFFLE="enable"

# Generate a new random list file for wallpapers (enable/disable) 
RANDOM_GENERATE_LIST="disable"

# Wallpaper directory (add path to file to enable or disable)
# Example: /home/user/Pictures/wallpapers
MAIN_WALLPAPER_DIR="$HOME/Pictures/sway_wallpapers"

# Currently set wallpaper for sway
# Example: /home/user/Pictures/wallpapers/wallpaper.png
MAIN_SWAY_WALLPAPER="/home/john/Pictures/sway_wallpapers/tron_legacy4.png"

# Wallpaper scaling mode (fill,fit,tile)
WALLPAPER_FILL_MODE="fill"

# Sets a temporary wallpaper for random alphabetical (only works with random preview variable)
# No need to change manually
RANDOM_TEMPORARY_WALLPAPER="/home/john/Pictures/sway_wallpapers/car-synthwave-cityscape-wallpaper.png"

# Sway keyboard layout
SWAY_KEYBOARD_LAYOUT="gb"

# Sway gtk theme
SWAY_GTK_THEME="Yaru"

# Sway gtk icon theme
SWAY_ICON_THEME="Yaru"

# Sway gtk font name 
SWAY_FONT_NAME="Monospace"

# Sway gtk font size
SWAY_FONT_SIZE="12"

# Sway border size
SWAY_BORDER_SIZE="5"

# Sway gaps size
SWAY_GAPS_SIZE="6"

# Sway font (example: JetBrainsMono Nerd font 15.5)
SWAY_FONT="JetBrainsMono Nerd font 15.5"

# Sway focus follows mouse (yes/no)
SWAY_FOCUS_FOLLOWS_MOUSE="no"

# Quicklinks text file (add path to file to enable or disable)
# Example: $HOME/Documents/quick_links.txt
MAIN_QUICKLINKS_FILE="$HOME/Documents/mainprefix/quick_links.txt"

# Set the lock screen to use active window background (disable) or wallpaper (enable)
SWAY_LOCKING_BACKGROUND="disable"

# Use currently set wallpaper or random wallpaper background for lockscreen (enable/disable)
SWAY_RANDOM_LOCKING_BG="disable"

# Play sound effects (enable/disable)
SYSTEM_SOUNDS="enable"

# Keybindings text file
# Example: $HOME/Documents/sway_shortcuts.txt
MAIN_KEYBINDING_FILE="$HOME/Documents/sway_shortcuts.txt"

# Change wallpaper with colors from wallpaper (enable/disable)
# By changing 2 variables in sway_user_preferences.sh file
# Change the wallpaper in the variable MAIN SWAY WALLPAPER
# Change the variable UPDATE COLORS SCRIPT to enable
# Then press the keyboard shortcut win shift b
UPDATE_COLORS_SCRIPT="disable"

# i3blocks program (off) and artist title (on) module toggle at startup (on/off)
APP_ARTIST_TITLE_MOD="off"

# i3blocks date and weather module toggle at startup (on/off)
DATE_WEATHER_MOD="on"

# i3blocks tile layouts and keyboard layout module toggle at startup (on/off)
TILE_TAB_KEYB_MOD="on"

# Gtk light and dark mode toggle (light/dark)
CUSTOM_GTK_MODE_TOGGLE="light"

# Add email to paste into clipboard
SNIPPET_EMAIL="john.smith@example-email.com"

# Rofi theme
ROFI_THEME_CONFIG="/home/john/.config/rofi/KooL_style-8.rasi"

# Select run launcher to use with script. (rofi, wofi, fuzzel, tofi, bemenu) 
SELECT_RUN_LAUNCHER="rofi"

# i3blocks apps launcher module (wofi, rofi, fuzzel, tofi, bemenu)
# Fuzzel does not work when first logging in.
I3BLOCKS_APPS_LAUNCHER="rofi"

# i3blocks color config file for theming
# Example: /home/user/bin/i3blocks_accent_colors
I3BLOCKS_THEMES_CONFIG="$HOME/bin/i3blocks_accent_colors"

# Pango markup rise for icons and text (random module mode)
I3BLOCKS_PANGO_RISE="$HOME/bin/i3blocks_pango_rise.sh"

# Set file associations on startup and reloading (enable/disable)
SWAY_FILE_ASSOCIATIONS="enable"

# Set program in a specific workspace (enable/disable)
SPECIFIC_APP_WORKSPACES="disable"

# Kill confirmation script (enable/disable)
CONFIRM_KILL_APP="enable"

# Main search engine
MAIN_ENGINE_SEARCH="https://www.google.co.uk/search?q="

# Previous online web search
PREVIOUS_ONLINE_SEARCH="desktop wallpapers 4k"

# Suspend time amount
SUSPEND_TIME_AMOUNT="3000"

# i3blocks date format module (%d-%m-%Y = 10-10-2024) (%Y-%m-%d = 2024-10-10) (reload sway)
I3BLOCKS_DATE_FORMAT="%d-%m-%y %H:%M"

# i3block time format module (%I:%M %P = 10:17 am - 12 hour) (%H:%M = 10:18 - 24 hour) 
I3BLOCKS_TIME_FORMAT="%H:%M"

# i3blocks battery module, reload sway when changing (enable/disable)
# No actions
MODULE_BATTERY="enable"

# i3blocks microphone module, reload sway when changing (enable/disable)
# Left click: Show pavucontrol
# Right click: Mute toggle
# Middle click: Audio recorder
# Scroll up: Raise volume
# Scroll down: Lower volume
MODULE_MICROPHONE="disable"

# i3blocks date/weather module, reload sway when changing (enable/disable)
# Left click: Show calendar
# Right click: Update weather
# Middle click: Switch between date and weather
# Scroll up: Show forecast
MODULE_DATE="enable"

# i3blocks time module, reload sway when changing (enable/disable)
# Left click: Timezones
# Right click: Change your timezone
MODULE_TIME="disable"

# i3blocks time module, choose the time zone for your region. (la, gb, ny, au, jp, de)
MODULE_TIMEZONE="gb"

# i3blocks volume module, reload sway when changing (enable/disable)
# Left click: Show pavucontrol
# Right click: Mute toggle
# Scroll up: Raise volume
# Scroll down: Lower volume
MODULE_VOLUME="disable"

# i3blocks brightness module, reload sway when changing (enable/disable)
# Scroll up: Raise volume
# Scroll down: Lower volume
MODULE_BRIGHTNESS="disable"

# i3blocks workspace module, reload sway when changing (enable/disable)
# Left click: Go to empty workspace
# Right click: Send focused window to empty workspace
# Scroll up: Cycle through workspaces
MODULE_WORKSPACE="disable"

# i3blocks multiple workspaces module, reload sway when changing (enable/disable)
# Scroll up: Cycle through workspaces on the right
# Scroll down: Cycle through workspaces on the left
MODULE_MULTIPLE_WORKSPACES="disable"

# i3blocks sway layouts/keyboard layout module, reload sway when changing (enable/disable)
# Left click: Switch layouts
# Middle click: Switch between keyboard layout and layouts
# Scroll up: Switch layout up
# Scroll down: Switch layout down
MODULE_LAYOUTS="disable"

# i3blocks nightlight module, reload sway when changing (enable/disable)
# Left click: Turn on night light
# Right click: Turn off night light
# Middle click: Change color temperature
MODULE_NIGHTLIGHT="disable"

# i3blocks window title/music players module, reload sway when changing (enable/disable)
# Left click: Play pause toggle
# Right click: Next track (Does not work with radio streams)
# Middle click: Toggle between program title or artist and title.
# Scroll up: Skip 30 seconds forward
# Scroll down: Mute toggle
MODULE_PLAYERS="disable"

# i3blocks window title length, reload sway when changing (Number: 0 to 100)
MODULE_TITLE_LENGTH="20"

# i3blocks artist title length, reload sway when changing (Number: 0 to 100)
MODULE_ARTIST_TITLE_LENGTH="10"

# i3blocks artist only length, reload sway when changing (Number: 0 to 100)
MODULE_ONLY_ARTIST_LENGTH="10"

# i3blocks auto screen off module, reload sway when changing (enable/disable)
# Left click: Options for turning screen off
MODULE_MONITOR="disable"

# i3blocks wifi module, reload sway when changing (enable/disable)
# Left click: Options for wifi
MODULE_WIFI="disable"

# i3blocks shortcuts module, reload sway when changing (enable/disable)
# Left click: Show keyboard shortcuts
MODULE_KEYBDS="disable"

# i3blocks notifications module, reload sway when changing (enable/disable) 
# Left click: Toggle on or off
# Right click: Show previous notifications
MODULE_NOTIFICATIONS="enable"

# i3blocks keyboard layout module, reload sway when changing (enable/disable)
# Left click: Set keyboard layout
MODULE_LAYOUT_KEYBOARD="disable"

# i3blocks apps module, reload sway when changing (enable/disable)
# Left click: Search and run apps
# Right click: Change run launcher
MODULE_APPS="disable"

# i3blocks wiki module, reload sway when changing (enable/disable)
# Left click: Go to my gitlab wiki
MODULE_WIKI="disable"

# i3blocks color picker, reload sway when changing (enable/disable)
# When you first login you must use a program first, for the color picker to work.
# Left click: Select color
# Right click: Copy hex code to clipboard
# Middle click: Covert hex to rgb and copy rgb to clipboard
MODULE_PICKER="disable"

# i3blocks program title and track title, reload sway when changing (enable/disable) 
# Left click: Switch back to program title
# Right click: Switch to artist and title
# Middle click: Text to speech, speak program title or artist and title.
MODULE_WIN_TITLE="enable"

# i3blocks quick player (enable/disable)
# Left click: Play pause toggle
# Right click: Next track (Does not work with radio streams)
# Middle click: Stop playback and recording
# Scroll up: Skip 30 seconds forward
# Scroll down: Record 
MODULE_QUICK_PLAYER="disable"

# i3blocks weather module, reload sway when changing (enable/disable)
# This module only works with wttr.
# Left click: Show forecast
# Right click: Update weather
MODULE_WEATHER="disable"

# i3blocks find local files, reload sway when changing (enable/disable)
# Left click: Search local files
# Right click: Google search
# Scroll up: Do various actions on the last file you did actions on.
MODULE_LOCAL_SEARCH="disable"

# i3blocks chatbot (enable/disable)
# Left click: Use chatgpt or copilot search
# Right click: Change chatbot
MODULE_CHATBOT="disable"

# i3blocks power menu (enable/disable)
# Left click: Show power menu
MODULE_POWERMENU="enable"

# i3blocks quick actions (enable/disable)
# Middle click: Switch between modes
# Mode 1: Fullscreen screenshot
# - Left click: Take screenshot
# - Right click: Open screenshot
# Mode 2: Random wallpaper
# - Left click: Random wallpaper
# - Right click: Wallpaper effects
# Mode 3: Color picker
# - Left click: Pick color
# - Right click: Copy hex code to clipboard
MODULE_QUICK_ACTIONS="disable"

# i3blocks settings (enable/disable)
# Left click: Open settings script
MODULE_SETTINGS="disable"

# i3blocks multiple modes brightness,volume and microphone (enable/disable)
MODULE_BRIGHTNESS_VOLUME_MICROPHONE="enable"

# Separate workspaces (enable/disable) 
MODULE_SEPARATE_WORKSPACES="enable"

# Only show active apps when there running (enable) or show them as permanent shortcuts (disable) (enable/disable)
ACTIVE_SHORTCUT_STATUS="disable"

# Active app search engine, right click menu for new tab (duckduckgo.com\google.com\bing.com)
ACTIVE_APP_SEARCH_ENGINE="google.com"

# Active app web search, right click menu (duckduckgo/google/bing)
ACTIVE_APP_WEB_SEARCH="google"

# Shows active browser if it is running (enable/disable)
MODULE_ACTIVE_BROWSER="disable"

# Show active file manager if it is running (enable/disable)
MODULE_ACTIVE_FILEMANAGER="disable"

# Show active terminal if it is running (enable/disable)
MODULE_ACTIVE_TERMINAL="disable"

# Show active text editor if it is running (enable/disable)
MODULE_ACTIVE_TEXTEDITOR="disable"

# Show active temporary app if it is running (enable/disable)
MODULE_ACTIVE_TEMPORARY="disable"

# Show active development app if it is running (enable/disable)
MODULE_ACTIVE_DEVELOPMENT="disable"

# Show active social app if it is running (enable/disable)
MODULE_ACTIVE_SOCIAL="disable"

# Show active image editor app if it is running (enable/disable)
MODULE_ACTIVE_IMAGEEDITOR="disable"

# Show active video editor app if it is running (enable/disable)
MODULE_ACTIVE_VIDEOEDITOR="disable"

# Show active audio editor app if it is running (enable/disable)
MODULE_ACTIVE_AUDIOEDITOR="disable"

# Show active office app if it is running (enable/disable)
MODULE_ACTIVE_OFFICE="disable"
