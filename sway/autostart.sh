#!/bin/bash

# Remember to reload configuration
# Example: exec --no-startup-id your_app

exec --no-startup-id ~/bin/startup_mime_list.sh
exec --no-startup-id ~/bin/create_dirs_from_start.sh
exec --no-startup-id ~/bin/create_files_from_start.sh
exec --no-startup-id ~/bin/weather_api_code.sh
exec --no-startup-id ~/bin/startup_sound.sh
exec --no-startup-id ~/bin/autosway_app_title.sh
exec --no-startup-id ~/bin/i3blocks_only_title_and_track.sh
exec --no-startup-id ~/bin/autosway_layouts.sh
exec --no-startup-id ~/bin/auto_date_weather.sh
exec --no-startup-id ~/bin/check_chatbot_file.sh
exec --no-startup-id nm-applet
exec --no-startup-id blueman-applet
exec --no-startup-id waybar
exec --no-startup-id ~/bin/kill_waybar_on_startup.sh
exec --no-startup-id ~/bin/sway_load_settings.sh
