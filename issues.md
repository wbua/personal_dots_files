## Issues with swaywm and my setup

#### Issue 1: logout unexpectedly

To try and fix this issue I have done the following.  

Added this line to my .profile: export WLR_NO_HARDWARE_CURSORS=1  

Added these lines to my sway config.  

   input "type:mouse" {  
    accel_profile "flat"  
}  

seat seat0 xcursor_theme "default" 24   

---

#### My yad custom gtk.css file

Example: background-image: url("/home/john/Pictures/.yad_css_background/yad_background.jpg");  

It will not let me use $HOME, $USER or tilde.  

---

#### Wofi style.css

Example: @import "/home/john/.config/wofi/colors.css";  

It will not let me use $HOME, $USER or tilde.  
