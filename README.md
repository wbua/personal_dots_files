![](https://gitlab.com/wbua/personal_dots_files/-/raw/main/screenshots/main02.png?ref_type=heads)

![](https://gitlab.com/wbua/personal_dots_files/-/raw/main/screenshots/main03.png?ref_type=heads)

![](https://gitlab.com/wbua/personal_dots_files/-/raw/main/screenshots/main04.png?ref_type=heads)

### My setup

**WM:** Sway   
**Terminal** Foot     
**Bar:** Swaybar with i3blocks    
**Launchers:** Rofi, tofi, fuzzel, wofi, bemenu        
**Dialog boxes:** yad, zenity    

### Supported Programs that are themed

- Firefox
- Thunar
- Foot
- Geany
- Dunst
- yad
- Sway
- i3blocks  
- Rofi  
- Wofi  
- Tofi  
- Fuzzel  
- Bemenu  

### Dependencies

yad, yt-dlp, amixer, zenity, dictd, dict-gcide, aspell, wl-clipboard, qrencode,  
festival, libreoffice, amixer, grim, slurp, ffmpeg, swaylock, wf-recorder, imagemagick, swaymsg,   
jq, zbar-tools, wtype, playerctl, mpv-mpris, fzf, thunar, dunst, pactl, pacmd, arecord,  
audacious, webp, qalc, grimshot, gammastep, slock, fuzzel, tofi, wofi, material icons,  
JetBrainsMono Nerd Font, wget, pamixer, wpctl, pavucontrol, mpv, smplayer, cliphist,  
waybar, sway, i3blocks, libxml2-utils, curl, foot, rofi

This is all of them I think. It something does not work. Open up script and look for dependencies.  

### Fonts

- Jetbrains mono    
- Material Icons  
- Cascadia Code  
- Inter  

### Installation

This has been used and tested only on ubuntu 24.04.  
If you are using anything not based on ubuntu it might not work.  

You will have to created a directory called **bin** in your home directory.  
Copy all the bin scripts into the bin directory.

**Remember to make all the scripts excutable**  

Run this command **chmod u+x script** to make script excutable.    

**If directories do not exist them create them.**  

##### Sounds

Create a new directory in the following path: **/home/user/Music/sounds/**  
Put all the sounds into this directory.  

##### Misc documents

.bashrc and .bash_aliases goes into the home directory.  

.festivalrc goes into the home directory.  

configs.txt goes into ~/Documents/  

emoji-data.txt goes into ~/Documents/mainprefix/  

foot.ini goes into ~/.config/foot/  

gtk.css goes into ~/.config/gtk-3.0/  

mytheme.css goes into ~/.config/gtk-3.0/  

mpv.conf goes into ~/.config/mpv/  

radio_links.txt goes into ~/Documents/mainprefix/  

sway_shortcuts.txt goes into ~/Documents/  

##### Yad gtk.css files

Contents of alt-dialog22/gtk-3.0 directory goes into ~/.local/share/themes/alt-dialog22/gtk-3.0/  

Contents of alt-dialog25/gtk-3.0 directory goes into ~/.local/share/themes/alt-dialog25/gtk-3.0/  

You will need to change this to your username:  

**alt-dialog22/gtk-3.0**  

background-image: url("/home/john/Pictures/.yad_css_background/yad_background.jpg");  

##### Dunst

Copy contents of dunst directory into /.config/dunst/  

##### Fuzzel

Copy contents of fuzzel directory into ~/.config/fuzzel/  

##### i3blocks

Copy contents of i3blocks directory into ~/.config/i3blocks/  

##### Sway

Copy contents of sway directory into ~/.config/sway/  

##### Rofi

Copy contents of rofi directory into ~/.config/rofi/

##### Tofi

Copy contents of tofi directory into ~/.config/tofi/  

##### Wallpapers

Copy contents of wallpapers directory into ~/Pictures/sway_wallpapers/    

##### Waybar

Copy contents of waybar directory into ~/.config/waybar/  

##### Wofi

Copy contents of wofi directory into ~/.config/wofi/  

You will need to change this to your username:  

**style.css**  

@import "/home/john/.config/wofi/colors.css";  
