#!/bin/bash

source ~/.config/i3blocks/scripts/themes/gnome_transparent

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
source "$I3BLOCKS_THEMES_CONFIG"

#-------------------------------------------------------------#
# User preferences
#-------------------------------------------------------------#

# Right edge shape
RIGHT_EDGE="$MODULE_RIGHT_SHAPE"

# Left edge shape
LEFT_EDGE="$MODULE_LEFT_SHAPE"

# Keyboard layout
MY_SWAY_KEYBOARD_LAYOUT="$SWAY_KEYBOARD_LAYOUT"

# Before and after output
SPACE_OUTPUT="$BEFORE_AFTER_OUTPUT"

#-------------------------------------------------------------#
# Create layout file
#-------------------------------------------------------------#

   if [ -f "$HOME"/Documents/.layouts_toggle.txt ]; then
     :
   else
     touch "$HOME"/Documents/.layouts_toggle.txt
     echo "on" > "$HOME"/Documents/.layouts_toggle.txt
   fi

#-------------------------------------------------------------#
# Output layouts
#-------------------------------------------------------------#

# Splith
output_splith() {

   if grep -q "layout" ~/Documents/.random_modules.txt; then
     echo "${color60}${LEFT_EDGE}${end}${color34}${end}${color87}${SPACE_OUTPUT}${SPACE_OUTPUT}${end}${color60}${RIGHT_EDGE}${end}"
   fi
   
echo "${color39}${LEFT_EDGE}${end}${color44}${SPACE_OUTPUT}${SPACE_OUTPUT}${end}${color39}${RIGHT_EDGE}${end}"
	
}

# Splitv
output_splitvert() {

   if grep -q "layout" ~/Documents/.random_modules.txt; then
     echo "${color60}${LEFT_EDGE}${end}${color34}${end}${color87}${SPACE_OUTPUT}${SPACE_OUTPUT}${end}${color60}${RIGHT_EDGE}${end}"
   fi
   
echo "${color39}${LEFT_EDGE}${end}${color44}${SPACE_OUTPUT}${SPACE_OUTPUT}${end}${color39}${RIGHT_EDGE}${end}"
	
}

# Tabbed
output_tabbed() {

   if grep -q "layout" ~/Documents/.random_modules.txt; then
     echo "${color60}${LEFT_EDGE}${end}${color34}${end}${color87}${SPACE_OUTPUT}${SPACE_OUTPUT}${end}${color60}${RIGHT_EDGE}${end}"
   fi
   
echo "${color39}${LEFT_EDGE}${end}${color44}${SPACE_OUTPUT}${SPACE_OUTPUT}${end}${color39}${RIGHT_EDGE}${end}"
	
}

# Empty
output_empty() {

   if grep -q "layout" ~/Documents/.random_modules.txt; then
     echo "${color60}${LEFT_EDGE}${end}${color34}${end}${color87}${SPACE_OUTPUT}${SPACE_OUTPUT}${end}${color60}${RIGHT_EDGE}${end}"
   fi
   
echo "${color39}${LEFT_EDGE}${end}${color44}${SPACE_OUTPUT}${SPACE_OUTPUT}${end}${color39}${RIGHT_EDGE}${end}"
	
}

#-------------------------------------------------------------#
# Sway layouts
#-------------------------------------------------------------#

layout_modes() {

choice=$(swaymsg -t get_tree | jq 'recurse(.nodes[]) | select(.nodes[].focused == true).layout' | tr -d '"')
  
   if [[ "$choice" == "splith" ]]; then
     output_splith  
   elif [[ "$choice" == "splitv" ]]; then 
     output_splitvert  
   elif [[ "$choice" == "tabbed" ]]; then
     output_tabbed  
   else
     output_empty  
   fi

}

#-------------------------------------------------------------#
# Sway keyboard layouts
#-------------------------------------------------------------#

keyboard_layout() {

   if grep -q "layout" ~/Documents/.random_modules.txt; then
     echo "${color60}${LEFT_EDGE}${end}${color34}${end}${color88}${SPACE_OUTPUT}$MY_SWAY_KEYBOARD_LAYOUT${SPACE_OUTPUT}${end}${color60}${RIGHT_EDGE}${end}"
   fi

echo "${color39}${LEFT_EDGE}${end}${color45}${SPACE_OUTPUT}$MY_SWAY_KEYBOARD_LAYOUT${SPACE_OUTPUT}${end}${color39}${RIGHT_EDGE}${end}"
	
}

#-------------------------------------------------------------#
# Toggle status file on or off
#-------------------------------------------------------------#

toggle_file() {

   if [[ "$(cat ~/Documents/.layouts_toggle.txt)" == 'on' ]]; then
     echo "off" > ~/Documents/.layouts_toggle.txt
   elif [[ "$(cat ~/Documents/.layouts_toggle.txt)" == 'off' ]]; then
     echo "on" > ~/Documents/.layouts_toggle.txt
   else
     :
   fi

}

#-------------------------------------------------------------#
# Toggle layouts
#-------------------------------------------------------------#

toggle_layout() {

layout_choice=$(swaymsg -t get_tree | jq 'recurse(.nodes[]) | select(.nodes[].focused == true).layout' | tr -d '"')

   if [[ "$layout_choice" == "splitv" ]]; then
     sleep 0.2
     swaymsg layout splith > /dev/null
   elif [[ "$layout_choice" == "tabbed" ]]; then
     sleep 0.2
     swaymsg layout splitv > /dev/null
   elif [[ "$layout_choice" == "splith" ]]; then  
     sleep 0.2
     swaymsg layout tabbed > /dev/null
   else
     :
   fi
	
}

#-------------------------------------------------------------#
# Switch layout
#-------------------------------------------------------------#

switch_uplayout() {

layout_choice=$(swaymsg -t get_tree | jq 'recurse(.nodes[]) | select(.nodes[].focused == true).layout' | tr -d '"')

   if [[ "$layout_choice" == "splitv" ]]; then
     swaymsg layout splith > /dev/null
   elif [[ "$layout_choice" == "tabbed" ]]; then
     swaymsg layout splitv > /dev/null
   elif [[ "$layout_choice" == "splith" ]]; then  
     swaymsg layout tabbed > /dev/null
   else
     :
   fi
   	
}

#-------------------------------------------------------------#
# Switch layout
#-------------------------------------------------------------#

switch_downlayout() {

layout_choice=$(swaymsg -t get_tree | jq 'recurse(.nodes[]) | select(.nodes[].focused == true).layout' | tr -d '"')

   if [[ "$layout_choice" == "splitv" ]]; then
     swaymsg layout splith > /dev/null
   elif [[ "$layout_choice" == "tabbed" ]]; then
     swaymsg layout splitv > /dev/null
   elif [[ "$layout_choice" == "splith" ]]; then  
     swaymsg layout tabbed > /dev/null
   else
     :
   fi
	
}

#-------------------------------------------------------------#
# Clickable actions
#-------------------------------------------------------------#

case ${BLOCK_BUTTON} in

  1) toggle_layout ;;  
  2) toggle_file ;; 
  4) switch_uplayout ;;
  5) switch_downlayout ;;
  
esac

#-------------------------------------------------------------#
# Shows sway layouts or keyboard layout
#-------------------------------------------------------------#

show_layouts() {

   # Sway layout
   if [[ "$(cat ~/Documents/.layouts_toggle.txt)" == 'on' ]]; then
     layout_modes
   # Keyboard layout
   elif [[ "$(cat ~/Documents/.layouts_toggle.txt)" == 'off' ]]; then
     keyboard_layout
   else
     :
   fi

}

#-------------------------------------------------------------#
# Main
#-------------------------------------------------------------#

   if [[ "$(grep MODULE_LAYOUTS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     show_layouts
   elif [[ "$(grep MODULE_LAYOUTS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     :
   else
     :
   fi
