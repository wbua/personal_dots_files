#!/bin/bash

# Program command: night_light
# Description: i3blocks module for changing color temperature.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 08-03-2025
# Program_license: GPL 3.0
# Dependencies: yad, gammastep
# Website: https://gitlab.com/wbua

# shellcheck source=/dev/null
source ~/.config/i3blocks/scripts/themes/gnome_transparent

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#-----------------------------------------------------#
# User preferences
#-----------------------------------------------------#

# Gtk theme
MY_GTK_THEME="$ALL_YAD_GTK"
export MY_GTK_THEME

# Right edge shape
RIGHT_EDGE="$MODULE_RIGHT_SHAPE"

# Left edge shape
LEFT_EDGE="$MODULE_LEFT_SHAPE"

# Temperature value
MY_TEMP_VALUE="$COLOR_TEMPERATURE_VALUE"
export MY_TEMP_VALUE

# Before and after output
SPACE_OUTPUT="$BEFORE_AFTER_OUTPUT"

#-----------------------------------------------------#
# Turn on
#-----------------------------------------------------#

turn_on() {

   # Sets the temperature
   if [[ "$(pidof gammastep)" ]]; then
     :
   else
     setsid gammastep -O "$MY_TEMP_VALUE" >/dev/null 2>&1 & disown
   fi

}

#-----------------------------------------------------#
# Turn off
#-----------------------------------------------------#

turn_off() {

pkill gammastep 
	
}

#-----------------------------------------------------#
# Yad Dialog
#-----------------------------------------------------#

selected_temp() {

temp_choice=$(GTK_THEME="$MY_GTK_THEME" yad --entry \
--width=400 \
--height=100 \
--borders=10 \
--text-align="center" \
--text="\nEnter number...

Current temperature: $MY_TEMP_VALUE\n" \
--button="_Exit":1)
#[[ -z "$temp_choice" ]] && exit 0

   if [[ -z "$temp_choice" ]]; then
     setsid exit 0 >/dev/null 2>&1 & disown
   else
     sed -i "s|.*\(COLOR_TEMPERATURE_VALUE=\).*|COLOR_TEMPERATURE_VALUE=\"$(printf '%s' "$temp_choice")\"|" ~/bin/sway_user_preferences.sh || exit 1
   fi
	
}
export -f selected_temp

#-----------------------------------------------------#
# Clickable actions
#-----------------------------------------------------#

case ${BLOCK_BUTTON} in
 
  1) turn_on ;;

  2) selected_temp ;;

  3) turn_off ;;
   
esac

#-----------------------------------------------------#
# Show nightlight on
#-----------------------------------------------------#

output_nightlight() {

   if grep -q "night" ~/Documents/.random_modules.txt; then
     echo "${color60}${LEFT_EDGE}${end}${color34}$EMPTY_INBETWEEN_MOD${end}${color80}${SPACE_OUTPUT}${SPACE_OUTPUT}${end}${color60}${RIGHT_EDGE}${end}"
   fi
   
echo "${color39}${LEFT_EDGE}${end}${color34}$EMPTY_INBETWEEN_MOD${end}${color42}${SPACE_OUTPUT}${SPACE_OUTPUT}${end}${color39}${RIGHT_EDGE}${end}"
	
}

#-----------------------------------------------------#
# Show nightlight off
#-----------------------------------------------------#

nightlight_disabled() {

   if grep -q "night" ~/Documents/.random_modules.txt; then
     echo "${color60}${LEFT_EDGE}${end}${color34}$EMPTY_INBETWEEN_MOD${end}${color81}${SPACE_OUTPUT}${SPACE_OUTPUT}${end}${color60}${RIGHT_EDGE}${end}"
   fi

echo "${color39}${LEFT_EDGE}${end}${color34}$EMPTY_INBETWEEN_MOD${end}${color43}${SPACE_OUTPUT}${SPACE_OUTPUT}${end}${color39}${RIGHT_EDGE}${end}"
	
}

#-----------------------------------------------------#
# Show nightlight
#-----------------------------------------------------#

show_nightlight() {

   if [[ "$(pidof gammastep)" ]]; then
     output_nightlight
   else
     nightlight_disabled
   fi

}
export -f show_nightlight

#-----------------------------------------------------#
# Main
#-----------------------------------------------------#

   if [[ "$(grep MODULE_NIGHTLIGHT= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     show_nightlight
   elif [[ "$(grep MODULE_NIGHTLIGHT= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     :
   else
     :
   fi
  
#-----------------------------------------------------#
# Unset variables and functions
#-----------------------------------------------------#

unset selected_temp
unset MY_GTK_THEME
unset show_nightlight
unset MY_TEMP_VALUE
