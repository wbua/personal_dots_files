#!/bin/bash

source ~/.config/i3blocks/scripts/themes/gnome_transparent

   if [[ "$(pidof wf-recorder)" ]]; then
     echo "${color37}REC ${end}"
   elif [[ "$(pidof arecord)" ]]; then 
     echo "${color37}REC ${end}"
   elif [[ "$(ps aux | grep mpv | tr -d '\n')" == *"--stream-record"* ]]; then
     echo "${color37}Radio REC ${end}"
   else
     :
   fi
