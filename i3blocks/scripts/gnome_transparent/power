#!/bin/bash

# Program command: powermenu.sh
# Description: Power menu to shutdown, reboot, lock and logout.
# Contributors: John Mcgrath
# Program_version: 1.0.0
# Program updated: 31-08-2024
# Program_license: GPL 3.0
# Dependencies: yad, google material icons font, slock, ffmpeg, grim, imagemagick

# Yad settings app
# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Colors
# shellcheck source=/dev/null
source "$I3BLOCKS_THEMES_CONFIG"

#-------------------------------------------------------#
# User preferences
#-------------------------------------------------------#

# Sway currently set wallpaper
WALL_BACKGROUND="$MAIN_SWAY_WALLPAPER"
export WALL_BACKGROUND

# Background color
MY_COLOR_BG="$COLOR_BG"
export MY_COLOR_BG

#-------------------------------------------------------#
# Greeting
#-------------------------------------------------------#

greet=$(printf '%s\n' "$USER")
export greet

#-------------------------------------------------------#
# Colors
#-------------------------------------------------------#

# Buttons
COLOR1="<span color='#212132' font_family='Material Icons' font='100' rise='0pt'>"
# Uptime
COLOR2="<span color='#FFFFFF' font_family='Monospace' font='16' rise='0pt'>"
# Goodbye text
COLOR3="<span color='#FFFFFF' font_family='Monospace' font='25'>"
# Greeting
COLOR4="<span color='#FFFFFF' font_family='Monospace' font='25'>"
# End colors
END="</span>"

#-------------------------------------------------------#
# Create files and directories
#-------------------------------------------------------#

   if [ -d "$HOME"/Pictures ]; then
     :
   else
     mkdir "$HOME"/Pictures
   fi

   if [ -d "$HOME"/Pictures/sway_lock ]; then
     :
   else
     mkdir "$HOME"/Pictures/sway_lock
   fi

#-------------------------------------------------------#
# Sound effects
#-------------------------------------------------------#

power_snd_effects() {

   if [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     setsid paplay ~/Music/sounds/locking.mp3 >/dev/null 2>&1 & disown
   elif [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     :
   else
     :
   fi
	
}
export -f power_snd_effects

#-------------------------------------------------------#
# Confirm prompt
#-------------------------------------------------------#

# Reboot
_reboot() {

   if [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     paplay ~/Music/sounds/restarting.mp3
     poweroff --reboot
     killall yad
   elif [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     poweroff --reboot
     killall yad
   else
     :
   fi
       
}
export -f _reboot

# Shutdown 
_shutdown() {

   if [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     paplay ~/Music/sounds/shuttingdown.mp3   
     poweroff --poweroff
     killall yad
   elif [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     poweroff --poweroff
     killall yad
   else
     :
   fi
      
}
export -f _shutdown

# Lock 
_lock() {

   if [[ "$(grep SWAY_LOCKING_BACKGROUND= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     rm -f ~/Pictures/sway_lock/*.* || exit 1
     power_snd_effects
     killall yad
     sleep 0.6
     cp "$WALL_BACKGROUND" ~/Pictures/sway_lock/lockscreen.jpg
     convert ~/Pictures/sway_lock/lockscreen.jpg -fill black -colorize 80% ~/Pictures/sway_lock/lockscreen.jpg     
     setsid swaylock -f -i ~/Pictures/sway_lock/lockscreen.jpg >/dev/null 2>&1 & disown && exit    
   elif [[ "$(grep SWAY_LOCKING_BACKGROUND= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     rm -f ~/Pictures/sway_lock/*.* || exit 1
     power_snd_effects
     killall yad
     sleep 0.6
     grim ~/Pictures/sway_lock/lockscreen.jpg && convert -filter Gaussian -blur 0x1 ~/Pictures/sway_lock/lockscreen.jpg ~/Pictures/sway_lock/lockscreen.jpg
     convert ~/Pictures/sway_lock/lockscreen.jpg -fill black -colorize 80% ~/Pictures/sway_lock/lockscreen.jpg
     setsid swaylock -f -i ~/Pictures/sway_lock/lockscreen.jpg >/dev/null 2>&1 & disown && exit
   else
     :
   fi
      
}
export -f _lock

# Logout
_logout() {

   if [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "enable" ]]; then
     paplay ~/Music/sounds/loggingout.mp3  
     bash ~/bin/sway_logout
     killall yad
   elif [[ "$(grep SYSTEM_SOUNDS= ~/bin/sway_user_preferences.sh | tr -d '\"' | awk -F '=' '{print $NF}')" == "disable" ]]; then
     bash ~/bin/sway_logout
     killall yad
   else
     :
   fi
      
}
export -f _logout

#-------------------------------------------------------#
# Kill yad
#-------------------------------------------------------#

_kill_yad() {

killall yad
	
}
export -f _kill_yad

#-------------------------------------------------------#
# Yad settings
#-------------------------------------------------------#

_yad() {

GTK_THEME="alt-dialog25" yad --form --fullscreen --title="Power Menu" \
--text="\n\n\n\n\n\n${COLOR3}Goodbye${END} ${COLOR4}${greet}${END}\n${COLOR2}$(uptime -p)${END}\n\n\n\n\n\n\n" \
--text-align="center" \
--center \
--borders=35 \
--columns=8 \
--no-buttons \
--buttons-layout="center" \
--field=" ":LBL "" \
--field="<b>
${COLOR1}${END}
</b>":fbtn "/bin/bash -c '_shutdown'" \
--field="<b>${COLOR1}${END}</b>":fbtn "/bin/bash -c '_reboot'" \
--field="<b>${COLOR1}${END}</b>":fbtn "/bin/bash -c '_lock'" \
--field="<b>${COLOR1}${END}</b>":fbtn "/bin/bash -c '_logout'" \
--field="<b>${COLOR1}${END}</b>":fbtn "/bin/bash -c '_kill_yad'" \
--field=" ":LBL "" \

}
export -f _yad
_yad

#-------------------------------------------------------#
# Unset variables and functions
#-------------------------------------------------------#

# Functions
unset _yad
unset _main
unset _menu
unset _shutdown
unset _reboot
unset _lock
unset _logout
unset _kill_yad
unset power_snd_effects

# Variables
unset greet
unset logout
unset lock
unset reboot
unset shutdown
unset MY_GTK_THEME
unset COLOR1
unset COLOR2
unset COLOR3
unset COLOR4
unset END
unset WALL_BACKGROUND
