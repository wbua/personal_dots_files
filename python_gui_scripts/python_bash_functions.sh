#!/bin/bash

# Define functions
function func1 {
    notify-send "Function 1 is running"
}

function func2 {
    notify-send "Function 2 is running"
}

function func3 {
    notify-send "Function 3 is running"
}

function func4 {
    notify-send "Function 4 is running"
}

function func5 {
    notify-send "Function 5 is running"
}

# Check for arguments and call corresponding function
case $1 in
    "func1") func1 ;;
    "func2") func2 ;;
    "func3") func3 ;;
    "func4") func4 ;;
    "func5") func5 ;;
    *)
        echo "Invalid option"
        ;;
esac
