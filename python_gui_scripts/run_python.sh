#!/bin/bash

# shellcheck source=/dev/null
source "$HOME"/bin/sway_user_preferences.sh

# Run the Python script and capture the output into a variable
output=$(python3 gui_entry_to_stdout.py)

# Print the output using echo
echo "$output"
