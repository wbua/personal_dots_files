import tkinter as tk
import sys
from colors import PRIMARY_COLOR, SECONDARY_COLOR, BACKGROUND_COLOR, TEXT_COLOR
import subprocess

# Function to retrieve and print the value from the entry box to stdout
def get_input(event=None):
    user_input = entry.get()  # Get the value from the entry box
    sys.stdout.write(f"{user_input}\n")
    sys.stdout.flush()  # Ensure the output is immediately displayed
    root.quit()  # Exit the application after submitting

# Function to exit the application
def exit_app(event=None):
    root.quit()  # Quit the Tkinter main loop

# Create the main window
root = tk.Tk()
root.title("Change run launcher py")

# Set window size
root.geometry("500x300")

# Apply the background color to the window
root.configure(bg=BACKGROUND_COLOR)

# Set the font style for labels, buttons, and entry
label_font = ("Helvetica", 14, "bold")
button_font = ("Helvetica", 12, "bold")

# Bind the Escape key to exit the app
root.bind("<Escape>", exit_app)

# Bind the Enter key to trigger the submit action
root.bind("<Return>", get_input)

#############################################################
# Gets information from text file

#def read_file_and_display():
    # Read the contents of the text file
    #with open('bash' 'entry_text.sh', 'r') as file:
     #   content = file.read()

    #label.config(text=content, fg='white', bg=BACKGROUND_COLOR, font=("Arial", 16, "bold"), padx=20, pady=10)  # gold text on dark slate background

    # Set the content to the Label widget
    #label.config(text=content)

# Create a Label widget
#label = tk.Label(root, text="", justify=tk.LEFT, width=100, height=2)
#label.pack()

# Automatically read from the file and display the content
#read_file_and_display()
##############################################################
# Get information from echo command in bash script

def run_bash_script_and_display():
    # Run the bash script and capture its output
    result = subprocess.run(['bash', 'entry_text.sh'], capture_output=True, text=True)

    # Get the output from the bash script
    content = result.stdout.strip()  # .strip() to remove any extra newline characters

    # Set the content to the Label widget with custom font, color, padding
    label.config(text=content, fg='white', bg=BACKGROUND_COLOR, font=("Arial", 16, "bold"), padx=20, pady=10)

# Create a Label widget
label = tk.Label(root, text="", justify=tk.LEFT, width=50, height=2)
label.pack()

# Run the bash script and display the result
run_bash_script_and_display()

##############################################################

# Create a label with a themed style
label = tk.Label(root, text="Enter something:", fg="#ECF0F1", bg=BACKGROUND_COLOR, font=label_font)
label.pack(padx=0, pady=10)

# Create an entry box with a themed style
entry = tk.Entry(root, width=30, font=("Helvetica", 14), relief="solid", background=SECONDARY_COLOR, fg="white", bd=0, highlightthickness=0)
entry.pack(padx=10, pady=10)

# Create a submit button with themed style
button = tk.Button(root, text="Submit", command=get_input, font=button_font, background=SECONDARY_COLOR, fg="white", relief="flat", bd=0, highlightthickness=0, activebackground=PRIMARY_COLOR, activeforeground="#fff")
button.pack(padx=10, pady=10)

# Create an exit button with themed style
exit_button = tk.Button(root, text="Exit", command=exit_app, font=button_font, background=SECONDARY_COLOR, fg="white", relief="flat", bd=0, highlightthickness=0, activebackground=PRIMARY_COLOR, activeforeground="#fff")
exit_button.pack(padx=10, pady=10)

# Run the Tkinter event loop
root.mainloop()
