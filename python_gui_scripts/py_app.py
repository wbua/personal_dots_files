import tkinter as tk
from tkinter import ttk
import subprocess
from colors import PRIMARY_COLOR, SECONDARY_COLOR, BACKGROUND_COLOR, TEXT_COLOR

# Create the main window
root = tk.Tk()
root.title("Screenshot pytool")
root.geometry("500x400")
root.configure(bg=BACKGROUND_COLOR)
root.config(padx=20, pady=20)

def read_file_and_display():
    # Read the contents of the text file
    with open('output.txt', 'r') as file:
        content = file.read()

    label.config(text=content, fg='white', bg=BACKGROUND_COLOR, font=("Arial", 16, "bold"))  # gold text on dark slate background

    # Set the content to the Label widget
    label.config(text=content)

# Create a Label widget
label = tk.Label(root, text="", justify=tk.LEFT, width=100, height=2)
label.pack()

# Automatically read from the file and display the content
read_file_and_display()

# Create a style object to apply CSS-like theme
style = ttk.Style()

# Set the theme for buttons
style.configure("TButton", 
                font=("Helvetica", 12), 
                padding=10, 
                relief="solid", 
                background=SECONDARY_COLOR,
                foreground="white")

# Add a hover effect by modifying the button styles
style.map("TButton",
          background=[('active', PRIMARY_COLOR), ('pressed', '#388e3c')])

# Function to call the bash script with a specific function and then exit the GUI
def call_bash_function_and_exit(func_name):
    # Run the bash script with the corresponding function as an argument
    subprocess.run(["bash", "python_bash_functions.sh", func_name])
    
    # Close the GUI after executing the function
    root.quit()  # This will stop the Tkinter event loop and close the GUI window

# Function to exit the application when Escape key is pressed
def exit_on_escape(event):
    root.quit()  # This will stop the Tkinter event loop and close the GUI window

# Bind the Escape key to the exit function
root.bind("<Escape>", exit_on_escape)

# Create 5 buttons with different texts
buttons_text = ["Button 1", "Button 2", "Button 3", "Button 4", "Button 5"]
functions = ["func1", "func2", "func3", "func4", "func5"]

for text, func in zip(buttons_text, functions):
    button = ttk.Button(root, text=text, style="TButton", command=lambda f=func: call_bash_function_and_exit(f))
    button.pack(padx=0, pady=10)

# Run the GUI event loop
root.mainloop()
