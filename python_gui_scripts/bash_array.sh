#!/bin/bash

# Define the associative array
declare -A fruits
fruits=(
    ["apple"]="Red"
    ["banana"]="Yellow"
    ["cherry"]="Red"
    ["date"]="Brown"
    ["elderberry"]="Purple"
)

# Get the keys of the associative array
keys=$(echo ${!fruits[@]})

# Convert keys to a Python-friendly string (comma-separated)
key_string=$(echo $keys | tr ' ' ',')

# Call the Python script, capture the output (selected item) into a variable
selected_item=$(python3 python_list_dialog.py "$key_string")

# Output the selected item to stdout
echo "Selected item: $selected_item"
