
alias lsm='ls -l -t -r'
alias geo='ls . | rofi -dmenu -i -p '' | xargs -0 -d "\n"  geany'
alias radio='bash ./fzf-radio-wayland.sh'
alias ql='bash ./fzf-quicklinks.sh'
alias emo='bash ./fzf-emoji-wayland.sh'
